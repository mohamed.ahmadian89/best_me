using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using FarsiLibrary.Utils;
using FarsiLibrary.Win;

namespace CWMS
{
    public partial class FrmBedehkarDetails : DevComponents.DotNetBar.Metro.MetroForm
    {

        public FrmBedehkarDetails(string gharardad, string co_name)
        {
            InitializeComponent();
            txt_find_ghabz_byCode.Text = gharardad;
            txt_moshtarek.Text = co_name;
            sharj_ghabzTableAdapter.FillByGharardad(cWMSDataSet.sharj_ghabz, Convert.ToInt32(gharardad));
            txtcount.Text = cWMSDataSet.sharj_ghabz.Rows.Count.ToString();
            double mablaghkol = 0;
            for (int i = 0; i < cWMSDataSet.sharj_ghabz.Rows.Count; i++)
            {
                mablaghkol += Convert.ToDouble(cWMSDataSet.sharj_ghabz.Rows[i]["mablaghkol"]);
            }
            txtmablaghkol.Text = mablaghkol.ToString();
            txtmablaghkolhorrof.Text = Classes.clsNumber.Number_to_Str(mablaghkol.ToString());

        }

        private void FrmBedehkarDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'cWMSDataSet.sharj_ghabz' table. You can move, or remove it, as needed.

        }

        private void dataGridViewX1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}