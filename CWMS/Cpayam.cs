using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Cpayam : MyMetroForm
    {
        public Cpayam()
        {
            InitializeComponent();
        }

        private void Cpayam_Load(object sender, EventArgs e)
        {
            btnYes.Focus();
        }
        public static DialogResult Show(string payam)
        {
            Cpayam frm = new Cpayam();
            frm.TopMost = true;
            frm.lblpayam.Text = payam;
            return frm.ShowDialog();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {

        }

        private void lblpayam_Click(object sender, EventArgs e)
        {

        }
    }
}