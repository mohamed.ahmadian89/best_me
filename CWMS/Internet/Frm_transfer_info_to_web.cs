﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Common;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Configuration;

namespace CWMS.Internet
{
    public partial class Frm_transfer_info_to_web : MyMetroForm
    {
        DataTable dt_ab, dt_sharj;
        bool loading =
            true;
        public Frm_transfer_info_to_web()
        {
            InitializeComponent();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (ch_intg.Checked)
                Classes.clsInternet.ConnectionStr = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=true";
            else
                Classes.clsInternet.ConnectionStr = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=false;user id=" + Txtusername.Text + ";password=" + txtpassword.Text;
            if (Classes.clsInternet.Check_connection(Classes.clsInternet.ConnectionStr))
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var connectionStrings = config.ConnectionStrings;
                for (int i = 0; i < connectionStrings.ConnectionStrings.Count; i++)
                {
                    if (connectionStrings.ConnectionStrings[i].Name == "CWMS.Properties.Settings.web_connection")
                    {
                        connectionStrings.ConnectionStrings[i].ConnectionString = Classes.clsInternet.ConnectionStr;
                        break;
                    }
                }

                config.Save(ConfigurationSaveMode.Modified);
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {

        }

        private void cmb_ab_doreh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loading == false)
            {
                txt_ab_count.Text = cmb_ab_doreh.SelectedValue.ToString();
            }
        }

        private void cmb_sharj_doreh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loading == false)
            {
                txt_sharj_count.Text = cmb_sharj_doreh.SelectedValue.ToString();
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (Classes.clsInternet.uploading_moshtarekin_data == true)
            {
                Payam.Show("سیستم در حال انجام عملیات می باشد . لطفا صبر نمایید");
                return;
            }
            if (Classes.clsInternet.Check_connection(Classes.clsInternet.ConnectionStr))
            {

                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(Classes.clsInternet.Upload_Moshtarekin_data));
                th.Start();
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Classes.clsInternet.uploading_ab_ghabz)
            {
                Payam.Show("سیستم در حال انجام عملیات می باشد . لطفا صبر نمایید");
                return;
            }
            if (cmb_ab_doreh.Text != "")
            {
                string dcode = cmb_ab_doreh.Text;
                Payam.Show("لطفا تا اتمام عملیات انتقال اطلاعات قبوض آب صبر نمایید");
                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsInternet.Upload_ab_ghabz));
                th.Start(dcode);
            }
        }

        private void groupPanel3_Click(object sender, EventArgs e)
        {

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (Classes.clsInternet.uploading_sharj_ghabz)
            {
                Payam.Show("سیستم در حال انجام عملیات می باشد . لطفا صبر نمایید");
                return;
            }
            if (cmb_sharj_doreh.Text != "")
            {
                string dcode = cmb_sharj_doreh.Text;
                Payam.Show("لطفا تا اتمام عملیات انتقال اطلاعات قبوض شارژ صبر نمایید");
                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsInternet.Upload_sharj_ghabz));
                th.Start(dcode);
            }
        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsInternet.uploading_setting == true)
            {
                Payam.Show("سیستم در حال انجام عملیات می باشد . لطفا صبر نمایید");
                return;
            }
            if (Classes.clsInternet.Check_connection(Classes.clsInternet.ConnectionStr))
            {

                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(Classes.clsInternet.Upload_Setting));
                th.Start();
            }
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (Classes.clsInternet.downloading_bargeh_khorooj == true)
            {
                Payam.Show("سیستم در حال دریافت اطلاعات می باشد . لطفا صبر نمایید");
                return;
            }
            if (Classes.clsInternet.Check_connection(Classes.clsInternet.ConnectionStr))
            {

                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(Classes.clsInternet.Download_bargeh_khorooj));
                th.Start();
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            if (Classes.clsInternet.uploading_bargeh_khorooj == true)
            {
                Payam.Show("سیستم در حال انتقال اطلاعات می باشد . لطفا صبر نمایید");
                return;
            }
            if (Classes.clsInternet.Check_connection(Classes.clsInternet.ConnectionStr))
            {

                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(Classes.clsInternet.ulpoad_bargeh_khorooj));
                th.Start();
            }
        }

        private void Frm_transfer_info_to_web_Load(object sender, EventArgs e)
        {
            loading = true;
            txt_moshtarekin.Text = Classes.ClsMain.ExecuteScalar("select count(*) from moshtarekin").ToString();
            dt_ab = Classes.ClsMain.GetDataTable("select dcode,count(dcode) as count from ab_ghabz group by dcode order by dcode desc");
            dt_sharj = Classes.ClsMain.GetDataTable("select dcode,count(dcode) as count from sharj_ghabz group by dcode  order by dcode desc");

            cmb_ab_doreh.DataSource = dt_ab;
            cmb_ab_doreh.ValueMember = "count";
            cmb_ab_doreh.DisplayMember = "dcode";

            cmb_sharj_doreh.DataSource = dt_sharj;
            cmb_sharj_doreh.ValueMember = "count";
            cmb_sharj_doreh.DisplayMember = "dcode";
            loading = false;
            cmb_ab_doreh_SelectedIndexChanged(sender, e);
            cmb_sharj_doreh_SelectedIndexChanged(sender, e);


            Classes.clsInternet.ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.web_connection"].ConnectionString;


            try
            {
                string[] con = Classes.clsInternet.ConnectionStr.Split(';');

                txtIP.Text = con[0].Split('=')[1].ToString();
                txt_db_name.Text = con[1].Split('=')[1].ToString();
                if (con.Length == 5)
                {
                    Txtusername.Text = con[3].Split('=')[1].ToString();
                    txtpassword.Text = con[4].Split('=')[1].ToString();
                    ch_intg.Checked = false;

                }
                else
                {
                    txtpassword.Text = Txtusername.Text = "";
                    ch_intg.Checked = true;
                }
            }
            catch (Exception)
            {
                txtIP.Text = txt_db_name.Text = Txtusername.Text = txtpassword.Text = "";
            }



        }
    }
}
