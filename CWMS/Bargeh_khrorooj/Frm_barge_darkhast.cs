﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;

namespace CWMS
{
    public partial class Frm_barge_darkhast : MyMetroForm
    {
        DataTable dt;
        public Frm_barge_darkhast()
        {
            InitializeComponent();
            dt = new DataTable();
        }
        public Frm_barge_darkhast(string gharardad)
        {
            InitializeComponent();
            dt = new DataTable();
            txt_find_moshtarek.Text = gharardad;
            btn_find_company.PerformClick();
        }

        string type = "";

        private void btn_find_company_Click(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(txt_find_moshtarek.Text);
            }
            catch (Exception)
            {
                Payam.Show("شماره قرارداد نامعتبر می باشد");
                txt_find_moshtarek.SelectAll();
                txt_find_moshtarek.Focus();
            }


            if (txt_find_moshtarek.Text == "")
                txt_find_moshtarek.Focus();
            else
            {
                type = "1";
                lbl_moshtarek_name.Text = Classes.clsMoshtarekin.Exist_gharardad(txt_find_moshtarek.Text);
                if (lbl_moshtarek_name.Text == "")
                {
                    btn_new_darkhast.Visible = false;
                    lbl_moshtarek_name.Text = "";
                }
                else
                {
                    btn_new_darkhast.Visible = true;
                    dt = Classes.clsBargeh.GetBrageForMoshtarek(txt_find_moshtarek.Text);
                    dgvDarkhast.DataSource = dt;
                    txt_find_moshtarek.SelectAll();
                    txt_find_moshtarek.Focus();
                    if (dt.Rows.Count == 0)
                    {
                        lblMoshtariCount.Text = "0";
                        lblMoshtgariSum.Text = "0";

                    }
                    else
                    {
                        btn_new_darkhast.Visible = true;
                        int sum = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(dt.Rows[i]["barrasi"]))
                            {
                                sum += Convert.ToInt32(dt.Rows[i]["tedad"]);
                            }
                        }
                        lblMoshtariCount.Text = dt.Rows.Count.ToString();
                        lblMoshtgariSum.Text = sum.ToString();


                    }
                }

            }




        }




        private void FrmBargeDarkhast_Load(object sender, EventArgs e)
        {
            txt_find_moshtarek.Focus();
            //if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
            //    btn_transfer.Visible = true;
        }



        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_company.PerformClick();
        }



        private void dgvDarkhast_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Frm_barge_barrasi frm = new Frm_barge_barrasi(dgvDarkhast.Rows[e.RowIndex].Cells[2].Value.ToString(), dgvDarkhast.Rows[e.RowIndex].Cells[0].Value.ToString(), dgvDarkhast.Rows[e.RowIndex].Cells[5].Value.ToString(), dgvDarkhast.Rows[e.RowIndex].Cells[1].Value.ToString(), dgvDarkhast.Rows[e.RowIndex].Cells[3].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[7].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[8].Value.ToString());
                frm.ShowDialog();
                if (txt_find_moshtarek.Text.Trim() == "")
                    dt = Classes.clsBargeh.GetBrageForMoshtarek("-1");
                else
                    dt = Classes.clsBargeh.GetBrageForMoshtarek(txt_find_moshtarek.Text.Trim());
                dgvDarkhast.DataSource = dt;



                int sum = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dt.Rows[i]["barrasi"]))
                    {
                        sum += Convert.ToInt32(dt.Rows[i]["tedad"]);
                    }
                }
                lblMoshtariCount.Text = dt.Rows.Count.ToString();
                lblMoshtgariSum.Text = sum.ToString();


            }
        }


        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p84)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 84, "کد در خواست:" + dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString());

                if (dgvDarkhast.SelectedRows.Count != 0)
                {
                    if (Convert.ToBoolean(dgvDarkhast.SelectedRows[0].Cells[6].Value) == true)
                    {
                        new Bargeh_khrorooj.Frm_barge_chap(dgvDarkhast.SelectedRows[0].Cells[7].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[8].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[2].Value.ToString()).ShowDialog();

                    }
                    else
                        Payam.Show("امکان چاپ برگه خروج های مربوط به درخواست های تایید نشده امکان پذیر نمی باشد");
                }
                else
                    Payam.Show("لطفا یکی از درخواست ها را انتخاب نمایید");
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (Classes.clsMoshtarekin.UserInfo.p83)
            {


                if (dgvDarkhast.SelectedRows.Count != 0)
                {
                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 83, "کد در خواست:" + dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString());

                    if (Cpayam.Show("آیا مطمئن هستید می خواهید این درخواست را حذف کنید") == DialogResult.Yes)
                    {

                        Classes.clsBargeh.HazDarkhast(dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString(), "درخواست  با کد " + dgvDarkhast.SelectedRows[0].Cells[2].Value.ToString() + " توسط مدیریت از سیستم حذف شد", dgvDarkhast.SelectedRows[0].Cells[2].Value.ToString());
                        Classes.clsBargeh.client_new_Deletelist(Convert.ToInt32(dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString()));
                        btn_find_company.PerformClick();
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (dgvDarkhast.SelectedRows.Count != 0)
            {
                Frm_barge_details frm = new Frm_barge_details(dgvDarkhast.SelectedRows[0].Cells[2].Value.ToString(), dgvDarkhast.SelectedRows[0].Cells[0].Value.ToString());
                frm.ShowDialog();
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            if (txt_find_moshtarek.Text.Trim() != "")
            {
                if (Classes.clsMoshtarekin.UserInfo.p21)
                {
                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 21, "باز کردن فرم صدور برگه خروج برای مشترک:" + txt_find_moshtarek.Text);
                    new frm_barge_sodoor(txt_find_moshtarek.Text).Show();

                }
            }
        }

        private void txt_find_moshtarek_TextChanged(object sender, EventArgs e)
        {
            btn_new_darkhast.Visible = false;
            lbl_moshtarek_name.Text = "";
        }

        private void groupPanel2_Click(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            DataTable dt_moshtarekin = Classes.ClsMain.GetDataTable("select gharardad from moshtarekin");
            string gharardad = "";
            Dictionary<string, string> moshtarek_info = new Dictionary<string, string>();
            Classes.ClsMain.ChangeCulture("e");
            Dictionary<string, int> moshtarek_bargeh_count = new Dictionary<string, int>();
            for (int i = 0; i < dt_moshtarekin.Rows.Count; i++)
            {
                gharardad = dt_moshtarekin.Rows[i][0].ToString();

                string tozih = "Old";
                Classes.ClsMain.ExecuteNoneQuery("insert into barge_khorooj values (" + gharardad + ",0,'" + DateTime.Now + "',N'" + tozih + "',1,5,0,0,1)");
                moshtarek_info[gharardad] = Classes.clsBargeh.GetBareh_Darkhast_code(gharardad);
                moshtarek_bargeh_count[gharardad] = 0;
            }


            OpenFileDialog op = new OpenFileDialog();
            op.ShowDialog();
            string[] contents = File.ReadAllLines(op.FileName);
            string bargeh = "";
            int used = 0;
            int block = 0;
            int counter = 0;


            
            Match match;

            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            con.ConnectionString = Classes.ClsMain.ConnectionStr;
            con.Open();
            SqlTransaction transaction = con.BeginTransaction("SodoorGhabzNahayi");
            com.Connection = con;
            com.Transaction = transaction;

            for (int i = 0; i < contents.Length; i++)
            {
                counter++;
                string[] info = contents[i].Split(' ');
                match = Regex.Match(contents[i], @"\d+");

                gharardad = match.Value.ToString();
                moshtarek_bargeh_count[gharardad]++;

                bargeh = match.NextMatch().NextMatch().Value.ToString();
                if (contents[i].Contains("."))
                {
                    used = 1;
                }
                else
                    used = 0;

                if (contents[i].Substring(contents[i].Length - 1) == "T")
                {
                    block = 0;
                }
                else
                    block = 1;

                Classes.ClsMain.ChangeCulture("e");





                com.CommandText = "insert into barge_khorooj_details values (" + counter + "," + moshtarek_info[gharardad] + ","
                    + block + "," + used + "," + (used == 1 ? "'" + DateTime.Now + "'" : "null") + "," + gharardad + ",'" + DateTime.Now + "')";
                com.ExecuteNonQuery();
                if (i % 100 == 0)
                {
                    com.Transaction.Commit();
                    com.Transaction = con.BeginTransaction("SodoorGhabzNahayi");
                }

            }

            com.Transaction.Commit();
            Classes.ClsMain.ChangeCulture("e");

            for (int i = 0; i < dt_moshtarekin.Rows.Count; i++)
            {
                Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj set tedad=" + moshtarek_bargeh_count[dt_moshtarekin.Rows[i][0].ToString()].ToString()
                    + " where gharardad=" + dt_moshtarekin.Rows[i][0].ToString());
            }

            Classes.ClsMain.ChangeCulture("f");
            Payam.Show("عملیات با موفقیت به پایان رسید");
        }
    }
}