﻿namespace CWMS
{
    partial class Frm_barge_barrasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_cancel = new DevComponents.DotNetBar.ButtonX();
            this.txtTozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.btn_tayid = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblend = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lblstart = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.lblTedad = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.lblgharardad = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lblMoshtarekName = new DevComponents.DotNetBar.LabelX();
            this.lblcode = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dgvElhaghiBedehi = new System.Windows.Forms.DataGridView();
            this.gharardad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.co_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghiBedehi)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btn_cancel);
            this.groupPanel1.Controls.Add(this.txtTozihat);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.btn_tayid);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(23, 229);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1152, 189);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 17;
            this.groupPanel1.Text = "بررسی درخواست ";
            // 
            // btn_cancel
            // 
            this.btn_cancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_cancel.BackColor = System.Drawing.Color.Transparent;
            this.btn_cancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btn_cancel.Location = new System.Drawing.Point(311, 80);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_cancel.Size = new System.Drawing.Size(259, 60);
            this.btn_cancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_cancel.Symbol = "";
            this.btn_cancel.SymbolColor = System.Drawing.Color.Maroon;
            this.btn_cancel.SymbolSize = 9F;
            this.btn_cancel.TabIndex = 7;
            this.btn_cancel.Text = "عدم پذیرش درخواست مشترک";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // txtTozihat
            // 
            this.txtTozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTozihat.Border.Class = "TextBoxBorder";
            this.txtTozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txtTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtTozihat.Location = new System.Drawing.Point(76, 29);
            this.txtTozihat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTozihat.Name = "txtTozihat";
            this.txtTozihat.PreventEnterBeep = true;
            this.txtTozihat.Size = new System.Drawing.Size(935, 28);
            this.txtTozihat.TabIndex = 6;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(996, 20);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(95, 39);
            this.labelX9.TabIndex = 5;
            this.labelX9.Text = "نتیجه بررسی :";
            // 
            // btn_tayid
            // 
            this.btn_tayid.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_tayid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_tayid.BackColor = System.Drawing.Color.Transparent;
            this.btn_tayid.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_tayid.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_tayid.Location = new System.Drawing.Point(619, 80);
            this.btn_tayid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_tayid.Name = "btn_tayid";
            this.btn_tayid.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_tayid.Size = new System.Drawing.Size(259, 60);
            this.btn_tayid.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_tayid.Symbol = "";
            this.btn_tayid.SymbolColor = System.Drawing.Color.Green;
            this.btn_tayid.SymbolSize = 9F;
            this.btn_tayid.TabIndex = 6;
            this.btn_tayid.Text = "تایید درخواست مشترک";
            this.btn_tayid.Click += new System.EventHandler(this.btn_tayid_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.dgvElhaghiBedehi);
            this.groupPanel2.Controls.Add(this.lblend);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.lblstart);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.Controls.Add(this.lblTedad);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.lblgharardad);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.Controls.Add(this.lblMoshtarekName);
            this.groupPanel2.Controls.Add(this.lblcode);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(23, 2);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1152, 219);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 2;
            this.groupPanel2.Text = "اطلاعات درخواست";
            // 
            // lblend
            // 
            this.lblend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblend.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblend.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblend.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblend.ForeColor = System.Drawing.Color.Black;
            this.lblend.Location = new System.Drawing.Point(143, 4);
            this.lblend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblend.Name = "lblend";
            this.lblend.Size = new System.Drawing.Size(81, 39);
            this.lblend.TabIndex = 24;
            this.lblend.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(209, 4);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(37, 39);
            this.labelX7.TabIndex = 23;
            this.labelX7.Text = "تا :";
            // 
            // lblstart
            // 
            this.lblstart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblstart.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblstart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblstart.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblstart.ForeColor = System.Drawing.Color.Black;
            this.lblstart.Location = new System.Drawing.Point(256, 4);
            this.lblstart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblstart.Name = "lblstart";
            this.lblstart.Size = new System.Drawing.Size(81, 39);
            this.lblstart.TabIndex = 22;
            this.lblstart.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(333, 4);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(68, 39);
            this.labelX4.TabIndex = 21;
            this.labelX4.Text = "از شماره :";
            // 
            // lblTedad
            // 
            this.lblTedad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTedad.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTedad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTedad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblTedad.ForeColor = System.Drawing.Color.Black;
            this.lblTedad.Location = new System.Drawing.Point(961, 21);
            this.lblTedad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblTedad.Name = "lblTedad";
            this.lblTedad.Size = new System.Drawing.Size(81, 39);
            this.lblTedad.TabIndex = 20;
            this.lblTedad.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(1080, 21);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(43, 39);
            this.labelX6.TabIndex = 19;
            this.labelX6.Text = "تعداد:";
            // 
            // lblgharardad
            // 
            this.lblgharardad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblgharardad.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblgharardad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblgharardad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblgharardad.ForeColor = System.Drawing.Color.Black;
            this.lblgharardad.Location = new System.Drawing.Point(701, -8);
            this.lblgharardad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblgharardad.Name = "lblgharardad";
            this.lblgharardad.Size = new System.Drawing.Size(81, 39);
            this.lblgharardad.TabIndex = 18;
            this.lblgharardad.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(789, -8);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(60, 39);
            this.labelX5.TabIndex = 17;
            this.labelX5.Text = "قرارداد:";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(783, 21);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(69, 39);
            this.labelX3.TabIndex = 16;
            this.labelX3.Text = "مشترک :";
            // 
            // lblMoshtarekName
            // 
            this.lblMoshtarekName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMoshtarekName.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMoshtarekName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMoshtarekName.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblMoshtarekName.ForeColor = System.Drawing.Color.Black;
            this.lblMoshtarekName.Location = new System.Drawing.Point(476, 21);
            this.lblMoshtarekName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblMoshtarekName.Name = "lblMoshtarekName";
            this.lblMoshtarekName.Size = new System.Drawing.Size(320, 39);
            this.lblMoshtarekName.TabIndex = 15;
            this.lblMoshtarekName.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblcode
            // 
            this.lblcode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblcode.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblcode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblcode.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblcode.ForeColor = System.Drawing.Color.Black;
            this.lblcode.Location = new System.Drawing.Point(961, -8);
            this.lblcode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblcode.Name = "lblcode";
            this.lblcode.Size = new System.Drawing.Size(81, 39);
            this.lblcode.TabIndex = 12;
            this.lblcode.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(1033, -8);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(95, 39);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "کد درخواست :";
            // 
            // dgvElhaghiBedehi
            // 
            this.dgvElhaghiBedehi.AllowUserToAddRows = false;
            this.dgvElhaghiBedehi.AllowUserToDeleteRows = false;
            this.dgvElhaghiBedehi.AllowUserToResizeRows = false;
            this.dgvElhaghiBedehi.BackgroundColor = System.Drawing.Color.White;
            this.dgvElhaghiBedehi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvElhaghiBedehi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardad,
            this.co_name,
            this.sharj,
            this.ab});
            this.dgvElhaghiBedehi.Location = new System.Drawing.Point(24, 68);
            this.dgvElhaghiBedehi.Margin = new System.Windows.Forms.Padding(4);
            this.dgvElhaghiBedehi.Name = "dgvElhaghiBedehi";
            this.dgvElhaghiBedehi.RowHeadersVisible = false;
            this.dgvElhaghiBedehi.RowTemplate.Height = 24;
            this.dgvElhaghiBedehi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvElhaghiBedehi.Size = new System.Drawing.Size(1099, 118);
            this.dgvElhaghiBedehi.TabIndex = 25;
            // 
            // gharardad
            // 
            this.gharardad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.gharardad.HeaderText = "قرارداد";
            this.gharardad.Name = "gharardad";
            // 
            // co_name
            // 
            this.co_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.co_name.FillWeight = 200F;
            this.co_name.HeaderText = "نام مشترک";
            this.co_name.Name = "co_name";
            // 
            // sharj
            // 
            this.sharj.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Format = "0,0";
            this.sharj.DefaultCellStyle = dataGridViewCellStyle1;
            this.sharj.FillWeight = 150F;
            this.sharj.HeaderText = "بدهی شارژ";
            this.sharj.Name = "sharj";
            // 
            // ab
            // 
            this.ab.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Format = "0,0";
            this.ab.DefaultCellStyle = dataGridViewCellStyle2;
            this.ab.FillWeight = 150F;
            this.ab.HeaderText = "بدهی آب";
            this.ab.Name = "ab";
            // 
            // Frm_barge_barrasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 431);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_barge_barrasi";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmBargeBarrasi_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghiBedehi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblMoshtarekName;
        private DevComponents.DotNetBar.LabelX lblcode;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btn_tayid;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_cancel;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTozihat;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX lblgharardad;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX lblTedad;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX lblend;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX lblstart;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.DataGridView dgvElhaghiBedehi;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardad;
        private System.Windows.Forms.DataGridViewTextBoxColumn co_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharj;
        private System.Windows.Forms.DataGridViewTextBoxColumn ab;
    }
}