﻿namespace CWMS.Bargeh_khrorooj
{
    partial class frm_Bageh_print_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.btn_not_printed = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_not_prined = new System.Windows.Forms.TextBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_not_printed_count = new System.Windows.Forms.TextBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dgv_not_printed = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tarikh = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tedadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bs_not_printed = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset = new CWMS.Ab.MainAbDataset();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.btnb_printed = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_in_printed = new System.Windows.Forms.TextBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_printed_count = new System.Windows.Forms.TextBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.dgv_printed = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tarikh_1 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bs_printed = new System.Windows.Forms.BindingSource(this.components);
            this.barge_khorooj_detailsTableAdapter = new CWMS.Ab.MainAbDatasetTableAdapters.barge_khorooj_detailsTableAdapter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grpUpdateGhobooozAb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_not_printed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs_not_printed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_printed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs_printed)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(45, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(975, 475);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grpUpdateGhobooozAb);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(967, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "               لیست درخواست های در انتظار چاپ            ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.White;
            this.grpUpdateGhobooozAb.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.buttonX4);
            this.grpUpdateGhobooozAb.Controls.Add(this.buttonX2);
            this.grpUpdateGhobooozAb.Controls.Add(this.buttonX1);
            this.grpUpdateGhobooozAb.Controls.Add(this.btn_not_printed);
            this.grpUpdateGhobooozAb.Controls.Add(this.txt_find_not_prined);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX6);
            this.grpUpdateGhobooozAb.Controls.Add(this.txt_not_printed_count);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX1);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX2);
            this.grpUpdateGhobooozAb.Controls.Add(this.dgv_not_printed);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(48, 19);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(892, 409);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 22;
            this.grpUpdateGhobooozAb.Text = "لیست درخواست ها";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(26, 3);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX4.Size = new System.Drawing.Size(228, 28);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX4.SymbolSize = 12F;
            this.buttonX4.TabIndex = 114;
            this.buttonX4.Text = "چاپ درخواست انتخاب شده";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(26, 355);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 2, 2, 8);
            this.buttonX2.Size = new System.Drawing.Size(218, 22);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX2.SymbolSize = 12F;
            this.buttonX2.TabIndex = 113;
            this.buttonX2.Text = "درخواست های قدیمی";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(26, 329);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 2, 2, 8);
            this.buttonX1.Size = new System.Drawing.Size(218, 22);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 112;
            this.buttonX1.Text = "جدیدترین درخواست ها";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btn_not_printed
            // 
            this.btn_not_printed.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_not_printed.BackColor = System.Drawing.Color.Transparent;
            this.btn_not_printed.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_not_printed.Location = new System.Drawing.Point(671, 3);
            this.btn_not_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_not_printed.Name = "btn_not_printed";
            this.btn_not_printed.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btn_not_printed.Size = new System.Drawing.Size(34, 27);
            this.btn_not_printed.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_not_printed.Symbol = "";
            this.btn_not_printed.SymbolColor = System.Drawing.Color.Teal;
            this.btn_not_printed.SymbolSize = 12F;
            this.btn_not_printed.TabIndex = 110;
            this.btn_not_printed.Click += new System.EventHandler(this.btn_not_printed_Click);
            // 
            // txt_find_not_prined
            // 
            this.txt_find_not_prined.Location = new System.Drawing.Point(713, 2);
            this.txt_find_not_prined.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_find_not_prined.Name = "txt_find_not_prined";
            this.txt_find_not_prined.Size = new System.Drawing.Size(82, 28);
            this.txt_find_not_prined.TabIndex = 109;
            this.txt_find_not_prined.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_not_prined_KeyDown);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(574, 0);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(283, 33);
            this.labelX6.TabIndex = 108;
            this.labelX6.Text = "قرارداد :";
            this.labelX6.WordWrap = true;
            // 
            // txt_not_printed_count
            // 
            this.txt_not_printed_count.Location = new System.Drawing.Point(479, 340);
            this.txt_not_printed_count.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_not_printed_count.Name = "txt_not_printed_count";
            this.txt_not_printed_count.ReadOnly = true;
            this.txt_not_printed_count.Size = new System.Drawing.Size(82, 28);
            this.txt_not_printed_count.TabIndex = 103;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(824, 339);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(33, 27);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Green;
            this.labelX1.SymbolSize = 15F;
            this.labelX1.TabIndex = 102;
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(530, 331);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(283, 42);
            this.labelX2.TabIndex = 101;
            this.labelX2.Text = "تعداد درخواست های موجود در لیست انتظار چاپ :";
            this.labelX2.WordWrap = true;
            // 
            // dgv_not_printed
            // 
            this.dgv_not_printed.AllowUserToAddRows = false;
            this.dgv_not_printed.AllowUserToDeleteRows = false;
            this.dgv_not_printed.AllowUserToResizeColumns = false;
            this.dgv_not_printed.AllowUserToResizeRows = false;
            this.dgv_not_printed.AutoGenerateColumns = false;
            this.dgv_not_printed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_not_printed.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_not_printed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_not_printed.ColumnHeadersHeight = 30;
            this.dgv_not_printed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tarikh,
            this.idDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.tedadDataGridViewTextBoxColumn,
            this.startDataGridViewTextBoxColumn,
            this.finishDataGridViewTextBoxColumn});
            this.dgv_not_printed.DataSource = this.bs_not_printed;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_not_printed.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_not_printed.EnableHeadersVisualStyles = false;
            this.dgv_not_printed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_not_printed.Location = new System.Drawing.Point(26, 37);
            this.dgv_not_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_not_printed.Name = "dgv_not_printed";
            this.dgv_not_printed.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_not_printed.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_not_printed.RowHeadersVisible = false;
            this.dgv_not_printed.RowTemplate.Height = 40;
            this.dgv_not_printed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_not_printed.Size = new System.Drawing.Size(837, 288);
            this.dgv_not_printed.TabIndex = 68;
            this.dgv_not_printed.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_not_printed_CellDoubleClick);
            // 
            // tarikh
            // 
            this.tarikh.DataPropertyName = "tarikh";
            this.tarikh.HeaderText = "تاریخ درخواست";
            this.tarikh.Name = "tarikh";
            this.tarikh.ReadOnly = true;
            this.tarikh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "کد درخواست";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tedadDataGridViewTextBoxColumn
            // 
            this.tedadDataGridViewTextBoxColumn.DataPropertyName = "tedad";
            this.tedadDataGridViewTextBoxColumn.HeaderText = "تعداد برگه ها";
            this.tedadDataGridViewTextBoxColumn.Name = "tedadDataGridViewTextBoxColumn";
            this.tedadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // startDataGridViewTextBoxColumn
            // 
            this.startDataGridViewTextBoxColumn.DataPropertyName = "start";
            this.startDataGridViewTextBoxColumn.HeaderText = "شروع ";
            this.startDataGridViewTextBoxColumn.Name = "startDataGridViewTextBoxColumn";
            this.startDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // finishDataGridViewTextBoxColumn
            // 
            this.finishDataGridViewTextBoxColumn.DataPropertyName = "finish";
            this.finishDataGridViewTextBoxColumn.HeaderText = "پایان";
            this.finishDataGridViewTextBoxColumn.Name = "finishDataGridViewTextBoxColumn";
            this.finishDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bs_not_printed
            // 
            this.bs_not_printed.DataMember = "barge_khorooj_details";
            this.bs_not_printed.DataSource = this.mainAbDataset;
            // 
            // mainAbDataset
            // 
            this.mainAbDataset.DataSetName = "MainAbDataset";
            this.mainAbDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(967, 444);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "       لیست کلیه درخواست های چاپ شده           ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX5);
            this.groupPanel1.Controls.Add(this.buttonX3);
            this.groupPanel1.Controls.Add(this.btnb_printed);
            this.groupPanel1.Controls.Add(this.txt_find_in_printed);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txt_printed_count);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.dgv_printed);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(50, 18);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(892, 409);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 23;
            this.groupPanel1.Text = "لیست درخواست ها";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(26, 11);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX5.Size = new System.Drawing.Size(228, 28);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX5.SymbolSize = 12F;
            this.buttonX5.TabIndex = 115;
            this.buttonX5.Text = "چاپ درخواست انتخاب شده";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(26, 333);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 2, 2, 8);
            this.buttonX3.Size = new System.Drawing.Size(218, 22);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX3.SymbolSize = 12F;
            this.buttonX3.TabIndex = 113;
            this.buttonX3.Text = "آخرین درخواست های چاپ شده";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // btnb_printed
            // 
            this.btnb_printed.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnb_printed.BackColor = System.Drawing.Color.Transparent;
            this.btnb_printed.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnb_printed.Location = new System.Drawing.Point(666, 3);
            this.btnb_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnb_printed.Name = "btnb_printed";
            this.btnb_printed.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnb_printed.Size = new System.Drawing.Size(34, 27);
            this.btnb_printed.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnb_printed.Symbol = "";
            this.btnb_printed.SymbolColor = System.Drawing.Color.Teal;
            this.btnb_printed.SymbolSize = 12F;
            this.btnb_printed.TabIndex = 111;
            this.btnb_printed.Click += new System.EventHandler(this.btnb_printed_Click);
            // 
            // txt_find_in_printed
            // 
            this.txt_find_in_printed.Location = new System.Drawing.Point(708, 2);
            this.txt_find_in_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_find_in_printed.Name = "txt_find_in_printed";
            this.txt_find_in_printed.Size = new System.Drawing.Size(82, 28);
            this.txt_find_in_printed.TabIndex = 107;
            this.txt_find_in_printed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_in_printed_KeyDown);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(569, -3);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(283, 42);
            this.labelX5.TabIndex = 106;
            this.labelX5.Text = "قرارداد :";
            this.labelX5.WordWrap = true;
            // 
            // txt_printed_count
            // 
            this.txt_printed_count.Location = new System.Drawing.Point(569, 339);
            this.txt_printed_count.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_printed_count.Name = "txt_printed_count";
            this.txt_printed_count.ReadOnly = true;
            this.txt_printed_count.Size = new System.Drawing.Size(82, 28);
            this.txt_printed_count.TabIndex = 103;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(824, 339);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(33, 27);
            this.labelX3.Symbol = "";
            this.labelX3.SymbolColor = System.Drawing.Color.Green;
            this.labelX3.SymbolSize = 15F;
            this.labelX3.TabIndex = 102;
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(530, 331);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(283, 42);
            this.labelX4.TabIndex = 101;
            this.labelX4.Text = "تعداد درخواست های چاپ شده :";
            this.labelX4.WordWrap = true;
            // 
            // dgv_printed
            // 
            this.dgv_printed.AllowUserToAddRows = false;
            this.dgv_printed.AllowUserToDeleteRows = false;
            this.dgv_printed.AllowUserToResizeColumns = false;
            this.dgv_printed.AllowUserToResizeRows = false;
            this.dgv_printed.AutoGenerateColumns = false;
            this.dgv_printed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_printed.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_printed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_printed.ColumnHeadersHeight = 30;
            this.dgv_printed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tarikh_1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgv_printed.DataSource = this.bs_printed;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_printed.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_printed.EnableHeadersVisualStyles = false;
            this.dgv_printed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_printed.Location = new System.Drawing.Point(26, 43);
            this.dgv_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_printed.Name = "dgv_printed";
            this.dgv_printed.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_printed.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_printed.RowHeadersVisible = false;
            this.dgv_printed.RowTemplate.Height = 40;
            this.dgv_printed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_printed.Size = new System.Drawing.Size(837, 284);
            this.dgv_printed.TabIndex = 68;
            this.dgv_printed.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_printed_CellDoubleClick);
            // 
            // tarikh_1
            // 
            this.tarikh_1.DataPropertyName = "tarikh";
            this.tarikh_1.HeaderText = "تاریخ درخواست";
            this.tarikh_1.Name = "tarikh_1";
            this.tarikh_1.ReadOnly = true;
            this.tarikh_1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikh_1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "کد درخواست";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "gharardad";
            this.dataGridViewTextBoxColumn2.HeaderText = "قرارداد";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "tedad";
            this.dataGridViewTextBoxColumn3.HeaderText = "تعداد برگه ها";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "start";
            this.dataGridViewTextBoxColumn4.HeaderText = "شروع ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "finish";
            this.dataGridViewTextBoxColumn5.HeaderText = "پایان";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // bs_printed
            // 
            this.bs_printed.DataMember = "barge_khorooj_details";
            this.bs_printed.DataSource = this.mainAbDataset;
            // 
            // barge_khorooj_detailsTableAdapter
            // 
            this.barge_khorooj_detailsTableAdapter.ClearBeforeFill = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frm_Bageh_print_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 556);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_Bageh_print_Management";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سامانه چاپ برگه های خروج";
            this.Load += new System.EventHandler(this.frm_Bageh_print_Management_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            this.grpUpdateGhobooozAb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_not_printed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs_not_printed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_printed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs_printed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_not_printed;
        private Ab.MainAbDataset mainAbDataset;
        private System.Windows.Forms.BindingSource bs_not_printed;
        private Ab.MainAbDatasetTableAdapters.barge_khorooj_detailsTableAdapter barge_khorooj_detailsTableAdapter;
        private System.Windows.Forms.TextBox txt_not_printed_count;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TextBox txt_find_not_prined;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.TextBox txt_find_in_printed;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.TextBox txt_printed_count;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_printed;
        private DevComponents.DotNetBar.ButtonX btn_not_printed;
        private DevComponents.DotNetBar.ButtonX btnb_printed;
        private System.Windows.Forms.BindingSource bs_printed;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikh;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tedadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finishDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikh_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private System.Windows.Forms.Timer timer1;
    }
}