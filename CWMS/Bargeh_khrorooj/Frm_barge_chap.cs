﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;

namespace CWMS.Bargeh_khrorooj
{
    public partial class Frm_barge_chap : MyMetroForm
    {
        public Frm_barge_chap()
        {
            InitializeComponent();
        }
        string gharardad = "", darkhastID = "";
        string MainStart = "", MainFinish = "";
        public Frm_barge_chap(string start, string end, string darkhastIDt, string gharardadt)
        {
            InitializeComponent();
            MainStart = TxtStart.Text = start;
            MainFinish = TxtEnd.Text = end;
            darkhastID = darkhastIDt;
            gharardad = gharardadt;

        }
        private void TxtStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                TxtEnd.Focus();
        }

        private void TxtEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSetNotFaal.PerformClick();
        }

        private void FrmChapBargeh_Load(object sender, EventArgs e)
        {
            TxtStart.Focus();
        }

        private void btnSetNotFaal_Click(object sender, EventArgs e)
        {


            if (Convert.ToInt64(TxtStart.Text) < Convert.ToInt64(MainStart))
            {
                Payam.Show("برگه خروج های این مشترک از " + MainStart + " باید شروع شود ");
                TxtStart.Text = MainStart;
                return;
            }

            if (Convert.ToInt64(TxtEnd.Text) > Convert.ToInt64(MainFinish))
            {
                Payam.Show("برگه خروج های این مشترک از " + MainFinish + " باید کمتر باشد ");
                TxtStart.Text = MainStart;
                return;
            }
            btnSetNotFaal.Enabled = false;

            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.BargehKhorooj rpt = new CrysReports.BargehKhorooj();

            Classes.clsBargeh.ChapBargeh(
                gharardad, darkhastID,
                repVUer,
                rpt
                ,
               Convert.ToInt32(TxtStart.Text), Convert.ToInt32(TxtEnd.Text)
                );
            btnSetNotFaal.Enabled = true;
            this.Close();



        }
    }
}