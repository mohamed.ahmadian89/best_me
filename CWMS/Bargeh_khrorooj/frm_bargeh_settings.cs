﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Bargeh_khrorooj
{
    public partial class frm_bargeh_settings : MyMetroForm
    {


        public frm_bargeh_settings()
        {
            InitializeComponent();
            string[] con_info = Classes.clsBargeh.Client_connection_string.Split(';');
            if (con_info.Length != 0)
            {
                try
                {
                    txtIP.Text = con_info[0].Split('=')[1];
                    txt_db_name.Text = con_info[1].Split('=')[1];
                    if (con_info[3].Split('=')[0].ToString().ToLower().StartsWith("user"))
                    {
                        Txtusername.Text = con_info[3].Split('=')[1];
                        txtpassword.Text = con_info[4].Split('=')[1];
                        ch_intg.Checked = false;
                    }
                    else
                    {
                        Txtusername.Text = txtpassword.Text = "";
                        ch_intg.Checked = true;
                    }

                    
                }
                catch (Exception)
                {

                }
            }
        }

        private void frm_bargeh_settings_Load(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            string connection = "";
            if (ch_intg.Checked)
            {
                connection = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=true";

            }
            else
            {
                connection = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=false;user id=" + Txtusername.Text + ";password=" + txtpassword.Text;
            }





            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStrings = config.ConnectionStrings;
            for (int i = 0; i < connectionStrings.ConnectionStrings.Count; i++)
            {
                if (connectionStrings.ConnectionStrings[i].Name == "CWMS.Properties.Settings.Client_connectionString")
                {
                    connectionStrings.ConnectionStrings[i].ConnectionString = connection;
                    break;
                }
            }

            config.Save(ConfigurationSaveMode.Modified);
            Classes.clsBargeh.Client_connection_string = connection;
            Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
        }

        static bool Check_connection(string connectionstr)
        {
            bool result = true;
            try
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionstr);
                con.Open();
                Payam.Show("ارتباط با سیستم نگهبانی با موفقیت برقرار شد");
                con.Close();
            }
            catch (Exception)
            {
                Payam.Show("متاسفانه ارتباط با سیستم نگهبانی امکان پذیر نمی باشد");
                result = false;
            }
            return result;
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Check_connection(Classes.clsBargeh.Client_connection_string);

        }

        private void btn_transfer_new_darkhast_Click(object sender, EventArgs e)
        {
            if(Classes.clsBargeh.AddList_started)
            {
                Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                return;
            }
            

            if (ch_online.Checked)
            {
                if (Classes.clsBargeh.Check_connection())
                {
                    Payam.Show("لطفا تا اتمام عملیات بروز رسانی کمی صبر نمایید");
                    Classes.clsBargeh.AddList_started = true;
                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_AddList_to_Client));
                    th.Start(true);
                }
                else
                {
                    Payam.Show("متاسفانه امکان برقراری تماس با سرور وجود ندارد");
                }

            }
            else
            {
                Classes.clsBargeh.Send_AddList_to_Client(false);
            }
        }

    
        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Classes.clsBargeh.DeletedList_started)
            {
                Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                return;
            }
            if (ch_online.Checked)
            {
                if (Classes.clsBargeh.Check_connection())
                {
                    Payam.Show("لطفا تا اتمام عملیات بروز رسانی کمی صبر نمایید");
                    Classes.clsBargeh.DeletedList_started = true;
                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_DeleteList_to_Client));
                    th.Start(true);
                }
                else
                {
                    Payam.Show("متاسفانه امکان برقراری تماس با سرور وجود ندارد");
                }

            }
            else
            {
                Classes.clsBargeh.Send_DeleteList_to_Client(false);
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (Classes.clsBargeh.moshtarekin_started)
            {
                Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                return;
            }
            if (ch_online.Checked)
            {
                if (Classes.clsBargeh.Check_connection())
                {
                    Payam.Show("لطفا تا اتمام عملیات بروز رسانی کمی صبر نمایید");
                    Classes.clsBargeh.moshtarekin_started = true;
                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_Moshtarekin_to_Client));
                    th.Start(true);
                }
                else
                {
                    Payam.Show("متاسفانه امکان برقراری تماس با سیستم نگهبانی  وجود ندارد");
                }

            }
            else
            {
                Classes.clsBargeh.Send_Moshtarekin_to_Client(false);
            }
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (Classes.clsBargeh.BlockList_started)
            {
                Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                return;
            }
            if (ch_online.Checked)
            {
                if (Classes.clsBargeh.Check_connection())
                {
                    Payam.Show("لطفا تا اتمام عملیات بروز رسانی کمی صبر نمایید");
                    Classes.clsBargeh.BlockList_started = true;

                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_BlockList_to_Client));
                    th.Start(true);
                }
                else
                {
                    Payam.Show("متاسفانه امکان برقراری تماس با سرور وجود ندارد");
                }

            }
            else
            {
                Classes.clsBargeh.Send_BlockList_to_Client(false);
            }
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            if (Classes.clsBargeh.AllChangedFromServer_started)
            {
                if (Classes.clsBargeh.AddList_started == false
                    && Classes.clsBargeh.BlockList_started == false
                    && Classes.clsBargeh.DeletedList_started == false
                    )
                    Classes.clsBargeh.AllChangedFromServer_started = false;
                else
                {
                    Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                    return;
                }
            }
            
            
            Classes.clsBargeh.AllChangedFromServer_started = true;

            if (ch_online.Checked)
            {
                if (Classes.clsBargeh.Check_connection())
                {
                    Classes.clsBargeh.moshtarekin_started = true;
                    System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_Moshtarekin_to_Client));
                    th.Start(true);

                    Classes.clsBargeh.DeletedList_started = true;
                    System.Threading.Thread th_delete = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_DeleteList_to_Client));
                    th_delete.Start(true);


                    Classes.clsBargeh.AddList_started = true;
                    System.Threading.Thread th_add = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_AddList_to_Client));
                    th_add.Start(true);


                    Classes.clsBargeh.BlockList_started = true;
                    System.Threading.Thread th_2 = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Send_BlockList_to_Client));
                    th_2.Start(true);
                  

                }
                else
                {
                    Payam.Show("متاسفانه امکان برقراری تماس با سرور وجود ندارد");
                }

            }
            else
            {
                Classes.clsBargeh.Send_BlockList_to_Client(false);
            }


        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            if (Classes.clsBargeh.AllChangedFromClient_started)
            {
                Payam.Show("سیستم در حال انجام عملیات انتقال می باشد . لطفا چند لحظه صبر کنید");
                return;
            }
            if (Classes.clsBargeh.Check_connection())
            {
                Payam.Show("لطفا تا اتمام عملیات بروز رسانی کمی صبر نمایید");
                Classes.clsBargeh.AllChangedFromClient_started = true;
                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsBargeh.Get_Used_bargeh_from_Negahbani));
                th.Start(true);
            }
            else
            {
                Payam.Show("متاسفانه امکان برقراری تماس با سیستم نگهبانی  وجود ندارد");
            }
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.frm_all_client_darkhast().ShowDialog();

        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.frm_bargeh_transfer_report().ShowDialog();
        }
    }
}
