﻿namespace CWMS.Bargeh_khrorooj
{
    partial class frm_bargeh_transfer_report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgv_not_printed = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.bargehtransferBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barge_ds = new CWMS.Bargeh_khrorooj.barge_ds();
            this.bargeh_transferTableAdapter = new CWMS.Bargeh_khrorooj.barge_dsTableAdapters.bargeh_transferTableAdapter();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.startdateDataGridViewTextBoxColumn = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.enddateDataGridViewTextBoxColumn = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.statusDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.actionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpUpdateGhobooozAb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_not_printed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargehtransferBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barge_ds)).BeginInit();
            this.SuspendLayout();
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.White;
            this.grpUpdateGhobooozAb.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.dgv_not_printed);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(13, 33);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(865, 441);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 23;
            this.grpUpdateGhobooozAb.Text = "لیست درخواست ها";
            // 
            // dgv_not_printed
            // 
            this.dgv_not_printed.AllowUserToAddRows = false;
            this.dgv_not_printed.AllowUserToDeleteRows = false;
            this.dgv_not_printed.AllowUserToResizeColumns = false;
            this.dgv_not_printed.AllowUserToResizeRows = false;
            this.dgv_not_printed.AutoGenerateColumns = false;
            this.dgv_not_printed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_not_printed.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_not_printed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_not_printed.ColumnHeadersHeight = 30;
            this.dgv_not_printed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tarikhDataGridViewTextBoxColumn,
            this.startdateDataGridViewTextBoxColumn,
            this.enddateDataGridViewTextBoxColumn,
            this.statusDataGridViewCheckBoxColumn,
            this.actionDataGridViewTextBoxColumn});
            this.dgv_not_printed.DataSource = this.bargehtransferBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_not_printed.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_not_printed.EnableHeadersVisualStyles = false;
            this.dgv_not_printed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_not_printed.Location = new System.Drawing.Point(8, 19);
            this.dgv_not_printed.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_not_printed.Name = "dgv_not_printed";
            this.dgv_not_printed.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_not_printed.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_not_printed.RowHeadersVisible = false;
            this.dgv_not_printed.RowTemplate.Height = 40;
            this.dgv_not_printed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_not_printed.Size = new System.Drawing.Size(837, 384);
            this.dgv_not_printed.TabIndex = 68;
            // 
            // bargehtransferBindingSource
            // 
            this.bargehtransferBindingSource.DataMember = "bargeh_transfer";
            this.bargehtransferBindingSource.DataSource = this.barge_ds;
            // 
            // barge_ds
            // 
            this.barge_ds.DataSetName = "barge_ds";
            this.barge_ds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bargeh_transferTableAdapter
            // 
            this.bargeh_transferTableAdapter.ClearBeforeFill = true;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // startdateDataGridViewTextBoxColumn
            // 
            // 
            // 
            // 
            this.startdateDataGridViewTextBoxColumn.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.startdateDataGridViewTextBoxColumn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.startdateDataGridViewTextBoxColumn.DataPropertyName = "start_date";
            this.startdateDataGridViewTextBoxColumn.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime;
            this.startdateDataGridViewTextBoxColumn.HeaderText = "تاریخ شروع";
            this.startdateDataGridViewTextBoxColumn.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            // 
            // 
            // 
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.DisplayMonth = new System.DateTime(2022, 5, 1, 0, 0, 0, 0);
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Saturday;
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.Visible = false;
            this.startdateDataGridViewTextBoxColumn.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.startdateDataGridViewTextBoxColumn.Name = "startdateDataGridViewTextBoxColumn";
            this.startdateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startdateDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // enddateDataGridViewTextBoxColumn
            // 
            // 
            // 
            // 
            this.enddateDataGridViewTextBoxColumn.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.enddateDataGridViewTextBoxColumn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.enddateDataGridViewTextBoxColumn.DataPropertyName = "end_date";
            this.enddateDataGridViewTextBoxColumn.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime;
            this.enddateDataGridViewTextBoxColumn.HeaderText = "تاریخ پایان ";
            this.enddateDataGridViewTextBoxColumn.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            // 
            // 
            // 
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.DisplayMonth = new System.DateTime(2022, 5, 1, 0, 0, 0, 0);
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Saturday;
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.Visible = false;
            this.enddateDataGridViewTextBoxColumn.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.enddateDataGridViewTextBoxColumn.Name = "enddateDataGridViewTextBoxColumn";
            this.enddateDataGridViewTextBoxColumn.ReadOnly = true;
            this.enddateDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // statusDataGridViewCheckBoxColumn
            // 
            this.statusDataGridViewCheckBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewCheckBoxColumn.HeaderText = "اتمام موفقیت آمیز";
            this.statusDataGridViewCheckBoxColumn.Name = "statusDataGridViewCheckBoxColumn";
            this.statusDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // actionDataGridViewTextBoxColumn
            // 
            this.actionDataGridViewTextBoxColumn.DataPropertyName = "action";
            this.actionDataGridViewTextBoxColumn.HeaderText = "عملیات";
            this.actionDataGridViewTextBoxColumn.Name = "actionDataGridViewTextBoxColumn";
            this.actionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // frm_bargeh_transfer_report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 474);
            this.Controls.Add(this.grpUpdateGhobooozAb);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_bargeh_transfer_report";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "گزارشات";
            this.Load += new System.EventHandler(this.frm_bargeh_transfer_report_Load);
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_not_printed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargehtransferBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barge_ds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_not_printed;
        private barge_ds barge_ds;
        private System.Windows.Forms.BindingSource bargehtransferBindingSource;
        private barge_dsTableAdapters.bargeh_transferTableAdapter bargeh_transferTableAdapter;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn startdateDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn enddateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn statusDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actionDataGridViewTextBoxColumn;
    }
}