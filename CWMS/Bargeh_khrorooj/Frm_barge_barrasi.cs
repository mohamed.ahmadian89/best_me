﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Frm_barge_barrasi : MyMetroForm
    {
        public Frm_barge_barrasi(string gharardad, string darkhastCode, string tozihat, string doreh, string Tedad, string start, string end)
        {
            InitializeComponent();
            txtTozihat.Text = tozihat;
            lblcode.Text = darkhastCode;
            lblMoshtarekName.Text = Classes.clsMoshtarekin.Exist_gharardad(gharardad);
            lblgharardad.Text = gharardad;
            lblTedad.Text = Tedad;
            lblstart.Text = start;
            lblend.Text = end;
        }

        private void FrmBargeBarrasi_Load(object sender, EventArgs e)
        {
            string elhaghiList = lblgharardad.Text + "," + Classes.ClsMain
                .GetDataTable("select elhaghi from moshtarekin where gharardad = " + lblgharardad.Text)
                .AsEnumerable().First()["elhaghi"].ToString();
            if (elhaghiList.EndsWith(","))
            {
                elhaghiList = elhaghiList.Substring(0, elhaghiList.Length - 1);
            }
            if (elhaghiList.Trim().Length > 0)
                foreach (string elhaghi in elhaghiList.Split(','))
                {
                    long bedehiAb, bedehiSharj;
                    string co_name;
                    try
                    {
                        bedehiAb = Convert.ToInt64(Classes.ClsMain
             .GetDataTable("select top(1) mande from ab_ghabz where gharardad = "
             + elhaghi + " order by dcode desc")
             .AsEnumerable().First()["mande"]);
                    }
                    catch (Exception)
                    {

                        bedehiAb = 0;
                    }

                    try
                    {
                        bedehiSharj = Convert.ToInt64(Classes.ClsMain
                   .GetDataTable("select top(1) mande from sharj_ghabz where gharardad = "
                   + elhaghi + " order by dcode desc")
                   .AsEnumerable().First()["mande"]);
                    }
                    catch (Exception)
                    {
                        bedehiSharj = 0;
                    }
               

                    co_name = Classes.ClsMain
                        .GetDataTable("select co_name from moshtarekin where gharardad = " + elhaghi)
                        .AsEnumerable().First()["co_name"].ToString();

                    dgvElhaghiBedehi.Rows.Add(elhaghi, co_name,  bedehiSharj,bedehiAb);
                }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Classes.clsBargeh.RadDarkhast(lblcode.Text, txtTozihat.Text, lblgharardad.Text);

        }

        private void btn_tayid_Click(object sender, EventArgs e)
        {
            bool AyaBarrasishode = Classes.clsBargeh.GetBareh_Status(lblcode.Text);
            if (AyaBarrasishode == false)
            {
                Classes.clsBargeh.TqayidDarkhast(lblcode.Text, txtTozihat.Text, lblgharardad.Text, lblTedad.Text);
                Payam.Show("این درخواست مورد تایید قرار گرفت .");

            }
            else
            {
                Payam.Show("این برگه قبلا تایید شده است و هم اکنون فقط توضیحات ان ویرایش خواهد شد");
                Classes.clsBargeh.UpdateBareh_Tozihat(txtTozihat.Text, lblcode.Text);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا مطمئن هستید می خواهید این درخواست را حذف کنید") == DialogResult.Yes)
            {

                Classes.clsBargeh.HazDarkhast(lblcode.Text, "درخواست  با کد " + lblgharardad.Text + " توسط مدیریت از سیستم حذف شد", lblgharardad.Text);
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.Frm_barge_chap(lblstart.Text, lblend.Text, lblcode.Text, lblgharardad.Text).ShowDialog();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Frm_barge_details frm = new Frm_barge_details(lblgharardad.Text, lblcode.Text);
            frm.ShowDialog();
        }
    }
}