﻿namespace CWMS
{
    partial class Frm_barge_details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }





        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bargekhoroojBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.bargekhoroojdetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest1 = new CWMS.MainDataSest();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvNotUsed = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewDateTimeInputColumn1 = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.TabNotUsed = new DevComponents.DotNetBar.SuperTabItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.barge_khoroojTableAdapter = new CWMS.MainDataSestTableAdapters.barge_khoroojTableAdapter();
            this.barge_khorooj_detailsTableAdapter = new CWMS.MainDataSestTableAdapters.barge_khorooj_detailsTableAdapter();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnManageAll = new DevComponents.DotNetBar.ButtonX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.grpAllBargehD = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_darkhast_id = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btnSetNotFaal = new DevComponents.DotNetBar.ButtonX();
            this.btnSetfaal = new DevComponents.DotNetBar.ButtonX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.dgvBargeDeatils = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blockDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.usedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tarikhDataGridViewTextBoxColumn1 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.ساعت = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.tabAll = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvGheirFaal = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TabBlocked = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvUsed = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.TabUsed = new DevComponents.DotNetBar.SuperTabItem();
            this.grpDarkhast = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_show_darkhast_details = new DevComponents.DotNetBar.ButtonX();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.dgvDarkhjast = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.tedadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barrasiDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lblConame = new DevComponents.DotNetBar.LabelX();
            this.btn_find_company = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usedDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.useddateDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.used_date2 = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojdetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).BeginInit();
            this.superTabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotUsed)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.grpAllBargehD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBargeDeatils)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGheirFaal)).BeginInit();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsed)).BeginInit();
            this.grpDarkhast.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhjast)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bargekhoroojBindingSource
            // 
            this.bargekhoroojBindingSource.DataMember = "barge_khorooj";
            this.bargekhoroojBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bargekhoroojdetailsBindingSource
            // 
            this.bargekhoroojdetailsBindingSource.DataMember = "barge_khorooj_details";
            this.bargekhoroojdetailsBindingSource.DataSource = this.mainDataSest1;
            // 
            // mainDataSest1
            // 
            this.mainDataSest1.DataSetName = "MainDataSest";
            this.mainDataSest1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.dgvNotUsed);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(455, 324);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.TabNotUsed;
            // 
            // dgvNotUsed
            // 
            this.dgvNotUsed.AllowUserToAddRows = false;
            this.dgvNotUsed.AllowUserToDeleteRows = false;
            this.dgvNotUsed.AllowUserToResizeColumns = false;
            this.dgvNotUsed.AllowUserToResizeRows = false;
            this.dgvNotUsed.AutoGenerateColumns = false;
            this.dgvNotUsed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNotUsed.BackgroundColor = System.Drawing.Color.White;
            this.dgvNotUsed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNotUsed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNotUsed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewDateTimeInputColumn1});
            this.dgvNotUsed.DataSource = this.bargekhoroojdetailsBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNotUsed.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNotUsed.EnableHeadersVisualStyles = false;
            this.dgvNotUsed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvNotUsed.Location = new System.Drawing.Point(16, 13);
            this.dgvNotUsed.Name = "dgvNotUsed";
            this.dgvNotUsed.ReadOnly = true;
            this.dgvNotUsed.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNotUsed.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvNotUsed.RowHeadersVisible = false;
            this.dgvNotUsed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNotUsed.Size = new System.Drawing.Size(422, 279);
            this.dgvNotUsed.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "کد برگه";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "used";
            this.dataGridViewCheckBoxColumn1.HeaderText = "استفاده شده";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "used_date";
            this.dataGridViewTextBoxColumn3.HeaderText = "تاریخ استفاده";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewDateTimeInputColumn1
            // 
            // 
            // 
            // 
            this.dataGridViewDateTimeInputColumn1.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.dataGridViewDateTimeInputColumn1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDateTimeInputColumn1.DataPropertyName = "used_date";
            this.dataGridViewDateTimeInputColumn1.Format = DevComponents.Editors.eDateTimePickerFormat.LongTime;
            this.dataGridViewDateTimeInputColumn1.HeaderText = "ساعت";
            this.dataGridViewDateTimeInputColumn1.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            // 
            // 
            // 
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.DisplayMonth = new System.DateTime(2022, 5, 1, 0, 0, 0, 0);
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Saturday;
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.Visible = false;
            this.dataGridViewDateTimeInputColumn1.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dataGridViewDateTimeInputColumn1.Name = "dataGridViewDateTimeInputColumn1";
            this.dataGridViewDateTimeInputColumn1.ReadOnly = true;
            this.dataGridViewDateTimeInputColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // TabNotUsed
            // 
            this.TabNotUsed.AttachedControl = this.superTabControlPanel3;
            this.TabNotUsed.GlobalItem = false;
            this.TabNotUsed.Name = "TabNotUsed";
            this.TabNotUsed.Text = "برگه های استفاده نشده";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // barge_khoroojTableAdapter
            // 
            this.barge_khoroojTableAdapter.ClearBeforeFill = true;
            // 
            // barge_khorooj_detailsTableAdapter
            // 
            this.barge_khorooj_detailsTableAdapter.ClearBeforeFill = true;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btnManageAll);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(14, 526);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(933, 66);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 3;
            this.groupPanel2.Text = "انتخاب مشترک";
            // 
            // btnManageAll
            // 
            this.btnManageAll.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnManageAll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnManageAll.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnManageAll.Location = new System.Drawing.Point(215, 3);
            this.btnManageAll.Name = "btnManageAll";
            this.btnManageAll.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnManageAll.Size = new System.Drawing.Size(237, 30);
            this.btnManageAll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnManageAll.Symbol = "";
            this.btnManageAll.SymbolColor = System.Drawing.Color.Green;
            this.btnManageAll.SymbolSize = 9F;
            this.btnManageAll.TabIndex = 14;
            this.btnManageAll.Text = "مدیریت تعداد بالای برگه های درخواست ها";
            this.btnManageAll.Click += new System.EventHandler(this.btnManageAll_Click);
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(225, 0);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(688, 33);
            this.labelX5.TabIndex = 13;
            this.labelX5.Text = "فعال یاغیرفعال کردن برگه های خروج به صورت کلی  بواسطه سامانه مدیریت انبوه برگه ها" +
    "";
            // 
            // grpAllBargehD
            // 
            this.grpAllBargehD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpAllBargehD.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpAllBargehD.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpAllBargehD.Controls.Add(this.lbl_darkhast_id);
            this.grpAllBargehD.Controls.Add(this.labelX1);
            this.grpAllBargehD.Controls.Add(this.btnSetNotFaal);
            this.grpAllBargehD.Controls.Add(this.btnSetfaal);
            this.grpAllBargehD.Controls.Add(this.superTabControl1);
            this.grpAllBargehD.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpAllBargehD.Location = new System.Drawing.Point(12, 88);
            this.grpAllBargehD.Name = "grpAllBargehD";
            this.grpAllBargehD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpAllBargehD.Size = new System.Drawing.Size(491, 432);
            // 
            // 
            // 
            this.grpAllBargehD.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpAllBargehD.Style.BackColorGradientAngle = 90;
            this.grpAllBargehD.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpAllBargehD.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpAllBargehD.Style.BorderBottomWidth = 1;
            this.grpAllBargehD.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpAllBargehD.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpAllBargehD.Style.BorderLeftWidth = 1;
            this.grpAllBargehD.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpAllBargehD.Style.BorderRightWidth = 1;
            this.grpAllBargehD.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpAllBargehD.Style.BorderTopWidth = 1;
            this.grpAllBargehD.Style.CornerDiameter = 4;
            this.grpAllBargehD.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpAllBargehD.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpAllBargehD.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpAllBargehD.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpAllBargehD.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpAllBargehD.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpAllBargehD.TabIndex = 2;
            this.grpAllBargehD.Text = "وضعیت برگه ها";
            // 
            // lbl_darkhast_id
            // 
            this.lbl_darkhast_id.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_darkhast_id.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_darkhast_id.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_darkhast_id.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_darkhast_id.ForeColor = System.Drawing.Color.Black;
            this.lbl_darkhast_id.Location = new System.Drawing.Point(300, 7);
            this.lbl_darkhast_id.Name = "lbl_darkhast_id";
            this.lbl_darkhast_id.Size = new System.Drawing.Size(80, 33);
            this.lbl_darkhast_id.TabIndex = 15;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(386, 7);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(80, 33);
            this.labelX1.TabIndex = 14;
            this.labelX1.Text = "کد درخواست :";
            // 
            // btnSetNotFaal
            // 
            this.btnSetNotFaal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSetNotFaal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSetNotFaal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSetNotFaal.Location = new System.Drawing.Point(14, 376);
            this.btnSetNotFaal.Name = "btnSetNotFaal";
            this.btnSetNotFaal.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnSetNotFaal.Size = new System.Drawing.Size(191, 23);
            this.btnSetNotFaal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSetNotFaal.Symbol = "";
            this.btnSetNotFaal.SymbolColor = System.Drawing.Color.Green;
            this.btnSetNotFaal.SymbolSize = 9F;
            this.btnSetNotFaal.TabIndex = 3;
            this.btnSetNotFaal.Text = "غیر فعال کردن برگه های انتخاب شده";
            this.btnSetNotFaal.Click += new System.EventHandler(this.btnSetNotFaal_Click);
            // 
            // btnSetfaal
            // 
            this.btnSetfaal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSetfaal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSetfaal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSetfaal.Location = new System.Drawing.Point(300, 376);
            this.btnSetfaal.Name = "btnSetfaal";
            this.btnSetfaal.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnSetfaal.Size = new System.Drawing.Size(169, 23);
            this.btnSetfaal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSetfaal.Symbol = "";
            this.btnSetfaal.SymbolColor = System.Drawing.Color.Green;
            this.btnSetfaal.SymbolSize = 9F;
            this.btnSetfaal.TabIndex = 2;
            this.btnSetfaal.Text = "فعال کردن برگه های انتخاب شده";
            this.btnSetfaal.Click += new System.EventHandler(this.btnSetfaal_Click);
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(14, 46);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(455, 324);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 0;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.tabAll,
            this.TabUsed,
            this.TabNotUsed,
            this.TabBlocked});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.buttonX3);
            this.superTabControlPanel1.Controls.Add(this.buttonX2);
            this.superTabControlPanel1.Controls.Add(this.dgvBargeDeatils);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(455, 295);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.tabAll;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(45, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(23, 23);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Maroon;
            this.buttonX3.SymbolSize = 14F;
            this.buttonX3.TabIndex = 23;
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(16, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(23, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 14F;
            this.buttonX2.TabIndex = 22;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // dgvBargeDeatils
            // 
            this.dgvBargeDeatils.AllowUserToAddRows = false;
            this.dgvBargeDeatils.AllowUserToDeleteRows = false;
            this.dgvBargeDeatils.AllowUserToResizeColumns = false;
            this.dgvBargeDeatils.AllowUserToResizeRows = false;
            this.dgvBargeDeatils.AutoGenerateColumns = false;
            this.dgvBargeDeatils.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBargeDeatils.BackgroundColor = System.Drawing.Color.White;
            this.dgvBargeDeatils.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBargeDeatils.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvBargeDeatils.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.blockDataGridViewCheckBoxColumn,
            this.usedDataGridViewCheckBoxColumn,
            this.tarikhDataGridViewTextBoxColumn1,
            this.ساعت});
            this.dgvBargeDeatils.DataSource = this.bargekhoroojdetailsBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBargeDeatils.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvBargeDeatils.EnableHeadersVisualStyles = false;
            this.dgvBargeDeatils.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvBargeDeatils.Location = new System.Drawing.Point(16, 17);
            this.dgvBargeDeatils.Name = "dgvBargeDeatils";
            this.dgvBargeDeatils.ReadOnly = true;
            this.dgvBargeDeatils.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBargeDeatils.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvBargeDeatils.RowHeadersVisible = false;
            this.dgvBargeDeatils.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBargeDeatils.Size = new System.Drawing.Size(422, 260);
            this.dgvBargeDeatils.TabIndex = 2;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "کدبرگه";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // blockDataGridViewCheckBoxColumn
            // 
            this.blockDataGridViewCheckBoxColumn.DataPropertyName = "block";
            this.blockDataGridViewCheckBoxColumn.FillWeight = 50F;
            this.blockDataGridViewCheckBoxColumn.HeaderText = "غیرفعال";
            this.blockDataGridViewCheckBoxColumn.Name = "blockDataGridViewCheckBoxColumn";
            this.blockDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // usedDataGridViewCheckBoxColumn
            // 
            this.usedDataGridViewCheckBoxColumn.DataPropertyName = "used";
            this.usedDataGridViewCheckBoxColumn.FillWeight = 80F;
            this.usedDataGridViewCheckBoxColumn.HeaderText = "استفاده شده";
            this.usedDataGridViewCheckBoxColumn.Name = "usedDataGridViewCheckBoxColumn";
            this.usedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // tarikhDataGridViewTextBoxColumn1
            // 
            this.tarikhDataGridViewTextBoxColumn1.DataPropertyName = "used_date";
            this.tarikhDataGridViewTextBoxColumn1.HeaderText = "تاریخ استفاده";
            this.tarikhDataGridViewTextBoxColumn1.Name = "tarikhDataGridViewTextBoxColumn1";
            this.tarikhDataGridViewTextBoxColumn1.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ساعت
            // 
            // 
            // 
            // 
            this.ساعت.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.ساعت.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ساعت.DataPropertyName = "used_date";
            this.ساعت.Format = DevComponents.Editors.eDateTimePickerFormat.ShortTime;
            this.ساعت.HeaderText = "ساعت";
            this.ساعت.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.ساعت.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.ساعت.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ساعت.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            // 
            // 
            // 
            this.ساعت.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ساعت.MonthCalendar.DisplayMonth = new System.DateTime(2016, 5, 1, 0, 0, 0, 0);
            this.ساعت.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Saturday;
            this.ساعت.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.ساعت.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.ساعت.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ساعت.MonthCalendar.Visible = false;
            this.ساعت.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.ساعت.Name = "ساعت";
            this.ساعت.ReadOnly = true;
            this.ساعت.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabAll
            // 
            this.tabAll.AttachedControl = this.superTabControlPanel1;
            this.tabAll.GlobalItem = false;
            this.tabAll.Name = "tabAll";
            this.tabAll.Text = "کلیه برگه ها";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.dgvGheirFaal);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(455, 324);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.TabBlocked;
            // 
            // dgvGheirFaal
            // 
            this.dgvGheirFaal.AllowUserToAddRows = false;
            this.dgvGheirFaal.AllowUserToDeleteRows = false;
            this.dgvGheirFaal.AllowUserToResizeColumns = false;
            this.dgvGheirFaal.AllowUserToResizeRows = false;
            this.dgvGheirFaal.AutoGenerateColumns = false;
            this.dgvGheirFaal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGheirFaal.BackgroundColor = System.Drawing.Color.White;
            this.dgvGheirFaal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGheirFaal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvGheirFaal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewCheckBoxColumn6});
            this.dgvGheirFaal.DataSource = this.bargekhoroojdetailsBindingSource;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGheirFaal.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvGheirFaal.EnableHeadersVisualStyles = false;
            this.dgvGheirFaal.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvGheirFaal.Location = new System.Drawing.Point(16, 17);
            this.dgvGheirFaal.Name = "dgvGheirFaal";
            this.dgvGheirFaal.ReadOnly = true;
            this.dgvGheirFaal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGheirFaal.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvGheirFaal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGheirFaal.Size = new System.Drawing.Size(422, 260);
            this.dgvGheirFaal.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn5.HeaderText = "کدبرگه";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.DataPropertyName = "block";
            this.dataGridViewCheckBoxColumn5.FillWeight = 50F;
            this.dataGridViewCheckBoxColumn5.HeaderText = "غیرفعال";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.DataPropertyName = "used";
            this.dataGridViewCheckBoxColumn6.FillWeight = 80F;
            this.dataGridViewCheckBoxColumn6.HeaderText = "استفاده شده";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.ReadOnly = true;
            // 
            // TabBlocked
            // 
            this.TabBlocked.AttachedControl = this.superTabControlPanel4;
            this.TabBlocked.GlobalItem = false;
            this.TabBlocked.Name = "TabBlocked";
            this.TabBlocked.Text = "برگه های غیرفعال";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.dgvUsed);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(455, 295);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.TabUsed;
            // 
            // dgvUsed
            // 
            this.dgvUsed.AllowUserToAddRows = false;
            this.dgvUsed.AllowUserToDeleteRows = false;
            this.dgvUsed.AllowUserToResizeColumns = false;
            this.dgvUsed.AllowUserToResizeRows = false;
            this.dgvUsed.AutoGenerateColumns = false;
            this.dgvUsed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUsed.BackgroundColor = System.Drawing.Color.White;
            this.dgvUsed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUsed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvUsed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.usedDataGridViewCheckBoxColumn1,
            this.useddateDataGridViewTextBoxColumn,
            this.used_date2});
            this.dgvUsed.DataSource = this.bargekhoroojdetailsBindingSource;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvUsed.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvUsed.EnableHeadersVisualStyles = false;
            this.dgvUsed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvUsed.Location = new System.Drawing.Point(16, 17);
            this.dgvUsed.Name = "dgvUsed";
            this.dgvUsed.ReadOnly = true;
            this.dgvUsed.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUsed.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvUsed.RowHeadersVisible = false;
            this.dgvUsed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsed.Size = new System.Drawing.Size(422, 275);
            this.dgvUsed.TabIndex = 3;
            // 
            // TabUsed
            // 
            this.TabUsed.AttachedControl = this.superTabControlPanel2;
            this.TabUsed.GlobalItem = false;
            this.TabUsed.Name = "TabUsed";
            this.TabUsed.Text = "برگه های استفاده شده";
            // 
            // grpDarkhast
            // 
            this.grpDarkhast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpDarkhast.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpDarkhast.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpDarkhast.Controls.Add(this.btn_show_darkhast_details);
            this.grpDarkhast.Controls.Add(this.btnPrint);
            this.grpDarkhast.Controls.Add(this.dgvDarkhjast);
            this.grpDarkhast.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpDarkhast.Location = new System.Drawing.Point(509, 88);
            this.grpDarkhast.Name = "grpDarkhast";
            this.grpDarkhast.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpDarkhast.Size = new System.Drawing.Size(442, 374);
            // 
            // 
            // 
            this.grpDarkhast.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpDarkhast.Style.BackColorGradientAngle = 90;
            this.grpDarkhast.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpDarkhast.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDarkhast.Style.BorderBottomWidth = 1;
            this.grpDarkhast.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpDarkhast.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDarkhast.Style.BorderLeftWidth = 1;
            this.grpDarkhast.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDarkhast.Style.BorderRightWidth = 1;
            this.grpDarkhast.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDarkhast.Style.BorderTopWidth = 1;
            this.grpDarkhast.Style.CornerDiameter = 4;
            this.grpDarkhast.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpDarkhast.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpDarkhast.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpDarkhast.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpDarkhast.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpDarkhast.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpDarkhast.TabIndex = 1;
            this.grpDarkhast.Text = "لیست درخواست ها";
            // 
            // btn_show_darkhast_details
            // 
            this.btn_show_darkhast_details.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show_darkhast_details.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_show_darkhast_details.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show_darkhast_details.Location = new System.Drawing.Point(9, 314);
            this.btn_show_darkhast_details.Name = "btn_show_darkhast_details";
            this.btn_show_darkhast_details.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_show_darkhast_details.Size = new System.Drawing.Size(196, 23);
            this.btn_show_darkhast_details.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show_darkhast_details.Symbol = "";
            this.btn_show_darkhast_details.SymbolColor = System.Drawing.Color.Green;
            this.btn_show_darkhast_details.SymbolSize = 9F;
            this.btn_show_darkhast_details.TabIndex = 21;
            this.btn_show_darkhast_details.Text = "نمایش برگه های درخواست انتخاب شده";
            this.btn_show_darkhast_details.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Location = new System.Drawing.Point(9, 7);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnPrint.Size = new System.Drawing.Size(125, 23);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.Symbol = "";
            this.btnPrint.SymbolColor = System.Drawing.Color.Green;
            this.btnPrint.SymbolSize = 9F;
            this.btnPrint.TabIndex = 20;
            this.btnPrint.Text = "چاپ برگه های خروج";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // dgvDarkhjast
            // 
            this.dgvDarkhjast.AllowUserToAddRows = false;
            this.dgvDarkhjast.AllowUserToDeleteRows = false;
            this.dgvDarkhjast.AllowUserToResizeColumns = false;
            this.dgvDarkhjast.AllowUserToResizeRows = false;
            this.dgvDarkhjast.AutoGenerateColumns = false;
            this.dgvDarkhjast.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDarkhjast.BackgroundColor = System.Drawing.Color.White;
            this.dgvDarkhjast.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhjast.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvDarkhjast.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.tarikhDataGridViewTextBoxColumn,
            this.tedadDataGridViewTextBoxColumn,
            this.barrasiDataGridViewCheckBoxColumn});
            this.dgvDarkhjast.DataSource = this.bargekhoroojBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDarkhjast.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvDarkhjast.EnableHeadersVisualStyles = false;
            this.dgvDarkhjast.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvDarkhjast.Location = new System.Drawing.Point(9, 36);
            this.dgvDarkhjast.MultiSelect = false;
            this.dgvDarkhjast.Name = "dgvDarkhjast";
            this.dgvDarkhjast.ReadOnly = true;
            this.dgvDarkhjast.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhjast.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvDarkhjast.RowHeadersVisible = false;
            this.dgvDarkhjast.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDarkhjast.Size = new System.Drawing.Size(418, 272);
            this.dgvDarkhjast.TabIndex = 1;
            this.dgvDarkhjast.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDarkhjast_CellClick);
            this.dgvDarkhjast.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDarkhjast_CellContentClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.FillWeight = 60F;
            this.id.HeaderText = "کد";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.FillWeight = 120F;
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tedadDataGridViewTextBoxColumn
            // 
            this.tedadDataGridViewTextBoxColumn.DataPropertyName = "tedad";
            this.tedadDataGridViewTextBoxColumn.FillWeight = 80F;
            this.tedadDataGridViewTextBoxColumn.HeaderText = "تعداد";
            this.tedadDataGridViewTextBoxColumn.Name = "tedadDataGridViewTextBoxColumn";
            this.tedadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // barrasiDataGridViewCheckBoxColumn
            // 
            this.barrasiDataGridViewCheckBoxColumn.DataPropertyName = "barrasi";
            this.barrasiDataGridViewCheckBoxColumn.FillWeight = 50F;
            this.barrasiDataGridViewCheckBoxColumn.HeaderText = "تایید";
            this.barrasiDataGridViewCheckBoxColumn.Name = "barrasiDataGridViewCheckBoxColumn";
            this.barrasiDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.lblConame);
            this.groupPanel1.Controls.Add(this.btn_find_company);
            this.groupPanel1.Controls.Add(this.txt_find_moshtarek);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(14, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(931, 66);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "انتخاب مشترک";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(569, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(60, 33);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "نام مشترک:";
            // 
            // lblConame
            // 
            this.lblConame.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblConame.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblConame.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblConame.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblConame.ForeColor = System.Drawing.Color.Black;
            this.lblConame.Location = new System.Drawing.Point(125, 5);
            this.lblConame.Name = "lblConame";
            this.lblConame.Size = new System.Drawing.Size(438, 33);
            this.lblConame.TabIndex = 10;
            // 
            // btn_find_company
            // 
            this.btn_find_company.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_company.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_find_company.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_company.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_company.Location = new System.Drawing.Point(644, 8);
            this.btn_find_company.Name = "btn_find_company";
            this.btn_find_company.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_company.Size = new System.Drawing.Size(103, 23);
            this.btn_find_company.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_company.Symbol = "";
            this.btn_find_company.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_company.SymbolSize = 9F;
            this.btn_find_company.TabIndex = 9;
            this.btn_find_company.Text = "جستجو";
            this.btn_find_company.Click += new System.EventHandler(this.btn_find_company_Click);
            // 
            // txt_find_moshtarek
            // 
            this.txt_find_moshtarek.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_find_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_find_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_find_moshtarek.Location = new System.Drawing.Point(753, 7);
            this.txt_find_moshtarek.Name = "txt_find_moshtarek";
            this.txt_find_moshtarek.PreventEnterBeep = true;
            this.txt_find_moshtarek.Size = new System.Drawing.Size(86, 24);
            this.txt_find_moshtarek.TabIndex = 7;
            this.txt_find_moshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_moshtarek_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(854, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(35, 33);
            this.labelX2.TabIndex = 8;
            this.labelX2.Text = "قرارداد:";
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "کد برگه";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // usedDataGridViewCheckBoxColumn1
            // 
            this.usedDataGridViewCheckBoxColumn1.DataPropertyName = "used";
            this.usedDataGridViewCheckBoxColumn1.HeaderText = "استفاده شده";
            this.usedDataGridViewCheckBoxColumn1.Name = "usedDataGridViewCheckBoxColumn1";
            this.usedDataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // useddateDataGridViewTextBoxColumn
            // 
            this.useddateDataGridViewTextBoxColumn.DataPropertyName = "used_date";
            this.useddateDataGridViewTextBoxColumn.HeaderText = "تاریخ استفاده";
            this.useddateDataGridViewTextBoxColumn.Name = "useddateDataGridViewTextBoxColumn";
            this.useddateDataGridViewTextBoxColumn.ReadOnly = true;
            this.useddateDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.useddateDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // used_date2
            // 
            // 
            // 
            // 
            this.used_date2.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.used_date2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.used_date2.DataPropertyName = "used_date";
            this.used_date2.Format = DevComponents.Editors.eDateTimePickerFormat.ShortTime;
            this.used_date2.HeaderText = "ساعت";
            this.used_date2.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.used_date2.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.used_date2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.used_date2.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            // 
            // 
            // 
            this.used_date2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.used_date2.MonthCalendar.DisplayMonth = new System.DateTime(2022, 5, 1, 0, 0, 0, 0);
            this.used_date2.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Saturday;
            this.used_date2.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.used_date2.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.used_date2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.used_date2.MonthCalendar.Visible = false;
            this.used_date2.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.used_date2.Name = "used_date2";
            this.used_date2.ReadOnly = true;
            this.used_date2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Frm_barge_details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 592);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.grpAllBargehD);
            this.Controls.Add(this.grpDarkhast);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_barge_details";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmBargehDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojdetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).EndInit();
            this.superTabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotUsed)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.grpAllBargehD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBargeDeatils)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGheirFaal)).EndInit();
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsed)).EndInit();
            this.grpDarkhast.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhjast)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpDarkhast;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDarkhjast;
        private DevComponents.DotNetBar.Controls.GroupPanel grpAllBargehD;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvBargeDeatils;
        private DevComponents.DotNetBar.SuperTabItem tabAll;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.SuperTabItem TabBlocked;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem TabNotUsed;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem TabUsed;
        private DevComponents.DotNetBar.ButtonX btn_find_company;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblConame;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource bargekhoroojBindingSource;
        private MainDataSestTableAdapters.barge_khoroojTableAdapter barge_khoroojTableAdapter;
        private MainDataSest mainDataSest1;
        private System.Windows.Forms.BindingSource bargekhoroojdetailsBindingSource;
        private MainDataSestTableAdapters.barge_khorooj_detailsTableAdapter barge_khorooj_detailsTableAdapter;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvGheirFaal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevComponents.DotNetBar.ButtonX btnSetNotFaal;
        private DevComponents.DotNetBar.ButtonX btnSetfaal;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btnManageAll;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.DotNetBar.ButtonX btn_show_darkhast_details;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tedadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn barrasiDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn blockDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn usedDataGridViewCheckBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn1;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn ساعت;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvNotUsed;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn dataGridViewDateTimeInputColumn1;
        private DevComponents.DotNetBar.LabelX lbl_darkhast_id;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn usedDataGridViewCheckBoxColumn1;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn useddateDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn used_date2;
    }
}