using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Frm_barge_report_All : MyMetroForm
    {
        public Frm_barge_report_All()
        {
            InitializeComponent();
        }

        private void Rpt_bageh_all_doreh_stat_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.Bargeh_all_stat' table. You can move, or remove it, as needed.
            this.bargeh_all_statTableAdapter.Fill(this.mainDataSest.Bargeh_all_stat);
            int sum = 0;
            for (int i = 0; i < mainDataSest.Bargeh_all_stat.Rows.Count; i++)
            {
                sum += Convert.ToInt32(mainDataSest.Bargeh_all_stat.Rows[i]["majmoo"]);
            }
            lblsum.Text = sum.ToString();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {

        }
    }
}