﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
using System.Diagnostics;
namespace CWMS.Bargeh_khrorooj
{
    public partial class Frm_barge_for_gate_export : MyMetroForm
    {
        public Frm_barge_for_gate_export()
        {
            InitializeComponent();
        }

        private void FrmExportBargeh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.bargeh_export_report' table. You can move, or remove it, as needed.
            this.bargeh_export_reportTableAdapter.Fill(this.mainDataSest.bargeh_export_report);

        }




        private void btn_tayid_Click(object sender, EventArgs e)
        {
            Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 85);
            
            Payam.Show("با تایید شما عملیات تولید پوشه حاوی برگه ها اغاز خواهد شد . لطفا پس از تایید چند لحظه صبر نمایید");
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            barge_khorooj_detailsTableAdapter1.FillForExport(ds.barge_khorooj_details);

            if (ds.barge_khorooj_details.Rows.Count != 0)
            {

                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)  +"\\Bargeh";
                if (System.IO.Directory.Exists(path) == true)
                {
                    System.IO.Directory.Delete(path,true);
                }
                System.IO.Directory.CreateDirectory(path);
                ds.barge_khorooj_details.WriteXml(path + "\\bargeh.xml");
                bargeh_export_reportTableAdapter.Insert(DateTime.Now,"گرفتن برگه خروج جهت انتقال به گیت خروجی", 0);
                this.bargeh_export_reportTableAdapter.Fill(this.mainDataSest.bargeh_export_report);
                System.IO.File.WriteAllText(path + "\\PLYbargeh.xml",Classes.ClsMain.ExecuteScalar("select top(1) DeleteTheseID from Bargeh_info").ToString());
                Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj_details set InExport=0 where darkhast NOT IN (SELECT id FROM barge_khorooj WHERE(barrasi = 0))");

                Payam.Show("لطفا پوشه ایجاد شده بر روی دسکتاپ را به کامپیوتر گیت خروجی انتقال دهید. Bargeh");

            }
            else
                Payam.Show("هیچ برگه خروج جدیدی در سیستم ثبت نشده است ");

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            if (System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\bargeh.xml") == false)
            {
                try
                {
                    ds.barge_khorooj_details.ReadXml(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\bargeh.xml");
                    barge_khorooj_detailsTableAdapter1.Update(ds.barge_khorooj_details);
                }
                catch (Exception ert)
                {
                    Payam.Show("متاسفانه میان اطلاعات سرور اصلی و گیت خروجی ناهماهنگی پیش امده است . لطفا هرچه سریعتر با پشتیبان تماس حاصل فرمایید");
                            Classes.ClsMain.logError(ert);
                }

            }
            else
                Payam.Show("لطفا فایل حاوی اطلاعات را بر روی دسکتاپ کپی نمایید");
        }
    }
}