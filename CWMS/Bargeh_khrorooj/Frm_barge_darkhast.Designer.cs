﻿namespace CWMS
{
    partial class Frm_barge_darkhast
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.btnDelete = new DevComponents.DotNetBar.ButtonX();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.lblMoshtgariSum = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.lblMoshtariCount = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.dgvDarkhast = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tedadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.tozihatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barrasiDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finish = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bargekhoroojBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.btn_new_darkhast = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_moshtarek_name = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_find_company = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.barge_khoroojTableAdapter = new CWMS.MainDataSestTableAdapters.barge_khoroojTableAdapter();
            this.btn_transfer = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX6);
            this.groupPanel1.Controls.Add(this.btnDelete);
            this.groupPanel1.Controls.Add(this.btnPrint);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.lblMoshtgariSum);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.lblMoshtariCount);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.dgvDarkhast);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(25, 102);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1125, 353);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "درخواست هاي صدور برگه خروج";
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Font = new System.Drawing.Font("B Yekan", 9F);
            this.buttonX6.Location = new System.Drawing.Point(5, 196);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX6.Size = new System.Drawing.Size(160, 78);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Green;
            this.buttonX6.SymbolSize = 9F;
            this.buttonX6.TabIndex = 22;
            this.buttonX6.Text = "مشاهده برگه ها";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDelete.Font = new System.Drawing.Font("B Yekan", 9F);
            this.btnDelete.Location = new System.Drawing.Point(3, 106);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnDelete.Size = new System.Drawing.Size(160, 70);
            this.btnDelete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDelete.Symbol = "";
            this.btnDelete.SymbolColor = System.Drawing.Color.Maroon;
            this.btnDelete.SymbolSize = 9F;
            this.btnDelete.TabIndex = 20;
            this.btnDelete.Text = "حذف برگه ها";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Font = new System.Drawing.Font("B Yekan", 9F);
            this.btnPrint.Location = new System.Drawing.Point(3, 16);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnPrint.Size = new System.Drawing.Size(160, 72);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.Symbol = "";
            this.btnPrint.SymbolColor = System.Drawing.Color.Green;
            this.btnPrint.SymbolSize = 9F;
            this.btnPrint.TabIndex = 19;
            this.btnPrint.Text = "چاپ برگه های خروج";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // labelX9
            // 
            this.labelX9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(638, 285);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(35, 33);
            this.labelX9.Symbol = "";
            this.labelX9.SymbolSize = 12F;
            this.labelX9.TabIndex = 17;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(1000, 284);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(35, 33);
            this.labelX8.Symbol = "";
            this.labelX8.SymbolSize = 12F;
            this.labelX8.TabIndex = 16;
            // 
            // lblMoshtgariSum
            // 
            this.lblMoshtgariSum.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMoshtgariSum.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMoshtgariSum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMoshtgariSum.Font = new System.Drawing.Font("B Yekan", 12F, System.Drawing.FontStyle.Underline);
            this.lblMoshtgariSum.ForeColor = System.Drawing.Color.Black;
            this.lblMoshtgariSum.Location = new System.Drawing.Point(367, 284);
            this.lblMoshtgariSum.Name = "lblMoshtgariSum";
            this.lblMoshtgariSum.Size = new System.Drawing.Size(81, 33);
            this.lblMoshtgariSum.TabIndex = 13;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(433, 285);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(199, 33);
            this.labelX5.TabIndex = 12;
            this.labelX5.Text = "تعدادبرگه های صادر شده مشترک :";
            // 
            // lblMoshtariCount
            // 
            this.lblMoshtariCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMoshtariCount.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMoshtariCount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMoshtariCount.Font = new System.Drawing.Font("B Yekan", 12F, System.Drawing.FontStyle.Underline);
            this.lblMoshtariCount.ForeColor = System.Drawing.Color.Black;
            this.lblMoshtariCount.Location = new System.Drawing.Point(753, 284);
            this.lblMoshtariCount.Name = "lblMoshtariCount";
            this.lblMoshtariCount.Size = new System.Drawing.Size(87, 33);
            this.lblMoshtariCount.TabIndex = 11;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(847, 285);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(147, 33);
            this.labelX7.TabIndex = 10;
            this.labelX7.Text = "تعداد درخواست های مشترک:";
            // 
            // dgvDarkhast
            // 
            this.dgvDarkhast.AllowUserToAddRows = false;
            this.dgvDarkhast.AllowUserToDeleteRows = false;
            this.dgvDarkhast.AllowUserToResizeColumns = false;
            this.dgvDarkhast.AllowUserToResizeRows = false;
            this.dgvDarkhast.AutoGenerateColumns = false;
            this.dgvDarkhast.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDarkhast.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhast.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDarkhast.ColumnHeadersHeight = 30;
            this.dgvDarkhast.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dcode,
            this.gharardadDataGridViewTextBoxColumn,
            this.tedadDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.tozihatDataGridViewTextBoxColumn,
            this.barrasiDataGridViewCheckBoxColumn,
            this.start,
            this.finish});
            this.dgvDarkhast.DataSource = this.bargekhoroojBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDarkhast.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDarkhast.EnableHeadersVisualStyles = false;
            this.dgvDarkhast.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvDarkhast.Location = new System.Drawing.Point(180, 16);
            this.dgvDarkhast.Name = "dgvDarkhast";
            this.dgvDarkhast.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhast.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDarkhast.RowHeadersVisible = false;
            this.dgvDarkhast.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDarkhast.Size = new System.Drawing.Size(926, 262);
            this.dgvDarkhast.TabIndex = 0;
            this.dgvDarkhast.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDarkhast_CellClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.FillWeight = 40F;
            this.id.HeaderText = "کد";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // dcode
            // 
            this.dcode.DataPropertyName = "dcode";
            this.dcode.FillWeight = 50F;
            this.dcode.HeaderText = "دوره";
            this.dcode.Name = "dcode";
            this.dcode.ReadOnly = true;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tedadDataGridViewTextBoxColumn
            // 
            this.tedadDataGridViewTextBoxColumn.DataPropertyName = "tedad";
            this.tedadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.tedadDataGridViewTextBoxColumn.HeaderText = "تعداد";
            this.tedadDataGridViewTextBoxColumn.Name = "tedadDataGridViewTextBoxColumn";
            this.tedadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.FillWeight = 75F;
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tozihatDataGridViewTextBoxColumn
            // 
            this.tozihatDataGridViewTextBoxColumn.DataPropertyName = "tozihat";
            this.tozihatDataGridViewTextBoxColumn.FillWeight = 175F;
            this.tozihatDataGridViewTextBoxColumn.HeaderText = "توضیحات";
            this.tozihatDataGridViewTextBoxColumn.Name = "tozihatDataGridViewTextBoxColumn";
            this.tozihatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // barrasiDataGridViewCheckBoxColumn
            // 
            this.barrasiDataGridViewCheckBoxColumn.DataPropertyName = "barrasi";
            this.barrasiDataGridViewCheckBoxColumn.FillWeight = 40F;
            this.barrasiDataGridViewCheckBoxColumn.HeaderText = "بررسی";
            this.barrasiDataGridViewCheckBoxColumn.Name = "barrasiDataGridViewCheckBoxColumn";
            this.barrasiDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // start
            // 
            this.start.DataPropertyName = "start";
            this.start.FillWeight = 80F;
            this.start.HeaderText = "شروع";
            this.start.Name = "start";
            this.start.ReadOnly = true;
            // 
            // finish
            // 
            this.finish.DataPropertyName = "finish";
            this.finish.FillWeight = 80F;
            this.finish.HeaderText = "پایان";
            this.finish.Name = "finish";
            this.finish.ReadOnly = true;
            // 
            // bargekhoroojBindingSource
            // 
            this.bargekhoroojBindingSource.DataMember = "barge_khorooj";
            this.bargekhoroojBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btn_new_darkhast
            // 
            this.btn_new_darkhast.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_new_darkhast.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_new_darkhast.BackColor = System.Drawing.Color.Transparent;
            this.btn_new_darkhast.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_new_darkhast.Location = new System.Drawing.Point(3, 3);
            this.btn_new_darkhast.Name = "btn_new_darkhast";
            this.btn_new_darkhast.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_new_darkhast.Size = new System.Drawing.Size(160, 60);
            this.btn_new_darkhast.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_new_darkhast.Symbol = "";
            this.btn_new_darkhast.SymbolColor = System.Drawing.Color.Green;
            this.btn_new_darkhast.SymbolSize = 9F;
            this.btn_new_darkhast.TabIndex = 23;
            this.btn_new_darkhast.Text = "ثبت درخواست جدید";
            this.btn_new_darkhast.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_transfer);
            this.groupPanel2.Controls.Add(this.lbl_moshtarek_name);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.btn_new_darkhast);
            this.groupPanel2.Controls.Add(this.btn_find_company);
            this.groupPanel2.Controls.Add(this.txt_find_moshtarek);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel2.Location = new System.Drawing.Point(25, 0);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1125, 96);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "جستجوی درخواست مشترک";
            this.groupPanel2.Click += new System.EventHandler(this.groupPanel2_Click);
            // 
            // lbl_moshtarek_name
            // 
            this.lbl_moshtarek_name.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_moshtarek_name.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_moshtarek_name.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.lbl_moshtarek_name.ForeColor = System.Drawing.Color.Black;
            this.lbl_moshtarek_name.Location = new System.Drawing.Point(367, 18);
            this.lbl_moshtarek_name.Name = "lbl_moshtarek_name";
            this.lbl_moshtarek_name.Size = new System.Drawing.Size(339, 33);
            this.lbl_moshtarek_name.TabIndex = 25;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(694, 18);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 33);
            this.labelX1.TabIndex = 24;
            this.labelX1.Text = "نام مشترک :";
            // 
            // btn_find_company
            // 
            this.btn_find_company.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_company.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_company.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_company.Location = new System.Drawing.Point(830, 23);
            this.btn_find_company.Name = "btn_find_company";
            this.btn_find_company.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_company.Size = new System.Drawing.Size(103, 23);
            this.btn_find_company.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_company.Symbol = "";
            this.btn_find_company.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_company.SymbolSize = 9F;
            this.btn_find_company.TabIndex = 1;
            this.btn_find_company.Text = "جستجو";
            this.btn_find_company.Click += new System.EventHandler(this.btn_find_company_Click);
            // 
            // txt_find_moshtarek
            // 
            this.txt_find_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_find_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_find_moshtarek.Location = new System.Drawing.Point(939, 20);
            this.txt_find_moshtarek.Name = "txt_find_moshtarek";
            this.txt_find_moshtarek.PreventEnterBeep = true;
            this.txt_find_moshtarek.Size = new System.Drawing.Size(86, 28);
            this.txt_find_moshtarek.TabIndex = 0;
            this.txt_find_moshtarek.TextChanged += new System.EventHandler(this.txt_find_moshtarek_TextChanged);
            this.txt_find_moshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_moshtarek_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(1040, 18);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(35, 33);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "قرارداد:";
            // 
            // barge_khoroojTableAdapter
            // 
            this.barge_khoroojTableAdapter.ClearBeforeFill = true;
            // 
            // btn_transfer
            // 
            this.btn_transfer.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_transfer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_transfer.BackColor = System.Drawing.Color.Transparent;
            this.btn_transfer.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_transfer.Location = new System.Drawing.Point(180, 3);
            this.btn_transfer.Name = "btn_transfer";
            this.btn_transfer.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_transfer.Size = new System.Drawing.Size(305, 60);
            this.btn_transfer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_transfer.Symbol = "";
            this.btn_transfer.SymbolColor = System.Drawing.Color.Green;
            this.btn_transfer.SymbolSize = 9F;
            this.btn_transfer.TabIndex = 26;
            this.btn_transfer.Text = "انتقال برگه ها از سیستم قدیم به جدید";
            this.btn_transfer.Visible = false;
            this.btn_transfer.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // Frm_barge_darkhast
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1170, 464);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_barge_darkhast";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmBargeDarkhast_Load);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDarkhast;
        private DevComponents.DotNetBar.ButtonX btn_find_company;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource bargekhoroojBindingSource;
        private MainDataSestTableAdapters.barge_khoroojTableAdapter barge_khoroojTableAdapter;
        private DevComponents.DotNetBar.LabelX lblMoshtgariSum;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX lblMoshtariCount;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX btnDelete;
        private DevComponents.DotNetBar.ButtonX btn_new_darkhast;
        private DevComponents.DotNetBar.LabelX lbl_moshtarek_name;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tedadDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn barrasiDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn start;
        private System.Windows.Forms.DataGridViewTextBoxColumn finish;
        private DevComponents.DotNetBar.ButtonX btn_transfer;
    }
}