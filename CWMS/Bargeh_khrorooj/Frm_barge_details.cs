﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Frm_barge_details : MyMetroForm
    {






        public Frm_barge_details()
        {
            InitializeComponent();
        }

        public Frm_barge_details(string ghararada,string DarkhastId)
        {

            InitializeComponent();
            txt_find_moshtarek.Text = ghararada;
            btn_find_company.PerformClick();
          
        }

        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_company.PerformClick();
        }

        private void btn_find_company_Click(object sender, EventArgs e)
        {
            
            dgvUsed.DataSource = dgvNotUsed.DataSource = dgvGheirFaal.DataSource = null;
            barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, -1);
            tabAll.Text = "کلیه برگه ها";
            TabBlocked.Text = "غیرفعال";
            TabUsed.Text = "استفاده شده";
            TabNotUsed.Text = "استفاده نشده";
            grpAllBargehD.Text = "وضعیت برگه ها";
            btnManageAll.Text = "مدیریت تعداد بالای برگه های درخواست ها";
            if (txt_find_moshtarek.Text.Trim() != "")
            {
                try
                {
                    int gharardad = Convert.ToInt32(txt_find_moshtarek.Text);
                    string co_name = Classes.clsMoshtarekin.Exist_gharardad(gharardad.ToString());
                    if (co_name == "")
                    {
                        Payam.Show("شماره قرارداد در سیستم تعریف نشده است ");
                        txt_find_moshtarek.SelectAll();
                        txt_find_moshtarek.Focus();
                        

                    }
                    else
                    {
                        lblConame.Text = co_name;
                    }
                    barge_khoroojTableAdapter.FillByGharardad(mainDataSest.barge_khorooj, gharardad);
                    grpDarkhast.Text = mainDataSest.barge_khorooj.Rows.Count.ToString() + " - لیست درخواست ها  ";

                }
                catch
                {
                    Payam.Show("شماره قرارداد نامعتبر می باشد");
                    txt_find_moshtarek.SelectAll();
                    txt_find_moshtarek.Focus();
                 
                }
            }
        }

        private void FrmBargehDetails_Load(object sender, EventArgs e)
        {
     
        } 

        private void dgvDarkhjast_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                btn_show_darkhast_details.PerformClick();

            }
        }

        private void btnSetfaal_Click(object sender, EventArgs e)
        {
            if (superTabControl1.SelectedTabIndex == 0)
            {

                if (dgvBargeDeatils.SelectedRows.Count != 0)
                {
                    
                    Classes.clsBargeh.block_unblock_bargeh(Convert.ToInt32(dgvBargeDeatils.SelectedRows[0].Cells[0].Value.ToString()),Convert.ToInt32(lbl_darkhast_id.Text),0);
                    barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, Convert.ToInt32(dgvDarkhjast.Rows[dgvDarkhjast.SelectedRows[0].Index].Cells[0].Value));
                    DataView dv = new DataView(mainDataSest1.barge_khorooj_details);
                    dv.RowFilter = "block=1";
                    dgvGheirFaal.DataSource = dv.ToTable();

                    TabBlocked.Text = dv.Count.ToString() + " - غیرفعال  ";
                }
            }
            else if(superTabControl1.SelectedTabIndex==3)
            {
                if (dgvGheirFaal.SelectedRows.Count != 0)
                {
                    
                    Classes.clsBargeh.block_unblock_bargeh(Convert.ToInt32(dgvGheirFaal.SelectedRows[0].Cells[0].Value.ToString()), Convert.ToInt32(lbl_darkhast_id.Text), 0);
                    
                    barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, Convert.ToInt32(dgvDarkhjast.Rows[dgvDarkhjast.SelectedRows[0].Index].Cells[0].Value));
                    DataView dv = new DataView(mainDataSest1.barge_khorooj_details);
                    dv.RowFilter = "block=1";
                    dgvGheirFaal.DataSource = dv.ToTable();

                    TabBlocked.Text = dv.Count.ToString() + " - غیرفعال  ";
                }
            }
        }

        private void btnSetNotFaal_Click(object sender, EventArgs e)
        {
            

            if (dgvBargeDeatils.SelectedRows.Count != 0)
            {
                Classes.clsBargeh.block_unblock_bargeh( Convert.ToInt32(dgvBargeDeatils.SelectedRows[0].Cells[0].Value.ToString())  ,Convert.ToInt32(lbl_darkhast_id.Text),1);
                
                barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, Convert.ToInt32(dgvDarkhjast.Rows[dgvDarkhjast.SelectedRows[0].Index].Cells[0].Value));
                DataView dv = new DataView(mainDataSest1.barge_khorooj_details);
                dv.RowFilter = "block=1";
                dgvGheirFaal.DataSource = dv.ToTable();
                TabBlocked.Text = dv.Count.ToString() + " - غیرفعال  ";

            }
        }

        private void btnManageAll_Click(object sender, EventArgs e)
        {
            if (dgvDarkhjast.SelectedRows.Count != 0)
            {
                Classes.ClsMain.ChangeCulture("e");
            
                
                frm_barge_active_deactive frm = new frm_barge_active_deactive(dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString(), FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(dgvDarkhjast.SelectedRows[0].Cells[1].Value.ToString()).ToString("d"));
                Classes.ClsMain.ChangeCulture("f");

                frm.ShowDialog();
                barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, Convert.ToInt32(dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString()));
                DataView dv = new DataView(mainDataSest1.barge_khorooj_details);
                dv.RowFilter = "block=1";
                dgvGheirFaal.DataSource = dv.ToTable();
                TabBlocked.Text = dv.Count.ToString() + " - غیرفعال  ";
            }
            else
                Payam.Show("لطفا ابتدا یکی از درخواست ها را از کادر سمت چپ انتخاب نمایید");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (dgvDarkhjast.SelectedRows.Count != 0 && txt_find_moshtarek.Text.Trim()!="")
            {
                if (Convert.ToBoolean(dgvDarkhjast.SelectedRows[0].Cells[3].Value) == true)
                {
                    new Bargeh_khrorooj.Frm_barge_chap(dgvDarkhjast.SelectedRows[0].Cells[4].Value.ToString(), dgvDarkhjast.SelectedRows[0].Cells[5].Value.ToString(), dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString(), txt_find_moshtarek.Text).ShowDialog();

                }
                else
                    Payam.Show("امکان چاپ برگه خروج های مربوط به درخواست های تایید نشده امکان پذیر نمی باشد");

            }
            else
                Payam.Show("لطفا یکی از درخواست ها را انتخاب نمایید");
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (dgvDarkhjast.SelectedRows.Count != 0)
            {
                barge_khorooj_detailsTableAdapter.FillByDarkhastID(mainDataSest1.barge_khorooj_details, Convert.ToInt32(dgvDarkhjast.SelectedRows[0].Cells[0].Value));
                grpAllBargehD.Text = "وضعیت برگه های درخواست :" + dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString();
                btnManageAll.Text = "مدیریت تعدادبالای برگه های درخواست" + dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString(); ;
                tabAll.Text = mainDataSest1.barge_khorooj_details.Rows.Count.ToString() + " - کلیه برگه ها  ";
                DataView dv = new DataView(mainDataSest1.barge_khorooj_details);
                dv.RowFilter = "used=1";
                dgvUsed.DataSource = dv.ToTable();
                TabUsed.Text = dv.Count.ToString() + " - استفاده شده";
                dv.RowFilter = "used=0";
                dgvNotUsed.DataSource = dv.ToTable();
                TabNotUsed.Text = dv.Count.ToString() + " - استفاده نشده  ";
                dv.RowFilter = "block=1";
                dgvGheirFaal.DataSource = dv.ToTable();
                TabBlocked.Text = dv.Count.ToString() + " - غیرفعال  ";
                lbl_darkhast_id.Text= dgvDarkhjast.SelectedRows[0].Cells[0].Value.ToString();

            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            dgvBargeDeatils.SelectAll();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            dgvBargeDeatils.ClearSelection();
        }

        private void dgvDarkhjast_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}