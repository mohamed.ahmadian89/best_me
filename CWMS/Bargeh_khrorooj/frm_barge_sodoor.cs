﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
namespace CWMS
{
    public partial class frm_barge_sodoor : MyMetroForm
    {
        public frm_barge_sodoor()
        {
            InitializeComponent();
        }

        public frm_barge_sodoor(string ghararad)
        {
            InitializeComponent();
            txt_find_moshtarek.Text = ghararad;
            btn_find_company.PerformClick();
        }


        private void frmBargeSoddor_Load(object sender, EventArgs e)
        {

        }

        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_company.PerformClick();
        }

        private void btn_find_company_Click(object sender, EventArgs e)
        {
            grptedad.Visible = false;
            dgvElhaghiBedehi.Rows.Clear();
            if (txt_find_moshtarek.Text.Trim() == "")
            {
                txt_find_moshtarek.Focus();
            }
            else
            {
                lblMoshtarekname.Text = Classes.clsMoshtarekin.Exist_gharardad(txt_find_moshtarek.Text);
                if (lblMoshtarekname.Text.Trim() == "")
                {
                    Payam.Show("شماره قرارداد وارد شده صحیح نمی باشد");
                    mainDataSest.barge_khorooj.Rows.Clear();
                    txtMoshtarekCode.Text = "";
                    txt_find_moshtarek.SelectAll();
                    txt_find_moshtarek.Focus();
                }
                else
                {
                    grptedad.Visible = true;
                    txtMoshtarekCode.Text = txt_find_moshtarek.Text;
                    string elhaghiList = txtMoshtarekCode.Text + "," + Classes.ClsMain
               .GetDataTable("select elhaghi from moshtarekin where gharardad = " + txtMoshtarekCode.Text)
               .AsEnumerable().First()["elhaghi"].ToString();
                    if(elhaghiList.EndsWith(","))
                    {
                        elhaghiList = elhaghiList.Substring(0, elhaghiList.Length - 1);
                    }
                    if (elhaghiList.Trim().Length > 0)
                        foreach (string elhaghi in elhaghiList.Split(','))
                        {
                            long bedehiAb, bedehiSharj;

                            try
                            {
                                bedehiAb = Convert.ToInt64(Classes.ClsMain
                     .GetDataTable("select top(1) mande from ab_ghabz where gharardad = "
                     + elhaghi + " order by dcode desc")
                     .AsEnumerable().First()["mande"]);
                            }
                            catch (Exception)
                            {

                                bedehiAb = 0;
                            }

                            try
                            {
                                bedehiSharj = Convert.ToInt64(Classes.ClsMain
                           .GetDataTable("select top(1) mande from sharj_ghabz where gharardad = "
                           + elhaghi + " order by dcode desc")
                           .AsEnumerable().First()["mande"]);
                            }
                            catch (Exception)
                            {
                                bedehiSharj = 0;
                            }




                            dgvElhaghiBedehi.Rows.Add(elhaghi, bedehiSharj,bedehiAb );
                        }


                    cmbtedad.Focus();
                }

            }
        }



        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void btnSabt_Click_1(object sender, EventArgs e)
        {
            if(Classes.clsMoshtarekin.UserInfo.p82)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 82, txtMoshtarekCode.Text);

                if (txtMoshtarekCode.Text.Trim() == "")
                {
                    Payam.Show("لطفا ابتدا کد مشترک را در کادر بالا وارد کرده و بر روی دکمه جستجو کلیک نمایید");
                    txt_find_moshtarek.SelectAll();
                    txt_find_moshtarek.Focus();
                }
                else
                {
                    
                    try
                    {
                        int number = Convert.ToInt32(cmbtedad.Text);
                        if (number > 0)
                        {
                            
                            

                            Classes.clsBargeh.DarkhastJadidByAdmin(txtMoshtarekCode.Text, cmbtedad.Text.ToString());
                            Payam.Show("درخواست جدید با موفقیت در سیستم ثبت شد ");
                            this.Close();
                            

                            //if(Classes.clsBargeh.Check_connection())
                            //{
                            //   int result= Classes.clsBargeh.Sodoor_bargeh_in_client(start, finish, txtMoshtarekCode.Text, darkhast_id);
                            //    if(result!=-1)
                            //    {
                            //        Classes.clsBargeh.client_Delete_addlist(darkhast_id);
                            //    }
                            //}
                            

                        }
                    }
                    catch (Exception)
                    {

                        Payam.Show("لطفا تعداد برگه های خروجی را به درستی وارد نمایید");

                    }
                    cmbtedad.Text = "";
                    cmbtedad.Focus();


                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void cmbtedad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSabt.PerformClick();
        }
        
        private void groupPanel2_Click(object sender, EventArgs e)
        {

        }
    }
}