﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Bargeh_khrorooj
{
    public partial class frm_Bageh_print_Management : Form
    {
        public frm_Bageh_print_Management()
        {
            InitializeComponent();
        }

        void Show_data()
        {
            this.barge_khorooj_detailsTableAdapter.Fill(this.mainAbDataset.barge_khorooj_details);
            bs_printed.Filter = "printed=true";
            bs_not_printed.Filter = "printed=false";
            bs_not_printed.Sort = "id desc";
            bs_printed.Sort = "id desc";

            txt_not_printed_count.Text = bs_not_printed.Count.ToString();
            txt_printed_count.Text = bs_printed.Count.ToString();
        }

        private void frm_Bageh_print_Management_Load(object sender, EventArgs e)
        {
            Show_data();
        }

        private void txt_find_not_prined_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_not_printed.PerformClick();
        }

        private void txt_find_in_printed_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnb_printed.PerformClick();
        }

        private void btn_not_printed_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_find_not_prined.Text != "")
                {
                    bs_not_printed.Filter = "gharardad=" + txt_find_not_prined.Text + " and printed=false";
                }
                else
                {
                    txt_find_not_prined.Focus();
                    bs_not_printed.Filter = "printed=false";
                }
                }


             catch (Exception)
            {
                bs_not_printed.Filter ="printed=false";
                Payam.Show("لطفا شماره قرارداد را به درستی وارد نمایید");
                txt_find_not_prined.SelectAll();
                txt_find_not_prined.Focus();
            }
        }

        private void btnb_printed_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_find_in_printed.Text != "")
                {
                    bs_printed.Filter = "gharardad=" + txt_find_in_printed.Text + " and printed=true";
                    bs_printed.Sort = "id desc";
                }
                else
                {
                    txt_find_in_printed.Focus();
                    bs_printed.Filter = "printed=true";
                    bs_printed.Sort = "id desc";

                }
            }


            catch (Exception)
            {
                bs_printed.Filter = "printed=true";
                bs_printed.Sort = "id desc";
                Payam.Show("لطفا شماره قرارداد را به درستی وارد نمایید");
                txt_find_in_printed.SelectAll();
                txt_find_in_printed.Focus();
            }
        }

        private void dgv_not_printed_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_not_printed.SelectedRows.Count != 0)
            {
                new Bargeh_khrorooj.Frm_barge_chap(
                    dgv_not_printed.SelectedRows[0].Cells["startDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["finishDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["idDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["gharardadDataGridViewTextBoxColumn"].Value.ToString()
                    ).ShowDialog();
                Show_data();

            }
        }

        private void dgv_printed_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_printed.SelectedRows.Count != 0)
            {
                new Bargeh_khrorooj.Frm_barge_chap(
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn4"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn5"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn1"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn2"].Value.ToString()
                    ).ShowDialog();
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            bs_not_printed.Sort = "id desc";
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            bs_not_printed.Sort = "id asc";
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            bs_printed.Sort = "id desc";
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (dgv_not_printed.SelectedRows.Count != 0)
            {
                new Bargeh_khrorooj.Frm_barge_chap(
                    dgv_not_printed.SelectedRows[0].Cells["startDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["finishDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["idDataGridViewTextBoxColumn"].Value.ToString(),
                    dgv_not_printed.SelectedRows[0].Cells["gharardadDataGridViewTextBoxColumn"].Value.ToString()
                    ).ShowDialog();
                Show_data();

            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if (dgv_printed.SelectedRows.Count != 0)
            {
                new Bargeh_khrorooj.Frm_barge_chap(
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn4"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn5"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn1"].Value.ToString(),
                    dgv_printed.SelectedRows[0].Cells["dataGridViewTextBoxColumn2"].Value.ToString()
                    ).ShowDialog();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Show_data();
        }
    }
}
