using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class frm_barge_active_deactive : MyMetroForm
    {
        public frm_barge_active_deactive(string darkhast_id, string tarikh)
        {
            InitializeComponent();
            lblCode.Text = darkhast_id;
            DataTable dt_darkhast_info = Classes.ClsMain.GetDataTable("select start,finish from barge_khorooj where id=" + darkhast_id);

            string start = dt_darkhast_info.Rows[0]["start"].ToString();
            string finish = dt_darkhast_info.Rows[0]["finish"].ToString();

            lblEnd.Text = finish;
            lblStart.Text = start;
            lblTarikh.Text = tarikh;
            TxtStart.MinValue = Convert.ToInt32(start);
            TxtStart.MaxValue = Convert.ToInt32(finish);
            TxtStart.Value = Convert.ToInt32(start);

            TxtEnd.MaxValue = Convert.ToInt32(finish);
            TxtEnd.Value = Convert.ToInt32(finish);
            TxtEnd.MinValue = Convert.ToInt32(start);


        }

        private void FrmBargeManagePeriod_Load(object sender, EventArgs e)
        {





        }

        private void btnSetfaal_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj_details set block=0  where id>=" + TxtStart.Value.ToString() + " and id<=" + TxtEnd.Value.ToString());
            for (int i = Convert.ToInt32(TxtStart.Value); i <= Convert.ToInt32(TxtEnd.Value); i++)
            {
                Classes.clsBargeh.client_new_Blocklist( Convert.ToInt32(lblCode.Text) , i  , 0);
            }
            Classes.ClsMain.ChangeCulture("f");

            this.Close();
        }

        private void btnSetNotFaal_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj_details set block=1 where id>=" + TxtStart.Value.ToString() + " and id<=" + TxtEnd.Value.ToString());
            for (int i = Convert.ToInt32(TxtStart.Value); i <= Convert.ToInt32(TxtEnd.Value); i++)
            {
                Classes.clsBargeh.client_new_Blocklist(Convert.ToInt32(lblCode.Text), i, 1);
            }
            Classes.ClsMain.ChangeCulture("f");

            this.Close();
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }
    }
}