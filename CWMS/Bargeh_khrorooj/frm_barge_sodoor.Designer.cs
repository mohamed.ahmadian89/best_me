﻿namespace CWMS
{
    partial class frm_barge_sodoor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bargekhoroojBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.barge_khoroojTableAdapter = new CWMS.MainDataSestTableAdapters.barge_khoroojTableAdapter();
            this.grptedad = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cmbtedad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSabt = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find_company = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dgvElhaghiBedehi = new System.Windows.Forms.DataGridView();
            this.gharardad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMoshtarekCode = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.lblMoshtarekname = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.grptedad.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghiBedehi)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bargekhoroojBindingSource
            // 
            this.bargekhoroojBindingSource.DataMember = "barge_khorooj";
            this.bargekhoroojBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // barge_khoroojTableAdapter
            // 
            this.barge_khoroojTableAdapter.ClearBeforeFill = true;
            // 
            // grptedad
            // 
            this.grptedad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grptedad.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grptedad.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grptedad.Controls.Add(this.cmbtedad);
            this.grptedad.Controls.Add(this.btnSabt);
            this.grptedad.Controls.Add(this.labelX3);
            this.grptedad.DisabledBackColor = System.Drawing.Color.Empty;
            this.grptedad.Location = new System.Drawing.Point(45, 301);
            this.grptedad.Margin = new System.Windows.Forms.Padding(4);
            this.grptedad.Name = "grptedad";
            this.grptedad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grptedad.Size = new System.Drawing.Size(799, 80);
            // 
            // 
            // 
            this.grptedad.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grptedad.Style.BackColorGradientAngle = 90;
            this.grptedad.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grptedad.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grptedad.Style.BorderBottomWidth = 1;
            this.grptedad.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grptedad.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grptedad.Style.BorderLeftWidth = 1;
            this.grptedad.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grptedad.Style.BorderRightWidth = 1;
            this.grptedad.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grptedad.Style.BorderTopWidth = 1;
            this.grptedad.Style.CornerDiameter = 4;
            this.grptedad.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grptedad.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grptedad.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grptedad.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grptedad.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grptedad.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grptedad.TabIndex = 6;
            this.grptedad.Text = "درخواست صدور جدید";
            this.grptedad.Visible = false;
            // 
            // cmbtedad
            // 
            this.cmbtedad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbtedad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.cmbtedad.Border.Class = "TextBoxBorder";
            this.cmbtedad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cmbtedad.DisabledBackColor = System.Drawing.Color.White;
            this.cmbtedad.ForeColor = System.Drawing.Color.Black;
            this.cmbtedad.Location = new System.Drawing.Point(434, 12);
            this.cmbtedad.Margin = new System.Windows.Forms.Padding(4);
            this.cmbtedad.Name = "cmbtedad";
            this.cmbtedad.PreventEnterBeep = true;
            this.cmbtedad.Size = new System.Drawing.Size(108, 28);
            this.cmbtedad.TabIndex = 15;
            this.cmbtedad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbtedad_KeyDown);
            // 
            // btnSabt
            // 
            this.btnSabt.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSabt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSabt.BackColor = System.Drawing.Color.Transparent;
            this.btnSabt.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSabt.Location = new System.Drawing.Point(114, 13);
            this.btnSabt.Margin = new System.Windows.Forms.Padding(4);
            this.btnSabt.Name = "btnSabt";
            this.btnSabt.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnSabt.Size = new System.Drawing.Size(283, 27);
            this.btnSabt.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSabt.Symbol = "";
            this.btnSabt.SymbolColor = System.Drawing.Color.Green;
            this.btnSabt.SymbolSize = 9F;
            this.btnSabt.TabIndex = 13;
            this.btnSabt.Text = "ثبت درخواست جدید در سیستم";
            this.btnSabt.Click += new System.EventHandler(this.btnSabt_Click_1);
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(523, 4);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(145, 39);
            this.labelX3.TabIndex = 12;
            this.labelX3.Text = "تعداد برگه درخواستی :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_find_company);
            this.groupPanel2.Controls.Add(this.txt_find_moshtarek);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(45, 4);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(799, 85);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 2;
            this.groupPanel2.Text = "جستجوی اطلاعات مشترک بر اساس شماره قرارداد";
            this.groupPanel2.Click += new System.EventHandler(this.groupPanel2_Click);
            // 
            // btn_find_company
            // 
            this.btn_find_company.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_company.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_find_company.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_company.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_company.Location = new System.Drawing.Point(182, 14);
            this.btn_find_company.Margin = new System.Windows.Forms.Padding(4);
            this.btn_find_company.Name = "btn_find_company";
            this.btn_find_company.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_company.Size = new System.Drawing.Size(137, 27);
            this.btn_find_company.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_company.Symbol = "";
            this.btn_find_company.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_company.SymbolSize = 9F;
            this.btn_find_company.TabIndex = 1;
            this.btn_find_company.Text = "جستجو";
            this.btn_find_company.Click += new System.EventHandler(this.btn_find_company_Click);
            // 
            // txt_find_moshtarek
            // 
            this.txt_find_moshtarek.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_find_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_find_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_find_moshtarek.Location = new System.Drawing.Point(347, 13);
            this.txt_find_moshtarek.Margin = new System.Windows.Forms.Padding(4);
            this.txt_find_moshtarek.Name = "txt_find_moshtarek";
            this.txt_find_moshtarek.PreventEnterBeep = true;
            this.txt_find_moshtarek.Size = new System.Drawing.Size(115, 28);
            this.txt_find_moshtarek.TabIndex = 0;
            this.txt_find_moshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_moshtarek_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(482, 8);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(97, 39);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "قرارداد مشترک:";
            // 
            // dgvElhaghiBedehi
            // 
            this.dgvElhaghiBedehi.AllowUserToAddRows = false;
            this.dgvElhaghiBedehi.AllowUserToDeleteRows = false;
            this.dgvElhaghiBedehi.AllowUserToResizeRows = false;
            this.dgvElhaghiBedehi.BackgroundColor = System.Drawing.Color.White;
            this.dgvElhaghiBedehi.ColumnHeadersHeight = 25;
            this.dgvElhaghiBedehi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardad,
            this.sharj,
            this.ab});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvElhaghiBedehi.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvElhaghiBedehi.Location = new System.Drawing.Point(20, 51);
            this.dgvElhaghiBedehi.Margin = new System.Windows.Forms.Padding(4);
            this.dgvElhaghiBedehi.Name = "dgvElhaghiBedehi";
            this.dgvElhaghiBedehi.RowHeadersVisible = false;
            this.dgvElhaghiBedehi.RowTemplate.Height = 24;
            this.dgvElhaghiBedehi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvElhaghiBedehi.Size = new System.Drawing.Size(741, 112);
            this.dgvElhaghiBedehi.TabIndex = 18;
            // 
            // gharardad
            // 
            this.gharardad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.gharardad.FillWeight = 70F;
            this.gharardad.HeaderText = "قرارداد";
            this.gharardad.Name = "gharardad";
            // 
            // sharj
            // 
            this.sharj.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Format = "0,0";
            this.sharj.DefaultCellStyle = dataGridViewCellStyle1;
            this.sharj.FillWeight = 150F;
            this.sharj.HeaderText = "بدهی شارژ";
            this.sharj.Name = "sharj";
            // 
            // ab
            // 
            this.ab.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Format = "0,0";
            this.ab.DefaultCellStyle = dataGridViewCellStyle2;
            this.ab.FillWeight = 150F;
            this.ab.HeaderText = "بدهی آب";
            this.ab.Name = "ab";
            // 
            // txtMoshtarekCode
            // 
            this.txtMoshtarekCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMoshtarekCode.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txtMoshtarekCode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMoshtarekCode.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMoshtarekCode.ForeColor = System.Drawing.Color.Black;
            this.txtMoshtarekCode.Location = new System.Drawing.Point(593, 18);
            this.txtMoshtarekCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtMoshtarekCode.Name = "txtMoshtarekCode";
            this.txtMoshtarekCode.Size = new System.Drawing.Size(108, 25);
            this.txtMoshtarekCode.TabIndex = 17;
            this.txtMoshtarekCode.Text = " ";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(688, 18);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(73, 25);
            this.labelX4.TabIndex = 16;
            this.labelX4.Text = "قرارداد:";
            // 
            // lblMoshtarekname
            // 
            this.lblMoshtarekname.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMoshtarekname.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMoshtarekname.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMoshtarekname.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblMoshtarekname.ForeColor = System.Drawing.Color.Black;
            this.lblMoshtarekname.Location = new System.Drawing.Point(52, 18);
            this.lblMoshtarekname.Margin = new System.Windows.Forms.Padding(4);
            this.lblMoshtarekname.Name = "lblMoshtarekname";
            this.lblMoshtarekname.Size = new System.Drawing.Size(356, 25);
            this.lblMoshtarekname.TabIndex = 8;
            this.lblMoshtarekname.Text = " ";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(408, 18);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(81, 25);
            this.labelX1.TabIndex = 7;
            this.labelX1.Text = "نام مشترک :";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.dgvElhaghiBedehi);
            this.groupPanel1.Controls.Add(this.txtMoshtarekCode);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.lblMoshtarekname);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(45, 97);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(799, 196);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 7;
            this.groupPanel1.Text = "اطلاعات مشترک به همراه قراردادهای الحاقیه";
            // 
            // frm_barge_sodoor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 393);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.grptedad);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_barge_sodoor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmBargeSoddor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bargekhoroojBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.grptedad.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghiBedehi)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_find_company;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX lblMoshtarekname;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.BindingSource bargekhoroojBindingSource;
        private MainDataSest mainDataSest;
        private MainDataSestTableAdapters.barge_khoroojTableAdapter barge_khoroojTableAdapter;
        private DevComponents.DotNetBar.Controls.GroupPanel grptedad;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnSabt;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX txtMoshtarekCode;
        private DevComponents.DotNetBar.Controls.TextBoxX cmbtedad;
        private System.Windows.Forms.DataGridView dgvElhaghiBedehi;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardad;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharj;
        private System.Windows.Forms.DataGridViewTextBoxColumn ab;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
    }
}