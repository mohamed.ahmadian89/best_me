﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Bargeh_khrorooj
{
    public partial class frm_all_client_darkhast : MyMetroForm
    {
        public frm_all_client_darkhast()
        {
            InitializeComponent();
        }

        private void frm_all_client_darkhast_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'barge_ds.bargeh_client_blocklist' table. You can move, or remove it, as needed.
            this.bargeh_client_blocklistTableAdapter.Fill(this.barge_ds.bargeh_client_blocklist);
            // TODO: This line of code loads data into the 'barge_ds.bargeh_client_deletelist' table. You can move, or remove it, as needed.
            this.bargeh_client_deletelistTableAdapter.Fill(this.barge_ds.bargeh_client_deletelist);
            // TODO: This line of code loads data into the 'barge_ds.bargeh_client_addlist' table. You can move, or remove it, as needed.
            this.bargeh_client_addlistTableAdapter.Fill(this.barge_ds.bargeh_client_addlist);

        }

        private void superTabControlPanel1_Click(object sender, EventArgs e)
        {

        }

        private void dgv_ghobooz_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
