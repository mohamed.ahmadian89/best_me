﻿namespace CWMS
{
    partial class Frm_barge_report_All
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblsum = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.dgvDarkhast = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cntDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majmooDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bargehallstatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.bargeh_all_statTableAdapter = new CWMS.MainDataSestTableAdapters.Bargeh_all_statTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargehallstatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.lblsum);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.dgvDarkhast);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(63, 1);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(761, 514);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "اطلاعات آماری کلیه دوره ها";
            // 
            // lblsum
            // 
            this.lblsum.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblsum.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblsum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblsum.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.lblsum.ForeColor = System.Drawing.Color.Black;
            this.lblsum.Location = new System.Drawing.Point(412, 442);
            this.lblsum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblsum.Name = "lblsum";
            this.lblsum.Size = new System.Drawing.Size(144, 39);
            this.lblsum.TabIndex = 17;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(688, 441);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(47, 39);
            this.labelX3.Symbol = "";
            this.labelX3.SymbolColor = System.Drawing.Color.CadetBlue;
            this.labelX3.SymbolSize = 12F;
            this.labelX3.TabIndex = 16;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(559, 442);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(119, 39);
            this.labelX1.TabIndex = 15;
            this.labelX1.Text = "مجموع کلیه برگه ها:";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonX1.Location = new System.Drawing.Point(28, 445);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(205, 27);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 8;
            this.buttonX1.Text = "تایید";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // dgvDarkhast
            // 
            this.dgvDarkhast.AllowUserToAddRows = false;
            this.dgvDarkhast.AllowUserToDeleteRows = false;
            this.dgvDarkhast.AllowUserToResizeColumns = false;
            this.dgvDarkhast.AllowUserToResizeRows = false;
            this.dgvDarkhast.AutoGenerateColumns = false;
            this.dgvDarkhast.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDarkhast.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhast.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDarkhast.ColumnHeadersHeight = 30;
            this.dgvDarkhast.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dcodeDataGridViewTextBoxColumn,
            this.cntDataGridViewTextBoxColumn,
            this.majmooDataGridViewTextBoxColumn});
            this.dgvDarkhast.DataSource = this.bargehallstatBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDarkhast.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDarkhast.EnableHeadersVisualStyles = false;
            this.dgvDarkhast.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvDarkhast.Location = new System.Drawing.Point(39, 16);
            this.dgvDarkhast.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvDarkhast.Name = "dgvDarkhast";
            this.dgvDarkhast.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDarkhast.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDarkhast.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDarkhast.Size = new System.Drawing.Size(696, 421);
            this.dgvDarkhast.TabIndex = 1;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cntDataGridViewTextBoxColumn
            // 
            this.cntDataGridViewTextBoxColumn.DataPropertyName = "cnt";
            this.cntDataGridViewTextBoxColumn.HeaderText = "تعداد درخواست ها";
            this.cntDataGridViewTextBoxColumn.Name = "cntDataGridViewTextBoxColumn";
            this.cntDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // majmooDataGridViewTextBoxColumn
            // 
            this.majmooDataGridViewTextBoxColumn.DataPropertyName = "majmoo";
            this.majmooDataGridViewTextBoxColumn.HeaderText = "مجموع برگه های صادر شده";
            this.majmooDataGridViewTextBoxColumn.Name = "majmooDataGridViewTextBoxColumn";
            this.majmooDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bargehallstatBindingSource
            // 
            this.bargehallstatBindingSource.DataMember = "Bargeh_all_stat";
            this.bargehallstatBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bargeh_all_statTableAdapter
            // 
            this.bargeh_all_statTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_barge_report_All
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 529);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_barge_report_All";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Rpt_bageh_all_doreh_stat_Load);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDarkhast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bargehallstatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvDarkhast;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource bargehallstatBindingSource;
        private MainDataSestTableAdapters.Bargeh_all_statTableAdapter bargeh_all_statTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cntDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn majmooDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX lblsum;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}