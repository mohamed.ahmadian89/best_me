﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Bargeh_khrorooj
{
    public partial class frm_bargeh_transfer_report : MyMetroForm
    {
        public frm_bargeh_transfer_report()
        {
            InitializeComponent();
        }

        private void frm_bargeh_transfer_report_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'barge_ds.bargeh_transfer' table. You can move, or remove it, as needed.
            this.bargeh_transferTableAdapter.Fill(this.barge_ds.bargeh_transfer);

        }
    }
}
