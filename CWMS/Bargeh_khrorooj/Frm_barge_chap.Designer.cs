﻿namespace CWMS.Bargeh_khrorooj
{
    partial class Frm_barge_chap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnSetNotFaal = new DevComponents.DotNetBar.ButtonX();
            this.TxtEnd = new DevComponents.Editors.IntegerInput();
            this.TxtStart = new DevComponents.Editors.IntegerInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStart)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btnSetNotFaal);
            this.groupPanel2.Controls.Add(this.TxtEnd);
            this.groupPanel2.Controls.Add(this.TxtStart);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(16, 1);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(635, 175);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 3;
            this.groupPanel2.Text = "چاپ برگه های خروج";
            // 
            // btnSetNotFaal
            // 
            this.btnSetNotFaal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSetNotFaal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSetNotFaal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSetNotFaal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.btnSetNotFaal.Location = new System.Drawing.Point(188, 76);
            this.btnSetNotFaal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSetNotFaal.Name = "btnSetNotFaal";
            this.btnSetNotFaal.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnSetNotFaal.Size = new System.Drawing.Size(255, 41);
            this.btnSetNotFaal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSetNotFaal.Symbol = "";
            this.btnSetNotFaal.SymbolColor = System.Drawing.Color.Green;
            this.btnSetNotFaal.SymbolSize = 9F;
            this.btnSetNotFaal.TabIndex = 2;
            this.btnSetNotFaal.Text = "چاپ برگه ها";
            this.btnSetNotFaal.Click += new System.EventHandler(this.btnSetNotFaal_Click);
            // 
            // TxtEnd
            // 
            this.TxtEnd.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtEnd.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TxtEnd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtEnd.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TxtEnd.Font = new System.Drawing.Font("B Yekan", 10F);
            this.TxtEnd.ForeColor = System.Drawing.Color.Black;
            this.TxtEnd.Location = new System.Drawing.Point(139, 25);
            this.TxtEnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEnd.Name = "TxtEnd";
            this.TxtEnd.ShowUpDown = true;
            this.TxtEnd.Size = new System.Drawing.Size(88, 28);
            this.TxtEnd.TabIndex = 1;
            this.TxtEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtEnd_KeyDown);
            // 
            // TxtStart
            // 
            this.TxtStart.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtStart.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TxtStart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtStart.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TxtStart.Font = new System.Drawing.Font("B Yekan", 10F);
            this.TxtStart.ForeColor = System.Drawing.Color.Black;
            this.TxtStart.Location = new System.Drawing.Point(330, 25);
            this.TxtStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtStart.Name = "TxtStart";
            this.TxtStart.ShowUpDown = true;
            this.TxtStart.Size = new System.Drawing.Size(88, 28);
            this.TxtStart.TabIndex = 0;
            this.TxtStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtStart_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(248, 20);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(52, 39);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "تا برگه :";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(439, 20);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(52, 39);
            this.labelX4.TabIndex = 11;
            this.labelX4.Text = "از برگه  :";
            // 
            // Frm_barge_chap
            // 
            this.AcceptButton = this.btnSetNotFaal;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 189);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_barge_chap";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmChapBargeh_Load);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btnSetNotFaal;
        private DevComponents.Editors.IntegerInput TxtEnd;
        private DevComponents.Editors.IntegerInput TxtStart;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}