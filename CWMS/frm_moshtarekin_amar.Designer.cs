﻿namespace CWMS
{
    partial class frm_moshtarekin_amar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_main = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarekin_count = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_have_ab = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_have_sharj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_metraj_all = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_metraj_tejari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.txt_metraj_gheir_tejari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarekin_count_tejari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_moshtarekin_count_gheir_tejari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.dgv_sharj_result = new System.Windows.Forms.DataGridView();
            this.dgv_ab_result = new System.Windows.Forms.DataGridView();
            this.grp_main.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_result)).BeginInit();
            this.SuspendLayout();
            // 
            // grp_main
            // 
            this.grp_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grp_main.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grp_main.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_main.Controls.Add(this.txt_moshtarekin_count_gheir_tejari);
            this.grp_main.Controls.Add(this.labelX9);
            this.grp_main.Controls.Add(this.txt_moshtarekin_count_tejari);
            this.grp_main.Controls.Add(this.labelX8);
            this.grp_main.Controls.Add(this.txt_metraj_gheir_tejari);
            this.grp_main.Controls.Add(this.labelX7);
            this.grp_main.Controls.Add(this.txt_metraj_tejari);
            this.grp_main.Controls.Add(this.labelX5);
            this.grp_main.Controls.Add(this.txt_metraj_all);
            this.grp_main.Controls.Add(this.labelX2);
            this.grp_main.Controls.Add(this.txt_moshtarek_have_sharj);
            this.grp_main.Controls.Add(this.labelX1);
            this.grp_main.Controls.Add(this.txt_moshtarek_have_ab);
            this.grp_main.Controls.Add(this.labelX6);
            this.grp_main.Controls.Add(this.txt_moshtarekin_count);
            this.grp_main.Controls.Add(this.labelX3);
            this.grp_main.Controls.Add(this.labelX4);
            this.grp_main.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_main.Location = new System.Drawing.Point(13, 13);
            this.grp_main.Margin = new System.Windows.Forms.Padding(4);
            this.grp_main.Name = "grp_main";
            this.grp_main.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_main.Size = new System.Drawing.Size(984, 241);
            // 
            // 
            // 
            this.grp_main.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_main.Style.BackColorGradientAngle = 90;
            this.grp_main.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_main.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderBottomWidth = 1;
            this.grp_main.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_main.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderLeftWidth = 1;
            this.grp_main.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderRightWidth = 1;
            this.grp_main.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderTopWidth = 1;
            this.grp_main.Style.CornerDiameter = 4;
            this.grp_main.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_main.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_main.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_main.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_main.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_main.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_main.TabIndex = 29;
            this.grp_main.Text = "اطلاعات اصلی";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(823, 19);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(112, 28);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "تعداد کل مشترکین :";
            // 
            // txt_moshtarekin_count
            // 
            this.txt_moshtarekin_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarekin_count.Border.Class = "TextBoxBorder";
            this.txt_moshtarekin_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarekin_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarekin_count.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarekin_count.Location = new System.Drawing.Point(609, 19);
            this.txt_moshtarekin_count.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moshtarekin_count.MaxLength = 5;
            this.txt_moshtarekin_count.Name = "txt_moshtarekin_count";
            this.txt_moshtarekin_count.PreventEnterBeep = true;
            this.txt_moshtarekin_count.Size = new System.Drawing.Size(113, 28);
            this.txt_moshtarekin_count.TabIndex = 0;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(964, 16);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(92, 28);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "کد قرارداد:";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(621, 163);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(312, 28);
            this.labelX1.TabIndex = 22;
            this.labelX1.Text = "تعداد کل مشترکینی که برای آنها قبض آب صادر خواهد شد :";
            // 
            // txt_moshtarek_have_ab
            // 
            this.txt_moshtarek_have_ab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_have_ab.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_have_ab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_have_ab.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_have_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_have_ab.Location = new System.Drawing.Point(500, 163);
            this.txt_moshtarek_have_ab.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moshtarek_have_ab.MaxLength = 5;
            this.txt_moshtarek_have_ab.Name = "txt_moshtarek_have_ab";
            this.txt_moshtarek_have_ab.PreventEnterBeep = true;
            this.txt_moshtarek_have_ab.Size = new System.Drawing.Size(113, 28);
            this.txt_moshtarek_have_ab.TabIndex = 21;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(163, 163);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(312, 28);
            this.labelX2.TabIndex = 24;
            this.labelX2.Text = "تعداد کل مشترکینی که برای آنها قبض شارژ صادر خواهد شد :";
            // 
            // txt_moshtarek_have_sharj
            // 
            this.txt_moshtarek_have_sharj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_have_sharj.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_have_sharj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_have_sharj.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_have_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_have_sharj.Location = new System.Drawing.Point(42, 163);
            this.txt_moshtarek_have_sharj.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moshtarek_have_sharj.MaxLength = 5;
            this.txt_moshtarek_have_sharj.Name = "txt_moshtarek_have_sharj";
            this.txt_moshtarek_have_sharj.PreventEnterBeep = true;
            this.txt_moshtarek_have_sharj.Size = new System.Drawing.Size(113, 28);
            this.txt_moshtarek_have_sharj.TabIndex = 23;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(103, 19);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(312, 28);
            this.labelX4.TabIndex = 26;
            this.labelX4.Text = "مجموع متراژ کلیه مشترکین:";
            // 
            // txt_metraj_all
            // 
            this.txt_metraj_all.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_metraj_all.Border.Class = "TextBoxBorder";
            this.txt_metraj_all.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_metraj_all.DisabledBackColor = System.Drawing.Color.White;
            this.txt_metraj_all.ForeColor = System.Drawing.Color.Black;
            this.txt_metraj_all.Location = new System.Drawing.Point(157, 19);
            this.txt_metraj_all.Margin = new System.Windows.Forms.Padding(4);
            this.txt_metraj_all.MaxLength = 5;
            this.txt_metraj_all.Name = "txt_metraj_all";
            this.txt_metraj_all.PreventEnterBeep = true;
            this.txt_metraj_all.Size = new System.Drawing.Size(113, 28);
            this.txt_metraj_all.TabIndex = 25;
            // 
            // txt_metraj_tejari
            // 
            this.txt_metraj_tejari.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_metraj_tejari.Border.Class = "TextBoxBorder";
            this.txt_metraj_tejari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_metraj_tejari.DisabledBackColor = System.Drawing.Color.White;
            this.txt_metraj_tejari.ForeColor = System.Drawing.Color.Black;
            this.txt_metraj_tejari.Location = new System.Drawing.Point(30, 55);
            this.txt_metraj_tejari.Margin = new System.Windows.Forms.Padding(4);
            this.txt_metraj_tejari.MaxLength = 5;
            this.txt_metraj_tejari.Name = "txt_metraj_tejari";
            this.txt_metraj_tejari.PreventEnterBeep = true;
            this.txt_metraj_tejari.Size = new System.Drawing.Size(113, 28);
            this.txt_metraj_tejari.TabIndex = 27;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(103, 55);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(312, 28);
            this.labelX5.TabIndex = 28;
            this.labelX5.Text = "مجموع متراژ کلیه مشترکین ( واحد های تجاری ):";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(103, 91);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(312, 28);
            this.labelX7.TabIndex = 29;
            this.labelX7.Text = "مجموع متراژ کلیه مشترکین ( واحد های غیرتجاری ) :";
            // 
            // txt_metraj_gheir_tejari
            // 
            this.txt_metraj_gheir_tejari.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_metraj_gheir_tejari.Border.Class = "TextBoxBorder";
            this.txt_metraj_gheir_tejari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_metraj_gheir_tejari.DisabledBackColor = System.Drawing.Color.White;
            this.txt_metraj_gheir_tejari.ForeColor = System.Drawing.Color.Black;
            this.txt_metraj_gheir_tejari.Location = new System.Drawing.Point(30, 91);
            this.txt_metraj_gheir_tejari.Margin = new System.Windows.Forms.Padding(4);
            this.txt_metraj_gheir_tejari.MaxLength = 5;
            this.txt_metraj_gheir_tejari.Name = "txt_metraj_gheir_tejari";
            this.txt_metraj_gheir_tejari.PreventEnterBeep = true;
            this.txt_metraj_gheir_tejari.Size = new System.Drawing.Size(113, 28);
            this.txt_metraj_gheir_tejari.TabIndex = 30;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(623, 55);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(312, 28);
            this.labelX8.TabIndex = 32;
            this.labelX8.Text = "تعداد کل مشترکین ( واحد های تجاری ) :";
            // 
            // txt_moshtarekin_count_tejari
            // 
            this.txt_moshtarekin_count_tejari.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarekin_count_tejari.Border.Class = "TextBoxBorder";
            this.txt_moshtarekin_count_tejari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarekin_count_tejari.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarekin_count_tejari.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarekin_count_tejari.Location = new System.Drawing.Point(609, 55);
            this.txt_moshtarekin_count_tejari.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moshtarekin_count_tejari.MaxLength = 5;
            this.txt_moshtarekin_count_tejari.Name = "txt_moshtarekin_count_tejari";
            this.txt_moshtarekin_count_tejari.PreventEnterBeep = true;
            this.txt_moshtarekin_count_tejari.Size = new System.Drawing.Size(113, 28);
            this.txt_moshtarekin_count_tejari.TabIndex = 31;
            // 
            // txt_moshtarekin_count_gheir_tejari
            // 
            this.txt_moshtarekin_count_gheir_tejari.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarekin_count_gheir_tejari.Border.Class = "TextBoxBorder";
            this.txt_moshtarekin_count_gheir_tejari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarekin_count_gheir_tejari.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarekin_count_gheir_tejari.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarekin_count_gheir_tejari.Location = new System.Drawing.Point(609, 91);
            this.txt_moshtarekin_count_gheir_tejari.Margin = new System.Windows.Forms.Padding(4);
            this.txt_moshtarekin_count_gheir_tejari.MaxLength = 5;
            this.txt_moshtarekin_count_gheir_tejari.Name = "txt_moshtarekin_count_gheir_tejari";
            this.txt_moshtarekin_count_gheir_tejari.PreventEnterBeep = true;
            this.txt_moshtarekin_count_gheir_tejari.Size = new System.Drawing.Size(113, 28);
            this.txt_moshtarekin_count_gheir_tejari.TabIndex = 33;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(623, 91);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(312, 28);
            this.labelX9.TabIndex = 34;
            this.labelX9.Text = "تعداد کل مشترکین ( واحد های غیرتجاری ):";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.dgv_sharj_result);
            this.groupPanel1.Controls.Add(this.labelX17);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(516, 262);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(481, 243);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 30;
            this.groupPanel1.Text = "اطلاعات مشترکین - امور شارژ";
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(964, 16);
            this.labelX17.Margin = new System.Windows.Forms.Padding(4);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(92, 28);
            this.labelX17.TabIndex = 2;
            this.labelX17.Text = "کد قرارداد:";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.dgv_ab_result);
            this.groupPanel2.Controls.Add(this.labelX10);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(13, 262);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(481, 243);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 31;
            this.groupPanel2.Text = "اطلاعات مشترکین - امور آب";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(964, 16);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(92, 28);
            this.labelX10.TabIndex = 2;
            this.labelX10.Text = "کد قرارداد:";
            // 
            // dgv_sharj_result
            // 
            this.dgv_sharj_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sharj_result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_sharj_result.BackgroundColor = System.Drawing.Color.White;
            this.dgv_sharj_result.ColumnHeadersHeight = 30;
            this.dgv_sharj_result.Location = new System.Drawing.Point(14, 15);
            this.dgv_sharj_result.Name = "dgv_sharj_result";
            this.dgv_sharj_result.RowHeadersVisible = false;
            this.dgv_sharj_result.Size = new System.Drawing.Size(440, 196);
            this.dgv_sharj_result.TabIndex = 101;
            // 
            // dgv_ab_result
            // 
            this.dgv_ab_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ab_result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_ab_result.BackgroundColor = System.Drawing.Color.White;
            this.dgv_ab_result.ColumnHeadersHeight = 30;
            this.dgv_ab_result.Location = new System.Drawing.Point(17, 15);
            this.dgv_ab_result.Name = "dgv_ab_result";
            this.dgv_ab_result.RowHeadersVisible = false;
            this.dgv_ab_result.Size = new System.Drawing.Size(440, 196);
            this.dgv_ab_result.TabIndex = 102;
            // 
            // frm_moshtarekin_amar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 518);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.grp_main);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_moshtarekin_amar";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اطلاعات آماری مشترکین";
            this.Load += new System.EventHandler(this.frm_moshtarekin_amar_Load);
            this.grp_main.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_result)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_main;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_metraj_gheir_tejari;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_metraj_tejari;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_metraj_all;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_have_sharj;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_have_ab;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarekin_count;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarekin_count_gheir_tejari;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarekin_count_tejari;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.DataGridView dgv_sharj_result;
        private System.Windows.Forms.DataGridView dgv_ab_result;
    }
}