using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Payam : MyMetroForm
    {
        public Payam()
        {
            InitializeComponent();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public static void Show(string payam)
        {
            Payam frm = new Payam();
            frm.TopMost = true;
            frm.lblpayam.Text = payam;
            frm.ShowDialog();
        }

        private void lblpayam_Click(object sender, EventArgs e)
        {

        }

        private void Payam_Load(object sender, EventArgs e)
        {

        }

        private void lblpayam_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}