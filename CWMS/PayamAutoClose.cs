using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Threading;

namespace CWMS
{
    public partial class PayamAutoClose : DevComponents.DotNetBar.Metro.MetroForm
    {
        public PayamAutoClose(string payam, int milliseconds)
        {
            InitializeComponent();
            this.TopMost = true;
            timer1.Interval = milliseconds;
            this.label1.Text = payam;
            timer1.Start();
            this.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}