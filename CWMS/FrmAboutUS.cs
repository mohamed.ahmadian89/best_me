﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Deployment.Application;
namespace CWMS
{
    public partial class FrmAboutUS : MyMetroForm
    {
        public FrmAboutUS()
        {
            InitializeComponent();
        }

        private void FrmAboutUS_Load(object sender, EventArgs e)
        {
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                lbl_version.Text =
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.Major.ToString() + "." +
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.Minor.ToString() + "." +
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.Build.ToString() + "." +
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.Revision.ToString() ;

            }
        }

        private void labelX1_Click(object sender, EventArgs e)
        {
            
        }

        private void groupPanel3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
