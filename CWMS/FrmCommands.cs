using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class FrmCommands : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmCommands()
        {
            InitializeComponent();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox1.Text.Contains("insert") || textBox1.Text.Contains("delet") || textBox1.Text.Contains("update"))
                {
                    Classes.ClsMain.ExecuteNoneQuery(textBox1.Text);
                    dataGridView1.DataSource = null;
                    this.Text = "Done Command :" + DateTime.Now.ToShortTimeString();
                }
                else
                    dataGridView1.DataSource = Classes.ClsMain.GetDataTable(textBox1.Text);
            }
        }
    }
}