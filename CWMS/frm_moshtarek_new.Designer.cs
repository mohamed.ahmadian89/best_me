﻿namespace CWMS
{
    partial class frm_moshtarek_new
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_moshtarek_new));
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.chsakhtosaz = new System.Windows.Forms.CheckBox();
            this.chtejari = new System.Windows.Forms.CheckBox();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_zamini = new System.Windows.Forms.TextBox();
            this.txt_havayi = new System.Windows.Forms.TextBox();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtKontorRaghan = new DevComponents.Editors.IntegerInput();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txt_zarib_fazelab = new System.Windows.Forms.TextBox();
            this.txt_serial_kontor = new DevComponents.Editors.IntegerInput();
            this.txt_size_ensheab = new DevComponents.Editors.IntegerInput();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.txt_masraf_mojaz = new DevComponents.Editors.IntegerInput();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_karmand = new DevComponents.Editors.IntegerInput();
            this.txt_serayedar = new DevComponents.Editors.IntegerInput();
            this.txt_kargar = new DevComponents.Editors.IntegerInput();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.warningBox1 = new DevComponents.DotNetBar.Controls.WarningBox();
            this.grp_main = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_tarikh_gharar_ab = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.txt_sh_va_ab = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.txt_sh_variz_sharj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.txt_parvandeh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_posti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_Eghtesadi = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_melli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.txt_metraj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtghataat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txtPelak = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.txt_tarikh = new FarsiLibrary.Win.Controls.FADatePicker();
            this.rd_gher_fal = new System.Windows.Forms.RadioButton();
            this.rd_faal = new System.Windows.Forms.RadioButton();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.txt_address = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_phone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_mobile = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_modir_amel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_co_name = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKontorRaghan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_serial_kontor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_size_ensheab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_masraf_mojaz)).BeginInit();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_karmand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_serayedar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_kargar)).BeginInit();
            this.grp_main.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.Location = new System.Drawing.Point(50, 446);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Size = new System.Drawing.Size(183, 28);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 31;
            this.btn_save.Text = "ثبت اطلاعات";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.chsakhtosaz);
            this.groupPanel1.Controls.Add(this.chtejari);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(275, 443);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(859, 43);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 37;
            // 
            // chsakhtosaz
            // 
            this.chsakhtosaz.AutoSize = true;
            this.chsakhtosaz.BackColor = System.Drawing.Color.Transparent;
            this.chsakhtosaz.ForeColor = System.Drawing.Color.Black;
            this.chsakhtosaz.Location = new System.Drawing.Point(282, 4);
            this.chsakhtosaz.Margin = new System.Windows.Forms.Padding(4);
            this.chsakhtosaz.Name = "chsakhtosaz";
            this.chsakhtosaz.Size = new System.Drawing.Size(225, 25);
            this.chsakhtosaz.TabIndex = 1;
            this.chsakhtosaz.Text = "این مشترک در حال ساخت و ساز است ";
            this.chsakhtosaz.UseVisualStyleBackColor = false;
            // 
            // chtejari
            // 
            this.chtejari.AutoSize = true;
            this.chtejari.BackColor = System.Drawing.Color.Transparent;
            this.chtejari.ForeColor = System.Drawing.Color.Black;
            this.chtejari.Location = new System.Drawing.Point(569, 4);
            this.chtejari.Margin = new System.Windows.Forms.Padding(4);
            this.chtejari.Name = "chtejari";
            this.chtejari.Size = new System.Drawing.Size(215, 25);
            this.chtejari.TabIndex = 0;
            this.chtejari.Text = "این مشترک دارای واحد تجاری است ";
            this.chtejari.UseVisualStyleBackColor = false;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.txt_zamini);
            this.groupPanel3.Controls.Add(this.txt_havayi);
            this.groupPanel3.Controls.Add(this.labelX23);
            this.groupPanel3.Controls.Add(this.labelX24);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(44, 270);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(527, 79);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 35;
            this.groupPanel3.Text = "حجم مخازن ";
            // 
            // txt_zamini
            // 
            this.txt_zamini.BackColor = System.Drawing.Color.White;
            this.txt_zamini.ForeColor = System.Drawing.Color.Black;
            this.txt_zamini.Location = new System.Drawing.Point(76, 10);
            this.txt_zamini.Margin = new System.Windows.Forms.Padding(4);
            this.txt_zamini.Name = "txt_zamini";
            this.txt_zamini.Size = new System.Drawing.Size(93, 28);
            this.txt_zamini.TabIndex = 14;
            this.txt_zamini.Text = "0";
            // 
            // txt_havayi
            // 
            this.txt_havayi.BackColor = System.Drawing.Color.White;
            this.txt_havayi.ForeColor = System.Drawing.Color.Black;
            this.txt_havayi.Location = new System.Drawing.Point(301, 10);
            this.txt_havayi.Margin = new System.Windows.Forms.Padding(4);
            this.txt_havayi.Name = "txt_havayi";
            this.txt_havayi.Size = new System.Drawing.Size(93, 28);
            this.txt_havayi.TabIndex = 1;
            this.txt_havayi.Text = "0";
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(166, 11);
            this.labelX23.Margin = new System.Windows.Forms.Padding(4);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(52, 28);
            this.labelX23.TabIndex = 21;
            this.labelX23.Text = "زمینی :";
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(385, 11);
            this.labelX24.Margin = new System.Windows.Forms.Padding(4);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(56, 28);
            this.labelX24.TabIndex = 19;
            this.labelX24.Text = "هوایی:";
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.txtKontorRaghan);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.txt_zarib_fazelab);
            this.groupPanel4.Controls.Add(this.txt_serial_kontor);
            this.groupPanel4.Controls.Add(this.txt_size_ensheab);
            this.groupPanel4.Controls.Add(this.labelX21);
            this.groupPanel4.Controls.Add(this.txt_masraf_mojaz);
            this.groupPanel4.Controls.Add(this.labelX20);
            this.groupPanel4.Controls.Add(this.labelX14);
            this.groupPanel4.Controls.Add(this.labelX15);
            this.groupPanel4.Controls.Add(this.labelX16);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(44, 357);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(1090, 79);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 36;
            this.groupPanel4.Text = "اطلاعات تکمیلی";
            // 
            // txtKontorRaghan
            // 
            this.txtKontorRaghan.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtKontorRaghan.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtKontorRaghan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtKontorRaghan.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtKontorRaghan.ForeColor = System.Drawing.Color.Black;
            this.txtKontorRaghan.Location = new System.Drawing.Point(208, 7);
            this.txtKontorRaghan.Margin = new System.Windows.Forms.Padding(4);
            this.txtKontorRaghan.MaxValue = 99999;
            this.txtKontorRaghan.Name = "txtKontorRaghan";
            this.txtKontorRaghan.ShowUpDown = true;
            this.txtKontorRaghan.Size = new System.Drawing.Size(92, 28);
            this.txtKontorRaghan.TabIndex = 27;
            this.txtKontorRaghan.Value = 9999;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(301, 10);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(102, 25);
            this.labelX9.TabIndex = 28;
            this.labelX9.Text = "آخرین رقم کنتور";
            // 
            // txt_zarib_fazelab
            // 
            this.txt_zarib_fazelab.BackColor = System.Drawing.Color.White;
            this.txt_zarib_fazelab.ForeColor = System.Drawing.Color.Black;
            this.txt_zarib_fazelab.Location = new System.Drawing.Point(4, 7);
            this.txt_zarib_fazelab.Margin = new System.Windows.Forms.Padding(4);
            this.txt_zarib_fazelab.Name = "txt_zarib_fazelab";
            this.txt_zarib_fazelab.Size = new System.Drawing.Size(93, 28);
            this.txt_zarib_fazelab.TabIndex = 18;
            this.txt_zarib_fazelab.Text = "0";
            // 
            // txt_serial_kontor
            // 
            this.txt_serial_kontor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_serial_kontor.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_serial_kontor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_serial_kontor.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_serial_kontor.ForeColor = System.Drawing.Color.Black;
            this.txt_serial_kontor.Location = new System.Drawing.Point(415, 7);
            this.txt_serial_kontor.Margin = new System.Windows.Forms.Padding(4);
            this.txt_serial_kontor.Name = "txt_serial_kontor";
            this.txt_serial_kontor.ShowUpDown = true;
            this.txt_serial_kontor.Size = new System.Drawing.Size(92, 28);
            this.txt_serial_kontor.TabIndex = 17;
            // 
            // txt_size_ensheab
            // 
            this.txt_size_ensheab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_size_ensheab.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_size_ensheab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_size_ensheab.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_size_ensheab.ForeColor = System.Drawing.Color.Black;
            this.txt_size_ensheab.Location = new System.Drawing.Point(620, 7);
            this.txt_size_ensheab.Margin = new System.Windows.Forms.Padding(4);
            this.txt_size_ensheab.Name = "txt_size_ensheab";
            this.txt_size_ensheab.ShowUpDown = true;
            this.txt_size_ensheab.Size = new System.Drawing.Size(92, 28);
            this.txt_size_ensheab.TabIndex = 16;
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.Font = new System.Drawing.Font("B Yekan", 7F);
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(818, 8);
            this.labelX21.Margin = new System.Windows.Forms.Padding(4);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(69, 28);
            this.labelX21.TabIndex = 26;
            this.labelX21.Text = "مترمکعب";
            // 
            // txt_masraf_mojaz
            // 
            this.txt_masraf_mojaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_masraf_mojaz.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_masraf_mojaz.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_masraf_mojaz.ButtonFreeText.Checked = true;
            this.txt_masraf_mojaz.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_masraf_mojaz.ForeColor = System.Drawing.Color.Black;
            this.txt_masraf_mojaz.FreeTextEntryMode = true;
            this.txt_masraf_mojaz.Location = new System.Drawing.Point(892, 7);
            this.txt_masraf_mojaz.Margin = new System.Windows.Forms.Padding(4);
            this.txt_masraf_mojaz.Name = "txt_masraf_mojaz";
            this.txt_masraf_mojaz.ShowUpDown = true;
            this.txt_masraf_mojaz.Size = new System.Drawing.Size(76, 28);
            this.txt_masraf_mojaz.TabIndex = 15;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(107, 10);
            this.labelX20.Margin = new System.Windows.Forms.Padding(4);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(92, 25);
            this.labelX20.TabIndex = 25;
            this.labelX20.Text = "ضریب فاضلاب :";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(516, 10);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(82, 25);
            this.labelX14.TabIndex = 23;
            this.labelX14.Text = "سریال کنتور:";
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(720, 10);
            this.labelX15.Margin = new System.Windows.Forms.Padding(4);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(82, 25);
            this.labelX15.TabIndex = 21;
            this.labelX15.Text = "سایز انشعاب :";
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(976, 10);
            this.labelX16.Margin = new System.Windows.Forms.Padding(4);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(82, 25);
            this.labelX16.TabIndex = 19;
            this.labelX16.Text = "مصرف مجاز :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_karmand);
            this.groupPanel2.Controls.Add(this.txt_serayedar);
            this.groupPanel2.Controls.Add(this.txt_kargar);
            this.groupPanel2.Controls.Add(this.labelX17);
            this.groupPanel2.Controls.Add(this.labelX18);
            this.groupPanel2.Controls.Add(this.labelX19);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(597, 271);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(538, 79);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 34;
            this.groupPanel2.Text = "تعداد پرسنل ها";
            // 
            // txt_karmand
            // 
            this.txt_karmand.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_karmand.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_karmand.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_karmand.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_karmand.ForeColor = System.Drawing.Color.Black;
            this.txt_karmand.Location = new System.Drawing.Point(37, 9);
            this.txt_karmand.Margin = new System.Windows.Forms.Padding(4);
            this.txt_karmand.Name = "txt_karmand";
            this.txt_karmand.ShowUpDown = true;
            this.txt_karmand.Size = new System.Drawing.Size(76, 28);
            this.txt_karmand.TabIndex = 12;
            // 
            // txt_serayedar
            // 
            this.txt_serayedar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_serayedar.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_serayedar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_serayedar.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_serayedar.ForeColor = System.Drawing.Color.Black;
            this.txt_serayedar.Location = new System.Drawing.Point(177, 9);
            this.txt_serayedar.Margin = new System.Windows.Forms.Padding(4);
            this.txt_serayedar.Name = "txt_serayedar";
            this.txt_serayedar.ShowUpDown = true;
            this.txt_serayedar.Size = new System.Drawing.Size(76, 28);
            this.txt_serayedar.TabIndex = 12;
            // 
            // txt_kargar
            // 
            this.txt_kargar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kargar.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txt_kargar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kargar.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txt_kargar.ForeColor = System.Drawing.Color.Black;
            this.txt_kargar.Location = new System.Drawing.Point(348, 9);
            this.txt_kargar.Margin = new System.Windows.Forms.Padding(4);
            this.txt_kargar.Name = "txt_kargar";
            this.txt_kargar.ShowUpDown = true;
            this.txt_kargar.Size = new System.Drawing.Size(76, 28);
            this.txt_kargar.TabIndex = 10;
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(96, 10);
            this.labelX17.Margin = new System.Windows.Forms.Padding(4);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(73, 28);
            this.labelX17.TabIndex = 23;
            this.labelX17.Text = "کارمندی:";
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(157, 10);
            this.labelX18.Margin = new System.Windows.Forms.Padding(4);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(183, 28);
            this.labelX18.TabIndex = 21;
            this.labelX18.Text = "سرایه داری :";
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(348, 10);
            this.labelX19.Margin = new System.Windows.Forms.Padding(4);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(136, 28);
            this.labelX19.TabIndex = 19;
            this.labelX19.Text = "کارگری:";
            // 
            // warningBox1
            // 
            this.warningBox1.AutoSize = true;
            this.warningBox1.BackColor = System.Drawing.Color.White;
            this.warningBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.warningBox1.CloseButtonVisible = false;
            this.warningBox1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.warningBox1.ForeColor = System.Drawing.Color.Black;
            this.warningBox1.Image = ((System.Drawing.Image)(resources.GetObject("warningBox1.Image")));
            this.warningBox1.Location = new System.Drawing.Point(45, 494);
            this.warningBox1.Margin = new System.Windows.Forms.Padding(4);
            this.warningBox1.Name = "warningBox1";
            this.warningBox1.OptionsButtonVisible = false;
            this.warningBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.warningBox1.Size = new System.Drawing.Size(1090, 41);
            this.warningBox1.TabIndex = 33;
            this.warningBox1.Visible = false;
            // 
            // grp_main
            // 
            this.grp_main.BackColor = System.Drawing.Color.White;
            this.grp_main.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_main.Controls.Add(this.txt_tarikh_gharar_ab);
            this.grp_main.Controls.Add(this.labelX30);
            this.grp_main.Controls.Add(this.txt_sh_va_ab);
            this.grp_main.Controls.Add(this.labelX29);
            this.grp_main.Controls.Add(this.txt_sh_variz_sharj);
            this.grp_main.Controls.Add(this.labelX27);
            this.grp_main.Controls.Add(this.txt_parvandeh);
            this.grp_main.Controls.Add(this.labelX28);
            this.grp_main.Controls.Add(this.txt_code_posti);
            this.grp_main.Controls.Add(this.labelX26);
            this.grp_main.Controls.Add(this.txt_code_Eghtesadi);
            this.grp_main.Controls.Add(this.labelX25);
            this.grp_main.Controls.Add(this.txt_code_melli);
            this.grp_main.Controls.Add(this.labelX11);
            this.grp_main.Controls.Add(this.txt_metraj);
            this.grp_main.Controls.Add(this.txtghataat);
            this.grp_main.Controls.Add(this.labelX10);
            this.grp_main.Controls.Add(this.txtPelak);
            this.grp_main.Controls.Add(this.labelX22);
            this.grp_main.Controls.Add(this.txt_tarikh);
            this.grp_main.Controls.Add(this.rd_gher_fal);
            this.grp_main.Controls.Add(this.rd_faal);
            this.grp_main.Controls.Add(this.labelX13);
            this.grp_main.Controls.Add(this.txt_address);
            this.grp_main.Controls.Add(this.labelX12);
            this.grp_main.Controls.Add(this.labelX8);
            this.grp_main.Controls.Add(this.labelX7);
            this.grp_main.Controls.Add(this.labelX6);
            this.grp_main.Controls.Add(this.txt_phone);
            this.grp_main.Controls.Add(this.labelX5);
            this.grp_main.Controls.Add(this.txt_mobile);
            this.grp_main.Controls.Add(this.labelX4);
            this.grp_main.Controls.Add(this.txt_modir_amel);
            this.grp_main.Controls.Add(this.labelX2);
            this.grp_main.Controls.Add(this.txt_co_name);
            this.grp_main.Controls.Add(this.labelX1);
            this.grp_main.Controls.Add(this.txt_gharardad);
            this.grp_main.Controls.Add(this.labelX3);
            this.grp_main.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_main.Location = new System.Drawing.Point(45, 15);
            this.grp_main.Margin = new System.Windows.Forms.Padding(4);
            this.grp_main.Name = "grp_main";
            this.grp_main.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_main.Size = new System.Drawing.Size(1090, 248);
            // 
            // 
            // 
            this.grp_main.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_main.Style.BackColorGradientAngle = 90;
            this.grp_main.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_main.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderBottomWidth = 1;
            this.grp_main.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_main.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderLeftWidth = 1;
            this.grp_main.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderRightWidth = 1;
            this.grp_main.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderTopWidth = 1;
            this.grp_main.Style.CornerDiameter = 4;
            this.grp_main.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_main.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_main.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_main.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_main.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_main.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_main.TabIndex = 28;
            this.grp_main.Text = "اطلاعات اصلی";
            // 
            // txt_tarikh_gharar_ab
            // 
            this.txt_tarikh_gharar_ab.Location = new System.Drawing.Point(33, 118);
            this.txt_tarikh_gharar_ab.Name = "txt_tarikh_gharar_ab";
            this.txt_tarikh_gharar_ab.Readonly = true;
            this.txt_tarikh_gharar_ab.ShowEmptyButton = false;
            this.txt_tarikh_gharar_ab.Size = new System.Drawing.Size(182, 25);
            this.txt_tarikh_gharar_ab.TabIndex = 43;
            this.txt_tarikh_gharar_ab.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX30
            // 
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.ForeColor = System.Drawing.Color.Black;
            this.labelX30.Location = new System.Drawing.Point(227, 118);
            this.labelX30.Margin = new System.Windows.Forms.Padding(4);
            this.labelX30.Name = "labelX30";
            this.labelX30.Size = new System.Drawing.Size(92, 28);
            this.labelX30.TabIndex = 42;
            this.labelX30.Text = "تاریخ قرارداد آب:";
            // 
            // txt_sh_va_ab
            // 
            this.txt_sh_va_ab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sh_va_ab.Border.Class = "TextBoxBorder";
            this.txt_sh_va_ab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sh_va_ab.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sh_va_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_sh_va_ab.Location = new System.Drawing.Point(399, 190);
            this.txt_sh_va_ab.MaxLength = 0;
            this.txt_sh_va_ab.Name = "txt_sh_va_ab";
            this.txt_sh_va_ab.PreventEnterBeep = true;
            this.txt_sh_va_ab.Size = new System.Drawing.Size(135, 28);
            this.txt_sh_va_ab.TabIndex = 40;
            // 
            // labelX29
            // 
            this.labelX29.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.ForeColor = System.Drawing.Color.Black;
            this.labelX29.Location = new System.Drawing.Point(533, 192);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(95, 23);
            this.labelX29.TabIndex = 41;
            this.labelX29.Text = "شناسه واریز آب:";
            // 
            // txt_sh_variz_sharj
            // 
            this.txt_sh_variz_sharj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sh_variz_sharj.Border.Class = "TextBoxBorder";
            this.txt_sh_variz_sharj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sh_variz_sharj.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sh_variz_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_sh_variz_sharj.Location = new System.Drawing.Point(640, 190);
            this.txt_sh_variz_sharj.MaxLength = 0;
            this.txt_sh_variz_sharj.Name = "txt_sh_variz_sharj";
            this.txt_sh_variz_sharj.PreventEnterBeep = true;
            this.txt_sh_variz_sharj.Size = new System.Drawing.Size(97, 28);
            this.txt_sh_variz_sharj.TabIndex = 38;
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(746, 193);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(99, 23);
            this.labelX27.TabIndex = 39;
            this.labelX27.Text = "شناسه واریز شارژ:";
            // 
            // txt_parvandeh
            // 
            this.txt_parvandeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_parvandeh.Border.Class = "TextBoxBorder";
            this.txt_parvandeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_parvandeh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_parvandeh.ForeColor = System.Drawing.Color.Black;
            this.txt_parvandeh.Location = new System.Drawing.Point(851, 190);
            this.txt_parvandeh.MaxLength = 11;
            this.txt_parvandeh.Name = "txt_parvandeh";
            this.txt_parvandeh.PreventEnterBeep = true;
            this.txt_parvandeh.Size = new System.Drawing.Size(94, 28);
            this.txt_parvandeh.TabIndex = 36;
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(964, 193);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(81, 23);
            this.labelX28.TabIndex = 37;
            this.labelX28.Text = "شماره پرونده :";
            // 
            // txt_code_posti
            // 
            this.txt_code_posti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_posti.Border.Class = "TextBoxBorder";
            this.txt_code_posti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_posti.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_posti.ForeColor = System.Drawing.Color.Black;
            this.txt_code_posti.Location = new System.Drawing.Point(33, 158);
            this.txt_code_posti.MaxLength = 14;
            this.txt_code_posti.Name = "txt_code_posti";
            this.txt_code_posti.PreventEnterBeep = true;
            this.txt_code_posti.Size = new System.Drawing.Size(135, 28);
            this.txt_code_posti.TabIndex = 34;
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(153, 161);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(67, 23);
            this.labelX26.TabIndex = 35;
            this.labelX26.Text = "کدپستی:";
            // 
            // txt_code_Eghtesadi
            // 
            this.txt_code_Eghtesadi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_Eghtesadi.Border.Class = "TextBoxBorder";
            this.txt_code_Eghtesadi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_Eghtesadi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_Eghtesadi.ForeColor = System.Drawing.Color.Black;
            this.txt_code_Eghtesadi.Location = new System.Drawing.Point(242, 158);
            this.txt_code_Eghtesadi.MaxLength = 14;
            this.txt_code_Eghtesadi.Name = "txt_code_Eghtesadi";
            this.txt_code_Eghtesadi.PreventEnterBeep = true;
            this.txt_code_Eghtesadi.Size = new System.Drawing.Size(135, 28);
            this.txt_code_Eghtesadi.TabIndex = 32;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(374, 161);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(67, 23);
            this.labelX25.TabIndex = 33;
            this.labelX25.Text = "کداقتصادی:";
            // 
            // txt_code_melli
            // 
            this.txt_code_melli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_melli.Border.Class = "TextBoxBorder";
            this.txt_code_melli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_melli.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_melli.ForeColor = System.Drawing.Color.Black;
            this.txt_code_melli.Location = new System.Drawing.Point(447, 158);
            this.txt_code_melli.MaxLength = 14;
            this.txt_code_melli.Name = "txt_code_melli";
            this.txt_code_melli.PreventEnterBeep = true;
            this.txt_code_melli.Size = new System.Drawing.Size(135, 28);
            this.txt_code_melli.TabIndex = 30;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(563, 161);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(67, 23);
            this.labelX11.TabIndex = 31;
            this.labelX11.Text = "کدملی:";
            // 
            // txt_metraj
            // 
            this.txt_metraj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_metraj.Border.Class = "TextBoxBorder";
            this.txt_metraj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_metraj.DisabledBackColor = System.Drawing.Color.White;
            this.txt_metraj.ForeColor = System.Drawing.Color.Black;
            this.txt_metraj.Location = new System.Drawing.Point(453, 84);
            this.txt_metraj.MaxLength = 11;
            this.txt_metraj.Name = "txt_metraj";
            this.txt_metraj.PreventEnterBeep = true;
            this.txt_metraj.Size = new System.Drawing.Size(71, 28);
            this.txt_metraj.TabIndex = 29;
            // 
            // txtghataat
            // 
            this.txtghataat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtghataat.Border.Class = "TextBoxBorder";
            this.txtghataat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtghataat.DisabledBackColor = System.Drawing.Color.White;
            this.txtghataat.ForeColor = System.Drawing.Color.Black;
            this.txtghataat.Location = new System.Drawing.Point(638, 158);
            this.txtghataat.MaxLength = 11;
            this.txtghataat.Name = "txtghataat";
            this.txtghataat.PreventEnterBeep = true;
            this.txtghataat.Size = new System.Drawing.Size(97, 28);
            this.txtghataat.TabIndex = 24;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(769, 161);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(67, 23);
            this.labelX10.TabIndex = 25;
            this.labelX10.Text = "قطعات :";
            // 
            // txtPelak
            // 
            this.txtPelak.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPelak.Border.Class = "TextBoxBorder";
            this.txtPelak.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPelak.DisabledBackColor = System.Drawing.Color.White;
            this.txtPelak.ForeColor = System.Drawing.Color.Black;
            this.txtPelak.Location = new System.Drawing.Point(851, 158);
            this.txtPelak.MaxLength = 11;
            this.txtPelak.Name = "txtPelak";
            this.txtPelak.PreventEnterBeep = true;
            this.txtPelak.Size = new System.Drawing.Size(94, 28);
            this.txtPelak.TabIndex = 22;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(978, 161);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(67, 23);
            this.labelX22.TabIndex = 23;
            this.labelX22.Text = "پلاک :";
            // 
            // txt_tarikh
            // 
            this.txt_tarikh.Location = new System.Drawing.Point(33, 87);
            this.txt_tarikh.Name = "txt_tarikh";
            this.txt_tarikh.Readonly = true;
            this.txt_tarikh.ShowEmptyButton = false;
            this.txt_tarikh.Size = new System.Drawing.Size(182, 25);
            this.txt_tarikh.TabIndex = 20;
            this.txt_tarikh.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // rd_gher_fal
            // 
            this.rd_gher_fal.AutoSize = true;
            this.rd_gher_fal.BackColor = System.Drawing.Color.Transparent;
            this.rd_gher_fal.ForeColor = System.Drawing.Color.Black;
            this.rd_gher_fal.Location = new System.Drawing.Point(798, 88);
            this.rd_gher_fal.Margin = new System.Windows.Forms.Padding(4);
            this.rd_gher_fal.Name = "rd_gher_fal";
            this.rd_gher_fal.Size = new System.Drawing.Size(72, 25);
            this.rd_gher_fal.TabIndex = 6;
            this.rd_gher_fal.Tag = "gfds";
            this.rd_gher_fal.Text = "غیر فعال";
            this.rd_gher_fal.UseVisualStyleBackColor = false;
            // 
            // rd_faal
            // 
            this.rd_faal.AutoSize = true;
            this.rd_faal.BackColor = System.Drawing.Color.Transparent;
            this.rd_faal.Checked = true;
            this.rd_faal.ForeColor = System.Drawing.Color.Black;
            this.rd_faal.Location = new System.Drawing.Point(883, 88);
            this.rd_faal.Margin = new System.Windows.Forms.Padding(4);
            this.rd_faal.Name = "rd_faal";
            this.rd_faal.Size = new System.Drawing.Size(52, 25);
            this.rd_faal.TabIndex = 5;
            this.rd_faal.TabStop = true;
            this.rd_faal.Tag = "gfds";
            this.rd_faal.Text = "فعال";
            this.rd_faal.UseVisualStyleBackColor = false;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(994, 87);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(51, 28);
            this.labelX13.TabIndex = 19;
            this.labelX13.Text = "وضعیت :";
            // 
            // txt_address
            // 
            this.txt_address.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_address.Border.Class = "TextBoxBorder";
            this.txt_address.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_address.DisabledBackColor = System.Drawing.Color.White;
            this.txt_address.ForeColor = System.Drawing.Color.Black;
            this.txt_address.Location = new System.Drawing.Point(331, 123);
            this.txt_address.Margin = new System.Windows.Forms.Padding(4);
            this.txt_address.Name = "txt_address";
            this.txt_address.PreventEnterBeep = true;
            this.txt_address.Size = new System.Drawing.Size(614, 28);
            this.txt_address.TabIndex = 9;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(953, 124);
            this.labelX12.Margin = new System.Windows.Forms.Padding(4);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(92, 28);
            this.labelX12.TabIndex = 18;
            this.labelX12.Text = "آدرس:";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(348, 87);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(69, 28);
            this.labelX8.TabIndex = 16;
            this.labelX8.Text = "مترمربع";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(227, 87);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(92, 28);
            this.labelX7.TabIndex = 15;
            this.labelX7.Text = "تاریخ قرارداد:";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(531, 87);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(51, 28);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "متراژ:";
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_phone.Border.Class = "TextBoxBorder";
            this.txt_phone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_phone.DisabledBackColor = System.Drawing.Color.White;
            this.txt_phone.ForeColor = System.Drawing.Color.Black;
            this.txt_phone.Location = new System.Drawing.Point(33, 54);
            this.txt_phone.Margin = new System.Windows.Forms.Padding(4);
            this.txt_phone.MaxLength = 11;
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.PreventEnterBeep = true;
            this.txt_phone.Size = new System.Drawing.Size(186, 28);
            this.txt_phone.TabIndex = 4;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(227, 55);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(92, 28);
            this.labelX5.TabIndex = 10;
            this.labelX5.Text = "تلفن ثابت :";
            // 
            // txt_mobile
            // 
            this.txt_mobile.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mobile.Border.Class = "TextBoxBorder";
            this.txt_mobile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mobile.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mobile.ForeColor = System.Drawing.Color.Black;
            this.txt_mobile.Location = new System.Drawing.Point(337, 54);
            this.txt_mobile.Margin = new System.Windows.Forms.Padding(4);
            this.txt_mobile.MaxLength = 11;
            this.txt_mobile.Name = "txt_mobile";
            this.txt_mobile.PreventEnterBeep = true;
            this.txt_mobile.Size = new System.Drawing.Size(186, 28);
            this.txt_mobile.TabIndex = 3;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(513, 55);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(92, 28);
            this.labelX4.TabIndex = 8;
            this.labelX4.Text = "تلفن همراه :";
            // 
            // txt_modir_amel
            // 
            this.txt_modir_amel.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_modir_amel.Border.Class = "TextBoxBorder";
            this.txt_modir_amel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_modir_amel.DisabledBackColor = System.Drawing.Color.White;
            this.txt_modir_amel.ForeColor = System.Drawing.Color.Black;
            this.txt_modir_amel.Location = new System.Drawing.Point(649, 54);
            this.txt_modir_amel.Margin = new System.Windows.Forms.Padding(4);
            this.txt_modir_amel.Name = "txt_modir_amel";
            this.txt_modir_amel.PreventEnterBeep = true;
            this.txt_modir_amel.Size = new System.Drawing.Size(296, 28);
            this.txt_modir_amel.TabIndex = 2;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(953, 55);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(92, 28);
            this.labelX2.TabIndex = 6;
            this.labelX2.Text = "مدیریت عامل :";
            // 
            // txt_co_name
            // 
            this.txt_co_name.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_co_name.Border.Class = "TextBoxBorder";
            this.txt_co_name.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_co_name.DisabledBackColor = System.Drawing.Color.White;
            this.txt_co_name.ForeColor = System.Drawing.Color.Black;
            this.txt_co_name.Location = new System.Drawing.Point(33, 19);
            this.txt_co_name.Margin = new System.Windows.Forms.Padding(4);
            this.txt_co_name.Name = "txt_co_name";
            this.txt_co_name.PreventEnterBeep = true;
            this.txt_co_name.Size = new System.Drawing.Size(490, 28);
            this.txt_co_name.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(516, 20);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(301, 28);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "نام مشترک ( کارخانه / واحد تولیدی / واحد تجاری ) :";
            // 
            // txt_gharardad
            // 
            this.txt_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_gharardad.Border.Class = "TextBoxBorder";
            this.txt_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_gharardad.Location = new System.Drawing.Point(832, 19);
            this.txt_gharardad.Margin = new System.Windows.Forms.Padding(4);
            this.txt_gharardad.MaxLength = 5;
            this.txt_gharardad.Name = "txt_gharardad";
            this.txt_gharardad.PreventEnterBeep = true;
            this.txt_gharardad.Size = new System.Drawing.Size(113, 28);
            this.txt_gharardad.TabIndex = 0;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(953, 20);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(92, 28);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "کد قرارداد:";
            // 
            // frm_moshtarek_new
            // 
            this.AcceptButton = this.btn_save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 538);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.warningBox1);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.grp_main);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_moshtarek_new";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmNewMoshtarekin_FormClosed);
            this.Load += new System.EventHandler(this.FrmNewMoshtarekin_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKontorRaghan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_serial_kontor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_size_ensheab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_masraf_mojaz)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_karmand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_serayedar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_kargar)).EndInit();
            this.grp_main.ResumeLayout(false);
            this.grp_main.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_main;
        private FarsiLibrary.Win.Controls.FADatePicker txt_tarikh;
        private System.Windows.Forms.RadioButton rd_gher_fal;
        private System.Windows.Forms.RadioButton rd_faal;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_address;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_phone;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mobile;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_modir_amel;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_co_name;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_gharardad;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.TextBox txt_zamini;
        private System.Windows.Forms.TextBox txt_havayi;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private System.Windows.Forms.TextBox txt_zarib_fazelab;
        private DevComponents.Editors.IntegerInput txt_serial_kontor;
        private DevComponents.Editors.IntegerInput txt_size_ensheab;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.Editors.IntegerInput txt_masraf_mojaz;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.Editors.IntegerInput txt_karmand;
        private DevComponents.Editors.IntegerInput txt_serayedar;
        private DevComponents.Editors.IntegerInput txt_kargar;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.WarningBox warningBox1;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.CheckBox chsakhtosaz;
        private System.Windows.Forms.CheckBox chtejari;
        private DevComponents.Editors.IntegerInput txtKontorRaghan;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtghataat;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPelak;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_metraj;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_posti;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_Eghtesadi;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_melli;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sh_variz_sharj;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_parvandeh;
        private DevComponents.DotNetBar.LabelX labelX28;
        private FarsiLibrary.Win.Controls.FADatePicker txt_tarikh_gharar_ab;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sh_va_ab;
        private DevComponents.DotNetBar.LabelX labelX29;
    }
}