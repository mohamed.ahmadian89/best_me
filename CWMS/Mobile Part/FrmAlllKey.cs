﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Mobile_Part
{
    public partial class FrmAlllKey : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmAlllKey()
        {
            InitializeComponent();
        }

        private void FrmAlllKey_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mobileDataSet.activation_key' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'mainDataSest.activation_key' table. You can move, or remove it, as needed.


        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (txtfindMoshtarek.Text.Trim() != "")
            {
                try
                {
                    grpkey.Text = "نمایش کلیدهای امنیتی مربوط به مشترک با قرارداد" + ":" + txtfindMoshtarek.Text;
                    this.activation_keyTableAdapter.FillBygharardad(this.mobileDataSet.activation_key, Convert.ToInt32(txtfindMoshtarek.Text));
                    dgvAll.DataSource = mobileDataSet.activation_key;
                    DataView dv = new DataView(mobileDataSet.activation_key);
                    dv.RowFilter = "active=1 and gharardad=" + txtfindMoshtarek.Text;
                    dgvfaal.DataSource = dv.ToTable();
                    dv.RowFilter = "active=0 and gharardad=" + txtfindMoshtarek.Text;
                    dgvGheirFaal.DataSource = dv.ToTable();
                    dv.RowFilter = "expired=1 and gharardad=" + txtfindMoshtarek.Text;

                    dgvUsed.DataSource = dv.ToTable();

                    txt_all_key.Text = dgvAll.Rows.Count.ToString();
                    txt_key_fal.Text = dgvfaal.Rows.Count.ToString();
                    txt_key_gheir_faal.Text = dgvGheirFaal.Rows.Count.ToString();
                    txt_key_used.Text = dgvUsed.Rows.Count.ToString();
                }
                catch
                {
                    Payam.Show("شماره قرارداد نامعتبرمی باشد. کلیه کلیدها نمایش داده می شوند");
                    btnAll.PerformClick();
                }
            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            grpkey.Text = "نمایش کلیه کلیدهای امنیتی ثبت شده در سیستم";
            this.activation_keyTableAdapter.Fill(this.mobileDataSet.activation_key);

            DataView dv = new DataView(mobileDataSet.activation_key);
            dv.RowFilter = "active=1";
            dgvfaal.DataSource = dv.ToTable();
            dv.RowFilter = "active=0";
            dgvGheirFaal.DataSource = dv.ToTable();
            dv.RowFilter = "expired=1";

            dgvUsed.DataSource = dv.ToTable();

            txt_all_key.Text = dgvAll.Rows.Count.ToString();
            txt_key_fal.Text = dgvfaal.Rows.Count.ToString();
            txt_key_gheir_faal.Text = dgvGheirFaal.Rows.Count.ToString();
            txt_key_used.Text = dgvUsed.Rows.Count.ToString();


        }

        private void txtfindMoshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFind.PerformClick();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p88)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 88);

                activationkeyBindingSource.EndEdit();
                activation_keyTableAdapter.Update(mobileDataSet.activation_key);
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if(dgvAll.SelectedRows.Count!=0)
            {
                CrysReports.mobile report1 = new CrysReports.mobile();
                report1.SetDataSource(Classes.ClsMain.ShahrakSetting_data_table.CopyToDataTable());
                report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
                report1.SetParameterValue("gharardad", dgvAll.SelectedRows[0].Cells[0].Value.ToString());
                report1.SetParameterValue("active_key", dgvAll.SelectedRows[0].Cells[1].Value.ToString());
                DataTable moshtarek_info = Classes.ClsMain.MoshtarekDT.Where(p => p.gharardad == Convert.ToInt32(dgvAll.SelectedRows[0].Cells[0].Value.ToString())).CopyToDataTable();
                report1.SetParameterValue("co_name", moshtarek_info.Rows[0]["co_name"].ToString());
                
                new FrmShowReport(report1).ShowDialog();
            }
        }
    }
}