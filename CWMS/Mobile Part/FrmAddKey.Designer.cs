﻿namespace CWMS.Mobile_Part
{
    partial class FrmAddKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnFind = new DevComponents.DotNetBar.ButtonX();
            this.txtfindMoshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.grp_key = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtKey = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtMoshtarekName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnCreate = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1.SuspendLayout();
            this.grp_key.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btnFind);
            this.groupPanel1.Controls.Add(this.txtfindMoshtarek);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(16, 8);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1015, 80);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "انتخاب مشترک ";
            // 
            // btnFind
            // 
            this.btnFind.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFind.BackColor = System.Drawing.Color.Transparent;
            this.btnFind.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFind.Location = new System.Drawing.Point(515, 13);
            this.btnFind.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnFind.Name = "btnFind";
            this.btnFind.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnFind.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnFind.Size = new System.Drawing.Size(52, 28);
            this.btnFind.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFind.Symbol = "";
            this.btnFind.SymbolColor = System.Drawing.Color.Green;
            this.btnFind.SymbolSize = 12F;
            this.btnFind.TabIndex = 12;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtfindMoshtarek
            // 
            this.txtfindMoshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfindMoshtarek.Border.Class = "TextBoxBorder";
            this.txtfindMoshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfindMoshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txtfindMoshtarek.ForeColor = System.Drawing.Color.Black;
            this.txtfindMoshtarek.Location = new System.Drawing.Point(590, 13);
            this.txtfindMoshtarek.Margin = new System.Windows.Forms.Padding(4);
            this.txtfindMoshtarek.Name = "txtfindMoshtarek";
            this.txtfindMoshtarek.PreventEnterBeep = true;
            this.txtfindMoshtarek.Size = new System.Drawing.Size(93, 28);
            this.txtfindMoshtarek.TabIndex = 8;
            this.txtfindMoshtarek.TextChanged += new System.EventHandler(this.textBoxX1_TextChanged);
            this.txtfindMoshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfindMoshtarek_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(663, 14);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(324, 27);
            this.labelX1.TabIndex = 6;
            this.labelX1.Text = "لطفا شماره مشترک مورد نظر را در این قسمت وارد نمایید :";
            // 
            // grp_key
            // 
            this.grp_key.BackColor = System.Drawing.Color.White;
            this.grp_key.CanvasColor = System.Drawing.SystemColors.Control;
            this.grp_key.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_key.Controls.Add(this.txtKey);
            this.grp_key.Controls.Add(this.labelX3);
            this.grp_key.Controls.Add(this.txtMoshtarekName);
            this.grp_key.Controls.Add(this.labelX2);
            this.grp_key.Controls.Add(this.btnCreate);
            this.grp_key.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_key.Location = new System.Drawing.Point(16, 104);
            this.grp_key.Margin = new System.Windows.Forms.Padding(4);
            this.grp_key.Name = "grp_key";
            this.grp_key.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_key.Size = new System.Drawing.Size(1024, 89);
            // 
            // 
            // 
            this.grp_key.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_key.Style.BackColorGradientAngle = 90;
            this.grp_key.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_key.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_key.Style.BorderBottomWidth = 1;
            this.grp_key.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_key.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_key.Style.BorderLeftWidth = 1;
            this.grp_key.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_key.Style.BorderRightWidth = 1;
            this.grp_key.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_key.Style.BorderTopWidth = 1;
            this.grp_key.Style.CornerDiameter = 4;
            this.grp_key.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_key.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_key.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_key.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_key.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_key.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_key.TabIndex = 1;
            this.grp_key.Text = "کلید امنیتی ";
            this.grp_key.Visible = false;
            // 
            // txtKey
            // 
            this.txtKey.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtKey.Border.Class = "TextBoxBorder";
            this.txtKey.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtKey.DisabledBackColor = System.Drawing.Color.White;
            this.txtKey.ForeColor = System.Drawing.Color.Black;
            this.txtKey.Location = new System.Drawing.Point(328, 16);
            this.txtKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtKey.Name = "txtKey";
            this.txtKey.PreventEnterBeep = true;
            this.txtKey.ReadOnly = true;
            this.txtKey.Size = new System.Drawing.Size(147, 28);
            this.txtKey.TabIndex = 16;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(458, 17);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(100, 27);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "کلید امنیتی :";
            // 
            // txtMoshtarekName
            // 
            this.txtMoshtarekName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMoshtarekName.Border.Class = "TextBoxBorder";
            this.txtMoshtarekName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMoshtarekName.DisabledBackColor = System.Drawing.Color.White;
            this.txtMoshtarekName.ForeColor = System.Drawing.Color.Black;
            this.txtMoshtarekName.Location = new System.Drawing.Point(572, 16);
            this.txtMoshtarekName.Margin = new System.Windows.Forms.Padding(4);
            this.txtMoshtarekName.Name = "txtMoshtarekName";
            this.txtMoshtarekName.PreventEnterBeep = true;
            this.txtMoshtarekName.ReadOnly = true;
            this.txtMoshtarekName.Size = new System.Drawing.Size(335, 28);
            this.txtMoshtarekName.TabIndex = 14;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(887, 18);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(100, 27);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "نام مشترک :";
            // 
            // btnCreate
            // 
            this.btnCreate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCreate.BackColor = System.Drawing.Color.Transparent;
            this.btnCreate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCreate.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnCreate.Enabled = false;
            this.btnCreate.Location = new System.Drawing.Point(29, 16);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCreate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnCreate.Size = new System.Drawing.Size(265, 28);
            this.btnCreate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCreate.Symbol = "";
            this.btnCreate.SymbolColor = System.Drawing.Color.Green;
            this.btnCreate.SymbolSize = 12F;
            this.btnCreate.TabIndex = 12;
            this.btnCreate.Text = "ایجاد کلید امنیتی";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // FrmAddKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 206);
            this.Controls.Add(this.grp_key);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAddKey";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.groupPanel1.ResumeLayout(false);
            this.grp_key.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfindMoshtarek;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnFind;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_key;
        private DevComponents.DotNetBar.Controls.TextBoxX txtKey;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMoshtarekName;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnCreate;
    }
}