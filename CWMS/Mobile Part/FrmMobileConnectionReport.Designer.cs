﻿namespace CWMS.Mobile_Part
{
    partial class FrmMobileConnectionReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnFindbygharardads = new DevComponents.DotNetBar.ButtonX();
            this.txtfindbyGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnFindBykey = new DevComponents.DotNetBar.ButtonX();
            this.txtfindbyKey = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.grppanel = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtinternet = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtmobile = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtAll = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.btnAll = new DevComponents.DotNetBar.ButtonX();
            this.dgvAll = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activekeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.connectiontypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expr1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useractionfullBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobileDataSet = new CWMS.Mobile_Part.MobileDataSet();
            this.user_action_fullTableAdapter = new CWMS.Mobile_Part.MobileDataSetTableAdapters.user_action_fullTableAdapter();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.grppanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useractionfullBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobileDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btnFindbygharardads);
            this.groupPanel1.Controls.Add(this.txtfindbyGharardad);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(101, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(304, 71);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 2;
            this.groupPanel1.Text = "مشاهده بر اساس شماره قرارداد";
            // 
            // btnFindbygharardads
            // 
            this.btnFindbygharardads.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFindbygharardads.BackColor = System.Drawing.Color.Transparent;
            this.btnFindbygharardads.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFindbygharardads.Location = new System.Drawing.Point(14, 12);
            this.btnFindbygharardads.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFindbygharardads.Name = "btnFindbygharardads";
            this.btnFindbygharardads.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnFindbygharardads.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnFindbygharardads.Size = new System.Drawing.Size(39, 24);
            this.btnFindbygharardads.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFindbygharardads.Symbol = "";
            this.btnFindbygharardads.SymbolColor = System.Drawing.Color.Green;
            this.btnFindbygharardads.SymbolSize = 12F;
            this.btnFindbygharardads.TabIndex = 1;
            this.btnFindbygharardads.Click += new System.EventHandler(this.btnFindbygharardads_Click);
            // 
            // txtfindbyGharardad
            // 
            this.txtfindbyGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfindbyGharardad.Border.Class = "TextBoxBorder";
            this.txtfindbyGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfindbyGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtfindbyGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtfindbyGharardad.Location = new System.Drawing.Point(79, 12);
            this.txtfindbyGharardad.Name = "txtfindbyGharardad";
            this.txtfindbyGharardad.PreventEnterBeep = true;
            this.txtfindbyGharardad.Size = new System.Drawing.Size(130, 24);
            this.txtfindbyGharardad.TabIndex = 0;
            this.txtfindbyGharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfindbyGharardad_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(203, 13);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(77, 23);
            this.labelX1.TabIndex = 6;
            this.labelX1.Text = "شماره قرارداد:";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btnFindBykey);
            this.groupPanel2.Controls.Add(this.txtfindbyKey);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(411, 12);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(304, 71);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 3;
            this.groupPanel2.Text = "مشاهده بر اساس کلید امنیتی";
            // 
            // btnFindBykey
            // 
            this.btnFindBykey.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFindBykey.BackColor = System.Drawing.Color.Transparent;
            this.btnFindBykey.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFindBykey.Location = new System.Drawing.Point(14, 12);
            this.btnFindBykey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFindBykey.Name = "btnFindBykey";
            this.btnFindBykey.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnFindBykey.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnFindBykey.Size = new System.Drawing.Size(39, 24);
            this.btnFindBykey.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFindBykey.Symbol = "";
            this.btnFindBykey.SymbolColor = System.Drawing.Color.Green;
            this.btnFindBykey.SymbolSize = 12F;
            this.btnFindBykey.TabIndex = 1;
            this.btnFindBykey.Click += new System.EventHandler(this.btnFindBykey_Click);
            // 
            // txtfindbyKey
            // 
            this.txtfindbyKey.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfindbyKey.Border.Class = "TextBoxBorder";
            this.txtfindbyKey.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfindbyKey.DisabledBackColor = System.Drawing.Color.White;
            this.txtfindbyKey.ForeColor = System.Drawing.Color.Black;
            this.txtfindbyKey.Location = new System.Drawing.Point(79, 12);
            this.txtfindbyKey.Name = "txtfindbyKey";
            this.txtfindbyKey.PreventEnterBeep = true;
            this.txtfindbyKey.Size = new System.Drawing.Size(130, 24);
            this.txtfindbyKey.TabIndex = 0;
            this.txtfindbyKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfindbyKey_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(203, 13);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(77, 23);
            this.labelX2.TabIndex = 6;
            this.labelX2.Text = "کلید امنیتی :";
            // 
            // grppanel
            // 
            this.grppanel.BackColor = System.Drawing.Color.White;
            this.grppanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grppanel.Controls.Add(this.txtinternet);
            this.grppanel.Controls.Add(this.labelX5);
            this.grppanel.Controls.Add(this.txtmobile);
            this.grppanel.Controls.Add(this.labelX4);
            this.grppanel.Controls.Add(this.txtAll);
            this.grppanel.Controls.Add(this.labelX3);
            this.grppanel.Controls.Add(this.btnAll);
            this.grppanel.Controls.Add(this.dgvAll);
            this.grppanel.DisabledBackColor = System.Drawing.Color.Empty;
            this.grppanel.Location = new System.Drawing.Point(12, 89);
            this.grppanel.Name = "grppanel";
            this.grppanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grppanel.Size = new System.Drawing.Size(784, 426);
            // 
            // 
            // 
            this.grppanel.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grppanel.Style.BackColorGradientAngle = 90;
            this.grppanel.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grppanel.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grppanel.Style.BorderBottomWidth = 1;
            this.grppanel.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grppanel.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grppanel.Style.BorderLeftWidth = 1;
            this.grppanel.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grppanel.Style.BorderRightWidth = 1;
            this.grppanel.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grppanel.Style.BorderTopWidth = 1;
            this.grppanel.Style.CornerDiameter = 4;
            this.grppanel.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grppanel.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grppanel.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grppanel.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grppanel.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grppanel.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grppanel.TabIndex = 4;
            this.grppanel.Text = "مشاهده بر اساس شماره قرارداد";
            this.grppanel.Click += new System.EventHandler(this.grppanel_Click);
            // 
            // txtinternet
            // 
            this.txtinternet.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtinternet.Border.Class = "TextBoxBorder";
            this.txtinternet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtinternet.DisabledBackColor = System.Drawing.Color.White;
            this.txtinternet.ForeColor = System.Drawing.Color.Black;
            this.txtinternet.Location = new System.Drawing.Point(30, 371);
            this.txtinternet.Name = "txtinternet";
            this.txtinternet.PreventEnterBeep = true;
            this.txtinternet.Size = new System.Drawing.Size(130, 24);
            this.txtinternet.TabIndex = 22;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(101, 372);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(131, 23);
            this.labelX5.TabIndex = 23;
            this.labelX5.Text = "عملیات اینترنتی :";
            // 
            // txtmobile
            // 
            this.txtmobile.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmobile.Border.Class = "TextBoxBorder";
            this.txtmobile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmobile.DisabledBackColor = System.Drawing.Color.White;
            this.txtmobile.ForeColor = System.Drawing.Color.Black;
            this.txtmobile.Location = new System.Drawing.Point(256, 371);
            this.txtmobile.Name = "txtmobile";
            this.txtmobile.PreventEnterBeep = true;
            this.txtmobile.Size = new System.Drawing.Size(130, 24);
            this.txtmobile.TabIndex = 20;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(327, 372);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(131, 23);
            this.labelX4.TabIndex = 21;
            this.labelX4.Text = "عملیات موبایلی :";
            // 
            // txtAll
            // 
            this.txtAll.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAll.Border.Class = "TextBoxBorder";
            this.txtAll.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAll.DisabledBackColor = System.Drawing.Color.White;
            this.txtAll.ForeColor = System.Drawing.Color.Black;
            this.txtAll.Location = new System.Drawing.Point(488, 371);
            this.txtAll.Name = "txtAll";
            this.txtAll.PreventEnterBeep = true;
            this.txtAll.Size = new System.Drawing.Size(130, 24);
            this.txtAll.TabIndex = 18;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(612, 372);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(131, 23);
            this.labelX3.TabIndex = 19;
            this.labelX3.Text = "تعدادکل عملیات انجام شده :";
            // 
            // btnAll
            // 
            this.btnAll.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAll.BackColor = System.Drawing.Color.Transparent;
            this.btnAll.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAll.Location = new System.Drawing.Point(8, 3);
            this.btnAll.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAll.Name = "btnAll";
            this.btnAll.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAll.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAll.Size = new System.Drawing.Size(304, 24);
            this.btnAll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAll.Symbol = "";
            this.btnAll.SymbolColor = System.Drawing.Color.Green;
            this.btnAll.SymbolSize = 12F;
            this.btnAll.TabIndex = 17;
            this.btnAll.Text = "مشاهده فعالیت های انجام شده همه کاربران سیستم";
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // dgvAll
            // 
            this.dgvAll.AllowUserToAddRows = false;
            this.dgvAll.AllowUserToResizeColumns = false;
            this.dgvAll.AllowUserToResizeRows = false;
            this.dgvAll.AutoGenerateColumns = false;
            this.dgvAll.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAll.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAll.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAll.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tarikhDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.activekeyDataGridViewTextBoxColumn,
            this.connectiontypeDataGridViewTextBoxColumn,
            this.expr1DataGridViewTextBoxColumn,
            this.tozihatDataGridViewTextBoxColumn});
            this.dgvAll.DataSource = this.useractionfullBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAll.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAll.EnableHeadersVisualStyles = false;
            this.dgvAll.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvAll.Location = new System.Drawing.Point(17, 31);
            this.dgvAll.Name = "dgvAll";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAll.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAll.Size = new System.Drawing.Size(749, 333);
            this.dgvAll.TabIndex = 16;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.FillWeight = 60F;
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            // 
            // activekeyDataGridViewTextBoxColumn
            // 
            this.activekeyDataGridViewTextBoxColumn.DataPropertyName = "active_key";
            this.activekeyDataGridViewTextBoxColumn.FillWeight = 50F;
            this.activekeyDataGridViewTextBoxColumn.HeaderText = "کلید امنیتی";
            this.activekeyDataGridViewTextBoxColumn.Name = "activekeyDataGridViewTextBoxColumn";
            // 
            // connectiontypeDataGridViewTextBoxColumn
            // 
            this.connectiontypeDataGridViewTextBoxColumn.DataPropertyName = "connection_type";
            this.connectiontypeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.connectiontypeDataGridViewTextBoxColumn.HeaderText = "نحوه دسترسی";
            this.connectiontypeDataGridViewTextBoxColumn.Name = "connectiontypeDataGridViewTextBoxColumn";
            // 
            // expr1DataGridViewTextBoxColumn
            // 
            this.expr1DataGridViewTextBoxColumn.DataPropertyName = "Expr1";
            this.expr1DataGridViewTextBoxColumn.FillWeight = 200F;
            this.expr1DataGridViewTextBoxColumn.HeaderText = "نوع عملیات انجام شده";
            this.expr1DataGridViewTextBoxColumn.Name = "expr1DataGridViewTextBoxColumn";
            // 
            // tozihatDataGridViewTextBoxColumn
            // 
            this.tozihatDataGridViewTextBoxColumn.DataPropertyName = "tozihat";
            this.tozihatDataGridViewTextBoxColumn.FillWeight = 200F;
            this.tozihatDataGridViewTextBoxColumn.HeaderText = "توضیحات";
            this.tozihatDataGridViewTextBoxColumn.Name = "tozihatDataGridViewTextBoxColumn";
            // 
            // useractionfullBindingSource
            // 
            this.useractionfullBindingSource.DataMember = "user_action_full";
            this.useractionfullBindingSource.DataSource = this.mobileDataSet;
            // 
            // mobileDataSet
            // 
            this.mobileDataSet.DataSetName = "MobileDataSet";
            this.mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // user_action_fullTableAdapter
            // 
            this.user_action_fullTableAdapter.ClearBeforeFill = true;
            // 
            // FrmMobileConnectionReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 518);
            this.Controls.Add(this.grppanel);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMobileConnectionReport";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmMobileConnectionReport_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.grppanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useractionfullBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobileDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btnFindbygharardads;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfindbyGharardad;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btnFindBykey;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfindbyKey;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.GroupPanel grppanel;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAll;
        private MobileDataSet mobileDataSet;
        private System.Windows.Forms.BindingSource useractionfullBindingSource;
        private MobileDataSetTableAdapters.user_action_fullTableAdapter user_action_fullTableAdapter;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activekeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn connectiontypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expr1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihatDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.ButtonX btnAll;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAll;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtinternet;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtmobile;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}