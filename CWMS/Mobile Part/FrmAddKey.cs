﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Mobile_Part
{
    public partial class FrmAddKey : MyMetroForm
    {
        public FrmAddKey()
        {
            InitializeComponent();
        }
        public FrmAddKey(string ghtemp)
        {
            InitializeComponent();
            txtfindMoshtarek.Text = ghtemp;
            btnFind.PerformClick();
        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtfindMoshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFind.PerformClick();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            grp_key.Visible = false;
            if (txtfindMoshtarek.Text.Trim() == "")
            {
                Payam.Show("لطفا شماره قرارداد را وارد نمایید");
                txtfindMoshtarek.Focus();
            }
            else
            {
                string ghararadad = txtfindMoshtarek.Text;
                try
                {

                    txtMoshtarekName.Text = Classes.ClsMain.ExecuteScalar("select co_name from moshtarekin where gharardad=" + ghararadad).ToString();
                    if (txtMoshtarekName.Text.Trim() == "")
                    {
                        Payam.Show("مشترک مورد  نظر یافت نشد");
                        txtfindMoshtarek.SelectAll();
                        txtMoshtarekName.Focus();
                    }
                    else
                    {
                        txtKey.Text = Classes.clsMoshtarekin.GetNewActiveKey();
                        btnCreate.Enabled = true;
                        grp_key.Visible = true;
                    }
                }
                catch
                {
                    Payam.Show("شماره قرارداد نامعتبر می باشد");
                    txtfindMoshtarek.Clear();
                    txtfindMoshtarek.Focus();
                }

            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p89)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 89);

                if (txtKey.Text != "" && txtMoshtarekName.Text != "")
                {
                    Classes.ClsMain.ExecuteNoneQuery("insert into activation_key (active_key,gharardad) values (" + txtKey.Text + "," + txtfindMoshtarek.Text + ")");
                    btnCreate.Enabled = false;
                    Payam.Show("کلید امنیتی جدید با موفقیت در سیستم ثبت شد. در صورت تمایل آن را چاپ نمایید");

                    CrysReports.mobile report1 = new CrysReports.mobile();
                    report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
                    report1.SetParameterValue("gharardad", txtfindMoshtarek.Text);
                    report1.SetParameterValue("co_name", txtMoshtarekName.Text);
                    report1.SetParameterValue("active_key", txtKey.Text);

                    new FrmShowReport(report1).ShowDialog();





                }
                else
                {
                    btnCreate.Enabled = false;
                    txtfindMoshtarek.Focus();
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }
    }
}