﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
namespace CWMS.Mobile_Part
{
    public partial class FrmMobileConnectionReport : MyMetroForm
    {
        public FrmMobileConnectionReport()
        {
            InitializeComponent();
        }

        private void FrmMobileConnectionReport_Load(object sender, EventArgs e)
        {


        }

        private void btnFindBykey_Click(object sender, EventArgs e)
        {
            if(txtfindbyKey.Text.Trim()!= "" )
            {
                try
                {
                    user_action_fullTableAdapter.FillByActiveKey(mobileDataSet.user_action_full, txtfindbyKey.Text);
                    grppanel.Text = "بر اساس کلید امنیتی :" + txtfindbyKey.Text;
                    txtAll.Text = mobileDataSet.user_action_full.Count().ToString();
                    txtinternet.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "اینترنت").ToString();
                    txtmobile.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "موبایل").ToString();
                    txtfindbyGharardad.Text = "";
                }
                catch (Exception)
                {
                    Payam.Show("شماره کلید امنیتی نامعتبر می باشد");
                }

            }
            else
                showall();


        }

        private void btnFindbygharardads_Click(object sender, EventArgs e)
        {
            if (txtfindbyGharardad.Text.Trim() != "")
            {
                try
                {
                    user_action_fullTableAdapter.FillByGharadad(mobileDataSet.user_action_full,Convert.ToInt32(txtfindbyGharardad.Text));
                    txtAll.Text = mobileDataSet.user_action_full.Count().ToString();
                    txtinternet.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "اینترنت").ToString();
                    txtmobile.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "موبایل").ToString();
                    grppanel.Text = "براساس شماره قرارداد :" + txtfindbyGharardad.Text;
                    txtfindbyKey.Text = "";
                }
                catch (Exception)
                {
                    Payam.Show("شماره قرارداد نامعتبر می باشد");
                }

            }
            else
                showall();


        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            showall();
        }
        void showall()
        {
            this.user_action_fullTableAdapter.Fill(this.mobileDataSet.user_action_full);
            txtAll.Text = mobileDataSet.user_action_full.Count().ToString();
            txtinternet.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "اینترنت").ToString();
            txtmobile.Text = mobileDataSet.user_action_full.Count(p => p.connection_type == "موبایل").ToString();
            grppanel.Text = "فعالیت های مربوط به کلیه کاربران سیستم";
        }

        private void grppanel_Click(object sender, EventArgs e)
        {

        }

        private void txtfindbyKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFindBykey.PerformClick();
        }

        private void txtfindbyGharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFindbygharardads.PerformClick();
        }
    }
}