﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Rpt_Sharj_Ghobooz_PN : DevComponents.DotNetBar.Metro.MetroForm
    {
        string dcode = "";
        public Rpt_Sharj_Ghobooz_PN(string code_dore)
        {
            InitializeComponent();
            dcode = code_dore;
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.Text = code_dore;
            lblDoreh.Text = "دوره " + dcode ;
            btn_find.PerformClick();
        }

       

        private void btn_find_Click(object sender, EventArgs e)
        {
            lblDoreh.Text = "دوره " + cmbDoreh.Text;
            sharj_doreh_PNTableAdapter.Fill(mainDataSest.Sharj_doreh_PN, Convert.ToInt32(cmbDoreh.Text));
            dgbedehkar.DataSource = mainDataSest.Sharj_doreh_PN;
            txtcnt.Text = mainDataSest.Sharj_doreh_PN.Rows.Count.ToString();
            double sum = 0;
            for (int i = 0; i < mainDataSest.Sharj_doreh_PN.Rows.Count; i++)
            {
                sum += Convert.ToDouble(mainDataSest.Sharj_doreh_PN[i].mablaghkol);
            }
            txtsum.Text = sum.ToString();
            txthoroof.Text = Classes.clsNumber.Number_to_Str(txtsum.Text);
            if (mainDataSest.Sharj_doreh_PN.Rows.Count == 0)
                Payam.Show("این مشترک هیچ درخواستی جهت صدور برگه خروج در سیستم ثبت نکرده است .");

        }

        private void dgbedehkar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Frm_sharj_details frm = new Frm_sharj_details(dgbedehkar.Rows[e.RowIndex].Cells[0].Value.ToString(), dgbedehkar.Rows[e.RowIndex].Cells[2].Value.ToString());
                frm.ShowDialog();
            }
        }

        private void Rpt_Sharj_Ghobooz_PN_Load(object sender, EventArgs e)
        {

        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            lblDoreh.Text = "نمایش بدهکاران دوره فعلی";

            new MainDataSestTableAdapters.Sharj_All_doreh_PNTableAdapter().Fill(mainDataSest.Sharj_All_doreh_PN);
            DataView bedehkaranDoreFeli = new DataView(mainDataSest.Sharj_All_doreh_PN);
            bedehkaranDoreFeli.RowFilter = "dcode=" + Classes.clsSharj.Get_last_doreh_dcode();
            dgbedehkar.DataSource = bedehkaranDoreFeli;



            txtcnt.Text = mainDataSest.Sharj_All_doreh_PN.Rows.Count.ToString();
            double sum = 0;
            for (int i = 0; i < mainDataSest.Sharj_All_doreh_PN.Rows.Count; i++)
            {
                sum += Convert.ToDouble(mainDataSest.Sharj_All_doreh_PN[i].mablaghkol);
            }
            txtsum.Text = sum.ToString();
            txthoroof.Text = Classes.clsNumber.Number_to_Str(txtsum.Text);
        }


        public void SetselectedDoreh(string s)
        {
         DataTable dt=  Classes.ClsMain.GetDataTable("select dcode,moshtarekin.gharardad,co_name,modir_amel,mablagh,bedehi,mablaghkol from moshtarekin right join sharj_ghabz on moshtarekin.gharardad=sharj_ghabz.gharardad where (sharj_ghabz.vaziat=0 or sharj_ghabz.vaziat=2)and dcode in (" +  s + ")");
         dgbedehkar.DataSource = dt;
         double sum = 0;
         for (int i = 0; i < dt.Rows.Count; i++)
         {
             sum += Convert.ToDouble(dt.Rows[i]["mablaghkol"].ToString());
         }
         txtsum.Text = sum.ToString();
         txthoroof.Text = Classes.clsNumber.Number_to_Str(txtsum.Text);
         lblDoreh.Text = "دوره ها:" + s;
         txtcnt.Text = dt.Rows.Count.ToString();
        }

        private void btn_somDeoreh_Click(object sender, EventArgs e)
        {
            FrmSelectSomDoreh frm = new FrmSelectSomDoreh(this);
            frm.ShowDialog();
        }
    }
}