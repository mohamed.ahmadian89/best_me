﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;

namespace CWMS
{
    public partial class FrmMoveInfoForMobile : DevComponents.DotNetBar.Metro.MetroForm
    {
        DataTable dt_info = null;
        public FrmMoveInfoForMobile()
        {
            InitializeComponent();
        }

        private void FrmMoveInfoForMobile_Load(object sender, EventArgs e)
        {
            dt_info = Classes.ClsMain.GetDataTable("select top(1) * from setting");
            if (dt_info.Rows.Count != 0)
            {
                txt_id.Text = dt_info.Rows[0]["id"].ToString();
                txt_name.Text = dt_info.Rows[0]["shahrak_name"].ToString();
            }

            cmbSharjDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode");
            cmbSharjDoreh.DisplayMember = "dcode";
            cmbSharjDoreh.ValueMember = "dcode";

            cmbAbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from ab_doreh order by dcode");
            cmbAbDoreh.DisplayMember = "dcode";
            cmbAbDoreh.ValueMember = "dcode";



        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            if (cmbSharjDoreh.Items.Count != 0)
            {
                if (backgroundWorker1.IsBusy)
                    Payam.Show("لطفا چند لحظه صبر نمایید. سیستم در حال انتقال اطلاعات فعلی می باشد");
                else
                {
                    Payam.Show("عملیات انتقال اطلاعات آغاز شد");
                    string[] array = new string[2];
                    array[0] = cmbSharjDoreh.SelectedValue.ToString();
                    array[1] = txt_id.Text;

                    backgroundWorker1.RunWorkerAsync(array);


                }
            }

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (cmbAbDoreh.Items.Count != 0)
            {
                if (Ab_BGWorker.IsBusy)
                    Payam.Show("لطفا چند لحظه صبر نمایید. سیستم در حال انتقال اطلاعات فعلی می باشد");
                else
                {
                    Payam.Show("عملیات انتقال اطلاعات آغاز شد");
                    string[] array = new string[2];
                    array[0] = cmbAbDoreh.SelectedValue.ToString();
                    array[1] = txt_id.Text;
                    Ab_BGWorker.RunWorkerAsync(array);
                }
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string Doreh = ((string[]) e.Argument)[0];
            string Shahrak_id=((string[]) e.Argument)[1];
            
            SqlConnection destinationConnection = new SqlConnection(@"Server=" + dt_info.Rows[0]["db_ip"].ToString() + ";Database=" + dt_info.Rows[0]["db_name"].ToString() + ";User Id=" + dt_info.Rows[0]["db_username"].ToString() + ";Password=" + dt_info.Rows[0]["db_password"].ToString());
            destinationConnection.Open();
            SqlCommand com = new SqlCommand();
            com.CommandText = "delete  from sharj_ghabz where dcode=" + Doreh + " and shahrak_id=" + Shahrak_id;
            com.Connection = destinationConnection;
            com.ExecuteNonQuery();
            com.CommandText = "delete  from ghabz_pardakht where dcode=" + Doreh + " and shahrak_id=" + Shahrak_id + " and is_ab=0";
            com.ExecuteNonQuery();
            




            DataTable dt = Classes.ClsMain.GetDataTable("select * from sharj_ghabz where dcode=" + Doreh );


            dt.Columns.Add("shahrak_id");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["shahrak_id"] = txt_id.Text;
            }


            DataTable dt_pardakht = Classes.ClsMain.GetDataTable("select * from ghabz_pardakht where dcode=" + Doreh +  " and is_ab=0");


            dt_pardakht.Columns.Add("shahrak_id");
            for (int i = 0; i < dt_pardakht.Rows.Count; i++)
            {
                dt_pardakht.Rows[i]["shahrak_id"] = txt_id.Text;
            }



            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection))
            {
                

                try
                {
                    bulkCopy.BulkCopyTimeout = 60;
                    bulkCopy.DestinationTableName ="sharj_ghabz";
                    bulkCopy.WriteToServer(dt);

                    bulkCopy.DestinationTableName = "ghabz_pardakht";
                    bulkCopy.WriteToServer(dt_pardakht);
                    Payam.Show("اطلاعات قبوض شارژ با موفقیت به سرور انتقال یافت");


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Payam.Show("متاسفانه در هنگام انتقال اطلاعات سیستم دچار مشکل شده است .. لطفا دوباره سعی کنید");

                }
                finally
                {
                    // Close the SqlDataReader. The SqlBulkCopy 
                    // object is automatically closed at the end 
                    // of the using block.
                    destinationConnection.Close();


                }
            }





        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            string Doreh = ((string[])e.Argument)[0];
            string Shahrak_id = ((string[])e.Argument)[1];

            SqlConnection destinationConnection = new SqlConnection(@"Server=" + dt_info.Rows[0]["db_ip"].ToString() + ";Database=" + dt_info.Rows[0]["db_name"].ToString() + ";User Id=" + dt_info.Rows[0]["db_username"].ToString() + ";Password=" + dt_info.Rows[0]["db_password"].ToString());
            destinationConnection.Open();
            SqlCommand com = new SqlCommand();
            com.CommandText = "delete  from ab_ghabz where dcode=" + Doreh + " and shahrak_id=" + Shahrak_id;
            com.Connection = destinationConnection;
            com.ExecuteNonQuery();
            com.CommandText = "delete  from ghabz_pardakht where dcode=" + Doreh + " and shahrak_id=" + Shahrak_id + " and is_ab=1";
            com.ExecuteNonQuery();





            DataTable dt = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + Doreh );


            dt.Columns.Add("shahrak_id");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["shahrak_id"] = txt_id.Text;
            }


            DataTable dt_pardakht = Classes.ClsMain.GetDataTable("select * from ghabz_pardakht where dcode=" + Doreh + " and is_ab=1");


            dt_pardakht.Columns.Add("shahrak_id");
            for (int i = 0; i < dt_pardakht.Rows.Count; i++)
            {
                dt_pardakht.Rows[i]["shahrak_id"] = txt_id.Text;
            }


            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection))
            {
               

                try
                {
                    bulkCopy.BulkCopyTimeout = 60;

                    // Write from the source to the destination.
                    bulkCopy.DestinationTableName ="ab_ghabz";
                    bulkCopy.WriteToServer(dt);

                    bulkCopy.DestinationTableName = "ghabz_pardakht";
                    bulkCopy.WriteToServer(dt_pardakht);
                    Payam.Show("اطلاعات قبوض آب با موفقیت به سرور منتقل شد");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Payam.Show("متاسفانه در هنگام انتقال اطلاعات سیستم دچار مشکل شده است .. لطفا دوباره سعی کنید");
                }
                finally
                {
                    // Close the SqlDataReader. The SqlBulkCopy 
                    // object is automatically closed at the end 
                    // of the using block.
                    destinationConnection.Close();

                }
            }




        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (Moshtarekin_BGFWorker.IsBusy)
                Payam.Show("لطفا چند لحظه صبر نمایید. سیستم در حال انتقال اطلاعات فعلی می باشد");
            else
            {
                Payam.Show("عملیات انتقال اطلاعات آغاز شد");

                Moshtarekin_BGFWorker.RunWorkerAsync();

            }
        }

        private void Moshtarekin_BGFWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection destinationConnection = new SqlConnection(@"Server=" + dt_info.Rows[0]["db_ip"].ToString() + ";Database=" + dt_info.Rows[0]["db_name"].ToString() + ";User Id=" + dt_info.Rows[0]["db_username"].ToString() + ";Password=" + dt_info.Rows[0]["db_password"].ToString());

            SqlCommand com = new SqlCommand("delete from moshtarekin", destinationConnection);
            destinationConnection.Open();
            com.ExecuteNonQuery();



            DataTable dt = Classes.ClsMain.GetDataTable("select gharardad,co_name,modir_amel,metraj,size_ensheab from moshtarekin");


            dt.Columns.Add("shahrak_id");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["shahrak_id"] = txt_id.Text;
            }



            // Set up the bulk copy object.  
            // Note that the column positions in the source 
            // data reader match the column positions in  
            // the destination table so there is no need to 
            // map columns. 
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection))
            {
                bulkCopy.DestinationTableName =
                    "moshtarekin";

                try
                {
                    // Write from the source to the destination.
                    bulkCopy.WriteToServer(dt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    // Close the SqlDataReader. The SqlBulkCopy 
                    // object is automatically closed at the end 
                    // of the using block.

                }
            }




            destinationConnection.Close();
            Payam.Show("اطلاعات مشترکین با موفقیت به سرور انتقال یافت");
        }

    }
}