﻿namespace CWMS
{
    partial class FrmBedehkaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_max_tedad = new DevComponents.DotNetBar.ButtonX();
            this.btn_min_tedad = new DevComponents.DotNetBar.ButtonX();
            this.btn_bedehimax = new DevComponents.DotNetBar.ButtonX();
            this.bedehi_min = new DevComponents.DotNetBar.ButtonX();
            this.btn_find_by_gcode = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_ghabz_byCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find = new DevComponents.DotNetBar.ButtonX();
            this.cmbDoreh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btn_all = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modiramelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tedadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehkaranWithBedehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.bedehkaranWithBedehTableAdapter = new CWMS.CWMSDataSetTableAdapters.bedehkaranWithBedehTableAdapter();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedehkaranWithBedehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btn_max_tedad);
            this.groupPanel1.Controls.Add(this.btn_min_tedad);
            this.groupPanel1.Controls.Add(this.btn_bedehimax);
            this.groupPanel1.Controls.Add(this.bedehi_min);
            this.groupPanel1.Controls.Add(this.btn_find_by_gcode);
            this.groupPanel1.Controls.Add(this.txt_find_ghabz_byCode);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(11, 74);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(820, 72);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 42;
            // 
            // btn_max_tedad
            // 
            this.btn_max_tedad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_max_tedad.BackColor = System.Drawing.Color.Transparent;
            this.btn_max_tedad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_max_tedad.Location = new System.Drawing.Point(140, 38);
            this.btn_max_tedad.Name = "btn_max_tedad";
            this.btn_max_tedad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_max_tedad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_max_tedad.Size = new System.Drawing.Size(121, 23);
            this.btn_max_tedad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_max_tedad.Symbol = "";
            this.btn_max_tedad.SymbolColor = System.Drawing.Color.Green;
            this.btn_max_tedad.SymbolSize = 9F;
            this.btn_max_tedad.TabIndex = 31;
            this.btn_max_tedad.Text = "بیشترین تعداد قبض";
            this.btn_max_tedad.Click += new System.EventHandler(this.btn_max_tedad_Click);
            // 
            // btn_min_tedad
            // 
            this.btn_min_tedad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_min_tedad.BackColor = System.Drawing.Color.Transparent;
            this.btn_min_tedad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_min_tedad.Location = new System.Drawing.Point(13, 38);
            this.btn_min_tedad.Name = "btn_min_tedad";
            this.btn_min_tedad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_min_tedad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_min_tedad.Size = new System.Drawing.Size(121, 23);
            this.btn_min_tedad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_min_tedad.Symbol = "";
            this.btn_min_tedad.SymbolColor = System.Drawing.Color.Green;
            this.btn_min_tedad.SymbolSize = 9F;
            this.btn_min_tedad.TabIndex = 30;
            this.btn_min_tedad.Text = "کمترین تعداد قبض";
            this.btn_min_tedad.Click += new System.EventHandler(this.btn_min_tedad_Click);
            // 
            // btn_bedehimax
            // 
            this.btn_bedehimax.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_bedehimax.BackColor = System.Drawing.Color.Transparent;
            this.btn_bedehimax.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_bedehimax.Location = new System.Drawing.Point(140, 9);
            this.btn_bedehimax.Name = "btn_bedehimax";
            this.btn_bedehimax.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_bedehimax.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_bedehimax.Size = new System.Drawing.Size(121, 23);
            this.btn_bedehimax.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_bedehimax.Symbol = "";
            this.btn_bedehimax.SymbolColor = System.Drawing.Color.Green;
            this.btn_bedehimax.SymbolSize = 9F;
            this.btn_bedehimax.TabIndex = 29;
            this.btn_bedehimax.Text = "بیشترین بدهی";
            this.btn_bedehimax.Click += new System.EventHandler(this.btn_bedehimax_Click);
            // 
            // bedehi_min
            // 
            this.bedehi_min.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.bedehi_min.BackColor = System.Drawing.Color.Transparent;
            this.bedehi_min.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.bedehi_min.Location = new System.Drawing.Point(13, 9);
            this.bedehi_min.Name = "bedehi_min";
            this.bedehi_min.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.bedehi_min.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.bedehi_min.Size = new System.Drawing.Size(121, 23);
            this.bedehi_min.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bedehi_min.Symbol = "";
            this.bedehi_min.SymbolColor = System.Drawing.Color.Green;
            this.bedehi_min.SymbolSize = 9F;
            this.bedehi_min.TabIndex = 28;
            this.bedehi_min.Text = "کمترین بدهی";
            this.bedehi_min.Click += new System.EventHandler(this.bedehi_min_Click);
            // 
            // btn_find_by_gcode
            // 
            this.btn_find_by_gcode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_by_gcode.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_by_gcode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_by_gcode.Location = new System.Drawing.Point(592, 5);
            this.btn_find_by_gcode.Name = "btn_find_by_gcode";
            this.btn_find_by_gcode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find_by_gcode.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_by_gcode.Size = new System.Drawing.Size(31, 23);
            this.btn_find_by_gcode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_by_gcode.Symbol = "";
            this.btn_find_by_gcode.SymbolColor = System.Drawing.Color.Teal;
            this.btn_find_by_gcode.SymbolSize = 9F;
            this.btn_find_by_gcode.TabIndex = 27;
            this.btn_find_by_gcode.Click += new System.EventHandler(this.btn_find_by_gcode_Click);
            // 
            // txt_find_ghabz_byCode
            // 
            this.txt_find_ghabz_byCode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_ghabz_byCode.Border.Class = "TextBoxBorder";
            this.txt_find_ghabz_byCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_ghabz_byCode.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_ghabz_byCode.ForeColor = System.Drawing.Color.Black;
            this.txt_find_ghabz_byCode.Location = new System.Drawing.Point(629, 5);
            this.txt_find_ghabz_byCode.Name = "txt_find_ghabz_byCode";
            this.txt_find_ghabz_byCode.PreventEnterBeep = true;
            this.txt_find_ghabz_byCode.Size = new System.Drawing.Size(109, 24);
            this.txt_find_ghabz_byCode.TabIndex = 26;
            this.txt_find_ghabz_byCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_ghabz_byCode_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(729, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(61, 23);
            this.labelX1.TabIndex = 25;
            this.labelX1.Text = "قرارداد:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_find);
            this.groupPanel2.Controls.Add(this.cmbDoreh);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.btn_all);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(12, 6);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(820, 60);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 40;
            this.groupPanel2.Text = "جستجوی اطلاعات";
            // 
            // btn_find
            // 
            this.btn_find.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find.BackColor = System.Drawing.Color.Transparent;
            this.btn_find.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find.Location = new System.Drawing.Point(449, 9);
            this.btn_find.Name = "btn_find";
            this.btn_find.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find.Size = new System.Drawing.Size(152, 23);
            this.btn_find.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find.Symbol = "";
            this.btn_find.SymbolColor = System.Drawing.Color.Green;
            this.btn_find.SymbolSize = 9F;
            this.btn_find.TabIndex = 2;
            this.btn_find.Text = "نمایش بدهکاران این دوره";
            this.btn_find.Click += new System.EventHandler(this.btn_find_Click);
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.DisplayMember = "Text";
            this.cmbDoreh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDoreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoreh.ForeColor = System.Drawing.Color.Black;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.ItemHeight = 18;
            this.cmbDoreh.Location = new System.Drawing.Point(617, 8);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDoreh.Size = new System.Drawing.Size(124, 24);
            this.cmbDoreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDoreh.TabIndex = 1;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(730, 9);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(61, 23);
            this.labelX7.TabIndex = 24;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btn_all
            // 
            this.btn_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_all.Location = new System.Drawing.Point(3, 7);
            this.btn_all.Name = "btn_all";
            this.btn_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_all.Size = new System.Drawing.Size(234, 23);
            this.btn_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_all.Symbol = "";
            this.btn_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_all.SymbolSize = 9F;
            this.btn_all.TabIndex = 1;
            this.btn_all.Text = "نمایش کلیه قبوض دوره فعلی";
            this.btn_all.Click += new System.EventHandler(this.btn_all_Click);
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.dataGridViewX1);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(11, 152);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(820, 319);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 43;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.AllowUserToResizeColumns = false;
            this.dataGridViewX1.AllowUserToResizeRows = false;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.modiramelDataGridViewTextBoxColumn,
            this.mablaghkolDataGridViewTextBoxColumn,
            this.tedadDataGridViewTextBoxColumn});
            this.dataGridViewX1.DataSource = this.bedehkaranWithBedehBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(13, 12);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(781, 296);
            this.dataGridViewX1.TabIndex = 0;
            this.dataGridViewX1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellClick);
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "شماره قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام شرکت";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modiramelDataGridViewTextBoxColumn
            // 
            this.modiramelDataGridViewTextBoxColumn.DataPropertyName = "modir_amel";
            this.modiramelDataGridViewTextBoxColumn.HeaderText = "مدیرعامل";
            this.modiramelDataGridViewTextBoxColumn.Name = "modiramelDataGridViewTextBoxColumn";
            this.modiramelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tedadDataGridViewTextBoxColumn
            // 
            this.tedadDataGridViewTextBoxColumn.DataPropertyName = "tedad";
            this.tedadDataGridViewTextBoxColumn.FillWeight = 30F;
            this.tedadDataGridViewTextBoxColumn.HeaderText = "تعداد قبوض";
            this.tedadDataGridViewTextBoxColumn.Name = "tedadDataGridViewTextBoxColumn";
            this.tedadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bedehkaranWithBedehBindingSource
            // 
            this.bedehkaranWithBedehBindingSource.DataMember = "bedehkaranWithBedeh";
            this.bedehkaranWithBedehBindingSource.DataSource = this.cWMSDataSet;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bedehkaranWithBedehTableAdapter
            // 
            this.bedehkaranWithBedehTableAdapter.ClearBeforeFill = true;
            // 
            // FrmBedehkaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 475);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBedehkaran";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmBedehkaran_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bedehkaranWithBedehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_find_by_gcode;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_ghabz_byCode;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_find;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDoreh;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btn_all;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private System.Windows.Forms.BindingSource bedehkaranWithBedehBindingSource;
        private CWMSDataSet cWMSDataSet;
        private CWMSDataSetTableAdapters.bedehkaranWithBedehTableAdapter bedehkaranWithBedehTableAdapter;
        private DevComponents.DotNetBar.ButtonX btn_bedehimax;
        private DevComponents.DotNetBar.ButtonX bedehi_min;
        private DevComponents.DotNetBar.ButtonX btn_max_tedad;
        private DevComponents.DotNetBar.ButtonX btn_min_tedad;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modiramelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tedadDataGridViewTextBoxColumn;
    }
}