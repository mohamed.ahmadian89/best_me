﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace CWMS
{
    public partial class smsForm : Form
    {

        string gcode = "",type="";
        public smsForm(string mobile,string gcode_t,string type_t)
        {
            InitializeComponent();
            txtMobile.Text = mobile;
            type = type_t;
            gcode = gcode_t;
        }

        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnsend.PerformClick();
        }

        private void btnsend_Click(object sender, EventArgs e)
        {
            if (txtMobile.Text.Trim() != "")
            {
                if (txtMobile.Text.StartsWith("09") == true && txtMobile.Text.Length == 11)
                {
                    string tablename = Classes.clsDoreh.gcode_tableName(gcode);
                    System.Data.DataTable dt_ghabz_info = Classes.ClsMain.GetDataTable("select  " + tablename + ".gharardad as gharardad,mande,shenase_ghabz,shenase_pardakht,password  from " + tablename + " inner join moshtarekin on " + tablename +".gharardad=moshtarekin.gharardad where gcode=" + gcode.ToString());
                    string gharardad = dt_ghabz_info.Rows[0]["gharardad"].ToString();
                    string mablaghmande= dt_ghabz_info.Rows[0]["mande"].ToString();
                    string password= dt_ghabz_info.Rows[0]["password"].ToString();
                    string shenase_ghabz = dt_ghabz_info.Rows[0]["shenase_ghabz"].ToString();
                    string shenase_pardakht = dt_ghabz_info.Rows[0]["shenase_pardakht"].ToString();
                    string tozihat = "پرداخت اینترنتی  :" +  System.Environment.NewLine + "www.abps.ir" + System.Environment.NewLine;
                    string tozihat2="پرداخت  موبایل:" + System.Environment.NewLine + " *780*111*# " + System.Environment.NewLine;
                    string Persiantable = "آب";
                    if (tablename != "ab_ghabz")
                        Persiantable = "شارژ";
                    txtsmscontent.Text = "قرارداد:" + gharardad + System.Environment.NewLine
                        +"رمزعبور:" + password + System.Environment.NewLine
                        + "مبلغ قابل پرداخت " + Persiantable + ":" + mablaghmande + System.Environment.NewLine
                        + "شناسه قبض:" + shenase_ghabz + System.Environment.NewLine
                        + "شناسه پرداخت:" + shenase_pardakht + System.Environment.NewLine
                        + tozihat + tozihat2;
                    SmsSamaneh.Person p = new SmsSamaneh.Person();

                    p.phone = txtMobile.Text;
                    SmsSamaneh.SmsOperation smg = new SmsSamaneh.SmsOperation();
                    smg.Send_Sms(p, txtsmscontent.Text);
                    Payam.Show("پیامک با موفقیت ارسال شد"); 
                }
                else
                {
                    Payam.Show("شماره تلفن همراه نامعتبر می باشد");
                    txtMobile.SelectAll();
                    txtMobile.Focus();
                }
                       

            }
            else

                txtMobile.Focus();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
                
        }

        private void smsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();

        }
    }
}
