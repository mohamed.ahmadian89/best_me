﻿namespace CWMS
{
    partial class frm_moshtarek_all
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.moshtarekinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.moshtarekinTableAdapter1 = new CWMS.MainDataSestTableAdapters.moshtarekinTableAdapter();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ch_hae_sharj_ghabz = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ch_have_ab_ghabz = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chsakhtosaz = new System.Windows.Forms.CheckBox();
            this.chtejari = new System.Windows.Forms.CheckBox();
            this.checkBoxX1 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txtKontorRaghan = new DevComponents.Editors.IntegerInput();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.integerInput6 = new DevComponents.Editors.IntegerInput();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.integerInput1 = new DevComponents.Editors.IntegerInput();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.grp_personel = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.integerInput4 = new DevComponents.Editors.IntegerInput();
            this.integerInput3 = new DevComponents.Editors.IntegerInput();
            this.integerInput2 = new DevComponents.Editors.IntegerInput();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.grp_main = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.tarikh_gheraat_ab = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX36 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX5 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.txt_sh_variz = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX33 = new DevComponents.DotNetBar.LabelX();
            this.txt_parvandeh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX34 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX4 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX3 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_pass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_posti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_Eghtesadi = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.txt_code_melli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.txt_metraj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtghataat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.txtPelak = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.faDatePicker1 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txt_address = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_phone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_mobile = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_modir_amel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_co_name = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_by_modir_amel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_find_by_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_find_by_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_all = new DevComponents.DotNetBar.ButtonX();
            this.dg_moshtarek = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modiramelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_find_modir_amel = new DevComponents.DotNetBar.ButtonX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.btn_find_moshtarek = new DevComponents.DotNetBar.ButtonX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.btn_find_gharardad = new DevComponents.DotNetBar.ButtonX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKontorRaghan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput1)).BeginInit();
            this.grp_personel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput2)).BeginInit();
            this.grp_main.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_moshtarek)).BeginInit();
            this.SuspendLayout();
            // 
            // moshtarekinBindingSource
            // 
            this.moshtarekinBindingSource.DataMember = "moshtarekin";
            this.moshtarekinBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // moshtarekinTableAdapter1
            // 
            this.moshtarekinTableAdapter1.ClearBeforeFill = true;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX5);
            this.groupPanel2.Controls.Add(this.buttonX6);
            this.groupPanel2.Controls.Add(this.buttonX4);
            this.groupPanel2.Controls.Add(this.buttonX1);
            this.groupPanel2.Controls.Add(this.groupPanel5);
            this.groupPanel2.Controls.Add(this.groupPanel3);
            this.groupPanel2.Controls.Add(this.btn_save);
            this.groupPanel2.Controls.Add(this.groupPanel4);
            this.groupPanel2.Controls.Add(this.grp_personel);
            this.groupPanel2.Controls.Add(this.grp_main);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(10, 204);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1213, 455);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 2;
            this.groupPanel2.Text = "اطلاعات مشترک";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(10, 334);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX5.Size = new System.Drawing.Size(257, 37);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Green;
            this.buttonX5.SymbolSize = 9F;
            this.buttonX5.TabIndex = 45;
            this.buttonX5.Text = "تعیین امضاء مشترک";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click_2);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(10, 382);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX6.Size = new System.Drawing.Size(257, 37);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Maroon;
            this.buttonX6.SymbolSize = 9F;
            this.buttonX6.TabIndex = 44;
            this.buttonX6.Text = "حذف اطلاعات مشترک";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(10, 248);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(257, 37);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 43;
            this.buttonX4.Text = "مشاهده سوابق مشترک";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(10, 292);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(257, 37);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 42;
            this.buttonX1.Text = "قراردادهای الحاقیه ";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click_2);
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.ch_hae_sharj_ghabz);
            this.groupPanel5.Controls.Add(this.ch_have_ab_ghabz);
            this.groupPanel5.Controls.Add(this.chsakhtosaz);
            this.groupPanel5.Controls.Add(this.chtejari);
            this.groupPanel5.Controls.Add(this.checkBoxX1);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(10, 6);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(257, 183);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 38;
            this.groupPanel5.Text = "وضعیت مشترک";
            // 
            // ch_hae_sharj_ghabz
            // 
            this.ch_hae_sharj_ghabz.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ch_hae_sharj_ghabz.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ch_hae_sharj_ghabz.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.moshtarekinBindingSource, "have_sharj_ghabz", true));
            this.ch_hae_sharj_ghabz.ForeColor = System.Drawing.Color.Black;
            this.ch_hae_sharj_ghabz.Location = new System.Drawing.Point(19, 119);
            this.ch_hae_sharj_ghabz.Name = "ch_hae_sharj_ghabz";
            this.ch_hae_sharj_ghabz.Size = new System.Drawing.Size(198, 23);
            this.ch_hae_sharj_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ch_hae_sharj_ghabz.TabIndex = 8;
            this.ch_hae_sharj_ghabz.Text = "برای این مشترک قبض شارژ صادر شود";
            // 
            // ch_have_ab_ghabz
            // 
            this.ch_have_ab_ghabz.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ch_have_ab_ghabz.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ch_have_ab_ghabz.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.moshtarekinBindingSource, "have_ab_ghabz", true));
            this.ch_have_ab_ghabz.ForeColor = System.Drawing.Color.Black;
            this.ch_have_ab_ghabz.Location = new System.Drawing.Point(31, 94);
            this.ch_have_ab_ghabz.Name = "ch_have_ab_ghabz";
            this.ch_have_ab_ghabz.Size = new System.Drawing.Size(186, 23);
            this.ch_have_ab_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ch_have_ab_ghabz.TabIndex = 7;
            this.ch_have_ab_ghabz.Text = "برای این مشترک قبض آب صادر شود ";
            // 
            // chsakhtosaz
            // 
            this.chsakhtosaz.AutoSize = true;
            this.chsakhtosaz.BackColor = System.Drawing.Color.Transparent;
            this.chsakhtosaz.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.moshtarekinBindingSource, "darhalsakht", true));
            this.chsakhtosaz.ForeColor = System.Drawing.Color.Black;
            this.chsakhtosaz.Location = new System.Drawing.Point(19, 40);
            this.chsakhtosaz.Name = "chsakhtosaz";
            this.chsakhtosaz.Size = new System.Drawing.Size(198, 22);
            this.chsakhtosaz.TabIndex = 1;
            this.chsakhtosaz.Text = "این مشترک در حال ساخت و ساز است ";
            this.chsakhtosaz.UseVisualStyleBackColor = false;
            // 
            // chtejari
            // 
            this.chtejari.AutoSize = true;
            this.chtejari.BackColor = System.Drawing.Color.Transparent;
            this.chtejari.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.moshtarekinBindingSource, "tejari", true));
            this.chtejari.ForeColor = System.Drawing.Color.Black;
            this.chtejari.Location = new System.Drawing.Point(31, 8);
            this.chtejari.Name = "chtejari";
            this.chtejari.Size = new System.Drawing.Size(186, 22);
            this.chtejari.TabIndex = 0;
            this.chtejari.Text = "این مشترک دارای واحد تجاری است ";
            this.chtejari.UseVisualStyleBackColor = false;
            // 
            // checkBoxX1
            // 
            this.checkBoxX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.checkBoxX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.moshtarekinBindingSource, "vaziat", true));
            this.checkBoxX1.ForeColor = System.Drawing.Color.Black;
            this.checkBoxX1.Location = new System.Drawing.Point(30, 70);
            this.checkBoxX1.Name = "checkBoxX1";
            this.checkBoxX1.Size = new System.Drawing.Size(186, 23);
            this.checkBoxX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checkBoxX1.TabIndex = 6;
            this.checkBoxX1.Text = "این مشترک فعال می باشد";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.textBox2);
            this.groupPanel3.Controls.Add(this.textBox1);
            this.groupPanel3.Controls.Add(this.labelX23);
            this.groupPanel3.Controls.Add(this.labelX24);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(292, 265);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(388, 73);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 26;
            this.groupPanel3.Text = "حجم مخازن ";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "zamini", true));
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Location = new System.Drawing.Point(55, 8);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(69, 26);
            this.textBox2.TabIndex = 14;
            this.textBox2.Text = "0";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "havayi", true));
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(219, 8);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(69, 26);
            this.textBox1.TabIndex = 13;
            this.textBox1.Text = "0";
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(130, 10);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(38, 23);
            this.labelX23.TabIndex = 21;
            this.labelX23.Text = "زمینی :";
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(294, 9);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(41, 23);
            this.labelX24.TabIndex = 19;
            this.labelX24.Text = "هوایی:";
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.Font = new System.Drawing.Font("B Yekan", 12F);
            this.btn_save.Location = new System.Drawing.Point(10, 195);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Size = new System.Drawing.Size(257, 40);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 25;
            this.btn_save.Text = "ثبت کلیه تغییرات ";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.textBox4);
            this.groupPanel4.Controls.Add(this.txtKontorRaghan);
            this.groupPanel4.Controls.Add(this.labelX25);
            this.groupPanel4.Controls.Add(this.textBox3);
            this.groupPanel4.Controls.Add(this.integerInput6);
            this.groupPanel4.Controls.Add(this.labelX21);
            this.groupPanel4.Controls.Add(this.integerInput1);
            this.groupPanel4.Controls.Add(this.labelX20);
            this.groupPanel4.Controls.Add(this.labelX14);
            this.groupPanel4.Controls.Add(this.labelX15);
            this.groupPanel4.Controls.Add(this.labelX16);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Font = new System.Drawing.Font("B Yekan", 9F);
            this.groupPanel4.Location = new System.Drawing.Point(292, 344);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(866, 64);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 24;
            this.groupPanel4.Text = "اطلاعات تکمیلی";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.White;
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "size_ensheab", true));
            this.textBox4.ForeColor = System.Drawing.Color.Black;
            this.textBox4.Location = new System.Drawing.Point(495, 5);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(38, 26);
            this.textBox4.TabIndex = 22;
            this.textBox4.Text = "0";
            // 
            // txtKontorRaghan
            // 
            this.txtKontorRaghan.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtKontorRaghan.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtKontorRaghan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtKontorRaghan.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtKontorRaghan.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "kontor_ragham", true));
            this.txtKontorRaghan.ForeColor = System.Drawing.Color.Black;
            this.txtKontorRaghan.Location = new System.Drawing.Point(162, 5);
            this.txtKontorRaghan.MaxValue = 9999999;
            this.txtKontorRaghan.Name = "txtKontorRaghan";
            this.txtKontorRaghan.ShowUpDown = true;
            this.txtKontorRaghan.Size = new System.Drawing.Size(67, 26);
            this.txtKontorRaghan.TabIndex = 30;
            this.txtKontorRaghan.Value = 9999;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(235, 8);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(85, 20);
            this.labelX25.TabIndex = 31;
            this.labelX25.Text = "آخرین رقم کنتور";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "zarib_fazelab", true));
            this.textBox3.ForeColor = System.Drawing.Color.Black;
            this.textBox3.Location = new System.Drawing.Point(5, 5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(69, 26);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "0";
            // 
            // integerInput6
            // 
            this.integerInput6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.integerInput6.BackgroundStyle.Class = "DateTimeInputBackground";
            this.integerInput6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.integerInput6.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.integerInput6.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "serial_kontor", true));
            this.integerInput6.ForeColor = System.Drawing.Color.Black;
            this.integerInput6.Location = new System.Drawing.Point(344, 5);
            this.integerInput6.Name = "integerInput6";
            this.integerInput6.ShowUpDown = true;
            this.integerInput6.Size = new System.Drawing.Size(67, 26);
            this.integerInput6.TabIndex = 17;
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.Font = new System.Drawing.Font("B Yekan", 7F);
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(657, 7);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(50, 23);
            this.labelX21.TabIndex = 26;
            this.labelX21.Text = "مترمکعب";
            // 
            // integerInput1
            // 
            this.integerInput1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.integerInput1.BackgroundStyle.Class = "DateTimeInputBackground";
            this.integerInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.integerInput1.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.integerInput1.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "masrafe_mojaz", true));
            this.integerInput1.ForeColor = System.Drawing.Color.Black;
            this.integerInput1.Location = new System.Drawing.Point(707, 5);
            this.integerInput1.Name = "integerInput1";
            this.integerInput1.ShowUpDown = true;
            this.integerInput1.Size = new System.Drawing.Size(55, 26);
            this.integerInput1.TabIndex = 15;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(80, 8);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(76, 20);
            this.labelX20.TabIndex = 25;
            this.labelX20.Text = "ضریب فاضلاب :";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(417, 8);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(60, 20);
            this.labelX14.TabIndex = 23;
            this.labelX14.Text = "سریال کنتور:";
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(543, 8);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(82, 20);
            this.labelX15.TabIndex = 21;
            this.labelX15.Text = "سایز انشعاب :";
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(746, 8);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(89, 20);
            this.labelX16.TabIndex = 19;
            this.labelX16.Text = "مصرف مجاز :";
            // 
            // grp_personel
            // 
            this.grp_personel.BackColor = System.Drawing.Color.White;
            this.grp_personel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_personel.Controls.Add(this.integerInput4);
            this.grp_personel.Controls.Add(this.integerInput3);
            this.grp_personel.Controls.Add(this.integerInput2);
            this.grp_personel.Controls.Add(this.labelX10);
            this.grp_personel.Controls.Add(this.labelX9);
            this.grp_personel.Controls.Add(this.labelX11);
            this.grp_personel.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_personel.Location = new System.Drawing.Point(702, 265);
            this.grp_personel.Name = "grp_personel";
            this.grp_personel.Size = new System.Drawing.Size(456, 73);
            // 
            // 
            // 
            this.grp_personel.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_personel.Style.BackColorGradientAngle = 90;
            this.grp_personel.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_personel.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_personel.Style.BorderBottomWidth = 1;
            this.grp_personel.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_personel.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_personel.Style.BorderLeftWidth = 1;
            this.grp_personel.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_personel.Style.BorderRightWidth = 1;
            this.grp_personel.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_personel.Style.BorderTopWidth = 1;
            this.grp_personel.Style.CornerDiameter = 4;
            this.grp_personel.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_personel.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_personel.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_personel.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_personel.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_personel.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_personel.TabIndex = 15;
            this.grp_personel.Text = "تعداد پرسنل ها";
            // 
            // integerInput4
            // 
            this.integerInput4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.integerInput4.BackgroundStyle.Class = "DateTimeInputBackground";
            this.integerInput4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.integerInput4.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.integerInput4.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "karmand", true));
            this.integerInput4.ForeColor = System.Drawing.Color.Black;
            this.integerInput4.Location = new System.Drawing.Point(39, 9);
            this.integerInput4.Name = "integerInput4";
            this.integerInput4.ShowUpDown = true;
            this.integerInput4.Size = new System.Drawing.Size(55, 26);
            this.integerInput4.TabIndex = 12;
            // 
            // integerInput3
            // 
            this.integerInput3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.integerInput3.BackgroundStyle.Class = "DateTimeInputBackground";
            this.integerInput3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.integerInput3.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.integerInput3.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "serayedar", true));
            this.integerInput3.ForeColor = System.Drawing.Color.Black;
            this.integerInput3.Location = new System.Drawing.Point(169, 9);
            this.integerInput3.Name = "integerInput3";
            this.integerInput3.ShowUpDown = true;
            this.integerInput3.Size = new System.Drawing.Size(55, 26);
            this.integerInput3.TabIndex = 11;
            // 
            // integerInput2
            // 
            this.integerInput2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.integerInput2.BackgroundStyle.Class = "DateTimeInputBackground";
            this.integerInput2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.integerInput2.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.integerInput2.DataBindings.Add(new System.Windows.Forms.Binding("ValueObject", this.moshtarekinBindingSource, "karger", true));
            this.integerInput2.ForeColor = System.Drawing.Color.Black;
            this.integerInput2.Location = new System.Drawing.Point(326, 9);
            this.integerInput2.Name = "integerInput2";
            this.integerInput2.ShowUpDown = true;
            this.integerInput2.Size = new System.Drawing.Size(55, 26);
            this.integerInput2.TabIndex = 10;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(82, 10);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(53, 23);
            this.labelX10.TabIndex = 23;
            this.labelX10.Text = "کارمندی:";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(154, 10);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(133, 23);
            this.labelX9.TabIndex = 21;
            this.labelX9.Text = "سرایه داری :";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(326, 10);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(99, 23);
            this.labelX11.TabIndex = 19;
            this.labelX11.Text = "کارگری:";
            // 
            // grp_main
            // 
            this.grp_main.BackColor = System.Drawing.Color.White;
            this.grp_main.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_main.Controls.Add(this.tarikh_gheraat_ab);
            this.grp_main.Controls.Add(this.labelX36);
            this.grp_main.Controls.Add(this.textBoxX5);
            this.grp_main.Controls.Add(this.labelX35);
            this.grp_main.Controls.Add(this.txt_sh_variz);
            this.grp_main.Controls.Add(this.labelX33);
            this.grp_main.Controls.Add(this.txt_parvandeh);
            this.grp_main.Controls.Add(this.labelX34);
            this.grp_main.Controls.Add(this.textBoxX4);
            this.grp_main.Controls.Add(this.labelX32);
            this.grp_main.Controls.Add(this.textBoxX3);
            this.grp_main.Controls.Add(this.labelX31);
            this.grp_main.Controls.Add(this.textBoxX2);
            this.grp_main.Controls.Add(this.labelX13);
            this.grp_main.Controls.Add(this.textBoxX1);
            this.grp_main.Controls.Add(this.labelX8);
            this.grp_main.Controls.Add(this.txt_pass);
            this.grp_main.Controls.Add(this.labelX30);
            this.grp_main.Controls.Add(this.txt_code_posti);
            this.grp_main.Controls.Add(this.labelX27);
            this.grp_main.Controls.Add(this.txt_code_Eghtesadi);
            this.grp_main.Controls.Add(this.labelX28);
            this.grp_main.Controls.Add(this.txt_code_melli);
            this.grp_main.Controls.Add(this.labelX29);
            this.grp_main.Controls.Add(this.txt_metraj);
            this.grp_main.Controls.Add(this.txtghataat);
            this.grp_main.Controls.Add(this.labelX26);
            this.grp_main.Controls.Add(this.txtPelak);
            this.grp_main.Controls.Add(this.labelX22);
            this.grp_main.Controls.Add(this.faDatePicker1);
            this.grp_main.Controls.Add(this.txt_address);
            this.grp_main.Controls.Add(this.labelX12);
            this.grp_main.Controls.Add(this.labelX7);
            this.grp_main.Controls.Add(this.labelX6);
            this.grp_main.Controls.Add(this.txt_phone);
            this.grp_main.Controls.Add(this.labelX5);
            this.grp_main.Controls.Add(this.txt_mobile);
            this.grp_main.Controls.Add(this.labelX4);
            this.grp_main.Controls.Add(this.txt_modir_amel);
            this.grp_main.Controls.Add(this.labelX2);
            this.grp_main.Controls.Add(this.txt_co_name);
            this.grp_main.Controls.Add(this.labelX1);
            this.grp_main.Controls.Add(this.txt_gharardad);
            this.grp_main.Controls.Add(this.labelX3);
            this.grp_main.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_main.Location = new System.Drawing.Point(292, 6);
            this.grp_main.Name = "grp_main";
            this.grp_main.Size = new System.Drawing.Size(866, 253);
            // 
            // 
            // 
            this.grp_main.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_main.Style.BackColorGradientAngle = 90;
            this.grp_main.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_main.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderBottomWidth = 1;
            this.grp_main.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_main.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderLeftWidth = 1;
            this.grp_main.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderRightWidth = 1;
            this.grp_main.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_main.Style.BorderTopWidth = 1;
            this.grp_main.Style.CornerDiameter = 4;
            this.grp_main.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_main.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_main.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_main.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_main.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_main.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_main.TabIndex = 14;
            this.grp_main.Text = "اطلاعات اصلی";
            // 
            // tarikh_gheraat_ab
            // 
            this.tarikh_gheraat_ab.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.moshtarekinBindingSource, "tarikh_gharardad_ab", true));
            this.tarikh_gheraat_ab.Location = new System.Drawing.Point(58, 95);
            this.tarikh_gheraat_ab.Name = "tarikh_gheraat_ab";
            this.tarikh_gheraat_ab.SelectedDateTime = new System.DateTime(1393, 1, 1, 0, 0, 0, 0);
            this.tarikh_gheraat_ab.Size = new System.Drawing.Size(114, 20);
            this.tarikh_gheraat_ab.TabIndex = 58;
            this.tarikh_gheraat_ab.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.tarikh_gheraat_ab.SelectedDateTimeChanged += new System.EventHandler(this.faDatePicker2_SelectedDateTimeChanged);
            // 
            // labelX36
            // 
            this.labelX36.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX36.ForeColor = System.Drawing.Color.Black;
            this.labelX36.Location = new System.Drawing.Point(178, 94);
            this.labelX36.Name = "labelX36";
            this.labelX36.Size = new System.Drawing.Size(88, 23);
            this.labelX36.TabIndex = 59;
            this.labelX36.Text = "تاریخ قرارداد آب:";
            // 
            // textBoxX5
            // 
            this.textBoxX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX5.Border.Class = "TextBoxBorder";
            this.textBoxX5.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "shenase_variz_ab", true));
            this.textBoxX5.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX5.ForeColor = System.Drawing.Color.Black;
            this.textBoxX5.Location = new System.Drawing.Point(5, 151);
            this.textBoxX5.MaxLength = 0;
            this.textBoxX5.Name = "textBoxX5";
            this.textBoxX5.PreventEnterBeep = true;
            this.textBoxX5.Size = new System.Drawing.Size(119, 26);
            this.textBoxX5.TabIndex = 56;
            // 
            // labelX35
            // 
            this.labelX35.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.ForeColor = System.Drawing.Color.Black;
            this.labelX35.Location = new System.Drawing.Point(133, 154);
            this.labelX35.Name = "labelX35";
            this.labelX35.Size = new System.Drawing.Size(76, 23);
            this.labelX35.TabIndex = 57;
            this.labelX35.Text = "شانسه واریز آب:";
            // 
            // txt_sh_variz
            // 
            this.txt_sh_variz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sh_variz.Border.Class = "TextBoxBorder";
            this.txt_sh_variz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sh_variz.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "shenase_variz_sharj", true));
            this.txt_sh_variz.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sh_variz.ForeColor = System.Drawing.Color.Black;
            this.txt_sh_variz.Location = new System.Drawing.Point(5, 119);
            this.txt_sh_variz.MaxLength = 0;
            this.txt_sh_variz.Name = "txt_sh_variz";
            this.txt_sh_variz.PreventEnterBeep = true;
            this.txt_sh_variz.Size = new System.Drawing.Size(119, 26);
            this.txt_sh_variz.TabIndex = 54;
            // 
            // labelX33
            // 
            this.labelX33.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX33.ForeColor = System.Drawing.Color.Black;
            this.labelX33.Location = new System.Drawing.Point(124, 122);
            this.labelX33.Name = "labelX33";
            this.labelX33.Size = new System.Drawing.Size(85, 23);
            this.labelX33.TabIndex = 55;
            this.labelX33.Text = "شناسه واریز شارژ:";
            // 
            // txt_parvandeh
            // 
            this.txt_parvandeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_parvandeh.Border.Class = "TextBoxBorder";
            this.txt_parvandeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_parvandeh.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "parvandeh", true));
            this.txt_parvandeh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_parvandeh.ForeColor = System.Drawing.Color.Black;
            this.txt_parvandeh.Location = new System.Drawing.Point(216, 119);
            this.txt_parvandeh.MaxLength = 11;
            this.txt_parvandeh.Name = "txt_parvandeh";
            this.txt_parvandeh.PreventEnterBeep = true;
            this.txt_parvandeh.Size = new System.Drawing.Size(104, 26);
            this.txt_parvandeh.TabIndex = 52;
            // 
            // labelX34
            // 
            this.labelX34.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX34.ForeColor = System.Drawing.Color.Black;
            this.labelX34.Location = new System.Drawing.Point(320, 122);
            this.labelX34.Name = "labelX34";
            this.labelX34.Size = new System.Drawing.Size(81, 23);
            this.labelX34.TabIndex = 53;
            this.labelX34.Text = "شماره پرونده :";
            // 
            // textBoxX4
            // 
            this.textBoxX4.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX4.Border.Class = "TextBoxBorder";
            this.textBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "sharj_tafsili", true));
            this.textBoxX4.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX4.ForeColor = System.Drawing.Color.Black;
            this.textBoxX4.Location = new System.Drawing.Point(22, 186);
            this.textBoxX4.MaxLength = 14;
            this.textBoxX4.Name = "textBoxX4";
            this.textBoxX4.PreventEnterBeep = true;
            this.textBoxX4.Size = new System.Drawing.Size(101, 26);
            this.textBoxX4.TabIndex = 50;
            // 
            // labelX32
            // 
            this.labelX32.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.ForeColor = System.Drawing.Color.Black;
            this.labelX32.Location = new System.Drawing.Point(130, 189);
            this.labelX32.Name = "labelX32";
            this.labelX32.Size = new System.Drawing.Size(77, 23);
            this.labelX32.TabIndex = 51;
            this.labelX32.Text = "کدتفضیلی شارژ:";
            // 
            // textBoxX3
            // 
            this.textBoxX3.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX3.Border.Class = "TextBoxBorder";
            this.textBoxX3.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "sharj_moeen", true));
            this.textBoxX3.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX3.ForeColor = System.Drawing.Color.Black;
            this.textBoxX3.Location = new System.Drawing.Point(219, 186);
            this.textBoxX3.MaxLength = 14;
            this.textBoxX3.Name = "textBoxX3";
            this.textBoxX3.PreventEnterBeep = true;
            this.textBoxX3.Size = new System.Drawing.Size(101, 26);
            this.textBoxX3.TabIndex = 48;
            // 
            // labelX31
            // 
            this.labelX31.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.ForeColor = System.Drawing.Color.Black;
            this.labelX31.Location = new System.Drawing.Point(334, 189);
            this.labelX31.Name = "labelX31";
            this.labelX31.Size = new System.Drawing.Size(67, 23);
            this.labelX31.TabIndex = 49;
            this.labelX31.Text = "کدمعین شارژ:";
            // 
            // textBoxX2
            // 
            this.textBoxX2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX2.Border.Class = "TextBoxBorder";
            this.textBoxX2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "ab_tafsili", true));
            this.textBoxX2.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX2.ForeColor = System.Drawing.Color.Black;
            this.textBoxX2.Location = new System.Drawing.Point(407, 186);
            this.textBoxX2.MaxLength = 14;
            this.textBoxX2.Name = "textBoxX2";
            this.textBoxX2.PreventEnterBeep = true;
            this.textBoxX2.Size = new System.Drawing.Size(101, 26);
            this.textBoxX2.TabIndex = 46;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(531, 189);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(67, 23);
            this.labelX13.TabIndex = 47;
            this.labelX13.Text = "کدتفصیلی آب:";
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "ab_moeen", true));
            this.textBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX1.ForeColor = System.Drawing.Color.Black;
            this.textBoxX1.Location = new System.Drawing.Point(620, 186);
            this.textBoxX1.MaxLength = 14;
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(101, 26);
            this.textBoxX1.TabIndex = 44;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(731, 189);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(67, 23);
            this.labelX8.TabIndex = 45;
            this.labelX8.Text = "کدمعین آب:";
            // 
            // txt_pass
            // 
            this.txt_pass.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pass.Border.Class = "TextBoxBorder";
            this.txt_pass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pass.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "password", true));
            this.txt_pass.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pass.ForeColor = System.Drawing.Color.Black;
            this.txt_pass.Location = new System.Drawing.Point(279, 70);
            this.txt_pass.MaxLength = 11;
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PreventEnterBeep = true;
            this.txt_pass.Size = new System.Drawing.Size(135, 26);
            this.txt_pass.TabIndex = 42;
            // 
            // labelX30
            // 
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.ForeColor = System.Drawing.Color.Black;
            this.labelX30.Location = new System.Drawing.Point(420, 71);
            this.labelX30.Name = "labelX30";
            this.labelX30.Size = new System.Drawing.Size(67, 23);
            this.labelX30.TabIndex = 43;
            this.labelX30.Text = "رمز اینترنت:";
            // 
            // txt_code_posti
            // 
            this.txt_code_posti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_posti.Border.Class = "TextBoxBorder";
            this.txt_code_posti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_posti.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "code_posti", true));
            this.txt_code_posti.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_posti.ForeColor = System.Drawing.Color.Black;
            this.txt_code_posti.Location = new System.Drawing.Point(219, 153);
            this.txt_code_posti.MaxLength = 14;
            this.txt_code_posti.Name = "txt_code_posti";
            this.txt_code_posti.PreventEnterBeep = true;
            this.txt_code_posti.Size = new System.Drawing.Size(101, 26);
            this.txt_code_posti.TabIndex = 40;
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(334, 156);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(67, 23);
            this.labelX27.TabIndex = 41;
            this.labelX27.Text = "کدپستی:";
            // 
            // txt_code_Eghtesadi
            // 
            this.txt_code_Eghtesadi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_Eghtesadi.Border.Class = "TextBoxBorder";
            this.txt_code_Eghtesadi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_Eghtesadi.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "code_eghtesadi", true));
            this.txt_code_Eghtesadi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_Eghtesadi.ForeColor = System.Drawing.Color.Black;
            this.txt_code_Eghtesadi.Location = new System.Drawing.Point(407, 153);
            this.txt_code_Eghtesadi.MaxLength = 14;
            this.txt_code_Eghtesadi.Name = "txt_code_Eghtesadi";
            this.txt_code_Eghtesadi.PreventEnterBeep = true;
            this.txt_code_Eghtesadi.Size = new System.Drawing.Size(101, 26);
            this.txt_code_Eghtesadi.TabIndex = 38;
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(531, 156);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(67, 23);
            this.labelX28.TabIndex = 39;
            this.labelX28.Text = "کداقتصادی:";
            // 
            // txt_code_melli
            // 
            this.txt_code_melli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_code_melli.Border.Class = "TextBoxBorder";
            this.txt_code_melli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_code_melli.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "code_melli", true));
            this.txt_code_melli.DisabledBackColor = System.Drawing.Color.White;
            this.txt_code_melli.ForeColor = System.Drawing.Color.Black;
            this.txt_code_melli.Location = new System.Drawing.Point(620, 153);
            this.txt_code_melli.MaxLength = 14;
            this.txt_code_melli.Name = "txt_code_melli";
            this.txt_code_melli.PreventEnterBeep = true;
            this.txt_code_melli.Size = new System.Drawing.Size(101, 26);
            this.txt_code_melli.TabIndex = 36;
            // 
            // labelX29
            // 
            this.labelX29.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.ForeColor = System.Drawing.Color.Black;
            this.labelX29.Location = new System.Drawing.Point(731, 156);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(67, 23);
            this.labelX29.TabIndex = 37;
            this.labelX29.Text = "کدملی:";
            // 
            // txt_metraj
            // 
            this.txt_metraj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_metraj.Border.Class = "TextBoxBorder";
            this.txt_metraj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_metraj.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "metraj", true));
            this.txt_metraj.DisabledBackColor = System.Drawing.Color.White;
            this.txt_metraj.ForeColor = System.Drawing.Color.Black;
            this.txt_metraj.Location = new System.Drawing.Point(506, 70);
            this.txt_metraj.MaxLength = 11;
            this.txt_metraj.Name = "txt_metraj";
            this.txt_metraj.PreventEnterBeep = true;
            this.txt_metraj.Size = new System.Drawing.Size(71, 26);
            this.txt_metraj.TabIndex = 28;
            // 
            // txtghataat
            // 
            this.txtghataat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtghataat.Border.Class = "TextBoxBorder";
            this.txtghataat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtghataat.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "ghataat", true));
            this.txtghataat.DisabledBackColor = System.Drawing.Color.White;
            this.txtghataat.ForeColor = System.Drawing.Color.Black;
            this.txtghataat.Location = new System.Drawing.Point(638, 71);
            this.txtghataat.MaxLength = 11;
            this.txtghataat.Name = "txtghataat";
            this.txtghataat.PreventEnterBeep = true;
            this.txtghataat.Size = new System.Drawing.Size(83, 26);
            this.txtghataat.TabIndex = 26;
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(731, 73);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(67, 23);
            this.labelX26.TabIndex = 27;
            this.labelX26.Text = "قطعات :";
            // 
            // txtPelak
            // 
            this.txtPelak.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPelak.Border.Class = "TextBoxBorder";
            this.txtPelak.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPelak.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "pelak", true));
            this.txtPelak.DisabledBackColor = System.Drawing.Color.White;
            this.txtPelak.ForeColor = System.Drawing.Color.Black;
            this.txtPelak.Location = new System.Drawing.Point(58, 3);
            this.txtPelak.MaxLength = 11;
            this.txtPelak.Name = "txtPelak";
            this.txtPelak.PreventEnterBeep = true;
            this.txtPelak.Size = new System.Drawing.Size(135, 26);
            this.txtPelak.TabIndex = 20;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(199, 4);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(67, 23);
            this.labelX22.TabIndex = 21;
            this.labelX22.Text = "شماره پلاک:";
            // 
            // faDatePicker1
            // 
            this.faDatePicker1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.moshtarekinBindingSource, "tarikh_gharardad", true));
            this.faDatePicker1.Location = new System.Drawing.Point(58, 69);
            this.faDatePicker1.Name = "faDatePicker1";
            this.faDatePicker1.SelectedDateTime = new System.DateTime(1393, 1, 1, 0, 0, 0, 0);
            this.faDatePicker1.Size = new System.Drawing.Size(114, 20);
            this.faDatePicker1.TabIndex = 8;
            this.faDatePicker1.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.faDatePicker1.SelectedDateTimeChanged += new System.EventHandler(this.faDatePicker1_SelectedDateTimeChanged);
            // 
            // txt_address
            // 
            this.txt_address.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_address.Border.Class = "TextBoxBorder";
            this.txt_address.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_address.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "address", true));
            this.txt_address.DisabledBackColor = System.Drawing.Color.White;
            this.txt_address.ForeColor = System.Drawing.Color.Black;
            this.txt_address.Location = new System.Drawing.Point(407, 121);
            this.txt_address.Name = "txt_address";
            this.txt_address.PreventEnterBeep = true;
            this.txt_address.Size = new System.Drawing.Size(314, 26);
            this.txt_address.TabIndex = 9;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(731, 122);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(67, 23);
            this.labelX12.TabIndex = 18;
            this.labelX12.Text = "آدرس:";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(199, 68);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(67, 23);
            this.labelX7.TabIndex = 15;
            this.labelX7.Text = "تاریخ قرارداد:";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(583, 72);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(37, 23);
            this.labelX6.TabIndex = 13;
            this.labelX6.Text = "متراژ:";
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_phone.Border.Class = "TextBoxBorder";
            this.txt_phone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_phone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "phone", true));
            this.txt_phone.DisabledBackColor = System.Drawing.Color.White;
            this.txt_phone.ForeColor = System.Drawing.Color.Black;
            this.txt_phone.Location = new System.Drawing.Point(58, 38);
            this.txt_phone.MaxLength = 11;
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.PreventEnterBeep = true;
            this.txt_phone.Size = new System.Drawing.Size(135, 26);
            this.txt_phone.TabIndex = 4;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(199, 39);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(67, 23);
            this.labelX5.TabIndex = 10;
            this.labelX5.Text = "تلفن ثابت :";
            // 
            // txt_mobile
            // 
            this.txt_mobile.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mobile.Border.Class = "TextBoxBorder";
            this.txt_mobile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mobile.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "mobile", true));
            this.txt_mobile.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mobile.ForeColor = System.Drawing.Color.Black;
            this.txt_mobile.Location = new System.Drawing.Point(279, 38);
            this.txt_mobile.MaxLength = 11;
            this.txt_mobile.Name = "txt_mobile";
            this.txt_mobile.PreventEnterBeep = true;
            this.txt_mobile.Size = new System.Drawing.Size(135, 26);
            this.txt_mobile.TabIndex = 3;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(407, 39);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(67, 23);
            this.labelX4.TabIndex = 8;
            this.labelX4.Text = "تلفن همراه :";
            // 
            // txt_modir_amel
            // 
            this.txt_modir_amel.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_modir_amel.Border.Class = "TextBoxBorder";
            this.txt_modir_amel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_modir_amel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "modir_amel", true));
            this.txt_modir_amel.DisabledBackColor = System.Drawing.Color.White;
            this.txt_modir_amel.ForeColor = System.Drawing.Color.Black;
            this.txt_modir_amel.Location = new System.Drawing.Point(506, 38);
            this.txt_modir_amel.Name = "txt_modir_amel";
            this.txt_modir_amel.PreventEnterBeep = true;
            this.txt_modir_amel.Size = new System.Drawing.Size(215, 26);
            this.txt_modir_amel.TabIndex = 2;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(731, 39);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(67, 23);
            this.labelX2.TabIndex = 6;
            this.labelX2.Text = "مدیریت عامل :";
            // 
            // txt_co_name
            // 
            this.txt_co_name.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_co_name.Border.Class = "TextBoxBorder";
            this.txt_co_name.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_co_name.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "co_name", true));
            this.txt_co_name.DisabledBackColor = System.Drawing.Color.White;
            this.txt_co_name.ForeColor = System.Drawing.Color.Black;
            this.txt_co_name.Location = new System.Drawing.Point(279, 3);
            this.txt_co_name.Name = "txt_co_name";
            this.txt_co_name.PreventEnterBeep = true;
            this.txt_co_name.Size = new System.Drawing.Size(260, 26);
            this.txt_co_name.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(415, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(219, 23);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "نام مشترک(کارخانه) :";
            // 
            // txt_gharardad
            // 
            this.txt_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_gharardad.Border.Class = "TextBoxBorder";
            this.txt_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_gharardad.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "gharardad", true));
            this.txt_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_gharardad.Location = new System.Drawing.Point(657, 3);
            this.txt_gharardad.Name = "txt_gharardad";
            this.txt_gharardad.PreventEnterBeep = true;
            this.txt_gharardad.ReadOnly = true;
            this.txt_gharardad.Size = new System.Drawing.Size(64, 26);
            this.txt_gharardad.TabIndex = 0;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(731, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(67, 23);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "کد قرارداد:";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(7, 89);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(204, 29);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 40;
            this.buttonX2.Text = "چاپ اطلاعات کلیه مشترکین";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX3);
            this.groupPanel1.Controls.Add(this.txt_find_by_modir_amel);
            this.groupPanel1.Controls.Add(this.txt_find_by_moshtarek);
            this.groupPanel1.Controls.Add(this.txt_find_by_gharardad);
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.btn_all);
            this.groupPanel1.Controls.Add(this.dg_moshtarek);
            this.groupPanel1.Controls.Add(this.btn_find_modir_amel);
            this.groupPanel1.Controls.Add(this.labelX19);
            this.groupPanel1.Controls.Add(this.btn_find_moshtarek);
            this.groupPanel1.Controls.Add(this.labelX18);
            this.groupPanel1.Controls.Add(this.btn_find_gharardad);
            this.groupPanel1.Controls.Add(this.labelX17);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(13, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1210, 198);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "انتخاب مشترک";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(7, 124);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(204, 44);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 41;
            this.buttonX3.Text = "چاپ اطلاعات مشترکین دارای قرارداد آب";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // txt_find_by_modir_amel
            // 
            this.txt_find_by_modir_amel.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_by_modir_amel.Border.Class = "TextBoxBorder";
            this.txt_find_by_modir_amel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_by_modir_amel.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_by_modir_amel.ForeColor = System.Drawing.Color.Black;
            this.txt_find_by_modir_amel.Location = new System.Drawing.Point(950, 105);
            this.txt_find_by_modir_amel.Name = "txt_find_by_modir_amel";
            this.txt_find_by_modir_amel.PreventEnterBeep = true;
            this.txt_find_by_modir_amel.Size = new System.Drawing.Size(149, 26);
            this.txt_find_by_modir_amel.TabIndex = 28;
            this.txt_find_by_modir_amel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_by_modir_amel_KeyDown_1);
            // 
            // txt_find_by_moshtarek
            // 
            this.txt_find_by_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_by_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_find_by_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_by_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_by_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_find_by_moshtarek.Location = new System.Drawing.Point(950, 70);
            this.txt_find_by_moshtarek.Name = "txt_find_by_moshtarek";
            this.txt_find_by_moshtarek.PreventEnterBeep = true;
            this.txt_find_by_moshtarek.Size = new System.Drawing.Size(149, 26);
            this.txt_find_by_moshtarek.TabIndex = 27;
            this.txt_find_by_moshtarek.TextChanged += new System.EventHandler(this.txt_find_by_moshtarek_TextChanged);
            this.txt_find_by_moshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_by_moshtarek_KeyDown_1);
            // 
            // txt_find_by_gharardad
            // 
            this.txt_find_by_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_by_gharardad.Border.Class = "TextBoxBorder";
            this.txt_find_by_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_by_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_by_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_find_by_gharardad.Location = new System.Drawing.Point(1045, 36);
            this.txt_find_by_gharardad.Name = "txt_find_by_gharardad";
            this.txt_find_by_gharardad.PreventEnterBeep = true;
            this.txt_find_by_gharardad.Size = new System.Drawing.Size(54, 26);
            this.txt_find_by_gharardad.TabIndex = 20;
            this.txt_find_by_gharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_by_gharardad_KeyDown_1);
            // 
            // btn_all
            // 
            this.btn_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_all.Location = new System.Drawing.Point(7, 12);
            this.btn_all.Name = "btn_all";
            this.btn_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_all.Size = new System.Drawing.Size(204, 50);
            this.btn_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_all.Symbol = "";
            this.btn_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_all.SymbolSize = 9F;
            this.btn_all.TabIndex = 26;
            this.btn_all.Text = "نمایش کلیه مشترکین";
            this.btn_all.Click += new System.EventHandler(this.btn_all_Click);
            // 
            // dg_moshtarek
            // 
            this.dg_moshtarek.AllowUserToAddRows = false;
            this.dg_moshtarek.AllowUserToDeleteRows = false;
            this.dg_moshtarek.AllowUserToResizeColumns = false;
            this.dg_moshtarek.AllowUserToResizeRows = false;
            this.dg_moshtarek.AutoGenerateColumns = false;
            this.dg_moshtarek.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_moshtarek.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_moshtarek.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_moshtarek.ColumnHeadersHeight = 29;
            this.dg_moshtarek.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.modiramelDataGridViewTextBoxColumn});
            this.dg_moshtarek.DataSource = this.moshtarekinBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_moshtarek.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_moshtarek.EnableHeadersVisualStyles = false;
            this.dg_moshtarek.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dg_moshtarek.Location = new System.Drawing.Point(222, 12);
            this.dg_moshtarek.Name = "dg_moshtarek";
            this.dg_moshtarek.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_moshtarek.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_moshtarek.RowHeadersVisible = false;
            this.dg_moshtarek.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_moshtarek.Size = new System.Drawing.Size(668, 135);
            this.dg_moshtarek.TabIndex = 12;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 43.94849F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.FillWeight = 134.2241F;
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام مشترک";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modiramelDataGridViewTextBoxColumn
            // 
            this.modiramelDataGridViewTextBoxColumn.DataPropertyName = "modir_amel";
            this.modiramelDataGridViewTextBoxColumn.FillWeight = 121.8274F;
            this.modiramelDataGridViewTextBoxColumn.HeaderText = "مدیریت عامل";
            this.modiramelDataGridViewTextBoxColumn.Name = "modiramelDataGridViewTextBoxColumn";
            this.modiramelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // btn_find_modir_amel
            // 
            this.btn_find_modir_amel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_modir_amel.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_modir_amel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_modir_amel.Location = new System.Drawing.Point(912, 105);
            this.btn_find_modir_amel.Name = "btn_find_modir_amel";
            this.btn_find_modir_amel.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.btn_find_modir_amel.Size = new System.Drawing.Size(32, 23);
            this.btn_find_modir_amel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_modir_amel.Symbol = "";
            this.btn_find_modir_amel.SymbolSize = 9F;
            this.btn_find_modir_amel.TabIndex = 11;
            this.btn_find_modir_amel.Click += new System.EventHandler(this.btn_find_modir_amel_Click);
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(1107, 106);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(67, 23);
            this.labelX19.TabIndex = 10;
            this.labelX19.Text = "مدیریت عامل :";
            // 
            // btn_find_moshtarek
            // 
            this.btn_find_moshtarek.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_moshtarek.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_moshtarek.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_moshtarek.Location = new System.Drawing.Point(912, 70);
            this.btn_find_moshtarek.Name = "btn_find_moshtarek";
            this.btn_find_moshtarek.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.btn_find_moshtarek.Size = new System.Drawing.Size(32, 23);
            this.btn_find_moshtarek.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_moshtarek.Symbol = "";
            this.btn_find_moshtarek.SymbolSize = 9F;
            this.btn_find_moshtarek.TabIndex = 8;
            this.btn_find_moshtarek.Click += new System.EventHandler(this.btn_find_moshtarek_Click);
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(1104, 71);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(70, 23);
            this.labelX18.TabIndex = 7;
            this.labelX18.Text = "نام مشترک :";
            // 
            // btn_find_gharardad
            // 
            this.btn_find_gharardad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_gharardad.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_gharardad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_gharardad.Location = new System.Drawing.Point(1007, 37);
            this.btn_find_gharardad.Name = "btn_find_gharardad";
            this.btn_find_gharardad.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.btn_find_gharardad.Size = new System.Drawing.Size(32, 23);
            this.btn_find_gharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_gharardad.Symbol = "";
            this.btn_find_gharardad.SymbolSize = 9F;
            this.btn_find_gharardad.TabIndex = 5;
            this.btn_find_gharardad.Click += new System.EventHandler(this.btn_find_gharardad_Click);
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(1107, 37);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(67, 23);
            this.labelX17.TabIndex = 4;
            this.labelX17.Text = "کد قرارداد:";
            // 
            // frm_moshtarek_all
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1255, 662);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(853, 680);
            this.Name = "frm_moshtarek_all";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm_all_moshtarek_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_all_moshtarek_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            this.groupPanel5.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKontorRaghan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput1)).EndInit();
            this.grp_personel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.integerInput4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.integerInput2)).EndInit();
            this.grp_main.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_moshtarek)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_personel;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_main;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_address;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_phone;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mobile;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_modir_amel;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_co_name;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_gharardad;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.DataGridViewX dg_moshtarek;
        private DevComponents.DotNetBar.ButtonX btn_find_modir_amel;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.ButtonX btn_find_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.ButtonX btn_find_gharardad;
        private DevComponents.DotNetBar.LabelX labelX17;
        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource moshtarekinBindingSource;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBoxX1;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.ButtonX btn_all;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modiramelDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker1;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.Editors.IntegerInput integerInput6;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.Editors.IntegerInput integerInput1;
        private DevComponents.Editors.IntegerInput integerInput4;
        private DevComponents.Editors.IntegerInput integerInput3;
        private DevComponents.Editors.IntegerInput integerInput2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_by_gharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_by_modir_amel;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_by_moshtarek;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private System.Windows.Forms.CheckBox chsakhtosaz;
        private System.Windows.Forms.CheckBox chtejari;
        private MainDataSest mainDataSest;
        private MainDataSestTableAdapters.moshtarekinTableAdapter moshtarekinTableAdapter1;
        private DevComponents.Editors.IntegerInput txtKontorRaghan;
        private DevComponents.DotNetBar.LabelX labelX25;
        private System.Windows.Forms.TextBox textBox4;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPelak;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtghataat;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_metraj;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_posti;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_Eghtesadi;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_code_melli;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_pass;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX4;
        private DevComponents.DotNetBar.LabelX labelX32;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX3;
        private DevComponents.DotNetBar.LabelX labelX31;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX2;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sh_variz;
        private DevComponents.DotNetBar.LabelX labelX33;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_parvandeh;
        private DevComponents.DotNetBar.LabelX labelX34;
        private FarsiLibrary.Win.Controls.FADatePicker tarikh_gheraat_ab;
        private DevComponents.DotNetBar.LabelX labelX36;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX5;
        private DevComponents.DotNetBar.LabelX labelX35;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.Controls.CheckBoxX ch_hae_sharj_ghabz;
        private DevComponents.DotNetBar.Controls.CheckBoxX ch_have_ab_ghabz;
    }
}