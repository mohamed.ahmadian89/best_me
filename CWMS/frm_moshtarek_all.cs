﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CWMS.Classes;
using CrystalDecisions.Windows.Forms;

namespace CWMS
{
    public partial class frm_moshtarek_all : MyMetroForm

    {
        public frm_moshtarek_all()
        {
            InitializeComponent();
        }

        private void frm_all_moshtarek_Load(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                try
                {


                    Classes.ClsMain.ExecuteNoneQuery(
                        "update moshtarekin set have_sharj_ghabz=0 where metraj=0;" +
                        "update moshtarekin set have_ab_ghabz=0 where isnull(tarikh_gharardad_ab,1)=1"
                        );

                }
                catch
                {

                }
            }
            this.moshtarekinTableAdapter1.Fill(this.mainDataSest.moshtarekin);
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                

                if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                {
                    if (faDatePicker1.SelectedDateTime == null || faDatePicker1.Text.Trim() == "")
                        ch_hae_sharj_ghabz.Checked = false;
                    else
                        ch_hae_sharj_ghabz.Checked = true;


                    if (tarikh_gheraat_ab.SelectedDateTime == null || tarikh_gheraat_ab.Text.Trim() == "")
                        ch_have_ab_ghabz.Checked = false;
                    else
                        ch_have_ab_ghabz.Checked = true;
                }


                moshtarekinBindingSource.EndEdit();

                moshtarekinTableAdapter1.Update(mainDataSest.moshtarekin);
                Payam.Show("اطلاعات با موفقیت در سیستم ثبت شد");
                Classes.ClsMain.MoshtarekDT = mainDataSest.moshtarekin;



            }
            catch (Exception ex)
            {
                ClsMain.logError(ex);
                Payam.Show("خطا! لطفا با پشتیبان هماهنگ نمایید");
            }
        }

        private void txt_find_by_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_moshtarek.PerformClick();
        }

        private void btn_find_moshtarek_Click(object sender, EventArgs e)
        {
            txt_find_by_gharardad.Text = txt_find_by_modir_amel.Text = "";
            moshtarekinBindingSource.Filter = "co_name like '%" + txt_find_by_moshtarek.Text + "%'";

        }

        private void btn_find_modir_amel_Click(object sender, EventArgs e)
        {
            txt_find_by_gharardad.Text = txt_find_by_moshtarek.Text = "";
            moshtarekinBindingSource.Filter = "modir_amel like '%" + txt_find_by_modir_amel.Text + "%'";
            txt_find_by_modir_amel.SelectAll();
            txt_find_by_modir_amel.Focus();
        }

        private void btn_find_gharardad_Click(object sender, EventArgs e)
        {
            if (txt_find_by_gharardad.Text.Trim() != "")
            {
                txt_find_by_modir_amel.Text = txt_find_by_moshtarek.Text = "";
                moshtarekinBindingSource.Filter = "gharardad=" + txt_find_by_gharardad.Text;
                txt_find_by_gharardad.SelectAll();
                txt_find_by_gharardad.Focus();
            }

        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            moshtarekinBindingSource.RemoveFilter();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_all_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
            else if (e.KeyCode == Keys.ControlKey)
                txt_find_by_gharardad.Focus();
        }

        private void txt_find_by_moshtarek_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_find_moshtarek.PerformClick();
            }
        }

        private void txt_find_by_gharardad_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (txt_find_by_gharardad.Text.Trim() != "" && e.KeyCode == Keys.Enter)
            {
                btn_find_gharardad.PerformClick();
            }
        }

        private void txt_find_by_modir_amel_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_find_modir_amel.PerformClick();
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {

            Classes.clsMoshtarekin.ChapMoshtarekin();

        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {

        }


        private void buttonX1_Click_2(object sender, EventArgs e)
        {
            if (txt_gharardad.Text != "")
            {
                new FrmElhaghi(Convert.ToInt32(txt_gharardad.Text)).ShowDialog();
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            new FrmMoshtarekSavabegh(txt_gharardad.Text).ShowDialog();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonX5_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با حذف کلیه اطلاعات مربوط به مشترک موافق هستید؟") == DialogResult.Yes)
            {
                string gharardad = txt_gharardad.Text;
                Classes.ClsMain.ExecuteNoneQuery("delete from ab_ghabz where gharardad=" + gharardad + ";" +
                "delete from sharj_ghabz where gharardad=" + gharardad + ";" +
                "delete from ghabz_pardakht where gharardad=" + gharardad + ";" +
                "delete from barge_khorooj where gharardad=" + gharardad + ";" +
                "delete from barge_khorooj_details where gharardad=" + gharardad);


                moshtarekinBindingSource.RemoveCurrent();
                moshtarekinTableAdapter1.Update(mainDataSest.moshtarekin);

                Payam.Show("اطلاعات مشترک با موفقیت از سیستم حذف شد");

            }
        }

        private void txt_find_by_moshtarek_TextChanged(object sender, EventArgs e)
        {
            txt_find_by_gharardad.Text = txt_find_by_modir_amel.Text = "";
            moshtarekinBindingSource.Filter = "co_name like '%" + txt_find_by_moshtarek.Text + "%'";

        }

        private void faDatePicker2_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            if (tarikh_gheraat_ab.SelectedDateTime == null || tarikh_gheraat_ab.Text.Trim()=="")
                ch_have_ab_ghabz.Checked = false;
            else
                ch_have_ab_ghabz.Checked = true;
        }

        private void faDatePicker1_SelectedDateTimeChanged(object sender, EventArgs e)
        {

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Classes.clsMoshtarekin.ChapMoshtarekin(true);

        }

        private void buttonX5_Click_2(object sender, EventArgs e)
        {
            new frm_moshtarek_emza(Convert.ToInt32(txt_gharardad.Text)).ShowDialog();
        }
    }
}