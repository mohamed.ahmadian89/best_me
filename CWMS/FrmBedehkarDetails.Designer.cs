﻿namespace CWMS
{
    partial class FrmBedehkarDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_find_ghabz_byCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.sharj_ghabzTableAdapter = new CWMS.CWMSDataSetTableAdapters.sharj_ghabzTableAdapter();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtmablaghkolhorrof = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtmablaghkol = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtcount = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.dataGridViewX1);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(3, 46);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(820, 343);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 45;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.AllowUserToResizeColumns = false;
            this.dataGridViewX1.AllowUserToResizeRows = false;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcodeDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.mablaghkolDataGridViewTextBoxColumn});
            this.dataGridViewX1.DataSource = this.sharjghabzBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(13, 4);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(781, 330);
            this.dataGridViewX1.TabIndex = 0;
            this.dataGridViewX1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellContentClick);
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "کددوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjghabzBindingSource
            // 
            this.sharjghabzBindingSource.DataMember = "sharj_ghabz";
            this.sharjghabzBindingSource.DataSource = this.cWMSDataSet;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_moshtarek);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.txt_find_ghabz_byCode);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(820, 36);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 44;
            // 
            // txt_moshtarek
            // 
            this.txt_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek.Location = new System.Drawing.Point(112, 3);
            this.txt_moshtarek.Name = "txt_moshtarek";
            this.txt_moshtarek.PreventEnterBeep = true;
            this.txt_moshtarek.ReadOnly = true;
            this.txt_moshtarek.Size = new System.Drawing.Size(323, 24);
            this.txt_moshtarek.TabIndex = 28;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(441, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(61, 23);
            this.labelX2.TabIndex = 27;
            this.labelX2.Text = "نام مشترک:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txt_find_ghabz_byCode
            // 
            this.txt_find_ghabz_byCode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_ghabz_byCode.Border.Class = "TextBoxBorder";
            this.txt_find_ghabz_byCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_ghabz_byCode.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_ghabz_byCode.ForeColor = System.Drawing.Color.Black;
            this.txt_find_ghabz_byCode.Location = new System.Drawing.Point(534, 3);
            this.txt_find_ghabz_byCode.Name = "txt_find_ghabz_byCode";
            this.txt_find_ghabz_byCode.PreventEnterBeep = true;
            this.txt_find_ghabz_byCode.ReadOnly = true;
            this.txt_find_ghabz_byCode.Size = new System.Drawing.Size(109, 24);
            this.txt_find_ghabz_byCode.TabIndex = 26;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(634, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(61, 23);
            this.labelX1.TabIndex = 25;
            this.labelX1.Text = "قرارداد:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // sharj_ghabzTableAdapter
            // 
            this.sharj_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txtmablaghkolhorrof);
            this.groupPanel2.Controls.Add(this.txtmablaghkol);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.Controls.Add(this.txtcount);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(3, 395);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(820, 36);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 45;
            // 
            // txtmablaghkolhorrof
            // 
            this.txtmablaghkolhorrof.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmablaghkolhorrof.Border.Class = "TextBoxBorder";
            this.txtmablaghkolhorrof.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmablaghkolhorrof.DisabledBackColor = System.Drawing.Color.White;
            this.txtmablaghkolhorrof.ForeColor = System.Drawing.Color.Black;
            this.txtmablaghkolhorrof.Location = new System.Drawing.Point(60, 4);
            this.txtmablaghkolhorrof.Name = "txtmablaghkolhorrof";
            this.txtmablaghkolhorrof.PreventEnterBeep = true;
            this.txtmablaghkolhorrof.ReadOnly = true;
            this.txtmablaghkolhorrof.Size = new System.Drawing.Size(363, 24);
            this.txtmablaghkolhorrof.TabIndex = 29;
            // 
            // txtmablaghkol
            // 
            this.txtmablaghkol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmablaghkol.Border.Class = "TextBoxBorder";
            this.txtmablaghkol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmablaghkol.DisabledBackColor = System.Drawing.Color.White;
            this.txtmablaghkol.ForeColor = System.Drawing.Color.Black;
            this.txtmablaghkol.Location = new System.Drawing.Point(429, 4);
            this.txtmablaghkol.Name = "txtmablaghkol";
            this.txtmablaghkol.PreventEnterBeep = true;
            this.txtmablaghkol.ReadOnly = true;
            this.txtmablaghkol.Size = new System.Drawing.Size(93, 24);
            this.txtmablaghkol.TabIndex = 28;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(528, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(61, 23);
            this.labelX3.TabIndex = 27;
            this.labelX3.Text = "مجموع مبالغ:";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // txtcount
            // 
            this.txtcount.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtcount.Border.Class = "TextBoxBorder";
            this.txtcount.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtcount.DisabledBackColor = System.Drawing.Color.White;
            this.txtcount.ForeColor = System.Drawing.Color.Black;
            this.txtcount.Location = new System.Drawing.Point(660, 4);
            this.txtcount.Name = "txtcount";
            this.txtcount.PreventEnterBeep = true;
            this.txtcount.ReadOnly = true;
            this.txtcount.Size = new System.Drawing.Size(70, 24);
            this.txtcount.TabIndex = 26;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(736, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(61, 23);
            this.labelX4.TabIndex = 25;
            this.labelX4.Text = "تعداد قبوض:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // FrmBedehkarDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 437);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBedehkarDetails";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmBedehkarDetails_Load);
            this.groupPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_ghabz_byCode;
        private DevComponents.DotNetBar.LabelX labelX1;
        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource sharjghabzBindingSource;
        private CWMSDataSetTableAdapters.sharj_ghabzTableAdapter sharj_ghabzTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtmablaghkolhorrof;
        private DevComponents.DotNetBar.Controls.TextBoxX txtmablaghkol;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtcount;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}