﻿namespace CWMS
{
    partial class Payam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.lblpayam = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(230, 63);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Size = new System.Drawing.Size(141, 34);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 1;
            this.buttonX1.Text = "تایید";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // lblpayam
            // 
            this.lblpayam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblpayam.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lblpayam.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblpayam.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lblpayam.ForeColor = System.Drawing.Color.Black;
            this.lblpayam.Location = new System.Drawing.Point(13, 13);
            this.lblpayam.Name = "lblpayam";
            this.lblpayam.Size = new System.Drawing.Size(572, 48);
            this.lblpayam.TabIndex = 0;
            this.lblpayam.Text = "اطلاعات با موفقیت در سیستم ثبت شد\r\nاطلاعات با موفقیت در سیستم ثبت شد";
            this.lblpayam.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblpayam.WordWrap = true;
            this.lblpayam.Click += new System.EventHandler(this.lblpayam_Click);
            this.lblpayam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lblpayam_KeyDown);
            // 
            // Payam
            // 
            this.AcceptButton = this.buttonX1;
            this.ClientSize = new System.Drawing.Size(623, 106);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.lblpayam);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(519, 106);
            this.Name = "Payam";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Payam_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblpayam;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}