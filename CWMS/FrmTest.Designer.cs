﻿namespace CWMS
{
    partial class FrmTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminuseraccesstypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.admin_user_access_typeTableAdapter = new CWMS.CWMSDataSetTableAdapters.admin_user_access_typeTableAdapter();
            this.txtfind = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtmasraf_mojaz = new System.Windows.Forms.TextBox();
            this.txtmasraf_kol = new System.Windows.Forms.TextBox();
            this.txtmasraf_gheir_mojaz = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DgvResult = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ض = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tadarsadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaribabbahaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jarimemasrafmazadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.label4 = new System.Windows.Forms.Label();
            this.jarime_masraf_mazadTableAdapter = new CWMS.AbDatasetTableAdapters.Jarime_masraf_mazadTableAdapter();
            this.label5 = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.faDatePicker1 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.fa_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.button4 = new System.Windows.Forms.Button();
            this.textboxPrice1 = new KohansalSoft.TextboxPrice();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox1 = new CWMS.FloatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminuseraccesstypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jarimemasrafmazadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeight = 25;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.actionDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.adminuseraccesstypeBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(40, 385);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(779, 121);
            this.dataGridView1.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.FillWeight = 50F;
            this.idDataGridViewTextBoxColumn.HeaderText = "شناسه";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // actionDataGridViewTextBoxColumn
            // 
            this.actionDataGridViewTextBoxColumn.DataPropertyName = "action";
            this.actionDataGridViewTextBoxColumn.HeaderText = "عملیات";
            this.actionDataGridViewTextBoxColumn.Name = "actionDataGridViewTextBoxColumn";
            // 
            // adminuseraccesstypeBindingSource
            // 
            this.adminuseraccesstypeBindingSource.DataMember = "admin_user_access_type";
            this.adminuseraccesstypeBindingSource.DataSource = this.cWMSDataSet;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // admin_user_access_typeTableAdapter
            // 
            this.admin_user_access_typeTableAdapter.ClearBeforeFill = true;
            // 
            // txtfind
            // 
            this.txtfind.Location = new System.Drawing.Point(40, 344);
            this.txtfind.Name = "txtfind";
            this.txtfind.Size = new System.Drawing.Size(779, 24);
            this.txtfind.TabIndex = 1;
            this.txtfind.TextChanged += new System.EventHandler(this.txtfind_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(396, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "نمایش ارایه مربوطه";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // txtmasraf_mojaz
            // 
            this.txtmasraf_mojaz.Location = new System.Drawing.Point(373, 49);
            this.txtmasraf_mojaz.Name = "txtmasraf_mojaz";
            this.txtmasraf_mojaz.Size = new System.Drawing.Size(100, 24);
            this.txtmasraf_mojaz.TabIndex = 0;
            this.txtmasraf_mojaz.TextChanged += new System.EventHandler(this.txtmasraf_mojaz_TextChanged);
            // 
            // txtmasraf_kol
            // 
            this.txtmasraf_kol.Location = new System.Drawing.Point(373, 109);
            this.txtmasraf_kol.Name = "txtmasraf_kol";
            this.txtmasraf_kol.Size = new System.Drawing.Size(100, 24);
            this.txtmasraf_kol.TabIndex = 4;
            this.txtmasraf_kol.TextChanged += new System.EventHandler(this.txtmasraf_kol_TextChanged);
            // 
            // txtmasraf_gheir_mojaz
            // 
            this.txtmasraf_gheir_mojaz.Location = new System.Drawing.Point(373, 79);
            this.txtmasraf_gheir_mojaz.Name = "txtmasraf_gheir_mojaz";
            this.txtmasraf_gheir_mojaz.Size = new System.Drawing.Size(100, 24);
            this.txtmasraf_gheir_mojaz.TabIndex = 1;
            this.txtmasraf_gheir_mojaz.TextChanged += new System.EventHandler(this.txtmasraf_gheir_mojaz_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(290, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "مصرف مجاز:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "مصرف کل:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(290, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "مصرف غیر مجاز:";
            // 
            // DgvResult
            // 
            this.DgvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.ض,
            this.Column2,
            this.Column3});
            this.DgvResult.Location = new System.Drawing.Point(602, 29);
            this.DgvResult.Name = "DgvResult";
            this.DgvResult.RowHeadersVisible = false;
            this.DgvResult.Size = new System.Drawing.Size(398, 279);
            this.DgvResult.TabIndex = 9;
            // 
            // Column1
            // 
            this.Column1.FillWeight = 50F;
            this.Column1.HeaderText = "درصد";
            this.Column1.Name = "Column1";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "درصد/مصرف";
            this.Column4.Name = "Column4";
            // 
            // ض
            // 
            this.ض.FillWeight = 50F;
            this.ض.HeaderText = "ضریب";
            this.ض.Name = "ض";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "مصرف";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "هزینه";
            this.Column3.Name = "Column3";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tadarsadDataGridViewTextBoxColumn,
            this.zaribabbahaDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.jarimemasrafmazadBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(54, 49);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(204, 259);
            this.dataGridView2.TabIndex = 10;
            // 
            // tadarsadDataGridViewTextBoxColumn
            // 
            this.tadarsadDataGridViewTextBoxColumn.DataPropertyName = "ta_darsad";
            this.tadarsadDataGridViewTextBoxColumn.HeaderText = "تا درصد";
            this.tadarsadDataGridViewTextBoxColumn.Name = "tadarsadDataGridViewTextBoxColumn";
            // 
            // zaribabbahaDataGridViewTextBoxColumn
            // 
            this.zaribabbahaDataGridViewTextBoxColumn.DataPropertyName = "zarib_ab_baha";
            this.zaribabbahaDataGridViewTextBoxColumn.HeaderText = "ضریب اب ها";
            this.zaribabbahaDataGridViewTextBoxColumn.Name = "zaribabbahaDataGridViewTextBoxColumn";
            // 
            // jarimemasrafmazadBindingSource
            // 
            this.jarimemasrafmazadBindingSource.DataMember = "Jarime_masraf_mazad";
            this.jarimemasrafmazadBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "مصرف مجاز:";
            // 
            // jarime_masraf_mazadTableAdapter
            // 
            this.jarime_masraf_mazadTableAdapter.ClearBeforeFill = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(290, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "مجموع هزینه :";
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Location = new System.Drawing.Point(382, 248);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(110, 17);
            this.lbltotal.TabIndex = 13;
            this.lbltotal.Text = "00000000000000000";
            // 
            // faDatePicker1
            // 
            this.faDatePicker1.Location = new System.Drawing.Point(708, 134);
            this.faDatePicker1.Multiline = true;
            this.faDatePicker1.Name = "faDatePicker1";
            this.faDatePicker1.Size = new System.Drawing.Size(226, 38);
            this.faDatePicker1.TabIndex = 16;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(385, 188);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "نمایش ارایه مربوطه";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(825, 344);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(185, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "اختلاف بین دو تاریخ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // fa_date
            // 
            this.fa_date.Location = new System.Drawing.Point(373, 268);
            this.fa_date.Name = "fa_date";
            this.fa_date.SelectedDateTime = new System.DateTime(1393, 1, 1, 0, 0, 0, 0);
            this.fa_date.Size = new System.Drawing.Size(114, 20);
            this.fa_date.TabIndex = 21;
            this.fa_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(373, 294);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(185, 23);
            this.button4.TabIndex = 22;
            this.button4.Text = "بررسی خطا تاریخ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textboxPrice1
            // 
            this.textboxPrice1.AutoSize = true;
            this.textboxPrice1.Location = new System.Drawing.Point(602, 308);
            this.textboxPrice1.Margin = new System.Windows.Forms.Padding(0);
            this.textboxPrice1.Name = "textboxPrice1";
            this.textboxPrice1.Size = new System.Drawing.Size(235, 42);
            this.textboxPrice1.TabIndex = 23;
            this.textboxPrice1.textWithcomma = null;
            this.textboxPrice1.textWithoutcomma = null;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(208, 318);
            this.maskedTextBox1.Mask = "###,###,###,###";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(136, 24);
            this.maskedTextBox1.TabIndex = 24;
            this.maskedTextBox1.Text = "2123";
            this.maskedTextBox1.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(350, 319);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(185, 23);
            this.button5.TabIndex = 25;
            this.button5.Text = "بررسی عدد";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBox1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBox1.DisabledBackColor = System.Drawing.Color.White;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(279, 188);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 18);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = ".0";
            // 
            // FrmTest
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 554);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.textboxPrice1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.fa_date);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.faDatePicker1);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.DgvResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtmasraf_gheir_mojaz);
            this.Controls.Add(this.txtmasraf_kol);
            this.Controls.Add(this.txtmasraf_mojaz);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtfind);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTest";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmTest";
            this.Load += new System.EventHandler(this.FrmTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminuseraccesstypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jarimemasrafmazadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource adminuseraccesstypeBindingSource;
        private CWMSDataSetTableAdapters.admin_user_access_typeTableAdapter admin_user_access_typeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actionDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox txtfind;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtmasraf_mojaz;
        private System.Windows.Forms.TextBox txtmasraf_kol;
        private System.Windows.Forms.TextBox txtmasraf_gheir_mojaz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView DgvResult;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label4;
        private AbDataset abDataset;
        private System.Windows.Forms.BindingSource jarimemasrafmazadBindingSource;
        private AbDatasetTableAdapters.Jarime_masraf_mazadTableAdapter jarime_masraf_mazadTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tadarsadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaribabbahaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ض;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker1;
        private FloatTextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private FarsiLibrary.Win.Controls.FADatePicker fa_date;
        private System.Windows.Forms.Button button4;
        private KohansalSoft.TextboxPrice textboxPrice1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Button button5;
    }
}