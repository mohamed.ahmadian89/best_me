﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{

    public partial class Frm_ghobooz_chap : MyMetroForm
    {
        string dcode = "", TableName = "";
        DataTable dt_ab_doreh, dt_sharj_doreh;
        public Frm_ghobooz_chap(string doreh, string table, string Ename)
        {
            InitializeComponent();

            loading = true;

            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                grp_doreh_for_esf.Visible = true;
            }
            else
                grp_doreh_for_esf.Visible = false;




            grpinfo.Text = "چاپ قبوض مربوط به دوره  " + doreh + "  " + table;
            dcode = doreh;
            TableName = table;
            DataTable dt = Classes.ClsMain.GetDataTable("select min(gharardad) as min1,max(gharardad) as max1 from " + Ename + "_ghabz where dcode=" + dcode);
            if (dt.Rows.Count == 0)
            {
                TxtStart.Value = 0;
                TxtEnd.Value = 0;
            }
            else
            {
                TxtStart.Value = Convert.ToInt32(dt.Rows[0][0]);
                TxtEnd.Value = Convert.ToInt32(dt.Rows[0][1]);
            }

            dt_ab_doreh = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            dt_sharj_doreh = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");
            cmb_ab.DataSource = dt_ab_doreh;
            cmb_sharj.DataSource = dt_sharj_doreh;
            cmb_sharj.DisplayMember = "dcode";
            cmb_ab.ValueMember = "dcode";

            if (Ename == "ab")
            {
                cmb_ghabz_type.SelectedIndex = 0;
                cmb_ghabz_doreh.DataSource = dt_ab_doreh;
            }
            else
            {
                cmb_ghabz_type.SelectedIndex = 1;
                cmb_ghabz_doreh.DataSource = dt_sharj_doreh;
            }
            cmb_ghabz_doreh.DisplayMember = "dcode";
            cmb_ghabz_doreh.ValueMember = "dcode";
            cmb_ghabz_doreh.Text = doreh;
            loading = false;
        }


        private void btnSetNotFaal_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("ایا با چاپ قبوض موافق هستید؟") == DialogResult.Yes)
            {

                if (TxtStart.Value.ToString() != "" && TxtEnd.Value.ToString() != "")
                {
                    TxtStart.Enabled = TxtEnd.Enabled = false;
                    progressBar1.Value = 0;


                    btnPrint.Enabled = false;
                    int count = Convert.ToInt32(TxtEnd.Value) - Convert.ToInt32(TxtStart.Value);
                    progressBar1.Maximum = count + 1;
                    int moshtarek_actve_state = 0;
                    if (ch_faal_company.Checked)
                        moshtarek_actve_state = 1;

                    bool Sort_by_gharardad = false;
                    if (rd_gharardad.Checked)
                        Sort_by_gharardad = true;

                    bool print_ghabz_which_not_printed = false;
                    if (rd_print_ghabz_which_not_printed.Checked)
                        print_ghabz_which_not_printed = true;

                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {
                        int sharj_dcode = Convert.ToInt32(cmb_sharj.Text);
                        int ab_dcode = Convert.ToInt32(cmb_ab.Text);

                        if (ch_have_ab_gahbz.Checked == false)
                            ab_dcode = -1;
                        if (ch_have_sharj_ghabz.Checked == false)
                            sharj_dcode = -1;

                        Classes.shahrak_classes.esf_bozorg.Print_Multi_Ghabz(sharj_dcode, ab_dcode, Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), Sort_by_gharardad, moshtarek_actve_state, progressBar1);
                        btnPrint.Enabled = true;
                    }
                    else
                    {
                        if (cmb_ghabz_type.Text == "شارژ")
                        {

                            Classes.clsSharj.print_Ghabz_Multi(backgroundWorker1, progressBar1, cmb_ghabz_doreh.Text, Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), moshtarek_actve_state, Sort_by_gharardad, print_ghabz_which_not_printed);

                        }
                        else
                        {

                            Classes.clsAb.print_Ghabz_Multi(backgroundWorker1, progressBar1, cmb_ghabz_doreh.Text, Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), moshtarek_actve_state, Sort_by_gharardad, print_ghabz_which_not_printed);

                        }
                    }

                    //backgroundWorker1.RunWorkerAsync();

                }

            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            btnPrint.Enabled = false;
            int count = Convert.ToInt32(TxtEnd.Value) - Convert.ToInt32(TxtStart.Value);
            progressBar1.Maximum = count + 1;
            int moshtarek_actve_state = 0;
            if (ch_faal_company.Checked)
                moshtarek_actve_state = 1;

            bool Sort_by_gharardad = false;
            if (rd_gharardad.Checked)
                Sort_by_gharardad = true;

            bool print_ghabz_which_not_printed = false;
            if (rd_print_ghabz_which_not_printed.Checked)
                print_ghabz_which_not_printed = true;


            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
            {
                Classes.shahrak_classes.esf_bozorg.Print_Multi_Ghabz(
                    Convert.ToInt32(cmb_sharj.Text), Convert.ToInt32(cmb_ab.Text),
                     Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), Sort_by_gharardad, moshtarek_actve_state, progressBar1)
                    ;
            }
            else
            {
                if (cmb_ghabz_type.Text == "شارژ")
                {

                    Classes.clsSharj.print_Ghabz_Multi(backgroundWorker1, progressBar1, cmb_ghabz_doreh.Text, Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), moshtarek_actve_state, Sort_by_gharardad, print_ghabz_which_not_printed);
                }
                else
                {

                    Classes.clsAb.print_Ghabz_Multi(backgroundWorker1, progressBar1, cmb_ghabz_doreh.Text, Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value), moshtarek_actve_state, Sort_by_gharardad, print_ghabz_which_not_printed);

                }
            }
            progressBar1.Value = count + 1;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Payam.Show("کلیه قبوض با موفقیت چاپ شدند");
            TxtStart.Enabled = TxtEnd.Enabled = true;
            btnPrint.Enabled = true;


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

        private void Frm_ghobooz_chap_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                if (Cpayam.Show("قبوض در حال چاپ هستند . ایا این عملیات به صورت پس زمینه ادامه پیدا کند؟") != DialogResult.Yes)
                {
                    backgroundWorker1.CancelAsync();
                }

            }
        }

        private void TxtStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                TxtEnd.Focus();
        }


        private void TxtEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnPrint.PerformClick();
        }

        private void cmb_ghabz_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_ghabz_type.Items.Count != 0)
            {
                if (cmb_ghabz_type.SelectedIndex == 0)
                {
                    cmb_ghabz_doreh.DataSource = dt_ab_doreh;
                }
                else
                {
                    cmb_ghabz_doreh.DataSource = dt_sharj_doreh;
                }

                cmb_ghabz_doreh.DisplayMember = "dcode";
                cmb_ghabz_doreh.ValueMember = "dcode";
            }

        }
        DataTable sharj_part(CrysReports.ReportDataSet ds, string ghabz_type_for_print)
        {
            CrysReports.ReportDataSetTableAdapters.persian_sharjTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.persian_sharjTableAdapter();
            if (ghabz_type_for_print == "all")
                ta1.FillByGharardadRange(ds.persian_sharj, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));
            else if (ghabz_type_for_print == "pardakht_kamel")
                ta1.Fill_full_pardakht(ds.persian_sharj, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));
            else
                ta1.Fill_nopardakht_jozee(ds.persian_sharj, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));

            DataTable dt_ghabz = ds.persian_sharj.CopyToDataTable();
            int vaziat = 0;
            bool tejari = false, has_pardakhti = false;
            dt_ghabz.Columns.Remove("وضعیت تجاری");
            dt_ghabz.Columns.Add("وضعیت تجاری");
            dt_ghabz.Columns.Remove("وضعیت");
            dt_ghabz.Columns.Add("وضعیت");
            dt_ghabz.Columns.Remove("دارای پرداختی");
            dt_ghabz.Columns.Add("دارای پرداختی");
            dt_ghabz.Columns.Remove("تاریخ");
            dt_ghabz.Columns.Add("تاریخ");
            dt_ghabz.Columns.Remove("آخرین پرداختی");
            dt_ghabz.Columns.Add("آخرین پرداختی");
            dt_ghabz.Columns.Remove("مهلت پرداخت");
            dt_ghabz.Columns.Add("مهلت پرداخت");
            for (int i = 0; i < dt_ghabz.Rows.Count; i++)
            {
                dt_ghabz.Rows[i]["تاریخ"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_sharj.Rows[i]["تاریخ"])).ToString("d");
                try
                {
                    dt_ghabz.Rows[i]["آخرین پرداختی"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_sharj.Rows[i]["آخرین پرداختی"])).ToString("d");
                }
                catch (Exception)
                {
                    dt_ghabz.Rows[i]["آخرین پرداختی"] = "";
                }
                dt_ghabz.Rows[i]["مهلت پرداخت"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_sharj.Rows[i]["مهلت پرداخت"])).ToString("d");
                tejari = Convert.ToBoolean(ds.persian_sharj.Rows[i]["وضعیت تجاری"]);
                has_pardakhti = Convert.ToBoolean(ds.persian_sharj.Rows[i]["دارای پرداختی"]);

                dt_ghabz.Rows[i]["وضعیت تجاری"] = "خیر";
                if (tejari)
                    dt_ghabz.Rows[i]["وضعیت تجاری"] = "بلی";

                dt_ghabz.Rows[i]["دارای پرداختی"] = "خیر";
                if (has_pardakhti)
                    dt_ghabz.Rows[i]["دارای پرداختی"] = "بلی";

                vaziat = Convert.ToInt32(ds.persian_sharj.Rows[i]["وضعیت"]);

                if (vaziat == 0)
                    dt_ghabz.Rows[i]["وضعیت"] = "بدون پرداخت";
                else if (vaziat == 1)
                    dt_ghabz.Rows[i]["وضعیت"] = "پرداخت کامل";
                else
                    dt_ghabz.Rows[i]["وضعیت"] = "پرداخت جزئی";


            }
            return dt_ghabz;
        }

        DataTable ab_part(CrysReports.ReportDataSet ds, string ghabz_type_for_print)
        {
            CrysReports.ReportDataSetTableAdapters.persian_abTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.persian_abTableAdapter();

            if (ghabz_type_for_print == "all")
                ta1.FillByGharardadRange(ds.persian_ab, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));
            else if (ghabz_type_for_print == "pardakht_kamel")
                ta1.Fill_full_pardakht(ds.persian_ab, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));
            else
                ta1.Fill_nopardakht_jozee(ds.persian_ab, Convert.ToInt32(cmb_ghabz_doreh.Text), Convert.ToInt32(TxtStart.Value), Convert.ToInt32(TxtEnd.Value));


            DataTable dt_ghabz = ds.persian_ab.CopyToDataTable();
            int vaziat = 0;
            bool tejari = false, has_pardakhti = false;
            dt_ghabz.Columns.Remove("وضعیت تجاری");
            dt_ghabz.Columns.Add("وضعیت تجاری");
            dt_ghabz.Columns.Remove("وضعیت");
            dt_ghabz.Columns.Add("وضعیت");
            dt_ghabz.Columns.Remove("دارای پرداختی");
            dt_ghabz.Columns.Add("دارای پرداختی");
            dt_ghabz.Columns.Remove("تاریخ");
            dt_ghabz.Columns.Add("تاریخ");
            dt_ghabz.Columns.Remove("آخرین پرداختی");
            dt_ghabz.Columns.Add("آخرین پرداختی");
            dt_ghabz.Columns.Remove("مهلت پرداخت");
            dt_ghabz.Columns.Add("مهلت پرداخت");

            dt_ghabz.Columns.Remove("قطع کنتور");
            dt_ghabz.Columns.Add("قطع کنتور");
            dt_ghabz.Columns.Remove("عدم قرائت کنتور");
            dt_ghabz.Columns.Add("عدم قرائت کنتور");
            dt_ghabz.Columns.Remove("تعویض کنتور");
            dt_ghabz.Columns.Add("تعویض کنتور");
            dt_ghabz.Columns.Remove("خراب");
            dt_ghabz.Columns.Add("خراب");
            dt_ghabz.Columns.Remove("دورکامل کنتور");
            dt_ghabz.Columns.Add("دورکامل کنتور");

            dt_ghabz.Columns.Remove("درحال ساخت و ساز");
            dt_ghabz.Columns.Add("درحال ساخت و ساز");

            for (int i = 0; i < dt_ghabz.Rows.Count; i++)
            {

                dt_ghabz.Rows[i]["تاریخ"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_ab.Rows[i]["تاریخ"])).ToString("d");
                try
                {
                    dt_ghabz.Rows[i]["آخرین پرداختی"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_ab.Rows[i]["آخرین پرداختی"])).ToString("d");
                }
                catch (Exception)
                {
                    dt_ghabz.Rows[i]["آخرین پرداختی"] = "";
                }
                dt_ghabz.Rows[i]["مهلت پرداخت"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(ds.persian_ab.Rows[i]["مهلت پرداخت"])).ToString("d");
                tejari = Convert.ToBoolean(ds.persian_ab.Rows[i]["وضعیت تجاری"]);
                has_pardakhti = Convert.ToBoolean(ds.persian_ab.Rows[i]["دارای پرداختی"]);


                dt_ghabz.Rows[i]["دورکامل کنتور"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["دورکامل کنتور"]) ? "بله" : "خیر";
                dt_ghabz.Rows[i]["درحال ساخت و ساز"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["درحال ساخت و ساز"]) ? "بله" : "خیر";
                dt_ghabz.Rows[i]["خراب"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["خراب"]) ? "بله" : "خیر";
                dt_ghabz.Rows[i]["عدم قرائت کنتور"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["عدم قرائت کنتور"]) ? "بله" : "خیر";
                dt_ghabz.Rows[i]["تعویض کنتور"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["تعویض کنتور"]) ? "بله" : "خیر";
                dt_ghabz.Rows[i]["قطع کنتور"] = Convert.ToBoolean(ds.persian_ab.Rows[i]["قطع کنتور"]) ? "بله" : "خیر";





                dt_ghabz.Rows[i]["وضعیت تجاری"] = "خیر";
                if (tejari)
                    dt_ghabz.Rows[i]["وضعیت تجاری"] = "بلی";

                dt_ghabz.Rows[i]["دارای پرداختی"] = "خیر";
                if (has_pardakhti)
                    dt_ghabz.Rows[i]["دارای پرداختی"] = "بلی";

                vaziat = Convert.ToInt32(ds.persian_ab.Rows[i]["وضعیت"]);

                if (vaziat == 0)
                    dt_ghabz.Rows[i]["وضعیت"] = "بدون پرداخت";
                else if (vaziat == 1)
                    dt_ghabz.Rows[i]["وضعیت"] = "پرداخت کامل";
                else
                    dt_ghabz.Rows[i]["وضعیت"] = "پرداخت جزئی";


            }
            return dt_ghabz;
        }
        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Excel Files (*.xlsx)|*.xlsx";
            sd.DefaultExt = "xlsx";
            sd.AddExtension = true;

            if (sd.ShowDialog() == DialogResult.OK)
            {
                if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                {


                    DataTable dt_ghabz = sharj_part(ds, "all");
                    wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                    wb.Worksheet("قبوض شارژ").RightToLeft = true;


                    dt_ghabz = ab_part(ds, "all");
                    wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                    wb.Worksheet("قبوض آب").RightToLeft = true;
                }
                else
                {
                    if (cmb_ghabz_type.Text == "شارژ")
                    {
                        DataTable dt_ghabz = sharj_part(ds, "all");
                        wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                        wb.Worksheet("قبوض شارژ").RightToLeft = true;

                    }
                    else
                    {
                        DataTable dt_ghabz = ab_part(ds, "all");
                        wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                        wb.Worksheet("قبوض آب").RightToLeft = true;
                    }
                }
                wb.SaveAs(sd.FileName);
            }





        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Excel Files (*.xlsx)|*.xlsx";
            sd.DefaultExt = "xlsx";
            sd.AddExtension = true;

            if (sd.ShowDialog() == DialogResult.OK)
            {
                if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                {

                    DataTable dt_ghabz = sharj_part(ds, "pardakht_kamel");
                    wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                    wb.Worksheet("قبوض شارژ").RightToLeft = true;

                    dt_ghabz = ab_part(ds, "pardakht_kamel");
                    wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                    wb.Worksheet("قبوض آب").RightToLeft = true;
                }
                else
                {
                    if (cmb_ghabz_type.Text == "شارژ")
                    {
                        DataTable dt_ghabz = sharj_part(ds, "pardakht_kamel");
                        wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                        wb.Worksheet("قبوض شارژ").RightToLeft = true;

                    }
                    else
                    {
                        DataTable dt_ghabz = ab_part(ds, "pardakht_kamel");
                        wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                        wb.Worksheet("قبوض آب").RightToLeft = true;
                    }
                }
                wb.SaveAs(sd.FileName);
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Excel Files (*.xlsx)|*.xlsx";
            sd.DefaultExt = "xlsx";
            sd.AddExtension = true;

            if (sd.ShowDialog() == DialogResult.OK)
            {

                if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                {

                    DataTable dt_ghabz = sharj_part(ds, "pardakht_nashode_jozee");
                    wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                    wb.Worksheet("قبوض شارژ").RightToLeft = true;

                    dt_ghabz = ab_part(ds, "pardakht_nashode_jozee");
                    wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                    wb.Worksheet("قبوض آب").RightToLeft = true;

                }
                else
                {
                    if (cmb_ghabz_type.Text == "شارژ")
                    {
                        DataTable dt_ghabz = sharj_part(ds, "pardakht_nashode_jozee");
                        wb.Worksheets.Add(dt_ghabz, "قبوض شارژ");
                        wb.Worksheet("قبوض شارژ").RightToLeft = true;

                    }
                    else
                    {
                        DataTable dt_ghabz = ab_part(ds, "pardakht_nashode_jozee");
                        wb.Worksheets.Add(dt_ghabz, "قبوض آب");
                        wb.Worksheet("قبوض آب").RightToLeft = true;
                    }
                }
                wb.SaveAs(sd.FileName);
            }
        }

        private void cmb_ghabz_doreh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (loading == false)
                grpinfo.Text = "چاپ قبوض مربوط به دوره  " + cmb_ghabz_doreh.Text + "  " + cmb_ghabz_type.Text;

        }
        bool loading = true;
        private void Frm_ghobooz_chap_Load(object sender, EventArgs e)
        {

        }

        private void Frm_ghobooz_chap_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.Close();
        }
    }
}