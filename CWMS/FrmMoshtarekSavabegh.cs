﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class FrmMoshtarekSavabegh : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmMoshtarekSavabegh(string gharardad)
        {
            InitializeComponent();
            moshtarekinTableAdapter.FillByGharardad(mainDataSest1.moshtarekin, Convert.ToInt32(gharardad));
        }

        private void FrmMoshtarekSavabegh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest1.moshtarekin' table. You can move, or remove it, as needed.
            
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            moshtarekinBindingSource.EndEdit();
            moshtarekinTableAdapter.Update(mainDataSest1.moshtarekin);
            Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
            this.Close();
        }
    }
}