﻿namespace CWMS
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ribbonTabItemGroup1 = new DevComponents.DotNetBar.RibbonTabItemGroup();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.comboItem11 = new DevComponents.Editors.ComboItem();
            this.comboItem12 = new DevComponents.Editors.ComboItem();
            this.comboItem13 = new DevComponents.Editors.ComboItem();
            this.comboItem14 = new DevComponents.Editors.ComboItem();
            this.comboItem15 = new DevComponents.Editors.ComboItem();
            this.comboItem16 = new DevComponents.Editors.ComboItem();
            this.comboItem17 = new DevComponents.Editors.ComboItem();
            this.comboItem18 = new DevComponents.Editors.ComboItem();
            this.bar2 = new DevComponents.DotNetBar.Bar();
            this.labelStatus = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer14 = new DevComponents.DotNetBar.ItemContainer();
            this.footer_time = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer25 = new DevComponents.DotNetBar.ItemContainer();
            this.HourLabel = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem15 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem14 = new DevComponents.DotNetBar.ButtonItem();
            this.progressBarItem1 = new DevComponents.DotNetBar.ProgressBarItem();
            this.labelPosition = new DevComponents.DotNetBar.LabelItem();
            this.grp_doreh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Panel_lbl_ab_mohalat = new DevComponents.DotNetBar.LabelX();
            this.Panel_lbl_ab_Start = new DevComponents.DotNetBar.LabelX();
            this.Panel_lbl_ab_endtime = new DevComponents.DotNetBar.LabelX();
            this.labelX37 = new DevComponents.DotNetBar.LabelX();
            this.labelX38 = new DevComponents.DotNetBar.LabelX();
            this.Panel_lbl_ab_doreh = new DevComponents.DotNetBar.LabelX();
            this.labelX40 = new DevComponents.DotNetBar.LabelX();
            this.labelX41 = new DevComponents.DotNetBar.LabelX();
            this.lbl_mohlat = new DevComponents.DotNetBar.LabelX();
            this.lbl_end_time = new DevComponents.DotNetBar.LabelX();
            this.lbl_start_time = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lbl_doreh_sharj = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX33 = new DevComponents.DotNetBar.LabelX();
            this.labelX34 = new DevComponents.DotNetBar.LabelX();
            this.Panel_lbl_ab_day_doreh_baad = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lbl_day_ta_doreh_baad = new DevComponents.DotNetBar.LabelX();
            this.grp_karkhaneh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.Main_Progress = new System.Windows.Forms.ProgressBar();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.grp_pardakht_ab = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_ab_new_pardakht = new TextBoxtest.TxtProNet();
            this.txt_ab_new_tarikh = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rd_ab_bank = new System.Windows.Forms.RadioButton();
            this.labelX44 = new DevComponents.DotNetBar.LabelX();
            this.btn_pardakht_ab_now = new DevComponents.DotNetBar.ButtonX();
            this.labelX46 = new DevComponents.DotNetBar.LabelX();
            this.grp_pardakht_sharj = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_sharj_new_pardakht = new TextBoxtest.TxtProNet();
            this.txt_sharj_new_tarikh = new System.Windows.Forms.TextBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rd_sharj_bank = new System.Windows.Forms.RadioButton();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.btn_sharj_new_pardakht = new DevComponents.DotNetBar.ButtonX();
            this.labelX39 = new DevComponents.DotNetBar.LabelX();
            this.grp_ab = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_ab_mablagh_kol = new TextBoxtest.TxtProNet();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.txt_ab_bestankari = new TextBoxtest.TxtProNet();
            this.lbl_ab_mandeh = new TextBoxtest.TxtProNet();
            this.btn_new_ab_ghabz = new DevComponents.DotNetBar.ButtonX();
            this.lbl_ab_pardakhti = new TextBoxtest.TxtProNet();
            this.btn_pardakht_ab = new DevComponents.DotNetBar.ButtonX();
            this.lbl_ab_gcode = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.lbl_ab_vaziat = new DevComponents.DotNetBar.LabelX();
            this.btn_print_ab_Ghabz = new DevComponents.DotNetBar.ButtonX();
            this.btn_show_ab_Ghabz = new DevComponents.DotNetBar.ButtonX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.lbl_ab_mablaghkol_horoof = new DevComponents.DotNetBar.LabelX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.grpMoshtarek = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtMobile = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX36 = new DevComponents.DotNetBar.LabelX();
            this.txtElhaghieh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_address = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_phone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_modir_amel = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_name = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.grp_sharj = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_moshtarek_mablagh_kol = new TextBoxtest.TxtProNet();
            this.txt_sharj_bestnkari = new TextBoxtest.TxtProNet();
            this.lbl_sharj_mandeh = new TextBoxtest.TxtProNet();
            this.lbl_sharj_pardakhti = new TextBoxtest.TxtProNet();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.btn_new_sharj_ghabz = new DevComponents.DotNetBar.ButtonX();
            this.btn_pardakht_sharj = new DevComponents.DotNetBar.ButtonX();
            this.btn_print_Sharj_Ghabz = new DevComponents.DotNetBar.ButtonX();
            this.lbl_sharj_gcode = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.btn_show_Sharj_Ghabz = new DevComponents.DotNetBar.ButtonX();
            this.lbl_moshtarek_mablgh_kol_str = new DevComponents.DotNetBar.LabelX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.lbl_sharj_vaziat = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.cmbsharjEnd = new System.Windows.Forms.ComboBox();
            this.cmbsharjStart = new System.Windows.Forms.ComboBox();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.cmbAbEnd = new System.Windows.Forms.ComboBox();
            this.cmbabStart = new System.Windows.Forms.ComboBox();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.tabusergozaresh = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.lblnoetefagh = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.dgvNOtofication = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subjectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chekecd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adminnotificationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnFindByPelak = new DevComponents.DotNetBar.ButtonX();
            this.txtPelak = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.btn_find_company = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_moshtarek = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.All_ghabz_sharj = new DevComponents.DotNetBar.Command(this.components);
            this.command1 = new DevComponents.DotNetBar.Command(this.components);
            this.New_sharj_ghabz = new DevComponents.DotNetBar.Command(this.components);
            this.barge = new DevComponents.DotNetBar.Command(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem62 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem64 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.galleryContainer2 = new DevComponents.DotNetBar.GalleryContainer();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem16 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer6 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer7 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer3 = new DevComponents.DotNetBar.GalleryContainer();
            this.itemContainer8 = new DevComponents.DotNetBar.ItemContainer();
            this.galleryContainer4 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer5 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer6 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer7 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer8 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer9 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer10 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer11 = new DevComponents.DotNetBar.GalleryContainer();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.galleryContainer12 = new DevComponents.DotNetBar.GalleryContainer();
            this.buttonItem17 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem18 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer13 = new DevComponents.DotNetBar.GalleryContainer();
            this.buttonItem19 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem20 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer14 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer15 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer16 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer17 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer18 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer19 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer20 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer21 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer22 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer23 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer24 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer25 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer26 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer27 = new DevComponents.DotNetBar.GalleryContainer();
            this.metroTileItem5 = new DevComponents.DotNetBar.Metro.MetroTileItem();
            this.galleryContainer28 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer29 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer30 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer31 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer32 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer33 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer34 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer35 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer36 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem8 = new DevComponents.DotNetBar.LabelItem();
            this.ErsalPayam = new DevComponents.DotNetBar.Command(this.components);
            this.galleryContainer37 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer38 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer39 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer40 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer41 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer42 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer43 = new DevComponents.DotNetBar.GalleryContainer();
            this.itemContainer52 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem73 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer53 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem83 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer54 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem84 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer44 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer45 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer46 = new DevComponents.DotNetBar.GalleryContainer();
            this.itemContainer21 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem25 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer22 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem26 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.galleryContainer47 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer48 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer1 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer49 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer50 = new DevComponents.DotNetBar.GalleryContainer();
            this.galleryContainer51 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.galleryContainer52 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem2 = new DevComponents.DotNetBar.LabelItem();
            this.galleryContainer53 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem3 = new DevComponents.DotNetBar.LabelItem();
            this.galleryContainer54 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem4 = new DevComponents.DotNetBar.LabelItem();
            this.labelX29 = new DevComponents.DotNetBar.LabelX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.txtUsername = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtpassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.btnLogin = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.grpLogin = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.galleryContainer55 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem5 = new DevComponents.DotNetBar.LabelItem();
            this.admin_notificationTableAdapter = new CWMS.MainDataSestTableAdapters.admin_notificationTableAdapter();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem39 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer44 = new DevComponents.DotNetBar.ItemContainer();
            this.fd = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer49 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem41 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer50 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem45 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer34 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem32 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer40 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem35 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer41 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem36 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer56 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem44 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel8 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer43 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem37 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer30 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem33 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer33 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem34 = new DevComponents.DotNetBar.ButtonItem();
            this.gozarehat_check = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer47 = new DevComponents.DotNetBar.ItemContainer();
            this.gozarehat_check1 = new DevComponents.DotNetBar.ButtonItem();
            this.gozarehat_tarefe_zarayeb = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer45 = new DevComponents.DotNetBar.ItemContainer();
            this.gozarehat_tarefe_zarayeb1 = new DevComponents.DotNetBar.ButtonItem();
            this.gozarehat_ab_sharj = new DevComponents.DotNetBar.RibbonBar();
            this.gozarehat_ab_sharj1 = new DevComponents.DotNetBar.ItemContainer();
            this.سیس = new DevComponents.DotNetBar.ButtonItem();
            this.gozarehat_ab_sharj_Pardakhti = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem24 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar12 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer42 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem43 = new DevComponents.DotNetBar.ButtonItem();
            this.InternetMobikePanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer10 = new DevComponents.DotNetBar.ItemContainer();
            this.internet_gozareshat_etesal_mobile = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer48 = new DevComponents.DotNetBar.ItemContainer();
            this.internet_moshahede_key = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer51 = new DevComponents.DotNetBar.ItemContainer();
            this.internet_afzoodan_key = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel6 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar11 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer46 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem40 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.bargeh_gheir_faal = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer31 = new DevComponents.DotNetBar.ItemContainer();
            this.bargeh_gheir_faal1 = new DevComponents.DotNetBar.ButtonItem();
            this.barge1panel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer17 = new DevComponents.DotNetBar.ItemContainer();
            this.bargeh_moshahede_darkhast = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer23 = new DevComponents.DotNetBar.ItemContainer();
            this.bargeh_sodoor = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel7 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar10 = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer35 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem38 = new DevComponents.DotNetBar.ButtonItem();
            this.modiriat_tayin_vaziat_sodoor = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem28 = new DevComponents.DotNetBar.ButtonItem();
            this.modiriat_setting = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.modiriat_etelaieh = new DevComponents.DotNetBar.RibbonBar();
            this.modiriat_etelaieh1 = new DevComponents.DotNetBar.ButtonItem();
            this.shahrakKKarbaranPanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer16 = new DevComponents.DotNetBar.ItemContainer();
            this.modiriat_admin_users = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer18 = new DevComponents.DotNetBar.ItemContainer();
            this.modiriat_gozareshat_dastarsi = new DevComponents.DotNetBar.ButtonItem();
            this.modiriat_backup = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem67 = new DevComponents.DotNetBar.ButtonItem();
            this.modiriat_peikarbandi = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer28 = new DevComponents.DotNetBar.ItemContainer();
            this.modiriat_bedehi_ab = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer12 = new DevComponents.DotNetBar.ItemContainer();
            this.modiriat_bedehi_sharj = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem21 = new DevComponents.DotNetBar.ButtonItem();
            this.ab_jarmieh = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem30 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_barge_darkhast = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer24 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem23 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_JostejooInSavabegh = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer20 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_afzoodan_key = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer55 = new DevComponents.DotNetBar.ItemContainer();
            this.moshtarekin_afzoodan_key1 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_sharj = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_ab = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer19 = new DevComponents.DotNetBar.ItemContainer();
            this.q3 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_moshahedeh = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer15 = new DevComponents.DotNetBar.ItemContainer();
            this.q1 = new DevComponents.DotNetBar.ButtonItem();
            this.moshtarekin_sabte_moshtarek_jadidi = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer9 = new DevComponents.DotNetBar.ItemContainer();
            this.q = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem22 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem31 = new DevComponents.DotNetBar.ButtonItem();
            this.ab_history = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem29 = new DevComponents.DotNetBar.ButtonItem();
            this.abTanzimatPanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer36 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_ababaha = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer38 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_aboonman = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_tanzimat = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer39 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_zarayeb = new DevComponents.DotNetBar.ButtonItem();
            this.ab_doreh_sodoor_ghabz = new DevComponents.DotNetBar.RibbonBar();
            this.ab_doreh_sodoor_ghabz1 = new DevComponents.DotNetBar.ButtonItem();
            this.abMoshahedePanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer11 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_moshtarek = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer32 = new DevComponents.DotNetBar.ItemContainer();
            this.ab_doreh = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.sharjTanzimatPanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer29 = new DevComponents.DotNetBar.ItemContainer();
            this.sharj_tanzimat = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer37 = new DevComponents.DotNetBar.ItemContainer();
            this.sharj_tarefe = new DevComponents.DotNetBar.ButtonItem();
            this.sharj_tarif_doreh_sodoor_ghabz = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem27 = new DevComponents.DotNetBar.ButtonItem();
            this.sharjMoshahedePanel = new DevComponents.DotNetBar.RibbonBar();
            this.itemContainer26 = new DevComponents.DotNetBar.ItemContainer();
            this.sharj_moshraek = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer27 = new DevComponents.DotNetBar.ItemContainer();
            this.sharj_doreh = new DevComponents.DotNetBar.ButtonItem();
            this.buttonFile = new DevComponents.DotNetBar.Office2007StartButton();
            this.buttonItem42 = new DevComponents.DotNetBar.ButtonItem();
            this.RbMoshtarekin = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbSharj = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbAb = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbBargehKhorooj = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbInternet = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbShahrak = new DevComponents.DotNetBar.RibbonTabItem();
            this.RbGozareshModiriat = new DevComponents.DotNetBar.RibbonTabItem();
            this.Rb_hesabdari = new DevComponents.DotNetBar.RibbonTabItem();
            this.galleryContainer56 = new DevComponents.DotNetBar.GalleryContainer();
            this.labelItem6 = new DevComponents.DotNetBar.LabelItem();
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).BeginInit();
            this.grp_doreh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grp_karkhaneh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.grp_pardakht_ab.SuspendLayout();
            this.grp_pardakht_sharj.SuspendLayout();
            this.grp_ab.SuspendLayout();
            this.grpMoshtarek.SuspendLayout();
            this.grp_sharj.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNOtofication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminnotificationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.grpLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel8.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.ribbonPanel6.SuspendLayout();
            this.ribbonPanel7.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonTabItemGroup1
            // 
            this.ribbonTabItemGroup1.Color = DevComponents.DotNetBar.eRibbonTabGroupColor.Orange;
            this.ribbonTabItemGroup1.GroupTitle = "Tab Group";
            this.ribbonTabItemGroup1.Name = "ribbonTabItemGroup1";
            // 
            // 
            // 
            this.ribbonTabItemGroup1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(159)))));
            this.ribbonTabItemGroup1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(226)))));
            this.ribbonTabItemGroup1.Style.BackColorGradientAngle = 90;
            this.ribbonTabItemGroup1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderBottomWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(58)))), ((int)(((byte)(59)))));
            this.ribbonTabItemGroup1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderLeftWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderRightWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderTopWidth = 1;
            this.ribbonTabItemGroup1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonTabItemGroup1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonTabItemGroup1.Style.TextColor = System.Drawing.Color.Black;
            this.ribbonTabItemGroup1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "6";
            // 
            // comboItem11
            // 
            this.comboItem11.Text = "7";
            // 
            // comboItem12
            // 
            this.comboItem12.Text = "8";
            // 
            // comboItem13
            // 
            this.comboItem13.Text = "9";
            // 
            // comboItem14
            // 
            this.comboItem14.Text = "10";
            // 
            // comboItem15
            // 
            this.comboItem15.Text = "11";
            // 
            // comboItem16
            // 
            this.comboItem16.Text = "12";
            // 
            // comboItem17
            // 
            this.comboItem17.Text = "13";
            // 
            // comboItem18
            // 
            this.comboItem18.Text = "14";
            // 
            // bar2
            // 
            this.bar2.AccessibleDescription = "DotNetBar Bar (bar2)";
            this.bar2.AccessibleName = "DotNetBar Bar";
            this.bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.bar2.AntiAlias = true;
            this.bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.bar2.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.bar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelStatus,
            this.itemContainer14,
            this.itemContainer25});
            this.bar2.ItemSpacing = 2;
            this.bar2.Location = new System.Drawing.Point(5, 692);
            this.bar2.Name = "bar2";
            this.bar2.PaddingBottom = 0;
            this.bar2.PaddingTop = 0;
            this.bar2.Size = new System.Drawing.Size(1360, 27);
            this.bar2.Stretch = true;
            this.bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.bar2.TabIndex = 12;
            this.bar2.TabStop = false;
            this.bar2.Text = "barStatus";
            // 
            // labelStatus
            // 
            this.labelStatus.Font = new System.Drawing.Font("B Yekan", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.PaddingLeft = 2;
            this.labelStatus.PaddingRight = 2;
            this.labelStatus.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelStatus.Stretch = true;
            // 
            // itemContainer14
            // 
            // 
            // 
            // 
            this.itemContainer14.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer14.FixedSize = new System.Drawing.Size(200, 0);
            this.itemContainer14.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center;
            this.itemContainer14.Name = "itemContainer14";
            this.itemContainer14.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.footer_time});
            // 
            // 
            // 
            this.itemContainer14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer14.TitleStyle.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.itemContainer14.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // footer_time
            // 
            this.footer_time.Font = new System.Drawing.Font("B Yekan", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.footer_time.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.footer_time.Name = "footer_time";
            this.footer_time.PaddingLeft = 10;
            this.footer_time.PaddingRight = 2;
            this.footer_time.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.footer_time.Stretch = true;
            this.footer_time.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // itemContainer25
            // 
            // 
            // 
            // 
            this.itemContainer25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer25.FixedSize = new System.Drawing.Size(150, 0);
            this.itemContainer25.Name = "itemContainer25";
            this.itemContainer25.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.HourLabel});
            // 
            // 
            // 
            this.itemContainer25.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // HourLabel
            // 
            this.HourLabel.Font = new System.Drawing.Font("B Yekan", 12F);
            this.HourLabel.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.HourLabel.Name = "HourLabel";
            this.HourLabel.PaddingRight = 15;
            this.HourLabel.Text = " ";
            this.HourLabel.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.BeginGroup = true;
            this.itemContainer13.Name = "itemContainer13";
            this.itemContainer13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem13,
            this.buttonItem15,
            this.buttonItem14});
            // 
            // 
            // 
            this.itemContainer13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem13
            // 
            this.buttonItem13.Checked = true;
            this.buttonItem13.ImagePaddingVertical = 9;
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.OptionGroup = "statusGroup";
            this.buttonItem13.Symbol = "";
            this.buttonItem13.SymbolSize = 10F;
            this.buttonItem13.Text = "Rating";
            this.buttonItem13.Tooltip = "Rating";
            // 
            // buttonItem15
            // 
            this.buttonItem15.ImagePaddingVertical = 9;
            this.buttonItem15.Name = "buttonItem15";
            this.buttonItem15.OptionGroup = "statusGroup";
            this.buttonItem15.Symbol = "";
            this.buttonItem15.SymbolSize = 10F;
            this.buttonItem15.Text = "Comments";
            this.buttonItem15.Tooltip = "Comments";
            // 
            // buttonItem14
            // 
            this.buttonItem14.ImagePaddingVertical = 9;
            this.buttonItem14.Name = "buttonItem14";
            this.buttonItem14.OptionGroup = "statusGroup";
            this.buttonItem14.Symbol = "";
            this.buttonItem14.SymbolSize = 10F;
            this.buttonItem14.Text = "Rating";
            this.buttonItem14.Tooltip = "Rating";
            // 
            // progressBarItem1
            // 
            // 
            // 
            // 
            this.progressBarItem1.BackStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.progressBarItem1.ChunkGradientAngle = 0F;
            this.progressBarItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.progressBarItem1.Name = "progressBarItem1";
            this.progressBarItem1.RecentlyUsed = false;
            // 
            // labelPosition
            // 
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.PaddingLeft = 2;
            this.labelPosition.PaddingRight = 2;
            this.labelPosition.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelPosition.Width = 100;
            // 
            // grp_doreh
            // 
            this.grp_doreh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_doreh.BackColor = System.Drawing.Color.Transparent;
            this.grp_doreh.CanvasColor = System.Drawing.SystemColors.Control;
            this.grp_doreh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.grp_doreh.Controls.Add(this.pictureBox2);
            this.grp_doreh.Controls.Add(this.pictureBox1);
            this.grp_doreh.Controls.Add(this.Panel_lbl_ab_mohalat);
            this.grp_doreh.Controls.Add(this.Panel_lbl_ab_Start);
            this.grp_doreh.Controls.Add(this.Panel_lbl_ab_endtime);
            this.grp_doreh.Controls.Add(this.labelX37);
            this.grp_doreh.Controls.Add(this.labelX38);
            this.grp_doreh.Controls.Add(this.Panel_lbl_ab_doreh);
            this.grp_doreh.Controls.Add(this.labelX40);
            this.grp_doreh.Controls.Add(this.labelX41);
            this.grp_doreh.Controls.Add(this.lbl_mohlat);
            this.grp_doreh.Controls.Add(this.lbl_end_time);
            this.grp_doreh.Controls.Add(this.lbl_start_time);
            this.grp_doreh.Controls.Add(this.labelX4);
            this.grp_doreh.Controls.Add(this.labelX3);
            this.grp_doreh.Controls.Add(this.lbl_doreh_sharj);
            this.grp_doreh.Controls.Add(this.labelX1);
            this.grp_doreh.Controls.Add(this.labelX5);
            this.grp_doreh.Controls.Add(this.labelX33);
            this.grp_doreh.Controls.Add(this.labelX34);
            this.grp_doreh.Controls.Add(this.Panel_lbl_ab_day_doreh_baad);
            this.grp_doreh.Controls.Add(this.labelX6);
            this.grp_doreh.Controls.Add(this.labelX7);
            this.grp_doreh.Controls.Add(this.lbl_day_ta_doreh_baad);
            this.grp_doreh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_doreh.Location = new System.Drawing.Point(81, 160);
            this.grp_doreh.Name = "grp_doreh";
            this.grp_doreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_doreh.Size = new System.Drawing.Size(1214, 97);
            // 
            // 
            // 
            this.grp_doreh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_doreh.Style.BackColorGradientAngle = 90;
            this.grp_doreh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_doreh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderBottomWidth = 1;
            this.grp_doreh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_doreh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderLeftWidth = 1;
            this.grp_doreh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderRightWidth = 1;
            this.grp_doreh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderTopWidth = 1;
            this.grp_doreh.Style.CornerDiameter = 4;
            this.grp_doreh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_doreh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_doreh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_doreh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_doreh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_doreh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_doreh.TabIndex = 14;
            this.grp_doreh.Text = "اطلاعات دوره جاری";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1114, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(62, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // Panel_lbl_ab_mohalat
            // 
            this.Panel_lbl_ab_mohalat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.Panel_lbl_ab_mohalat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Panel_lbl_ab_mohalat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Panel_lbl_ab_mohalat.Location = new System.Drawing.Point(256, -5);
            this.Panel_lbl_ab_mohalat.Name = "Panel_lbl_ab_mohalat";
            this.Panel_lbl_ab_mohalat.Size = new System.Drawing.Size(83, 33);
            this.Panel_lbl_ab_mohalat.TabIndex = 23;
            this.Panel_lbl_ab_mohalat.Text = " ";
            // 
            // Panel_lbl_ab_Start
            // 
            this.Panel_lbl_ab_Start.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.Panel_lbl_ab_Start.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Panel_lbl_ab_Start.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Panel_lbl_ab_Start.Location = new System.Drawing.Point(412, 22);
            this.Panel_lbl_ab_Start.Name = "Panel_lbl_ab_Start";
            this.Panel_lbl_ab_Start.Size = new System.Drawing.Size(83, 33);
            this.Panel_lbl_ab_Start.TabIndex = 22;
            this.Panel_lbl_ab_Start.Text = " ";
            // 
            // Panel_lbl_ab_endtime
            // 
            this.Panel_lbl_ab_endtime.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.Panel_lbl_ab_endtime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Panel_lbl_ab_endtime.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Panel_lbl_ab_endtime.Location = new System.Drawing.Point(254, 22);
            this.Panel_lbl_ab_endtime.Name = "Panel_lbl_ab_endtime";
            this.Panel_lbl_ab_endtime.Size = new System.Drawing.Size(83, 33);
            this.Panel_lbl_ab_endtime.TabIndex = 17;
            this.Panel_lbl_ab_endtime.Text = " ";
            // 
            // labelX37
            // 
            this.labelX37.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX37.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX37.Location = new System.Drawing.Point(353, 21);
            this.labelX37.Name = "labelX37";
            this.labelX37.Size = new System.Drawing.Size(60, 33);
            this.labelX37.TabIndex = 16;
            this.labelX37.Text = "تاریخ پایان :";
            // 
            // labelX38
            // 
            this.labelX38.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX38.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX38.Location = new System.Drawing.Point(500, 22);
            this.labelX38.Name = "labelX38";
            this.labelX38.Size = new System.Drawing.Size(60, 33);
            this.labelX38.TabIndex = 15;
            this.labelX38.Text = "تاریخ شروع :";
            // 
            // Panel_lbl_ab_doreh
            // 
            this.Panel_lbl_ab_doreh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.Panel_lbl_ab_doreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Panel_lbl_ab_doreh.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Panel_lbl_ab_doreh.Location = new System.Drawing.Point(432, -5);
            this.Panel_lbl_ab_doreh.Name = "Panel_lbl_ab_doreh";
            this.Panel_lbl_ab_doreh.Size = new System.Drawing.Size(61, 33);
            this.Panel_lbl_ab_doreh.TabIndex = 14;
            this.Panel_lbl_ab_doreh.Text = " ";
            // 
            // labelX40
            // 
            this.labelX40.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX40.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX40.Location = new System.Drawing.Point(495, -5);
            this.labelX40.Name = "labelX40";
            this.labelX40.Size = new System.Drawing.Size(70, 33);
            this.labelX40.TabIndex = 13;
            this.labelX40.Text = "دوره جاری آب :";
            // 
            // labelX41
            // 
            this.labelX41.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX41.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX41.Location = new System.Drawing.Point(340, -5);
            this.labelX41.Name = "labelX41";
            this.labelX41.Size = new System.Drawing.Size(75, 33);
            this.labelX41.TabIndex = 18;
            this.labelX41.Text = "مهلت پرداخت :";
            // 
            // lbl_mohlat
            // 
            this.lbl_mohlat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lbl_mohlat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_mohlat.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_mohlat.Location = new System.Drawing.Point(639, -8);
            this.lbl_mohlat.Name = "lbl_mohlat";
            this.lbl_mohlat.Size = new System.Drawing.Size(83, 33);
            this.lbl_mohlat.TabIndex = 9;
            this.lbl_mohlat.Text = " ";
            // 
            // lbl_end_time
            // 
            this.lbl_end_time.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lbl_end_time.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_end_time.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_end_time.Location = new System.Drawing.Point(641, 21);
            this.lbl_end_time.Name = "lbl_end_time";
            this.lbl_end_time.Size = new System.Drawing.Size(83, 33);
            this.lbl_end_time.TabIndex = 7;
            this.lbl_end_time.Text = " ";
            // 
            // lbl_start_time
            // 
            this.lbl_start_time.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lbl_start_time.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_start_time.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_start_time.Location = new System.Drawing.Point(812, 21);
            this.lbl_start_time.Name = "lbl_start_time";
            this.lbl_start_time.Size = new System.Drawing.Size(83, 33);
            this.lbl_start_time.TabIndex = 6;
            this.lbl_start_time.Text = " ";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX4.Location = new System.Drawing.Point(738, 21);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(60, 33);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "تاریخ پایان :";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX3.Location = new System.Drawing.Point(897, 21);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(60, 33);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "تاریخ شروع :";
            // 
            // lbl_doreh_sharj
            // 
            this.lbl_doreh_sharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lbl_doreh_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_doreh_sharj.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_doreh_sharj.Location = new System.Drawing.Point(812, -8);
            this.lbl_doreh_sharj.Name = "lbl_doreh_sharj";
            this.lbl_doreh_sharj.Size = new System.Drawing.Size(71, 33);
            this.lbl_doreh_sharj.TabIndex = 3;
            this.lbl_doreh_sharj.Text = " ";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX1.Location = new System.Drawing.Point(886, -8);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(73, 33);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "دوره جاری شارژ:";
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX5.Location = new System.Drawing.Point(716, -8);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(82, 33);
            this.labelX5.TabIndex = 8;
            this.labelX5.Text = "مهلت پرداخت :";
            // 
            // labelX33
            // 
            this.labelX33.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX33.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX33.Location = new System.Drawing.Point(419, 43);
            this.labelX33.Name = "labelX33";
            this.labelX33.Size = new System.Drawing.Size(74, 33);
            this.labelX33.TabIndex = 20;
            this.labelX33.Text = "تا شروع دوره بعد ";
            // 
            // labelX34
            // 
            this.labelX34.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX34.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX34.Location = new System.Drawing.Point(297, 43);
            this.labelX34.Name = "labelX34";
            this.labelX34.Size = new System.Drawing.Size(94, 33);
            this.labelX34.TabIndex = 21;
            this.labelX34.Text = "روز باقی مانده است !";
            this.labelX34.Click += new System.EventHandler(this.labelX34_Click);
            // 
            // Panel_lbl_ab_day_doreh_baad
            // 
            this.Panel_lbl_ab_day_doreh_baad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.Panel_lbl_ab_day_doreh_baad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Panel_lbl_ab_day_doreh_baad.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.Panel_lbl_ab_day_doreh_baad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Panel_lbl_ab_day_doreh_baad.Location = new System.Drawing.Point(391, 41);
            this.Panel_lbl_ab_day_doreh_baad.Name = "Panel_lbl_ab_day_doreh_baad";
            this.Panel_lbl_ab_day_doreh_baad.Size = new System.Drawing.Size(32, 33);
            this.Panel_lbl_ab_day_doreh_baad.TabIndex = 19;
            this.Panel_lbl_ab_day_doreh_baad.Text = " ";
            this.Panel_lbl_ab_day_doreh_baad.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX6.Location = new System.Drawing.Point(857, 41);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(74, 33);
            this.labelX6.TabIndex = 11;
            this.labelX6.Text = "تا شروع دوره بعد ";
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX7.Location = new System.Drawing.Point(735, 41);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(94, 33);
            this.labelX7.TabIndex = 12;
            this.labelX7.Text = "روز باقی مانده است !";
            // 
            // lbl_day_ta_doreh_baad
            // 
            this.lbl_day_ta_doreh_baad.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lbl_day_ta_doreh_baad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_day_ta_doreh_baad.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Underline);
            this.lbl_day_ta_doreh_baad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_day_ta_doreh_baad.Location = new System.Drawing.Point(829, 39);
            this.lbl_day_ta_doreh_baad.Name = "lbl_day_ta_doreh_baad";
            this.lbl_day_ta_doreh_baad.Size = new System.Drawing.Size(32, 33);
            this.lbl_day_ta_doreh_baad.TabIndex = 10;
            this.lbl_day_ta_doreh_baad.Text = " ";
            this.lbl_day_ta_doreh_baad.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // grp_karkhaneh
            // 
            this.grp_karkhaneh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_karkhaneh.BackColor = System.Drawing.Color.Transparent;
            this.grp_karkhaneh.CanvasColor = System.Drawing.SystemColors.Control;
            this.grp_karkhaneh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_karkhaneh.Controls.Add(this.Main_Progress);
            this.grp_karkhaneh.Controls.Add(this.superTabControl1);
            this.grp_karkhaneh.Controls.Add(this.groupPanel1);
            this.grp_karkhaneh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_karkhaneh.Location = new System.Drawing.Point(55, 261);
            this.grp_karkhaneh.Name = "grp_karkhaneh";
            this.grp_karkhaneh.Size = new System.Drawing.Size(1289, 468);
            // 
            // 
            // 
            this.grp_karkhaneh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_karkhaneh.Style.BackColorGradientAngle = 90;
            this.grp_karkhaneh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_karkhaneh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_karkhaneh.Style.BorderBottomWidth = 1;
            this.grp_karkhaneh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_karkhaneh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_karkhaneh.Style.BorderLeftWidth = 1;
            this.grp_karkhaneh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_karkhaneh.Style.BorderRightWidth = 1;
            this.grp_karkhaneh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_karkhaneh.Style.BorderTopWidth = 1;
            this.grp_karkhaneh.Style.CornerDiameter = 4;
            this.grp_karkhaneh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_karkhaneh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_karkhaneh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_karkhaneh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_karkhaneh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_karkhaneh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_karkhaneh.TabIndex = 1;
            this.grp_karkhaneh.Click += new System.EventHandler(this.grp_karkhaneh_Click);
            // 
            // Main_Progress
            // 
            this.Main_Progress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.Main_Progress.ForeColor = System.Drawing.Color.Black;
            this.Main_Progress.Location = new System.Drawing.Point(3, 2);
            this.Main_Progress.Name = "Main_Progress";
            this.Main_Progress.Size = new System.Drawing.Size(282, 23);
            this.Main_Progress.TabIndex = 15;
            this.Main_Progress.Visible = false;
            // 
            // superTabControl1
            // 
            this.superTabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Font = new System.Drawing.Font("B Yekan", 9F);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(25, 82);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(1273, 377);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 2;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.tabusergozaresh,
            this.superTabItem2});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.grp_pardakht_ab);
            this.superTabControlPanel1.Controls.Add(this.grp_pardakht_sharj);
            this.superTabControlPanel1.Controls.Add(this.grp_ab);
            this.superTabControlPanel1.Controls.Add(this.grpMoshtarek);
            this.superTabControlPanel1.Controls.Add(this.grp_sharj);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(1273, 348);
            this.superTabControlPanel1.TabIndex = 10;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            this.superTabControlPanel1.Click += new System.EventHandler(this.superTabControlPanel1_Click);
            // 
            // grp_pardakht_ab
            // 
            this.grp_pardakht_ab.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grp_pardakht_ab.BackColor = System.Drawing.Color.Transparent;
            this.grp_pardakht_ab.CanvasColor = System.Drawing.SystemColors.Highlight;
            this.grp_pardakht_ab.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.grp_pardakht_ab.Controls.Add(this.txt_ab_new_pardakht);
            this.grp_pardakht_ab.Controls.Add(this.txt_ab_new_tarikh);
            this.grp_pardakht_ab.Controls.Add(this.radioButton1);
            this.grp_pardakht_ab.Controls.Add(this.rd_ab_bank);
            this.grp_pardakht_ab.Controls.Add(this.labelX44);
            this.grp_pardakht_ab.Controls.Add(this.btn_pardakht_ab_now);
            this.grp_pardakht_ab.Controls.Add(this.labelX46);
            this.grp_pardakht_ab.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_pardakht_ab.Location = new System.Drawing.Point(30, 270);
            this.grp_pardakht_ab.Name = "grp_pardakht_ab";
            this.grp_pardakht_ab.Size = new System.Drawing.Size(611, 65);
            // 
            // 
            // 
            this.grp_pardakht_ab.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_pardakht_ab.Style.BackColorGradientAngle = 90;
            this.grp_pardakht_ab.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_pardakht_ab.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_ab.Style.BorderBottomWidth = 1;
            this.grp_pardakht_ab.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_pardakht_ab.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_ab.Style.BorderLeftWidth = 1;
            this.grp_pardakht_ab.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_ab.Style.BorderRightWidth = 1;
            this.grp_pardakht_ab.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_ab.Style.BorderTopWidth = 1;
            this.grp_pardakht_ab.Style.CornerDiameter = 4;
            this.grp_pardakht_ab.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_pardakht_ab.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_pardakht_ab.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_pardakht_ab.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_pardakht_ab.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_pardakht_ab.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_pardakht_ab.TabIndex = 24;
            this.grp_pardakht_ab.Text = "پرداخت سریع قبض آب";
            this.grp_pardakht_ab.Visible = false;
            // 
            // txt_ab_new_pardakht
            // 
            this.txt_ab_new_pardakht.AutoSprator = true;
            this.txt_ab_new_pardakht.EnterToTab = false;
            this.txt_ab_new_pardakht.EscToClose = true;
            this.txt_ab_new_pardakht.FocusBackColor = System.Drawing.Color.PaleTurquoise;
            this.txt_ab_new_pardakht.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_new_pardakht.FocusForeColor = System.Drawing.Color.Blue;
            this.txt_ab_new_pardakht.FocusTextSelect = true;
            this.txt_ab_new_pardakht.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_new_pardakht.Location = new System.Drawing.Point(265, 6);
            this.txt_ab_new_pardakht.MaxLength = 15;
            this.txt_ab_new_pardakht.MessageEmptyShowDialog = false;
            this.txt_ab_new_pardakht.MessegeEmpty = "";
            this.txt_ab_new_pardakht.MessegeEmptyInFormRight = true;
            this.txt_ab_new_pardakht.MessegeEmptyShowInForm = false;
            this.txt_ab_new_pardakht.Name = "txt_ab_new_pardakht";
            this.txt_ab_new_pardakht.Size = new System.Drawing.Size(138, 28);
            this.txt_ab_new_pardakht.TabIndex = 63;
            this.txt_ab_new_pardakht.TypeAllChar = false;
            this.txt_ab_new_pardakht.TypeDateShamsi = false;
            this.txt_ab_new_pardakht.TypeEnglishOnly = false;
            this.txt_ab_new_pardakht.TypeFarsiOnly = false;
            this.txt_ab_new_pardakht.TypeNumricOnly = true;
            this.txt_ab_new_pardakht.TypeOtherChar = "-";
            this.txt_ab_new_pardakht.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ab_new_pardakht_KeyDown_2);
            // 
            // txt_ab_new_tarikh
            // 
            this.txt_ab_new_tarikh.Location = new System.Drawing.Point(438, 7);
            this.txt_ab_new_tarikh.Name = "txt_ab_new_tarikh";
            this.txt_ab_new_tarikh.Size = new System.Drawing.Size(100, 26);
            this.txt_ab_new_tarikh.TabIndex = 58;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(149, 9);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(40, 22);
            this.radioButton1.TabIndex = 57;
            this.radioButton1.Text = "پوز";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radioButton1_KeyDown);
            // 
            // rd_ab_bank
            // 
            this.rd_ab_bank.AutoSize = true;
            this.rd_ab_bank.Checked = true;
            this.rd_ab_bank.Location = new System.Drawing.Point(195, 9);
            this.rd_ab_bank.Name = "rd_ab_bank";
            this.rd_ab_bank.Size = new System.Drawing.Size(45, 22);
            this.rd_ab_bank.TabIndex = 56;
            this.rd_ab_bank.TabStop = true;
            this.rd_ab_bank.Text = "بانک";
            this.rd_ab_bank.UseVisualStyleBackColor = true;
            this.rd_ab_bank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rd_ab_bank_KeyDown);
            // 
            // labelX44
            // 
            // 
            // 
            // 
            this.labelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX44.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX44.Location = new System.Drawing.Point(499, 8);
            this.labelX44.Name = "labelX44";
            this.labelX44.Size = new System.Drawing.Size(72, 25);
            this.labelX44.TabIndex = 50;
            this.labelX44.Text = "تاریخ:";
            // 
            // btn_pardakht_ab_now
            // 
            this.btn_pardakht_ab_now.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_pardakht_ab_now.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_pardakht_ab_now.Location = new System.Drawing.Point(23, 8);
            this.btn_pardakht_ab_now.Name = "btn_pardakht_ab_now";
            this.btn_pardakht_ab_now.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_pardakht_ab_now.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.AltRight);
            this.btn_pardakht_ab_now.Size = new System.Drawing.Size(95, 25);
            this.btn_pardakht_ab_now.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_pardakht_ab_now.Symbol = "";
            this.btn_pardakht_ab_now.SymbolColor = System.Drawing.Color.Green;
            this.btn_pardakht_ab_now.SymbolSize = 9F;
            this.btn_pardakht_ab_now.TabIndex = 37;
            this.btn_pardakht_ab_now.Text = "ثبت پرداخت";
            this.btn_pardakht_ab_now.Tooltip = "AltRight";
            this.btn_pardakht_ab_now.Click += new System.EventHandler(this.buttonX4_Click_1);
            // 
            // labelX46
            // 
            // 
            // 
            // 
            this.labelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX46.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX46.Location = new System.Drawing.Point(409, 8);
            this.labelX46.Name = "labelX46";
            this.labelX46.Size = new System.Drawing.Size(23, 25);
            this.labelX46.TabIndex = 35;
            this.labelX46.Text = "مبلغ:";
            // 
            // grp_pardakht_sharj
            // 
            this.grp_pardakht_sharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grp_pardakht_sharj.BackColor = System.Drawing.Color.Transparent;
            this.grp_pardakht_sharj.CanvasColor = System.Drawing.SystemColors.Highlight;
            this.grp_pardakht_sharj.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.grp_pardakht_sharj.Controls.Add(this.txt_sharj_new_pardakht);
            this.grp_pardakht_sharj.Controls.Add(this.txt_sharj_new_tarikh);
            this.grp_pardakht_sharj.Controls.Add(this.radioButton2);
            this.grp_pardakht_sharj.Controls.Add(this.rd_sharj_bank);
            this.grp_pardakht_sharj.Controls.Add(this.labelX35);
            this.grp_pardakht_sharj.Controls.Add(this.btn_sharj_new_pardakht);
            this.grp_pardakht_sharj.Controls.Add(this.labelX39);
            this.grp_pardakht_sharj.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_pardakht_sharj.Location = new System.Drawing.Point(647, 270);
            this.grp_pardakht_sharj.Name = "grp_pardakht_sharj";
            this.grp_pardakht_sharj.Size = new System.Drawing.Size(611, 65);
            // 
            // 
            // 
            this.grp_pardakht_sharj.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_pardakht_sharj.Style.BackColorGradientAngle = 90;
            this.grp_pardakht_sharj.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_pardakht_sharj.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_sharj.Style.BorderBottomWidth = 1;
            this.grp_pardakht_sharj.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_pardakht_sharj.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_sharj.Style.BorderLeftWidth = 1;
            this.grp_pardakht_sharj.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_sharj.Style.BorderRightWidth = 1;
            this.grp_pardakht_sharj.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_pardakht_sharj.Style.BorderTopWidth = 1;
            this.grp_pardakht_sharj.Style.CornerDiameter = 4;
            this.grp_pardakht_sharj.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_pardakht_sharj.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_pardakht_sharj.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_pardakht_sharj.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_pardakht_sharj.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_pardakht_sharj.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_pardakht_sharj.TabIndex = 50;
            this.grp_pardakht_sharj.Text = "پرداخت سریع قبض شارژ ";
            this.grp_pardakht_sharj.Visible = false;
            // 
            // txt_sharj_new_pardakht
            // 
            this.txt_sharj_new_pardakht.AutoSprator = true;
            this.txt_sharj_new_pardakht.EnterToTab = false;
            this.txt_sharj_new_pardakht.EscToClose = true;
            this.txt_sharj_new_pardakht.FocusBackColor = System.Drawing.Color.PaleTurquoise;
            this.txt_sharj_new_pardakht.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_new_pardakht.FocusForeColor = System.Drawing.Color.Blue;
            this.txt_sharj_new_pardakht.FocusTextSelect = true;
            this.txt_sharj_new_pardakht.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_new_pardakht.Location = new System.Drawing.Point(236, 7);
            this.txt_sharj_new_pardakht.MaxLength = 15;
            this.txt_sharj_new_pardakht.MessageEmptyShowDialog = false;
            this.txt_sharj_new_pardakht.MessegeEmpty = "";
            this.txt_sharj_new_pardakht.MessegeEmptyInFormRight = true;
            this.txt_sharj_new_pardakht.MessegeEmptyShowInForm = false;
            this.txt_sharj_new_pardakht.Name = "txt_sharj_new_pardakht";
            this.txt_sharj_new_pardakht.Size = new System.Drawing.Size(138, 28);
            this.txt_sharj_new_pardakht.TabIndex = 62;
            this.txt_sharj_new_pardakht.TypeAllChar = false;
            this.txt_sharj_new_pardakht.TypeDateShamsi = false;
            this.txt_sharj_new_pardakht.TypeEnglishOnly = false;
            this.txt_sharj_new_pardakht.TypeFarsiOnly = false;
            this.txt_sharj_new_pardakht.TypeNumricOnly = true;
            this.txt_sharj_new_pardakht.TypeOtherChar = "-";
            this.txt_sharj_new_pardakht.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_sharj_new_pardakht_KeyDown_2);
            // 
            // txt_sharj_new_tarikh
            // 
            this.txt_sharj_new_tarikh.Location = new System.Drawing.Point(439, 6);
            this.txt_sharj_new_tarikh.Name = "txt_sharj_new_tarikh";
            this.txt_sharj_new_tarikh.Size = new System.Drawing.Size(100, 26);
            this.txt_sharj_new_tarikh.TabIndex = 56;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(132, 9);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(40, 22);
            this.radioButton2.TabIndex = 55;
            this.radioButton2.Text = "پوز";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radioButton2_KeyDown);
            // 
            // rd_sharj_bank
            // 
            this.rd_sharj_bank.AutoSize = true;
            this.rd_sharj_bank.Checked = true;
            this.rd_sharj_bank.Location = new System.Drawing.Point(178, 9);
            this.rd_sharj_bank.Name = "rd_sharj_bank";
            this.rd_sharj_bank.Size = new System.Drawing.Size(45, 22);
            this.rd_sharj_bank.TabIndex = 54;
            this.rd_sharj_bank.TabStop = true;
            this.rd_sharj_bank.Text = "بانک";
            this.rd_sharj_bank.UseVisualStyleBackColor = true;
            this.rd_sharj_bank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rd_sharj_bank_KeyDown);
            // 
            // labelX35
            // 
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX35.Location = new System.Drawing.Point(545, 7);
            this.labelX35.Name = "labelX35";
            this.labelX35.Size = new System.Drawing.Size(36, 25);
            this.labelX35.TabIndex = 50;
            this.labelX35.Text = "تاریخ:";
            // 
            // btn_sharj_new_pardakht
            // 
            this.btn_sharj_new_pardakht.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_sharj_new_pardakht.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_sharj_new_pardakht.Location = new System.Drawing.Point(31, 8);
            this.btn_sharj_new_pardakht.Name = "btn_sharj_new_pardakht";
            this.btn_sharj_new_pardakht.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_sharj_new_pardakht.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.AltRight);
            this.btn_sharj_new_pardakht.Size = new System.Drawing.Size(95, 25);
            this.btn_sharj_new_pardakht.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_sharj_new_pardakht.Symbol = "";
            this.btn_sharj_new_pardakht.SymbolColor = System.Drawing.Color.Green;
            this.btn_sharj_new_pardakht.SymbolSize = 9F;
            this.btn_sharj_new_pardakht.TabIndex = 53;
            this.btn_sharj_new_pardakht.Text = "ثبت پرداخت";
            this.btn_sharj_new_pardakht.Tooltip = "AltRight";
            this.btn_sharj_new_pardakht.Click += new System.EventHandler(this.btn_sharj_new_pardakht_Click);
            // 
            // labelX39
            // 
            // 
            // 
            // 
            this.labelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX39.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX39.Location = new System.Drawing.Point(391, 7);
            this.labelX39.Name = "labelX39";
            this.labelX39.Size = new System.Drawing.Size(26, 25);
            this.labelX39.TabIndex = 35;
            this.labelX39.Text = "مبلغ:";
            // 
            // grp_ab
            // 
            this.grp_ab.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grp_ab.BackColor = System.Drawing.Color.Transparent;
            this.grp_ab.CanvasColor = System.Drawing.SystemColors.Highlight;
            this.grp_ab.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.grp_ab.Controls.Add(this.lbl_ab_mablagh_kol);
            this.grp_ab.Controls.Add(this.labelX28);
            this.grp_ab.Controls.Add(this.txt_ab_bestankari);
            this.grp_ab.Controls.Add(this.lbl_ab_mandeh);
            this.grp_ab.Controls.Add(this.btn_new_ab_ghabz);
            this.grp_ab.Controls.Add(this.lbl_ab_pardakhti);
            this.grp_ab.Controls.Add(this.btn_pardakht_ab);
            this.grp_ab.Controls.Add(this.lbl_ab_gcode);
            this.grp_ab.Controls.Add(this.labelX16);
            this.grp_ab.Controls.Add(this.lbl_ab_vaziat);
            this.grp_ab.Controls.Add(this.btn_print_ab_Ghabz);
            this.grp_ab.Controls.Add(this.btn_show_ab_Ghabz);
            this.grp_ab.Controls.Add(this.labelX25);
            this.grp_ab.Controls.Add(this.labelX17);
            this.grp_ab.Controls.Add(this.lbl_ab_mablaghkol_horoof);
            this.grp_ab.Controls.Add(this.labelX23);
            this.grp_ab.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_ab.Location = new System.Drawing.Point(29, 133);
            this.grp_ab.Name = "grp_ab";
            this.grp_ab.Size = new System.Drawing.Size(611, 134);
            // 
            // 
            // 
            this.grp_ab.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_ab.Style.BackColorGradientAngle = 90;
            this.grp_ab.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_ab.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_ab.Style.BorderBottomWidth = 1;
            this.grp_ab.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_ab.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_ab.Style.BorderLeftWidth = 1;
            this.grp_ab.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_ab.Style.BorderRightWidth = 1;
            this.grp_ab.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_ab.Style.BorderTopWidth = 1;
            this.grp_ab.Style.CornerDiameter = 4;
            this.grp_ab.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_ab.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_ab.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_ab.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_ab.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_ab.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_ab.TabIndex = 22;
            this.grp_ab.Text = "امورآب";
            // 
            // lbl_ab_mablagh_kol
            // 
            this.lbl_ab_mablagh_kol.AutoSprator = true;
            this.lbl_ab_mablagh_kol.BackColor = System.Drawing.Color.White;
            this.lbl_ab_mablagh_kol.EnterToTab = false;
            this.lbl_ab_mablagh_kol.EscToClose = true;
            this.lbl_ab_mablagh_kol.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_ab_mablagh_kol.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_mablagh_kol.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_ab_mablagh_kol.FocusTextSelect = true;
            this.lbl_ab_mablagh_kol.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_mablagh_kol.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_mablagh_kol.Location = new System.Drawing.Point(394, 33);
            this.lbl_ab_mablagh_kol.MaxLength = 15;
            this.lbl_ab_mablagh_kol.MessageEmptyShowDialog = false;
            this.lbl_ab_mablagh_kol.MessegeEmpty = "";
            this.lbl_ab_mablagh_kol.MessegeEmptyInFormRight = true;
            this.lbl_ab_mablagh_kol.MessegeEmptyShowInForm = false;
            this.lbl_ab_mablagh_kol.Name = "lbl_ab_mablagh_kol";
            this.lbl_ab_mablagh_kol.ReadOnly = true;
            this.lbl_ab_mablagh_kol.Size = new System.Drawing.Size(122, 28);
            this.lbl_ab_mablagh_kol.TabIndex = 61;
            this.lbl_ab_mablagh_kol.TypeAllChar = false;
            this.lbl_ab_mablagh_kol.TypeDateShamsi = false;
            this.lbl_ab_mablagh_kol.TypeEnglishOnly = false;
            this.lbl_ab_mablagh_kol.TypeFarsiOnly = false;
            this.lbl_ab_mablagh_kol.TypeNumricOnly = true;
            this.lbl_ab_mablagh_kol.TypeOtherChar = "-";
            // 
            // labelX28
            // 
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX28.Location = new System.Drawing.Point(133, 64);
            this.labelX28.Name = "labelX28";
            this.labelX28.Size = new System.Drawing.Size(89, 33);
            this.labelX28.TabIndex = 55;
            this.labelX28.Text = "بستانکاری دوره بعد :";
            // 
            // txt_ab_bestankari
            // 
            this.txt_ab_bestankari.AutoSprator = true;
            this.txt_ab_bestankari.BackColor = System.Drawing.Color.White;
            this.txt_ab_bestankari.EnterToTab = false;
            this.txt_ab_bestankari.EscToClose = true;
            this.txt_ab_bestankari.FocusBackColor = System.Drawing.Color.Yellow;
            this.txt_ab_bestankari.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_bestankari.FocusForeColor = System.Drawing.Color.Blue;
            this.txt_ab_bestankari.FocusTextSelect = true;
            this.txt_ab_bestankari.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_bestankari.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_bestankari.Location = new System.Drawing.Point(6, 69);
            this.txt_ab_bestankari.MaxLength = 15;
            this.txt_ab_bestankari.MessageEmptyShowDialog = false;
            this.txt_ab_bestankari.MessegeEmpty = "";
            this.txt_ab_bestankari.MessegeEmptyInFormRight = true;
            this.txt_ab_bestankari.MessegeEmptyShowInForm = false;
            this.txt_ab_bestankari.Name = "txt_ab_bestankari";
            this.txt_ab_bestankari.ReadOnly = true;
            this.txt_ab_bestankari.Size = new System.Drawing.Size(122, 28);
            this.txt_ab_bestankari.TabIndex = 60;
            this.txt_ab_bestankari.TypeAllChar = false;
            this.txt_ab_bestankari.TypeDateShamsi = false;
            this.txt_ab_bestankari.TypeEnglishOnly = false;
            this.txt_ab_bestankari.TypeFarsiOnly = false;
            this.txt_ab_bestankari.TypeNumricOnly = true;
            this.txt_ab_bestankari.TypeOtherChar = "-";
            // 
            // lbl_ab_mandeh
            // 
            this.lbl_ab_mandeh.AutoSprator = true;
            this.lbl_ab_mandeh.BackColor = System.Drawing.Color.White;
            this.lbl_ab_mandeh.EnterToTab = false;
            this.lbl_ab_mandeh.EscToClose = true;
            this.lbl_ab_mandeh.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_ab_mandeh.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_mandeh.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_ab_mandeh.FocusTextSelect = true;
            this.lbl_ab_mandeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_mandeh.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_mandeh.Location = new System.Drawing.Point(233, 69);
            this.lbl_ab_mandeh.MaxLength = 15;
            this.lbl_ab_mandeh.MessageEmptyShowDialog = false;
            this.lbl_ab_mandeh.MessegeEmpty = "";
            this.lbl_ab_mandeh.MessegeEmptyInFormRight = true;
            this.lbl_ab_mandeh.MessegeEmptyShowInForm = false;
            this.lbl_ab_mandeh.Name = "lbl_ab_mandeh";
            this.lbl_ab_mandeh.ReadOnly = true;
            this.lbl_ab_mandeh.Size = new System.Drawing.Size(122, 28);
            this.lbl_ab_mandeh.TabIndex = 59;
            this.lbl_ab_mandeh.TypeAllChar = false;
            this.lbl_ab_mandeh.TypeDateShamsi = false;
            this.lbl_ab_mandeh.TypeEnglishOnly = false;
            this.lbl_ab_mandeh.TypeFarsiOnly = false;
            this.lbl_ab_mandeh.TypeNumricOnly = true;
            this.lbl_ab_mandeh.TypeOtherChar = "-";
            // 
            // btn_new_ab_ghabz
            // 
            this.btn_new_ab_ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_new_ab_ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_new_ab_ghabz.Location = new System.Drawing.Point(38, 35);
            this.btn_new_ab_ghabz.Name = "btn_new_ab_ghabz";
            this.btn_new_ab_ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_new_ab_ghabz.Size = new System.Drawing.Size(350, 23);
            this.btn_new_ab_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_new_ab_ghabz.Symbol = "";
            this.btn_new_ab_ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_new_ab_ghabz.SymbolSize = 9F;
            this.btn_new_ab_ghabz.TabIndex = 53;
            this.btn_new_ab_ghabz.Text = "صدور قبض جدید آب مشترک";
            this.btn_new_ab_ghabz.Visible = false;
            this.btn_new_ab_ghabz.Click += new System.EventHandler(this.btn_new_ab_ghabz_Click);
            // 
            // lbl_ab_pardakhti
            // 
            this.lbl_ab_pardakhti.AutoSprator = true;
            this.lbl_ab_pardakhti.BackColor = System.Drawing.Color.White;
            this.lbl_ab_pardakhti.EnterToTab = false;
            this.lbl_ab_pardakhti.EscToClose = true;
            this.lbl_ab_pardakhti.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_ab_pardakhti.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_pardakhti.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_ab_pardakhti.FocusTextSelect = true;
            this.lbl_ab_pardakhti.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_ab_pardakhti.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_pardakhti.Location = new System.Drawing.Point(394, 68);
            this.lbl_ab_pardakhti.MaxLength = 15;
            this.lbl_ab_pardakhti.MessageEmptyShowDialog = false;
            this.lbl_ab_pardakhti.MessegeEmpty = "";
            this.lbl_ab_pardakhti.MessegeEmptyInFormRight = true;
            this.lbl_ab_pardakhti.MessegeEmptyShowInForm = false;
            this.lbl_ab_pardakhti.Name = "lbl_ab_pardakhti";
            this.lbl_ab_pardakhti.ReadOnly = true;
            this.lbl_ab_pardakhti.Size = new System.Drawing.Size(122, 28);
            this.lbl_ab_pardakhti.TabIndex = 58;
            this.lbl_ab_pardakhti.TypeAllChar = false;
            this.lbl_ab_pardakhti.TypeDateShamsi = false;
            this.lbl_ab_pardakhti.TypeEnglishOnly = false;
            this.lbl_ab_pardakhti.TypeFarsiOnly = false;
            this.lbl_ab_pardakhti.TypeNumricOnly = true;
            this.lbl_ab_pardakhti.TypeOtherChar = "-";
            // 
            // btn_pardakht_ab
            // 
            this.btn_pardakht_ab.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_pardakht_ab.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_pardakht_ab.Location = new System.Drawing.Point(251, 6);
            this.btn_pardakht_ab.Name = "btn_pardakht_ab";
            this.btn_pardakht_ab.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_pardakht_ab.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F12);
            this.btn_pardakht_ab.Size = new System.Drawing.Size(135, 23);
            this.btn_pardakht_ab.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_pardakht_ab.Symbol = "";
            this.btn_pardakht_ab.SymbolColor = System.Drawing.Color.Green;
            this.btn_pardakht_ab.SymbolSize = 9F;
            this.btn_pardakht_ab.TabIndex = 51;
            this.btn_pardakht_ab.Text = "پرداخت آب-F12";
            this.btn_pardakht_ab.Tooltip = "CtrlS";
            this.btn_pardakht_ab.Click += new System.EventHandler(this.btn_pardakht_ab_Click);
            // 
            // lbl_ab_gcode
            // 
            // 
            // 
            // 
            this.lbl_ab_gcode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_gcode.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_ab_gcode.ForeColor = System.Drawing.Color.Teal;
            this.lbl_ab_gcode.Location = new System.Drawing.Point(448, 3);
            this.lbl_ab_gcode.Name = "lbl_ab_gcode";
            this.lbl_ab_gcode.Size = new System.Drawing.Size(107, 25);
            this.lbl_ab_gcode.TabIndex = 36;
            this.lbl_ab_gcode.Text = " ";
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX16.Location = new System.Drawing.Point(540, 4);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(56, 25);
            this.labelX16.TabIndex = 35;
            this.labelX16.Text = "کدقبض:";
            // 
            // lbl_ab_vaziat
            // 
            // 
            // 
            // 
            this.lbl_ab_vaziat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_vaziat.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.lbl_ab_vaziat.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ab_vaziat.Location = new System.Drawing.Point(403, 3);
            this.lbl_ab_vaziat.Name = "lbl_ab_vaziat";
            this.lbl_ab_vaziat.Size = new System.Drawing.Size(30, 23);
            this.lbl_ab_vaziat.Symbol = "";
            this.lbl_ab_vaziat.TabIndex = 22;
            this.lbl_ab_vaziat.Text = " ";
            this.lbl_ab_vaziat.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btn_print_ab_Ghabz
            // 
            this.btn_print_ab_Ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_ab_Ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_ab_Ghabz.Location = new System.Drawing.Point(12, 6);
            this.btn_print_ab_Ghabz.Name = "btn_print_ab_Ghabz";
            this.btn_print_ab_Ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_print_ab_Ghabz.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.AltLeft);
            this.btn_print_ab_Ghabz.Size = new System.Drawing.Size(95, 23);
            this.btn_print_ab_Ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_ab_Ghabz.Symbol = "";
            this.btn_print_ab_Ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_ab_Ghabz.SymbolSize = 9F;
            this.btn_print_ab_Ghabz.TabIndex = 38;
            this.btn_print_ab_Ghabz.Text = "چاپ قبض";
            this.btn_print_ab_Ghabz.Tooltip = "AltLeft";
            this.btn_print_ab_Ghabz.Click += new System.EventHandler(this.btn_print_ab_Ghabz_Click);
            // 
            // btn_show_ab_Ghabz
            // 
            this.btn_show_ab_Ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show_ab_Ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show_ab_Ghabz.Location = new System.Drawing.Point(113, 6);
            this.btn_show_ab_Ghabz.Name = "btn_show_ab_Ghabz";
            this.btn_show_ab_Ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_show_ab_Ghabz.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F11);
            this.btn_show_ab_Ghabz.Size = new System.Drawing.Size(129, 23);
            this.btn_show_ab_Ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show_ab_Ghabz.Symbol = "";
            this.btn_show_ab_Ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_show_ab_Ghabz.SymbolSize = 9F;
            this.btn_show_ab_Ghabz.TabIndex = 34;
            this.btn_show_ab_Ghabz.Text = "مشاهده قبض-F11";
            this.btn_show_ab_Ghabz.Tooltip = "CtrlA";
            this.btn_show_ab_Ghabz.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // labelX25
            // 
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX25.Location = new System.Drawing.Point(514, 64);
            this.labelX25.Name = "labelX25";
            this.labelX25.Size = new System.Drawing.Size(90, 33);
            this.labelX25.TabIndex = 42;
            this.labelX25.Text = "پرداختی این قبض:";
            // 
            // labelX17
            // 
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX17.Location = new System.Drawing.Point(514, 28);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(90, 38);
            this.labelX17.TabIndex = 29;
            this.labelX17.Text = "مبلغ قابل پرداخت:";
            this.labelX17.Click += new System.EventHandler(this.labelX17_Click);
            // 
            // lbl_ab_mablaghkol_horoof
            // 
            // 
            // 
            // 
            this.lbl_ab_mablaghkol_horoof.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_mablaghkol_horoof.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_ab_mablaghkol_horoof.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_ab_mablaghkol_horoof.Location = new System.Drawing.Point(-2, 35);
            this.lbl_ab_mablaghkol_horoof.Name = "lbl_ab_mablaghkol_horoof";
            this.lbl_ab_mablaghkol_horoof.Size = new System.Drawing.Size(384, 25);
            this.lbl_ab_mablaghkol_horoof.TabIndex = 25;
            this.lbl_ab_mablaghkol_horoof.Text = " ";
            this.lbl_ab_mablaghkol_horoof.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lbl_ab_mablaghkol_horoof.Click += new System.EventHandler(this.lbl_ab_mablaghkol_horoof_Click);
            // 
            // labelX23
            // 
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX23.Location = new System.Drawing.Point(351, 66);
            this.labelX23.Name = "labelX23";
            this.labelX23.Size = new System.Drawing.Size(33, 33);
            this.labelX23.TabIndex = 44;
            this.labelX23.Text = "مانده :";
            // 
            // grpMoshtarek
            // 
            this.grpMoshtarek.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grpMoshtarek.BackColor = System.Drawing.Color.Transparent;
            this.grpMoshtarek.CanvasColor = System.Drawing.SystemColors.Highlight;
            this.grpMoshtarek.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.grpMoshtarek.Controls.Add(this.txtMobile);
            this.grpMoshtarek.Controls.Add(this.labelX36);
            this.grpMoshtarek.Controls.Add(this.txtElhaghieh);
            this.grpMoshtarek.Controls.Add(this.labelX19);
            this.grpMoshtarek.Controls.Add(this.txt_moshtarek_address);
            this.grpMoshtarek.Controls.Add(this.labelX12);
            this.grpMoshtarek.Controls.Add(this.txt_moshtarek_phone);
            this.grpMoshtarek.Controls.Add(this.labelX11);
            this.grpMoshtarek.Controls.Add(this.txt_moshtarek_modir_amel);
            this.grpMoshtarek.Controls.Add(this.labelX10);
            this.grpMoshtarek.Controls.Add(this.txt_moshtarek_name);
            this.grpMoshtarek.Controls.Add(this.labelX9);
            this.grpMoshtarek.Controls.Add(this.txt_moshtarek_code);
            this.grpMoshtarek.Controls.Add(this.labelX8);
            this.grpMoshtarek.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpMoshtarek.Font = new System.Drawing.Font("B Yekan", 11F);
            this.grpMoshtarek.Location = new System.Drawing.Point(70, 6);
            this.grpMoshtarek.Name = "grpMoshtarek";
            this.grpMoshtarek.Size = new System.Drawing.Size(1127, 121);
            // 
            // 
            // 
            this.grpMoshtarek.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpMoshtarek.Style.BackColorGradientAngle = 90;
            this.grpMoshtarek.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpMoshtarek.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpMoshtarek.Style.BorderBottomWidth = 1;
            this.grpMoshtarek.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpMoshtarek.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpMoshtarek.Style.BorderLeftWidth = 1;
            this.grpMoshtarek.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpMoshtarek.Style.BorderRightWidth = 1;
            this.grpMoshtarek.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpMoshtarek.Style.BorderTopWidth = 1;
            this.grpMoshtarek.Style.CornerDiameter = 4;
            this.grpMoshtarek.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpMoshtarek.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpMoshtarek.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpMoshtarek.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpMoshtarek.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpMoshtarek.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpMoshtarek.TabIndex = 21;
            this.grpMoshtarek.Text = "اطلاعات مشترک";
            // 
            // txtMobile
            // 
            this.txtMobile.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMobile.Border.Class = "TextBoxBorder";
            this.txtMobile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMobile.DisabledBackColor = System.Drawing.Color.White;
            this.txtMobile.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txtMobile.ForeColor = System.Drawing.Color.Black;
            this.txtMobile.Location = new System.Drawing.Point(640, 41);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.PreventEnterBeep = true;
            this.txtMobile.ReadOnly = true;
            this.txtMobile.Size = new System.Drawing.Size(150, 26);
            this.txtMobile.TabIndex = 42;
            this.txtMobile.Text = " ";
            // 
            // labelX36
            // 
            // 
            // 
            // 
            this.labelX36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX36.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX36.Location = new System.Drawing.Point(807, 38);
            this.labelX36.Name = "labelX36";
            this.labelX36.Size = new System.Drawing.Size(73, 33);
            this.labelX36.TabIndex = 41;
            this.labelX36.Text = "تلفن همراه :";
            // 
            // txtElhaghieh
            // 
            this.txtElhaghieh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtElhaghieh.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.txtElhaghieh.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.txtElhaghieh.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.txtElhaghieh.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Dash;
            this.txtElhaghieh.Border.Class = "TextBoxBorder";
            this.txtElhaghieh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtElhaghieh.DisabledBackColor = System.Drawing.Color.White;
            this.txtElhaghieh.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txtElhaghieh.ForeColor = System.Drawing.Color.Black;
            this.txtElhaghieh.Location = new System.Drawing.Point(30, 7);
            this.txtElhaghieh.Name = "txtElhaghieh";
            this.txtElhaghieh.PreventEnterBeep = true;
            this.txtElhaghieh.ReadOnly = true;
            this.txtElhaghieh.Size = new System.Drawing.Size(188, 26);
            this.txtElhaghieh.TabIndex = 40;
            this.txtElhaghieh.Text = " ";
            // 
            // labelX19
            // 
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX19.Location = new System.Drawing.Point(201, 4);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(136, 33);
            this.labelX19.TabIndex = 39;
            this.labelX19.Text = "قراردادهای الحاقیه :";
            // 
            // txt_moshtarek_address
            // 
            this.txt_moshtarek_address.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_address.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_address.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_address.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_address.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_moshtarek_address.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_address.Location = new System.Drawing.Point(30, 41);
            this.txt_moshtarek_address.Name = "txt_moshtarek_address";
            this.txt_moshtarek_address.PreventEnterBeep = true;
            this.txt_moshtarek_address.ReadOnly = true;
            this.txt_moshtarek_address.Size = new System.Drawing.Size(520, 26);
            this.txt_moshtarek_address.TabIndex = 22;
            this.txt_moshtarek_address.Text = " ";
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX12.Location = new System.Drawing.Point(563, 38);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(61, 33);
            this.labelX12.TabIndex = 21;
            this.labelX12.Text = "آدرس :";
            // 
            // txt_moshtarek_phone
            // 
            this.txt_moshtarek_phone.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_phone.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_phone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_phone.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_phone.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_moshtarek_phone.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_phone.Location = new System.Drawing.Point(887, 41);
            this.txt_moshtarek_phone.Name = "txt_moshtarek_phone";
            this.txt_moshtarek_phone.PreventEnterBeep = true;
            this.txt_moshtarek_phone.ReadOnly = true;
            this.txt_moshtarek_phone.Size = new System.Drawing.Size(150, 26);
            this.txt_moshtarek_phone.TabIndex = 20;
            this.txt_moshtarek_phone.Text = " ";
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX11.Location = new System.Drawing.Point(1060, 38);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(61, 33);
            this.labelX11.TabIndex = 19;
            this.labelX11.Text = "تلفن تماس :";
            // 
            // txt_moshtarek_modir_amel
            // 
            this.txt_moshtarek_modir_amel.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_modir_amel.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_modir_amel.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_modir_amel.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_modir_amel.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_moshtarek_modir_amel.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_modir_amel.Location = new System.Drawing.Point(350, 7);
            this.txt_moshtarek_modir_amel.Name = "txt_moshtarek_modir_amel";
            this.txt_moshtarek_modir_amel.PreventEnterBeep = true;
            this.txt_moshtarek_modir_amel.ReadOnly = true;
            this.txt_moshtarek_modir_amel.Size = new System.Drawing.Size(200, 26);
            this.txt_moshtarek_modir_amel.TabIndex = 18;
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX10.Location = new System.Drawing.Point(523, 4);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(101, 33);
            this.labelX10.TabIndex = 17;
            this.labelX10.Text = "مدیریت عامل :";
            // 
            // txt_moshtarek_name
            // 
            this.txt_moshtarek_name.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_name.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_name.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_name.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_name.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_moshtarek_name.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_name.Location = new System.Drawing.Point(640, 7);
            this.txt_moshtarek_name.Name = "txt_moshtarek_name";
            this.txt_moshtarek_name.PreventEnterBeep = true;
            this.txt_moshtarek_name.ReadOnly = true;
            this.txt_moshtarek_name.Size = new System.Drawing.Size(229, 26);
            this.txt_moshtarek_name.TabIndex = 16;
            this.txt_moshtarek_name.Text = " ";
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX9.Location = new System.Drawing.Point(871, 4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(90, 33);
            this.labelX9.TabIndex = 15;
            this.labelX9.Text = "نام واحد تولیدی:";
            // 
            // txt_moshtarek_code
            // 
            this.txt_moshtarek_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_code.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_code.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_moshtarek_code.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_code.Location = new System.Drawing.Point(969, 7);
            this.txt_moshtarek_code.Name = "txt_moshtarek_code";
            this.txt_moshtarek_code.PreventEnterBeep = true;
            this.txt_moshtarek_code.ReadOnly = true;
            this.txt_moshtarek_code.Size = new System.Drawing.Size(67, 26);
            this.txt_moshtarek_code.TabIndex = 14;
            this.txt_moshtarek_code.Text = " ";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX8.Location = new System.Drawing.Point(1042, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(79, 33);
            this.labelX8.TabIndex = 13;
            this.labelX8.Text = "شماره قرارداد :";
            // 
            // grp_sharj
            // 
            this.grp_sharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grp_sharj.BackColor = System.Drawing.Color.Transparent;
            this.grp_sharj.CanvasColor = System.Drawing.SystemColors.Highlight;
            this.grp_sharj.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.grp_sharj.Controls.Add(this.lbl_moshtarek_mablagh_kol);
            this.grp_sharj.Controls.Add(this.txt_sharj_bestnkari);
            this.grp_sharj.Controls.Add(this.lbl_sharj_mandeh);
            this.grp_sharj.Controls.Add(this.lbl_sharj_pardakhti);
            this.grp_sharj.Controls.Add(this.labelX13);
            this.grp_sharj.Controls.Add(this.btn_new_sharj_ghabz);
            this.grp_sharj.Controls.Add(this.btn_pardakht_sharj);
            this.grp_sharj.Controls.Add(this.btn_print_Sharj_Ghabz);
            this.grp_sharj.Controls.Add(this.lbl_sharj_gcode);
            this.grp_sharj.Controls.Add(this.labelX20);
            this.grp_sharj.Controls.Add(this.btn_show_Sharj_Ghabz);
            this.grp_sharj.Controls.Add(this.lbl_moshtarek_mablgh_kol_str);
            this.grp_sharj.Controls.Add(this.labelX21);
            this.grp_sharj.Controls.Add(this.labelX22);
            this.grp_sharj.Controls.Add(this.lbl_sharj_vaziat);
            this.grp_sharj.Controls.Add(this.labelX15);
            this.grp_sharj.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_sharj.Location = new System.Drawing.Point(646, 133);
            this.grp_sharj.Name = "grp_sharj";
            this.grp_sharj.Size = new System.Drawing.Size(611, 134);
            // 
            // 
            // 
            this.grp_sharj.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_sharj.Style.BackColorGradientAngle = 90;
            this.grp_sharj.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_sharj.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_sharj.Style.BorderBottomWidth = 1;
            this.grp_sharj.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_sharj.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_sharj.Style.BorderLeftWidth = 1;
            this.grp_sharj.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_sharj.Style.BorderRightWidth = 1;
            this.grp_sharj.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_sharj.Style.BorderTopWidth = 1;
            this.grp_sharj.Style.CornerDiameter = 4;
            this.grp_sharj.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_sharj.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_sharj.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_sharj.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_sharj.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_sharj.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_sharj.TabIndex = 20;
            this.grp_sharj.Text = "امور شارژ";
            // 
            // lbl_moshtarek_mablagh_kol
            // 
            this.lbl_moshtarek_mablagh_kol.AutoSprator = true;
            this.lbl_moshtarek_mablagh_kol.BackColor = System.Drawing.Color.White;
            this.lbl_moshtarek_mablagh_kol.EnterToTab = false;
            this.lbl_moshtarek_mablagh_kol.EscToClose = true;
            this.lbl_moshtarek_mablagh_kol.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_moshtarek_mablagh_kol.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_moshtarek_mablagh_kol.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_moshtarek_mablagh_kol.FocusTextSelect = true;
            this.lbl_moshtarek_mablagh_kol.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_moshtarek_mablagh_kol.ForeColor = System.Drawing.Color.Black;
            this.lbl_moshtarek_mablagh_kol.Location = new System.Drawing.Point(393, 37);
            this.lbl_moshtarek_mablagh_kol.MaxLength = 15;
            this.lbl_moshtarek_mablagh_kol.MessageEmptyShowDialog = false;
            this.lbl_moshtarek_mablagh_kol.MessegeEmpty = "";
            this.lbl_moshtarek_mablagh_kol.MessegeEmptyInFormRight = true;
            this.lbl_moshtarek_mablagh_kol.MessegeEmptyShowInForm = false;
            this.lbl_moshtarek_mablagh_kol.Name = "lbl_moshtarek_mablagh_kol";
            this.lbl_moshtarek_mablagh_kol.ReadOnly = true;
            this.lbl_moshtarek_mablagh_kol.Size = new System.Drawing.Size(122, 28);
            this.lbl_moshtarek_mablagh_kol.TabIndex = 57;
            this.lbl_moshtarek_mablagh_kol.TypeAllChar = false;
            this.lbl_moshtarek_mablagh_kol.TypeDateShamsi = false;
            this.lbl_moshtarek_mablagh_kol.TypeEnglishOnly = false;
            this.lbl_moshtarek_mablagh_kol.TypeFarsiOnly = false;
            this.lbl_moshtarek_mablagh_kol.TypeNumricOnly = true;
            this.lbl_moshtarek_mablagh_kol.TypeOtherChar = "-";
            // 
            // txt_sharj_bestnkari
            // 
            this.txt_sharj_bestnkari.AutoSprator = true;
            this.txt_sharj_bestnkari.BackColor = System.Drawing.Color.White;
            this.txt_sharj_bestnkari.EnterToTab = false;
            this.txt_sharj_bestnkari.EscToClose = true;
            this.txt_sharj_bestnkari.FocusBackColor = System.Drawing.Color.Yellow;
            this.txt_sharj_bestnkari.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_bestnkari.FocusForeColor = System.Drawing.Color.Blue;
            this.txt_sharj_bestnkari.FocusTextSelect = true;
            this.txt_sharj_bestnkari.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_bestnkari.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_bestnkari.Location = new System.Drawing.Point(6, 73);
            this.txt_sharj_bestnkari.MaxLength = 15;
            this.txt_sharj_bestnkari.MessageEmptyShowDialog = false;
            this.txt_sharj_bestnkari.MessegeEmpty = "";
            this.txt_sharj_bestnkari.MessegeEmptyInFormRight = true;
            this.txt_sharj_bestnkari.MessegeEmptyShowInForm = false;
            this.txt_sharj_bestnkari.Name = "txt_sharj_bestnkari";
            this.txt_sharj_bestnkari.ReadOnly = true;
            this.txt_sharj_bestnkari.Size = new System.Drawing.Size(122, 28);
            this.txt_sharj_bestnkari.TabIndex = 56;
            this.txt_sharj_bestnkari.TypeAllChar = false;
            this.txt_sharj_bestnkari.TypeDateShamsi = false;
            this.txt_sharj_bestnkari.TypeEnglishOnly = false;
            this.txt_sharj_bestnkari.TypeFarsiOnly = false;
            this.txt_sharj_bestnkari.TypeNumricOnly = true;
            this.txt_sharj_bestnkari.TypeOtherChar = "-";
            // 
            // lbl_sharj_mandeh
            // 
            this.lbl_sharj_mandeh.AutoSprator = true;
            this.lbl_sharj_mandeh.BackColor = System.Drawing.Color.White;
            this.lbl_sharj_mandeh.EnterToTab = false;
            this.lbl_sharj_mandeh.EscToClose = true;
            this.lbl_sharj_mandeh.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_sharj_mandeh.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_sharj_mandeh.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_sharj_mandeh.FocusTextSelect = true;
            this.lbl_sharj_mandeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_sharj_mandeh.ForeColor = System.Drawing.Color.Black;
            this.lbl_sharj_mandeh.Location = new System.Drawing.Point(228, 73);
            this.lbl_sharj_mandeh.MaxLength = 15;
            this.lbl_sharj_mandeh.MessageEmptyShowDialog = false;
            this.lbl_sharj_mandeh.MessegeEmpty = "";
            this.lbl_sharj_mandeh.MessegeEmptyInFormRight = true;
            this.lbl_sharj_mandeh.MessegeEmptyShowInForm = false;
            this.lbl_sharj_mandeh.Name = "lbl_sharj_mandeh";
            this.lbl_sharj_mandeh.ReadOnly = true;
            this.lbl_sharj_mandeh.Size = new System.Drawing.Size(122, 28);
            this.lbl_sharj_mandeh.TabIndex = 55;
            this.lbl_sharj_mandeh.TypeAllChar = false;
            this.lbl_sharj_mandeh.TypeDateShamsi = false;
            this.lbl_sharj_mandeh.TypeEnglishOnly = false;
            this.lbl_sharj_mandeh.TypeFarsiOnly = false;
            this.lbl_sharj_mandeh.TypeNumricOnly = true;
            this.lbl_sharj_mandeh.TypeOtherChar = "-";
            // 
            // lbl_sharj_pardakhti
            // 
            this.lbl_sharj_pardakhti.AutoSprator = true;
            this.lbl_sharj_pardakhti.BackColor = System.Drawing.Color.White;
            this.lbl_sharj_pardakhti.EnterToTab = false;
            this.lbl_sharj_pardakhti.EscToClose = true;
            this.lbl_sharj_pardakhti.FocusBackColor = System.Drawing.Color.Yellow;
            this.lbl_sharj_pardakhti.FocusFont = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_sharj_pardakhti.FocusForeColor = System.Drawing.Color.Blue;
            this.lbl_sharj_pardakhti.FocusTextSelect = true;
            this.lbl_sharj_pardakhti.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_sharj_pardakhti.ForeColor = System.Drawing.Color.Black;
            this.lbl_sharj_pardakhti.Location = new System.Drawing.Point(394, 72);
            this.lbl_sharj_pardakhti.MaxLength = 15;
            this.lbl_sharj_pardakhti.MessageEmptyShowDialog = false;
            this.lbl_sharj_pardakhti.MessegeEmpty = "";
            this.lbl_sharj_pardakhti.MessegeEmptyInFormRight = true;
            this.lbl_sharj_pardakhti.MessegeEmptyShowInForm = false;
            this.lbl_sharj_pardakhti.Name = "lbl_sharj_pardakhti";
            this.lbl_sharj_pardakhti.ReadOnly = true;
            this.lbl_sharj_pardakhti.Size = new System.Drawing.Size(122, 28);
            this.lbl_sharj_pardakhti.TabIndex = 54;
            this.lbl_sharj_pardakhti.TypeAllChar = false;
            this.lbl_sharj_pardakhti.TypeDateShamsi = false;
            this.lbl_sharj_pardakhti.TypeEnglishOnly = false;
            this.lbl_sharj_pardakhti.TypeFarsiOnly = false;
            this.lbl_sharj_pardakhti.TypeNumricOnly = true;
            this.lbl_sharj_pardakhti.TypeOtherChar = "-";
            // 
            // labelX13
            // 
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX13.Location = new System.Drawing.Point(132, 71);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(85, 33);
            this.labelX13.TabIndex = 53;
            this.labelX13.Text = "بستانکاری دوره بعد :";
            // 
            // btn_new_sharj_ghabz
            // 
            this.btn_new_sharj_ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_new_sharj_ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_new_sharj_ghabz.Location = new System.Drawing.Point(25, 37);
            this.btn_new_sharj_ghabz.Name = "btn_new_sharj_ghabz";
            this.btn_new_sharj_ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_new_sharj_ghabz.Size = new System.Drawing.Size(350, 23);
            this.btn_new_sharj_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_new_sharj_ghabz.Symbol = "";
            this.btn_new_sharj_ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_new_sharj_ghabz.SymbolSize = 9F;
            this.btn_new_sharj_ghabz.TabIndex = 52;
            this.btn_new_sharj_ghabz.Text = "صدور قبض جدید شارژ مشترک";
            this.btn_new_sharj_ghabz.Visible = false;
            this.btn_new_sharj_ghabz.Click += new System.EventHandler(this.buttonX4_Click_2);
            // 
            // btn_pardakht_sharj
            // 
            this.btn_pardakht_sharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_pardakht_sharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_pardakht_sharj.Location = new System.Drawing.Point(242, 5);
            this.btn_pardakht_sharj.Name = "btn_pardakht_sharj";
            this.btn_pardakht_sharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_pardakht_sharj.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F10);
            this.btn_pardakht_sharj.Size = new System.Drawing.Size(126, 23);
            this.btn_pardakht_sharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_pardakht_sharj.Symbol = "";
            this.btn_pardakht_sharj.SymbolColor = System.Drawing.Color.Green;
            this.btn_pardakht_sharj.SymbolSize = 9F;
            this.btn_pardakht_sharj.TabIndex = 50;
            this.btn_pardakht_sharj.Text = "پرداخت شارژ F10";
            this.btn_pardakht_sharj.Tooltip = "CtrlS";
            this.btn_pardakht_sharj.Click += new System.EventHandler(this.btn_pardakht_sharj_Click);
            // 
            // btn_print_Sharj_Ghabz
            // 
            this.btn_print_Sharj_Ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_Sharj_Ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_Sharj_Ghabz.Location = new System.Drawing.Point(3, 6);
            this.btn_print_Sharj_Ghabz.Name = "btn_print_Sharj_Ghabz";
            this.btn_print_Sharj_Ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_print_Sharj_Ghabz.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.AltRight);
            this.btn_print_Sharj_Ghabz.Size = new System.Drawing.Size(95, 23);
            this.btn_print_Sharj_Ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_Sharj_Ghabz.Symbol = "";
            this.btn_print_Sharj_Ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_Sharj_Ghabz.SymbolSize = 9F;
            this.btn_print_Sharj_Ghabz.TabIndex = 37;
            this.btn_print_Sharj_Ghabz.Text = "پرینت قبض";
            this.btn_print_Sharj_Ghabz.Tooltip = "AltRight";
            this.btn_print_Sharj_Ghabz.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // lbl_sharj_gcode
            // 
            // 
            // 
            // 
            this.lbl_sharj_gcode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sharj_gcode.Font = new System.Drawing.Font("B Yekan", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_sharj_gcode.ForeColor = System.Drawing.Color.Teal;
            this.lbl_sharj_gcode.Location = new System.Drawing.Point(444, 3);
            this.lbl_sharj_gcode.Name = "lbl_sharj_gcode";
            this.lbl_sharj_gcode.Size = new System.Drawing.Size(104, 24);
            this.lbl_sharj_gcode.TabIndex = 36;
            this.lbl_sharj_gcode.Text = " ";
            // 
            // labelX20
            // 
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.Font = new System.Drawing.Font("B Yekan", 9F);
            this.labelX20.Location = new System.Drawing.Point(541, 1);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(56, 33);
            this.labelX20.TabIndex = 35;
            this.labelX20.Text = "کدقبض:";
            // 
            // btn_show_Sharj_Ghabz
            // 
            this.btn_show_Sharj_Ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show_Sharj_Ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show_Sharj_Ghabz.Location = new System.Drawing.Point(117, 6);
            this.btn_show_Sharj_Ghabz.Name = "btn_show_Sharj_Ghabz";
            this.btn_show_Sharj_Ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_show_Sharj_Ghabz.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F9);
            this.btn_show_Sharj_Ghabz.Size = new System.Drawing.Size(119, 23);
            this.btn_show_Sharj_Ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show_Sharj_Ghabz.Symbol = "";
            this.btn_show_Sharj_Ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_show_Sharj_Ghabz.SymbolSize = 9F;
            this.btn_show_Sharj_Ghabz.TabIndex = 34;
            this.btn_show_Sharj_Ghabz.Text = "مشاهده قبض-F9";
            this.btn_show_Sharj_Ghabz.Tooltip = "CtrlS";
            this.btn_show_Sharj_Ghabz.Click += new System.EventHandler(this.buttonX3_Click_2);
            // 
            // lbl_moshtarek_mablgh_kol_str
            // 
            // 
            // 
            // 
            this.lbl_moshtarek_mablgh_kol_str.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_moshtarek_mablgh_kol_str.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_moshtarek_mablgh_kol_str.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_moshtarek_mablgh_kol_str.Location = new System.Drawing.Point(10, 39);
            this.lbl_moshtarek_mablgh_kol_str.Name = "lbl_moshtarek_mablgh_kol_str";
            this.lbl_moshtarek_mablgh_kol_str.Size = new System.Drawing.Size(373, 22);
            this.lbl_moshtarek_mablgh_kol_str.TabIndex = 32;
            this.lbl_moshtarek_mablgh_kol_str.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelX21
            // 
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX21.Location = new System.Drawing.Point(349, 70);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(33, 33);
            this.labelX21.TabIndex = 40;
            this.labelX21.Text = "مانده :";
            // 
            // labelX22
            // 
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.Font = new System.Drawing.Font("B Yekan", 8F);
            this.labelX22.Location = new System.Drawing.Point(512, 34);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(85, 33);
            this.labelX22.TabIndex = 29;
            this.labelX22.Text = "مبلغ قابل پرداخت:";
            // 
            // lbl_sharj_vaziat
            // 
            // 
            // 
            // 
            this.lbl_sharj_vaziat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sharj_vaziat.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lbl_sharj_vaziat.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_sharj_vaziat.Location = new System.Drawing.Point(408, 3);
            this.lbl_sharj_vaziat.Name = "lbl_sharj_vaziat";
            this.lbl_sharj_vaziat.Size = new System.Drawing.Size(30, 23);
            this.lbl_sharj_vaziat.Symbol = "";
            this.lbl_sharj_vaziat.SymbolColor = System.Drawing.Color.Purple;
            this.lbl_sharj_vaziat.TabIndex = 22;
            this.lbl_sharj_vaziat.Text = " ";
            this.lbl_sharj_vaziat.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX15.Location = new System.Drawing.Point(513, 69);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(84, 33);
            this.labelX15.TabIndex = 38;
            this.labelX15.Text = "پرداختی این قبض:";
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "اطلاعات مشترک";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.groupPanel2);
            this.superTabControlPanel3.Controls.Add(this.groupPanel3);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(1273, 377);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.tabusergozaresh;
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX4);
            this.groupPanel2.Controls.Add(this.pictureBox5);
            this.groupPanel2.Controls.Add(this.buttonX8);
            this.groupPanel2.Controls.Add(this.cmbsharjEnd);
            this.groupPanel2.Controls.Add(this.cmbsharjStart);
            this.groupPanel2.Controls.Add(this.labelX24);
            this.groupPanel2.Controls.Add(this.labelX18);
            this.groupPanel2.Controls.Add(this.buttonX9);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Font = new System.Drawing.Font("B Yekan", 11F);
            this.groupPanel2.Location = new System.Drawing.Point(647, 8);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(578, 318);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 47;
            this.groupPanel2.Text = "بخش شارژ";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Font = new System.Drawing.Font("B Yekan", 11F);
            this.buttonX4.Location = new System.Drawing.Point(74, 150);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(434, 32);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 49;
            this.buttonX4.Tag = "مشاه";
            this.buttonX4.Text = "پیامک اطلاعات قبض شارژ مشترک";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click_3);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(520, 144);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(52, 46);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 48;
            this.pictureBox5.TabStop = false;
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Font = new System.Drawing.Font("B Yekan", 11F);
            this.buttonX8.Location = new System.Drawing.Point(74, 93);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX8.Size = new System.Drawing.Size(498, 32);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.Green;
            this.buttonX8.SymbolSize = 9F;
            this.buttonX8.TabIndex = 47;
            this.buttonX8.Tag = "مشاه";
            this.buttonX8.Text = "مشاهده کلیه قبوض شارژ  مشترک";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // cmbsharjEnd
            // 
            this.cmbsharjEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbsharjEnd.FormattingEnabled = true;
            this.cmbsharjEnd.Location = new System.Drawing.Point(288, 29);
            this.cmbsharjEnd.Name = "cmbsharjEnd";
            this.cmbsharjEnd.Size = new System.Drawing.Size(69, 31);
            this.cmbsharjEnd.TabIndex = 45;
            // 
            // cmbsharjStart
            // 
            this.cmbsharjStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbsharjStart.FormattingEnabled = true;
            this.cmbsharjStart.Location = new System.Drawing.Point(414, 29);
            this.cmbsharjStart.Name = "cmbsharjStart";
            this.cmbsharjStart.Size = new System.Drawing.Size(69, 31);
            this.cmbsharjStart.TabIndex = 44;
            // 
            // labelX24
            // 
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX24.Location = new System.Drawing.Point(363, 28);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(32, 33);
            this.labelX24.TabIndex = 43;
            this.labelX24.Text = "تا دوره:";
            // 
            // labelX18
            // 
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX18.Location = new System.Drawing.Point(498, 29);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(37, 33);
            this.labelX18.TabIndex = 42;
            this.labelX18.Text = "از دوره:";
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(49, 29);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX9.Size = new System.Drawing.Size(217, 30);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.Symbol = "";
            this.buttonX9.SymbolColor = System.Drawing.Color.Green;
            this.buttonX9.SymbolSize = 9F;
            this.buttonX9.TabIndex = 41;
            this.buttonX9.Text = "چاپ گزارش ریز حساب شارژ";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click_1);
            // 
            // groupPanel3
            // 
            this.groupPanel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.buttonX11);
            this.groupPanel3.Controls.Add(this.pictureBox6);
            this.groupPanel3.Controls.Add(this.buttonX6);
            this.groupPanel3.Controls.Add(this.cmbAbEnd);
            this.groupPanel3.Controls.Add(this.cmbabStart);
            this.groupPanel3.Controls.Add(this.labelX26);
            this.groupPanel3.Controls.Add(this.labelX27);
            this.groupPanel3.Controls.Add(this.buttonX10);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Font = new System.Drawing.Font("B Yekan", 11F);
            this.groupPanel3.Location = new System.Drawing.Point(58, 9);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(578, 318);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 46;
            this.groupPanel3.Text = "بخش آب";
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Font = new System.Drawing.Font("B Yekan", 11F);
            this.buttonX11.Location = new System.Drawing.Point(23, 149);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX11.Size = new System.Drawing.Size(434, 32);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.Symbol = "";
            this.buttonX11.SymbolColor = System.Drawing.Color.Green;
            this.buttonX11.SymbolSize = 9F;
            this.buttonX11.TabIndex = 51;
            this.buttonX11.Tag = "مشاه";
            this.buttonX11.Text = "پیامک اطلاعات قبض آب مشترک";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(469, 143);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(52, 46);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 50;
            this.pictureBox6.TabStop = false;
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Font = new System.Drawing.Font("B Yekan", 11F);
            this.buttonX6.Location = new System.Drawing.Point(22, 94);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX6.Size = new System.Drawing.Size(498, 32);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Green;
            this.buttonX6.SymbolSize = 9F;
            this.buttonX6.TabIndex = 46;
            this.buttonX6.Tag = "مشاه";
            this.buttonX6.Text = "مشاهده کلیه قبوض آب مشترک";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click_1);
            // 
            // cmbAbEnd
            // 
            this.cmbAbEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAbEnd.FormattingEnabled = true;
            this.cmbAbEnd.Location = new System.Drawing.Point(277, 29);
            this.cmbAbEnd.Name = "cmbAbEnd";
            this.cmbAbEnd.Size = new System.Drawing.Size(69, 31);
            this.cmbAbEnd.TabIndex = 45;
            // 
            // cmbabStart
            // 
            this.cmbabStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbabStart.FormattingEnabled = true;
            this.cmbabStart.Location = new System.Drawing.Point(410, 29);
            this.cmbabStart.Name = "cmbabStart";
            this.cmbabStart.Size = new System.Drawing.Size(69, 31);
            this.cmbabStart.TabIndex = 44;
            // 
            // labelX26
            // 
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX26.Location = new System.Drawing.Point(352, 25);
            this.labelX26.Name = "labelX26";
            this.labelX26.Size = new System.Drawing.Size(37, 33);
            this.labelX26.TabIndex = 43;
            this.labelX26.Text = "تا دوره:";
            // 
            // labelX27
            // 
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX27.Location = new System.Drawing.Point(485, 25);
            this.labelX27.Name = "labelX27";
            this.labelX27.Size = new System.Drawing.Size(37, 33);
            this.labelX27.TabIndex = 42;
            this.labelX27.Text = "از دوره:";
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(26, 29);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX10.Size = new System.Drawing.Size(217, 30);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 9F;
            this.buttonX10.TabIndex = 41;
            this.buttonX10.Text = "چاپ گزارش ریز حساب آب\r\n";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click_1);
            // 
            // tabusergozaresh
            // 
            this.tabusergozaresh.AttachedControl = this.superTabControlPanel3;
            this.tabusergozaresh.GlobalItem = false;
            this.tabusergozaresh.Name = "tabusergozaresh";
            this.tabusergozaresh.Text = "پنل مشترک";
            this.tabusergozaresh.Visible = false;
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.lblnoetefagh);
            this.superTabControlPanel2.Controls.Add(this.buttonX1);
            this.superTabControlPanel2.Controls.Add(this.dgvNOtofication);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(1273, 377);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // lblnoetefagh
            // 
            this.lblnoetefagh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblnoetefagh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblnoetefagh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblnoetefagh.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblnoetefagh.Location = new System.Drawing.Point(503, 307);
            this.lblnoetefagh.Name = "lblnoetefagh";
            this.lblnoetefagh.Size = new System.Drawing.Size(303, 22);
            this.lblnoetefagh.TabIndex = 4;
            this.lblnoetefagh.Text = "در طی چند دقیقه گذشته هیچ اتفاق جدیدی در سیستم رخ نداده است ";
            this.lblnoetefagh.Visible = false;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(18, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(169, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 2;
            this.buttonX1.Text = "به روز رسانی اطلاعیه ها";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // dgvNOtofication
            // 
            this.dgvNOtofication.AllowUserToAddRows = false;
            this.dgvNOtofication.AllowUserToDeleteRows = false;
            this.dgvNOtofication.AllowUserToResizeColumns = false;
            this.dgvNOtofication.AllowUserToResizeRows = false;
            this.dgvNOtofication.AutoGenerateColumns = false;
            this.dgvNOtofication.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNOtofication.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNOtofication.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvNOtofication.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.subjectDataGridViewTextBoxColumn,
            this.matnDataGridViewTextBoxColumn,
            this.chekecd,
            this.time});
            this.dgvNOtofication.DataSource = this.adminnotificationBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNOtofication.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvNOtofication.EnableHeadersVisualStyles = false;
            this.dgvNOtofication.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvNOtofication.Location = new System.Drawing.Point(18, 33);
            this.dgvNOtofication.Name = "dgvNOtofication";
            this.dgvNOtofication.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNOtofication.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvNOtofication.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNOtofication.Size = new System.Drawing.Size(1239, 268);
            this.dgvNOtofication.TabIndex = 0;
            this.dgvNOtofication.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNOtofication_CellClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.FillWeight = 60F;
            this.idDataGridViewTextBoxColumn.HeaderText = "کد";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subjectDataGridViewTextBoxColumn
            // 
            this.subjectDataGridViewTextBoxColumn.DataPropertyName = "subject";
            this.subjectDataGridViewTextBoxColumn.FillWeight = 120F;
            this.subjectDataGridViewTextBoxColumn.HeaderText = "عنوان";
            this.subjectDataGridViewTextBoxColumn.Name = "subjectDataGridViewTextBoxColumn";
            this.subjectDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // matnDataGridViewTextBoxColumn
            // 
            this.matnDataGridViewTextBoxColumn.DataPropertyName = "matn";
            this.matnDataGridViewTextBoxColumn.FillWeight = 300F;
            this.matnDataGridViewTextBoxColumn.HeaderText = "متن";
            this.matnDataGridViewTextBoxColumn.Name = "matnDataGridViewTextBoxColumn";
            this.matnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // chekecd
            // 
            this.chekecd.FillWeight = 70F;
            this.chekecd.HeaderText = "وضعیت";
            this.chekecd.Name = "chekecd";
            this.chekecd.ReadOnly = true;
            this.chekecd.Text = "بررسی شد";
            this.chekecd.UseColumnTextForButtonValue = true;
            // 
            // time
            // 
            this.time.DataPropertyName = "time";
            this.time.HeaderText = "زمان";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // adminnotificationBindingSource
            // 
            this.adminnotificationBindingSource.DataMember = "admin_notification";
            this.adminnotificationBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "آخرین اطلاعیه های سیستم";
            this.superTabItem2.Visible = false;
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btnFindByPelak);
            this.groupPanel1.Controls.Add(this.txtPelak);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.btn_find_company);
            this.groupPanel1.Controls.Add(this.txt_find_moshtarek);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(220, 2);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(836, 74);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 2;
            this.groupPanel1.Text = "جستجو ";
            // 
            // btnFindByPelak
            // 
            this.btnFindByPelak.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFindByPelak.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFindByPelak.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFindByPelak.Enabled = false;
            this.btnFindByPelak.Location = new System.Drawing.Point(3, 14);
            this.btnFindByPelak.Name = "btnFindByPelak";
            this.btnFindByPelak.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnFindByPelak.Size = new System.Drawing.Size(103, 23);
            this.btnFindByPelak.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFindByPelak.Symbol = "";
            this.btnFindByPelak.SymbolColor = System.Drawing.Color.Green;
            this.btnFindByPelak.SymbolSize = 9F;
            this.btnFindByPelak.TabIndex = 6;
            this.btnFindByPelak.Text = "جستجو";
            this.btnFindByPelak.Click += new System.EventHandler(this.btnFindByPelak_Click);
            // 
            // txtPelak
            // 
            this.txtPelak.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPelak.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPelak.Border.Class = "TextBoxBorder";
            this.txtPelak.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPelak.DisabledBackColor = System.Drawing.Color.White;
            this.txtPelak.ForeColor = System.Drawing.Color.Black;
            this.txtPelak.Location = new System.Drawing.Point(118, 13);
            this.txtPelak.Name = "txtPelak";
            this.txtPelak.PreventEnterBeep = true;
            this.txtPelak.Size = new System.Drawing.Size(95, 28);
            this.txtPelak.TabIndex = 4;
            this.txtPelak.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPelak_KeyDown);
            // 
            // labelX14
            // 
            this.labelX14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX14.Location = new System.Drawing.Point(198, 9);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(79, 33);
            this.labelX14.TabIndex = 5;
            this.labelX14.Text = "شماره پلاک:";
            // 
            // btn_find_company
            // 
            this.btn_find_company.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_company.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_find_company.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_company.Enabled = false;
            this.btn_find_company.Location = new System.Drawing.Point(310, 14);
            this.btn_find_company.Name = "btn_find_company";
            this.btn_find_company.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_company.Size = new System.Drawing.Size(103, 23);
            this.btn_find_company.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_company.Symbol = "";
            this.btn_find_company.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_company.SymbolSize = 9F;
            this.btn_find_company.TabIndex = 1;
            this.btn_find_company.Text = "جستجو";
            this.btn_find_company.Click += new System.EventHandler(this.btn_find_company_Click);
            // 
            // txt_find_moshtarek
            // 
            this.txt_find_moshtarek.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_find_moshtarek.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_moshtarek.Border.Class = "TextBoxBorder";
            this.txt_find_moshtarek.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_moshtarek.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_moshtarek.Enabled = false;
            this.txt_find_moshtarek.ForeColor = System.Drawing.Color.Black;
            this.txt_find_moshtarek.Location = new System.Drawing.Point(424, 13);
            this.txt_find_moshtarek.Name = "txt_find_moshtarek";
            this.txt_find_moshtarek.PreventEnterBeep = true;
            this.txt_find_moshtarek.Size = new System.Drawing.Size(274, 28);
            this.txt_find_moshtarek.TabIndex = 0;
            this.txt_find_moshtarek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_moshtarek_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX2.Location = new System.Drawing.Point(664, 9);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(135, 33);
            this.labelX2.TabIndex = 0;
            this.labelX2.Text = "&نام مشترک/ قرارداد :";
            // 
            // All_ghabz_sharj
            // 
            this.All_ghabz_sharj.Name = "All_ghabz_sharj";
            this.All_ghabz_sharj.Executed += new System.EventHandler(this.All_ghabz_sharj_Executed);
            // 
            // command1
            // 
            this.command1.Name = "command1";
            this.command1.Executed += new System.EventHandler(this.command1_Executed);
            // 
            // New_sharj_ghabz
            // 
            this.New_sharj_ghabz.Name = "New_sharj_ghabz";
            this.New_sharj_ghabz.Executed += new System.EventHandler(this.New_sharj_ghabz_Executed);
            // 
            // barge
            // 
            this.barge.Name = "barge";
            this.barge.Executed += new System.EventHandler(this.barge_Executed);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.ItemSpacing = 0;
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem62,
            this.buttonItem64});
            // 
            // 
            // 
            this.itemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem62
            // 
            this.buttonItem62.Enabled = false;
            this.buttonItem62.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem62.Image")));
            this.buttonItem62.Name = "buttonItem62";
            this.buttonItem62.Text = "Cu&t";
            // 
            // buttonItem64
            // 
            this.buttonItem64.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem64.Image")));
            this.buttonItem64.Name = "buttonItem64";
            this.buttonItem64.Symbol = "";
            this.buttonItem64.SymbolSize = 10F;
            this.buttonItem64.Text = "Format Painter";
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.ItemSpacing = 0;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.galleryContainer2});
            // 
            // 
            // 
            this.itemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer2
            // 
            // 
            // 
            // 
            this.galleryContainer2.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer2.EnableGalleryPopup = false;
            this.galleryContainer2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer2.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer2.MultiLine = false;
            this.galleryContainer2.Name = "galleryContainer2";
            this.galleryContainer2.PopupUsesStandardScrollbars = false;
            this.galleryContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12,
            this.buttonItem16});
            // 
            // 
            // 
            this.galleryContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem12
            // 
            this.buttonItem12.BeginGroup = true;
            this.buttonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem12.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem12.Image")));
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItemsExpandWidth = 24;
            this.buttonItem12.Text = "Opt&ions";
            // 
            // buttonItem16
            // 
            this.buttonItem16.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem16.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem16.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem16.Image")));
            this.buttonItem16.Name = "buttonItem16";
            this.buttonItem16.SubItemsExpandWidth = 24;
            this.buttonItem16.Text = "E&xit";
            // 
            // itemContainer6
            // 
            // 
            // 
            // 
            this.itemContainer6.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer6.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer6.Name = "itemContainer6";
            // 
            // 
            // 
            this.itemContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer7
            // 
            // 
            // 
            // 
            this.itemContainer7.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer7.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer7.Name = "itemContainer7";
            this.itemContainer7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6,
            this.buttonItem7});
            // 
            // 
            // 
            this.itemContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem2.Image")));
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.SubItemsExpandWidth = 24;
            this.buttonItem2.Text = "Browse for Templates...";
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.SubItemsExpandWidth = 24;
            this.buttonItem3.Text = "Save Current Template...";
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem4.Image")));
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 24;
            this.buttonItem4.Text = "&Save...";
            // 
            // buttonItem5
            // 
            this.buttonItem5.BeginGroup = true;
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem5.Image")));
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.SubItemsExpandWidth = 24;
            this.buttonItem5.Text = "S&hare...";
            // 
            // buttonItem6
            // 
            this.buttonItem6.BeginGroup = true;
            this.buttonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem6.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem6.Image")));
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.SubItemsExpandWidth = 24;
            this.buttonItem6.Text = "&Print...";
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 24;
            this.buttonItem7.Text = "&Close";
            // 
            // galleryContainer3
            // 
            // 
            // 
            // 
            this.galleryContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer3.EnableGalleryPopup = false;
            this.galleryContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer3.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer3.MultiLine = false;
            this.galleryContainer3.Name = "galleryContainer3";
            this.galleryContainer3.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer8
            // 
            // 
            // 
            // 
            this.itemContainer8.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer8.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer8.Name = "itemContainer8";
            // 
            // 
            // 
            this.itemContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer4
            // 
            // 
            // 
            // 
            this.galleryContainer4.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer4.EnableGalleryPopup = false;
            this.galleryContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer4.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer4.MultiLine = false;
            this.galleryContainer4.Name = "galleryContainer4";
            this.galleryContainer4.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer5
            // 
            // 
            // 
            // 
            this.galleryContainer5.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer5.EnableGalleryPopup = false;
            this.galleryContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer5.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer5.MultiLine = false;
            this.galleryContainer5.Name = "galleryContainer5";
            this.galleryContainer5.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer6
            // 
            // 
            // 
            // 
            this.galleryContainer6.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer6.EnableGalleryPopup = false;
            this.galleryContainer6.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer6.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer6.MultiLine = false;
            this.galleryContainer6.Name = "galleryContainer6";
            this.galleryContainer6.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer7
            // 
            // 
            // 
            // 
            this.galleryContainer7.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer7.EnableGalleryPopup = false;
            this.galleryContainer7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer7.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer7.MultiLine = false;
            this.galleryContainer7.Name = "galleryContainer7";
            this.galleryContainer7.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer8
            // 
            // 
            // 
            // 
            this.galleryContainer8.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer8.EnableGalleryPopup = false;
            this.galleryContainer8.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer8.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer8.MultiLine = false;
            this.galleryContainer8.Name = "galleryContainer8";
            this.galleryContainer8.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer9
            // 
            // 
            // 
            // 
            this.galleryContainer9.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer9.EnableGalleryPopup = false;
            this.galleryContainer9.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer9.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer9.MultiLine = false;
            this.galleryContainer9.Name = "galleryContainer9";
            this.galleryContainer9.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer10
            // 
            // 
            // 
            // 
            this.galleryContainer10.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer10.EnableGalleryPopup = false;
            this.galleryContainer10.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer10.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer10.MultiLine = false;
            this.galleryContainer10.Name = "galleryContainer10";
            this.galleryContainer10.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer11
            // 
            // 
            // 
            // 
            this.galleryContainer11.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer11.EnableGalleryPopup = false;
            this.galleryContainer11.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer11.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer11.MultiLine = false;
            this.galleryContainer11.Name = "galleryContainer11";
            this.galleryContainer11.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.VisualStudio2012Light;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.White);
            // 
            // galleryContainer12
            // 
            // 
            // 
            // 
            this.galleryContainer12.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer12.EnableGalleryPopup = false;
            this.galleryContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer12.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer12.MultiLine = false;
            this.galleryContainer12.Name = "galleryContainer12";
            this.galleryContainer12.PopupUsesStandardScrollbars = false;
            this.galleryContainer12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem17,
            this.buttonItem18});
            // 
            // 
            // 
            this.galleryContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem17
            // 
            this.buttonItem17.Name = "buttonItem17";
            this.buttonItem17.Text = "&3. Customer Email.rtf";
            // 
            // buttonItem18
            // 
            this.buttonItem18.Name = "buttonItem18";
            this.buttonItem18.Text = "&4. example.rtf";
            // 
            // galleryContainer13
            // 
            // 
            // 
            // 
            this.galleryContainer13.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer13.EnableGalleryPopup = false;
            this.galleryContainer13.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer13.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer13.MultiLine = false;
            this.galleryContainer13.Name = "galleryContainer13";
            this.galleryContainer13.PopupUsesStandardScrollbars = false;
            this.galleryContainer13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem19,
            this.buttonItem20});
            // 
            // 
            // 
            this.galleryContainer13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem19
            // 
            this.buttonItem19.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem19.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem19.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem19.Image")));
            this.buttonItem19.Name = "buttonItem19";
            this.buttonItem19.SubItemsExpandWidth = 24;
            this.buttonItem19.Text = "Opt&ions";
            // 
            // buttonItem20
            // 
            this.buttonItem20.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem20.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem20.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem20.Image")));
            this.buttonItem20.Name = "buttonItem20";
            this.buttonItem20.SubItemsExpandWidth = 24;
            this.buttonItem20.Text = "E&xit";
            // 
            // galleryContainer14
            // 
            // 
            // 
            // 
            this.galleryContainer14.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer14.EnableGalleryPopup = false;
            this.galleryContainer14.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer14.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer14.MultiLine = false;
            this.galleryContainer14.Name = "galleryContainer14";
            this.galleryContainer14.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer15
            // 
            // 
            // 
            // 
            this.galleryContainer15.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer15.EnableGalleryPopup = false;
            this.galleryContainer15.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer15.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer15.MultiLine = false;
            this.galleryContainer15.Name = "galleryContainer15";
            this.galleryContainer15.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer16
            // 
            // 
            // 
            // 
            this.galleryContainer16.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer16.EnableGalleryPopup = false;
            this.galleryContainer16.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer16.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer16.MultiLine = false;
            this.galleryContainer16.Name = "galleryContainer16";
            this.galleryContainer16.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer17
            // 
            // 
            // 
            // 
            this.galleryContainer17.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer17.EnableGalleryPopup = false;
            this.galleryContainer17.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer17.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer17.MultiLine = false;
            this.galleryContainer17.Name = "galleryContainer17";
            this.galleryContainer17.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer18
            // 
            // 
            // 
            // 
            this.galleryContainer18.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer18.EnableGalleryPopup = false;
            this.galleryContainer18.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer18.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer18.MultiLine = false;
            this.galleryContainer18.Name = "galleryContainer18";
            this.galleryContainer18.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer18.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer19
            // 
            // 
            // 
            // 
            this.galleryContainer19.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer19.EnableGalleryPopup = false;
            this.galleryContainer19.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer19.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer19.MultiLine = false;
            this.galleryContainer19.Name = "galleryContainer19";
            this.galleryContainer19.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer20
            // 
            // 
            // 
            // 
            this.galleryContainer20.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer20.EnableGalleryPopup = false;
            this.galleryContainer20.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer20.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer20.MultiLine = false;
            this.galleryContainer20.Name = "galleryContainer20";
            this.galleryContainer20.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer21
            // 
            // 
            // 
            // 
            this.galleryContainer21.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer21.EnableGalleryPopup = false;
            this.galleryContainer21.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer21.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer21.MultiLine = false;
            this.galleryContainer21.Name = "galleryContainer21";
            this.galleryContainer21.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer21.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer22
            // 
            // 
            // 
            // 
            this.galleryContainer22.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer22.EnableGalleryPopup = false;
            this.galleryContainer22.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer22.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer22.MultiLine = false;
            this.galleryContainer22.Name = "galleryContainer22";
            this.galleryContainer22.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer22.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer23
            // 
            // 
            // 
            // 
            this.galleryContainer23.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer23.EnableGalleryPopup = false;
            this.galleryContainer23.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer23.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer23.MultiLine = false;
            this.galleryContainer23.Name = "galleryContainer23";
            this.galleryContainer23.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer23.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer24
            // 
            // 
            // 
            // 
            this.galleryContainer24.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer24.EnableGalleryPopup = false;
            this.galleryContainer24.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer24.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer24.MultiLine = false;
            this.galleryContainer24.Name = "galleryContainer24";
            this.galleryContainer24.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer24.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer25
            // 
            // 
            // 
            // 
            this.galleryContainer25.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer25.EnableGalleryPopup = false;
            this.galleryContainer25.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer25.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer25.MultiLine = false;
            this.galleryContainer25.Name = "galleryContainer25";
            this.galleryContainer25.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer25.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer26
            // 
            // 
            // 
            // 
            this.galleryContainer26.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer26.EnableGalleryPopup = false;
            this.galleryContainer26.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer26.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer26.MultiLine = false;
            this.galleryContainer26.Name = "galleryContainer26";
            this.galleryContainer26.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer26.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer27
            // 
            // 
            // 
            // 
            this.galleryContainer27.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer27.EnableGalleryPopup = false;
            this.galleryContainer27.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer27.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer27.MultiLine = false;
            this.galleryContainer27.Name = "galleryContainer27";
            this.galleryContainer27.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer27.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // metroTileItem5
            // 
            this.metroTileItem5.ImageTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileItem5.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.metroTileItem5.Name = "metroTileItem5";
            this.metroTileItem5.SymbolColor = System.Drawing.Color.Empty;
            this.metroTileItem5.TileColor = DevComponents.DotNetBar.Metro.eMetroTileColor.DarkBlue;
            this.metroTileItem5.TileSize = new System.Drawing.Size(100, 75);
            // 
            // 
            // 
            this.metroTileItem5.TileStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroTileItem5.TitleText = "کلیه قبوض";
            this.metroTileItem5.TitleTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileItem5.TitleTextFont = new System.Drawing.Font("B Yekan", 8.25F);
            // 
            // galleryContainer28
            // 
            // 
            // 
            // 
            this.galleryContainer28.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer28.EnableGalleryPopup = false;
            this.galleryContainer28.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer28.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer28.MultiLine = false;
            this.galleryContainer28.Name = "galleryContainer28";
            this.galleryContainer28.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer28.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer29
            // 
            // 
            // 
            // 
            this.galleryContainer29.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer29.EnableGalleryPopup = false;
            this.galleryContainer29.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer29.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer29.MultiLine = false;
            this.galleryContainer29.Name = "galleryContainer29";
            this.galleryContainer29.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer29.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer30
            // 
            // 
            // 
            // 
            this.galleryContainer30.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer30.EnableGalleryPopup = false;
            this.galleryContainer30.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer30.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer30.MultiLine = false;
            this.galleryContainer30.Name = "galleryContainer30";
            this.galleryContainer30.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer30.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer31
            // 
            // 
            // 
            // 
            this.galleryContainer31.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer31.EnableGalleryPopup = false;
            this.galleryContainer31.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer31.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer31.MultiLine = false;
            this.galleryContainer31.Name = "galleryContainer31";
            this.galleryContainer31.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer31.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer32
            // 
            // 
            // 
            // 
            this.galleryContainer32.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer32.EnableGalleryPopup = false;
            this.galleryContainer32.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer32.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer32.MultiLine = false;
            this.galleryContainer32.Name = "galleryContainer32";
            this.galleryContainer32.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer32.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer33
            // 
            // 
            // 
            // 
            this.galleryContainer33.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer33.EnableGalleryPopup = false;
            this.galleryContainer33.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer33.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer33.MultiLine = false;
            this.galleryContainer33.Name = "galleryContainer33";
            this.galleryContainer33.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer33.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer34
            // 
            // 
            // 
            // 
            this.galleryContainer34.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer34.EnableGalleryPopup = false;
            this.galleryContainer34.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer34.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer34.MultiLine = false;
            this.galleryContainer34.Name = "galleryContainer34";
            this.galleryContainer34.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer34.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer35
            // 
            // 
            // 
            // 
            this.galleryContainer35.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer35.EnableGalleryPopup = false;
            this.galleryContainer35.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer35.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer35.MultiLine = false;
            this.galleryContainer35.Name = "galleryContainer35";
            this.galleryContainer35.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer35.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer36
            // 
            // 
            // 
            // 
            this.galleryContainer36.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer36.EnableGalleryPopup = false;
            this.galleryContainer36.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer36.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer36.MultiLine = false;
            this.galleryContainer36.Name = "galleryContainer36";
            this.galleryContainer36.PopupUsesStandardScrollbars = false;
            this.galleryContainer36.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem8});
            // 
            // 
            // 
            this.galleryContainer36.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem8
            // 
            this.labelItem8.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem8.CanCustomize = false;
            this.labelItem8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem8.Name = "labelItem8";
            this.labelItem8.PaddingBottom = 2;
            this.labelItem8.PaddingTop = 2;
            this.labelItem8.Stretch = true;
            this.labelItem8.Text = "Recent Documents";
            // 
            // ErsalPayam
            // 
            this.ErsalPayam.Name = "ErsalPayam";
            this.ErsalPayam.Executed += new System.EventHandler(this.ErsalPayam_Executed);
            // 
            // galleryContainer37
            // 
            // 
            // 
            // 
            this.galleryContainer37.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer37.EnableGalleryPopup = false;
            this.galleryContainer37.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer37.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer37.MultiLine = false;
            this.galleryContainer37.Name = "galleryContainer37";
            this.galleryContainer37.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer37.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer38
            // 
            // 
            // 
            // 
            this.galleryContainer38.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer38.EnableGalleryPopup = false;
            this.galleryContainer38.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer38.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer38.MultiLine = false;
            this.galleryContainer38.Name = "galleryContainer38";
            this.galleryContainer38.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer38.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer39
            // 
            // 
            // 
            // 
            this.galleryContainer39.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer39.EnableGalleryPopup = false;
            this.galleryContainer39.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer39.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer39.MultiLine = false;
            this.galleryContainer39.Name = "galleryContainer39";
            this.galleryContainer39.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer39.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer40
            // 
            // 
            // 
            // 
            this.galleryContainer40.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer40.EnableGalleryPopup = false;
            this.galleryContainer40.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer40.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer40.MultiLine = false;
            this.galleryContainer40.Name = "galleryContainer40";
            this.galleryContainer40.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer40.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer41
            // 
            // 
            // 
            // 
            this.galleryContainer41.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer41.EnableGalleryPopup = false;
            this.galleryContainer41.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer41.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer41.MultiLine = false;
            this.galleryContainer41.Name = "galleryContainer41";
            this.galleryContainer41.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer41.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer42
            // 
            // 
            // 
            // 
            this.galleryContainer42.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer42.EnableGalleryPopup = false;
            this.galleryContainer42.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer42.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer42.MultiLine = false;
            this.galleryContainer42.Name = "galleryContainer42";
            this.galleryContainer42.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer42.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer43
            // 
            // 
            // 
            // 
            this.galleryContainer43.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer43.EnableGalleryPopup = false;
            this.galleryContainer43.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer43.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer43.MultiLine = false;
            this.galleryContainer43.Name = "galleryContainer43";
            this.galleryContainer43.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer43.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer52
            // 
            // 
            // 
            // 
            this.itemContainer52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer52.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer52.Name = "itemContainer52";
            this.itemContainer52.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem73});
            // 
            // 
            // 
            this.itemContainer52.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem73
            // 
            this.buttonItem73.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem73.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem73.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem73.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem73.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem73.Name = "buttonItem73";
            this.buttonItem73.Text = "افزودن کلید امنیتی";
            // 
            // itemContainer53
            // 
            // 
            // 
            // 
            this.itemContainer53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer53.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer53.Name = "itemContainer53";
            this.itemContainer53.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem83});
            // 
            // 
            // 
            this.itemContainer53.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem83
            // 
            this.buttonItem83.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem83.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem83.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem83.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem83.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem83.Name = "buttonItem83";
            this.buttonItem83.Text = "افزودن کلید امنیتی";
            // 
            // itemContainer54
            // 
            // 
            // 
            // 
            this.itemContainer54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer54.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer54.Name = "itemContainer54";
            this.itemContainer54.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem84});
            // 
            // 
            // 
            this.itemContainer54.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem84
            // 
            this.buttonItem84.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem84.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem84.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem84.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem84.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem84.Name = "buttonItem84";
            this.buttonItem84.Text = "افزودن کلید امنیتی";
            // 
            // galleryContainer44
            // 
            // 
            // 
            // 
            this.galleryContainer44.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer44.EnableGalleryPopup = false;
            this.galleryContainer44.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer44.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer44.MultiLine = false;
            this.galleryContainer44.Name = "galleryContainer44";
            this.galleryContainer44.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer44.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer45
            // 
            // 
            // 
            // 
            this.galleryContainer45.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer45.EnableGalleryPopup = false;
            this.galleryContainer45.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer45.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer45.MultiLine = false;
            this.galleryContainer45.Name = "galleryContainer45";
            this.galleryContainer45.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer45.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer46
            // 
            // 
            // 
            // 
            this.galleryContainer46.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer46.EnableGalleryPopup = false;
            this.galleryContainer46.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer46.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer46.MultiLine = false;
            this.galleryContainer46.Name = "galleryContainer46";
            this.galleryContainer46.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer46.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer21
            // 
            // 
            // 
            // 
            this.itemContainer21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer21.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer21.Name = "itemContainer21";
            this.itemContainer21.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem25});
            // 
            // 
            // 
            this.itemContainer21.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem25
            // 
            this.buttonItem25.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem25.Image = global::CWMS.Properties.Resources.Windows_Card_Space;
            this.buttonItem25.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem25.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem25.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem25.Name = "buttonItem25";
            this.buttonItem25.Text = "صدور برگه ";
            // 
            // itemContainer22
            // 
            // 
            // 
            // 
            this.itemContainer22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer22.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer22.Name = "itemContainer22";
            this.itemContainer22.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem26});
            // 
            // 
            // 
            this.itemContainer22.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem26
            // 
            this.buttonItem26.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem26.Image = global::CWMS.Properties.Resources.Windows_Card_Space;
            this.buttonItem26.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem26.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem26.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem26.Name = "buttonItem26";
            this.buttonItem26.Text = "صدور برگه ";
            // 
            // buttonItem1
            // 
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "buttonItem1";
            // 
            // galleryContainer47
            // 
            // 
            // 
            // 
            this.galleryContainer47.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer47.EnableGalleryPopup = false;
            this.galleryContainer47.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer47.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer47.MultiLine = false;
            this.galleryContainer47.Name = "galleryContainer47";
            this.galleryContainer47.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer47.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer48
            // 
            // 
            // 
            // 
            this.galleryContainer48.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer48.EnableGalleryPopup = false;
            this.galleryContainer48.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer48.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer48.MultiLine = false;
            this.galleryContainer48.Name = "galleryContainer48";
            this.galleryContainer48.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer48.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer1
            // 
            // 
            // 
            // 
            this.galleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer1.EnableGalleryPopup = false;
            this.galleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer1.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer1.MultiLine = false;
            this.galleryContainer1.Name = "galleryContainer1";
            this.galleryContainer1.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer49
            // 
            // 
            // 
            // 
            this.galleryContainer49.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer49.EnableGalleryPopup = false;
            this.galleryContainer49.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer49.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer49.MultiLine = false;
            this.galleryContainer49.Name = "galleryContainer49";
            this.galleryContainer49.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer49.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer50
            // 
            // 
            // 
            // 
            this.galleryContainer50.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer50.EnableGalleryPopup = false;
            this.galleryContainer50.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer50.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer50.MultiLine = false;
            this.galleryContainer50.Name = "galleryContainer50";
            this.galleryContainer50.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer50.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // galleryContainer51
            // 
            // 
            // 
            // 
            this.galleryContainer51.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer51.EnableGalleryPopup = false;
            this.galleryContainer51.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer51.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer51.MultiLine = false;
            this.galleryContainer51.Name = "galleryContainer51";
            this.galleryContainer51.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer51.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem1
            // 
            this.labelItem1.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem1.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem1.CanCustomize = false;
            this.labelItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem1.Name = "labelItem1";
            // 
            // galleryContainer52
            // 
            // 
            // 
            // 
            this.galleryContainer52.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer52.EnableGalleryPopup = false;
            this.galleryContainer52.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer52.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer52.MultiLine = false;
            this.galleryContainer52.Name = "galleryContainer52";
            this.galleryContainer52.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer52.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem2
            // 
            this.labelItem2.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem2.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem2.CanCustomize = false;
            this.labelItem2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem2.Name = "labelItem2";
            // 
            // galleryContainer53
            // 
            // 
            // 
            // 
            this.galleryContainer53.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer53.EnableGalleryPopup = false;
            this.galleryContainer53.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer53.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer53.MultiLine = false;
            this.galleryContainer53.Name = "galleryContainer53";
            this.galleryContainer53.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer53.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem3
            // 
            this.labelItem3.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem3.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem3.CanCustomize = false;
            this.labelItem3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem3.Name = "labelItem3";
            // 
            // galleryContainer54
            // 
            // 
            // 
            // 
            this.galleryContainer54.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer54.EnableGalleryPopup = false;
            this.galleryContainer54.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer54.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer54.MultiLine = false;
            this.galleryContainer54.Name = "galleryContainer54";
            this.galleryContainer54.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer54.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem4
            // 
            this.labelItem4.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem4.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem4.CanCustomize = false;
            this.labelItem4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem4.Name = "labelItem4";
            // 
            // labelX29
            // 
            this.labelX29.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX29.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX29.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX29.Location = new System.Drawing.Point(699, 16);
            this.labelX29.Name = "labelX29";
            this.labelX29.Size = new System.Drawing.Size(69, 24);
            this.labelX29.TabIndex = 4;
            this.labelX29.Text = "نام کاربری:";
            // 
            // labelX30
            // 
            this.labelX30.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.labelX30.Location = new System.Drawing.Point(699, 46);
            this.labelX30.Name = "labelX30";
            this.labelX30.Size = new System.Drawing.Size(69, 24);
            this.labelX30.TabIndex = 5;
            this.labelX30.Text = "رمز عبور:";
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtUsername.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtUsername.Border.Class = "TextBoxBorder";
            this.txtUsername.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtUsername.DisabledBackColor = System.Drawing.Color.White;
            this.txtUsername.ForeColor = System.Drawing.Color.Black;
            this.txtUsername.Location = new System.Drawing.Point(576, 16);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.PreventEnterBeep = true;
            this.txtUsername.Size = new System.Drawing.Size(137, 28);
            this.txtUsername.TabIndex = 0;
            this.txtUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsername_KeyDown);
            // 
            // txtpassword
            // 
            this.txtpassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtpassword.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtpassword.Border.Class = "TextBoxBorder";
            this.txtpassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtpassword.DisabledBackColor = System.Drawing.Color.White;
            this.txtpassword.ForeColor = System.Drawing.Color.Black;
            this.txtpassword.Location = new System.Drawing.Point(576, 48);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PreventEnterBeep = true;
            this.txtpassword.Size = new System.Drawing.Size(137, 28);
            this.txtpassword.TabIndex = 7;
            this.txtpassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyDown);
            // 
            // labelX31
            // 
            this.labelX31.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX31.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.Location = new System.Drawing.Point(781, 14);
            this.labelX31.Name = "labelX31";
            this.labelX31.Size = new System.Drawing.Size(30, 23);
            this.labelX31.Symbol = "";
            this.labelX31.SymbolColor = System.Drawing.Color.SaddleBrown;
            this.labelX31.SymbolSize = 13F;
            this.labelX31.TabIndex = 8;
            // 
            // labelX32
            // 
            this.labelX32.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX32.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.Location = new System.Drawing.Point(781, 47);
            this.labelX32.Name = "labelX32";
            this.labelX32.Size = new System.Drawing.Size(30, 23);
            this.labelX32.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX32.Symbol = "";
            this.labelX32.SymbolColor = System.Drawing.Color.SaddleBrown;
            this.labelX32.SymbolSize = 13F;
            this.labelX32.TabIndex = 9;
            // 
            // btnLogin
            // 
            this.btnLogin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnLogin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnLogin.Location = new System.Drawing.Point(524, 78);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnLogin.Size = new System.Drawing.Size(116, 23);
            this.btnLogin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnLogin.Symbol = "";
            this.btnLogin.SymbolColor = System.Drawing.Color.Green;
            this.btnLogin.SymbolSize = 9F;
            this.btnLogin.TabIndex = 10;
            this.btnLogin.Text = "ورود به سیستم";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(182, 80);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(205, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Red;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 11;
            this.buttonX2.Text = "خروج از سیستم";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // grpLogin
            // 
            this.grpLogin.BackColor = System.Drawing.Color.Transparent;
            this.grpLogin.CanvasColor = System.Drawing.SystemColors.Control;
            this.grpLogin.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpLogin.Controls.Add(this.pictureBox4);
            this.grpLogin.Controls.Add(this.pictureBox3);
            this.grpLogin.Controls.Add(this.buttonX2);
            this.grpLogin.Controls.Add(this.btnLogin);
            this.grpLogin.Controls.Add(this.labelX32);
            this.grpLogin.Controls.Add(this.labelX31);
            this.grpLogin.Controls.Add(this.txtpassword);
            this.grpLogin.Controls.Add(this.txtUsername);
            this.grpLogin.Controls.Add(this.labelX30);
            this.grpLogin.Controls.Add(this.labelX29);
            this.grpLogin.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpLogin.Location = new System.Drawing.Point(5, 551);
            this.grpLogin.Name = "grpLogin";
            this.grpLogin.Size = new System.Drawing.Size(1360, 141);
            // 
            // 
            // 
            this.grpLogin.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpLogin.Style.BackColorGradientAngle = 90;
            this.grpLogin.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpLogin.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpLogin.Style.BorderBottomWidth = 1;
            this.grpLogin.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpLogin.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpLogin.Style.BorderLeftWidth = 1;
            this.grpLogin.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpLogin.Style.BorderRightWidth = 1;
            this.grpLogin.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpLogin.Style.BorderTopWidth = 1;
            this.grpLogin.Style.CornerDiameter = 4;
            this.grpLogin.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpLogin.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpLogin.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpLogin.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpLogin.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpLogin.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpLogin.TabIndex = 1;
            this.grpLogin.TabStop = true;
            this.grpLogin.Text = "ورود به سامانه مدیریت نگاه";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(1228, 16);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 90);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(18, 16);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 90);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // galleryContainer55
            // 
            // 
            // 
            // 
            this.galleryContainer55.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer55.EnableGalleryPopup = false;
            this.galleryContainer55.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer55.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer55.MultiLine = false;
            this.galleryContainer55.Name = "galleryContainer55";
            this.galleryContainer55.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer55.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem5
            // 
            this.labelItem5.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem5.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem5.CanCustomize = false;
            this.labelItem5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem5.Name = "labelItem5";
            // 
            // admin_notificationTableAdapter
            // 
            this.admin_notificationTableAdapter.ClearBeforeFill = true;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AutoExpand = false;
            this.ribbonControl1.BackColor = System.Drawing.Color.SkyBlue;
            this.ribbonControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ribbonControl1.BackgroundImage")));
            this.ribbonControl1.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.TopLeft;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel7);
            this.ribbonControl1.Controls.Add(this.ribbonPanel5);
            this.ribbonControl1.Controls.Add(this.ribbonPanel8);
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Controls.Add(this.ribbonPanel6);
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.Enabled = false;
            this.ribbonControl1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.ribbonControl1.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonFile,
            this.RbMoshtarekin,
            this.RbSharj,
            this.RbAb,
            this.RbBargehKhorooj,
            this.RbInternet,
            this.RbShahrak,
            this.RbGozareshModiriat,
            this.Rb_hesabdari});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.MdiSystemItemVisible = false;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.RibbonStripFont = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ribbonControl1.Size = new System.Drawing.Size(1360, 157);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 15;
            this.ribbonControl1.TabGroups.AddRange(new DevComponents.DotNetBar.RibbonTabItemGroup[] {
            this.ribbonTabItemGroup1});
            this.ribbonControl1.TabIndex = 10;
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.ribbonBar7);
            this.ribbonPanel1.Controls.Add(this.ribbonBar4);
            this.ribbonPanel1.Controls.Add(this.ribbonBar9);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel1.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 12;
            this.ribbonPanel1.Visible = false;
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.DragDropSupport = true;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem39,
            this.itemContainer44,
            this.itemContainer49,
            this.itemContainer50});
            this.ribbonBar7.Location = new System.Drawing.Point(537, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(431, 98);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar7.TabIndex = 25;
            this.ribbonBar7.Text = "صدور فاکتور";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem39
            // 
            this.buttonItem39.FixedSize = new System.Drawing.Size(120, 70);
            this.buttonItem39.Image = global::CWMS.Properties.Resources.ExtensionsManager_summer;
            this.buttonItem39.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.buttonItem39.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem39.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem39.Name = "buttonItem39";
            this.buttonItem39.Text = "صدور فاکتور آب و شارژ";
            this.buttonItem39.Click += new System.EventHandler(this.buttonItem39_Click_2);
            // 
            // itemContainer44
            // 
            // 
            // 
            // 
            this.itemContainer44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer44.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer44.Name = "itemContainer44";
            this.itemContainer44.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.fd});
            // 
            // 
            // 
            this.itemContainer44.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // fd
            // 
            this.fd.FixedSize = new System.Drawing.Size(100, 70);
            this.fd.Image = global::CWMS.Properties.Resources.Network_Drive;
            this.fd.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.fd.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.fd.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.fd.Name = "fd";
            this.fd.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.fd.Text = "گزارشات";
            this.fd.Click += new System.EventHandler(this.fd_Click_1);
            // 
            // itemContainer49
            // 
            // 
            // 
            // 
            this.itemContainer49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer49.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer49.Name = "itemContainer49";
            this.itemContainer49.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem41});
            // 
            // 
            // 
            this.itemContainer49.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem41
            // 
            this.buttonItem41.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem41.Image = global::CWMS.Properties.Resources.Internet_Options;
            this.buttonItem41.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem41.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem41.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem41.Name = "buttonItem41";
            this.buttonItem41.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.buttonItem41.Text = "تاریخچه";
            this.buttonItem41.Click += new System.EventHandler(this.buttonItem41_Click_1);
            // 
            // itemContainer50
            // 
            // 
            // 
            // 
            this.itemContainer50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer50.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer50.Name = "itemContainer50";
            this.itemContainer50.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem45});
            // 
            // 
            // 
            this.itemContainer50.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem45
            // 
            this.buttonItem45.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem45.Image = global::CWMS.Properties.Resources.Network_Drive;
            this.buttonItem45.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem45.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem45.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem45.Name = "buttonItem45";
            this.buttonItem45.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.buttonItem45.Text = "گزارشات آماری";
            this.buttonItem45.Click += new System.EventHandler(this.buttonItem45_Click_1);
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.DragDropSupport = true;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer34,
            this.itemContainer40,
            this.itemContainer41});
            this.ribbonBar4.Location = new System.Drawing.Point(224, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(313, 98);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar4.TabIndex = 22;
            this.ribbonBar4.Text = "گرارشات حسابداری";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer34
            // 
            // 
            // 
            // 
            this.itemContainer34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer34.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer34.Name = "itemContainer34";
            this.itemContainer34.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem32});
            // 
            // 
            // 
            this.itemContainer34.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem32
            // 
            this.buttonItem32.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem32.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem32.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem32.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem32.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem32.Name = "buttonItem32";
            this.buttonItem32.Text = "شارژ";
            this.buttonItem32.Click += new System.EventHandler(this.buttonItem32_Click_3);
            // 
            // itemContainer40
            // 
            // 
            // 
            // 
            this.itemContainer40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer40.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer40.Name = "itemContainer40";
            this.itemContainer40.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem35});
            // 
            // 
            // 
            this.itemContainer40.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem35
            // 
            this.buttonItem35.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem35.Image = global::CWMS.Properties.Resources.Update;
            this.buttonItem35.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem35.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem35.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem35.Name = "buttonItem35";
            this.buttonItem35.Text = "آب";
            this.buttonItem35.Click += new System.EventHandler(this.buttonItem35_Click_1);
            // 
            // itemContainer41
            // 
            // 
            // 
            // 
            this.itemContainer41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer41.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer41.Name = "itemContainer41";
            this.itemContainer41.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem36});
            // 
            // 
            // 
            this.itemContainer41.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem36
            // 
            this.buttonItem36.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem36.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.buttonItem36.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem36.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem36.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem36.Name = "buttonItem36";
            this.buttonItem36.Text = "مشترک";
            this.buttonItem36.Click += new System.EventHandler(this.buttonItem36_Click_3);
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.DragDropSupport = true;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer56});
            this.ribbonBar9.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(221, 98);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar9.TabIndex = 14;
            this.ribbonBar9.Text = "مالیات بر ارزش افزوده";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer56
            // 
            // 
            // 
            // 
            this.itemContainer56.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer56.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer56.Name = "itemContainer56";
            this.itemContainer56.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem44});
            // 
            // 
            // 
            this.itemContainer56.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem44
            // 
            this.buttonItem44.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem44.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.buttonItem44.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem44.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem44.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem44.Name = "buttonItem44";
            this.buttonItem44.Text = "پرداختی ها";
            this.buttonItem44.Click += new System.EventHandler(this.buttonItem44_Click_1);
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel8.Controls.Add(this.ribbonBar5);
            this.ribbonPanel8.Controls.Add(this.ribbonBar3);
            this.ribbonPanel8.Controls.Add(this.gozarehat_check);
            this.ribbonPanel8.Controls.Add(this.gozarehat_tarefe_zarayeb);
            this.ribbonPanel8.Controls.Add(this.gozarehat_ab_sharj);
            this.ribbonPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel8.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel8.Name = "ribbonPanel8";
            this.ribbonPanel8.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel8.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel8.TabIndex = 11;
            this.ribbonPanel8.Visible = false;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.DragDropSupport = true;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer43});
            this.ribbonBar5.Location = new System.Drawing.Point(639, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(110, 98);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar5.TabIndex = 22;
            this.ribbonBar5.Text = "درامدهای مالیاتی";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer43
            // 
            // 
            // 
            // 
            this.itemContainer43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer43.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer43.Name = "itemContainer43";
            this.itemContainer43.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem37});
            // 
            // 
            // 
            this.itemContainer43.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem37
            // 
            this.buttonItem37.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem37.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem37.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem37.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem37.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem37.Name = "buttonItem37";
            this.buttonItem37.Text = "کلیه دوره ها";
            this.buttonItem37.Click += new System.EventHandler(this.buttonItem37_Click_1);
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.DragDropSupport = true;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer30,
            this.itemContainer33});
            this.ribbonBar3.Location = new System.Drawing.Point(426, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(213, 98);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar3.TabIndex = 20;
            this.ribbonBar3.Text = "گزارشات تفصیلی ";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer30
            // 
            // 
            // 
            // 
            this.itemContainer30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer30.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer30.Name = "itemContainer30";
            this.itemContainer30.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem33});
            // 
            // 
            // 
            this.itemContainer30.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem33
            // 
            this.buttonItem33.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem33.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem33.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem33.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem33.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem33.Name = "buttonItem33";
            this.buttonItem33.Text = "شارژ";
            this.buttonItem33.Click += new System.EventHandler(this.buttonItem33_Click_2);
            // 
            // itemContainer33
            // 
            // 
            // 
            // 
            this.itemContainer33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer33.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer33.Name = "itemContainer33";
            this.itemContainer33.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem34});
            // 
            // 
            // 
            this.itemContainer33.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem34
            // 
            this.buttonItem34.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem34.Image = global::CWMS.Properties.Resources.Update;
            this.buttonItem34.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem34.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem34.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem34.Name = "buttonItem34";
            this.buttonItem34.Text = "آب";
            this.buttonItem34.Click += new System.EventHandler(this.buttonItem34_Click);
            // 
            // gozarehat_check
            // 
            this.gozarehat_check.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.gozarehat_check.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_check.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gozarehat_check.ContainerControlProcessDialogKey = true;
            this.gozarehat_check.Dock = System.Windows.Forms.DockStyle.Left;
            this.gozarehat_check.DragDropSupport = true;
            this.gozarehat_check.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer47});
            this.gozarehat_check.Location = new System.Drawing.Point(325, 0);
            this.gozarehat_check.Name = "gozarehat_check";
            this.gozarehat_check.Size = new System.Drawing.Size(101, 98);
            this.gozarehat_check.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.gozarehat_check.TabIndex = 19;
            this.gozarehat_check.Text = "مدیریت چک";
            // 
            // 
            // 
            this.gozarehat_check.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_check.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer47
            // 
            // 
            // 
            // 
            this.itemContainer47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer47.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer47.Name = "itemContainer47";
            this.itemContainer47.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.gozarehat_check1});
            // 
            // 
            // 
            this.itemContainer47.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // gozarehat_check1
            // 
            this.gozarehat_check1.FixedSize = new System.Drawing.Size(100, 70);
            this.gozarehat_check1.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.gozarehat_check1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.gozarehat_check1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.gozarehat_check1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.gozarehat_check1.Name = "gozarehat_check1";
            this.gozarehat_check1.Text = " ";
            this.gozarehat_check1.Click += new System.EventHandler(this.gozarehat_check1_Click);
            // 
            // gozarehat_tarefe_zarayeb
            // 
            this.gozarehat_tarefe_zarayeb.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.gozarehat_tarefe_zarayeb.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_tarefe_zarayeb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gozarehat_tarefe_zarayeb.ContainerControlProcessDialogKey = true;
            this.gozarehat_tarefe_zarayeb.Dock = System.Windows.Forms.DockStyle.Left;
            this.gozarehat_tarefe_zarayeb.DragDropSupport = true;
            this.gozarehat_tarefe_zarayeb.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer45});
            this.gozarehat_tarefe_zarayeb.Location = new System.Drawing.Point(224, 0);
            this.gozarehat_tarefe_zarayeb.Name = "gozarehat_tarefe_zarayeb";
            this.gozarehat_tarefe_zarayeb.Size = new System.Drawing.Size(101, 98);
            this.gozarehat_tarefe_zarayeb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.gozarehat_tarefe_zarayeb.TabIndex = 17;
            this.gozarehat_tarefe_zarayeb.Text = "تعرفه ها / ضرایب";
            // 
            // 
            // 
            this.gozarehat_tarefe_zarayeb.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_tarefe_zarayeb.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer45
            // 
            // 
            // 
            // 
            this.itemContainer45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer45.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer45.Name = "itemContainer45";
            this.itemContainer45.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.gozarehat_tarefe_zarayeb1});
            // 
            // 
            // 
            this.itemContainer45.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // gozarehat_tarefe_zarayeb1
            // 
            this.gozarehat_tarefe_zarayeb1.FixedSize = new System.Drawing.Size(100, 70);
            this.gozarehat_tarefe_zarayeb1.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.gozarehat_tarefe_zarayeb1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.gozarehat_tarefe_zarayeb1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.gozarehat_tarefe_zarayeb1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.gozarehat_tarefe_zarayeb1.Name = "gozarehat_tarefe_zarayeb1";
            this.gozarehat_tarefe_zarayeb1.Click += new System.EventHandler(this.buttonItem59_Click);
            // 
            // gozarehat_ab_sharj
            // 
            this.gozarehat_ab_sharj.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.gozarehat_ab_sharj.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_ab_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gozarehat_ab_sharj.ContainerControlProcessDialogKey = true;
            this.gozarehat_ab_sharj.Dock = System.Windows.Forms.DockStyle.Left;
            this.gozarehat_ab_sharj.DragDropSupport = true;
            this.gozarehat_ab_sharj.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.gozarehat_ab_sharj1,
            this.gozarehat_ab_sharj_Pardakhti});
            this.gozarehat_ab_sharj.Location = new System.Drawing.Point(3, 0);
            this.gozarehat_ab_sharj.Name = "gozarehat_ab_sharj";
            this.gozarehat_ab_sharj.Size = new System.Drawing.Size(221, 98);
            this.gozarehat_ab_sharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.gozarehat_ab_sharj.TabIndex = 13;
            this.gozarehat_ab_sharj.Text = "گزارشات قبوض";
            // 
            // 
            // 
            this.gozarehat_ab_sharj.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gozarehat_ab_sharj.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // gozarehat_ab_sharj1
            // 
            // 
            // 
            // 
            this.gozarehat_ab_sharj1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gozarehat_ab_sharj1.FixedSize = new System.Drawing.Size(100, 0);
            this.gozarehat_ab_sharj1.Name = "gozarehat_ab_sharj1";
            this.gozarehat_ab_sharj1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.سیس});
            // 
            // 
            // 
            this.gozarehat_ab_sharj1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // سیس
            // 
            this.سیس.FixedSize = new System.Drawing.Size(100, 70);
            this.سیس.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.سیس.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.سیس.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.سیس.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.سیس.Name = "سیس";
            this.سیس.Text = "قبوض ";
            this.سیس.Click += new System.EventHandler(this.buttonItem57_Click);
            // 
            // gozarehat_ab_sharj_Pardakhti
            // 
            // 
            // 
            // 
            this.gozarehat_ab_sharj_Pardakhti.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gozarehat_ab_sharj_Pardakhti.FixedSize = new System.Drawing.Size(100, 0);
            this.gozarehat_ab_sharj_Pardakhti.Name = "gozarehat_ab_sharj_Pardakhti";
            this.gozarehat_ab_sharj_Pardakhti.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem24});
            // 
            // 
            // 
            this.gozarehat_ab_sharj_Pardakhti.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem24
            // 
            this.buttonItem24.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem24.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.buttonItem24.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem24.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem24.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem24.Name = "buttonItem24";
            this.buttonItem24.Text = "پرداختی ها";
            this.buttonItem24.Click += new System.EventHandler(this.buttonItem24_Click_1);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel5.Controls.Add(this.ribbonBar12);
            this.ribbonPanel5.Controls.Add(this.InternetMobikePanel);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel5.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 6;
            this.ribbonPanel5.Visible = false;
            // 
            // ribbonBar12
            // 
            this.ribbonBar12.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.ContainerControlProcessDialogKey = true;
            this.ribbonBar12.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar12.DragDropSupport = true;
            this.ribbonBar12.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer42});
            this.ribbonBar12.Location = new System.Drawing.Point(330, 0);
            this.ribbonBar12.Name = "ribbonBar12";
            this.ribbonBar12.Size = new System.Drawing.Size(168, 98);
            this.ribbonBar12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar12.TabIndex = 16;
            this.ribbonBar12.Text = "بروزرسانی اطلاعات مشترکین و قبوض";
            // 
            // 
            // 
            this.ribbonBar12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer42
            // 
            // 
            // 
            // 
            this.itemContainer42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer42.FixedSize = new System.Drawing.Size(160, 0);
            this.itemContainer42.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer42.Name = "itemContainer42";
            this.itemContainer42.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem43});
            // 
            // 
            // 
            this.itemContainer42.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem43
            // 
            this.buttonItem43.FixedSize = new System.Drawing.Size(160, 75);
            this.buttonItem43.Image = global::CWMS.Properties.Resources.RockMelt;
            this.buttonItem43.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem43.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem43.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem43.Name = "buttonItem43";
            this.buttonItem43.Text = "با پورتال تحت وب";
            this.buttonItem43.Click += new System.EventHandler(this.buttonItem43_Click_2);
            // 
            // InternetMobikePanel
            // 
            this.InternetMobikePanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.InternetMobikePanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.InternetMobikePanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.InternetMobikePanel.ContainerControlProcessDialogKey = true;
            this.InternetMobikePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.InternetMobikePanel.DragDropSupport = true;
            this.InternetMobikePanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer10,
            this.itemContainer48,
            this.itemContainer51});
            this.InternetMobikePanel.Location = new System.Drawing.Point(3, 0);
            this.InternetMobikePanel.Name = "InternetMobikePanel";
            this.InternetMobikePanel.Size = new System.Drawing.Size(327, 98);
            this.InternetMobikePanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.InternetMobikePanel.TabIndex = 10;
            this.InternetMobikePanel.Text = "موبایل";
            // 
            // 
            // 
            this.InternetMobikePanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.InternetMobikePanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer10
            // 
            // 
            // 
            // 
            this.itemContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer10.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer10.Name = "itemContainer10";
            this.itemContainer10.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.internet_gozareshat_etesal_mobile});
            // 
            // 
            // 
            this.itemContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // internet_gozareshat_etesal_mobile
            // 
            this.internet_gozareshat_etesal_mobile.FixedSize = new System.Drawing.Size(100, 70);
            this.internet_gozareshat_etesal_mobile.Image = global::CWMS.Properties.Resources.excel_rainbow2003;
            this.internet_gozareshat_etesal_mobile.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.internet_gozareshat_etesal_mobile.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.internet_gozareshat_etesal_mobile.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.internet_gozareshat_etesal_mobile.Name = "internet_gozareshat_etesal_mobile";
            this.internet_gozareshat_etesal_mobile.Text = "گزارشات اتصال موبایل";
            this.internet_gozareshat_etesal_mobile.Click += new System.EventHandler(this.buttonItem68_Click);
            // 
            // itemContainer48
            // 
            // 
            // 
            // 
            this.itemContainer48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer48.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer48.Name = "itemContainer48";
            this.itemContainer48.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.internet_moshahede_key});
            // 
            // 
            // 
            this.itemContainer48.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // internet_moshahede_key
            // 
            this.internet_moshahede_key.FixedSize = new System.Drawing.Size(100, 70);
            this.internet_moshahede_key.Image = global::CWMS.Properties.Resources.ExtensionsManager_summer;
            this.internet_moshahede_key.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.internet_moshahede_key.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.internet_moshahede_key.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.internet_moshahede_key.Name = "internet_moshahede_key";
            this.internet_moshahede_key.Text = "مشاهده کلیه کلیدها";
            this.internet_moshahede_key.Click += new System.EventHandler(this.buttonItem69_Click);
            // 
            // itemContainer51
            // 
            // 
            // 
            // 
            this.itemContainer51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer51.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer51.Name = "itemContainer51";
            this.itemContainer51.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.internet_afzoodan_key});
            // 
            // 
            // 
            this.itemContainer51.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // internet_afzoodan_key
            // 
            this.internet_afzoodan_key.FixedSize = new System.Drawing.Size(100, 70);
            this.internet_afzoodan_key.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.internet_afzoodan_key.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.internet_afzoodan_key.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.internet_afzoodan_key.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.internet_afzoodan_key.Name = "internet_afzoodan_key";
            this.internet_afzoodan_key.Text = "افزودن کلید امنیتی";
            this.internet_afzoodan_key.Click += new System.EventHandler(this.buttonItem72_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel6.Controls.Add(this.ribbonBar11);
            this.ribbonPanel6.Controls.Add(this.ribbonBar2);
            this.ribbonPanel6.Controls.Add(this.bargeh_gheir_faal);
            this.ribbonPanel6.Controls.Add(this.barge1panel);
            this.ribbonPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel6.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel6.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel6.TabIndex = 9;
            this.ribbonPanel6.Visible = false;
            // 
            // ribbonBar11
            // 
            this.ribbonBar11.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.ContainerControlProcessDialogKey = true;
            this.ribbonBar11.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar11.DragDropSupport = true;
            this.ribbonBar11.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer46});
            this.ribbonBar11.Location = new System.Drawing.Point(508, 0);
            this.ribbonBar11.Name = "ribbonBar11";
            this.ribbonBar11.Size = new System.Drawing.Size(168, 98);
            this.ribbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar11.TabIndex = 15;
            this.ribbonBar11.Text = "بروزرسانی کلیه اطلاعات سرور";
            // 
            // 
            // 
            this.ribbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer46
            // 
            // 
            // 
            // 
            this.itemContainer46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer46.FixedSize = new System.Drawing.Size(160, 0);
            this.itemContainer46.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer46.Name = "itemContainer46";
            this.itemContainer46.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem40});
            // 
            // 
            // 
            this.itemContainer46.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem40
            // 
            this.buttonItem40.FixedSize = new System.Drawing.Size(160, 75);
            this.buttonItem40.Image = global::CWMS.Properties.Resources.RockMelt;
            this.buttonItem40.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem40.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem40.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem40.Name = "buttonItem40";
            this.buttonItem40.Text = "با سامانه ورودی شهرک";
            this.buttonItem40.Click += new System.EventHandler(this.buttonItem40_Click);
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.DragDropSupport = true;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2});
            this.ribbonBar2.Location = new System.Drawing.Point(340, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(168, 98);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar2.TabIndex = 13;
            this.ribbonBar2.Text = "سامانه چاپ درخواست ها";
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.FixedSize = new System.Drawing.Size(160, 0);
            this.itemContainer2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem10});
            // 
            // 
            // 
            this.itemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem10
            // 
            this.buttonItem10.FixedSize = new System.Drawing.Size(160, 75);
            this.buttonItem10.Image = global::CWMS.Properties.Resources.Network_Drive;
            this.buttonItem10.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem10.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Text = "برگه خروج";
            this.buttonItem10.Click += new System.EventHandler(this.buttonItem10_Click);
            // 
            // bargeh_gheir_faal
            // 
            this.bargeh_gheir_faal.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.bargeh_gheir_faal.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.bargeh_gheir_faal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.bargeh_gheir_faal.ContainerControlProcessDialogKey = true;
            this.bargeh_gheir_faal.Dock = System.Windows.Forms.DockStyle.Left;
            this.bargeh_gheir_faal.DragDropSupport = true;
            this.bargeh_gheir_faal.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer31});
            this.bargeh_gheir_faal.Location = new System.Drawing.Point(211, 0);
            this.bargeh_gheir_faal.Name = "bargeh_gheir_faal";
            this.bargeh_gheir_faal.Size = new System.Drawing.Size(129, 98);
            this.bargeh_gheir_faal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bargeh_gheir_faal.TabIndex = 11;
            this.bargeh_gheir_faal.Text = "گزارشات";
            // 
            // 
            // 
            this.bargeh_gheir_faal.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.bargeh_gheir_faal.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer31
            // 
            // 
            // 
            // 
            this.itemContainer31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer31.FixedSize = new System.Drawing.Size(120, 0);
            this.itemContainer31.Name = "itemContainer31";
            this.itemContainer31.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bargeh_gheir_faal1});
            // 
            // 
            // 
            this.itemContainer31.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // bargeh_gheir_faal1
            // 
            this.bargeh_gheir_faal1.FixedSize = new System.Drawing.Size(100, 70);
            this.bargeh_gheir_faal1.Image = global::CWMS.Properties.Resources.Update;
            this.bargeh_gheir_faal1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.bargeh_gheir_faal1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.bargeh_gheir_faal1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.bargeh_gheir_faal1.Name = "bargeh_gheir_faal1";
            this.bargeh_gheir_faal1.Text = "اطلاعات آماری کلیه دوره ها";
            this.bargeh_gheir_faal1.Click += new System.EventHandler(this.buttonItem42_Click);
            // 
            // barge1panel
            // 
            this.barge1panel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.barge1panel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barge1panel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.barge1panel.ContainerControlProcessDialogKey = true;
            this.barge1panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.barge1panel.DragDropSupport = true;
            this.barge1panel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer17,
            this.itemContainer23});
            this.barge1panel.Location = new System.Drawing.Point(3, 0);
            this.barge1panel.Name = "barge1panel";
            this.barge1panel.Size = new System.Drawing.Size(208, 98);
            this.barge1panel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.barge1panel.TabIndex = 10;
            this.barge1panel.Text = "برگه خروج";
            // 
            // 
            // 
            this.barge1panel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.barge1panel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer17
            // 
            // 
            // 
            // 
            this.itemContainer17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer17.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer17.Name = "itemContainer17";
            this.itemContainer17.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bargeh_moshahede_darkhast});
            // 
            // 
            // 
            this.itemContainer17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // bargeh_moshahede_darkhast
            // 
            this.bargeh_moshahede_darkhast.FixedSize = new System.Drawing.Size(100, 70);
            this.bargeh_moshahede_darkhast.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.bargeh_moshahede_darkhast.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.bargeh_moshahede_darkhast.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.bargeh_moshahede_darkhast.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.bargeh_moshahede_darkhast.Name = "bargeh_moshahede_darkhast";
            this.bargeh_moshahede_darkhast.Text = "نمایش درخواست ها";
            this.bargeh_moshahede_darkhast.Click += new System.EventHandler(this.buttonItem32_Click);
            // 
            // itemContainer23
            // 
            // 
            // 
            // 
            this.itemContainer23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer23.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer23.Name = "itemContainer23";
            this.itemContainer23.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bargeh_sodoor});
            // 
            // 
            // 
            this.itemContainer23.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // bargeh_sodoor
            // 
            this.bargeh_sodoor.FixedSize = new System.Drawing.Size(100, 70);
            this.bargeh_sodoor.Image = global::CWMS.Properties.Resources.Windows_Card_Space;
            this.bargeh_sodoor.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.bargeh_sodoor.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.bargeh_sodoor.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.bargeh_sodoor.Name = "bargeh_sodoor";
            this.bargeh_sodoor.Text = "صدور برگه ";
            this.bargeh_sodoor.Click += new System.EventHandler(this.bargeh_sodoor_Click);
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel7.Controls.Add(this.ribbonBar10);
            this.ribbonPanel7.Controls.Add(this.modiriat_tayin_vaziat_sodoor);
            this.ribbonPanel7.Controls.Add(this.modiriat_setting);
            this.ribbonPanel7.Controls.Add(this.modiriat_etelaieh);
            this.ribbonPanel7.Controls.Add(this.shahrakKKarbaranPanel);
            this.ribbonPanel7.Controls.Add(this.modiriat_backup);
            this.ribbonPanel7.Controls.Add(this.modiriat_peikarbandi);
            this.ribbonPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel7.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel7.Name = "ribbonPanel7";
            this.ribbonPanel7.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel7.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel7.TabIndex = 10;
            // 
            // ribbonBar10
            // 
            this.ribbonBar10.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.ContainerControlProcessDialogKey = true;
            this.ribbonBar10.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar10.DragDropSupport = true;
            this.ribbonBar10.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer35});
            this.ribbonBar10.Location = new System.Drawing.Point(1117, 0);
            this.ribbonBar10.Name = "ribbonBar10";
            this.ribbonBar10.Size = new System.Drawing.Size(168, 98);
            this.ribbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar10.TabIndex = 23;
            this.ribbonBar10.Text = "سامانه پیشرفته صدور قبض";
            // 
            // 
            // 
            this.ribbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer35
            // 
            // 
            // 
            // 
            this.itemContainer35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer35.FixedSize = new System.Drawing.Size(160, 0);
            this.itemContainer35.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer35.Name = "itemContainer35";
            this.itemContainer35.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem38});
            // 
            // 
            // 
            this.itemContainer35.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem38
            // 
            this.buttonItem38.FixedSize = new System.Drawing.Size(160, 75);
            this.buttonItem38.Image = global::CWMS.Properties.Resources.RockMelt;
            this.buttonItem38.ImageFixedSize = new System.Drawing.Size(40, 40);
            this.buttonItem38.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem38.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem38.Name = "buttonItem38";
            this.buttonItem38.Text = "قبوض آب و شارژ";
            this.buttonItem38.Click += new System.EventHandler(this.buttonItem38_Click_2);
            // 
            // modiriat_tayin_vaziat_sodoor
            // 
            this.modiriat_tayin_vaziat_sodoor.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.modiriat_tayin_vaziat_sodoor.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_tayin_vaziat_sodoor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.modiriat_tayin_vaziat_sodoor.ContainerControlProcessDialogKey = true;
            this.modiriat_tayin_vaziat_sodoor.Dock = System.Windows.Forms.DockStyle.Left;
            this.modiriat_tayin_vaziat_sodoor.DragDropSupport = true;
            this.modiriat_tayin_vaziat_sodoor.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem28});
            this.modiriat_tayin_vaziat_sodoor.Location = new System.Drawing.Point(957, 0);
            this.modiriat_tayin_vaziat_sodoor.Name = "modiriat_tayin_vaziat_sodoor";
            this.modiriat_tayin_vaziat_sodoor.Size = new System.Drawing.Size(160, 98);
            this.modiriat_tayin_vaziat_sodoor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.modiriat_tayin_vaziat_sodoor.TabIndex = 22;
            this.modiriat_tayin_vaziat_sodoor.Text = "تعیین وضعیت صدور قبوض مشترکین";
            // 
            // 
            // 
            this.modiriat_tayin_vaziat_sodoor.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_tayin_vaziat_sodoor.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem28
            // 
            this.buttonItem28.FixedSize = new System.Drawing.Size(150, 70);
            this.buttonItem28.Image = global::CWMS.Properties.Resources.ExtensionsManager_summer;
            this.buttonItem28.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.buttonItem28.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem28.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem28.Name = "buttonItem28";
            this.buttonItem28.Text = "اب و شارژ";
            this.buttonItem28.Click += new System.EventHandler(this.buttonItem28_Click);
            // 
            // modiriat_setting
            // 
            this.modiriat_setting.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.modiriat_setting.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_setting.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.modiriat_setting.ContainerControlProcessDialogKey = true;
            this.modiriat_setting.Dock = System.Windows.Forms.DockStyle.Left;
            this.modiriat_setting.DragDropSupport = true;
            this.modiriat_setting.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem11});
            this.modiriat_setting.Location = new System.Drawing.Point(837, 0);
            this.modiriat_setting.Name = "modiriat_setting";
            this.modiriat_setting.Size = new System.Drawing.Size(120, 98);
            this.modiriat_setting.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.modiriat_setting.TabIndex = 21;
            this.modiriat_setting.Text = "تنظیمات کلی شهرک";
            // 
            // 
            // 
            this.modiriat_setting.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_setting.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem11
            // 
            this.buttonItem11.FixedSize = new System.Drawing.Size(110, 70);
            this.buttonItem11.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.buttonItem11.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.buttonItem11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem11.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Text = " ";
            this.buttonItem11.Click += new System.EventHandler(this.buttonItem11_Click_1);
            // 
            // modiriat_etelaieh
            // 
            this.modiriat_etelaieh.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.modiriat_etelaieh.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_etelaieh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.modiriat_etelaieh.ContainerControlProcessDialogKey = true;
            this.modiriat_etelaieh.Dock = System.Windows.Forms.DockStyle.Left;
            this.modiriat_etelaieh.DragDropSupport = true;
            this.modiriat_etelaieh.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.modiriat_etelaieh1});
            this.modiriat_etelaieh.Location = new System.Drawing.Point(651, 0);
            this.modiriat_etelaieh.Name = "modiriat_etelaieh";
            this.modiriat_etelaieh.Size = new System.Drawing.Size(186, 98);
            this.modiriat_etelaieh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.modiriat_etelaieh.TabIndex = 20;
            this.modiriat_etelaieh.Text = "اطلاعیه های سیستم";
            // 
            // 
            // 
            this.modiriat_etelaieh.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_etelaieh.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // modiriat_etelaieh1
            // 
            this.modiriat_etelaieh1.FixedSize = new System.Drawing.Size(180, 70);
            this.modiriat_etelaieh1.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.modiriat_etelaieh1.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.modiriat_etelaieh1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.modiriat_etelaieh1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.modiriat_etelaieh1.Name = "modiriat_etelaieh1";
            this.modiriat_etelaieh1.Text = " ";
            this.modiriat_etelaieh1.Click += new System.EventHandler(this.modiriat_etelaieh1_Click);
            // 
            // shahrakKKarbaranPanel
            // 
            this.shahrakKKarbaranPanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.shahrakKKarbaranPanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.shahrakKKarbaranPanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.shahrakKKarbaranPanel.ContainerControlProcessDialogKey = true;
            this.shahrakKKarbaranPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.shahrakKKarbaranPanel.DragDropSupport = true;
            this.shahrakKKarbaranPanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer16,
            this.itemContainer18});
            this.shahrakKKarbaranPanel.Location = new System.Drawing.Point(452, 0);
            this.shahrakKKarbaranPanel.Name = "shahrakKKarbaranPanel";
            this.shahrakKKarbaranPanel.Size = new System.Drawing.Size(199, 98);
            this.shahrakKKarbaranPanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.shahrakKKarbaranPanel.TabIndex = 18;
            this.shahrakKKarbaranPanel.Text = "مدیریت کاربران";
            // 
            // 
            // 
            this.shahrakKKarbaranPanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.shahrakKKarbaranPanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer16
            // 
            // 
            // 
            // 
            this.itemContainer16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer16.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer16.Name = "itemContainer16";
            this.itemContainer16.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.modiriat_admin_users});
            // 
            // 
            // 
            this.itemContainer16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // modiriat_admin_users
            // 
            this.modiriat_admin_users.FixedSize = new System.Drawing.Size(100, 70);
            this.modiriat_admin_users.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.modiriat_admin_users.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.modiriat_admin_users.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.modiriat_admin_users.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.modiriat_admin_users.Name = "modiriat_admin_users";
            this.modiriat_admin_users.Text = "مشاهده کاربران";
            this.modiriat_admin_users.Click += new System.EventHandler(this.modiriat_admin_users_Click);
            // 
            // itemContainer18
            // 
            // 
            // 
            // 
            this.itemContainer18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer18.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer18.Name = "itemContainer18";
            this.itemContainer18.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.modiriat_gozareshat_dastarsi});
            // 
            // 
            // 
            this.itemContainer18.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // modiriat_gozareshat_dastarsi
            // 
            this.modiriat_gozareshat_dastarsi.FixedSize = new System.Drawing.Size(100, 70);
            this.modiriat_gozareshat_dastarsi.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.modiriat_gozareshat_dastarsi.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.modiriat_gozareshat_dastarsi.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.modiriat_gozareshat_dastarsi.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.modiriat_gozareshat_dastarsi.Name = "modiriat_gozareshat_dastarsi";
            this.modiriat_gozareshat_dastarsi.Text = "گزارشات دسترسی";
            this.modiriat_gozareshat_dastarsi.Click += new System.EventHandler(this.modiriat_gozareshat_dastarsi_Click);
            // 
            // modiriat_backup
            // 
            this.modiriat_backup.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.modiriat_backup.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_backup.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.modiriat_backup.ContainerControlProcessDialogKey = true;
            this.modiriat_backup.Dock = System.Windows.Forms.DockStyle.Left;
            this.modiriat_backup.DragDropSupport = true;
            this.modiriat_backup.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem67});
            this.modiriat_backup.Location = new System.Drawing.Point(266, 0);
            this.modiriat_backup.Name = "modiriat_backup";
            this.modiriat_backup.Size = new System.Drawing.Size(186, 98);
            this.modiriat_backup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.modiriat_backup.TabIndex = 17;
            this.modiriat_backup.Text = "سامانه پشتیبان گیری";
            // 
            // 
            // 
            this.modiriat_backup.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_backup.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem67
            // 
            this.buttonItem67.FixedSize = new System.Drawing.Size(180, 70);
            this.buttonItem67.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.buttonItem67.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.buttonItem67.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem67.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem67.Name = "buttonItem67";
            this.buttonItem67.Text = "پشیبان گیری از اطلاعات";
            this.buttonItem67.Click += new System.EventHandler(this.buttonItem67_Click);
            // 
            // modiriat_peikarbandi
            // 
            this.modiriat_peikarbandi.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.modiriat_peikarbandi.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_peikarbandi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.modiriat_peikarbandi.ContainerControlProcessDialogKey = true;
            this.modiriat_peikarbandi.Dock = System.Windows.Forms.DockStyle.Left;
            this.modiriat_peikarbandi.DragDropSupport = true;
            this.modiriat_peikarbandi.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer28,
            this.itemContainer12});
            this.modiriat_peikarbandi.Location = new System.Drawing.Point(3, 0);
            this.modiriat_peikarbandi.Name = "modiriat_peikarbandi";
            this.modiriat_peikarbandi.Size = new System.Drawing.Size(263, 98);
            this.modiriat_peikarbandi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.modiriat_peikarbandi.TabIndex = 5;
            this.modiriat_peikarbandi.Text = "راه اندازی اولیه سیستم";
            // 
            // 
            // 
            this.modiriat_peikarbandi.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.modiriat_peikarbandi.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer28
            // 
            // 
            // 
            // 
            this.itemContainer28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer28.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer28.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer28.Name = "itemContainer28";
            this.itemContainer28.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.modiriat_bedehi_ab});
            this.itemContainer28.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer28.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // modiriat_bedehi_ab
            // 
            this.modiriat_bedehi_ab.Image = global::CWMS.Properties.Resources.Parental_Controls;
            this.modiriat_bedehi_ab.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.modiriat_bedehi_ab.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.modiriat_bedehi_ab.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.modiriat_bedehi_ab.Name = "modiriat_bedehi_ab";
            this.modiriat_bedehi_ab.Text = "اطلاعات اولیه آب";
            this.modiriat_bedehi_ab.Click += new System.EventHandler(this.modiriat_bedehi_ab_Click);
            // 
            // itemContainer12
            // 
            // 
            // 
            // 
            this.itemContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer12.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer12.Name = "itemContainer12";
            this.itemContainer12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.modiriat_bedehi_sharj});
            this.itemContainer12.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // modiriat_bedehi_sharj
            // 
            this.modiriat_bedehi_sharj.Image = global::CWMS.Properties.Resources.Parental_Controls;
            this.modiriat_bedehi_sharj.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.modiriat_bedehi_sharj.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.modiriat_bedehi_sharj.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.modiriat_bedehi_sharj.Name = "modiriat_bedehi_sharj";
            this.modiriat_bedehi_sharj.Text = "اطلاعات اولیه شارژ";
            this.modiriat_bedehi_sharj.Click += new System.EventHandler(this.buttonItem11_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.CanvasColor = System.Drawing.Color.SkyBlue;
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel4.Controls.Add(this.ribbonBar6);
            this.ribbonPanel4.Controls.Add(this.ab_jarmieh);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_barge_darkhast);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_JostejooInSavabegh);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_afzoodan_key);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_sharj);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_ab);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_moshahedeh);
            this.ribbonPanel4.Controls.Add(this.moshtarekin_sabte_moshtarek_jadidi);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel4.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 5;
            this.ribbonPanel4.Visible = false;
            this.ribbonPanel4.Click += new System.EventHandler(this.ribbonPanel4_Click);
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar6.DragDropSupport = true;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem21});
            this.ribbonBar6.Location = new System.Drawing.Point(1006, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(186, 98);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar6.TabIndex = 24;
            this.ribbonBar6.Text = "سامانه پیشرفته چاپ قبوض";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem21
            // 
            this.buttonItem21.FixedSize = new System.Drawing.Size(180, 80);
            this.buttonItem21.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.buttonItem21.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.buttonItem21.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem21.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem21.Name = "buttonItem21";
            this.buttonItem21.Text = "آب و شارژ";
            this.buttonItem21.Click += new System.EventHandler(this.buttonItem21_Click);
            // 
            // ab_jarmieh
            // 
            this.ab_jarmieh.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ab_jarmieh.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_jarmieh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ab_jarmieh.ContainerControlProcessDialogKey = true;
            this.ab_jarmieh.Dock = System.Windows.Forms.DockStyle.Left;
            this.ab_jarmieh.DragDropSupport = true;
            this.ab_jarmieh.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem30});
            this.ab_jarmieh.Location = new System.Drawing.Point(846, 0);
            this.ab_jarmieh.Name = "ab_jarmieh";
            this.ab_jarmieh.Size = new System.Drawing.Size(160, 98);
            this.ab_jarmieh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ab_jarmieh.TabIndex = 23;
            this.ab_jarmieh.Text = "اطلاعات آماری  مشترکین";
            // 
            // 
            // 
            this.ab_jarmieh.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_jarmieh.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem30
            // 
            this.buttonItem30.FixedSize = new System.Drawing.Size(150, 70);
            this.buttonItem30.Image = global::CWMS.Properties.Resources.ExtensionsManager_summer;
            this.buttonItem30.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.buttonItem30.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem30.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem30.Name = "buttonItem30";
            this.buttonItem30.Text = "آمار ";
            this.buttonItem30.Click += new System.EventHandler(this.buttonItem30_Click_1);
            // 
            // moshtarekin_barge_darkhast
            // 
            this.moshtarekin_barge_darkhast.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_barge_darkhast.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_barge_darkhast.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_barge_darkhast.ContainerControlProcessDialogKey = true;
            this.moshtarekin_barge_darkhast.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_barge_darkhast.DragDropSupport = true;
            this.moshtarekin_barge_darkhast.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer24});
            this.moshtarekin_barge_darkhast.Location = new System.Drawing.Point(733, 0);
            this.moshtarekin_barge_darkhast.Name = "moshtarekin_barge_darkhast";
            this.moshtarekin_barge_darkhast.Size = new System.Drawing.Size(113, 98);
            this.moshtarekin_barge_darkhast.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_barge_darkhast.TabIndex = 13;
            this.moshtarekin_barge_darkhast.Text = "برگه خروج";
            // 
            // 
            // 
            this.moshtarekin_barge_darkhast.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_barge_darkhast.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer24
            // 
            // 
            // 
            // 
            this.itemContainer24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer24.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer24.Name = "itemContainer24";
            this.itemContainer24.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem23});
            // 
            // 
            // 
            this.itemContainer24.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem23
            // 
            this.buttonItem23.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem23.Image = global::CWMS.Properties.Resources.Internet_Options;
            this.buttonItem23.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem23.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem23.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem23.Name = "buttonItem23";
            this.buttonItem23.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.buttonItem23.Text = "F6";
            this.buttonItem23.Click += new System.EventHandler(this.buttonItem23_Click_2);
            // 
            // moshtarekin_JostejooInSavabegh
            // 
            this.moshtarekin_JostejooInSavabegh.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_JostejooInSavabegh.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_JostejooInSavabegh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_JostejooInSavabegh.ContainerControlProcessDialogKey = true;
            this.moshtarekin_JostejooInSavabegh.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_JostejooInSavabegh.DragDropSupport = true;
            this.moshtarekin_JostejooInSavabegh.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer20});
            this.moshtarekin_JostejooInSavabegh.Location = new System.Drawing.Point(579, 0);
            this.moshtarekin_JostejooInSavabegh.Name = "moshtarekin_JostejooInSavabegh";
            this.moshtarekin_JostejooInSavabegh.Size = new System.Drawing.Size(154, 98);
            this.moshtarekin_JostejooInSavabegh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_JostejooInSavabegh.TabIndex = 12;
            this.moshtarekin_JostejooInSavabegh.Text = "جستجو در سوابق مشترکین";
            // 
            // 
            // 
            this.moshtarekin_JostejooInSavabegh.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_JostejooInSavabegh.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer20
            // 
            // 
            // 
            // 
            this.itemContainer20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer20.FixedSize = new System.Drawing.Size(150, 0);
            this.itemContainer20.Name = "itemContainer20";
            this.itemContainer20.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem9});
            // 
            // 
            // 
            this.itemContainer20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem9
            // 
            this.buttonItem9.FixedSize = new System.Drawing.Size(120, 70);
            this.buttonItem9.Image = global::CWMS.Properties.Resources.ExtensionsManager_summer;
            this.buttonItem9.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem9.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F7);
            this.buttonItem9.Text = "F7";
            this.buttonItem9.Click += new System.EventHandler(this.buttonItem9_Click);
            // 
            // moshtarekin_afzoodan_key
            // 
            this.moshtarekin_afzoodan_key.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_afzoodan_key.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_afzoodan_key.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_afzoodan_key.ContainerControlProcessDialogKey = true;
            this.moshtarekin_afzoodan_key.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_afzoodan_key.DragDropSupport = true;
            this.moshtarekin_afzoodan_key.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer55});
            this.moshtarekin_afzoodan_key.Location = new System.Drawing.Point(466, 0);
            this.moshtarekin_afzoodan_key.Name = "moshtarekin_afzoodan_key";
            this.moshtarekin_afzoodan_key.Size = new System.Drawing.Size(113, 98);
            this.moshtarekin_afzoodan_key.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_afzoodan_key.TabIndex = 11;
            this.moshtarekin_afzoodan_key.Text = "کلیدامنیتی موبایل";
            // 
            // 
            // 
            this.moshtarekin_afzoodan_key.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_afzoodan_key.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer55
            // 
            // 
            // 
            // 
            this.itemContainer55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer55.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer55.Name = "itemContainer55";
            this.itemContainer55.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.moshtarekin_afzoodan_key1});
            // 
            // 
            // 
            this.itemContainer55.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // moshtarekin_afzoodan_key1
            // 
            this.moshtarekin_afzoodan_key1.FixedSize = new System.Drawing.Size(100, 70);
            this.moshtarekin_afzoodan_key1.Image = global::CWMS.Properties.Resources.Network_Drive;
            this.moshtarekin_afzoodan_key1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.moshtarekin_afzoodan_key1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.moshtarekin_afzoodan_key1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.moshtarekin_afzoodan_key1.Name = "moshtarekin_afzoodan_key1";
            this.moshtarekin_afzoodan_key1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F6);
            this.moshtarekin_afzoodan_key1.Text = "F6";
            this.moshtarekin_afzoodan_key1.Click += new System.EventHandler(this.buttonItem85_Click);
            // 
            // moshtarekin_sharj
            // 
            this.moshtarekin_sharj.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_sharj.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_sharj.ContainerControlProcessDialogKey = true;
            this.moshtarekin_sharj.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_sharj.DragDropSupport = true;
            this.moshtarekin_sharj.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3});
            this.moshtarekin_sharj.Location = new System.Drawing.Point(359, 0);
            this.moshtarekin_sharj.Name = "moshtarekin_sharj";
            this.moshtarekin_sharj.Size = new System.Drawing.Size(107, 98);
            this.moshtarekin_sharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_sharj.TabIndex = 9;
            this.moshtarekin_sharj.Text = "اطلاعات قبوض شارژ";
            // 
            // 
            // 
            this.moshtarekin_sharj.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_sharj.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.ItemSpacing = 0;
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem8});
            // 
            // 
            // 
            this.itemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem8
            // 
            this.buttonItem8.BeginGroup = true;
            this.buttonItem8.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem8.FixedSize = new System.Drawing.Size(100, 70);
            this.buttonItem8.Image = global::CWMS.Properties.Resources.PowerPoint;
            this.buttonItem8.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.buttonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem8.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.buttonItem8.SubItemsExpandWidth = 24;
            this.buttonItem8.Text = "F4";
            this.buttonItem8.Click += new System.EventHandler(this.buttonItem8_Click);
            // 
            // moshtarekin_ab
            // 
            this.moshtarekin_ab.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_ab.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_ab.ContainerControlProcessDialogKey = true;
            this.moshtarekin_ab.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_ab.DragDropSupport = true;
            this.moshtarekin_ab.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer19});
            this.moshtarekin_ab.Location = new System.Drawing.Point(260, 0);
            this.moshtarekin_ab.Name = "moshtarekin_ab";
            this.moshtarekin_ab.Size = new System.Drawing.Size(99, 98);
            this.moshtarekin_ab.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_ab.TabIndex = 8;
            this.moshtarekin_ab.Text = "اطلاعات قبوض آب";
            // 
            // 
            // 
            this.moshtarekin_ab.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_ab.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer19
            // 
            // 
            // 
            // 
            this.itemContainer19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer19.Name = "itemContainer19";
            this.itemContainer19.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.q3});
            // 
            // 
            // 
            this.itemContainer19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // q3
            // 
            this.q3.FixedSize = new System.Drawing.Size(100, 70);
            this.q3.Image = global::CWMS.Properties.Resources.RockMelt;
            this.q3.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.q3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.q3.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.q3.Name = "q3";
            this.q3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.q3.Text = "F3";
            this.q3.Click += new System.EventHandler(this.buttonItem30_Click);
            // 
            // moshtarekin_moshahedeh
            // 
            this.moshtarekin_moshahedeh.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_moshahedeh.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_moshahedeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_moshahedeh.ContainerControlProcessDialogKey = true;
            this.moshtarekin_moshahedeh.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_moshahedeh.DragDropSupport = true;
            this.moshtarekin_moshahedeh.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer15});
            this.moshtarekin_moshahedeh.Location = new System.Drawing.Point(134, 0);
            this.moshtarekin_moshahedeh.Name = "moshtarekin_moshahedeh";
            this.moshtarekin_moshahedeh.Size = new System.Drawing.Size(126, 98);
            this.moshtarekin_moshahedeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_moshahedeh.TabIndex = 7;
            this.moshtarekin_moshahedeh.Text = "مشاهده اطلاعات مشترکین";
            // 
            // 
            // 
            this.moshtarekin_moshahedeh.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_moshahedeh.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer15
            // 
            // 
            // 
            // 
            this.itemContainer15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer15.FixedSize = new System.Drawing.Size(120, 0);
            this.itemContainer15.Name = "itemContainer15";
            this.itemContainer15.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.q1});
            // 
            // 
            // 
            this.itemContainer15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // q1
            // 
            this.q1.FixedSize = new System.Drawing.Size(120, 70);
            this.q1.Image = global::CWMS.Properties.Resources.BitTorrent;
            this.q1.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.q1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.q1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.q1.Name = "q1";
            this.q1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2);
            this.q1.Text = "F2";
            this.q1.Click += new System.EventHandler(this.buttonItem28_Click_1);
            // 
            // moshtarekin_sabte_moshtarek_jadidi
            // 
            this.moshtarekin_sabte_moshtarek_jadidi.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.moshtarekin_sabte_moshtarek_jadidi.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_sabte_moshtarek_jadidi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.moshtarekin_sabte_moshtarek_jadidi.ContainerControlProcessDialogKey = true;
            this.moshtarekin_sabte_moshtarek_jadidi.Dock = System.Windows.Forms.DockStyle.Left;
            this.moshtarekin_sabte_moshtarek_jadidi.DragDropSupport = true;
            this.moshtarekin_sabte_moshtarek_jadidi.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer9});
            this.moshtarekin_sabte_moshtarek_jadidi.Location = new System.Drawing.Point(3, 0);
            this.moshtarekin_sabte_moshtarek_jadidi.Name = "moshtarekin_sabte_moshtarek_jadidi";
            this.moshtarekin_sabte_moshtarek_jadidi.Size = new System.Drawing.Size(131, 98);
            this.moshtarekin_sabte_moshtarek_jadidi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.moshtarekin_sabte_moshtarek_jadidi.TabIndex = 4;
            this.moshtarekin_sabte_moshtarek_jadidi.Text = "مشترکین جدید";
            // 
            // 
            // 
            this.moshtarekin_sabte_moshtarek_jadidi.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.moshtarekin_sabte_moshtarek_jadidi.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer9
            // 
            // 
            // 
            // 
            this.itemContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer9.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer9.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer9.Name = "itemContainer9";
            this.itemContainer9.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.q});
            this.itemContainer9.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // q
            // 
            this.q.Image = global::CWMS.Properties.Resources.Parental_Controls;
            this.q.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.q.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.q.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.q.Name = "q";
            this.q.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.q.Text = "F1";
            this.q.Click += new System.EventHandler(this.buttonItem27_Click_1);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribbonBar8);
            this.ribbonPanel3.Controls.Add(this.ribbonBar1);
            this.ribbonPanel3.Controls.Add(this.ab_history);
            this.ribbonPanel3.Controls.Add(this.abTanzimatPanel);
            this.ribbonPanel3.Controls.Add(this.ab_doreh_sodoor_ghabz);
            this.ribbonPanel3.Controls.Add(this.abMoshahedePanel);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel3.Size = new System.Drawing.Size(1360, 100);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.DragDropSupport = true;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem22});
            this.ribbonBar8.Location = new System.Drawing.Point(1173, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(100, 98);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar8.TabIndex = 20;
            this.ribbonBar8.Text = "لیست قطعی ها";
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem22
            // 
            this.buttonItem22.FixedSize = new System.Drawing.Size(100, 80);
            this.buttonItem22.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem22.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.buttonItem22.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem22.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem22.Name = "buttonItem22";
            this.buttonItem22.Text = "بدهکاران ";
            this.buttonItem22.Click += new System.EventHandler(this.buttonItem22_Click);
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.DragDropSupport = true;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem31});
            this.ribbonBar1.Location = new System.Drawing.Point(987, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(186, 98);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar1.TabIndex = 19;
            this.ribbonBar1.Text = "ارسال اطلاعات جهت کنتورخوانی";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem31
            // 
            this.buttonItem31.FixedSize = new System.Drawing.Size(180, 80);
            this.buttonItem31.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.buttonItem31.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.buttonItem31.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem31.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem31.Name = "buttonItem31";
            this.buttonItem31.Text = "نرم افزار اندروید کنتورخوان";
            this.buttonItem31.Click += new System.EventHandler(this.buttonItem31_Click_1);
            // 
            // ab_history
            // 
            this.ab_history.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ab_history.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_history.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ab_history.ContainerControlProcessDialogKey = true;
            this.ab_history.Dock = System.Windows.Forms.DockStyle.Left;
            this.ab_history.DragDropSupport = true;
            this.ab_history.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem29});
            this.ab_history.Location = new System.Drawing.Point(869, 0);
            this.ab_history.Name = "ab_history";
            this.ab_history.Size = new System.Drawing.Size(118, 98);
            this.ab_history.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ab_history.TabIndex = 18;
            this.ab_history.Text = "تاریخچه قرائت کنتورخوان";
            // 
            // 
            // 
            this.ab_history.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_history.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem29
            // 
            this.buttonItem29.FixedSize = new System.Drawing.Size(110, 70);
            this.buttonItem29.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.buttonItem29.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.buttonItem29.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem29.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem29.Name = "buttonItem29";
            this.buttonItem29.Click += new System.EventHandler(this.buttonItem29_Click);
            // 
            // abTanzimatPanel
            // 
            this.abTanzimatPanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.abTanzimatPanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.abTanzimatPanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.abTanzimatPanel.ContainerControlProcessDialogKey = true;
            this.abTanzimatPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.abTanzimatPanel.DragDropSupport = true;
            this.abTanzimatPanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer36,
            this.itemContainer38,
            this.itemContainer5,
            this.itemContainer39});
            this.abTanzimatPanel.Location = new System.Drawing.Point(388, 0);
            this.abTanzimatPanel.Name = "abTanzimatPanel";
            this.abTanzimatPanel.Size = new System.Drawing.Size(481, 98);
            this.abTanzimatPanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.abTanzimatPanel.TabIndex = 17;
            this.abTanzimatPanel.Text = "تنظیمات کلی دوره های آب";
            // 
            // 
            // 
            this.abTanzimatPanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.abTanzimatPanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer36
            // 
            // 
            // 
            // 
            this.itemContainer36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer36.ItemSpacing = 0;
            this.itemContainer36.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer36.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer36.Name = "itemContainer36";
            this.itemContainer36.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_ababaha});
            this.itemContainer36.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer36.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_ababaha
            // 
            this.ab_ababaha.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.ab_ababaha.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.ab_ababaha.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_ababaha.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_ababaha.Name = "ab_ababaha";
            this.ab_ababaha.Text = "مدیریت آب بها";
            this.ab_ababaha.Click += new System.EventHandler(this.buttonItem50_Click);
            // 
            // itemContainer38
            // 
            // 
            // 
            // 
            this.itemContainer38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer38.ItemSpacing = 0;
            this.itemContainer38.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer38.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer38.Name = "itemContainer38";
            this.itemContainer38.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_aboonman});
            this.itemContainer38.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer38.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_aboonman
            // 
            this.ab_aboonman.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.ab_aboonman.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.ab_aboonman.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_aboonman.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_aboonman.Name = "ab_aboonman";
            this.ab_aboonman.Text = "آبونمان انشعاب";
            this.ab_aboonman.Click += new System.EventHandler(this.buttonItem52_Click_1);
            // 
            // itemContainer5
            // 
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.ItemSpacing = 0;
            this.itemContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer5.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_tanzimat});
            this.itemContainer5.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_tanzimat
            // 
            this.ab_tanzimat.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.ab_tanzimat.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.ab_tanzimat.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_tanzimat.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_tanzimat.Name = "ab_tanzimat";
            this.ab_tanzimat.Text = "تنظیمات کلی";
            this.ab_tanzimat.Click += new System.EventHandler(this.buttonItem43_Click_1);
            // 
            // itemContainer39
            // 
            // 
            // 
            // 
            this.itemContainer39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer39.ItemSpacing = 0;
            this.itemContainer39.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer39.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer39.Name = "itemContainer39";
            this.itemContainer39.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_zarayeb});
            this.itemContainer39.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer39.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_zarayeb
            // 
            this.ab_zarayeb.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.ab_zarayeb.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.ab_zarayeb.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_zarayeb.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_zarayeb.Name = "ab_zarayeb";
            this.ab_zarayeb.Text = "تعیین ضرایب  مصرف مازاد";
            this.ab_zarayeb.Click += new System.EventHandler(this.buttonItem53_Click);
            // 
            // ab_doreh_sodoor_ghabz
            // 
            this.ab_doreh_sodoor_ghabz.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ab_doreh_sodoor_ghabz.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_doreh_sodoor_ghabz.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ab_doreh_sodoor_ghabz.ContainerControlProcessDialogKey = true;
            this.ab_doreh_sodoor_ghabz.Dock = System.Windows.Forms.DockStyle.Left;
            this.ab_doreh_sodoor_ghabz.DragDropSupport = true;
            this.ab_doreh_sodoor_ghabz.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_doreh_sodoor_ghabz1});
            this.ab_doreh_sodoor_ghabz.Location = new System.Drawing.Point(202, 0);
            this.ab_doreh_sodoor_ghabz.Name = "ab_doreh_sodoor_ghabz";
            this.ab_doreh_sodoor_ghabz.Size = new System.Drawing.Size(186, 98);
            this.ab_doreh_sodoor_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ab_doreh_sodoor_ghabz.TabIndex = 16;
            this.ab_doreh_sodoor_ghabz.Text = "مدیریت دوره های آب";
            // 
            // 
            // 
            this.ab_doreh_sodoor_ghabz.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ab_doreh_sodoor_ghabz.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_doreh_sodoor_ghabz1
            // 
            this.ab_doreh_sodoor_ghabz1.FixedSize = new System.Drawing.Size(180, 70);
            this.ab_doreh_sodoor_ghabz1.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.ab_doreh_sodoor_ghabz1.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.ab_doreh_sodoor_ghabz1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_doreh_sodoor_ghabz1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_doreh_sodoor_ghabz1.Name = "ab_doreh_sodoor_ghabz1";
            this.ab_doreh_sodoor_ghabz1.Text = "تعریف دوره  - صدور قبض ";
            this.ab_doreh_sodoor_ghabz1.Click += new System.EventHandler(this.buttonItem44_Click);
            // 
            // abMoshahedePanel
            // 
            this.abMoshahedePanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.abMoshahedePanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.abMoshahedePanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.abMoshahedePanel.ContainerControlProcessDialogKey = true;
            this.abMoshahedePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.abMoshahedePanel.DragDropSupport = true;
            this.abMoshahedePanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer11,
            this.itemContainer32});
            this.abMoshahedePanel.Location = new System.Drawing.Point(3, 0);
            this.abMoshahedePanel.Name = "abMoshahedePanel";
            this.abMoshahedePanel.Size = new System.Drawing.Size(199, 98);
            this.abMoshahedePanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.abMoshahedePanel.TabIndex = 15;
            this.abMoshahedePanel.Text = "مشاهده قبوض آب";
            // 
            // 
            // 
            this.abMoshahedePanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.abMoshahedePanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer11
            // 
            // 
            // 
            // 
            this.itemContainer11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer11.Name = "itemContainer11";
            this.itemContainer11.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_moshtarek});
            // 
            // 
            // 
            this.itemContainer11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_moshtarek
            // 
            this.ab_moshtarek.FixedSize = new System.Drawing.Size(100, 70);
            this.ab_moshtarek.Image = global::CWMS.Properties.Resources.Live_Messenger;
            this.ab_moshtarek.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.ab_moshtarek.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_moshtarek.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_moshtarek.Name = "ab_moshtarek";
            this.ab_moshtarek.Text = "مشترک";
            this.ab_moshtarek.Click += new System.EventHandler(this.buttonItem45_Click);
            // 
            // itemContainer32
            // 
            // 
            // 
            // 
            this.itemContainer32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer32.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer32.Name = "itemContainer32";
            this.itemContainer32.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ab_doreh});
            // 
            // 
            // 
            this.itemContainer32.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ab_doreh
            // 
            this.ab_doreh.FixedSize = new System.Drawing.Size(100, 70);
            this.ab_doreh.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.ab_doreh.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.ab_doreh.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.ab_doreh.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.ab_doreh.Name = "ab_doreh";
            this.ab_doreh.Text = "دوره";
            this.ab_doreh.Click += new System.EventHandler(this.buttonItem46_Click);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Controls.Add(this.sharjTanzimatPanel);
            this.ribbonPanel2.Controls.Add(this.sharj_tarif_doreh_sodoor_ghabz);
            this.ribbonPanel2.Controls.Add(this.sharjMoshahedePanel);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 58);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.ribbonPanel2.Size = new System.Drawing.Size(1360, 99);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 8;
            this.ribbonPanel2.Visible = false;
            // 
            // sharjTanzimatPanel
            // 
            this.sharjTanzimatPanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.sharjTanzimatPanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharjTanzimatPanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.sharjTanzimatPanel.ContainerControlProcessDialogKey = true;
            this.sharjTanzimatPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sharjTanzimatPanel.DragDropSupport = true;
            this.sharjTanzimatPanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer29,
            this.itemContainer37});
            this.sharjTanzimatPanel.Location = new System.Drawing.Point(388, 0);
            this.sharjTanzimatPanel.Name = "sharjTanzimatPanel";
            this.sharjTanzimatPanel.Size = new System.Drawing.Size(238, 97);
            this.sharjTanzimatPanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.sharjTanzimatPanel.TabIndex = 13;
            this.sharjTanzimatPanel.Text = "تنظیمات کلی دوره های شارژ";
            // 
            // 
            // 
            this.sharjTanzimatPanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharjTanzimatPanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer29
            // 
            // 
            // 
            // 
            this.itemContainer29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer29.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer29.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer29.Name = "itemContainer29";
            this.itemContainer29.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.sharj_tanzimat});
            this.itemContainer29.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer29.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // sharj_tanzimat
            // 
            this.sharj_tanzimat.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.sharj_tanzimat.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.sharj_tanzimat.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.sharj_tanzimat.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.sharj_tanzimat.Name = "sharj_tanzimat";
            this.sharj_tanzimat.Text = "تنظیمات کلی";
            this.sharj_tanzimat.Click += new System.EventHandler(this.buttonItem39_Click_1);
            // 
            // itemContainer37
            // 
            // 
            // 
            // 
            this.itemContainer37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer37.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer37.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer37.Name = "itemContainer37";
            this.itemContainer37.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.sharj_tarefe});
            this.itemContainer37.Text = "fghfdgdf";
            // 
            // 
            // 
            this.itemContainer37.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // sharj_tarefe
            // 
            this.sharj_tarefe.Image = global::CWMS.Properties.Resources.Google_Picasa_alt;
            this.sharj_tarefe.ImageFixedSize = new System.Drawing.Size(50, 48);
            this.sharj_tarefe.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.sharj_tarefe.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.sharj_tarefe.Name = "sharj_tarefe";
            this.sharj_tarefe.Text = "تعرفه های شارژ";
            this.sharj_tarefe.Click += new System.EventHandler(this.buttonItem51_Click);
            // 
            // sharj_tarif_doreh_sodoor_ghabz
            // 
            this.sharj_tarif_doreh_sodoor_ghabz.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.sharj_tarif_doreh_sodoor_ghabz.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharj_tarif_doreh_sodoor_ghabz.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.sharj_tarif_doreh_sodoor_ghabz.ContainerControlProcessDialogKey = true;
            this.sharj_tarif_doreh_sodoor_ghabz.Dock = System.Windows.Forms.DockStyle.Left;
            this.sharj_tarif_doreh_sodoor_ghabz.DragDropSupport = true;
            this.sharj_tarif_doreh_sodoor_ghabz.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem27});
            this.sharj_tarif_doreh_sodoor_ghabz.Location = new System.Drawing.Point(202, 0);
            this.sharj_tarif_doreh_sodoor_ghabz.Name = "sharj_tarif_doreh_sodoor_ghabz";
            this.sharj_tarif_doreh_sodoor_ghabz.Size = new System.Drawing.Size(186, 97);
            this.sharj_tarif_doreh_sodoor_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.sharj_tarif_doreh_sodoor_ghabz.TabIndex = 12;
            this.sharj_tarif_doreh_sodoor_ghabz.Text = "مدیریت دوره های شارژ";
            // 
            // 
            // 
            this.sharj_tarif_doreh_sodoor_ghabz.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharj_tarif_doreh_sodoor_ghabz.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem27
            // 
            this.buttonItem27.FixedSize = new System.Drawing.Size(180, 80);
            this.buttonItem27.Image = global::CWMS.Properties.Resources.Network_Drive_Connected;
            this.buttonItem27.ImageFixedSize = new System.Drawing.Size(50, 40);
            this.buttonItem27.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem27.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.buttonItem27.Name = "buttonItem27";
            this.buttonItem27.Text = "تعریف دوره  - صدور قبض ";
            this.buttonItem27.Click += new System.EventHandler(this.buttonItem27_Click_2);
            // 
            // sharjMoshahedePanel
            // 
            this.sharjMoshahedePanel.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.sharjMoshahedePanel.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharjMoshahedePanel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.sharjMoshahedePanel.ContainerControlProcessDialogKey = true;
            this.sharjMoshahedePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sharjMoshahedePanel.DragDropSupport = true;
            this.sharjMoshahedePanel.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer26,
            this.itemContainer27});
            this.sharjMoshahedePanel.Location = new System.Drawing.Point(3, 0);
            this.sharjMoshahedePanel.Name = "sharjMoshahedePanel";
            this.sharjMoshahedePanel.Size = new System.Drawing.Size(199, 97);
            this.sharjMoshahedePanel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.sharjMoshahedePanel.TabIndex = 10;
            this.sharjMoshahedePanel.Text = "مشاهده قبوض شارژ";
            // 
            // 
            // 
            this.sharjMoshahedePanel.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.sharjMoshahedePanel.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // itemContainer26
            // 
            // 
            // 
            // 
            this.itemContainer26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer26.Name = "itemContainer26";
            this.itemContainer26.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.sharj_moshraek});
            // 
            // 
            // 
            this.itemContainer26.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // sharj_moshraek
            // 
            this.sharj_moshraek.FixedSize = new System.Drawing.Size(100, 70);
            this.sharj_moshraek.Image = global::CWMS.Properties.Resources.Live_Messenger;
            this.sharj_moshraek.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.sharj_moshraek.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.sharj_moshraek.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.sharj_moshraek.Name = "sharj_moshraek";
            this.sharj_moshraek.Text = "مشترک";
            this.sharj_moshraek.Click += new System.EventHandler(this.buttonItem36_Click_1);
            // 
            // itemContainer27
            // 
            // 
            // 
            // 
            this.itemContainer27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer27.FixedSize = new System.Drawing.Size(100, 0);
            this.itemContainer27.Name = "itemContainer27";
            this.itemContainer27.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.sharj_doreh});
            // 
            // 
            // 
            this.itemContainer27.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // sharj_doreh
            // 
            this.sharj_doreh.FixedSize = new System.Drawing.Size(100, 70);
            this.sharj_doreh.Image = global::CWMS.Properties.Resources.OS_Ubuntu;
            this.sharj_doreh.ImageFixedSize = new System.Drawing.Size(50, 50);
            this.sharj_doreh.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.sharj_doreh.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.sharj_doreh.Name = "sharj_doreh";
            this.sharj_doreh.Text = "دوره";
            this.sharj_doreh.Click += new System.EventHandler(this.buttonItem37_Click);
            // 
            // buttonFile
            // 
            this.buttonFile.AutoExpandOnClick = true;
            this.buttonFile.CanCustomize = false;
            this.buttonFile.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.buttonFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonFile.Image")));
            this.buttonFile.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.buttonFile.ImagePaddingHorizontal = 0;
            this.buttonFile.ImagePaddingVertical = 1;
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.ShowSubItems = false;
            this.buttonFile.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem42});
            this.buttonFile.Text = "آب و شارژ";
            this.buttonFile.Click += new System.EventHandler(this.buttonFile_Click);
            // 
            // buttonItem42
            // 
            this.buttonItem42.Name = "buttonItem42";
            this.buttonItem42.Text = "درباره برنامه";
            this.buttonItem42.Click += new System.EventHandler(this.buttonItem42_Click_2);
            // 
            // RbMoshtarekin
            // 
            this.RbMoshtarekin.Name = "RbMoshtarekin";
            this.RbMoshtarekin.Panel = this.ribbonPanel4;
            this.RbMoshtarekin.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl1);
            this.RbMoshtarekin.Text = "مشترکین";
            this.RbMoshtarekin.Click += new System.EventHandler(this.RbMoshtarekin_Click);
            // 
            // RbSharj
            // 
            this.RbSharj.Name = "RbSharj";
            this.RbSharj.Panel = this.ribbonPanel2;
            this.RbSharj.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl2);
            this.RbSharj.Text = "امور شارژ";
            // 
            // RbAb
            // 
            this.RbAb.Name = "RbAb";
            this.RbAb.Panel = this.ribbonPanel3;
            this.RbAb.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl3);
            this.RbAb.Text = "امور آب";
            this.RbAb.Click += new System.EventHandler(this.RbAb_Click);
            // 
            // RbBargehKhorooj
            // 
            this.RbBargehKhorooj.Name = "RbBargehKhorooj";
            this.RbBargehKhorooj.Panel = this.ribbonPanel6;
            this.RbBargehKhorooj.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl4);
            this.RbBargehKhorooj.Text = "برگه خروج";
            this.RbBargehKhorooj.Click += new System.EventHandler(this.RbBargehKhorooj_Click);
            // 
            // RbInternet
            // 
            this.RbInternet.Name = "RbInternet";
            this.RbInternet.Panel = this.ribbonPanel5;
            this.RbInternet.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl5);
            this.RbInternet.Text = "اینترنت و موبایل";
            // 
            // RbShahrak
            // 
            this.RbShahrak.Checked = true;
            this.RbShahrak.Name = "RbShahrak";
            this.RbShahrak.Panel = this.ribbonPanel7;
            this.RbShahrak.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl6);
            this.RbShahrak.Text = "مدیریت شهرک";
            this.RbShahrak.Click += new System.EventHandler(this.ribbonTabItem6_Click);
            // 
            // RbGozareshModiriat
            // 
            this.RbGozareshModiriat.Name = "RbGozareshModiriat";
            this.RbGozareshModiriat.Panel = this.ribbonPanel8;
            this.RbGozareshModiriat.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Ctrl7);
            this.RbGozareshModiriat.Text = "گزارشات مدیریت";
            // 
            // Rb_hesabdari
            // 
            this.Rb_hesabdari.Name = "Rb_hesabdari";
            this.Rb_hesabdari.Panel = this.ribbonPanel1;
            this.Rb_hesabdari.Text = "گزارشات حسابداری";
            // 
            // galleryContainer56
            // 
            // 
            // 
            // 
            this.galleryContainer56.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.galleryContainer56.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.galleryContainer56.EnableGalleryPopup = false;
            this.galleryContainer56.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.galleryContainer56.MinimumSize = new System.Drawing.Size(180, 240);
            this.galleryContainer56.MultiLine = false;
            this.galleryContainer56.Name = "galleryContainer56";
            this.galleryContainer56.PopupUsesStandardScrollbars = false;
            // 
            // 
            // 
            this.galleryContainer56.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // labelItem6
            // 
            this.labelItem6.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem6.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem6.CanCustomize = false;
            this.labelItem6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelItem6.Name = "labelItem6";
            // 
            // FrmMain
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1370, 721);
            this.Controls.Add(this.grpLogin);
            this.Controls.Add(this.grp_karkhaneh);
            this.Controls.Add(this.grp_doreh);
            this.Controls.Add(this.bar2);
            this.Controls.Add(this.ribbonControl1);
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سیستم مدیریت آب و شارژ شهرک صنعتی";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).EndInit();
            this.grp_doreh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grp_karkhaneh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.grp_pardakht_ab.ResumeLayout(false);
            this.grp_pardakht_ab.PerformLayout();
            this.grp_pardakht_sharj.ResumeLayout(false);
            this.grp_pardakht_sharj.PerformLayout();
            this.grp_ab.ResumeLayout(false);
            this.grp_ab.PerformLayout();
            this.grpMoshtarek.ResumeLayout(false);
            this.grp_sharj.ResumeLayout(false);
            this.grp_sharj.PerformLayout();
            this.superTabControlPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNOtofication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminnotificationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.grpLogin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel8.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.ribbonPanel6.ResumeLayout(false);
            this.ribbonPanel7.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.Editors.ComboItem comboItem10;
        private DevComponents.Editors.ComboItem comboItem11;
        private DevComponents.Editors.ComboItem comboItem12;
        private DevComponents.Editors.ComboItem comboItem13;
        private DevComponents.Editors.ComboItem comboItem14;
        private DevComponents.Editors.ComboItem comboItem15;
        private DevComponents.Editors.ComboItem comboItem16;
        private DevComponents.Editors.ComboItem comboItem17;
        private DevComponents.Editors.ComboItem comboItem18;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel5;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.Office2007StartButton buttonFile;
        private DevComponents.DotNetBar.RibbonTabItem RbAb;
        private DevComponents.DotNetBar.RibbonTabItemGroup ribbonTabItemGroup1;
        private DevComponents.DotNetBar.RibbonTabItem RbMoshtarekin;
        private DevComponents.DotNetBar.RibbonTabItem RbInternet;
        private DevComponents.DotNetBar.Bar bar2;
        private DevComponents.DotNetBar.LabelItem labelStatus;
        private DevComponents.DotNetBar.ItemContainer itemContainer14;
        private DevComponents.DotNetBar.ItemContainer itemContainer13;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ButtonItem buttonItem15;
        private DevComponents.DotNetBar.ButtonItem buttonItem14;
        private DevComponents.DotNetBar.ProgressBarItem progressBarItem1;
        internal DevComponents.DotNetBar.LabelItem labelPosition;
        private DevComponents.DotNetBar.LabelItem footer_time;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_doreh;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_karkhaneh;
        private DevComponents.DotNetBar.LabelX lbl_end_time;
        private DevComponents.DotNetBar.LabelX lbl_start_time;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lbl_doreh_sharj;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.LabelX lbl_mohlat;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX lbl_day_ta_doreh_baad;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ButtonItem buttonItem62;
        private DevComponents.DotNetBar.ButtonItem buttonItem64;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem buttonItem16;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_find_company;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_moshtarek;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX33;
        private DevComponents.DotNetBar.LabelX labelX34;
        private DevComponents.DotNetBar.LabelX Panel_lbl_ab_day_doreh_baad;
        private DevComponents.DotNetBar.LabelX Panel_lbl_ab_endtime;
        private DevComponents.DotNetBar.LabelX labelX37;
        private DevComponents.DotNetBar.LabelX labelX38;
        private DevComponents.DotNetBar.LabelX Panel_lbl_ab_doreh;
        private DevComponents.DotNetBar.LabelX labelX40;
        private DevComponents.DotNetBar.LabelX labelX41;
        private DevComponents.DotNetBar.LabelX Panel_lbl_ab_mohalat;
        private DevComponents.DotNetBar.LabelX Panel_lbl_ab_Start;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer6;
        private DevComponents.DotNetBar.ItemContainer itemContainer7;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer3;
        private DevComponents.DotNetBar.ItemContainer itemContainer8;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer4;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer5;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer6;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer7;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer8;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer9;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer10;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer11;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer12;
        private DevComponents.DotNetBar.ButtonItem buttonItem17;
        private DevComponents.DotNetBar.ButtonItem buttonItem18;
        private DevComponents.DotNetBar.ButtonItem buttonItem19;
        private DevComponents.DotNetBar.ButtonItem buttonItem20;
        private DevComponents.DotNetBar.Command command1;
        private DevComponents.DotNetBar.Command All_ghabz_sharj;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer13;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer14;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer15;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer16;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer17;
        private DevComponents.DotNetBar.Command New_sharj_ghabz;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer18;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer19;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer20;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer21;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer22;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_sharj;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_ab;
        private DevComponents.DotNetBar.ItemContainer itemContainer19;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_moshahedeh;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_sabte_moshtarek_jadidi;
        private DevComponents.DotNetBar.ItemContainer itemContainer9;
        private DevComponents.DotNetBar.ButtonItem q;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer23;
        private DevComponents.DotNetBar.ButtonItem q3;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer24;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer25;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer26;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer27;
        private DevComponents.DotNetBar.Metro.MetroTileItem metroTileItem5;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer28;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer29;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel6;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonTabItem RbSharj;
        private DevComponents.DotNetBar.RibbonTabItem RbBargehKhorooj;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer30;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer31;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer32;
        private DevComponents.DotNetBar.RibbonBar sharjMoshahedePanel;
        private DevComponents.DotNetBar.ItemContainer itemContainer26;
        private DevComponents.DotNetBar.ButtonItem sharj_moshraek;
        private DevComponents.DotNetBar.ItemContainer itemContainer27;
        private DevComponents.DotNetBar.ButtonItem sharj_doreh;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer33;
        private DevComponents.DotNetBar.RibbonBar sharj_tarif_doreh_sodoor_ghabz;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer34;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer35;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel7;
        private DevComponents.DotNetBar.RibbonTabItem RbShahrak;
        private DevComponents.DotNetBar.RibbonBar modiriat_peikarbandi;
        private DevComponents.DotNetBar.ItemContainer itemContainer28;
        private DevComponents.DotNetBar.RibbonBar sharjTanzimatPanel;
        private DevComponents.DotNetBar.ItemContainer itemContainer29;
        private DevComponents.DotNetBar.ButtonItem sharj_tanzimat;
        private DevComponents.DotNetBar.RibbonBar bargeh_gheir_faal;
        private DevComponents.DotNetBar.ItemContainer itemContainer31;
        private DevComponents.DotNetBar.ButtonItem bargeh_gheir_faal1;
        private DevComponents.DotNetBar.RibbonBar barge1panel;
        private DevComponents.DotNetBar.ItemContainer itemContainer17;
        private DevComponents.DotNetBar.ButtonItem bargeh_moshahede_darkhast;
        private DevComponents.DotNetBar.RibbonBar abTanzimatPanel;
        private DevComponents.DotNetBar.RibbonBar ab_doreh_sodoor_ghabz;
        private DevComponents.DotNetBar.ButtonItem ab_doreh_sodoor_ghabz1;
        private DevComponents.DotNetBar.RibbonBar abMoshahedePanel;
        private DevComponents.DotNetBar.ItemContainer itemContainer11;
        private DevComponents.DotNetBar.ButtonItem ab_moshtarek;
        private DevComponents.DotNetBar.ItemContainer itemContainer32;
        private DevComponents.DotNetBar.ButtonItem ab_doreh;
        private DevComponents.DotNetBar.Command barge;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer36;
        private DevComponents.DotNetBar.LabelItem labelItem8;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvNOtofication;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpMoshtarek;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_address;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_phone;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_modir_amel;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_name;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_code;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_sharj;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.LabelX lbl_moshtarek_mablgh_kol_str;
        private DevComponents.DotNetBar.LabelX lbl_sharj_vaziat;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource adminnotificationBindingSource;
        private MainDataSestTableAdapters.admin_notificationTableAdapter admin_notificationTableAdapter;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX lblnoetefagh;
        private System.Windows.Forms.DataGridViewButtonColumn chekecd;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn subjectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn matnDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.ItemContainer itemContainer36;
        private DevComponents.DotNetBar.ButtonItem ab_ababaha;
        private DevComponents.DotNetBar.ItemContainer itemContainer37;
        private DevComponents.DotNetBar.ButtonItem sharj_tarefe;
        private DevComponents.DotNetBar.ItemContainer itemContainer38;
        private DevComponents.DotNetBar.ButtonItem ab_aboonman;
        private DevComponents.DotNetBar.ItemContainer itemContainer5;
        private DevComponents.DotNetBar.ButtonItem ab_tanzimat;
        private DevComponents.DotNetBar.Command ErsalPayam;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer37;
        private DevComponents.DotNetBar.ItemContainer itemContainer39;
        private DevComponents.DotNetBar.ButtonItem ab_zarayeb;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel8;
        private DevComponents.DotNetBar.RibbonBar gozarehat_tarefe_zarayeb;
        private DevComponents.DotNetBar.ItemContainer itemContainer45;
        private DevComponents.DotNetBar.ButtonItem gozarehat_tarefe_zarayeb1;
        private DevComponents.DotNetBar.RibbonBar gozarehat_ab_sharj;
        private DevComponents.DotNetBar.ItemContainer gozarehat_ab_sharj1;
        private DevComponents.DotNetBar.ButtonItem سیس;
        private DevComponents.DotNetBar.RibbonTabItem RbGozareshModiriat;
        private DevComponents.DotNetBar.RibbonBar gozarehat_check;
        private DevComponents.DotNetBar.ItemContainer itemContainer47;
        private DevComponents.DotNetBar.ButtonItem gozarehat_check1;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer38;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer39;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer40;
        private DevComponents.DotNetBar.RibbonBar modiriat_backup;
        private DevComponents.DotNetBar.ButtonItem buttonItem67;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer41;
        private DevComponents.DotNetBar.RibbonBar InternetMobikePanel;
        private DevComponents.DotNetBar.ItemContainer itemContainer10;
        private DevComponents.DotNetBar.ButtonItem internet_gozareshat_etesal_mobile;
        private DevComponents.DotNetBar.ItemContainer itemContainer48;
        private DevComponents.DotNetBar.ButtonItem internet_moshahede_key;
        private DevComponents.DotNetBar.ItemContainer itemContainer51;
        private DevComponents.DotNetBar.ButtonItem internet_afzoodan_key;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer42;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer43;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_afzoodan_key;
        private DevComponents.DotNetBar.ItemContainer itemContainer55;
        private DevComponents.DotNetBar.ButtonItem moshtarekin_afzoodan_key1;
        private DevComponents.DotNetBar.ItemContainer itemContainer52;
        private DevComponents.DotNetBar.ButtonItem buttonItem73;
        private DevComponents.DotNetBar.ItemContainer itemContainer53;
        private DevComponents.DotNetBar.ButtonItem buttonItem83;
        private DevComponents.DotNetBar.ItemContainer itemContainer54;
        private DevComponents.DotNetBar.ButtonItem buttonItem84;
        private DevComponents.DotNetBar.ItemContainer itemContainer15;
        private DevComponents.DotNetBar.ButtonItem q1;
        private DevComponents.DotNetBar.RibbonBar shahrakKKarbaranPanel;
        private DevComponents.DotNetBar.ItemContainer itemContainer16;
        private DevComponents.DotNetBar.ButtonItem modiriat_admin_users;
        private DevComponents.DotNetBar.ItemContainer itemContainer18;
        private DevComponents.DotNetBar.ButtonItem modiriat_gozareshat_dastarsi;
        private DevComponents.DotNetBar.RibbonBar modiriat_etelaieh;
        private DevComponents.DotNetBar.ButtonItem modiriat_etelaieh1;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer44;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer45;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer46;
        private DevComponents.DotNetBar.ItemContainer itemContainer23;
        private DevComponents.DotNetBar.ButtonItem bargeh_sodoor;
        private DevComponents.DotNetBar.ItemContainer itemContainer21;
        private DevComponents.DotNetBar.ButtonItem buttonItem25;
        private DevComponents.DotNetBar.ItemContainer itemContainer22;
        private DevComponents.DotNetBar.ButtonItem buttonItem26;
        private DevComponents.DotNetBar.ButtonItem buttonItem27;
        private DevComponents.DotNetBar.LabelX lbl_sharj_gcode;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.ButtonX btn_show_Sharj_Ghabz;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_ab;
        private DevComponents.DotNetBar.LabelX lbl_ab_gcode;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.ButtonX btn_show_ab_Ghabz;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX lbl_ab_mablaghkol_horoof;
        private DevComponents.DotNetBar.LabelX lbl_ab_vaziat;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer47;
        private DevComponents.DotNetBar.ButtonX btn_print_ab_Ghabz;
        private DevComponents.DotNetBar.ButtonX btn_print_Sharj_Ghabz;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer48;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonX btnFindByPelak;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPelak;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtElhaghieh;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer49;
        private DevComponents.DotNetBar.RibbonBar moshtarekin_JostejooInSavabegh;
        private DevComponents.DotNetBar.ItemContainer itemContainer20;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer50;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem tabusergozaresh;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ItemContainer itemContainer12;
        private DevComponents.DotNetBar.ButtonItem modiriat_bedehi_sharj;
        
        
        private DevComponents.DotNetBar.RibbonBar moshtarekin_barge_darkhast;
        private DevComponents.DotNetBar.ItemContainer itemContainer24;
        private DevComponents.DotNetBar.ButtonItem buttonItem23;
        private DevComponents.DotNetBar.RibbonBar modiriat_setting;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.RibbonBar modiriat_tayin_vaziat_sodoor;
        private DevComponents.DotNetBar.ButtonItem buttonItem28;
        private DevComponents.DotNetBar.RibbonBar ab_history;
        private DevComponents.DotNetBar.ButtonItem buttonItem29;
        private DevComponents.DotNetBar.ButtonItem modiriat_bedehi_ab;
        private DevComponents.DotNetBar.LabelItem HourLabel;
        private DevComponents.DotNetBar.ItemContainer itemContainer25;
        private DevComponents.DotNetBar.RibbonBar ab_jarmieh;
        private DevComponents.DotNetBar.ButtonItem buttonItem30;
        private DevComponents.DotNetBar.ButtonX btn_pardakht_ab;
        private DevComponents.DotNetBar.ButtonX btn_pardakht_sharj;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_pardakht_sharj;
        private DevComponents.DotNetBar.LabelX labelX35;
        
        private DevComponents.DotNetBar.ButtonX btn_sharj_new_pardakht;
        private DevComponents.DotNetBar.LabelX labelX39;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_pardakht_ab;
        private DevComponents.DotNetBar.LabelX labelX44;
        
        private DevComponents.DotNetBar.ButtonX btn_pardakht_ab_now;
        private DevComponents.DotNetBar.LabelX labelX46;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rd_ab_bank;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rd_sharj_bank;
        private System.Windows.Forms.TextBox txt_ab_new_tarikh;
        private System.Windows.Forms.TextBox txt_sharj_new_tarikh;
        private DevComponents.DotNetBar.ButtonX btn_new_sharj_ghabz;
        private DevComponents.DotNetBar.ButtonX btn_new_ab_ghabz;
        private DevComponents.DotNetBar.ItemContainer gozarehat_ab_sharj_Pardakhti;
        private DevComponents.DotNetBar.ButtonItem buttonItem24;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer51;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.LabelX labelX13;
        
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer52;
        private DevComponents.DotNetBar.LabelItem labelItem2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private System.Windows.Forms.ComboBox cmbAbEnd;
        private System.Windows.Forms.ComboBox cmbabStart;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private System.Windows.Forms.ComboBox cmbsharjEnd;
        private System.Windows.Forms.ComboBox cmbsharjStart;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.ButtonItem buttonItem31;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer53;
        private DevComponents.DotNetBar.LabelItem labelItem3;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer54;
        private DevComponents.DotNetBar.LabelItem labelItem4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMobile;
        private DevComponents.DotNetBar.LabelX labelX36;
        private DevComponents.DotNetBar.RibbonBar ribbonBar3;
        private DevComponents.DotNetBar.ItemContainer itemContainer30;
        private DevComponents.DotNetBar.ButtonItem buttonItem33;
        private DevComponents.DotNetBar.ItemContainer itemContainer33;
        private DevComponents.DotNetBar.ButtonItem buttonItem34;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ItemContainer itemContainer43;
        private DevComponents.DotNetBar.ButtonItem buttonItem37;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar9;
        private DevComponents.DotNetBar.ItemContainer itemContainer56;
        private DevComponents.DotNetBar.ButtonItem buttonItem44;
        private DevComponents.DotNetBar.RibbonTabItem Rb_hesabdari;
        private DevComponents.DotNetBar.RibbonBar ribbonBar4;
        private DevComponents.DotNetBar.ItemContainer itemContainer34;
        private DevComponents.DotNetBar.ButtonItem buttonItem32;
        private DevComponents.DotNetBar.ItemContainer itemContainer40;
        private DevComponents.DotNetBar.ButtonItem buttonItem35;
        private DevComponents.DotNetBar.ItemContainer itemContainer41;
        private DevComponents.DotNetBar.ButtonItem buttonItem36;
        private DevComponents.DotNetBar.RibbonBar ribbonBar7;
        private DevComponents.DotNetBar.ButtonItem buttonItem39;
        private DevComponents.DotNetBar.ItemContainer itemContainer44;
        private DevComponents.DotNetBar.ButtonItem fd;
        private DevComponents.DotNetBar.ItemContainer itemContainer49;
        private DevComponents.DotNetBar.ButtonItem buttonItem41;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.RibbonBar ribbonBar6;
        private DevComponents.DotNetBar.ButtonItem buttonItem21;
        private DevComponents.DotNetBar.LabelX labelX29;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUsername;
        private DevComponents.DotNetBar.Controls.TextBoxX txtpassword;
        private DevComponents.DotNetBar.LabelX labelX31;
        private DevComponents.DotNetBar.LabelX labelX32;
        private DevComponents.DotNetBar.ButtonX btnLogin;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private DevComponents.DotNetBar.Controls.GroupPanel grpLogin;
        private TextBoxtest.TxtProNet lbl_moshtarek_mablagh_kol;
        private TextBoxtest.TxtProNet txt_sharj_bestnkari;
        private TextBoxtest.TxtProNet lbl_sharj_mandeh;
        private TextBoxtest.TxtProNet lbl_sharj_pardakhti;
        private TextBoxtest.TxtProNet lbl_ab_mablagh_kol;
        private TextBoxtest.TxtProNet txt_ab_bestankari;
        private TextBoxtest.TxtProNet lbl_ab_mandeh;
        private TextBoxtest.TxtProNet lbl_ab_pardakhti;
        private DevComponents.DotNetBar.RibbonBar ribbonBar8;
        private DevComponents.DotNetBar.ButtonItem buttonItem22;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer55;
        private DevComponents.DotNetBar.LabelItem labelItem5;
        private TextBoxtest.TxtProNet txt_ab_new_pardakht;
        private TextBoxtest.TxtProNet txt_sharj_new_pardakht;
        private DevComponents.DotNetBar.RibbonBar ribbonBar11;
        private DevComponents.DotNetBar.ItemContainer itemContainer46;
        private DevComponents.DotNetBar.ButtonItem buttonItem40;
        public  System.Windows.Forms.ProgressBar Main_Progress;
        private DevComponents.DotNetBar.GalleryContainer galleryContainer56;
        private DevComponents.DotNetBar.LabelItem labelItem6;
        private DevComponents.DotNetBar.RibbonBar ribbonBar10;
        private DevComponents.DotNetBar.ItemContainer itemContainer35;
        private DevComponents.DotNetBar.ButtonItem buttonItem38;
        private DevComponents.DotNetBar.ButtonItem buttonItem42;
        private DevComponents.DotNetBar.RibbonBar ribbonBar12;
        private DevComponents.DotNetBar.ItemContainer itemContainer42;
        private DevComponents.DotNetBar.ButtonItem buttonItem43;
        private DevComponents.DotNetBar.ItemContainer itemContainer50;
        private DevComponents.DotNetBar.ButtonItem buttonItem45;
    }
}