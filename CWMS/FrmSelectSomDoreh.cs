﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class FrmSelectSomDoreh : DevComponents.DotNetBar.Metro.MetroForm
    {
        Rpt_Sharj_Ghobooz_PN frm;
        public FrmSelectSomDoreh(Rpt_Sharj_Ghobooz_PN f)
        {
            frm = f;
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
        }

        private void FrmSelectSomDoreh_Load(object sender, EventArgs e)
        {

        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Contains(cmbDoreh.Text) == true)
                Payam.Show("این دوره قبلا به لیست اضافه شده است");
            else
                listBox1.Items.Add(cmbDoreh.Text);
        }

        private void comboBoxEx1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0)
                btn_add.Enabled = false;
            else
                btn_add.Enabled = true;

        }

        private void btn_al_Click(object sender, EventArgs e)
        {
            string res = "";
            for (int i = 0; i < listBox1.Items.Count; i++)
                res += listBox1.Items[i].ToString().Trim() + ",";
            res= res.Remove(res.Length - 1);
            frm.SetselectedDoreh(res);
            this.Close();
            
        }
    }
}