﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMS
{
    public class MyMetroForm : DevComponents.DotNetBar.Metro.MetroForm
    {
        public static FrmMain frmMainRefrence = null;

        public MyMetroForm()
            : base()
        {
            //this.TopMost = true;
            this.Owner = frmMainRefrence;
        }


        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Classes.ClsMain.closeForm(this.Name);
            base.OnClosing(e);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MyMetroForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MyMetroForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MyMetroForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MyMetroForm_KeyDown);
            this.ResumeLayout(false);

        }

        private void MyMetroForm_Load(object sender, EventArgs e)
        {

        }

        private void MyMetroForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Escape)
                this.Close();
        }
    }
}
