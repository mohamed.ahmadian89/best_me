﻿using DevComponents.DotNetBar.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CWMS
{
    class MoneyTextBox : TextBoxX
    {
        [Bindable(true)]
        public override string Text
        {
            set
            {
                base.Text = value;
            }
            get
            {
                if (String.IsNullOrEmpty(base.Text) || base.Text == "0")
                    return base.Text;
                return skipComma(base.Text);
            }
        }

        public string skipComma(string str)
        {
            string strnew = "";
            if (str == "")
            {
                strnew = "0";
            }
            else
            {
                strnew = str.Replace(",", String.Empty);
            }
            return strnew;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (base.Text == "")
            {
                this.Text = "0";
            }
            else
            {
                if (this.Text != "")
                {
                    double d;
                    if (!Double.TryParse(this.Text, out d))
                    {
                        this.Text = null;
                        return;
                    }
                    if (d == 0)
                    {
                        this.Text = "0";
                    }
                    else
                        this.Text = d.ToString("#,#", System.Globalization.CultureInfo.InvariantCulture);
                }
            }
            this.Select(this.TextLength, 0);
        }





        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Control & e.KeyCode == Keys.A)
                this.SelectAll();
            base.OnKeyDown(e);
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            this.SelectAll();

            base.OnMouseClick(e);
        }
        
    }
}