﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS
{
    public partial class Frm_errors : Form
    {
        public Frm_errors()
        {
            InitializeComponent();
        }

        private void Frm_errors_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Classes.ClsMain.GetDataTable("select top(100) errorText from errors order by id desc");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ExecuteNoneQuery("delete from errors");
        }

        private void txt_code_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                dgv_result.DataSource = Classes.ClsMain.GetDataTable(txt_code.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Classes.ClsMain.GetDataTable("select top(100) * from errors order by id desc");

        }
    }
}
