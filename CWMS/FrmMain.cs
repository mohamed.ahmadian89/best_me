﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using FarsiLibrary.Utils;
using FarsiLibrary.Win;
using System.Data;
using CWMS.Classes;
using CrystalDecisions.Windows.Forms;
using System.Globalization;

namespace CWMS
{

    public partial class FrmMain : DevComponents.DotNetBar.RibbonForm
    {

        //----------------------------------------------------------

        //----------------------------------------------------------


        public FrmMain()
        {
            InitializeComponent();
        }

        void FirstTimeWizard()
        {
            DataTable Dt_Setting = ClsMain.GetDataTable("select * from setting");
            if (Dt_Setting.Rows.Count == 0)
            {
               
                Payam.Show("برنامه هم اکنون دوباره راه اندازی می شود");
                Application.Restart();
            }
            else
            {
                if (Convert.ToBoolean(Dt_Setting.Rows[0]["configured"]) == false)
                {
                   
                    Payam.Show("برنامه هم اکنون دوباره راه اندازی می شود");
                    Application.Restart();
                }
            }

        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            try
            {
                CrysReports.ReportDataSetTableAdapters.settingTableAdapter
                sa = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
                CrysReports.ReportDataSet.settingDataTable dt = new CrysReports.ReportDataSet.settingDataTable();
                sa.Fill(dt);
                ClsMain.ShahrakSetting = (CrysReports.ReportDataSet.settingRow)dt.Rows[0];
                ClsMain.ShahrakSetting_data_table = dt;
                txt_sharj_new_tarikh.Text = txt_ab_new_tarikh.Text = new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d");
                txt_find_moshtarek.Focus();


                if (Classes.ClsMain.ShahrakSetting.shahrak_ID != Classes.Shahrak_IDS.abbas_abad)
                    ClsMain.SherkartCode_MelliAccount = "111";

                //FarsiLibrary.Utils.PersianDate p = new PersianDate(DateTime.Now);
                //if (p.Day > 18)
                //{
                //    Payam.Show("پایان کاربری دمو. لطفا با کارشناسان نگاه تماس حاصل فرمایید");
                //    Classes.ClsMain.ExecuteNoneQuery("EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL' ;EXEC sp_MSForEachTable 'DELETE FROM ?'; EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL' ");
                //    Application.Exit();
                //}





                //txtUsername.Text = "a";
                //txtpassword.Text = "aa";
                //btnLogin.PerformClick();
            }
            catch (Exception)
            {
                new FrmSetConnection().ShowDialog();
                Application.Exit();
                //Application.Restart();
            }





            //FirstTimeWizard();

            PersianDate pd = PersianDate.Now;
            footer_time.Text = PersianDate.Now.ToString("D");

            txt_moshtarek_code.Focus();
            Classes.ClsMain.FrmMainAccess = this;

            Classes.ClsMain.MoshtarekDT = new MainDataSest.moshtarekinDataTable();
            LastSearch = DateTime.Now;


            this.MinimumSize = this.Size;




        }
        void CheckLogin(string username, string password)
        {
            if (Classes.clsMoshtarekin.checkLoginInfo(username, password) == true)
            {

                txt_find_moshtarek.Enabled = true;
                txt_find_moshtarek.Focus();
                RbMoshtarekin.Visible = clsMoshtarekin.UserInfo.p93;
                //------------------------------------------------
                moshtarekin_afzoodan_key.Visible = clsMoshtarekin.UserInfo.p27;
                moshtarekin_sharj.Visible = clsMoshtarekin.UserInfo.p48;
                moshtarekin_moshahedeh.Visible = clsMoshtarekin.UserInfo.p2;
                moshtarekin_barge_darkhast.Visible = clsMoshtarekin.UserInfo.p20;
                moshtarekin_sabte_moshtarek_jadidi.Visible = clsMoshtarekin.UserInfo.p1;
                moshtarekin_ab.Visible = clsMoshtarekin.UserInfo.p3;


                //------------------------------------------------



                RbAb.Visible = clsMoshtarekin.UserInfo.p95;

                //------------------------------------------------
                ab_ababaha.Visible = clsMoshtarekin.UserInfo.p16;
                ab_aboonman.Visible = clsMoshtarekin.UserInfo.p17;
                ab_doreh.Visible = clsMoshtarekin.UserInfo.p14;
                ab_doreh_sodoor_ghabz.Visible = clsMoshtarekin.UserInfo.p15;
                ab_moshtarek.Visible = clsMoshtarekin.UserInfo.p13;
                ab_tanzimat.Visible = clsMoshtarekin.UserInfo.p18;
                ab_zarayeb.Visible = clsMoshtarekin.UserInfo.p19;

                if (ab_moshtarek.Visible == false && ab_doreh.Visible == false)
                    abMoshahedePanel.Visible = false;
                else
                    abMoshahedePanel.Visible = true;


                if (ab_tanzimat.Visible == false && ab_zarayeb.Visible == false && ab_ababaha.Visible == false && ab_aboonman.Visible == false)
                    abTanzimatPanel.Visible = false;
                else
                    abTanzimatPanel.Visible = true;

                //------------------------------------------------


                RbSharj.Visible = clsMoshtarekin.UserInfo.p94;
                //------------------------------------------------
                sharj_doreh.Visible = clsMoshtarekin.UserInfo.p9;
                sharj_moshraek.Visible = clsMoshtarekin.UserInfo.p8;
                sharj_tanzimat.Visible = clsMoshtarekin.UserInfo.p11;
                sharj_tarefe.Visible = clsMoshtarekin.UserInfo.p12;
                sharj_tarif_doreh_sodoor_ghabz.Visible = clsMoshtarekin.UserInfo.p10;


                if (sharj_doreh.Visible == false && sharj_moshraek.Visible == false)
                    sharjMoshahedePanel.Visible = false;
                else
                    sharjMoshahedePanel.Visible = true;


                if (sharj_tarefe.Visible == false && sharj_tanzimat.Visible == false)
                    sharjTanzimatPanel.Visible = false;
                else
                    sharjTanzimatPanel.Visible = true;


                //------------------------------------------------

                RbShahrak.Visible = clsMoshtarekin.UserInfo.p94;

                //------------------------------------------------
                modiriat_admin_users.Visible = clsMoshtarekin.UserInfo.p31;
                modiriat_etelaieh.Visible = clsMoshtarekin.UserInfo.p33;
                modiriat_gozareshat_dastarsi.Visible = clsMoshtarekin.UserInfo.p32;
                modiriat_backup.Visible = clsMoshtarekin.UserInfo.p30;
                modiriat_bedehi_ab.Visible = clsMoshtarekin.UserInfo.p28;
                modiriat_bedehi_sharj.Visible = clsMoshtarekin.UserInfo.p29;

                if (modiriat_admin_users.Visible == false && modiriat_gozareshat_dastarsi.Visible == false)
                    shahrakKKarbaranPanel.Visible = false;
                else
                    shahrakKKarbaranPanel.Visible = true;

                if (modiriat_bedehi_ab.Visible == false && modiriat_bedehi_ab.Visible == false)
                    modiriat_peikarbandi.Visible = false;
                else
                    modiriat_peikarbandi.Visible = true;

                //------------------------------------------------



                RbInternet.Visible = clsMoshtarekin.UserInfo.p97;
                //------------------------------------------------
                internet_afzoodan_key.Visible = clsMoshtarekin.UserInfo.p27;
                internet_gozareshat_etesal_mobile.Visible = clsMoshtarekin.UserInfo.p25;
                internet_moshahede_key.Visible = clsMoshtarekin.UserInfo.p26;

                if (internet_afzoodan_key.Visible == false && internet_gozareshat_etesal_mobile.Visible == false && internet_moshahede_key.Visible == false)
                    InternetMobikePanel.Visible = false;
                else
                    InternetMobikePanel.Visible = true;


                //------------------------------------------------

                RbGozareshModiriat.Visible = clsMoshtarekin.UserInfo.p99;
                //------------------------------------------------
                gozarehat_ab_sharj.Visible = clsMoshtarekin.UserInfo.p34;

                gozarehat_check.Visible = clsMoshtarekin.UserInfo.p39;
                //gozarehat_daramad_akhar.Visible = clsMoshtarekin.UserInfo.p36;

                gozarehat_tarefe_zarayeb.Visible = clsMoshtarekin.UserInfo.p38;

                Rb_hesabdari.Visible = clsMoshtarekin.UserInfo.p107;




                //if (gozarehat_daramad_akhar.Visible == false && gdf.Visible == false)
                //    gozareshat1panel.Visible = false;
                //else
                //    gozareshat1panel.Visible = true;



                //------------------------------------------------



                RbBargehKhorooj.Visible = clsMoshtarekin.UserInfo.p96;
                //------------------------------------------------
                bargeh_gheir_faal.Visible = clsMoshtarekin.UserInfo.p23;

                bargeh_moshahede_darkhast.Visible = clsMoshtarekin.UserInfo.p20;
                bargeh_sodoor.Visible = clsMoshtarekin.UserInfo.p21;

                if (bargeh_sodoor.Visible == false && bargeh_moshahede_darkhast.Visible == false)
                    barge1panel.Visible = false;
                else
                    barge1panel.Visible = true;


                //------------------------------------------------


                modiriat_setting.Visible = clsMoshtarekin.UserInfo.p100;
                modiriat_tayin_vaziat_sodoor.Visible = clsMoshtarekin.UserInfo.p101;


                ab_history.Visible = clsMoshtarekin.UserInfo.p102;

                ab_jarmieh.Visible = clsMoshtarekin.UserInfo.p103;











                ribbonControl1.Enabled = true;
                grpLogin.Visible = false;

                if (StartUPActionsIsDon)
                {
                    grp_doreh.Visible = true;
                    grp_karkhaneh.Visible = true;
                }


                ribbonControl1.Refresh();

                btnFindByPelak.Enabled = btn_find_company.Enabled = true;
            }
            else
            {
                Payam.Show("نام کاربری و رمز عبور وارد شده صحیح نمی باشد");
                txtUsername.SelectAll();
                txtpassword.SelectAll();
                txtUsername.Focus();
                ribbonControl1.Enabled = false;
                grpLogin.Visible = true;
                btnFindByPelak.Enabled = btn_find_company.Enabled = false;
            }
        }

        private void btn_create_Click(object sender, EventArgs e)
        {


        }

        public void UpdateDorehPanel()
        {

            DataTable dt_sharj = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh");
            DataTable dt_ab = Classes.ClsMain.GetDataTable("select dcode from ab_doreh");

            cmbabStart.Items.Clear();
            cmbAbEnd.Items.Clear();

            cmbsharjStart.Items.Clear();
            cmbsharjEnd.Items.Clear();



            for (int i = 0; i < dt_sharj.Rows.Count; i++)
            {
                cmbsharjStart.Items.Add(dt_sharj.Rows[dt_sharj.Rows.Count - 1 - i][0].ToString());
                cmbsharjEnd.Items.Add(dt_sharj.Rows[i][0].ToString());

            }

            for (int i = 0; i < dt_ab.Rows.Count; i++)
            {
                cmbabStart.Items.Add(dt_ab.Rows[dt_ab.Rows.Count - 1 - i][0].ToString());
                cmbAbEnd.Items.Add(dt_ab.Rows[i][0].ToString());

            }
            if (cmbAbEnd.Items.Count != 0)
                cmbAbEnd.SelectedIndex = cmbabStart.SelectedIndex = 0;

            if (cmbsharjEnd.Items.Count != 0)
                cmbsharjEnd.SelectedIndex = cmbsharjStart.SelectedIndex = 0;

            DataTable dt_info = Classes.clsDoreh.Get_last_Sharj_Doreh();
            if (dt_info.Rows.Count != 0)
            {
                DataRow CurrentDorehInfo = dt_info.Rows[0];

                lbl_doreh_sharj.Text = CurrentDorehInfo["dcode"].ToString();
                lbl_start_time.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["start_time"])).ToString("d"); ;
                lbl_end_time.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["end_time"])).ToString("d");
                lbl_mohlat.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["mohlat_pardakht"])).ToString("d");
                lbl_day_ta_doreh_baad.Text = Classes.clsSharj.Get_day_ta_shoru_doreh_badd().ToString();

            }



            dt_info = Classes.clsDoreh.Get_last_Ab_Doreh();
            if (dt_info.Rows.Count != 0)
            {
                DataRow CurrentDorehInfo = dt_info.Rows[0];

                Panel_lbl_ab_doreh.Text = CurrentDorehInfo["dcode"].ToString();
                Panel_lbl_ab_Start.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["start_time"])).ToString("d"); ;
                Panel_lbl_ab_endtime.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["end_time"])).ToString("d");
                Panel_lbl_ab_mohalat.Text = PersianDateConverter.ToPersianDate(Convert.ToDateTime(CurrentDorehInfo["mohlat_pardakht"])).ToString("d");
                Panel_lbl_ab_day_doreh_baad.Text = Classes.clsAb.Get_day_ta_shoru_doreh_badd().ToString();
            }
            grp_karkhaneh.Visible = grp_doreh.Visible = true;


        }


        int Counter = 0;

        void Sharj_Operation()
        {
            int New_doreh_dcode = Classes.clsSharj.Check_new_sharj_doreh_should_created_and_return_NewDcode();
            if (New_doreh_dcode == -1)
            {
                Classes.clsAdmin.Notify("عدم صدور دوره جدید شارژ", "متاسفانه امکان بدست اوردن اخرین کد دوره شارژی وجود ندارد . لطفا هرچه سریعتر با شرکت پشتیبان هماهنگی لازم را انجام دهید", "0", "0");
            }

            //--- این بدان معناست که دوره جدید باید ایجاد شود دوره 
            else if (New_doreh_dcode != 0)
            {
                DataTable DtSharjInfo = Classes.clsDoreh.Get_Sharj_info();
                if (DtSharjInfo.Rows.Count == 0)
                {
                    Payam.Show("لطفا اطلاعات اولیه شارژ را وارد نمایید");
                    return;
                }
                if (Convert.ToBoolean(DtSharjInfo.Rows[0]["ejraye_automat"]))
                {

                    if (Cpayam.Show("کاربر گرامی ! باتوجه به تنظیمات سیستم ، امروز می بایست دوره جدید شارژ ایجاد شود، آیا موافقید؟") == DialogResult.Yes)
                    {
                        DataTable DtLastSharjDoreh = Classes.clsDoreh.Get_last_Sharj_Doreh();
                        DataTable DtSharjMablagh = Classes.clsSharj.Get_last_Sharj_mablagh_tasvib_shodeh();

                        if (DtSharjMablagh.Rows.Count == 0)
                        {
                            Payam.Show("متاسفانه هیچ مبلغ مصوب جهت تعیین هزینه شارژ بر اساس تاریخ سیستم شما تعریف نشده است .");
                            Payam.Show("لطفا به قسمت امور شارژ، تعیین مبالغ شارژ مراجعه نمایید و پس از ان دوره را به صورت دستی ایجاد نمایید");
                        }
                        else
                        {


                            DateTime NewDoreh_StartTime = DateTime.Now, EndTime = DateTime.Now, Mohlat = DateTime.Now;
                            if (DtLastSharjDoreh.Rows.Count == 0)
                                NewDoreh_StartTime = DateTime.Now;
                            else
                                NewDoreh_StartTime = Convert.ToDateTime(DtLastSharjDoreh.Rows[0]["end_time"]).AddDays(1);

                            //.. تاریخ پایان دوره فعلی ، تاریخ شروع دوره جدید می باشد

                            //----------- بدست اوردن بازه دوره بعدی 
                            Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(NewDoreh_StartTime, Convert.ToInt32(DtSharjInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtSharjInfo.Rows[0]["mohlat_pardakht"]));
                            //----------- بدست اوردن بازه دوره بعدی 
                            Classes.ClsMain.ChangeCulture("e");

                            Classes.clsSharj.Ijad_Doreh_jadid_sharj_V2(Convert.ToInt32(New_doreh_dcode), NewDoreh_StartTime, Doreh_length.End_Of_Doreh, Doreh_length.Mohlat_pardakht, Convert.ToSingle(DtSharjMablagh.Rows[0]["mablagh"]), DtSharjInfo.Rows[0]["darsad_maliat"].ToString(), DtSharjInfo.Rows[0]["ZaribTejari"].ToString());
                            Payam.Show("دوره جدید شارژ با موفقیت در سیستم ذخیره شد");

                            if (Cpayam.Show("آیا مایل به صدور قبوض شارژ این دوره جدید هستید؟") == DialogResult.Yes)
                            {

                                DateTime StartTime = DateTime.Now;
                                Classes.clsSharj sharjClass = new clsSharj();
                                string Result = sharjClass.Generate_Ghabz();
                                DateTime EndTime1 = DateTime.Now;
                                Classes.clsSharj.UpdateDorehInfoAtEndOfSodoorProcess(StartTime, EndTime1, New_doreh_dcode.ToString());




                            }
                            Classes.ClsMain.ChangeCulture("f");

                        }
                    }
                }


            }
        }
        void Ab_Operation()
        {
            int New_doreh_dcode = Classes.clsAb.Check_new_ab_doreh_should_created_and_return_NewDcode();
            if (New_doreh_dcode == -1)
            {
                Classes.clsAdmin.Notify("عدم صدور دوره جدید اب", "متاسفانه امکان بدست اوردن اخرین کد دوره آب وجود ندارد . لطفا هرچه سریعتر با شرکت پشتیبان هماهنگی لازم را انجام دهید", "0", "0");
            }

            //--- این بدان معناست که دوره جدید باید ایجاد شود دوره 
            else if (New_doreh_dcode != 0)
            {
                DataTable DtAbInfo = Classes.clsDoreh.Get_Ab_info();
                if (DtAbInfo.Rows.Count == 0)
                {
                    Payam.Show("لطفا اطلاعات اولیه آب را وارد نمایید");
                    return;
                }
                if (Convert.ToBoolean(DtAbInfo.Rows[0]["ejraye_automat"]))
                {
                    if (Cpayam.Show("کاربر گرامی ! باتوجه به تنظیمات سیستم ، امروز می بایست دوره جدید آب ایجاد شود، آیا موافقید؟") == DialogResult.Yes)
                    {
                        DataTable DtLastAbDoreh = Classes.clsDoreh.Get_last_Ab_Doreh();
                        DataTable DtAbMablagh = Classes.clsAb.Get_last_Ab_mablagh_tasvib_shodeh();

                        if (DtAbMablagh.Rows.Count == 0)
                        {
                            Payam.Show("متاسفانه هیچ مبلغ مصوب جهت تعیین هزینه آب بر اساس تاریخ سیستم شما تعریف نشده است .");
                            Payam.Show("لطفا به قسمت امور آب، تعیین مبالغ اب مراجعه نمایید و پس از ان دوره را به صورت دستی ایجاد نمایید");
                        }
                        else
                        {
                            DateTime NewDoreh_StartTime = DateTime.Now, EndTime = DateTime.Now, Mohlat = DateTime.Now;
                            if (DtLastAbDoreh.Rows.Count == 0)
                                NewDoreh_StartTime = DateTime.Now;
                            else
                                NewDoreh_StartTime = Convert.ToDateTime(DtLastAbDoreh.Rows[0]["end_time"]);
                            //.. تاریخ پایان دوره فعلی ، تاریخ شروع دوره جدید می باشد

                            //----------- بدست اوردن بازه دوره بعدی 
                            Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(NewDoreh_StartTime, Convert.ToInt32(DtAbInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtAbInfo.Rows[0]["mohlat_pardakht"]));
                            //----------- بدست اوردن بازه دوره بعدی 

                            ClsMain.ChangeCulture("e");

                            Classes.clsAb.Ijad_Doreh_jadid_ab_V2(Convert.ToInt32(New_doreh_dcode), NewDoreh_StartTime,
                                Doreh_length.End_Of_Doreh, Doreh_length.Mohlat_pardakht,
                                Convert.ToDecimal(DtAbMablagh.Rows[0]["mablagh"]), DtAbInfo.Rows[0]["darsad_maliat"].ToString(),
                                DtAbInfo.Rows[0]["ZaribTejari"].ToString(), DtAbInfo.Rows[0]["ZaribDarhalSakht"].ToString()
                                , Convert.ToDecimal(DtAbInfo.Rows[0]["mablagh_fazelab"].ToString()),
                                Convert.ToDecimal(DtAbInfo.Rows[0]["zarib_aboonman"].ToString()),
                                Convert.ToDecimal(DtAbInfo.Rows[0]["masraf_omoomi"].ToString()),
                                 Convert.ToDecimal(DtAbInfo.Rows[0]["hazineh_taviz_kontor"].ToString())
                                );
                            Payam.Show("دوره جدید اب با موفقیت در سیستم ذخیره شد");

                            ClsMain.ChangeCulture("f");
                        }
                    }
                }


            }
        }



        void StartUPActions()
        {




            //--------------------------------------------------
            Sharj_Operation();
            Ab_Operation();
            //--------------------------------------------------
            StartUPActionsIsDon = true;
            UpdateDorehPanel();
            //--------------------------------------------------
            new MainDataSestTableAdapters.moshtarekinTableAdapter().Fill(Classes.ClsMain.MoshtarekDT);
            //new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter().Fill(Classes.ClsMain.MoshtarekDT);
        }


        Boolean StartUPActionsIsDon = false;

        DateTime LastSearch;
        int CheckPeriod = 60;
        private void timer1_Tick(object sender, EventArgs e)
        {
            HourLabel.Text = DateTime.Now.ToShortTimeString();
            if (clsMoshtarekin.UserInfo == null)
                return;
            Counter++;

            if (Counter == 2)
            {
                if (StartUPActionsIsDon == false)
                    StartUPActions();
            }

            if (Counter == 3)
                ClsMain.IsChequeTime();

            if (ClsMain.ShahrakSetting.mainPageEtelaiehChaneg)
                if (Counter % CheckPeriod == 0)
                {
                    if ((DateTime.Now - LastSearch).TotalSeconds > CheckPeriod)
                    {
                        superTabControl1.SelectedTabIndex = 0;
                        DataTable dt = Classes.ClsMain.GetDataTable("select top(10) * from admin_notification where state=0  order by id desc");
                        DataColumn c = new System.Data.DataColumn();
                        c.ColumnName = "time";
                        dt.Columns.Add(c);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["time"] = Classes.ClsMain.GetPrettyDate(Convert.ToDateTime(dt.Rows[i]["tarikh"]));
                        }
                        dgvNOtofication.DataSource = dt;

                        if (dgvNOtofication.Rows.Count == 0)
                        {
                            lblnoetefagh.Visible = true;
                        }
                        else
                        {
                            lblnoetefagh.Visible = false;

                        }
                    }
                }







        }

        private void btn_doreh_Click(object sender, EventArgs e)
        {
            //System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(Classes.clsSharj.Create_ghobooz_sharj));
            //th.Start();

        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }





        public void SetGharadadAndSerach( int gharardad, DataTable dt_moshtarek)
        {
            DataView dv = new System.Data.DataView(Classes.ClsMain.MoshtarekDT);
            dv.RowFilter = "gharardad=" + gharardad;
            dt_moshtarek = dv.ToTable();
            if (Show_Moshtarek_info(dt_moshtarek,gharardad.ToString() ))
            {
                txt_find_moshtarek.Text = gharardad.ToString();
                Show_Moshtarek_sharj_info(gharardad, lbl_doreh_sharj.Text);
                Show_Moshtarek_ab_info(gharardad, Panel_lbl_ab_doreh.Text);
            }


            txt_find_moshtarek.SelectAll();
            txt_find_moshtarek.Focus();
        }

        public void Update_Ab_sharj_panel()
        {
            if (txt_moshtarek_code.Text.Trim() != "")
            {
                Show_Moshtarek_sharj_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), lbl_doreh_sharj.Text);
                Show_Moshtarek_ab_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), Panel_lbl_ab_doreh.Text);
            }
        }

        public void Update_Moshtarek_Status()
        {
            if (txt_find_moshtarek.Text.Trim() != "")
                btn_find_company.PerformClick();
        }


        private void btn_find_company_Click(object sender, EventArgs e)
        {
            if (txt_find_moshtarek.Text == "errors")
            {
                new Frm_errors().ShowDialog();
                return;
            }
            else if(txt_find_moshtarek.Text=="shahrak")
            {
                if(Classes.ClsMain.ShahrakSetting.shahrak_ID==Classes.Shahrak_IDS.shams_abad)
                {
                    new forms.shamsAbad.frm_rah_andazi().ShowDialog();
                }
            }
         


            //if (txt_find_moshtarek.Text == "script")
            //{
            //    DataTable dt_ab_first_doreh = ClsMain.GetDataTable("select * from ab_ghabz where dcode=1");

            //    Classes.ClsMain.ChangeCulture("e");
            //    for (int i = 0; i < dt_ab_first_doreh.Rows.Count; i++)
            //    {
            //        try
            //        {
            //            decimal bestankari_from_last_doreh = Convert.ToDecimal(
            //                      Classes.ClsMain.ExecuteScalar("select bestankari from ab_ghabz where gharardad=" + dt_ab_first_doreh.Rows[i]["gharardad"].ToString() + " and dcode=2"));

            //            DataTable dt_ab_3_doreh = ClsMain.GetDataTable("select * from ab_ghabz where dcode=3 and gharardad=" + dt_ab_first_doreh.Rows[i]["gharardad"].ToString());

            //            Classes.ClsMain.Add_mablagh_to_ghabz
            //                (
            //                dt_ab_3_doreh.Rows[0]["gcode"].ToString(), Convert.ToDecimal(dt_ab_3_doreh.Rows[0]["mablaghkol"])
            //                , bestankari_from_last_doreh, "ab_ghabz", dt_ab_first_doreh.Rows[i]["gharardad"].ToString(), Convert.ToDecimal(dt_ab_first_doreh.Rows[i]["maliat"]), "3"

            //                );
            //        }
            //        catch
            //        {

            //        }
            //    }

            //    DataTable dt_sharj = ClsMain.GetDataTable("select * from sharj_ghabz where dcode=1");

            //    Classes.ClsMain.ChangeCulture("e");

            //    //decimal bestankari_from_last_doreh = Convert.ToDecimal(
            //    //             Classes.ClsMain.ExecuteScalar("select bestankari from sharj_ghabz where gharardad=" + dt_sharj.Rows[i]["gharardad"].ToString() + " and dcode=2"));

            //    for (int i = 0; i < dt_sharj.Rows.Count; i++)
            //    {
            //        try
            //        {
            //            DataTable dt_sharj_2_doreh = ClsMain.GetDataTable("select * from sharj_ghabz where dcode=2 and gharardad=" + dt_sharj.Rows[i]["gharardad"].ToString());


            //            Classes.ClsMain.Add_mablagh_to_ghabz
            //           (
            //           dt_sharj_2_doreh.Rows[0]["gcode"].ToString(), Convert.ToDecimal(dt_sharj_2_doreh.Rows[0]["mablaghkol"])
            //           , Convert.ToDecimal(dt_sharj.Rows[0]["bestankari"]), "sharj_ghabz", dt_sharj.Rows[i]["gharardad"].ToString(), Convert.ToDecimal(dt_sharj.Rows[i]["maliat"]), "2"

            //           );
            //        }
            //        catch (Exception)
            //        {


            //        }

            //    }

            //    Payam.Show("کاربر گرامی ، عملیات ثبت مالیات های دوره قبل برای دوره فعلی انجام شد . لطفا دیگر این دستور تکرار نشود");
            //    txt_find_moshtarek.Text = "";
            //    txt_find_moshtarek.Focus();





            //}
         


            if (Classes.clsMoshtarekin.UserInfo.p42)
            {
                txtPelak.Clear();
                Clear_ab_panel();
                Clear_sharj_panel();
                LastSearch = DateTime.Now;
                superTabControl1.SelectedTabIndex = 0;
                if (txt_find_moshtarek.Text.Trim() == "")
                {
                    Payam.Show("لطفا شماره قرارداد یا نام مشترک را وارد نمایید");
                    txt_find_moshtarek.Focus();
                }
                else
                {
                    DataTable dt_moshtarek = null;
                    try
                    {
                        decimal Moshtarek_Gharardad = Convert.ToDecimal(txt_find_moshtarek.Text, CultureInfo.InvariantCulture);
                        DataView dv = new System.Data.DataView(Classes.ClsMain.MoshtarekDT);
                        dv.RowFilter = "gharardad=" + Moshtarek_Gharardad.ToString(CultureInfo.InvariantCulture);
                        dt_moshtarek = dv.ToTable();


                    }
                    catch
                    {
                        string Moshtarek_company_name = txt_find_moshtarek.Text.ToString();
                        DataView dv = new System.Data.DataView(Classes.ClsMain.MoshtarekDT);

                        try
                        {
                            dv.RowFilter = "co_name like '%" + Moshtarek_company_name.ToString() + "%'";
                            dt_moshtarek = dv.ToTable();

                        }
                        catch (Exception)
                        {
                            Payam.Show("لطفا مقادیر معتبر وارد کنید");
                            return;
                        }

                    }
                    if (dt_moshtarek.Rows.Count > 1)
                    {
                        frmMain_search frm = new frmMain_search(this, dt_moshtarek);
                        frm.Show();
                    }
                    else if (Show_Moshtarek_info(dt_moshtarek))
                    {
                        Show_Moshtarek_sharj_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), lbl_doreh_sharj.Text);
                        Show_Moshtarek_ab_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), Panel_lbl_ab_doreh.Text);

                    }


                    txt_find_moshtarek.SelectAll();
                    txt_sharj_new_pardakht.Focus();


                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void Show_Moshtarek_ab_info(decimal gharardad, string ab_docde)
        {
            //if (gharardad == "")
            //{
            //    gharardad = txt_moshtarek_code.Text;
            //    ab_docde = Panel_lbl_ab_doreh.Text;
            //}

            try
            {
                grp_ab.Text = "امور اب";
                DataRow Moshtarek_ghabz = null;
                if (ab_docde.ToString().Trim() != "")
                    Moshtarek_ghabz = Classes.clsMoshtarekin.Akharin_ghabz_ab(gharardad, ab_docde);

                if (Moshtarek_ghabz != null)
                {

                    if (Moshtarek_ghabz["mande"].ToString() == "0")
                    {
                        lbl_ab_mandeh.AutoSprator = false;
                        lbl_ab_mandeh.Text = "0";
                    }
                    else
                    {
                        lbl_ab_mandeh.AutoSprator = true;
                        lbl_ab_mandeh.Text = Moshtarek_ghabz["mande"].ToString();
                    }


                    lbl_ab_mandeh.Tag = Moshtarek_ghabz["bestankari"];
                    if (Moshtarek_ghabz["bestankari"].ToString() == "0")
                    {
                        txt_ab_bestankari.AutoSprator = false;
                        txt_ab_bestankari.Text = "0";
                    }
                    else
                    {
                        txt_ab_bestankari.AutoSprator = true;
                        txt_ab_bestankari.Text = Moshtarek_ghabz["bestankari"].ToString();
                    }



                    string pardakhti = (Convert.ToInt64(Moshtarek_ghabz["mablaghkol"]) -
                    (Convert.ToInt64(Moshtarek_ghabz["mande"]) + Convert.ToInt64(Moshtarek_ghabz["kasr_hezar"]))).ToString();

                    if (pardakhti == "0")
                    {
                        lbl_ab_pardakhti.AutoSprator = false;
                        lbl_ab_pardakhti.Text = "0";
                    }
                    else
                    {
                        lbl_ab_pardakhti.AutoSprator = true;
                        lbl_ab_pardakhti.Text = pardakhti;
                    }


                    lbl_ab_mablagh_kol.Text = Moshtarek_ghabz["mablaghkol"].ToString();
                    lbl_ab_mablaghkol_horoof.Text = Classes.clsNumber.Number_to_Str(lbl_ab_mablagh_kol.TextValue.ToString()) + " " + "ریال";
                    lbl_ab_gcode.Text = Moshtarek_ghabz["gcode"].ToString();

                    //lbl_ab_vaziat.Text = " " + "‍‍‍‍برداخت نشده";
                    //lbl_ab_vaziat.ForeColor = Color.Maroon;

                    this.lbl_ab_vaziat.Symbol = "";
                    this.lbl_ab_vaziat.SymbolColor = System.Drawing.Color.Purple;

                    if (Moshtarek_ghabz["vaziat"].ToString() == "1")
                    {
                        //lbl_ab_vaziat.Text = " " + "‍‍‍‍برداخت شده";
                        //lbl_ab_vaziat.ForeColor = Color.Teal;

                        this.lbl_ab_vaziat.Symbol = "";
                        this.lbl_ab_vaziat.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));




                    }
                    else if (Moshtarek_ghabz["vaziat"].ToString() == "2")
                    {
                        //lbl_ab_vaziat.Text = " " + "پرداخت جزئی";
                        //lbl_ab_vaziat.ForeColor = Color.Teal;

                        this.lbl_ab_vaziat.Symbol = "";
                        this.lbl_ab_vaziat.SymbolColor = System.Drawing.Color.Purple;
                    }





                    grp_pardakht_ab.Visible = true;
                    btn_show_ab_Ghabz.Visible = btn_print_ab_Ghabz.Visible = true;
                    btn_pardakht_ab.Visible = true;
                    btn_new_ab_ghabz.Visible = false;
                }
                else
                {
                    Clear_ab_panel();
                    grp_ab.Text = "برای دوره جاری ، قبض آبی صادر نشده است ";
                    btn_show_ab_Ghabz.Visible = btn_print_ab_Ghabz.Visible = false;
                    grp_pardakht_ab.Visible = btn_pardakht_ab.Visible = false;
                    btn_new_ab_ghabz.Visible = true;

                }
            }
            catch (Exception)
            {
                Clear_ab_panel();
                grp_ab.Text = "برای دوره جاری ، قبض آبی صادر نشده است ";
                btn_show_ab_Ghabz.Visible = btn_print_ab_Ghabz.Visible = false;

            }


        }

        bool Show_Moshtarek_info(DataTable dt_moshtarek, string gharardad = "0")
        {
            

            if (dt_moshtarek.Rows.Count != 0)
            {
                int ind = 0;
                txt_moshtarek_address.Text = dt_moshtarek.Rows[ind]["address"].ToString();
                txt_moshtarek_code.Text = Convert.ToString(dt_moshtarek.Rows[ind]["gharardad"], CultureInfo.InvariantCulture);
                txt_moshtarek_modir_amel.Text = dt_moshtarek.Rows[ind]["modir_amel"].ToString();
                txt_moshtarek_name.Text = dt_moshtarek.Rows[ind]["co_name"].ToString();
                txt_moshtarek_phone.Text = dt_moshtarek.Rows[ind]["phone"].ToString();
                txtElhaghieh.Text = dt_moshtarek.Rows[ind]["elhaghi"].ToString().Replace(",", " - ");
                txtMobile.Text = dt_moshtarek.Rows[ind]["mobile"].ToString();
                tabusergozaresh.Visible = true;
                return true;
            }
            tabusergozaresh.Visible = false;
            txtElhaghieh.Text = txt_moshtarek_address.Text = txt_moshtarek_code.Text = txt_moshtarek_modir_amel.Text = txt_moshtarek_name.Text = txt_moshtarek_phone.Text = txtMobile.Text = "";
            return false;

        }

        void Clear_sharj_panel()
        {
            lbl_sharj_mandeh.Text =
            lbl_sharj_pardakhti.Text =
            lbl_moshtarek_mablagh_kol.Text =
            lbl_moshtarek_mablgh_kol_str.Text =
            lbl_sharj_vaziat.Text =
            lbl_sharj_gcode.Text =
            txt_sharj_bestnkari.Text = "";
        }


        void Clear_ab_panel()
        {
            lbl_ab_mandeh.Text =
            lbl_ab_pardakhti.Text =
            lbl_ab_mablagh_kol.Text =
            lbl_ab_mablaghkol_horoof.Text =
            lbl_ab_gcode.Text =
            lbl_ab_vaziat.Text =
            txt_ab_bestankari.Text = "";
        }


        public void Show_Moshtarek_sharj_info(decimal gharardad, string dcode)
        {
            try
            {
                grp_sharj.Text = "امور شارژ";
                DataRow Moshtarek_ghabz = null;
                if (dcode.ToString().Trim() != "")
                    Moshtarek_ghabz = Classes.clsMoshtarekin.Akharin_ghabz_sharj(gharardad, dcode);

                if (Moshtarek_ghabz != null)
                {
                    if (Moshtarek_ghabz["mande"].ToString() == "0")
                    {
                        lbl_sharj_mandeh.AutoSprator = false;
                        lbl_sharj_mandeh.Text = "0";
                    }
                    else
                    {
                        lbl_sharj_mandeh.AutoSprator = true;
                        lbl_sharj_mandeh.Text = Moshtarek_ghabz["mande"].ToString();

                    }

                    if (Moshtarek_ghabz["bestankari"].ToString() == "0")
                    {
                        txt_sharj_bestnkari.AutoSprator = false;
                        txt_sharj_bestnkari.Text = "0";
                    }
                    else
                    {
                        txt_sharj_bestnkari.AutoSprator = true;
                        txt_sharj_bestnkari.Text = Moshtarek_ghabz["bestankari"].ToString();

                    }

                    string pardakhti = (Convert.ToInt64(Moshtarek_ghabz["mablaghkol"]) -
                        (Convert.ToInt64(Moshtarek_ghabz["mande"]) + Convert.ToInt64(Moshtarek_ghabz["kasr_hezar"]))).ToString();
                    if (pardakhti == "0")
                    {
                        lbl_sharj_pardakhti.AutoSprator = false;
                        lbl_sharj_pardakhti.Text = "0";
                    }
                    else
                    {
                        lbl_sharj_pardakhti.AutoSprator = true;
                        lbl_sharj_pardakhti.Text = pardakhti;
                    }

                    lbl_moshtarek_mablagh_kol.Text = Moshtarek_ghabz["mablaghkol"].ToString();
                    lbl_moshtarek_mablgh_kol_str.Text = Classes.clsNumber.Number_to_Str(lbl_moshtarek_mablagh_kol.TextValue.ToString()) + " " + "ریال";
                    lbl_sharj_gcode.Text = Moshtarek_ghabz["gcode"].ToString();

                    //lbl_sharj_vaziat.Text = " " + "‍‍‍‍برداخت نشده";
                    //lbl_sharj_vaziat.ForeColor = Color.Maroon;

                    this.lbl_sharj_vaziat.Symbol = "";
                    this.lbl_sharj_vaziat.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));

                    if (Moshtarek_ghabz["vaziat"].ToString() == "1")
                    {
                        //lbl_sharj_vaziat.Text = " " + "‍‍‍‍برداخت شده";
                        //lbl_sharj_vaziat.ForeColor = Color.Teal;

                        this.lbl_sharj_vaziat.Symbol = "";
                        this.lbl_sharj_vaziat.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));



                    }
                    else if (Moshtarek_ghabz["vaziat"].ToString() == "2")
                    {

                        this.lbl_sharj_vaziat.Symbol = "";
                        this.lbl_sharj_vaziat.SymbolColor = System.Drawing.Color.Purple;

                    }

                    grp_pardakht_sharj.Visible = true;

                    btn_show_Sharj_Ghabz.Visible = btn_print_Sharj_Ghabz.Visible = true;
                    btn_pardakht_sharj.Visible = true;
                    btn_new_sharj_ghabz.Visible = false;
                }
                else
                {
                    Clear_sharj_panel();
                    grp_sharj.Text = "برای دوره جاری ، قبض شارژی صادر نشده است ";
                    btn_show_Sharj_Ghabz.Visible = btn_print_Sharj_Ghabz.Visible = false;
                    grp_pardakht_sharj.Visible = btn_pardakht_sharj.Visible = false;
                    btn_new_sharj_ghabz.Visible = true;
                }
            }
            catch (Exception)
            {


                Clear_sharj_panel();
                grp_sharj.Text = "برای دوره جاری ، قبض شارژی صادر نشده است ";
                btn_show_Sharj_Ghabz.Visible = btn_print_Sharj_Ghabz.Visible = false;
            }
        }


        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_company.PerformClick();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt_all_moshtarek = Classes.ClsMain.GetDataTable("select gharardad,metraj from moshtarekin");
            Random r = new Random();

            for (int i = 0; i < dt_all_moshtarek.Rows.Count; i++)
            {
                Classes.ClsMain.ExecuteNoneQuery("update moshtarekin set metraj=" + r.Next(1000, 10000).ToString() + " where gharardad=" + dt_all_moshtarek.Rows[i]["gharardad"].ToString());

            }
        }

        private void command1_Executed(object sender, EventArgs e)
        {


        }

        private void All_ghabz_sharj_Executed(object sender, EventArgs e)
        {
            if (txt_moshtarek_code.Text.Trim() != "")
            {
                Frm_sharj_For_all frm = new Frm_sharj_For_all(txt_moshtarek_code.Text);
                frm.Show();
            }
        }

        private void metroTileItem6_Click(object sender, EventArgs e)
        {
            Frm_sharj_For_all frm = new Frm_sharj_For_all();
            frm.Show();
        }

        private void metroTileItem3_Click(object sender, EventArgs e)
        {
            Frm_sharj_per_user frm = new Frm_sharj_per_user();
            frm.Show();
        }



        private void New_sharj_ghabz_Executed(object sender, EventArgs e)
        {

        }


        private void buttonItem30_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_for_all"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 3);

                Ab.Frm_ab_for_all frm = new Ab.Frm_ab_for_all();
                ClsMain.openForm("Frm_ab_for_all");
                frm.Show();

            }
        }





        private void buttonItem27_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("frm_moshtarek_new"))
            {

                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 1);
                frm_moshtarek_new frm = new frm_moshtarek_new();
                ClsMain.openForm("frm_moshtarek_new");
                frm.Show();
                new MainDataSestTableAdapters.moshtarekinTableAdapter().Fill(Classes.ClsMain.MoshtarekDT);
                //new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter().Fill(Classes.ClsMain.MoshtarekDT);


            }
        }



        private void buttonItem36_Click(object sender, EventArgs e)
        {
            Frm_sharj_per_user frm = new Frm_sharj_per_user();
            frm.Show();
        }

        private void buttonItem36_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_per_user"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 8);

                Frm_sharj_per_user frm = new Frm_sharj_per_user();
                ClsMain.openForm("Frm_sharj_per_user");
                frm.Show();
            }

        }

        private void buttonItem37_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_For_all"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 9);
                Frm_sharj_For_all frm = new Frm_sharj_For_all();
                ClsMain.openForm("Frm_sharj_For_all");
                frm.Show();
            }
        }



        private void ribbonTabItem6_Click(object sender, EventArgs e)
        {

        }




        private void buttonItem42_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_barge_report_All"))
            {
                ClsMain.openForm("Frm_barge_report_All");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 22);
                Frm_barge_report_All frm = new Frm_barge_report_All();
                frm.Show();
            }
        }

        private void buttonItem32_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_barge_darkhast"))
            {
                ClsMain.openForm("Frm_barge_darkhast");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 20);
                Frm_barge_darkhast frm = new Frm_barge_darkhast();
                frm.Show();
            }
        }

        private void barge_Executed(object sender, EventArgs e)
        {
            Frm_barge_darkhast frm = new Frm_barge_darkhast(txt_moshtarek_code.Text);
            frm.Show();
        }

        private void buttonItem48_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, "منوی اصلی - " + "صدور برگه خروج", "دسترسی");

            frm_barge_sodoor frm = new frm_barge_sodoor();
            frm.Show();
        }

        private void labelX34_Click(object sender, EventArgs e)
        {

        }

        private void dgvNOtofication_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 3)
                {
                    Classes.clsAdmin.Notify_checked(dgvNOtofication.Rows[e.RowIndex].Cells[0].Value.ToString());
                    dgvNOtofication.Rows.RemoveAt(e.RowIndex);

                }
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            DataTable dt = Classes.ClsMain.GetDataTable("select top(10) * from admin_notification where state=0  order by id desc");

            if (dt.Rows.Count == 0)
            {
                Payam.Show("خیالتون راحت ! هیچ اتفاق جدیدی در سیستم رخ نداده است .");
                lblnoetefagh.Visible = true;
            }
            else
            {
                lblnoetefagh.Visible = false;

                DataColumn c = new System.Data.DataColumn();
                c.ColumnName = "time";
                dt.Columns.Add(c);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["time"] = Classes.ClsMain.GetPrettyDate(Convert.ToDateTime(dt.Rows[i]["tarikh"]));
                }
                dgvNOtofication.DataSource = dt;

            }

        }

        private void buttonItem49_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem49_Click_1(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.frm_bargeh_settings().ShowDialog();
        }

        private void buttonItem39_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_setting"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 11);

                Frm_sharj_setting frm = new Frm_sharj_setting();
                ClsMain.openForm("Frm_sharj_setting");
                frm.Show();
            }
        }

        private void buttonItem44_Click(object sender, EventArgs e)
        {


            if (ClsMain.isClosed("Frm_ab_management"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 15);
                Ab.Frm_ab_management frm = new Ab.Frm_ab_management();
                if (frm.IsDisposed)
                    return;
                ClsMain.openForm("Frm_ab_management");
                frm.Show();
                UpdateDorehPanel();
            }
        }

        private void buttonItem50_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_mablagh"))
            {
                ClsMain.openForm("Frm_ab_mablagh");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 16);
                Ab.Frm_ab_mablagh frm = new Ab.Frm_ab_mablagh();
                frm.Show();
            }

        }

        private void buttonItem51_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_mablagh"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 12);

                Sharj.Frm_sharj_mablagh frm = new Sharj.Frm_sharj_mablagh();
                ClsMain.openForm("Frm_sharj_mablagh");
                frm.Show();
            }
        }

        private void buttonItem43_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem52_Click(object sender, EventArgs e)
        {
            Ab.Frm_ab_ensheab_hazineh frm = new Ab.Frm_ab_ensheab_hazineh();
            frm.Show();
        }

        private void buttonItem43_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_setting"))
            {
                ClsMain.openForm("Frm_ab_setting");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 18);
                Ab.Frm_ab_setting frm = new Ab.Frm_ab_setting();
                frm.Show();
            }
        }

        private void buttonItem52_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_ensheab_hazineh"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 17);
                Ab.Frm_ab_ensheab_hazineh frm = new Ab.Frm_ab_ensheab_hazineh();
                ClsMain.openForm("Frm_ab_ensheab_hazineh");
                frm.Show();
            }
        }

        private void ErsalPayam_Executed(object sender, EventArgs e)
        {
            FrmPayam frm = new FrmPayam(txt_moshtarek_code.Text);
            frm.Show();
        }

        private void buttonItem54_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, "منوی اصلی - " + "ارسال پیام", "دسترسی");

            FrmPayam frm = new FrmPayam();
            frm.Show();
        }

        private void buttonItem46_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_for_all"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 14);

                Ab.Frm_ab_for_all frm = new Ab.Frm_ab_for_all();
                ClsMain.openForm("Frm_ab_for_all");
                frm.Show();
            }
        }

        private void buttonItem45_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_per_user"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 13);
                ClsMain.openForm("Frm_ab_per_user");
                Ab.Frm_ab_per_user frm = new Ab.Frm_ab_per_user("");
                frm.Show();
            }
        }

        private void buttonItem53_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_Jarimeh_Masaraf_mazad"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 19);
                ClsMain.openForm("Frm_ab_Jarimeh_Masaraf_mazad");
                Ab.Frm_ab_Jarimeh_Masaraf_mazad frm = new Ab.Frm_ab_Jarimeh_Masaraf_mazad();
                frm.Show();
            }

        }

        private void buttonItem56_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 35);

            ManagementReports.frmDaramadRpt FRM = new ManagementReports.frmDaramadRpt();
            FRM.Show();
        }

        private void buttonItem58_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem59_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 38);

            ManagementReports.FrmTarefeRpt frm = new ManagementReports.FrmTarefeRpt();
            frm.Show();
        }

        private void buttonItem57_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 34);

            ManagementReports.FrmGhabzRpt frm = new ManagementReports.FrmGhabzRpt();
            frm.Show();
        }

        private void buttonItem67_Click(object sender, EventArgs e)
        {
            Payam.Show("سیستم به صورت خودکار از اطلاعات پشتیبان می گیرد . لذا از تغییر در فرم جاری خودداری فرمایید ");
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 30);
            new Management.FrmBackupDB().Show();
        }

        private void buttonItem72_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmAddKey"))
            {
                ClsMain.openForm("FrmAddKey");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 27);
                new Mobile_Part.FrmAddKey().Show();
            }
        }

        private void buttonItem69_Click(object sender, EventArgs e)
        {

            if (ClsMain.isClosed("FrmAlllKey"))
            {
                ClsMain.openForm("FrmAlllKey");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 26);
                new Mobile_Part.FrmAlllKey().Show();
            }
        }

        private void buttonItem68_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmMobileConnectionReport"))
            {
                ClsMain.openForm("FrmMobileConnectionReport");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 25);
                new Mobile_Part.FrmMobileConnectionReport().Show();
            }
        }

        private void buttonItem85_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmMobileConnectionReport"))
            {
                ClsMain.openForm("FrmMobileConnectionReport");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 5);

                new Mobile_Part.FrmMobileConnectionReport().Show();
            }



        }

        private void buttonItem28_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("frm_moshtarek_all"))
            {
                ClsMain.openForm("frm_moshtarek_all");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 2);
                frm_moshtarek_all frm = new frm_moshtarek_all();
                frm.Show();
            }


        }

        private void buttonItem60_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 36);
            new ManagementReports.frmDaramadSource().Show();

        }

        private void buttonItem34_Click_1(object sender, EventArgs e)
        {
            CheckLogin("1", "1");
        }

        private void gozarehat_check1_Click(object sender, EventArgs e)
        {
            clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 39);
            new ManagementReports.frmChequeManage().Show();
        }

        private void buttonItem27_Click_2(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_management"))
            {

                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 10);

                Frm_sharj_management frm = new Frm_sharj_management(this);
                if (frm.IsDisposed)
                    return;
                ClsMain.openForm("Frm_sharj_management");
                frm.Show();
                UpdateDorehPanel();
            }

        }

        private void modiriat_admin_users_Click(object sender, EventArgs e)
        {

            if (ClsMain.isClosed("frm_user"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 31);
                ClsMain.openForm("frm_user");
                new Management.frm_user().Show();
            }
        }

        private void modiriat_etelaieh1_Click(object sender, EventArgs e)
        {


            if (ClsMain.isClosed("FrmNotification"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 33);
                ClsMain.openForm("FrmNotification");
                new Management.FrmNotification().Show();
            }
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtpassword.Focus();
        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnLogin.PerformClick();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Trim() == "")
                txtUsername.Focus();
            else if (txtpassword.Text.Trim() == "")
                txtpassword.Focus();
            else
            {
                CheckLogin(txtUsername.Text, txtpassword.Text);
            }

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo != null)
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 41);
            Application.Exit();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {


        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {

        }
        void Logout()
        {
            clsMoshtarekin.UserInfo = null;
            ribbonControl1.Enabled = false;
            txt_find_moshtarek.Enabled = false;

            grpLogin.Visible = true;
            txtUsername.Text = txtpassword.Text = "";
            txtUsername.Focus();
            if (grp_karkhaneh.Visible == true)
            {
                grp_karkhaneh.Visible = false;
                grp_doreh.Visible = false;
            }
        }

        private void buttonItem23_Click(object sender, EventArgs e)
        {
            Logout();
        }

        private void buttonX3_Click_2(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p21)
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 44, lbl_sharj_gcode.Text);

                if (lbl_sharj_gcode.Text != "")
                    new Frm_sharj_details(lbl_sharj_gcode.Text, txt_moshtarek_code.Text).ShowDialog();
                if (txt_find_moshtarek.Text != "")
                    btn_find_company.PerformClick();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p46)
            {
                if (lbl_ab_gcode.Text != "")
                    new Ab.Frm_ab_details(lbl_ab_gcode.Text, txt_moshtarek_code.Text).ShowDialog();
                if (txt_find_moshtarek.Text != "")
                    btn_find_company.PerformClick();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void modiriat_gozareshat_dastarsi_Click(object sender, EventArgs e)
        {

            if (ClsMain.isClosed("frm_user_access"))
            {
                ClsMain.openForm("frm_user_access");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 32);
                new Management.frm_user_access().Show();
            }
        }

        private void bargeh_sodoor_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("frm_barge_sodoor"))
            {
                ClsMain.openForm("frm_barge_sodoor");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 21);
                new frm_barge_sodoor().Show();
            }
        }

        private void buttonItem71_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem55_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem24_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p104)
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 104);
                new ManagementReports.FrmBedehkarBestankarSharj().Show();
            }
            else
            {
                Payam.Show("شما مجوز دسترسی به این بخش را ندارید");
            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            Logout();
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p45)
            {
                if (txt_moshtarek_code.Text.Trim() != "")
                {

                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {
                        Classes.shahrak_classes.esf_bozorg.Print_Ghabz
                            (
                            Convert.ToInt32(txt_moshtarek_code.Text),
                            Convert.ToInt32(lbl_doreh_sharj.Text),
                             Convert.ToInt32(Panel_lbl_ab_doreh.Text)

                            );
                    }
                    else
                    {
                        Classes.clsSharj.print_Ghabz(
                            lbl_sharj_gcode.Text,
                            txt_moshtarek_code.Text,


                             "print"
                             , true
                            );
                        clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 45, lbl_sharj_gcode.Text);
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void btn_print_ab_Ghabz_Click(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p47)
            {
                if (txt_moshtarek_code.Text.Trim() != "")
                {

                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {
                        Classes.shahrak_classes.esf_bozorg.Print_Ghabz
                            (
                            Convert.ToInt32(txt_moshtarek_code.Text),
                             Convert.ToInt32(lbl_doreh_sharj.Text),
                            Convert.ToInt32(Panel_lbl_ab_doreh.Text)

                            );
                    }
                    else
                    {
                        Classes.clsAb.print_Ghabz(lbl_ab_gcode.Text, txt_moshtarek_code.Text, true);
                        clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 47, lbl_ab_gcode.Text);
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void grp_karkhaneh_Click(object sender, EventArgs e)
        {

        }

        private void buttonX3_Click_3(object sender, EventArgs e)
        {

            if (txt_find_moshtarek.Text != "")
            {
                if (clsMoshtarekin.UserInfo.p21)
                {
                    new frm_barge_sodoor(txt_moshtarek_code.Text).Show();
                    clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 48, txt_moshtarek_code.Text);
                }
                else
                    Payam.Show("شما مجوز انجام این کار را ندارید");
            }
        }

        private void buttonItem8_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_For_all"))
            {

                Frm_sharj_For_all frm = new Frm_sharj_For_all();
                ClsMain.openForm("Frm_sharj_For_all");
                frm.Show();
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 4, lbl_ab_gcode.Text);

            }
        }

        private void moshtarekin_payam1_Click(object sender, EventArgs e)
        {

        }

        private void q5_Click(object sender, EventArgs e)
        {

            if (ClsMain.isClosed("frm_barge_sodoor"))
            {
                ClsMain.openForm("frm_barge_sodoor");
                if (txt_moshtarek_code.Text != "")
                    new frm_barge_sodoor(txt_moshtarek_code.Text).Show();
                else
                    new frm_barge_sodoor().Show();
            }
        }




        private void RbMoshtarekin_Click(object sender, EventArgs e)
        {

        }

        private void txtPelak_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFindByPelak.PerformClick();
        }

        private void btnFindByPelak_Click(object sender, EventArgs e)
        {
            LastSearch = DateTime.Now;

            txt_find_moshtarek.Clear();

            if (Classes.clsMoshtarekin.UserInfo.p43)
            {
                Clear_ab_panel();
                Clear_sharj_panel();
                LastSearch = DateTime.Now;
                superTabControl1.SelectedTabIndex = 0;
                if (txtPelak.Text.Trim() == "")
                {
                    Payam.Show("لطفا شماره پلاک مشترک را وارد نمایید");
                    txtPelak.Focus();
                }
                else
                {
                    DataTable dt_moshtarek = null;
                    DataView dv = new System.Data.DataView(Classes.ClsMain.MoshtarekDT);
                    dv.RowFilter = "pelak like '%" + txtPelak.Text.ToString() + "%'";
                    dt_moshtarek = dv.ToTable();

                    if (dt_moshtarek.Rows.Count > 1)
                    {
                        frmMain_search frm = new frmMain_search(this, dt_moshtarek);
                        frm.Show();
                    }
                    else if (Show_Moshtarek_info(dt_moshtarek))
                    {
                        Show_Moshtarek_sharj_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), lbl_doreh_sharj.Text);
                        Show_Moshtarek_ab_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), Panel_lbl_ab_doreh.Text);

                    }


                    txt_find_moshtarek.SelectAll();
                    txt_find_moshtarek.Focus();
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonItem9_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_moshtarekin_savabegh"))
            {
                ClsMain.openForm("Frm_moshtarekin_savabegh");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 6);

                new Frm_moshtarekin_savabegh().Show();

            }
        }

        private void lbl_ab_mablagh_kol_Click(object sender, EventArgs e)
        {

        }

        private void lbl_ab_mablaghkol_horoof_Click(object sender, EventArgs e)
        {

        }

        private void labelX17_Click(object sender, EventArgs e)
        {

        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }





        private void buttonItem10_Click(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.frm_Bageh_print_Management().ShowDialog();
        }

        private void ribbonPanel4_Click(object sender, EventArgs e)
        {

        }

        private void buttonFile_Click(object sender, EventArgs e)
        {
            RbMoshtarekin.Select();
        }

        private void FrmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
                txt_find_moshtarek.Focus();
        }

        private void RbBargehKhorooj_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem11_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_FirstTimeBedehiValues"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 29);
                ClsMain.openForm("Frm_sharj_FirstTimeBedehiValues");
                new Sharj.Frm_sharj_FirstTimeBedehiValues().Show();
            }
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            // عملیات پشتبیان یگری به صورت خودکار و توسط خود اس کیو ال سرور انجام می شود 

            ////if (Cpayam.Show("ایا مایل به گرفتن پشتیبان از وضعیت فعلی  بانک اطلاعاتی می باشید?") == DialogResult.Yes)
            ////{
            //System.Threading.Thread tr = new System.Threading.Thread(new System.Threading.ThreadStart(BackupOnExit));
            //tr.Start();
            ////}

        }
        void BackupOnExit()
        {

            if (System.IO.Directory.Exists(ClsMain.ShahrakSetting.backup_path))
            {

                long inde = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull(max(id),1) from  backup_history"));
                FarsiLibrary.Utils.PersianDate Alan = new FarsiLibrary.Utils.PersianDate(DateTime.Now);
                string backupDirectory = Alan.Year + "-" + Alan.Month + "-" + Alan.Day + "--" + Alan.Hour + "-" + Alan.Minute;
                string Fullpath = ClsMain.ShahrakSetting.backup_path + "\\" + backupDirectory;

                clsBackup.BackupDatabase(new System.Data.SqlClient.SqlConnection(ClsMain.ConnectionStr),
                    ClsMain.ShahrakSetting.db_name, ClsMain.ShahrakSetting.backup_path, "Negah_backup_");
                Classes.ClsMain.ChangeCulture("e");
                Classes.ClsMain.ExecuteNoneQuery("insert into backup_history values ('" + DateTime.Now + "','Negah_backup_" + (inde + 1).ToString() + ".nbk','" + Fullpath + "')");
                Classes.ClsMain.ChangeCulture("f");
                new PayamAutoClose("پیشتیبان گیری با موفقیت انجام شد", 2000);
            }
            else
                new PayamAutoClose("سیستم قادر به پشتیبان گیری خودکار نمی باشد. لطفا به منوی تنظیمات مراجعه کرده و مسیر پشتیبان را مشخص کنید", 3000);
        }

        private void buttonItem23_Click_1(object sender, EventArgs e)
        {

        }

        private void buttonItem23_Click_2(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_barge_darkhast"))
            {
                ClsMain.openForm("Frm_barge_darkhast");
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 20);
                Frm_barge_darkhast frm = new Frm_barge_darkhast();
                frm.Show();
            }
        }

        private void buttonItem11_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmSetting"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 100);
                ClsMain.openForm("FrmSetting");
                new FrmSetting().Show();

            }

        }

        private void buttonItem28_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmChooseMoshtarekForAbGhabz"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 101);
                ClsMain.openForm("FrmChooseMoshtarekForAbGhabz");
                new Ab.FrmChooseMoshtarekForAbGhabz().Show();

            }
        }

        private void buttonItem29_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("FrmKontorKhanHistory"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 102);
                ClsMain.openForm("FrmKontorKhanHistory");
                new Ab.FrmKontorKhanHistory().Show();

            }
        }

        private void modiriat_bedehi_ab_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_FirstTimeKontorValues"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 28);

                ClsMain.openForm("Frm_ab_FirstTimeKontorValues");
                new Ab.Frm_ab_FirstTimeKontorValues().Show();
            }
        }

        private void buttonItem30_Click_1(object sender, EventArgs e)
        {
            //if (ClsMain.isClosed("FrmJarimehAfterGhabzDeadLine"))
            //{
            //    clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 28);

            //    ClsMain.openForm("FrmJarimehAfterGhabzDeadLine");
            //    new FrmJarimehAfterGhabzDeadLine().Show();
            //}

            new frm_moshtarekin_amar().ShowDialog();
        }

        private void btn_pardakht_sharj_Click(object sender, EventArgs e)
        {
            if (lbl_sharj_gcode.Text.Trim() != "")
            {
                if (Classes.clsMoshtarekin.UserInfo.p58)
                {


                    long bestankari_from_last_doreh = Convert.ToInt64(
                        Classes.ClsMain.ExecuteScalar("select isnull((select bestankari from sharj_ghabz where dcode=" +
                        (Convert.ToInt32(lbl_doreh_sharj.Text) - 1) + " and gharardad=" +
                        txt_moshtarek_code.Text.ToString() + "),0)").ToString());



                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 58);
                    new Sharj.FrmGhabzPardakht(false, lbl_sharj_gcode.Text
                        , lbl_doreh_sharj.Text, txt_moshtarek_code.Text, lbl_moshtarek_mablagh_kol.Text,
                        lbl_moshtarek_mablgh_kol_str.Text, bestankari_from_last_doreh
                        ).Show();
                }
                else
                    Payam.Show("شما مجوز دسترسی به این قسمت را ندارید");
            }
        }

        private void btn_pardakht_ab_Click(object sender, EventArgs e)
        {
            if (lbl_ab_gcode.Text.Trim() != "")
            {
                if (Classes.clsMoshtarekin.UserInfo.p59)
                {

                    //       if (lbl_ab_mablagh_kol.Text == "0" && txt_ab_bestankari.Text != "0")
                    //       {
                    //           string matn = "کاربر گرامی ، قبض این دوره به دلیل وجود بستانکاری از دوره قبل با مبلغ صفر صادر شده است " +
                    //      " با این حال کاربر مبلغ " + txt_ab_bestankari.Text + " بستانکار می باشد  که قرار است به دوره بعد منتقل شود " +
                    //" . به اطلاع می رساند که در این دوره امکان ثبت هیچ گونه پرداختی جدید برای کاربر وجود ندارد ";

                    //           new FrmPeygham(matn).ShowDialog();
                    //           return;
                    //       }





                    long bestankari_from_last_doreh = Convert.ToInt64(
                            Classes.ClsMain.ExecuteScalar("select isnull((select bestankari from ab_ghabz where dcode=" +
                            (Convert.ToInt32(Panel_lbl_ab_doreh.Text) - 1) + " and gharardad=" +
                            txt_moshtarek_code.Text.ToString() + "),0)").ToString());

                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 59);
                    new Sharj.FrmGhabzPardakht(true, lbl_ab_gcode.Text
                        , Panel_lbl_ab_doreh.Text, txt_moshtarek_code.Text, lbl_ab_mablagh_kol.Text,
                        lbl_ab_mablaghkol_horoof.Text, bestankari_from_last_doreh
                        ).Show();
                }
                else
                    Payam.Show("شما مجوز دسترسی به این قسمت را ندارید");
            }
        }

        private void labelX18_Click(object sender, EventArgs e)
        {

        }

        private void groupPanel3_Click(object sender, EventArgs e)
        {

        }

        private void btn_sharj_new_pardakht_Click(object sender, EventArgs e)
        {

            if (Classes.clsMoshtarekin.UserInfo.p58)
            {
                try
                {
                    Classes.ClsMain.ChangeCulture("e");
                    long CurrentPardakht = Convert.ToInt64(txt_sharj_new_pardakht.TextValue);


                    DateTime currentDateTime = FarsiLibrary.Utils.PersianDateConverter.ToGregorianDateTime(txt_sharj_new_tarikh.Text + " " + DateTime.Now.ToShortTimeString());
                    if (CurrentPardakht != 0)
                    {
                        string type = "بانک";
                        if (!rd_sharj_bank.Checked)
                            type = "پوز";
                        Classes.ClsMain.ExecuteNoneQuery("insert into ghabz_pardakht (tarikh_sabt,tarikh_vosool,gharardad,dcode,gcode,nahve_pardakht,vaziat,mablagh,is_ab) values" +
                            "('" + DateTime.Now + "','" + currentDateTime + "'," + txt_moshtarek_code.Text + "," + lbl_doreh_sharj.Text + "," + lbl_sharj_gcode.Text + "," + "N'" + type + "'" + ",1," + txt_sharj_new_pardakht.TextValue + ",0);"
                            );


                        decimal mablagh_kol = Convert.ToDecimal(lbl_moshtarek_mablagh_kol.Text);
                        long bestankari_from_last_doreh = 0;
                        try
                        {
                            bestankari_from_last_doreh = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull((select isnull(bestankari,0) from sharj_ghabz where dcode=" + (Convert.ToInt32(lbl_doreh_sharj.Text) - 1) + " and gharardad=" + txt_moshtarek_code.Text + "),0)").ToString());
                        }
                        catch (Exception)
                        {
                            bestankari_from_last_doreh = 0;
                        }
                        clsBedehkar_Bestankar cls = new clsBedehkar_Bestankar(mablagh_kol, lbl_sharj_gcode.Text, bestankari_from_last_doreh, "GetIt", false);
                        cls.Check_Bedehkar_Bestankar_Status();

                        Classes.clsPardakhti.update_ghabzState_afterPardakht(cls);



                        Payam.Show("پرداخت با موفقیت در سیستم ثبت شد");
                        Show_Moshtarek_sharj_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), lbl_doreh_sharj.Text);
                        Classes.ClsMain.ChangeCulture("f");
                        txt_sharj_new_pardakht.Text = "";
                        txt_find_moshtarek.SelectAll();
                        txt_find_moshtarek.Focus();

                        if (clsFactor.Has_factor(lbl_sharj_gcode.Text) == false)
                        {
                            //================== فاکتور ============================
                            string table_name = "sharj_ghabz";
                            int ghabz_type = 1;
                            DataTable dt_ghabz_info = ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + Convert.ToInt32(lbl_sharj_gcode.Text));

                            decimal darsad_maliat = Convert.ToDecimal(dt_ghabz_info.Rows[0]["darsad_maliat"]);

                            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
                            {

                                Classes.shahrak_classes.abbas_abad_factor factor_class = new Classes.shahrak_classes.abbas_abad_factor(
                                    Classes.shahrak_classes.abbas_abad_factor.Get_Max_factor_num(), Convert.ToInt32(txt_moshtarek_code.Text),
                                    Convert.ToInt32(txt_moshtarek_code.Text), Convert.ToInt32(lbl_doreh_sharj.Text),
                                    0, darsad_maliat, table_name, ghabz_type, "ارائه خدمات شارژ دوره" + lbl_doreh_sharj.Text);
                                factor_class.dt_ghabz = dt_ghabz_info;
                                //تاریخ صدور فاکتور در درون کلاس فاکتور برابر خواهد شد با اولین تاریخ پرداخت سیستم
                                factor_class.Start_genete_ghabz();
                            }
                            else
                            {
                                clsFactor factor_class = new clsFactor(
                                   clsFactor.Get_Max_factor_num(), Convert.ToInt32(txt_moshtarek_code.Text),
                                   Convert.ToInt32(txt_moshtarek_code.Text), Convert.ToInt32(lbl_doreh_sharj.Text),
                                   0, darsad_maliat, table_name, ghabz_type, "ارائه خدمات شارژ دوره" + lbl_doreh_sharj.Text);
                                factor_class.dt_ghabz = dt_ghabz_info;
                                //تاریخ صدور فاکتور در درون کلاس فاکتور برابر خواهد شد با اولین تاریخ پرداخت سیستم
                                factor_class.Start_genete_ghabz();
                            }
                            //================== فاکتور ============================
                        }
                        else
                        {
                            //وضعیت فاکتور در توابع پرداخت به روز رسانی می شود ونیاز به صدور مجدد فاکتور نیست 
                        }




                    }
                }
                catch (Exception ert)
                {
                    Classes.ClsMain.logError(ert);
                    Payam.Show("اطلاعات را به صورت صحیح وارد نمایید");
                    txt_sharj_new_pardakht.Focus();
                }
            }
        }

        private void txt_sharj_new_pardakht_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txt_sharj_new_pardakht.Text.Trim() == "0")
                txt_ab_new_pardakht.Focus();
            else if (e.KeyCode == Keys.Enter)
                btn_sharj_new_pardakht.Focus();
        }

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p59)
            {
                try
                {

                    long CurrentPardakht = Convert.ToInt64(txt_ab_new_pardakht.TextValue);
                    DateTime CurrentDateTime = FarsiLibrary.Utils.PersianDateConverter.ToGregorianDateTime(txt_ab_new_tarikh.Text + " " + DateTime.Now.ToShortTimeString());

                    if (CurrentPardakht != 0)
                    {
                        Classes.ClsMain.ChangeCulture("e");

                        string type = "بانک";
                        if (!rd_ab_bank.Checked)
                            type = "پوز";

                        Classes.ClsMain.ExecuteNoneQuery("insert into ghabz_pardakht (tarikh_sabt,tarikh_vosool,gharardad,dcode,gcode,nahve_pardakht,vaziat,mablagh,is_ab) values" +
                            "('" + DateTime.Now + "','" + CurrentDateTime + "'," + txt_moshtarek_code.Text + "," + Panel_lbl_ab_doreh.Text + "," + lbl_ab_gcode.Text + "," + "N'" + type + "'" + ",1," + txt_ab_new_pardakht.TextValue + ",1);");



                        decimal mablagh_kol = Convert.ToDecimal(lbl_ab_mablagh_kol.Text);
                        long bestankari_from_last_doreh = 0;
                        try
                        {
                            bestankari_from_last_doreh = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull((select isnull(bestankari,0) from ab_ghabz where dcode=" +
                           (Convert.ToInt32(Panel_lbl_ab_doreh.Text) - 1) + " and gharardad=" + txt_moshtarek_code.Text + "),0)").ToString());
                        }
                        catch (Exception)
                        {
                            bestankari_from_last_doreh = 0;
                        }


                        clsBedehkar_Bestankar cls = new clsBedehkar_Bestankar(mablagh_kol, lbl_ab_gcode.Text, bestankari_from_last_doreh, "GetIt", false);
                        cls.Check_Bedehkar_Bestankar_Status();

                        Classes.clsPardakhti.update_ghabzState_afterPardakht(cls);

                        Payam.Show("پرداخت با موفقیت در سیستم ثبت شد");
                        Show_Moshtarek_ab_info(Convert.ToDecimal(txt_moshtarek_code.Text, CultureInfo.InvariantCulture), Panel_lbl_ab_doreh.Text);
                        Classes.ClsMain.ChangeCulture("f");
                        txt_ab_new_pardakht.Clear();
                        txt_find_moshtarek.Focus();

                        if (clsFactor.Has_factor(lbl_ab_gcode.Text) == false)
                        {

                            //================== فاکتور ============================
                            string table_name = "ab_ghabz";
                            int ghabz_type = 0;
                            DataTable dt_ghabz_info = ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + Convert.ToInt32(lbl_ab_gcode.Text));

                            decimal darsad_maliat = Convert.ToDecimal(dt_ghabz_info.Rows[0]["darsad_maliat"]);
                            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
                            {

                                Classes.shahrak_classes.abbas_abad_factor factor_class = new Classes.shahrak_classes.abbas_abad_factor(
                                      clsFactor.Get_Max_factor_num(), Convert.ToInt32(txt_moshtarek_code.Text),
                                      Convert.ToInt32(txt_moshtarek_code.Text), Convert.ToInt32(Panel_lbl_ab_doreh.Text),
                                      0, darsad_maliat, table_name, ghabz_type, "ارائه خدمات اب دوره" + Panel_lbl_ab_doreh.Text);
                                factor_class.dt_ghabz = dt_ghabz_info;
                                factor_class.Start_genete_ghabz();
                            }
                            else
                            {
                                clsFactor factor_class = new clsFactor(
                                clsFactor.Get_Max_factor_num(), Convert.ToInt32(txt_moshtarek_code.Text),
                                Convert.ToInt32(txt_moshtarek_code.Text), Convert.ToInt32(Panel_lbl_ab_doreh.Text),
                                0, darsad_maliat, table_name, ghabz_type, "ارائه خدمات اب دوره" + Panel_lbl_ab_doreh.Text);
                                factor_class.dt_ghabz = dt_ghabz_info;
                                factor_class.Start_genete_ghabz();
                            }
                            //================== فاکتور ============================
                        }
                        else
                        {
                            // تاریخ پرداخت و وضعیت پرداخت در تابع چند خط بالاتر به روز رسانی می شود و نیاز به صدور فاکتور نیست
                            //Classes.clsPardakhti.update_ghabzState_afterPardakht(cls);

                        }

                    }
                }
                catch (Exception)
                {
                    Payam.Show("اطلاعات را به صورت صحیح وارد نمایید");
                    txt_ab_new_pardakht.Focus();
                }
            }
        }



        private void rd_sharj_bank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_sharj_new_pardakht.PerformClick();
        }

        private void radioButton2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_sharj_new_pardakht.PerformClick();
        }

        private void rd_ab_bank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_pardakht_ab_now.PerformClick();
        }

        private void radioButton1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_pardakht_ab_now.PerformClick();
        }

        private void superTabControlPanel1_Click(object sender, EventArgs e)
        {

        }



        private void buttonX4_Click_2(object sender, EventArgs e)
        {
            bool HasDorehGhoboozGenerated = Convert.ToBoolean(Classes.ClsMain.ExecuteScalar(
                      "select ghabz_sodoor from sharj_doreh where dcode=" + lbl_doreh_sharj.Text));
            if (HasDorehGhoboozGenerated)
            {
                //if (Convert.ToBoolean(Classes.ClsMain.ExecuteScalar("select have_sharj_ghabz from moshtarekin where gharardad=" + txt_moshtarek_code.Text)))
                //{

                clsSharjGhabz ghabzInfo = new clsSharjGhabz(Convert.ToInt32(txt_moshtarek_code.Text));

                clsSharj sharjClass = new clsSharj();
                sharjClass.Generate_Ghabz(ghabzInfo);

                //new Frm_sharj_details(txt_moshtarek_code.Text, true).Show();
                btn_find_company.PerformClick();
                //}
                //else
                //    Payam.Show("لطفا به منوی مدیریت رفته و مجوز صدور قبض برای این مشترک را صادر کنید");
            }
            else
                Payam.Show("به دلیل عدم صدور قبوض دوره فعلی ، امکان صدور تک قبض وجود ندارد");
        }

        private void btn_new_ab_ghabz_Click(object sender, EventArgs e)
        {

            bool HasDorehGhoboozGenerated = Convert.ToBoolean(Classes.ClsMain.ExecuteScalar(
                "select ghabz_sodoor from ab_doreh where dcode=" + Panel_lbl_ab_doreh.Text));
            if (HasDorehGhoboozGenerated)
            {
                if (new Ab._ّFrmNewTakGhabz(Panel_lbl_ab_doreh.Text, txt_moshtarek_code.Text).ShowDialog() == DialogResult.Yes)
                {
                    txt_find_moshtarek.Text = txt_moshtarek_code.Text;
                    btn_find_company.PerformClick();
                }



            }
            else
                Payam.Show("به دلیل عدم صدور قبوض دوره ، امکان صدور تک قبض وجود ندارد ");

        }

        private void buttonItem24_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p106)
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 106);

                new ManagementReports.FrmGhabzPardakhtRpt().Show();
            }
            else
            {
                Payam.Show("شما مجوز دسترسی به این بخش را ندارید");
            }
        }



        private void buttonX10_Click_1(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p50)
            {
                if (txt_moshtarek_code.Text != "")
                {
                    CrystalReportViewer repVUer = new CrystalReportViewer();
                    CrysReports.rptRizGozareshAb rpt = new CrysReports.rptRizGozareshAb();

                    Classes.clsAb.RizHeasb(
                        cmbabStart.Text,
                        cmbAbEnd.Text,
                        txt_moshtarek_code.Text,
                        repVUer,
                        rpt, "print"

                        );
                }
                else
                    Payam.Show("لطفا یک مشترک را انتخاب کنید");
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX6_Click_1(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_ab_per_user"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 13);
                ClsMain.openForm("Frm_ab_per_user");
                Ab.Frm_ab_per_user frm = new Ab.Frm_ab_per_user(txt_moshtarek_code.Text);
                frm.Show();
            }
        }

        private void buttonX9_Click_1(object sender, EventArgs e)
        {
            if (clsMoshtarekin.UserInfo.p49)
            {
                if (txt_moshtarek_code.Text != "")
                {
                    CrystalReportViewer repVUer = new CrystalReportViewer();
                    CrysReports.rptRizGozareshSharj rpt = new CrysReports.rptRizGozareshSharj();
                    clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 41);

                    Classes.clsSharj.RizHeasb(
        cmbsharjStart.Text,
        cmbsharjEnd.Text,
                        txt_moshtarek_code.Text,
                        repVUer,
                        rpt, "print"
                        );
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            if (ClsMain.isClosed("Frm_sharj_per_user"))
            {
                clsMoshtarekin.Report_Access(clsMoshtarekin.UserInfo.username, 8);

                Frm_sharj_per_user frm = new Frm_sharj_per_user(txt_moshtarek_code.Text);
                ClsMain.openForm("Frm_sharj_per_user");
                frm.Show();
            }
        }

        private void buttonItem31_Click_1(object sender, EventArgs e)
        {
            // حتما به لیست مجوزها این قسمت رو اضافه کن 
            Ab.Frm_CreateKontorKhan_input frm = new Ab.Frm_CreateKontorKhan_input();
            frm.Show();
        }

        private void buttonX4_Click_3(object sender, EventArgs e)
        {
            if (txt_moshtarek_code.Text.Trim() != "")
                new smsForm(txtMobile.Text, lbl_sharj_gcode.Text, "ghabz_small_info").ShowDialog();
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            if (txt_moshtarek_code.Text.Trim() != "")
                new smsForm(txtMobile.Text, lbl_ab_gcode.Text, "ghabz_small_info").ShowDialog();

        }

        private void buttonItem33_Click_2(object sender, EventArgs e)
        {
            new ManagementReports.FrmSharjTafsili().ShowDialog();
        }

        private void buttonItem34_Click(object sender, EventArgs e)
        {
            new ManagementReports.New_Report.FrmAbTafsili().ShowDialog();
        }

        private void buttonItem36_Click_2(object sender, EventArgs e)
        {
        }

        private void buttonItem32_Click_2(object sender, EventArgs e)
        {
            new ManagementReports.New_Report.FrmSharj_bed_besForAll("sharj").ShowDialog();
        }

        private void buttonItem37_Click_1(object sender, EventArgs e)
        {
            new advanced_report.frm_daramad().ShowDialog();
        }

        private void buttonItem38_Click(object sender, EventArgs e)
        {
            new Management.frmCreateShenase().ShowDialog();
        }

        private void buttonItem39_Click(object sender, EventArgs e)
        {
        }

        private void fd_Click(object sender, EventArgs e)
        {
        }

        private void buttonItem41_Click(object sender, EventArgs e)
        {
        }

        private void buttonItem42_Click_1(object sender, EventArgs e)
        {
            {
                DataTable dt_ghabz = Classes.ClsMain.GetDataTable("select * from ab_ghabz");
                for (int i = 0; i < dt_ghabz.Rows.Count; i++)
                {
                    string gcode = dt_ghabz.Rows[i]["gcode"].ToString();
                    DataTable dt_pardakht = Classes.ClsMain.GetDataTable("select top(1) tarikh_vosool from ghabz_pardakht where gcode=" + gcode + "order by id desc");

                    if (dt_pardakht.Rows.Count == 0)
                        Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set last_pardakht=null,has_pardakhti=0 where gcode=" + gcode);
                    else
                        Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set last_pardakht='" + dt_pardakht.Rows[0]["tarikh_vosool"].ToString() + "',has_pardakhti=1 where gcode=" + gcode);

                }
                Payam.Show("کلیه قبوض آب بروز رسانی شدند.");
            }
            {
                DataTable dt_ghabz = Classes.ClsMain.GetDataTable("select * from sharj_ghabz");
                for (int i = 0; i < dt_ghabz.Rows.Count; i++)
                {
                    string gcode = dt_ghabz.Rows[i]["gcode"].ToString();
                    DataTable dt_pardakht = Classes.ClsMain.GetDataTable("select top(1) tarikh_vosool from ghabz_pardakht where gcode=" + gcode + "order by id desc");

                    if (dt_pardakht.Rows.Count == 0)
                        Classes.ClsMain.ExecuteNoneQuery("update sharj_ghabz set last_pardakht=null,has_pardakhti=0 where gcode=" + gcode);
                    else
                        Classes.ClsMain.ExecuteNoneQuery("update sharj_ghabz set last_pardakht='" + dt_pardakht.Rows[0]["tarikh_vosool"].ToString() + "',has_pardakhti=1 where gcode=" + gcode);

                }
                Payam.Show("کلیه قبوض شارژ بروز رسانی شدند.");
            }
        }

        private void buttonItem44_Click_1(object sender, EventArgs e)
        {
            new ManagementReports.Frm_Arzesh_afzoodeh().ShowDialog();
        }

        private void buttonItem35_Click(object sender, EventArgs e)
        {

            new ManagementReports.New_Report.FrmSharj_bed_besForAll("ab").ShowDialog();

        }

        private void buttonItem32_Click_3(object sender, EventArgs e)
        {
            new ManagementReports.New_Report.FrmSharj_bed_besForAll("sharj").ShowDialog();
        }

        private void buttonItem35_Click_1(object sender, EventArgs e)
        {
            new ManagementReports.New_Report.FrmSharj_bed_besForAll("ab").ShowDialog();

        }

        private void buttonItem36_Click_3(object sender, EventArgs e)
        {
            new ManagementReports.New_Report.frmMoshtarek_bes_bes().ShowDialog();

        }

        private void buttonItem39_Click_2(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
                new Factor.frm_factor_abbasAbad().ShowDialog();
            else
                new Factor.frm_factor().ShowDialog();

        }

        private void fd_Click_1(object sender, EventArgs e)
        {
            new Management.frm_factors_report().ShowDialog();

        }

        private void buttonItem41_Click_1(object sender, EventArgs e)
        {
            new Management.frm_factor_history().ShowDialog();

        }

        private void buttonItem21_Click(object sender, EventArgs e)
        {
            new Frm_ghobooz_chap(Panel_lbl_ab_doreh.Text, "اب", "ab").ShowDialog();
        }


        private void RbAb_Click(object sender, EventArgs e)
        {

        }

        private void txt_sharj_new_pardakht_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txt_sharj_new_pardakht.Text == "")
                    txt_ab_new_pardakht.Focus();
                else
                    btn_sharj_new_pardakht.PerformClick();
            }
        }

        private void txt_ab_new_pardakht_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_pardakht_ab_now.PerformClick();
        }

        private void buttonItem22_Click(object sender, EventArgs e)
        {
            new Management.frm_Ghati_ab_sharj().ShowDialog();
        }

        private void buttonItem38_Click_1(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
            {
                Payam.Show("لطفا ابتدا به نگهبانی اعلام کنید که برنامه مربوطه را ببندد ");
                Classes.ClsMain.ExecuteNoneQuery("exec negabani ");
                Payam.Show("عملیات بروز رسانی با موفقیت انجام شد ");
            }
            else
                Payam.Show("این قابلیت هم اکنون در شهرک شما پیاده سازی نشده است . ");

        }

        private void txt_sharj_new_pardakht_KeyDown_2(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txt_sharj_new_pardakht.Text == "")
                    txt_ab_new_pardakht.Focus();
                else
                    btn_sharj_new_pardakht.PerformClick();
            }
        }

        private void txt_ab_new_pardakht_KeyDown_2(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_pardakht_ab_now.Focus();
        }

        private void buttonItem40_Click(object sender, EventArgs e)
        {
            new Bargeh_khrorooj.frm_bargeh_settings().ShowDialog();

        }

        private void buttonItem38_Click_2(object sender, EventArgs e)
        {
            new Management.frm_ghabz_sodoor_advanced().ShowDialog();
        }

        private void buttonItem42_Click_2(object sender, EventArgs e)
        {
            new FrmAboutUS().ShowDialog();
        }

        private void buttonItem43_Click_2(object sender, EventArgs e)
        {
            new Internet.Frm_transfer_info_to_web().ShowDialog();
        }

        private void buttonItem45_Click_1(object sender, EventArgs e)
        {
            new Factor.frm_factor_report_Amari().ShowDialog();
        }
    }



}