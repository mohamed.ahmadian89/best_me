using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Globalization;

namespace CWMS
{
    public partial class frmMain_search : MyMetroForm
    {
        public DataTable dtInfo;
        public FrmMain FrmMainForm = null;
        public frmMain_search(FrmMain frm, DataTable dtInfoTemp)
        {
            InitializeComponent();
            dtInfo = dtInfoTemp;
            dataGridViewX1.DataSource = dtInfo;

            FrmMainForm = frm;
        }

        private void dataGridViewX1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewX1_Click(object sender, EventArgs e)
        {
            
            //FrmMainForm.SetGharadadAndSerach(dataGridViewX1.SelectedRows[0].Index.ToString(),dataGridViewX1.SelectedRows[0].Cells[0].Value.ToString(), dtInfo);
            //this.Close();
        }

        private void frmMain_search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'cWMSDataSet.moshtarekin' table. You can move, or remove it, as needed.


        }

        private void dataGridViewX1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FrmMainForm.SetGharadadAndSerach(Convert.ToInt32(dataGridViewX1.SelectedRows[0].Cells[0].Value), dtInfo);
                this.Close();
            }
        }

        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FrmMainForm.SetGharadadAndSerach(Convert.ToInt32(dataGridViewX1.SelectedRows[0].Cells[0].Value), dtInfo);
            this.Close(); 
        }
    }
}