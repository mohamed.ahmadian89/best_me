﻿namespace CWMS.Management
{
    partial class frm_Edit_ghabz_advanced
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find_company = new DevComponents.DotNetBar.ButtonX();
            this.txt_pardakhti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lbl_doreh = new DevComponents.DotNetBar.LabelX();
            this.cmb_ghabz_type = new System.Windows.Forms.ComboBox();
            this.cmb_doreh = new System.Windows.Forms.ComboBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_find_company);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.lbl_doreh);
            this.groupPanel2.Controls.Add(this.txt_pardakhti);
            this.groupPanel2.Controls.Add(this.cmb_ghabz_type);
            this.groupPanel2.Controls.Add(this.cmb_doreh);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(22, 13);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(799, 131);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 4;
            this.groupPanel2.Text = "ثبت پرداختی به ازا کلیه قبوض دوره ";
            // 
            // btn_find_company
            // 
            this.btn_find_company.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_company.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_find_company.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_company.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_company.Location = new System.Drawing.Point(16, 64);
            this.btn_find_company.Margin = new System.Windows.Forms.Padding(4);
            this.btn_find_company.Name = "btn_find_company";
            this.btn_find_company.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_company.Size = new System.Drawing.Size(389, 27);
            this.btn_find_company.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_company.Symbol = "";
            this.btn_find_company.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_company.SymbolSize = 9F;
            this.btn_find_company.TabIndex = 1;
            this.btn_find_company.Text = "بر روی کلیه قبوض مربوطه اعمال گردد";
            this.btn_find_company.Click += new System.EventHandler(this.btn_find_company_Click);
            // 
            // txt_pardakhti
            // 
            this.txt_pardakhti.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_pardakhti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhti.Border.Class = "TextBoxBorder";
            this.txt_pardakhti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhti.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhti.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhti.Location = new System.Drawing.Point(70, 20);
            this.txt_pardakhti.Margin = new System.Windows.Forms.Padding(4);
            this.txt_pardakhti.Name = "txt_pardakhti";
            this.txt_pardakhti.PreventEnterBeep = true;
            this.txt_pardakhti.Size = new System.Drawing.Size(138, 28);
            this.txt_pardakhti.TabIndex = 0;
            // 
            // lbl_doreh
            // 
            this.lbl_doreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_doreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_doreh.ForeColor = System.Drawing.Color.Black;
            this.lbl_doreh.Location = new System.Drawing.Point(436, 20);
            this.lbl_doreh.Name = "lbl_doreh";
            this.lbl_doreh.Size = new System.Drawing.Size(73, 28);
            this.lbl_doreh.TabIndex = 180;
            this.lbl_doreh.Text = "دوره :";
            // 
            // cmb_ghabz_type
            // 
            this.cmb_ghabz_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_ghabz_type.BackColor = System.Drawing.Color.White;
            this.cmb_ghabz_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ghabz_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_ghabz_type.FormattingEnabled = true;
            this.cmb_ghabz_type.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_ghabz_type.Location = new System.Drawing.Point(525, 20);
            this.cmb_ghabz_type.Name = "cmb_ghabz_type";
            this.cmb_ghabz_type.Size = new System.Drawing.Size(97, 28);
            this.cmb_ghabz_type.TabIndex = 179;
            this.cmb_ghabz_type.SelectedIndexChanged += new System.EventHandler(this.cmb_ghabz_type_SelectedIndexChanged);
            // 
            // cmb_doreh
            // 
            this.cmb_doreh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_doreh.FormattingEnabled = true;
            this.cmb_doreh.Location = new System.Drawing.Point(338, 20);
            this.cmb_doreh.Name = "cmb_doreh";
            this.cmb_doreh.Size = new System.Drawing.Size(97, 28);
            this.cmb_doreh.TabIndex = 178;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(634, 20);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(73, 28);
            this.labelX5.TabIndex = 177;
            this.labelX5.Text = "نوع فاکتور :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(215, 20);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(73, 28);
            this.labelX1.TabIndex = 181;
            this.labelX1.Text = "مبلغ پرداختی :";
            // 
            // frm_Edit_ghabz_advanced
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 159);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "frm_Edit_ghabz_advanced";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تغییرات کلی بر روی قبوض";
            this.Load += new System.EventHandler(this.frm_Edit_ghabz_advanced_Load);
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_find_company;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_pardakhti;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX lbl_doreh;
        private System.Windows.Forms.ComboBox cmb_ghabz_type;
        private System.Windows.Forms.ComboBox cmb_doreh;
        private DevComponents.DotNetBar.LabelX labelX5;
    }
}