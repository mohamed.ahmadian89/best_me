﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Management
{
    public partial class frm_ghabz_sodoor_advanced : MyMetroForm
    {
        DataTable dt_sharj_doreh, dt_ab_doreh;
        public frm_ghabz_sodoor_advanced()
        {
            InitializeComponent();
            dt_sharj_doreh = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode asc");
            dt_ab_doreh = Classes.ClsMain.GetDataTable("select * from ab_doreh order by dcode asc");

        }

        private void frm_ghabz_sodoor_advanced_Load(object sender, EventArgs e)
        {

        }

        private void btn_find_company_Click(object sender, EventArgs e)
        {
            if (txt_find_moshtarek.Text.Trim() == "")
            {
                txt_find_moshtarek.Focus();
                return;
            }
            try
            {
                DataTable dt_moshtarek_info = Classes.ClsMain.GetDataTable("select gharardad,co_name from moshtarekin where gharardad=" + txt_find_moshtarek.Text);
                lbl_gharardad.Text = txt_find_moshtarek.Text;
                lbl_co_name.Text = dt_moshtarek_info.Rows[0]["co_name"].ToString();



            }
            catch (Exception)
            {
                Payam.Show("لطفا شماره قرارداد را به درستی وارد نمایید");
                txt_find_moshtarek.SelectAll();
                txt_find_moshtarek.Focus();
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با حذف کلیه قبوض قبلی مشترک موافق هستید؟") == DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from ab_ghabz where gharardad=" + lbl_gharardad.Text);

                Classes.clsAb ab_class = new Classes.clsAb();
                int cnt = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select count(*) from kontor_khan where gharardad=" + lbl_gharardad.Text));
                if (cnt == 0)
                {
                    Classes.ClsMain.ChangeCulture("e");
                    Classes.ClsMain.ExecuteNoneQuery("insert into kontor_khan (gharardad,tarikh,shomareh_kontor) values "
                        + " (" + lbl_gharardad.Text + ",'" + DateTime.Now + "'" + ",0)"
                        );
                }
                else
                {
                    Classes.ClsMain.ExecuteNoneQuery("update kontor_khan set shomareh_kontor=0 where gharardad=" + lbl_gharardad.Text);

                }


                Classes.ClsMain.ChangeCulture("e");
                Classes.ClsMain.ExecuteNoneQuery("delete ab_bedehi_bestankar_First_time;insert into ab_bedehi_bestankar_First_time values (" + lbl_gharardad.Text + ",0,0,0,'" + DateTime.Now + "')");

                for (int i = 0; i < dt_sharj_doreh.Rows.Count; i++)
                {
                    Classes.clsGhabz ghabz_class = new Classes.clsGhabz();
                    ghabz_class.tarefe_ab = -10;
                    ghabz_class.doreh = Convert.ToInt32(dt_sharj_doreh.Rows[i]["dcode"]);
                    ghabz_class.gharardad = Convert.ToInt32(lbl_gharardad.Text);
                    ab_class.Generate_Ghabz(ghabz_class);
                }
                Payam.Show("کلیه قبض های آب مشترک با موفقیت ایجاد و در سیستم ذخیره شدند");
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            new Management.frm_Edit_ghabz_advanced().ShowDialog();
        }

        private void txt_find_moshtarek_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                btn_find_company.PerformClick();
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با حذف کلیه قبوض قبلی مشترک موافق هستید؟") == DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from sharj_ghabz where gharardad=" + lbl_gharardad.Text);
                Classes.clsSharj sharj_class = new Classes.clsSharj();

                for (int i = 0; i < dt_sharj_doreh.Rows.Count; i++)
                {
                    Classes.clsSharjGhabz ghabz_class = new Classes.clsSharjGhabz();
                    ghabz_class.Tarefe = -10;
                    ghabz_class.doreh = Convert.ToInt32(dt_sharj_doreh.Rows[i]["dcode"]);
                    ghabz_class.gharardad = Convert.ToInt32(lbl_gharardad.Text);
                    sharj_class.Generate_Ghabz(ghabz_class);
                }
                Payam.Show("کلیه قبض های شارژ مشترک با موفقیت ایجاد و در سیستم ذخیره شدند");
            }
        }
    }
}
