﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Management
{
    public partial class frm_user_permission : MyMetroForm
    {
        public frm_user_permission()
        {
            InitializeComponent();
        }
        public frm_user_permission(string username,string fullname)
        {
            InitializeComponent();
            admin_userTableAdapter.FillByUsername(user_Dataset.admin_user, username);
            txtusername.Text = username;
            txtFullname.Text = fullname;
        }

        private void frm_user_permission_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset.admin_user' table. You can move, or remove it, as needed.
        }


        void ChangeChildStatus(CheckBox sender)
        {
            Control ParentObject = ((Control)sender).Parent;
            foreach (Control item in ParentObject.Controls)
            {
                if (item.GetType() == typeof(CheckBox))
                    if (sender.Checked)
                        ((CheckBox)item).Enabled = true;
                    else
                        ((CheckBox)item).Checked = ((CheckBox)item).Enabled = sender.Checked;

            }
            sender.Enabled = true;

        }
        void Selection(Control sender,bool state)
        {
            Control ParentObject = ((Control)sender).Parent;
            foreach (Control item in ParentObject.Controls)
            {
                if (item.GetType() == typeof(CheckBox))

                    ((CheckBox)item).Checked = ((CheckBox)item).Enabled = state;

            }
        }


        private void chmodiriat_shahrak_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);
        }

        private void chBargeh_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);
        }

        private void chInternet_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);

        }

        private void chSharj_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);

        }

        private void chAb_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);

        }

        private void chGozareshatmodiriat_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);

        }



        private void chMoshtarekin_CheckedChanged(object sender, EventArgs e)
        {
            ChangeChildStatus((CheckBox)sender);

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 31, "تغییر در مجوزهای کاربران مدیریت");
            
            adminuserBindingSource.EndEdit();
            admin_userTableAdapter.Update(user_Dataset.admin_user); 
            Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);

        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);

        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);

        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, true);

        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            Selection((Control)sender, false);

        }


    }
}