﻿namespace CWMS.Management
{
    partial class frm_factor_history
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpByGharardad = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.user_Dataset = new CWMS.Management.user_Dataset();
            this.factorhistoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.factor_historyTableAdapter = new CWMS.Management.user_DatasetTableAdapters.factor_historyTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghabztypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.successDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpByGharardad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.factorhistoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grpByGharardad
            // 
            this.grpByGharardad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpByGharardad.Controls.Add(this.dataGridView1);
            this.grpByGharardad.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpByGharardad.Location = new System.Drawing.Point(3, 8);
            this.grpByGharardad.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.grpByGharardad.Name = "grpByGharardad";
            this.grpByGharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpByGharardad.Size = new System.Drawing.Size(1092, 322);
            // 
            // 
            // 
            this.grpByGharardad.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpByGharardad.Style.BackColorGradientAngle = 90;
            this.grpByGharardad.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpByGharardad.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderBottomWidth = 1;
            this.grpByGharardad.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpByGharardad.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderLeftWidth = 1;
            this.grpByGharardad.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderRightWidth = 1;
            this.grpByGharardad.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderTopWidth = 1;
            this.grpByGharardad.Style.CornerDiameter = 4;
            this.grpByGharardad.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpByGharardad.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpByGharardad.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpByGharardad.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpByGharardad.TabIndex = 169;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.ghabztypeDataGridViewTextBoxColumn,
            this.tozihatDataGridViewTextBoxColumn,
            this.startDataGridViewTextBoxColumn,
            this.finishDataGridViewTextBoxColumn,
            this.successDataGridViewTextBoxColumn,
            this.failDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.factorhistoryBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1053, 310);
            this.dataGridView1.TabIndex = 0;
            // 
            // user_Dataset
            // 
            this.user_Dataset.DataSetName = "user_Dataset";
            this.user_Dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // factorhistoryBindingSource
            // 
            this.factorhistoryBindingSource.DataMember = "factor_history";
            this.factorhistoryBindingSource.DataSource = this.user_Dataset;
            // 
            // factor_historyTableAdapter
            // 
            this.factor_historyTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.FillWeight = 50F;
            this.idDataGridViewTextBoxColumn.HeaderText = "شناسه";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 30F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            // 
            // ghabztypeDataGridViewTextBoxColumn
            // 
            this.ghabztypeDataGridViewTextBoxColumn.DataPropertyName = "ghabz_type";
            this.ghabztypeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.ghabztypeDataGridViewTextBoxColumn.HeaderText = "نوع قبض";
            this.ghabztypeDataGridViewTextBoxColumn.Name = "ghabztypeDataGridViewTextBoxColumn";
            // 
            // tozihatDataGridViewTextBoxColumn
            // 
            this.tozihatDataGridViewTextBoxColumn.DataPropertyName = "tozihat";
            this.tozihatDataGridViewTextBoxColumn.FillWeight = 300F;
            this.tozihatDataGridViewTextBoxColumn.HeaderText = "توضیحات";
            this.tozihatDataGridViewTextBoxColumn.Name = "tozihatDataGridViewTextBoxColumn";
            // 
            // startDataGridViewTextBoxColumn
            // 
            this.startDataGridViewTextBoxColumn.DataPropertyName = "start";
            this.startDataGridViewTextBoxColumn.FillWeight = 40F;
            this.startDataGridViewTextBoxColumn.HeaderText = "شروع";
            this.startDataGridViewTextBoxColumn.MinimumWidth = 40;
            this.startDataGridViewTextBoxColumn.Name = "startDataGridViewTextBoxColumn";
            // 
            // finishDataGridViewTextBoxColumn
            // 
            this.finishDataGridViewTextBoxColumn.DataPropertyName = "finish";
            this.finishDataGridViewTextBoxColumn.FillWeight = 40F;
            this.finishDataGridViewTextBoxColumn.HeaderText = "پایان";
            this.finishDataGridViewTextBoxColumn.MinimumWidth = 40;
            this.finishDataGridViewTextBoxColumn.Name = "finishDataGridViewTextBoxColumn";
            // 
            // successDataGridViewTextBoxColumn
            // 
            this.successDataGridViewTextBoxColumn.DataPropertyName = "success";
            this.successDataGridViewTextBoxColumn.FillWeight = 40F;
            this.successDataGridViewTextBoxColumn.HeaderText = "صادرشده";
            this.successDataGridViewTextBoxColumn.MinimumWidth = 40;
            this.successDataGridViewTextBoxColumn.Name = "successDataGridViewTextBoxColumn";
            // 
            // failDataGridViewTextBoxColumn
            // 
            this.failDataGridViewTextBoxColumn.DataPropertyName = "fail";
            this.failDataGridViewTextBoxColumn.FillWeight = 40F;
            this.failDataGridViewTextBoxColumn.HeaderText = "خطا";
            this.failDataGridViewTextBoxColumn.Name = "failDataGridViewTextBoxColumn";
            // 
            // frm_factor_history
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 338);
            this.Controls.Add(this.grpByGharardad);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "frm_factor_history";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_factor_history_Load);
            this.grpByGharardad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.factorhistoryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpByGharardad;
        private System.Windows.Forms.DataGridView dataGridView1;
        private user_Dataset user_Dataset;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjdcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource factorhistoryBindingSource;
        private user_DatasetTableAdapters.factor_historyTableAdapter factor_historyTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghabztypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finishDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn successDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn failDataGridViewTextBoxColumn;
    }
}