using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Management
{
    public partial class frm_user_access : MyMetroForm
    {
        public frm_user_access()
        {
            InitializeComponent();
        }

        private void frm_user_access_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset1.User_Activity' table. You can move, or remove it, as needed.
            this.user_ActivityTableAdapter.Fill(this.user_Dataset1.User_Activity);
            // TODO: This line of code loads data into the 'user_Dataset.admin_user_access_report' table. You can move, or remove it, as needed.

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            userActivityBindingSource.Filter = "username like '%" + txtfindbyusername.Text + "%'";
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            userActivityBindingSource.Filter = "action like '%" + txtfindByBakhsh.Text + "%'";

        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            userActivityBindingSource.Filter = "tozihat like '%" + txtfindbyAction.Text + "%'";

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            userActivityBindingSource.Filter = "tarikh >='" + txtstart.SelectedDateTime + "' and tarikh<='" + Convert.ToDateTime(txtend.SelectedDateTime).AddDays(1) + "'";

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            userActivityBindingSource.RemoveFilter();
        }

        private void txtfindbyusername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX2.PerformClick();
        }

        private void txtfindByBakhsh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX1.PerformClick();
        }

        private void txtfindbyAction_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX4.PerformClick();
        }
    }
}