﻿namespace CWMS.Management
{
    partial class Frm_Sodoor_factor_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpByGharardad = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.grp_error = new System.Windows.Forms.GroupBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lst_gharardad = new System.Windows.Forms.ListBox();
            this.txt_error = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.txt_factor_count = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_gharardad_count = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.grpByGharardad.SuspendLayout();
            this.grp_error.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpByGharardad
            // 
            this.grpByGharardad.BackColor = System.Drawing.Color.Transparent;
            this.grpByGharardad.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpByGharardad.Controls.Add(this.grp_error);
            this.grpByGharardad.Controls.Add(this.buttonX1);
            this.grpByGharardad.Controls.Add(this.txt_factor_count);
            this.grpByGharardad.Controls.Add(this.txt_gharardad_count);
            this.grpByGharardad.Controls.Add(this.labelX2);
            this.grpByGharardad.Controls.Add(this.labelX13);
            this.grpByGharardad.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpByGharardad.Location = new System.Drawing.Point(13, 29);
            this.grpByGharardad.Margin = new System.Windows.Forms.Padding(4);
            this.grpByGharardad.Name = "grpByGharardad";
            this.grpByGharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpByGharardad.Size = new System.Drawing.Size(822, 304);
            // 
            // 
            // 
            this.grpByGharardad.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpByGharardad.Style.BackColorGradientAngle = 90;
            this.grpByGharardad.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpByGharardad.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderBottomWidth = 1;
            this.grpByGharardad.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpByGharardad.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderLeftWidth = 1;
            this.grpByGharardad.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderRightWidth = 1;
            this.grpByGharardad.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderTopWidth = 1;
            this.grpByGharardad.Style.CornerDiameter = 4;
            this.grpByGharardad.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpByGharardad.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpByGharardad.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpByGharardad.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpByGharardad.TabIndex = 168;
            this.grpByGharardad.Click += new System.EventHandler(this.grpByGharardad_Click);
            // 
            // grp_error
            // 
            this.grp_error.Controls.Add(this.labelX3);
            this.grp_error.Controls.Add(this.lst_gharardad);
            this.grp_error.Controls.Add(this.txt_error);
            this.grp_error.Controls.Add(this.labelX1);
            this.grp_error.Controls.Add(this.labelX6);
            this.grp_error.Location = new System.Drawing.Point(34, 58);
            this.grp_error.Name = "grp_error";
            this.grp_error.Size = new System.Drawing.Size(712, 188);
            this.grp_error.TabIndex = 175;
            this.grp_error.TabStop = false;
            this.grp_error.Text = "بروز خطا";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(46, 149);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(614, 38);
            this.labelX3.TabIndex = 180;
            this.labelX3.Text = "لطفا در صورتی که سیستم نتوانسته است برای همه قراردادهای مربوطه فاکتور صادر کند ، " +
    "با پشتیبان تماس حاصل نمایید تا هر چه سریعتر نسبت  به رفع مشکل اقدام نمایند";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX3.WordWrap = true;
            // 
            // lst_gharardad
            // 
            this.lst_gharardad.FormattingEnabled = true;
            this.lst_gharardad.ItemHeight = 20;
            this.lst_gharardad.Location = new System.Drawing.Point(46, 58);
            this.lst_gharardad.Name = "lst_gharardad";
            this.lst_gharardad.Size = new System.Drawing.Size(388, 84);
            this.lst_gharardad.TabIndex = 179;
            // 
            // txt_error
            // 
            this.txt_error.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_error.Border.Class = "TextBoxBorder";
            this.txt_error.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_error.DisabledBackColor = System.Drawing.Color.White;
            this.txt_error.ForeColor = System.Drawing.Color.Black;
            this.txt_error.Location = new System.Drawing.Point(524, 27);
            this.txt_error.Margin = new System.Windows.Forms.Padding(4);
            this.txt_error.Name = "txt_error";
            this.txt_error.PreventEnterBeep = true;
            this.txt_error.Size = new System.Drawing.Size(68, 28);
            this.txt_error.TabIndex = 177;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(583, 28);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(66, 27);
            this.labelX1.TabIndex = 178;
            this.labelX1.Text = "تعداد خطا :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(121, 24);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(313, 27);
            this.labelX6.TabIndex = 176;
            this.labelX6.Text = "قراردادهایی که سیستم نتوانست برای آنها فاکتور صادر کند :";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(265, 256);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Size = new System.Drawing.Size(320, 31);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 174;
            this.buttonX1.Text = "مشاهده نمودم";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // txt_factor_count
            // 
            this.txt_factor_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_factor_count.Border.Class = "TextBoxBorder";
            this.txt_factor_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_factor_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_factor_count.ForeColor = System.Drawing.Color.Black;
            this.txt_factor_count.Location = new System.Drawing.Point(169, 23);
            this.txt_factor_count.Margin = new System.Windows.Forms.Padding(4);
            this.txt_factor_count.Name = "txt_factor_count";
            this.txt_factor_count.PreventEnterBeep = true;
            this.txt_factor_count.Size = new System.Drawing.Size(68, 28);
            this.txt_factor_count.TabIndex = 1;
            // 
            // txt_gharardad_count
            // 
            this.txt_gharardad_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_gharardad_count.Border.Class = "TextBoxBorder";
            this.txt_gharardad_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_gharardad_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_gharardad_count.ForeColor = System.Drawing.Color.Black;
            this.txt_gharardad_count.Location = new System.Drawing.Point(441, 23);
            this.txt_gharardad_count.Margin = new System.Windows.Forms.Padding(4);
            this.txt_gharardad_count.Name = "txt_gharardad_count";
            this.txt_gharardad_count.PreventEnterBeep = true;
            this.txt_gharardad_count.Size = new System.Drawing.Size(69, 28);
            this.txt_gharardad_count.TabIndex = 0;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(245, 24);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(149, 27);
            this.labelX2.TabIndex = 29;
            this.labelX2.Text = "تعداد فاکتورهای صادر شده :";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(508, 24);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(179, 27);
            this.labelX13.TabIndex = 169;
            this.labelX13.Text = "تعداد قراردادهای انتخاب شده :";
            // 
            // Frm_Sodoor_factor_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(860, 346);
            this.Controls.Add(this.grpByGharardad);
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Sodoor_factor_Report";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "جزئیات فرایند صدور فاکتور";
            this.Load += new System.EventHandler(this.Frm_Sodoor_factor_Report_Load);
            this.grpByGharardad.ResumeLayout(false);
            this.grp_error.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpByGharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_factor_count;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_gharardad_count;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX13;
        private System.Windows.Forms.GroupBox grp_error;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.ListBox lst_gharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_error;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}