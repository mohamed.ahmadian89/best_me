﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace CWMS.Management
{
    public partial class Frm_ArzeshAfzzoddeh_Problem : Form
    {
        public Frm_ArzeshAfzzoddeh_Problem(List<string[]> lst)
        {
            InitializeComponent();
            for (int i = 0; i < lst.Count; i++)
            {
                dgv_ab_result.Rows.Add(lst[i]);
            }
        }

        public Frm_ArzeshAfzzoddeh_Problem()
        {
            InitializeComponent();
            List<string[]> lst = new List<string[]>();
            string[] str = new string[3];
            str[0] = "test1";
            str[1] = "test2";
            str[2] = "test3";

            lst.Add(str);
            lst.Add(str);
            lst.Add(str);
            lst.Add(str);
            
            for (int i = 0; i < lst.Count; i++)
            {
                dgv_ab_result.Rows.Add(lst[i]);
            }
        }


        private void Frm_ArzeshAfzzoddeh_Problem_Load(object sender, EventArgs e)
        {

        }

        private void copyAlltoClipboard()
        {
            dgv_ab_result.SelectAll();
            DataObject dataObj = dgv_ab_result.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }
    }
}
