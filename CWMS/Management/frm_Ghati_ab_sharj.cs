﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace CWMS.Management
{
    public partial class frm_Ghati_ab_sharj : MyMetroForm
    {
        public frm_Ghati_ab_sharj()
        {
            InitializeComponent();
        }
        int max_sharj_doreh, max_ab_doreh;
        private void frm_Ghati_ab_sharj_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset.temp_ghati_ab_sharj' table. You can move, or remove it, as needed.
            this.temp_ghati_ab_sharjTableAdapter.Fill(this.user_Dataset.temp_ghati_ab_sharj);
            max_ab_doreh = Classes.clsDoreh.Get_last_Ab_dcode();
            txt_ab_dcode.Text = max_ab_doreh.ToString();
            max_sharj_doreh = Classes.clsDoreh.Get_last_Sharj_dcode();
            txt_sharj_dcode.Text = max_sharj_doreh.ToString();

            txt_bedehkaran.Text = user_Dataset.temp_ghati_ab_sharj.Rows.Count.ToString();

        }

        private void labelX3_Click(object sender, EventArgs e)
        {

        }


        static void Generate_ghati_members(object dt1)
        {
            try
            {


                object[] o =(object[]) dt1;
                user_Dataset.temp_ghati_ab_sharjDataTable dt = (user_Dataset.temp_ghati_ab_sharjDataTable)o[0];
                FolderBrowserDialog opendiag = new FolderBrowserDialog();

                List<Dictionary<string, object>> list_of_info = new List<Dictionary<string, object>>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Dictionary<string, object> info = new Dictionary<string, object>();
                    info["gharardad"] = dt.Rows[i]["gharardad"].ToString();
                    info["co_name"] = dt.Rows[i]["co_name"].ToString();
                    info["sharj_mandeh"] = dt.Rows[i]["sharj_mandeh"].ToString();
                    info["ab_mandeh"] = dt.Rows[i]["ab_mandeh"].ToString();
                    info["address"] = dt.Rows[i]["address"].ToString();

                    list_of_info.Add(info);
                }

                string input_json = JsonConvert.SerializeObject(list_of_info);


                System.IO.File.WriteAllText(o[1].ToString() + "\\ghati.json", input_json, Encoding.UTF8);
                System.Diagnostics.Process.Start(o[1].ToString());

            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert);
            }

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog op = new FolderBrowserDialog();
            if (op.ShowDialog() == DialogResult.OK)
            {
                Payam.Show("لطفا تا اتمام تولید لیست خروجی چند لحظه صبر نمایید");
                object[] o = new object[2];
                System.Threading.Thread th = new System.Threading.Thread(
                    new System.Threading.ParameterizedThreadStart(
                    Generate_ghati_members));
                o[0] = user_Dataset.temp_ghati_ab_sharj;
                if (System.IO.Directory.Exists(op.SelectedPath + "\\لیست قطعی"))
                    System.IO.Directory.Delete(op.SelectedPath + "\\لیست قطعی",true);
                System.IO.Directory.CreateDirectory(op.SelectedPath + "\\لیست قطعی");
                o[1] = op.SelectedPath + "\\لیست قطعی";
                th.Start(o);
            }
        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txt_find.Text.Trim() != "")
                {
                    tempghatiabsharjBindingSource.Filter = "gharardad=" + txt_find.Text;
                }
                else
                    tempghatiabsharjBindingSource.RemoveFilter();
            }
            catch (Exception)
            {
                Payam.Show("لطفا شماره قرارداد را به صورت صحیح وارد نمایید");
                txt_find.SelectAll();
                txt_find.Focus();
            }


        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_find.Text.Trim() != "")
                {
                    tempghatiabsharjBindingSource.Filter = "gharardad=" + txt_find.Text;
                }
                else
                    tempghatiabsharjBindingSource.RemoveFilter();
            }
            catch (Exception)
            {
                Payam.Show("لطفا شماره قرارداد را به صورت صحیح وارد نمایید");
                txt_find.SelectAll();
                txt_find.Focus();
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با حذف مشترک مورد نظر موافق هستید?") == DialogResult.Yes)
            {
                if (tempghatiabsharjBindingSource.Count > 0)
                {
                    tempghatiabsharjBindingSource.RemoveCurrent();
                    temp_ghati_ab_sharjTableAdapter.Update(user_Dataset.temp_ghati_ab_sharj);
                    Payam.Show("مشترک مورد نظر از لیست حذف شد");
                    txt_bedehkaran.Text = user_Dataset.temp_ghati_ab_sharj.Rows.Count.ToString();
                }
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            int sharj_dcode = 0, ab_dcode = 0;
            try
            {
                sharj_dcode = Convert.ToInt32(txt_sharj_dcode.Text);
            }
            catch (Exception)
            {
                sharj_dcode = max_sharj_doreh;
                txt_sharj_dcode.Text = max_sharj_doreh.ToString();
            }

            try
            {
                ab_dcode = Convert.ToInt32(txt_ab_dcode.Text);
            }
            catch (Exception)
            {
                ab_dcode = max_ab_doreh;
                txt_ab_dcode.Text = max_ab_doreh.ToString();
            }

            Classes.ClsMain.ExecuteNoneQuery("delete from temp_ghati_ab_sharj;"
                 +
                 @"insert into temp_ghati_ab_sharj
                SELECT        gharardad, co_name, sharj_mandeh, ab_mandeh, address
                    FROM            dbo.ghati_func(" + txt_sharj_dcode.Text + ", " + txt_ab_dcode.Text + ") AS ghati_func_1"
                    + @"
                    WHERE        (sharj_mandeh = 0) AND (ab_mandeh <> 0) OR
                         (sharj_mandeh <> 0) AND (ab_mandeh = 0) OR
                         (sharj_mandeh <> 0) AND (ab_mandeh <> 0)"
                );

            temp_ghati_ab_sharjTableAdapter.Fill(user_Dataset.temp_ghati_ab_sharj);

            //ghati_ab_sharjTableAdapter.Fill(user_Dataset.temp_ghati_ab_sharj, sharj_dcode, ab_dcode);



            txt_bedehkaran.Text = user_Dataset.temp_ghati_ab_sharj.Rows.Count.ToString();
        }
    }
}
