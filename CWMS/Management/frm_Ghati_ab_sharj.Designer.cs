﻿namespace CWMS.Management
{
    partial class frm_Ghati_ab_sharj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Ghati_ab_sharj));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.txt_find = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.txt_bedehkaran = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_ab_dcode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.dgv_group = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjmandehDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abmandehDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempghatiabsharjBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.user_Dataset = new CWMS.Management.user_Dataset();
            this.txt_sharj_dcode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnCreate = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.temp_ghati_ab_sharjTableAdapter = new CWMS.Management.user_DatasetTableAdapters.temp_ghati_ab_sharjTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_group)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempghatiabsharjBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX3);
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.txt_find);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.txt_bedehkaran);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txt_ab_dcode);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.dgv_group);
            this.groupPanel1.Controls.Add(this.txt_sharj_dcode);
            this.groupPanel1.Controls.Add(this.btnCreate);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(3, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1054, 495);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "بدهکاران آب و شارژ";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(795, 401);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX3.Size = new System.Drawing.Size(249, 41);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Firebrick;
            this.buttonX3.SymbolSize = 12F;
            this.buttonX3.TabIndex = 78;
            this.buttonX3.Text = "حذف مشترک از لیست با نظر مدیریت";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(25, 72);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX2.Size = new System.Drawing.Size(40, 24);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 12F;
            this.buttonX2.TabIndex = 77;
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // txt_find
            // 
            this.txt_find.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find.Border.Class = "TextBoxBorder";
            this.txt_find.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find.ForeColor = System.Drawing.Color.Black;
            this.txt_find.Location = new System.Drawing.Point(76, 70);
            this.txt_find.Name = "txt_find";
            this.txt_find.PreventEnterBeep = true;
            this.txt_find.Size = new System.Drawing.Size(65, 26);
            this.txt_find.TabIndex = 75;
            this.txt_find.TextChanged += new System.EventHandler(this.textBoxX1_TextChanged);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(104, 72);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(87, 23);
            this.labelX4.TabIndex = 76;
            this.labelX4.Text = "قرارداد :";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(25, 438);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Size = new System.Drawing.Size(453, 24);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 74;
            this.buttonX1.Text = "ارسال اطلاعات به نرم افزار موبایل - سامانه قطعی های کنتور آب";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // txt_bedehkaran
            // 
            this.txt_bedehkaran.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_bedehkaran.Border.Class = "TextBoxBorder";
            this.txt_bedehkaran.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_bedehkaran.DisabledBackColor = System.Drawing.Color.White;
            this.txt_bedehkaran.ForeColor = System.Drawing.Color.Black;
            this.txt_bedehkaran.Location = new System.Drawing.Point(63, 405);
            this.txt_bedehkaran.Name = "txt_bedehkaran";
            this.txt_bedehkaran.PreventEnterBeep = true;
            this.txt_bedehkaran.ReadOnly = true;
            this.txt_bedehkaran.Size = new System.Drawing.Size(88, 26);
            this.txt_bedehkaran.TabIndex = 72;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(144, 408);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(316, 23);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "تعداد مشترکینی که می بایست کنتورآب انها به دلیل بدهی قطع شود :";
            // 
            // txt_ab_dcode
            // 
            this.txt_ab_dcode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_dcode.Border.Class = "TextBoxBorder";
            this.txt_ab_dcode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_dcode.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_dcode.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_dcode.Location = new System.Drawing.Point(745, 74);
            this.txt_ab_dcode.Name = "txt_ab_dcode";
            this.txt_ab_dcode.PreventEnterBeep = true;
            this.txt_ab_dcode.Size = new System.Drawing.Size(65, 26);
            this.txt_ab_dcode.TabIndex = 70;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(777, 76);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(87, 23);
            this.labelX5.TabIndex = 71;
            this.labelX5.Text = "دوره آب:";
            // 
            // dgv_group
            // 
            this.dgv_group.AllowUserToAddRows = false;
            this.dgv_group.AllowUserToDeleteRows = false;
            this.dgv_group.AllowUserToResizeColumns = false;
            this.dgv_group.AllowUserToResizeRows = false;
            this.dgv_group.AutoGenerateColumns = false;
            this.dgv_group.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_group.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_group.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_group.ColumnHeadersHeight = 30;
            this.dgv_group.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.sharjmandehDataGridViewTextBoxColumn,
            this.abmandehDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn});
            this.dgv_group.DataSource = this.tempghatiabsharjBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_group.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_group.EnableHeadersVisualStyles = false;
            this.dgv_group.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_group.Location = new System.Drawing.Point(25, 111);
            this.dgv_group.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_group.Name = "dgv_group";
            this.dgv_group.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_group.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_group.RowHeadersVisible = false;
            this.dgv_group.RowTemplate.Height = 50;
            this.dgv_group.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_group.Size = new System.Drawing.Size(1019, 283);
            this.dgv_group.TabIndex = 69;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.FillWeight = 150F;
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام واحد صنعتی";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjmandehDataGridViewTextBoxColumn
            // 
            this.sharjmandehDataGridViewTextBoxColumn.DataPropertyName = "sharj_mandeh";
            dataGridViewCellStyle2.Format = "0,0";
            this.sharjmandehDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.sharjmandehDataGridViewTextBoxColumn.HeaderText = "بدهی شارژ";
            this.sharjmandehDataGridViewTextBoxColumn.Name = "sharjmandehDataGridViewTextBoxColumn";
            this.sharjmandehDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // abmandehDataGridViewTextBoxColumn
            // 
            this.abmandehDataGridViewTextBoxColumn.DataPropertyName = "ab_mandeh";
            dataGridViewCellStyle3.Format = "0,0";
            this.abmandehDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.abmandehDataGridViewTextBoxColumn.HeaderText = "بدهی آب";
            this.abmandehDataGridViewTextBoxColumn.Name = "abmandehDataGridViewTextBoxColumn";
            this.abmandehDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "address";
            this.addressDataGridViewTextBoxColumn.FillWeight = 200F;
            this.addressDataGridViewTextBoxColumn.HeaderText = "آدرس";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tempghatiabsharjBindingSource
            // 
            this.tempghatiabsharjBindingSource.DataMember = "temp_ghati_ab_sharj";
            this.tempghatiabsharjBindingSource.DataSource = this.user_Dataset;
            // 
            // user_Dataset
            // 
            this.user_Dataset.DataSetName = "user_Dataset";
            this.user_Dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txt_sharj_dcode
            // 
            this.txt_sharj_dcode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sharj_dcode.Border.Class = "TextBoxBorder";
            this.txt_sharj_dcode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sharj_dcode.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sharj_dcode.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_dcode.Location = new System.Drawing.Point(905, 74);
            this.txt_sharj_dcode.Name = "txt_sharj_dcode";
            this.txt_sharj_dcode.PreventEnterBeep = true;
            this.txt_sharj_dcode.Size = new System.Drawing.Size(65, 26);
            this.txt_sharj_dcode.TabIndex = 34;
            // 
            // btnCreate
            // 
            this.btnCreate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCreate.BackColor = System.Drawing.Color.Transparent;
            this.btnCreate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCreate.Location = new System.Drawing.Point(463, 74);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCreate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnCreate.Size = new System.Drawing.Size(253, 24);
            this.btnCreate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCreate.Symbol = "";
            this.btnCreate.SymbolColor = System.Drawing.Color.Green;
            this.btnCreate.SymbolSize = 12F;
            this.btnCreate.TabIndex = 38;
            this.btnCreate.Text = "نمایش بدهکاران مربوطه";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(28, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(997, 53);
            this.labelX2.TabIndex = 29;
            this.labelX2.Text = resources.GetString("labelX2.Text");
            this.labelX2.WordWrap = true;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(937, 76);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(87, 23);
            this.labelX3.TabIndex = 40;
            this.labelX3.Text = "دوره شارژ:";
            this.labelX3.Click += new System.EventHandler(this.labelX3_Click);
            // 
            // temp_ghati_ab_sharjTableAdapter
            // 
            this.temp_ghati_ab_sharjTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Ghati_ab_sharj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 509);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Ghati_ab_sharj";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "گزارش مربوط به مشترکین بدهکار آب و شارژ";
            this.Load += new System.EventHandler(this.frm_Ghati_ab_sharj_Load);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_group)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempghatiabsharjBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sharj_dcode;
        private DevComponents.DotNetBar.ButtonX btnCreate;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_group;
        private user_Dataset user_Dataset;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ab_dcode;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_bedehkaran;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.BindingSource tempghatiabsharjBindingSource;
        private user_DatasetTableAdapters.temp_ghati_ab_sharjTableAdapter temp_ghati_ab_sharjTableAdapter;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjmandehDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abmandehDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
    }
}