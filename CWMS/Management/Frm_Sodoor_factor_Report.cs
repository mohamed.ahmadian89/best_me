﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Management
{
    public partial class Frm_Sodoor_factor_Report : Form
    {
        public Frm_Sodoor_factor_Report(string gharardad_count,string Factor_count,List<string> errors)
        {
            InitializeComponent();
            txt_gharardad_count.Text = gharardad_count;
            txt_factor_count.Text = Factor_count;
            if(errors.Count==0)
            {
                grp_error.Visible = false;
            }
            else
            {
                grp_error.Visible = true;
                txt_error.Text = errors.Count.ToString();
                for (int i = 0; i < errors.Count; i++)
                {
                    lst_gharardad.Items.Add(errors[i].ToString());
                }
            }
        }

        private void grpByGharardad_Click(object sender, EventArgs e)
        {

        }

        private void Frm_Sodoor_factor_Report_Load(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
