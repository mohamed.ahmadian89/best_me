﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Management
{
    public partial class FrmNotification : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmNotification()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            txtstart.SelectedDateTime = txtend.SelectedDateTime = DateTime.Now;
        }

      

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            this.admin_notificationTableAdapter.Fill(this.user_Dataset.admin_notification);
            comboBoxEx1.SelectedIndex = 0;

        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            this.admin_notificationTableAdapter.FillByTarikh(this.user_Dataset.admin_notification, Convert.ToDateTime(txtstart.SelectedDateTime).AddDays(-1), txtend.SelectedDateTime);
            comboBoxEx1.SelectedIndex = 0;

        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            this.admin_notificationTableAdapter.FillBySubject(this.user_Dataset.admin_notification, "%" + txtfindByOnvan.Text + "%");
            comboBoxEx1.SelectedIndex = 0;

        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            this.admin_notificationTableAdapter.FillByMAtn(this.user_Dataset.admin_notification, "%" + txtfindbymatn.Text + "%");
            comboBoxEx1.SelectedIndex = 0;
        }

        private void comboBoxEx1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxEx1.SelectedIndex==0)
            {
                adminnotificationBindingSource.RemoveFilter();
            }
            else if(comboBoxEx1.SelectedIndex==1)
            {
                adminnotificationBindingSource.Filter = "state=True";
            }
            else
            {
                adminnotificationBindingSource.Filter = "state=false";

            }
        }

        //private void btnCreate_Click(object sender, EventArgs e)
        //{
        //    this.admin_notificationTableAdapter.FillByTarikh(this.user_Dataset.admin_notification, txtStart.SelectedDateTime, txtFinish.SelectedDateTime);

        //}
    }
}