﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Management
{
    public partial class frm_Edit_ghabz_advanced : MyMetroForm
    {
        DataTable dt_ab_doreh, dt_sharj_doreh;

        public frm_Edit_ghabz_advanced()
        {
            InitializeComponent();
            dt_ab_doreh = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            dt_sharj_doreh = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");

        }

        private void frm_Edit_ghabz_advanced_Load(object sender, EventArgs e)
        {

        }

        private void btn_find_company_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با اضافه کردن پرداختی برای کلیه قبوض موافق هستید؟") == DialogResult.Yes)
            {
                decimal mablagh = 0;
                int dcode = 0, is_ab = 0;
                string tableName = "ab_ghabz";
                try
                {
                    mablagh = Convert.ToDecimal(txt_pardakhti.Text);
                    dcode = Convert.ToInt32(cmb_doreh.Text);
                    if (cmb_ghabz_type.SelectedIndex == 0)
                    {
                        tableName = "ab_ghabz";
                        is_ab = 1;
                    }
                    else
                    {
                        tableName = "sharj_ghabz";
                        is_ab = 0;
                    }

                    DataTable dt_ghabz = Classes.ClsMain.GetDataTable("select gharardad,gcode,mablaghkol from " + tableName + " where dcode=" + dcode);
                    for (int i = 0; i < dt_ghabz.Rows.Count; i++)
                    {
                     
         
                        decimal bestankari_from_last_doreh = 0;
                        if (dcode == 1)
                        {
                            try
                            {
                                bestankari_from_last_doreh = Convert.ToDecimal(
                                    Classes.ClsMain.ExecuteScalar("select bestankar from " + (tableName.StartsWith("ab") ? "ab" : "sharj" + "_bedehi_bestankar_First_time where gharardad=" + dt_ghabz.Rows[i]["gharardad"].ToString())));


                            }
                            catch (Exception)
                            {
                                bestankari_from_last_doreh = 0;
                            }
                        }
                        else
                        {
                            try
                            {
                                bestankari_from_last_doreh = Convert.ToDecimal(
                                    Classes.ClsMain.ExecuteScalar("select bestankari from " + tableName + " where gharardad=" + dt_ghabz.Rows[i]["gharardad"].ToString() + " and dcode=" + (dcode - 1).ToString()));

                            }
                            catch (Exception)
                            {
                                bestankari_from_last_doreh = 0;
                            }
                        }

                        Classes.ClsMain.Add_mablagh_to_ghabz(dt_ghabz.Rows[i]["gcode"].ToString(), Convert.ToDecimal(dt_ghabz.Rows[i]["mablaghkol"])
                            , bestankari_from_last_doreh, tableName, dt_ghabz.Rows[i]["gharardad"].ToString(),Convert.ToDecimal(txt_pardakhti.Text),dcode.ToString());

                        //Classes.clsBedehkar_Bestankar cls = new Classes.clsBedehkar_Bestankar(
                        //    Convert.ToDecimal(dt_ghabz.Rows[i]["mablaghkol"]),
                        //    dt_ghabz.Rows[i]["gcode"].ToString(),
                        //    bestankari_from_last_doreh
                        //    , "");
                        //cls.Check_Bedehkar_Bestankar_Status();
                        //Classes.ClsMain.ExecuteNoneQuery("update " + tableName +  " set mande=" + cls.mandeh + ",vaziat=" + cls.vaziat_ghabz + ",bestankari=" + cls.bestankar + " where gcode=" + dt_ghabz.Rows[i]["gcode"]);



                    }
                    Payam.Show("عملیات با موفقیت به پایان رسید ");

                }
                catch (Exception)
                {
                    Payam.Show("لطفا مبلغ پرداختی را به درستی وارد نمایید");
                    txt_pardakhti.SelectAll();
                    txt_pardakhti.Focus();
                }
            }
        }

        private void cmb_ghabz_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_ghabz_type.SelectedIndex == 0)
                cmb_doreh.DataSource = dt_ab_doreh;
            else
                cmb_doreh.DataSource = dt_ab_doreh;
            cmb_doreh.DisplayMember = "dcode";
            cmb_doreh.ValueMember = "dcode";

        }
    }
}
