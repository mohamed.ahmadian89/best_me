﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;
using System.IO;
using System.Security.Permissions;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;

namespace CWMS.Management
{
    public partial class FrmBackupDB : DevComponents.DotNetBar.Metro.MetroForm
    {
        SqlConnection Conb, ConR;
        long index = 1;
        bool successBackup = false, successRestore = false;
        string Db_name = "cwms";

        public FrmBackupDB()
        {
            InitializeComponent();
            Conb = new SqlConnection(Classes.ClsMain.ConnectionStr);
            Db_name = Classes.ClsMain.ExecuteScalar("select db_name from setting").ToString();
            ConR = new SqlConnection(Classes.ClsMain.ConnectionStr.Replace(Db_name, "master"));

        }




        private void FrmBackupDB_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset.Restore_history' table. You can move, or remove it, as needed.
            this.restore_historyTableAdapter.Fill(this.user_Dataset.Restore_history);
            // TODO: This line of code loads data into the 'user_Dataset.backup_history' table. You can move, or remove it, as needed.
            this.backup_historyTableAdapter.Fill(this.user_Dataset.backup_history);

            if (user_Dataset.backup_history.Rows.Count != 0)
            {
                txtBackupPath.Text = user_Dataset.backup_history.Rows[0]["path"].ToString();
                btnBackup.Visible = true;
            }



            backupThread.ProgressChanged += new ProgressChangedEventHandler(
            delegate(object o, ProgressChangedEventArgs args)
            {
                // اگر خواستی پروگرس باز بذاری، این خط پایین رو از کامنت در بیار
                backupProgress.Value = args.ProgressPercentage;
            });

            backupThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate(object o, RunWorkerCompletedEventArgs args)
            {
                if (successBackup)
                {
                    this.backup_historyTableAdapter.Fill(this.user_Dataset.backup_history);
                    if (user_Dataset.backup_history.Rows.Count != 0)
                        if (System.IO.File.Exists(user_Dataset.backup_history.Rows[0]["path"].ToString() + "\\" + user_Dataset.backup_history.Rows[0]["filename"].ToString()))
                        {
                            MessageBox.Show("پشتیبان گیری از دیتابیس با موفقیت انجام شد");
                        }
                        else
                        {
                            Payam.Show("متاسفانه به دلیل نداشتن مجوز جهت ایجاد فایل پشتیبان ، عملیات پشتیبان گیری با موفقیت انجام نشد");
                            Classes.ClsMain.ExecuteNoneQuery("delete from backup_history where id=" + user_Dataset.backup_history.Rows[0]["id"].ToString());
                            this.backup_historyTableAdapter.Fill(this.user_Dataset.backup_history);

                        }

                    btnBackup.Enabled = true;

                }
            });

            restoreThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate(object o, RunWorkerCompletedEventArgs args)
            {
                if (successRestore)
                {
                    MessageBox.Show("بازیابی دیتابیس با موفقیت انجام شد");
                    Application.Exit();
                    btnRestore.Enabled = true;
                    this.restore_historyTableAdapter.Fill(this.user_Dataset.Restore_history);
                    this.backup_historyTableAdapter.Fill(this.user_Dataset.backup_history);

                }

            });

            
        }

        private void backupThread_DoWork(object sender, DoWorkEventArgs e)
        {
            backupThread.WorkerReportsProgress = true;
            BackupDatabase(Conb, Db_name, txtBackupPath.Text);
            backupThread.WorkerReportsProgress = false;
        }

        public void BackupDatabase(SqlConnection con, string databaseName, string backupFilename)
        {
            try
            {
                long inde = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull(max(id),1) from  backup_history"));
                FarsiLibrary.Utils.PersianDate Alan = new FarsiLibrary.Utils.PersianDate(DateTime.Now);
                string backupDirectory = Alan.Year + "-" + Alan.Month + "-" + Alan.Day + "--" + Alan.Hour + "-" + Alan.Minute;

                string Fullpath = backupFilename + "\\" + backupDirectory;
                if (System.IO.Directory.Exists(Fullpath))
                    System.IO.Directory.Delete(Fullpath, true);

                System.IO.Directory.CreateDirectory(Fullpath);


                Classes.ClsMain.ChangeCulture("e");
                Classes.ClsMain.ExecuteNoneQuery("insert into backup_history values ('" + DateTime.Now + "','Negah_backup_" + (inde + 1).ToString() + ".nbk','" + Fullpath + "')");

                Classes.ClsMain.ChangeCulture("f");


                con.FireInfoMessageEventOnUserErrors = true;
                con.InfoMessage += OnInfoMessage;
                con.Open();
                using (var cmd = new SqlCommand(string.Format(
                    "backup database {0} to disk = {1} with stats = 1",
                    QuoteIdentifier(databaseName),
                    QuoteString(Fullpath + "\\Negah_backup_" + (inde + 1).ToString() + ".nbk")), con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
                con.InfoMessage -= OnInfoMessage;
                con.FireInfoMessageEventOnUserErrors = false;
                successBackup = true;


            }
            catch (Exception ex)
            {
                Classes.ClsMain.logError(ex, "backup database failure");
                successBackup = false;
                Classes.ClsMain.ExecuteNoneQuery("delete from backup_history where id=(select max(id) from backup_history)");
            }
            finally
            {
                con.Close();
            }
        }


        private void OnInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            foreach (SqlError info in e.Errors)
            {
                if (info.Class > 10)
                {
                    // این قسمت یعنی اینکه عملیات با خطا مواجه شده است
                    Classes.ClsMain.logError(new Exception(info.Number + "###" + info.Message + "###" + info.Procedure
                        + "###" + info.Server + "###" + info.Source + "###" + info.State), "backup errors");
                }
                else
                {
                    if (info.Number == 3211) // کد 3211 مربوط به درصد پیشرفت عملیات است
                    {
                        int i = Convert.ToInt32(info.Message.Replace("percent processed.", "").Trim());
                        backupThread.ReportProgress(i);
                    }
                }
            }
        }

        public void RestoreDatabase(SqlConnection con, string databaseName, string backupFileName)
        {
            try
            {


                con.InfoMessage -= OnInfoMessage;
                con.FireInfoMessageEventOnUserErrors = true;
                con.InfoMessage += OnInfoMessageRestore;
                con.Open();

                //ALTER DATABASE oldName MODIFY NAME = newName


                using (var cmd = new SqlCommand(string.Format(
                    "USE master if db_id('" + databaseName + "') is not null begin ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK immediate drop database {0} end RESTORE DATABASE {0} FROM DISK = {1} WITH REPLACE ALTER DATABASE {0} SET MULTI_USER",
                    //"restore database {0} from disk = {1} with description = {2}, name = {3}, stats = 1",
                    QuoteIdentifier(databaseName),
                    QuoteString(backupFileName)), con))
                {
                    cmd.ExecuteNonQuery();
                }


                con.Close();
                con.InfoMessage -= OnInfoMessageRestore;
                con.FireInfoMessageEventOnUserErrors = true;

                Classes.ClsMain.ConnectionStr= System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.con1"].ConnectionString;
                //Classes.ClsMain.ChangeCulture("e");
                //Classes.ClsMain.ExecuteNoneQuery("insert into Restore_history values ('" + DateTime.Now + "','بازگردانی موفقیت آمیز اطلاعات')");
                //Classes.ClsMain.ChangeCulture("f");
                successRestore = true;

            }
            catch (Exception ex)
            {
                Classes.ClsMain.logError(ex, "restore database failure");
                successRestore = false;
            }
            finally
            {
                con.Close();
            }
        }


        private void OnInfoMessageRestore(object sender, SqlInfoMessageEventArgs e)
        {
            foreach (SqlError info in e.Errors)
            {
                Classes.ClsMain.logError(new Exception(info.Number + "###" + info.Message + "###" + info.Procedure
                    + "###" + info.Server + "###" + info.Source + "###" + info.State), "restore log");
            }
        }



        private string QuoteIdentifier(string name) { return "[" + name.Replace("]", "]]") + "]"; }

        private string QuoteString(string text) { return "'" + text.Replace("'", "''") + "'"; }


        private void btnBackup_Click(object sender, EventArgs e)
        {
            Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 30, "پشتیبان گیری از اطلاعات");
            
            btnBackup.Enabled = false;
            backupThread.RunWorkerAsync();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (txtRestorPath.Text != "")
            {
                btnRestore.Enabled = false;
                restoreThread.RunWorkerAsync();
            }
        }

        private void btnSetPath_Click_1(object sender, EventArgs e)
        {
            DialogResult result = setPathDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                Classes.CurrentUserSecurity Security = new Classes.CurrentUserSecurity();
                txtBackupPath.Text = setPathDlg.SelectedPath;
                btnBackup.Visible = true;

            }
            else
                btnBackup.Visible = true;
        }





        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {


                txtRestorPath.Text = openFileDialog1.FileName;
                btnRestore.Enabled = true;
            }
        }

        private void restoreThread_DoWork(object sender, DoWorkEventArgs e)
        {

            RestoreDatabase(ConR, Db_name, txtRestorPath.Text);
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void dgv_ghobooz_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                try
                {
                    System.Diagnostics.Process.Start(dgv_ghobooz.SelectedRows[0].Cells[3].Value.ToString());
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {

        }


    }
}