﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Management
{
    public partial class frm_user : MyMetroForm
    {
        public frm_user()
        {
            InitializeComponent();
        }

        private void frm_user_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset.admin_user' table. You can move, or remove it, as needed.
            this.admin_userTableAdapter.Fill(this.user_Dataset.admin_user);

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 31,"تغییر در اطلاعات کاربران مدیریت");
            
            adminuserBindingSource.EndEdit();
            admin_userTableAdapter.Update(user_Dataset.admin_user);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            new Management.Frm_add_user().ShowDialog();
            this.admin_userTableAdapter.Fill(this.user_Dataset.admin_user);

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (dgvAll.SelectedRows.Count != 0)
            {
                new Management.frm_user_permission(dgvAll.SelectedRows[0].Cells[0].Value.ToString(), dgvAll.SelectedRows[0].Cells[2].Value.ToString()).ShowDialog();
            }
        }

        private void vtb_delete_Click(object sender, EventArgs e)
        {
            if(adminuserBindingSource.Count>0)
            {
                string id = txtusername.Text;
                adminuserBindingSource.RemoveCurrent();
                adminuserBindingSource.EndEdit();
                admin_userTableAdapter.Update(user_Dataset.admin_user);
                Classes.ClsMain.ExecuteNoneQuery("delete from admin_user_access_report wherer username='" + id + "'");
                Payam.Show("کلیه اطلاعات مرتبط با کاربر با موفقیت از سیستم حذف شد");
            }
        }
    }
}