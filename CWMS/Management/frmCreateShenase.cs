﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Management
{
    public partial class frmCreateShenase : MyMetroForm
    {
        public frmCreateShenase()
        {
            Classes.ClsMain.ChangeCulture("f");

            InitializeComponent();
        }

        private void frmCreateShenase_Load(object sender, EventArgs e)
        {
            cmb_sharj_all.DataSource = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");
            cmb_sharj_all.DisplayMember = "dcode";
            cmb_sharj_all.ValueMember = "dcode";

            cmb_sharj_forOne.DataSource = cmb_sharj_all.DataSource;
            cmb_sharj_forOne.DisplayMember = "dcode";
            cmb_sharj_forOne.ValueMember = "dcode";


            cmbAbAll.DataSource = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            cmbAbAll.DisplayMember = "dcode";
            cmbAbAll.ValueMember = "dcode";


            cmb_ab_one.DataSource = cmbAbAll.DataSource;
            cmb_ab_one.DisplayMember = "dcode";
            cmb_ab_one.ValueMember = "dcode";

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                string type = "بر اساس مبلغ کل قبض";
                if (rdMablaghKolSharj.Checked == false)
                    type = "براساس مبلغ باقیمانده ";
                if (Cpayam.Show("ایا با صدور شناسه قبض و شناسه پرداخت " + " " + type + " موافق هستید؟ ") == DialogResult.Yes)
                {
                    
                    int dcode = Convert.ToInt32(cmb_sharj_all.SelectedValue);
                    Ghabz_Shenase(-1, dcode, rdMablaghKolSharj.Checked);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void btn_create_for_one_Click(object sender, EventArgs e)
        {
            if (txt_gharardad_sharj.Text.Trim() == "")
                txt_gharardad_sharj.Focus();
            try
            {
                string type = "بر اساس مبلغ کل قبض";
                if (rd_mablaghkol_moshtarek_sharj.Checked == false)
                    type = "براساس مبلغ باقیمانده ";
                if (Cpayam.Show("ایا با صدور شناسه قبض و شناسه پرداخت " + " " + type + " موافق هستید؟ ") == DialogResult.Yes)
                {
                    int gharardad = Convert.ToInt32(txt_gharardad_sharj.Text);
                    int dcode = Convert.ToInt32(cmb_sharj_forOne.SelectedValue);
                    Ghabz_Shenase(gharardad, dcode, rd_mablaghkol_moshtarek_sharj.Checked,"sharj_ghabz");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void Ghabz_Shenase(int gharardad, int dcode, bool ByMablghKol,string tablename="sharj_ghabz")
        {
            ProgressBar p = sharj_progress;
            try
            {
                DataTable dt_ghabz_info;
                if (gharardad == -1)
                    dt_ghabz_info = Classes.ClsMain.GetDataTable("select gharardad,tarikh,gcode,mablaghkol,mande,kasr_hezar from " + tablename +  " where dcode=" + dcode.ToString());
                else
                    dt_ghabz_info = Classes.ClsMain.GetDataTable("select gharardad,tarikh,gcode,mablaghkol,mande,kasr_hezar from " + tablename + " where gharardad=" + gharardad.ToString() + " and dcode=" + dcode.ToString());
                if (gharardad == -1)
                {
                    if (tablename == "sharj_ghabz")
                    {
                        p = sharj_progress;
                    }
                    else
                    {
                        p = ab_progress;
                    }
                    p.Maximum = dt_ghabz_info.Rows.Count;
                    p.Value = 0;
                }

                if (dt_ghabz_info.Rows.Count > 0)
                {
                    for (int i = 0; i < dt_ghabz_info.Rows.Count; i++)
                    {
                        int year = Convert.ToInt32(new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_ghabz_info.Rows[i]["tarikh"].ToString())).Year);
                        string shenase_ghabz = ShenasehGhabz.Tejarat_checkDigit6.Shenase_Ghabz((year % 100).ToString(), dt_ghabz_info.Rows[i]["gcode"].ToString());
                        string shenase_prdakht = "";
                        if (ByMablghKol)
                        {
                            shenase_prdakht = ShenasehGhabz.Ghabz.Shenase_Parakht(shenase_ghabz, ((Convert.ToInt64(dt_ghabz_info.Rows[i]["mablaghkol"]) - Convert.ToInt64(dt_ghabz_info.Rows[i]["kasr_hezar"])) / 1000).ToString(), year, dcode);

                        }
                        else
                        {
                            shenase_prdakht = ShenasehGhabz.Ghabz.Shenase_Parakht(shenase_ghabz, (Convert.ToInt64(dt_ghabz_info.Rows[i]["mande"]) / 1000).ToString(), year, dcode);

                        }


                        Classes.ClsMain.ExecuteNoneQuery("update " + tablename + " set shenase_ghabz='" + shenase_ghabz + "'  , shenase_pardakht='" + shenase_prdakht + "' where gcode=" + dt_ghabz_info.Rows[i]["gcode"].ToString());
                        p.Value += 1;
                    }
                    Payam.Show("شناسه قبض و شناسه پرداخت با موفقیت در سیستم ثبت شد");


                }
                else
                    Payam.Show("قبض مشترک در سیستم یافت نشد ");
            }
            catch (Exception exd)
            {

                Payam.Show(exd.ToString());
            }

        }


        private void btn_create_ab_For_all_Click(object sender, EventArgs e)
        {
            try
            {
                string type = "بر اساس مبلغ کل قبض";
                if (rd_ab_mablaghkol_all.Checked == false)
                    type = "براساس مبلغ باقیمانده ";
                if (Cpayam.Show("ایا با صدور شناسه قبض و شناسه پرداخت " + " " + type + " موافق هستید؟ ") == DialogResult.Yes)
                {

                    int dcode = Convert.ToInt32(cmbAbAll.SelectedValue);
                    Ghabz_Shenase(-1, dcode, rd_ab_mablaghkol_all.Checked,"ab_ghabz");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btn_create_forOne_Click(object sender, EventArgs e)
        {
            if (txt_gharardad_ab.Text.Trim() == "")
                txt_gharardad_ab.Focus();
            try
            {
                string type = "بر اساس مبلغ کل قبض";
                if (rd_ab_mablaghkol_one.Checked == false)
                    type = "براساس مبلغ باقیمانده ";
                if (Cpayam.Show("ایا با صدور شناسه قبض و شناسه پرداخت " + " " + type + " موافق هستید؟ ") == DialogResult.Yes)
                {
                    int gharardad = Convert.ToInt32(txt_gharardad_ab.Text);
                    int dcode = Convert.ToInt32(cmb_ab_one.SelectedValue);
                    Ghabz_Shenase(gharardad, dcode, rd_ab_mablaghkol_one.Checked, "ab_ghabz");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

   
    }
}
