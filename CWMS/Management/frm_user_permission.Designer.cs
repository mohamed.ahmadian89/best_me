﻿namespace CWMS.Management
{
    partial class frm_user_permission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtusername = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtFullname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnCreate = new DevComponents.DotNetBar.ButtonX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel7 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox94 = new System.Windows.Forms.CheckBox();
            this.adminuserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.user_Dataset = new CWMS.Management.user_Dataset();
            this.checkBox93 = new System.Windows.Forms.CheckBox();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX14 = new DevComponents.DotNetBar.ButtonX();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.chGozareshatmodiriat = new System.Windows.Forms.CheckBox();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.superTabItem7 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel6 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox92 = new System.Windows.Forms.CheckBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.chmodiriat_shahrak = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.superTabItem6 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox88 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.chMoshtarekin = new System.Windows.Forms.CheckBox();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.chAb = new System.Windows.Forms.CheckBox();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.chSharj = new System.Windows.Forms.CheckBox();
            this.grpSharj = new System.Windows.Forms.Panel();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.grpMoshtarekin = new System.Windows.Forms.Panel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.chBargeh = new System.Windows.Forms.CheckBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.chInternet = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.superTabItem5 = new DevComponents.DotNetBar.SuperTabItem();
            this.admin_userTableAdapter = new CWMS.Management.user_DatasetTableAdapters.admin_userTableAdapter();
            this.superTabItem8 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel8 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.ch_hesabdari = new System.Windows.Forms.CheckBox();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminuserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).BeginInit();
            this.superTabControlPanel6.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            this.grpSharj.SuspendLayout();
            this.grpMoshtarekin.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            this.superTabControlPanel5.SuspendLayout();
            this.superTabControlPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtusername);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.txtFullname);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 21);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(686, 68);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "اطلاعات کاربر";
            // 
            // txtusername
            // 
            this.txtusername.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtusername.Border.Class = "TextBoxBorder";
            this.txtusername.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtusername.DisabledBackColor = System.Drawing.Color.White;
            this.txtusername.ForeColor = System.Drawing.Color.Black;
            this.txtusername.Location = new System.Drawing.Point(164, 11);
            this.txtusername.Name = "txtusername";
            this.txtusername.PreventEnterBeep = true;
            this.txtusername.ReadOnly = true;
            this.txtusername.Size = new System.Drawing.Size(86, 24);
            this.txtusername.TabIndex = 2;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(216, 12);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(87, 23);
            this.labelX3.TabIndex = 19;
            this.labelX3.Text = "نام کاربری :";
            // 
            // txtFullname
            // 
            this.txtFullname.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFullname.Border.Class = "TextBoxBorder";
            this.txtFullname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFullname.DisabledBackColor = System.Drawing.Color.White;
            this.txtFullname.ForeColor = System.Drawing.Color.Black;
            this.txtFullname.Location = new System.Drawing.Point(340, 11);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.PreventEnterBeep = true;
            this.txtFullname.ReadOnly = true;
            this.txtFullname.Size = new System.Drawing.Size(165, 24);
            this.txtFullname.TabIndex = 0;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(499, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(87, 23);
            this.labelX2.TabIndex = 15;
            this.labelX2.Text = "نام و نام خانوادگی :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btnCreate);
            this.groupPanel2.Controls.Add(this.superTabControl1);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(8, 95);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(690, 530);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 5;
            this.groupPanel2.Text = "مشاهده مجوزهای مربوط به کاربر";
            // 
            // btnCreate
            // 
            this.btnCreate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCreate.BackColor = System.Drawing.Color.Transparent;
            this.btnCreate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCreate.Location = new System.Drawing.Point(198, 479);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCreate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnCreate.Size = new System.Drawing.Size(295, 24);
            this.btnCreate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCreate.Symbol = "";
            this.btnCreate.SymbolColor = System.Drawing.Color.Green;
            this.btnCreate.SymbolSize = 12F;
            this.btnCreate.TabIndex = 6;
            this.btnCreate.Text = "ذخیره کلیه مجوزهای دسترسی کاربر";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel8);
            this.superTabControl1.Controls.Add(this.superTabControlPanel7);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel6);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel5);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(19, 3);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 5;
            this.superTabControl1.Size = new System.Drawing.Size(647, 470);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 0;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.superTabItem2,
            this.superTabItem3,
            this.superTabItem4,
            this.superTabItem5,
            this.superTabItem6,
            this.superTabItem7,
            this.superTabItem8});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel7
            // 
            this.superTabControlPanel7.Controls.Add(this.checkBox94);
            this.superTabControlPanel7.Controls.Add(this.checkBox93);
            this.superTabControlPanel7.Controls.Add(this.buttonX13);
            this.superTabControlPanel7.Controls.Add(this.buttonX14);
            this.superTabControlPanel7.Controls.Add(this.checkBox33);
            this.superTabControlPanel7.Controls.Add(this.checkBox22);
            this.superTabControlPanel7.Controls.Add(this.labelX15);
            this.superTabControlPanel7.Controls.Add(this.chGozareshatmodiriat);
            this.superTabControlPanel7.Controls.Add(this.labelX16);
            this.superTabControlPanel7.Controls.Add(this.checkBox32);
            this.superTabControlPanel7.Controls.Add(this.labelX17);
            this.superTabControlPanel7.Controls.Add(this.checkBox31);
            this.superTabControlPanel7.Controls.Add(this.labelX18);
            this.superTabControlPanel7.Controls.Add(this.checkBox30);
            this.superTabControlPanel7.Controls.Add(this.checkBox25);
            this.superTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel7.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel7.Name = "superTabControlPanel7";
            this.superTabControlPanel7.Size = new System.Drawing.Size(647, 441);
            this.superTabControlPanel7.TabIndex = 0;
            this.superTabControlPanel7.TabItem = this.superTabItem7;
            // 
            // checkBox94
            // 
            this.checkBox94.AutoSize = true;
            this.checkBox94.BackColor = System.Drawing.Color.Transparent;
            this.checkBox94.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p106", true));
            this.checkBox94.ForeColor = System.Drawing.Color.Black;
            this.checkBox94.Location = new System.Drawing.Point(350, 239);
            this.checkBox94.Name = "checkBox94";
            this.checkBox94.Size = new System.Drawing.Size(198, 21);
            this.checkBox94.TabIndex = 94;
            this.checkBox94.Text = "گزارشات  مدیریت - مدیریت پرداختی ها";
            this.checkBox94.UseVisualStyleBackColor = false;
            // 
            // adminuserBindingSource
            // 
            this.adminuserBindingSource.DataMember = "admin_user";
            this.adminuserBindingSource.DataSource = this.user_Dataset;
            // 
            // user_Dataset
            // 
            this.user_Dataset.DataSetName = "user_Dataset";
            this.user_Dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkBox93
            // 
            this.checkBox93.AutoSize = true;
            this.checkBox93.BackColor = System.Drawing.Color.Transparent;
            this.checkBox93.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p104", true));
            this.checkBox93.ForeColor = System.Drawing.Color.Black;
            this.checkBox93.Location = new System.Drawing.Point(374, 173);
            this.checkBox93.Name = "checkBox93";
            this.checkBox93.Size = new System.Drawing.Size(175, 21);
            this.checkBox93.TabIndex = 93;
            this.checkBox93.Text = "گزارشات مدیریت - بدهکاران شارژ";
            this.checkBox93.UseVisualStyleBackColor = false;
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.BackColor = System.Drawing.Color.Transparent;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX13.Location = new System.Drawing.Point(30, 98);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX13.Size = new System.Drawing.Size(73, 63);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX13.Symbol = "";
            this.buttonX13.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX13.SymbolSize = 15F;
            this.buttonX13.TabIndex = 92;
            this.buttonX13.Text = "هیچکدام";
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // buttonX14
            // 
            this.buttonX14.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX14.BackColor = System.Drawing.Color.Transparent;
            this.buttonX14.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX14.Location = new System.Drawing.Point(30, 29);
            this.buttonX14.Name = "buttonX14";
            this.buttonX14.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX14.Size = new System.Drawing.Size(73, 63);
            this.buttonX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX14.Symbol = "";
            this.buttonX14.SymbolColor = System.Drawing.Color.Green;
            this.buttonX14.SymbolSize = 15F;
            this.buttonX14.TabIndex = 91;
            this.buttonX14.Text = "همه";
            this.buttonX14.Click += new System.EventHandler(this.buttonX14_Click);
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.BackColor = System.Drawing.Color.Transparent;
            this.checkBox33.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p38", true));
            this.checkBox33.ForeColor = System.Drawing.Color.Black;
            this.checkBox33.Location = new System.Drawing.Point(357, 195);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(192, 21);
            this.checkBox33.TabIndex = 67;
            this.checkBox33.Text = "گزارشات مدیریت -  تعرفه ها / ضرایب";
            this.checkBox33.UseVisualStyleBackColor = false;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.BackColor = System.Drawing.Color.Transparent;
            this.checkBox22.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p105", true));
            this.checkBox22.ForeColor = System.Drawing.Color.Black;
            this.checkBox22.Location = new System.Drawing.Point(382, 152);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(167, 21);
            this.checkBox22.TabIndex = 66;
            this.checkBox22.Text = "گزارشات مدیریت - بدهکاران آب";
            this.checkBox22.UseVisualStyleBackColor = false;
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(567, 266);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(25, 23);
            this.labelX15.Symbol = "";
            this.labelX15.SymbolColor = System.Drawing.Color.OrangeRed;
            this.labelX15.TabIndex = 65;
            // 
            // chGozareshatmodiriat
            // 
            this.chGozareshatmodiriat.AutoSize = true;
            this.chGozareshatmodiriat.BackColor = System.Drawing.Color.Transparent;
            this.chGozareshatmodiriat.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p99", true));
            this.chGozareshatmodiriat.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chGozareshatmodiriat.ForeColor = System.Drawing.Color.Black;
            this.chGozareshatmodiriat.Location = new System.Drawing.Point(400, 29);
            this.chGozareshatmodiriat.Name = "chGozareshatmodiriat";
            this.chGozareshatmodiriat.Size = new System.Drawing.Size(212, 22);
            this.chGozareshatmodiriat.TabIndex = 61;
            this.chGozareshatmodiriat.Text = "دسترسی به بخش اصلی گزارشات مدیریت";
            this.chGozareshatmodiriat.UseVisualStyleBackColor = false;
            this.chGozareshatmodiriat.CheckedChanged += new System.EventHandler(this.chGozareshatmodiriat_CheckedChanged);
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(62, 266);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(489, 35);
            this.labelX16.TabIndex = 64;
            this.labelX16.Text = "گزارشات مدیریت، شامل اطلاعات بسیار مهم مالی شهرک صنعتی می باشد . لطفا دقت کافی دا" +
    "شته باشید که چه اشخاصی امکان دسترسی به این بخش را دارا می باشند";
            this.labelX16.WordWrap = true;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.BackColor = System.Drawing.Color.Transparent;
            this.checkBox32.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p39", true));
            this.checkBox32.ForeColor = System.Drawing.Color.Black;
            this.checkBox32.Location = new System.Drawing.Point(382, 217);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(167, 21);
            this.checkBox32.TabIndex = 60;
            this.checkBox32.Text = "گزارشات  مدیریت - مدیریت چک";
            this.checkBox32.UseVisualStyleBackColor = false;
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(567, 57);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(25, 23);
            this.labelX17.Symbol = "";
            this.labelX17.SymbolColor = System.Drawing.Color.Orange;
            this.labelX17.TabIndex = 63;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.BackColor = System.Drawing.Color.Transparent;
            this.checkBox31.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p36", true));
            this.checkBox31.ForeColor = System.Drawing.Color.Black;
            this.checkBox31.Location = new System.Drawing.Point(344, 131);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(205, 21);
            this.checkBox31.TabIndex = 59;
            this.checkBox31.Text = "گزارشات مدیریت - درآمد ها - دوره جاری";
            this.checkBox31.UseVisualStyleBackColor = false;
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(216, 57);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(335, 23);
            this.labelX18.TabIndex = 62;
            this.labelX18.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.BackColor = System.Drawing.Color.Transparent;
            this.checkBox30.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p35", true));
            this.checkBox30.ForeColor = System.Drawing.Color.Black;
            this.checkBox30.Location = new System.Drawing.Point(336, 109);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(213, 21);
            this.checkBox30.TabIndex = 58;
            this.checkBox30.Text = "گزارشات مدیریت - درامد ها - کلیه دوره ها";
            this.checkBox30.UseVisualStyleBackColor = false;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.BackColor = System.Drawing.Color.Transparent;
            this.checkBox25.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p34", true));
            this.checkBox25.ForeColor = System.Drawing.Color.Black;
            this.checkBox25.Location = new System.Drawing.Point(369, 86);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(180, 21);
            this.checkBox25.TabIndex = 57;
            this.checkBox25.Text = "گزارشات مدیریت - گزارشات قبوض";
            this.checkBox25.UseVisualStyleBackColor = false;
            // 
            // superTabItem7
            // 
            this.superTabItem7.AttachedControl = this.superTabControlPanel7;
            this.superTabItem7.GlobalItem = false;
            this.superTabItem7.Name = "superTabItem7";
            this.superTabItem7.Text = "گزارشات مدیریت";
            // 
            // superTabControlPanel6
            // 
            this.superTabControlPanel6.Controls.Add(this.checkBox92);
            this.superTabControlPanel6.Controls.Add(this.checkBox91);
            this.superTabControlPanel6.Controls.Add(this.checkBox84);
            this.superTabControlPanel6.Controls.Add(this.checkBox83);
            this.superTabControlPanel6.Controls.Add(this.checkBox82);
            this.superTabControlPanel6.Controls.Add(this.checkBox47);
            this.superTabControlPanel6.Controls.Add(this.checkBox46);
            this.superTabControlPanel6.Controls.Add(this.buttonX11);
            this.superTabControlPanel6.Controls.Add(this.buttonX12);
            this.superTabControlPanel6.Controls.Add(this.labelX5);
            this.superTabControlPanel6.Controls.Add(this.labelX6);
            this.superTabControlPanel6.Controls.Add(this.labelX4);
            this.superTabControlPanel6.Controls.Add(this.labelX1);
            this.superTabControlPanel6.Controls.Add(this.chmodiriat_shahrak);
            this.superTabControlPanel6.Controls.Add(this.checkBox6);
            this.superTabControlPanel6.Controls.Add(this.checkBox7);
            this.superTabControlPanel6.Controls.Add(this.checkBox8);
            this.superTabControlPanel6.Controls.Add(this.checkBox9);
            this.superTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel6.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel6.Name = "superTabControlPanel6";
            this.superTabControlPanel6.Size = new System.Drawing.Size(647, 470);
            this.superTabControlPanel6.TabIndex = 0;
            this.superTabControlPanel6.TabItem = this.superTabItem6;
            // 
            // checkBox92
            // 
            this.checkBox92.AutoSize = true;
            this.checkBox92.BackColor = System.Drawing.Color.Transparent;
            this.checkBox92.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p101", true));
            this.checkBox92.ForeColor = System.Drawing.Color.Black;
            this.checkBox92.Location = new System.Drawing.Point(218, 261);
            this.checkBox92.Name = "checkBox92";
            this.checkBox92.Size = new System.Drawing.Size(339, 21);
            this.checkBox92.TabIndex = 97;
            this.checkBox92.Text = "منوی مدیریت شهرک - تعیین وضعیت صدور قبوض مشترکین ( اب و شارژ )";
            this.checkBox92.UseVisualStyleBackColor = false;
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.BackColor = System.Drawing.Color.Transparent;
            this.checkBox91.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p100", true));
            this.checkBox91.ForeColor = System.Drawing.Color.Black;
            this.checkBox91.Location = new System.Drawing.Point(351, 239);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(206, 21);
            this.checkBox91.TabIndex = 96;
            this.checkBox91.Text = "منوی مدیریت شهرک - تنظیمات کلی شهرک";
            this.checkBox91.UseVisualStyleBackColor = false;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.BackColor = System.Drawing.Color.Transparent;
            this.checkBox84.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p90", true));
            this.checkBox84.ForeColor = System.Drawing.Color.Black;
            this.checkBox84.Location = new System.Drawing.Point(372, 369);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(188, 21);
            this.checkBox84.TabIndex = 95;
            this.checkBox84.Text = "تعریف کاربر مدیریت  جدید در سیستم";
            this.checkBox84.UseVisualStyleBackColor = false;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.BackColor = System.Drawing.Color.Transparent;
            this.checkBox83.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p91", true));
            this.checkBox83.ForeColor = System.Drawing.Color.Black;
            this.checkBox83.Location = new System.Drawing.Point(349, 342);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(211, 21);
            this.checkBox83.TabIndex = 94;
            this.checkBox83.Text = "تغییرات اطلاعات کاربران مدیریت در سیستم";
            this.checkBox83.UseVisualStyleBackColor = false;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.BackColor = System.Drawing.Color.Transparent;
            this.checkBox82.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p92", true));
            this.checkBox82.ForeColor = System.Drawing.Color.Black;
            this.checkBox82.Location = new System.Drawing.Point(276, 315);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(284, 21);
            this.checkBox82.TabIndex = 93;
            this.checkBox82.Text = "اعمال تغییر در مجوزهای دسترسی کاربران مدیریت در سیستم";
            this.checkBox82.UseVisualStyleBackColor = false;
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.BackColor = System.Drawing.Color.Transparent;
            this.checkBox47.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p33", true));
            this.checkBox47.ForeColor = System.Drawing.Color.Black;
            this.checkBox47.Location = new System.Drawing.Point(350, 190);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(207, 21);
            this.checkBox47.TabIndex = 92;
            this.checkBox47.Text = "منوی مدیریت شهرک - اطلاعیه های سیستم";
            this.checkBox47.UseVisualStyleBackColor = false;
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.BackColor = System.Drawing.Color.Transparent;
            this.checkBox46.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p32", true));
            this.checkBox46.ForeColor = System.Drawing.Color.Black;
            this.checkBox46.Location = new System.Drawing.Point(275, 215);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(282, 21);
            this.checkBox46.TabIndex = 91;
            this.checkBox46.Text = "منوی مدیریت شهرک - مدیریت کاربران - گزارشات دسترسی";
            this.checkBox46.UseVisualStyleBackColor = false;
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.Transparent;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Location = new System.Drawing.Point(32, 97);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX11.Size = new System.Drawing.Size(73, 63);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.Symbol = "";
            this.buttonX11.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX11.SymbolSize = 15F;
            this.buttonX11.TabIndex = 90;
            this.buttonX11.Text = "هیچکدام";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.BackColor = System.Drawing.Color.Transparent;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Location = new System.Drawing.Point(32, 28);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX12.Size = new System.Drawing.Size(73, 63);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.Symbol = "";
            this.buttonX12.SymbolColor = System.Drawing.Color.Green;
            this.buttonX12.SymbolSize = 15F;
            this.buttonX12.TabIndex = 89;
            this.buttonX12.Text = "همه";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(597, 402);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(25, 23);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.OrangeRed;
            this.labelX5.TabIndex = 56;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(234, 404);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(357, 23);
            this.labelX6.TabIndex = 55;
            this.labelX6.Text = "تعریف نام و اطلاعات مهم مربوط به شهرک  . لطفا مجوز فقط به مدیریت داده شود ";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(566, 59);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(25, 23);
            this.labelX4.Symbol = "";
            this.labelX4.SymbolColor = System.Drawing.Color.Orange;
            this.labelX4.TabIndex = 54;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(225, 61);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(335, 23);
            this.labelX1.TabIndex = 53;
            this.labelX1.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // chmodiriat_shahrak
            // 
            this.chmodiriat_shahrak.AutoSize = true;
            this.chmodiriat_shahrak.BackColor = System.Drawing.Color.Transparent;
            this.chmodiriat_shahrak.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p98", true));
            this.chmodiriat_shahrak.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chmodiriat_shahrak.ForeColor = System.Drawing.Color.Black;
            this.chmodiriat_shahrak.Location = new System.Drawing.Point(411, 31);
            this.chmodiriat_shahrak.Name = "chmodiriat_shahrak";
            this.chmodiriat_shahrak.Size = new System.Drawing.Size(201, 22);
            this.chmodiriat_shahrak.TabIndex = 52;
            this.chmodiriat_shahrak.Text = "دسترسی به بخش اصلی مدیریت شهرک";
            this.chmodiriat_shahrak.UseVisualStyleBackColor = false;
            this.chmodiriat_shahrak.CheckedChanged += new System.EventHandler(this.chmodiriat_shahrak_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.BackColor = System.Drawing.Color.Transparent;
            this.checkBox6.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p28", true));
            this.checkBox6.ForeColor = System.Drawing.Color.Black;
            this.checkBox6.Location = new System.Drawing.Point(259, 89);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(298, 21);
            this.checkBox6.TabIndex = 48;
            this.checkBox6.Text = "منوی مدیریت شهرک - اطلاعات اولیه سیستم - بدهی و کنتور آب";
            this.checkBox6.UseVisualStyleBackColor = false;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.BackColor = System.Drawing.Color.Transparent;
            this.checkBox7.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p29", true));
            this.checkBox7.ForeColor = System.Drawing.Color.Black;
            this.checkBox7.Location = new System.Drawing.Point(285, 115);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(272, 21);
            this.checkBox7.TabIndex = 49;
            this.checkBox7.Text = "منوی مدیریت شهرک - اطلاعات اولیه سیستم - بدهی شارژ";
            this.checkBox7.UseVisualStyleBackColor = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.BackColor = System.Drawing.Color.Transparent;
            this.checkBox8.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p30", true));
            this.checkBox8.ForeColor = System.Drawing.Color.Black;
            this.checkBox8.Location = new System.Drawing.Point(329, 142);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(228, 21);
            this.checkBox8.TabIndex = 50;
            this.checkBox8.Text = "منوی مدیریت شهرک - پشتیبان گیری از اطلاعات";
            this.checkBox8.UseVisualStyleBackColor = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.BackColor = System.Drawing.Color.Transparent;
            this.checkBox9.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p31", true));
            this.checkBox9.ForeColor = System.Drawing.Color.Black;
            this.checkBox9.Location = new System.Drawing.Point(287, 166);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(270, 21);
            this.checkBox9.TabIndex = 51;
            this.checkBox9.Text = "منوی مدیریت شهرک - مدیریت کاربران - مشاهده کاربران";
            this.checkBox9.UseVisualStyleBackColor = false;
            // 
            // superTabItem6
            // 
            this.superTabItem6.AttachedControl = this.superTabControlPanel6;
            this.superTabItem6.GlobalItem = false;
            this.superTabItem6.Name = "superTabItem6";
            this.superTabItem6.Text = "مدیریت شهرک";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.checkBox88);
            this.superTabControlPanel1.Controls.Add(this.checkBox87);
            this.superTabControlPanel1.Controls.Add(this.checkBox86);
            this.superTabControlPanel1.Controls.Add(this.checkBox85);
            this.superTabControlPanel1.Controls.Add(this.checkBox37);
            this.superTabControlPanel1.Controls.Add(this.checkBox44);
            this.superTabControlPanel1.Controls.Add(this.buttonX2);
            this.superTabControlPanel1.Controls.Add(this.buttonX1);
            this.superTabControlPanel1.Controls.Add(this.labelX21);
            this.superTabControlPanel1.Controls.Add(this.labelX22);
            this.superTabControlPanel1.Controls.Add(this.checkBox34);
            this.superTabControlPanel1.Controls.Add(this.checkBox35);
            this.superTabControlPanel1.Controls.Add(this.chMoshtarekin);
            this.superTabControlPanel1.Controls.Add(this.labelX19);
            this.superTabControlPanel1.Controls.Add(this.checkBox38);
            this.superTabControlPanel1.Controls.Add(this.labelX20);
            this.superTabControlPanel1.Controls.Add(this.checkBox39);
            this.superTabControlPanel1.Controls.Add(this.checkBox40);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(647, 441);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // checkBox88
            // 
            this.checkBox88.AutoSize = true;
            this.checkBox88.BackColor = System.Drawing.Color.Transparent;
            this.checkBox88.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p50", true));
            this.checkBox88.ForeColor = System.Drawing.Color.Black;
            this.checkBox88.Location = new System.Drawing.Point(228, 331);
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new System.Drawing.Size(312, 21);
            this.checkBox88.TabIndex = 87;
            this.checkBox88.Text = "پنل اصلی - گزارشات کاربردی - چاپ گزارش ریز حساب آب مشترک";
            this.checkBox88.UseVisualStyleBackColor = false;
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.BackColor = System.Drawing.Color.Transparent;
            this.checkBox87.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p49", true));
            this.checkBox87.ForeColor = System.Drawing.Color.Black;
            this.checkBox87.Location = new System.Drawing.Point(220, 310);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(320, 21);
            this.checkBox87.TabIndex = 86;
            this.checkBox87.Text = "پنل اصلی - گزارشات کاربردی - چاپ گزارش ریز حساب شارژ مشترک";
            this.checkBox87.UseVisualStyleBackColor = false;
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.BackColor = System.Drawing.Color.Transparent;
            this.checkBox86.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p43", true));
            this.checkBox86.ForeColor = System.Drawing.Color.Black;
            this.checkBox86.Location = new System.Drawing.Point(277, 288);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(263, 21);
            this.checkBox86.TabIndex = 85;
            this.checkBox86.Text = "پنل اصلی - جستجوی اطلاعات کاربر بر اساس شماره پلاک";
            this.checkBox86.UseVisualStyleBackColor = false;
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.BackColor = System.Drawing.Color.Transparent;
            this.checkBox85.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p42", true));
            this.checkBox85.ForeColor = System.Drawing.Color.Black;
            this.checkBox85.Location = new System.Drawing.Point(242, 266);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(298, 21);
            this.checkBox85.TabIndex = 84;
            this.checkBox85.Text = "پنل اصلی - جستجوی اطلاعات کاربر بر اساس نام / شماره قرارداد";
            this.checkBox85.UseVisualStyleBackColor = false;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.BackColor = System.Drawing.Color.Transparent;
            this.checkBox37.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p6", true));
            this.checkBox37.ForeColor = System.Drawing.Color.Black;
            this.checkBox37.Location = new System.Drawing.Point(264, 235);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(276, 21);
            this.checkBox37.TabIndex = 83;
            this.checkBox37.Text = "منوی مشترکین - باز کردن منوی جستجو در سوابق مشترکین";
            this.checkBox37.UseVisualStyleBackColor = false;
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.BackColor = System.Drawing.Color.Transparent;
            this.checkBox44.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p4", true));
            this.checkBox44.ForeColor = System.Drawing.Color.Black;
            this.checkBox44.Location = new System.Drawing.Point(292, 164);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(248, 21);
            this.checkBox44.TabIndex = 82;
            this.checkBox44.Text = "منوی مشترکین - باز کردن منوی اطلاعات قبوض شارژ";
            this.checkBox44.UseVisualStyleBackColor = false;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(37, 99);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX2.Size = new System.Drawing.Size(73, 63);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX2.SymbolSize = 15F;
            this.buttonX2.TabIndex = 80;
            this.buttonX2.Text = "هیچکدام";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(37, 30);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX1.Size = new System.Drawing.Size(73, 63);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 15F;
            this.buttonX1.TabIndex = 79;
            this.buttonX1.Text = "همه";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(577, 365);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(25, 23);
            this.labelX21.Symbol = "";
            this.labelX21.SymbolColor = System.Drawing.Color.OrangeRed;
            this.labelX21.TabIndex = 78;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(57, 355);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(514, 44);
            this.labelX22.TabIndex = 77;
            this.labelX22.Text = "دقت نمایید که اکثر گزینه های این بخش ، از سایر قسمت هانیز قابل دسترس می باشد  و ق" +
    "رارگرفتن آنها در بخش امور مشترکین صرفا به منظور دسترسی راحت تر کاربر می باشد";
            this.labelX22.WordWrap = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.BackColor = System.Drawing.Color.Transparent;
            this.checkBox34.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p5", true));
            this.checkBox34.ForeColor = System.Drawing.Color.Black;
            this.checkBox34.Location = new System.Drawing.Point(297, 211);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(243, 21);
            this.checkBox34.TabIndex = 76;
            this.checkBox34.Text = "منوی مشترکین - باز کردن منوی کلید امنیتی موبایل";
            this.checkBox34.UseVisualStyleBackColor = false;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.BackColor = System.Drawing.Color.Transparent;
            this.checkBox35.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p7", true));
            this.checkBox35.ForeColor = System.Drawing.Color.Black;
            this.checkBox35.Location = new System.Drawing.Point(332, 188);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(208, 21);
            this.checkBox35.TabIndex = 75;
            this.checkBox35.Text = "منوی مشترکین - باز کردن منوی برگه خروج";
            this.checkBox35.UseVisualStyleBackColor = false;
            // 
            // chMoshtarekin
            // 
            this.chMoshtarekin.AutoSize = true;
            this.chMoshtarekin.BackColor = System.Drawing.Color.Transparent;
            this.chMoshtarekin.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p93", true));
            this.chMoshtarekin.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chMoshtarekin.ForeColor = System.Drawing.Color.Black;
            this.chMoshtarekin.Location = new System.Drawing.Point(422, 28);
            this.chMoshtarekin.Name = "chMoshtarekin";
            this.chMoshtarekin.Size = new System.Drawing.Size(196, 22);
            this.chMoshtarekin.TabIndex = 72;
            this.chMoshtarekin.Text = "دسترسی به بخش اصلی امور مشترکین";
            this.chMoshtarekin.UseVisualStyleBackColor = false;
            this.chMoshtarekin.CheckedChanged += new System.EventHandler(this.chMoshtarekin_CheckedChanged);
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(574, 56);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(25, 23);
            this.labelX19.Symbol = "";
            this.labelX19.SymbolColor = System.Drawing.Color.Orange;
            this.labelX19.TabIndex = 74;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.BackColor = System.Drawing.Color.Transparent;
            this.checkBox38.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p3", true));
            this.checkBox38.ForeColor = System.Drawing.Color.Black;
            this.checkBox38.Location = new System.Drawing.Point(303, 138);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(237, 21);
            this.checkBox38.TabIndex = 70;
            this.checkBox38.Text = "منوی مشترکین - بازکردن منوی اطلاعات قبوض آب";
            this.checkBox38.UseVisualStyleBackColor = false;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(223, 56);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(335, 23);
            this.labelX20.TabIndex = 73;
            this.labelX20.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.BackColor = System.Drawing.Color.Transparent;
            this.checkBox39.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p2", true));
            this.checkBox39.ForeColor = System.Drawing.Color.Black;
            this.checkBox39.Location = new System.Drawing.Point(266, 113);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(274, 21);
            this.checkBox39.TabIndex = 69;
            this.checkBox39.Text = "منوی مشترکین - باز کردن منوی  مشاهده اطلاعات مشترکین";
            this.checkBox39.UseVisualStyleBackColor = false;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.BackColor = System.Drawing.Color.Transparent;
            this.checkBox40.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p1", true));
            this.checkBox40.ForeColor = System.Drawing.Color.Black;
            this.checkBox40.Location = new System.Drawing.Point(300, 87);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(240, 21);
            this.checkBox40.TabIndex = 68;
            this.checkBox40.Text = "منوی مشترکین - باز کردن منوی ثبت مشترک جدید";
            this.checkBox40.UseVisualStyleBackColor = false;
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "امور مشترکین";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.checkBox90);
            this.superTabControlPanel3.Controls.Add(this.checkBox89);
            this.superTabControlPanel3.Controls.Add(this.checkBox63);
            this.superTabControlPanel3.Controls.Add(this.checkBox64);
            this.superTabControlPanel3.Controls.Add(this.checkBox65);
            this.superTabControlPanel3.Controls.Add(this.checkBox66);
            this.superTabControlPanel3.Controls.Add(this.checkBox67);
            this.superTabControlPanel3.Controls.Add(this.checkBox68);
            this.superTabControlPanel3.Controls.Add(this.checkBox69);
            this.superTabControlPanel3.Controls.Add(this.checkBox70);
            this.superTabControlPanel3.Controls.Add(this.checkBox71);
            this.superTabControlPanel3.Controls.Add(this.checkBox72);
            this.superTabControlPanel3.Controls.Add(this.checkBox73);
            this.superTabControlPanel3.Controls.Add(this.checkBox74);
            this.superTabControlPanel3.Controls.Add(this.checkBox75);
            this.superTabControlPanel3.Controls.Add(this.checkBox76);
            this.superTabControlPanel3.Controls.Add(this.checkBox77);
            this.superTabControlPanel3.Controls.Add(this.buttonX5);
            this.superTabControlPanel3.Controls.Add(this.buttonX6);
            this.superTabControlPanel3.Controls.Add(this.checkBox43);
            this.superTabControlPanel3.Controls.Add(this.checkBox42);
            this.superTabControlPanel3.Controls.Add(this.checkBox41);
            this.superTabControlPanel3.Controls.Add(this.checkBox1);
            this.superTabControlPanel3.Controls.Add(this.checkBox15);
            this.superTabControlPanel3.Controls.Add(this.checkBox23);
            this.superTabControlPanel3.Controls.Add(this.checkBox24);
            this.superTabControlPanel3.Controls.Add(this.labelX11);
            this.superTabControlPanel3.Controls.Add(this.labelX12);
            this.superTabControlPanel3.Controls.Add(this.chAb);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(647, 470);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.superTabItem3;
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.BackColor = System.Drawing.Color.Transparent;
            this.checkBox90.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p81", true));
            this.checkBox90.ForeColor = System.Drawing.Color.Black;
            this.checkBox90.Location = new System.Drawing.Point(103, 371);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(142, 21);
            this.checkBox90.TabIndex = 114;
            this.checkBox90.Text = "تغییر در آبونمان انشعاب اب";
            this.checkBox90.UseVisualStyleBackColor = false;
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.BackColor = System.Drawing.Color.Transparent;
            this.checkBox89.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p80", true));
            this.checkBox89.ForeColor = System.Drawing.Color.Black;
            this.checkBox89.Location = new System.Drawing.Point(35, 348);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(210, 21);
            this.checkBox89.TabIndex = 113;
            this.checkBox89.Text = "تغییر در اطلاعات مربوط به مصارف مازاد آب ";
            this.checkBox89.UseVisualStyleBackColor = false;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.BackColor = System.Drawing.Color.Transparent;
            this.checkBox63.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p46", true));
            this.checkBox63.ForeColor = System.Drawing.Color.Black;
            this.checkBox63.Location = new System.Drawing.Point(463, 394);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(151, 21);
            this.checkBox63.TabIndex = 112;
            this.checkBox63.Text = "پنل اصلی - مشاهده قبض آب";
            this.checkBox63.UseVisualStyleBackColor = false;
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.BackColor = System.Drawing.Color.Transparent;
            this.checkBox64.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p47", true));
            this.checkBox64.ForeColor = System.Drawing.Color.Black;
            this.checkBox64.Location = new System.Drawing.Point(469, 371);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(145, 21);
            this.checkBox64.TabIndex = 111;
            this.checkBox64.Text = "پنل اصلی - پرینت قبض آب";
            this.checkBox64.UseVisualStyleBackColor = false;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.BackColor = System.Drawing.Color.Transparent;
            this.checkBox65.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p54", true));
            this.checkBox65.ForeColor = System.Drawing.Color.Black;
            this.checkBox65.Location = new System.Drawing.Point(276, 348);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(157, 21);
            this.checkBox65.TabIndex = 110;
            this.checkBox65.Text = "مشاهده اطلاعات کامل قبض آب";
            this.checkBox65.UseVisualStyleBackColor = false;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.BackColor = System.Drawing.Color.Transparent;
            this.checkBox66.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p56", true));
            this.checkBox66.ForeColor = System.Drawing.Color.Black;
            this.checkBox66.Location = new System.Drawing.Point(298, 395);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(135, 21);
            this.checkBox66.TabIndex = 109;
            this.checkBox66.Text = "تغییر در اطلاعات قبض آب";
            this.checkBox66.UseVisualStyleBackColor = false;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.BackColor = System.Drawing.Color.Transparent;
            this.checkBox67.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p59", true));
            this.checkBox67.ForeColor = System.Drawing.Color.Black;
            this.checkBox67.Location = new System.Drawing.Point(256, 371);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(177, 21);
            this.checkBox67.TabIndex = 108;
            this.checkBox67.Text = " مشاهده پرداخت های مالی قبض آب";
            this.checkBox67.UseVisualStyleBackColor = false;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.BackColor = System.Drawing.Color.Transparent;
            this.checkBox68.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p78", true));
            this.checkBox68.ForeColor = System.Drawing.Color.Black;
            this.checkBox68.Location = new System.Drawing.Point(43, 311);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(202, 21);
            this.checkBox68.TabIndex = 107;
            this.checkBox68.Text = "اضافه / ثبت / حذف تعرفه آب در سیستم";
            this.checkBox68.UseVisualStyleBackColor = false;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.BackColor = System.Drawing.Color.Transparent;
            this.checkBox69.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p77", true));
            this.checkBox69.ForeColor = System.Drawing.Color.Black;
            this.checkBox69.Location = new System.Drawing.Point(454, 332);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(160, 21);
            this.checkBox69.TabIndex = 106;
            this.checkBox69.Text = "تغییر تنظیمات کلی دوره های آب";
            this.checkBox69.UseVisualStyleBackColor = false;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.BackColor = System.Drawing.Color.Transparent;
            this.checkBox70.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p71", true));
            this.checkBox70.ForeColor = System.Drawing.Color.Black;
            this.checkBox70.Location = new System.Drawing.Point(281, 309);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(152, 21);
            this.checkBox70.TabIndex = 105;
            this.checkBox70.Text = "چاپ کلیه قبوض آب مشترکین";
            this.checkBox70.UseVisualStyleBackColor = false;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.BackColor = System.Drawing.Color.Transparent;
            this.checkBox71.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p73", true));
            this.checkBox71.ForeColor = System.Drawing.Color.Black;
            this.checkBox71.Location = new System.Drawing.Point(9, 287);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(236, 21);
            this.checkBox71.TabIndex = 104;
            this.checkBox71.Text = "غیر فعال سازی امکان مشاهده قبوض آب مشترکین";
            this.checkBox71.UseVisualStyleBackColor = false;
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.BackColor = System.Drawing.Color.Transparent;
            this.checkBox72.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p72", true));
            this.checkBox72.ForeColor = System.Drawing.Color.Black;
            this.checkBox72.Location = new System.Drawing.Point(26, 264);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(219, 21);
            this.checkBox72.TabIndex = 103;
            this.checkBox72.Text = "فعال سازی امکان مشاهده قبوض آب مشترکین";
            this.checkBox72.UseVisualStyleBackColor = false;
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.BackColor = System.Drawing.Color.Transparent;
            this.checkBox73.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p67", true));
            this.checkBox73.ForeColor = System.Drawing.Color.Black;
            this.checkBox73.Location = new System.Drawing.Point(278, 282);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(155, 21);
            this.checkBox73.TabIndex = 102;
            this.checkBox73.Text = "حذف کلیه قبوض آب مشترکین";
            this.checkBox73.UseVisualStyleBackColor = false;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.BackColor = System.Drawing.Color.Transparent;
            this.checkBox74.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p65", true));
            this.checkBox74.ForeColor = System.Drawing.Color.Black;
            this.checkBox74.Location = new System.Drawing.Point(272, 254);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(161, 21);
            this.checkBox74.TabIndex = 101;
            this.checkBox74.Text = "صدور قبوض آب کلیه مشترکین ";
            this.checkBox74.UseVisualStyleBackColor = false;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.BackColor = System.Drawing.Color.Transparent;
            this.checkBox75.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p62", true));
            this.checkBox75.ForeColor = System.Drawing.Color.Black;
            this.checkBox75.Location = new System.Drawing.Point(528, 282);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(86, 21);
            this.checkBox75.TabIndex = 100;
            this.checkBox75.Text = "حذف دوره آب";
            this.checkBox75.UseVisualStyleBackColor = false;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.BackColor = System.Drawing.Color.Transparent;
            this.checkBox76.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p68", true));
            this.checkBox76.ForeColor = System.Drawing.Color.Black;
            this.checkBox76.Location = new System.Drawing.Point(465, 307);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(149, 21);
            this.checkBox76.TabIndex = 99;
            this.checkBox76.Text = "تمدید مهلت پرداخت دوره آب";
            this.checkBox76.UseVisualStyleBackColor = false;
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.BackColor = System.Drawing.Color.Transparent;
            this.checkBox77.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p61", true));
            this.checkBox77.ForeColor = System.Drawing.Color.Black;
            this.checkBox77.Location = new System.Drawing.Point(498, 254);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(116, 21);
            this.checkBox77.TabIndex = 98;
            this.checkBox77.Text = "تعریف دوره جدید آب";
            this.checkBox77.UseVisualStyleBackColor = false;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(29, 103);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX5.Size = new System.Drawing.Size(73, 63);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX5.SymbolSize = 15F;
            this.buttonX5.TabIndex = 84;
            this.buttonX5.Text = "هیچکدام";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(29, 34);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX6.Size = new System.Drawing.Size(73, 63);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Green;
            this.buttonX6.SymbolSize = 15F;
            this.buttonX6.TabIndex = 83;
            this.buttonX6.Text = "همه";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.BackColor = System.Drawing.Color.Transparent;
            this.checkBox43.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p15", true));
            this.checkBox43.ForeColor = System.Drawing.Color.Black;
            this.checkBox43.Location = new System.Drawing.Point(234, 211);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(311, 21);
            this.checkBox43.TabIndex = 70;
            this.checkBox43.Text = "منوی امور آب - مدیریت دوره های آب - تعریف دوره و صدور قبوض";
            this.checkBox43.UseVisualStyleBackColor = false;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.BackColor = System.Drawing.Color.Transparent;
            this.checkBox42.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p17", true));
            this.checkBox42.ForeColor = System.Drawing.Color.Black;
            this.checkBox42.Location = new System.Drawing.Point(382, 162);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(163, 21);
            this.checkBox42.TabIndex = 69;
            this.checkBox42.Text = "منوی امور آب - آبونمان  انشعاب";
            this.checkBox42.UseVisualStyleBackColor = false;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.BackColor = System.Drawing.Color.Transparent;
            this.checkBox41.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p19", true));
            this.checkBox41.ForeColor = System.Drawing.Color.Black;
            this.checkBox41.Location = new System.Drawing.Point(339, 186);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(206, 21);
            this.checkBox41.TabIndex = 68;
            this.checkBox41.Text = "منوی امور آب - تعیین ضرایب مصرف مازاد";
            this.checkBox41.UseVisualStyleBackColor = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p14", true));
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(301, 68);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(244, 21);
            this.checkBox1.TabIndex = 63;
            this.checkBox1.Text = "منوی امور آب - منوی مدیریت دوره های اب - دوره ";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.BackColor = System.Drawing.Color.Transparent;
            this.checkBox15.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p13", true));
            this.checkBox15.ForeColor = System.Drawing.Color.Black;
            this.checkBox15.Location = new System.Drawing.Point(291, 88);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(254, 21);
            this.checkBox15.TabIndex = 64;
            this.checkBox15.Text = "منوی امور آب - منوی مدیریت دوره های آب - مشترک";
            this.checkBox15.UseVisualStyleBackColor = false;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.BackColor = System.Drawing.Color.Transparent;
            this.checkBox23.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p18", true));
            this.checkBox23.ForeColor = System.Drawing.Color.Black;
            this.checkBox23.Location = new System.Drawing.Point(390, 110);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(155, 21);
            this.checkBox23.TabIndex = 65;
            this.checkBox23.Text = "منوی امور  آب - تنظیمات کلی ";
            this.checkBox23.UseVisualStyleBackColor = false;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.BackColor = System.Drawing.Color.Transparent;
            this.checkBox24.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p16", true));
            this.checkBox24.ForeColor = System.Drawing.Color.Black;
            this.checkBox24.Location = new System.Drawing.Point(386, 137);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(159, 21);
            this.checkBox24.TabIndex = 66;
            this.checkBox24.Text = "منوی امور آب - مدیریت آب بها";
            this.checkBox24.UseVisualStyleBackColor = false;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(551, 36);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(25, 23);
            this.labelX11.Symbol = "";
            this.labelX11.SymbolColor = System.Drawing.Color.Orange;
            this.labelX11.TabIndex = 58;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(210, 39);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(335, 23);
            this.labelX12.TabIndex = 57;
            this.labelX12.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // chAb
            // 
            this.chAb.AutoSize = true;
            this.chAb.BackColor = System.Drawing.Color.Transparent;
            this.chAb.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p95", true));
            this.chAb.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chAb.ForeColor = System.Drawing.Color.Black;
            this.chAb.Location = new System.Drawing.Point(441, 11);
            this.chAb.Name = "chAb";
            this.chAb.Size = new System.Drawing.Size(169, 22);
            this.chAb.TabIndex = 32;
            this.chAb.Text = "دسترسی به بخش اصلی امور آب";
            this.chAb.UseVisualStyleBackColor = false;
            this.chAb.CheckedChanged += new System.EventHandler(this.chAb_CheckedChanged);
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel3;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "امور آب";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.checkBox62);
            this.superTabControlPanel2.Controls.Add(this.checkBox61);
            this.superTabControlPanel2.Controls.Add(this.checkBox60);
            this.superTabControlPanel2.Controls.Add(this.checkBox59);
            this.superTabControlPanel2.Controls.Add(this.checkBox58);
            this.superTabControlPanel2.Controls.Add(this.checkBox57);
            this.superTabControlPanel2.Controls.Add(this.checkBox56);
            this.superTabControlPanel2.Controls.Add(this.checkBox55);
            this.superTabControlPanel2.Controls.Add(this.checkBox54);
            this.superTabControlPanel2.Controls.Add(this.checkBox53);
            this.superTabControlPanel2.Controls.Add(this.checkBox52);
            this.superTabControlPanel2.Controls.Add(this.checkBox51);
            this.superTabControlPanel2.Controls.Add(this.checkBox50);
            this.superTabControlPanel2.Controls.Add(this.checkBox49);
            this.superTabControlPanel2.Controls.Add(this.checkBox48);
            this.superTabControlPanel2.Controls.Add(this.buttonX3);
            this.superTabControlPanel2.Controls.Add(this.checkBox36);
            this.superTabControlPanel2.Controls.Add(this.buttonX4);
            this.superTabControlPanel2.Controls.Add(this.labelX13);
            this.superTabControlPanel2.Controls.Add(this.labelX14);
            this.superTabControlPanel2.Controls.Add(this.checkBox10);
            this.superTabControlPanel2.Controls.Add(this.checkBox14);
            this.superTabControlPanel2.Controls.Add(this.checkBox17);
            this.superTabControlPanel2.Controls.Add(this.checkBox21);
            this.superTabControlPanel2.Controls.Add(this.chSharj);
            this.superTabControlPanel2.Controls.Add(this.grpSharj);
            this.superTabControlPanel2.Controls.Add(this.grpMoshtarekin);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(647, 470);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.BackColor = System.Drawing.Color.Transparent;
            this.checkBox62.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p44", true));
            this.checkBox62.ForeColor = System.Drawing.Color.Black;
            this.checkBox62.Location = new System.Drawing.Point(470, 384);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(159, 21);
            this.checkBox62.TabIndex = 97;
            this.checkBox62.Text = "پنل اصلی - مشاهده قبض شارژ";
            this.checkBox62.UseVisualStyleBackColor = false;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.BackColor = System.Drawing.Color.Transparent;
            this.checkBox61.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p45", true));
            this.checkBox61.ForeColor = System.Drawing.Color.Black;
            this.checkBox61.Location = new System.Drawing.Point(476, 361);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(153, 21);
            this.checkBox61.TabIndex = 96;
            this.checkBox61.Text = "پنل اصلی - پرینت قبض شارژ";
            this.checkBox61.UseVisualStyleBackColor = false;
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.BackColor = System.Drawing.Color.Transparent;
            this.checkBox60.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p55", true));
            this.checkBox60.ForeColor = System.Drawing.Color.Black;
            this.checkBox60.Location = new System.Drawing.Point(283, 338);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(165, 21);
            this.checkBox60.TabIndex = 95;
            this.checkBox60.Text = "مشاهده اطلاعات کامل قبض شارژ";
            this.checkBox60.UseVisualStyleBackColor = false;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.BackColor = System.Drawing.Color.Transparent;
            this.checkBox59.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p57", true));
            this.checkBox59.ForeColor = System.Drawing.Color.Black;
            this.checkBox59.Location = new System.Drawing.Point(305, 385);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(143, 21);
            this.checkBox59.TabIndex = 94;
            this.checkBox59.Text = "تغییر در اطلاعات قبض شارژ";
            this.checkBox59.UseVisualStyleBackColor = false;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.BackColor = System.Drawing.Color.Transparent;
            this.checkBox58.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p58", true));
            this.checkBox58.ForeColor = System.Drawing.Color.Black;
            this.checkBox58.Location = new System.Drawing.Point(263, 361);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(185, 21);
            this.checkBox58.TabIndex = 93;
            this.checkBox58.Text = " مشاهده پرداخت های مالی قبض شارژ";
            this.checkBox58.UseVisualStyleBackColor = false;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.BackColor = System.Drawing.Color.Transparent;
            this.checkBox57.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p79", true));
            this.checkBox57.ForeColor = System.Drawing.Color.Black;
            this.checkBox57.Location = new System.Drawing.Point(50, 301);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(210, 21);
            this.checkBox57.TabIndex = 92;
            this.checkBox57.Text = "اضافه / ثبت / حذف تعرفه شارژ در سیستم";
            this.checkBox57.UseVisualStyleBackColor = false;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.BackColor = System.Drawing.Color.Transparent;
            this.checkBox56.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p76", true));
            this.checkBox56.ForeColor = System.Drawing.Color.Black;
            this.checkBox56.Location = new System.Drawing.Point(461, 322);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(168, 21);
            this.checkBox56.TabIndex = 91;
            this.checkBox56.Text = "تغییر تنظیمات کلی دوره های شارژ";
            this.checkBox56.UseVisualStyleBackColor = false;
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.BackColor = System.Drawing.Color.Transparent;
            this.checkBox55.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p70", true));
            this.checkBox55.ForeColor = System.Drawing.Color.Black;
            this.checkBox55.Location = new System.Drawing.Point(288, 299);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(160, 21);
            this.checkBox55.TabIndex = 90;
            this.checkBox55.Text = "چاپ کلیه قبوض شارژ مشترکین";
            this.checkBox55.UseVisualStyleBackColor = false;
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.BackColor = System.Drawing.Color.Transparent;
            this.checkBox54.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p75", true));
            this.checkBox54.ForeColor = System.Drawing.Color.Black;
            this.checkBox54.Location = new System.Drawing.Point(16, 277);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(244, 21);
            this.checkBox54.TabIndex = 89;
            this.checkBox54.Text = "غیر فعال سازی امکان مشاهده قبوض شارژ مشترکین";
            this.checkBox54.UseVisualStyleBackColor = false;
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.BackColor = System.Drawing.Color.Transparent;
            this.checkBox53.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p74", true));
            this.checkBox53.ForeColor = System.Drawing.Color.Black;
            this.checkBox53.Location = new System.Drawing.Point(33, 254);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(227, 21);
            this.checkBox53.TabIndex = 88;
            this.checkBox53.Text = "فعال سازی امکان مشاهده قبوض شارژ مشترکین";
            this.checkBox53.UseVisualStyleBackColor = false;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.BackColor = System.Drawing.Color.Transparent;
            this.checkBox52.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p66", true));
            this.checkBox52.ForeColor = System.Drawing.Color.Black;
            this.checkBox52.Location = new System.Drawing.Point(285, 272);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(163, 21);
            this.checkBox52.TabIndex = 87;
            this.checkBox52.Text = "حذف کلیه قبوض شارژ مشترکین";
            this.checkBox52.UseVisualStyleBackColor = false;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.BackColor = System.Drawing.Color.Transparent;
            this.checkBox51.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p64", true));
            this.checkBox51.ForeColor = System.Drawing.Color.Black;
            this.checkBox51.Location = new System.Drawing.Point(279, 244);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(169, 21);
            this.checkBox51.TabIndex = 86;
            this.checkBox51.Text = "صدور قبوض شارژ کلیه مشترکین ";
            this.checkBox51.UseVisualStyleBackColor = false;
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.BackColor = System.Drawing.Color.Transparent;
            this.checkBox50.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p63", true));
            this.checkBox50.ForeColor = System.Drawing.Color.Black;
            this.checkBox50.Location = new System.Drawing.Point(535, 272);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(94, 21);
            this.checkBox50.TabIndex = 85;
            this.checkBox50.Text = "حذف دوره شارژ";
            this.checkBox50.UseVisualStyleBackColor = false;
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.BackColor = System.Drawing.Color.Transparent;
            this.checkBox49.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p69", true));
            this.checkBox49.ForeColor = System.Drawing.Color.Black;
            this.checkBox49.Location = new System.Drawing.Point(472, 297);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(157, 21);
            this.checkBox49.TabIndex = 84;
            this.checkBox49.Text = "تمدید مهلت پرداخت دوره شارژ";
            this.checkBox49.UseVisualStyleBackColor = false;
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.BackColor = System.Drawing.Color.Transparent;
            this.checkBox48.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p60", true));
            this.checkBox48.ForeColor = System.Drawing.Color.Black;
            this.checkBox48.Location = new System.Drawing.Point(505, 244);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(124, 21);
            this.checkBox48.TabIndex = 83;
            this.checkBox48.Text = "تعریف دوره جدید شارژ";
            this.checkBox48.UseVisualStyleBackColor = false;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(34, 102);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX3.Size = new System.Drawing.Size(73, 63);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX3.SymbolSize = 15F;
            this.buttonX3.TabIndex = 82;
            this.buttonX3.Text = "هیچکدام";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.BackColor = System.Drawing.Color.Transparent;
            this.checkBox36.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p10", true));
            this.checkBox36.ForeColor = System.Drawing.Color.Black;
            this.checkBox36.Location = new System.Drawing.Point(154, 197);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(390, 21);
            this.checkBox36.TabIndex = 66;
            this.checkBox36.Text = "منوی امور شارژ - باز کردن منوی مدیریت دوره های شارژ - تعریف دوره و صدور قبوض";
            this.checkBox36.UseVisualStyleBackColor = false;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(34, 33);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX4.Size = new System.Drawing.Size(73, 63);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 15F;
            this.buttonX4.TabIndex = 81;
            this.buttonX4.Text = "همه";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(550, 48);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(25, 23);
            this.labelX13.Symbol = "";
            this.labelX13.SymbolColor = System.Drawing.Color.Orange;
            this.labelX13.TabIndex = 65;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(209, 51);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(335, 23);
            this.labelX14.TabIndex = 64;
            this.labelX14.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.Color.Transparent;
            this.checkBox10.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p9", true));
            this.checkBox10.ForeColor = System.Drawing.Color.Black;
            this.checkBox10.Location = new System.Drawing.Point(260, 89);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(284, 21);
            this.checkBox10.TabIndex = 59;
            this.checkBox10.Text = "منوی امور شارژ - باز کردن منوی مشاهده قبوض شارژ - دوره";
            this.checkBox10.UseVisualStyleBackColor = false;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.BackColor = System.Drawing.Color.Transparent;
            this.checkBox14.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p8", true));
            this.checkBox14.ForeColor = System.Drawing.Color.Black;
            this.checkBox14.Location = new System.Drawing.Point(245, 116);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(299, 21);
            this.checkBox14.TabIndex = 60;
            this.checkBox14.Text = "منوی امور شارژ - باز کردن منوی مشاهده  قبوض شارژ - مشترک";
            this.checkBox14.UseVisualStyleBackColor = false;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.BackColor = System.Drawing.Color.Transparent;
            this.checkBox17.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p11", true));
            this.checkBox17.ForeColor = System.Drawing.Color.Black;
            this.checkBox17.Location = new System.Drawing.Point(384, 143);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(160, 21);
            this.checkBox17.TabIndex = 61;
            this.checkBox17.Text = "منوی امور شارژ - تنظیمات کلی ";
            this.checkBox17.UseVisualStyleBackColor = false;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.BackColor = System.Drawing.Color.Transparent;
            this.checkBox21.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p12", true));
            this.checkBox21.ForeColor = System.Drawing.Color.Black;
            this.checkBox21.Location = new System.Drawing.Point(372, 170);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(172, 21);
            this.checkBox21.TabIndex = 62;
            this.checkBox21.Text = "منوی امور شارژ - تعرفه های شارژ";
            this.checkBox21.UseVisualStyleBackColor = false;
            // 
            // chSharj
            // 
            this.chSharj.AutoSize = true;
            this.chSharj.BackColor = System.Drawing.Color.Transparent;
            this.chSharj.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p94", true));
            this.chSharj.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chSharj.ForeColor = System.Drawing.Color.Black;
            this.chSharj.Location = new System.Drawing.Point(440, 23);
            this.chSharj.Name = "chSharj";
            this.chSharj.Size = new System.Drawing.Size(178, 22);
            this.chSharj.TabIndex = 63;
            this.chSharj.Text = "دسترسی به بخش اصلی امور شارژ";
            this.chSharj.UseVisualStyleBackColor = false;
            this.chSharj.CheckedChanged += new System.EventHandler(this.chSharj_CheckedChanged);
            // 
            // grpSharj
            // 
            this.grpSharj.BackColor = System.Drawing.Color.Transparent;
            this.grpSharj.Controls.Add(this.checkBox16);
            this.grpSharj.Controls.Add(this.checkBox18);
            this.grpSharj.Controls.Add(this.checkBox19);
            this.grpSharj.Controls.Add(this.checkBox20);
            this.grpSharj.ForeColor = System.Drawing.Color.Black;
            this.grpSharj.Location = new System.Drawing.Point(668, 23);
            this.grpSharj.Name = "grpSharj";
            this.grpSharj.Size = new System.Drawing.Size(146, 239);
            this.grpSharj.TabIndex = 42;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.BackColor = System.Drawing.Color.Transparent;
            this.checkBox16.ForeColor = System.Drawing.Color.Black;
            this.checkBox16.Location = new System.Drawing.Point(29, 39);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(87, 21);
            this.checkBox16.TabIndex = 1;
            this.checkBox16.Text = "checkBox16";
            this.checkBox16.UseVisualStyleBackColor = false;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.BackColor = System.Drawing.Color.Transparent;
            this.checkBox18.ForeColor = System.Drawing.Color.Black;
            this.checkBox18.Location = new System.Drawing.Point(29, 66);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(87, 21);
            this.checkBox18.TabIndex = 2;
            this.checkBox18.Text = "checkBox18";
            this.checkBox18.UseVisualStyleBackColor = false;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.BackColor = System.Drawing.Color.Transparent;
            this.checkBox19.ForeColor = System.Drawing.Color.Black;
            this.checkBox19.Location = new System.Drawing.Point(29, 93);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(87, 21);
            this.checkBox19.TabIndex = 3;
            this.checkBox19.Text = "checkBox19";
            this.checkBox19.UseVisualStyleBackColor = false;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.BackColor = System.Drawing.Color.Transparent;
            this.checkBox20.ForeColor = System.Drawing.Color.Black;
            this.checkBox20.Location = new System.Drawing.Point(29, 120);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(87, 21);
            this.checkBox20.TabIndex = 4;
            this.checkBox20.Text = "checkBox20";
            this.checkBox20.UseVisualStyleBackColor = false;
            // 
            // grpMoshtarekin
            // 
            this.grpMoshtarekin.BackColor = System.Drawing.Color.Transparent;
            this.grpMoshtarekin.Controls.Add(this.checkBox2);
            this.grpMoshtarekin.Controls.Add(this.checkBox3);
            this.grpMoshtarekin.Controls.Add(this.checkBox4);
            this.grpMoshtarekin.Controls.Add(this.checkBox5);
            this.grpMoshtarekin.ForeColor = System.Drawing.Color.Black;
            this.grpMoshtarekin.Location = new System.Drawing.Point(820, 24);
            this.grpMoshtarekin.Name = "grpMoshtarekin";
            this.grpMoshtarekin.Size = new System.Drawing.Size(146, 239);
            this.grpMoshtarekin.TabIndex = 41;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.Color.Transparent;
            this.checkBox2.ForeColor = System.Drawing.Color.Black;
            this.checkBox2.Location = new System.Drawing.Point(35, 39);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(81, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = false;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.Transparent;
            this.checkBox3.ForeColor = System.Drawing.Color.Black;
            this.checkBox3.Location = new System.Drawing.Point(35, 66);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(81, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.BackColor = System.Drawing.Color.Transparent;
            this.checkBox4.ForeColor = System.Drawing.Color.Black;
            this.checkBox4.Location = new System.Drawing.Point(35, 93);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(81, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.BackColor = System.Drawing.Color.Transparent;
            this.checkBox5.ForeColor = System.Drawing.Color.Black;
            this.checkBox5.Location = new System.Drawing.Point(35, 120);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(81, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "checkBox5";
            this.checkBox5.UseVisualStyleBackColor = false;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "امور شارژ";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.checkBox81);
            this.superTabControlPanel4.Controls.Add(this.checkBox80);
            this.superTabControlPanel4.Controls.Add(this.checkBox79);
            this.superTabControlPanel4.Controls.Add(this.checkBox78);
            this.superTabControlPanel4.Controls.Add(this.checkBox45);
            this.superTabControlPanel4.Controls.Add(this.buttonX7);
            this.superTabControlPanel4.Controls.Add(this.buttonX8);
            this.superTabControlPanel4.Controls.Add(this.chBargeh);
            this.superTabControlPanel4.Controls.Add(this.labelX7);
            this.superTabControlPanel4.Controls.Add(this.labelX8);
            this.superTabControlPanel4.Controls.Add(this.checkBox11);
            this.superTabControlPanel4.Controls.Add(this.checkBox12);
            this.superTabControlPanel4.Controls.Add(this.checkBox13);
            this.superTabControlPanel4.Controls.Add(this.checkBox29);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(647, 470);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem4;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.BackColor = System.Drawing.Color.Transparent;
            this.checkBox81.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p84", true));
            this.checkBox81.ForeColor = System.Drawing.Color.Black;
            this.checkBox81.Location = new System.Drawing.Point(422, 326);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(126, 21);
            this.checkBox81.TabIndex = 91;
            this.checkBox81.Text = "چاپ برگه خروج مشترک";
            this.checkBox81.UseVisualStyleBackColor = false;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.BackColor = System.Drawing.Color.Transparent;
            this.checkBox80.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p83", true));
            this.checkBox80.ForeColor = System.Drawing.Color.Black;
            this.checkBox80.Location = new System.Drawing.Point(348, 299);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(200, 21);
            this.checkBox80.TabIndex = 90;
            this.checkBox80.Text = "حذف درخواست صدور برگه خروج مشترک";
            this.checkBox80.UseVisualStyleBackColor = false;
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.BackColor = System.Drawing.Color.Transparent;
            this.checkBox79.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p82", true));
            this.checkBox79.ForeColor = System.Drawing.Color.Black;
            this.checkBox79.Location = new System.Drawing.Point(352, 272);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(196, 21);
            this.checkBox79.TabIndex = 89;
            this.checkBox79.Text = "ثبت درخواست صدور برگه خروج مشترک";
            this.checkBox79.UseVisualStyleBackColor = false;
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.BackColor = System.Drawing.Color.Transparent;
            this.checkBox78.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p48", true));
            this.checkBox78.ForeColor = System.Drawing.Color.Black;
            this.checkBox78.Location = new System.Drawing.Point(321, 245);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(227, 21);
            this.checkBox78.TabIndex = 88;
            this.checkBox78.Text = "پنل اصلی - صدور برگه برگه خروج برای مشترک";
            this.checkBox78.UseVisualStyleBackColor = false;
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.BackColor = System.Drawing.Color.Transparent;
            this.checkBox45.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p24", true));
            this.checkBox45.ForeColor = System.Drawing.Color.Black;
            this.checkBox45.Location = new System.Drawing.Point(314, 191);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(234, 21);
            this.checkBox45.TabIndex = 87;
            this.checkBox45.Text = "منوی برگه خروج - انتقال اطلاعات به گیت خروجی ";
            this.checkBox45.UseVisualStyleBackColor = false;
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Transparent;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Location = new System.Drawing.Point(37, 101);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX7.Size = new System.Drawing.Size(73, 63);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.Symbol = "";
            this.buttonX7.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX7.SymbolSize = 15F;
            this.buttonX7.TabIndex = 86;
            this.buttonX7.Text = "هیچکدام";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Transparent;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Location = new System.Drawing.Point(37, 32);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX8.Size = new System.Drawing.Size(73, 63);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.Green;
            this.buttonX8.SymbolSize = 15F;
            this.buttonX8.TabIndex = 85;
            this.buttonX8.Text = "همه";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // chBargeh
            // 
            this.chBargeh.AutoSize = true;
            this.chBargeh.BackColor = System.Drawing.Color.Transparent;
            this.chBargeh.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p96", true));
            this.chBargeh.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chBargeh.ForeColor = System.Drawing.Color.Black;
            this.chBargeh.Location = new System.Drawing.Point(441, 23);
            this.chBargeh.Name = "chBargeh";
            this.chBargeh.Size = new System.Drawing.Size(179, 22);
            this.chBargeh.TabIndex = 57;
            this.chBargeh.Text = "دسترسی به بخش اصلی برگه خروج";
            this.chBargeh.UseVisualStyleBackColor = false;
            this.chBargeh.CheckedChanged += new System.EventHandler(this.chBargeh_CheckedChanged);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(575, 51);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(25, 23);
            this.labelX7.Symbol = "";
            this.labelX7.SymbolColor = System.Drawing.Color.Orange;
            this.labelX7.TabIndex = 56;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(234, 54);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(335, 23);
            this.labelX8.TabIndex = 55;
            this.labelX8.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.BackColor = System.Drawing.Color.Transparent;
            this.checkBox11.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p21", true));
            this.checkBox11.ForeColor = System.Drawing.Color.Black;
            this.checkBox11.Location = new System.Drawing.Point(370, 83);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(178, 21);
            this.checkBox11.TabIndex = 6;
            this.checkBox11.Text = "منوی برگه خروج - صدور برگه خروج";
            this.checkBox11.UseVisualStyleBackColor = false;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.BackColor = System.Drawing.Color.Transparent;
            this.checkBox12.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p20", true));
            this.checkBox12.ForeColor = System.Drawing.Color.Black;
            this.checkBox12.Location = new System.Drawing.Point(358, 110);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(190, 21);
            this.checkBox12.TabIndex = 7;
            this.checkBox12.Text = "منوی برگه خروج - نمایش درخواست ها";
            this.checkBox12.UseVisualStyleBackColor = false;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.Color.Transparent;
            this.checkBox13.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p23", true));
            this.checkBox13.ForeColor = System.Drawing.Color.Black;
            this.checkBox13.Location = new System.Drawing.Point(333, 137);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(215, 21);
            this.checkBox13.TabIndex = 8;
            this.checkBox13.Text = "منوی برگه خروج - غیر فعال کردن برگه خروج";
            this.checkBox13.UseVisualStyleBackColor = false;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.BackColor = System.Drawing.Color.Transparent;
            this.checkBox29.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p22", true));
            this.checkBox29.ForeColor = System.Drawing.Color.Black;
            this.checkBox29.Location = new System.Drawing.Point(278, 164);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(270, 21);
            this.checkBox29.TabIndex = 9;
            this.checkBox29.Text = "منوی برگه خروج - گزارشات - اطلاعات آماری کلیه دوره ها";
            this.checkBox29.UseVisualStyleBackColor = false;
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel4;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "برگه خروج";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Controls.Add(this.buttonX9);
            this.superTabControlPanel5.Controls.Add(this.buttonX10);
            this.superTabControlPanel5.Controls.Add(this.labelX9);
            this.superTabControlPanel5.Controls.Add(this.labelX10);
            this.superTabControlPanel5.Controls.Add(this.chInternet);
            this.superTabControlPanel5.Controls.Add(this.checkBox26);
            this.superTabControlPanel5.Controls.Add(this.checkBox27);
            this.superTabControlPanel5.Controls.Add(this.checkBox28);
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(647, 470);
            this.superTabControlPanel5.TabIndex = 0;
            this.superTabControlPanel5.TabItem = this.superTabItem5;
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Transparent;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(29, 101);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX9.Size = new System.Drawing.Size(73, 63);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.Symbol = "";
            this.buttonX9.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX9.SymbolSize = 15F;
            this.buttonX9.TabIndex = 88;
            this.buttonX9.Text = "هیچکدام";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Transparent;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(29, 32);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX10.Size = new System.Drawing.Size(73, 63);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 15F;
            this.buttonX10.TabIndex = 87;
            this.buttonX10.Text = "همه";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(571, 61);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(25, 23);
            this.labelX9.Symbol = "";
            this.labelX9.SymbolColor = System.Drawing.Color.Orange;
            this.labelX9.TabIndex = 58;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(230, 64);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(335, 23);
            this.labelX10.TabIndex = 57;
            this.labelX10.Text = "در صورت غیرفعال ایتم فوق ، کلیه ایتم های زیر غیر قابل دسترس خواهند بود . ";
            // 
            // chInternet
            // 
            this.chInternet.AutoSize = true;
            this.chInternet.BackColor = System.Drawing.Color.Transparent;
            this.chInternet.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p97", true));
            this.chInternet.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.chInternet.ForeColor = System.Drawing.Color.Black;
            this.chInternet.Location = new System.Drawing.Point(400, 24);
            this.chInternet.Name = "chInternet";
            this.chInternet.Size = new System.Drawing.Size(210, 22);
            this.chInternet.TabIndex = 27;
            this.chInternet.Text = "دسترسی به بخش اصلی اینترنت و  موبایل";
            this.chInternet.UseVisualStyleBackColor = false;
            this.chInternet.CheckedChanged += new System.EventHandler(this.chInternet_CheckedChanged);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.BackColor = System.Drawing.Color.Transparent;
            this.checkBox26.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p27", true));
            this.checkBox26.ForeColor = System.Drawing.Color.Black;
            this.checkBox26.Location = new System.Drawing.Point(319, 103);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(246, 21);
            this.checkBox26.TabIndex = 24;
            this.checkBox26.Text = "منوی اینترنت و موبایل - افزودن کلید امنیتی موبایل";
            this.checkBox26.UseVisualStyleBackColor = false;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.BackColor = System.Drawing.Color.Transparent;
            this.checkBox27.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p26", true));
            this.checkBox27.ForeColor = System.Drawing.Color.Black;
            this.checkBox27.Location = new System.Drawing.Point(310, 129);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(255, 21);
            this.checkBox27.TabIndex = 25;
            this.checkBox27.Text = "منوی اینترنت و موبایل - مشاهده کلیه کلیدهای موبایل";
            this.checkBox27.UseVisualStyleBackColor = false;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.BackColor = System.Drawing.Color.Transparent;
            this.checkBox28.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p25", true));
            this.checkBox28.ForeColor = System.Drawing.Color.Black;
            this.checkBox28.Location = new System.Drawing.Point(334, 157);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(231, 21);
            this.checkBox28.TabIndex = 26;
            this.checkBox28.Text = "منوی اینترنت و موبایل - گزارشات اتصال موبایل";
            this.checkBox28.UseVisualStyleBackColor = false;
            // 
            // superTabItem5
            // 
            this.superTabItem5.AttachedControl = this.superTabControlPanel5;
            this.superTabItem5.GlobalItem = false;
            this.superTabItem5.Name = "superTabItem5";
            this.superTabItem5.Text = "اینترنت و موبایل";
            // 
            // admin_userTableAdapter
            // 
            this.admin_userTableAdapter.ClearBeforeFill = true;
            // 
            // superTabItem8
            // 
            this.superTabItem8.AttachedControl = this.superTabControlPanel8;
            this.superTabItem8.GlobalItem = false;
            this.superTabItem8.Name = "superTabItem8";
            this.superTabItem8.Text = "گزارشات حسابداری";
            // 
            // superTabControlPanel8
            // 
            this.superTabControlPanel8.Controls.Add(this.ch_hesabdari);
            this.superTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel8.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel8.Name = "superTabControlPanel8";
            this.superTabControlPanel8.Size = new System.Drawing.Size(647, 441);
            this.superTabControlPanel8.TabIndex = 0;
            this.superTabControlPanel8.TabItem = this.superTabItem8;
            // 
            // ch_hesabdari
            // 
            this.ch_hesabdari.AutoSize = true;
            this.ch_hesabdari.BackColor = System.Drawing.Color.Transparent;
            this.ch_hesabdari.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.adminuserBindingSource, "p107", true));
            this.ch_hesabdari.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.ch_hesabdari.ForeColor = System.Drawing.Color.Black;
            this.ch_hesabdari.Location = new System.Drawing.Point(400, 31);
            this.ch_hesabdari.Name = "ch_hesabdari";
            this.ch_hesabdari.Size = new System.Drawing.Size(221, 22);
            this.ch_hesabdari.TabIndex = 62;
            this.ch_hesabdari.Text = "دسترسی به بخش اصلی گزارشات حسابداری";
            this.ch_hesabdari.UseVisualStyleBackColor = false;
            // 
            // frm_user_permission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 626);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_user_permission";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_user_permission_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel7.ResumeLayout(false);
            this.superTabControlPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adminuserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).EndInit();
            this.superTabControlPanel6.ResumeLayout(false);
            this.superTabControlPanel6.PerformLayout();
            this.superTabControlPanel1.ResumeLayout(false);
            this.superTabControlPanel1.PerformLayout();
            this.superTabControlPanel3.ResumeLayout(false);
            this.superTabControlPanel3.PerformLayout();
            this.superTabControlPanel2.ResumeLayout(false);
            this.superTabControlPanel2.PerformLayout();
            this.grpSharj.ResumeLayout(false);
            this.grpSharj.PerformLayout();
            this.grpMoshtarekin.ResumeLayout(false);
            this.grpMoshtarekin.PerformLayout();
            this.superTabControlPanel4.ResumeLayout(false);
            this.superTabControlPanel4.PerformLayout();
            this.superTabControlPanel5.ResumeLayout(false);
            this.superTabControlPanel5.PerformLayout();
            this.superTabControlPanel8.ResumeLayout(false);
            this.superTabControlPanel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtusername;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFullname;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel6;
        private DevComponents.DotNetBar.SuperTabItem superTabItem6;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private DevComponents.DotNetBar.SuperTabItem superTabItem5;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private System.Windows.Forms.Panel grpSharj;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.Panel grpMoshtarekin;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private System.Windows.Forms.CheckBox chmodiriat_shahrak;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel7;
        private DevComponents.DotNetBar.SuperTabItem superTabItem7;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox chBargeh;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.CheckBox chInternet;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX12;
        private System.Windows.Forms.CheckBox chAb;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX14;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox chSharj;
        private DevComponents.DotNetBar.LabelX labelX15;
        private System.Windows.Forms.CheckBox chGozareshatmodiriat;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.CheckBox checkBox32;
        private DevComponents.DotNetBar.LabelX labelX17;
        private System.Windows.Forms.CheckBox checkBox31;
        private DevComponents.DotNetBar.LabelX labelX18;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox chMoshtarekin;
        private DevComponents.DotNetBar.LabelX labelX19;
        private System.Windows.Forms.CheckBox checkBox38;
        private DevComponents.DotNetBar.LabelX labelX20;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX labelX22;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox36;
        private user_Dataset user_Dataset;
        private System.Windows.Forms.BindingSource adminuserBindingSource;
        private user_DatasetTableAdapters.admin_userTableAdapter admin_userTableAdapter;
        private DevComponents.DotNetBar.ButtonX btnCreate;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private DevComponents.DotNetBar.ButtonX buttonX14;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox88;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox92;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox94;
        private System.Windows.Forms.CheckBox checkBox93;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel8;
        private System.Windows.Forms.CheckBox ch_hesabdari;
        private DevComponents.DotNetBar.SuperTabItem superTabItem8;
    }
}