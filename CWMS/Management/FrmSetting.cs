﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using System.Data.SqlClient;

namespace CWMS
{
    public partial class FrmSetting : MyMetroForm
    {
        public FrmSetting()
        {
            InitializeComponent();
        }

    

        private void FrmSetting_Load(object sender, EventArgs e)
        {
            DataTable dt_info = Classes.ClsMain.GetDataTable("select top(1) * from setting");
            if (dt_info.Rows.Count != 0)
            {
                txt_id.Text = dt_info.Rows[0]["shahrak_ID"].ToString();
                txt_name.Text = dt_info.Rows[0]["shahrak_name"].ToString();
                txt_phone.Text = dt_info.Rows[0]["phone"].ToString();
                txt_addresss.Text = dt_info.Rows[0]["address"].ToString();
                txtmaxkontor.Text = dt_info.Rows[0]["kontor_max"].ToString();
                txtbackuppath.Text = dt_info.Rows[0]["backup_path"].ToString();

                txt_sharj_mohasebe.Text = dt_info.Rows[0]["mohasebe_sharj"].ToString();
                txt_ab_mohasebe.Text = dt_info.Rows[0]["mohasebe_ab"].ToString();


                if (Convert.ToBoolean(dt_info.Rows[0]["a4print"]))
                    rdbozorg.Checked = true;
                else
                    rdforooshgah.Checked = true;
                chMainTabChange.Checked = Convert.ToBoolean(dt_info.Rows[0]["mainPageEtelaiehChaneg"]);
                byte[] logo = dt_info.Rows[0].Field<byte[]>("logo");
                if (logo != null)
                {
                    pictureBox1.Image = Image.FromStream(new MemoryStream(logo));
                }
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(openFileDialog1.FileName))
                {
                    SqlConnection con = new SqlConnection(Classes.ClsMain.ConnectionStr);
                    try
                    {
                        byte[] data = File.ReadAllBytes(openFileDialog1.FileName);
                        SqlCommand com = new SqlCommand("update setting set logo = @logo ", con);
                        com.Parameters.AddWithValue("@logo", data);
                        con.Open();
                        com.ExecuteNonQuery();
                        con.Close();
                        Image img = Image.FromFile(openFileDialog1.FileName);
                        pictureBox1.Image = img;
                        Payam.Show("لوگوی شهرک با موفقیت تغییر کرد");
                    }
                    catch (Exception ex)
                    {
                        Classes.ClsMain.logError(ex, "ذخیره لوگوی شهرک");
                        MessageBox.Show("خطا در خواندن لوگوی انتخاب شده");
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            /// این کار باعث می شود تاریخ اخرین پرداخت قبوض آب و شارژ بر اساس پرداختی های موجود در جدول قبض پرداخت به روز رسانی شود 
            /// البته بر اساس اخرین پرداختی انها چون امکان دارد چندین پرداخت برای مشترک ثبت شود .
            /// 
            DataTable dt_pardakhti_ab = Classes.ClsMain.GetDataTable("select gcode,tarikh_vosool from ghabz_pardakht where is_ab=1 order by gharardad desc,tarikh_vosool desc");
            List<string> lst = new List<string>();
            for (int i = 0; i < dt_pardakhti_ab.Rows.Count; i++)
            {
                if (lst.Contains(dt_pardakhti_ab.Rows[i]["gcode"].ToString()) == false)
                {
                    Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set has_pardakhti=1,last_pardakht='" + dt_pardakhti_ab.Rows[i]["tarikh_vosool"].ToString() + "' where gcode=" + dt_pardakhti_ab.Rows[i]["gcode"].ToString());
                    lst.Add(dt_pardakhti_ab.Rows[i]["gcode"].ToString());
                }

            }
            Payam.Show("قبوض آب به روزرسانی شدند");

            DataTable dt_pardakhti_sharj = Classes.ClsMain.GetDataTable("select gcode,tarikh_vosool from ghabz_pardakht where is_ab=0 order by gharardad desc,tarikh_vosool desc");
            List<string> lst2 = new List<string>();
            for (int i = 0; i < dt_pardakhti_sharj.Rows.Count; i++)
            {
                if (lst2.Contains(dt_pardakhti_sharj.Rows[i]["gcode"].ToString()) == false)
                {
                    Classes.ClsMain.ExecuteNoneQuery("update sharj_ghabz set has_pardakhti=1,last_pardakht='" + dt_pardakhti_sharj.Rows[i]["tarikh_vosool"].ToString() + "' where gcode=" + dt_pardakhti_sharj.Rows[i]["gcode"].ToString());
                    lst2.Add(dt_pardakhti_sharj.Rows[i]["gcode"].ToString());
                }

            }
            Payam.Show("قبوض شارژ به روزرسانی شدند");

        }

        private void btn_save_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p77)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 77);

                try
                {
                    Convert.ToInt32(txtmaxkontor.Text);
                }
                catch
                {
                    Payam.Show("شماره کنتور نامعتبر می باشد");
                    txtmaxkontor.SelectAll();
                    txtmaxkontor.Focus();
                    return;
                }

                int a4print = 1;
                if (rdforooshgah.Checked)
                    a4print = 0;


                int mainPageEtelaiehChaneg = 0;
                if (chMainTabChange.Checked)
                    mainPageEtelaiehChaneg = 1;
                if (txt_id.Text.Trim() != "")
                {
                    Classes.ClsMain.ExecuteNoneQuery("update setting set mohasebe_sharj='" + txt_sharj_mohasebe.Text +  "',mohasebe_ab='" + txt_ab_mohasebe.Text +  "',  id=" + txt_id.Text + ",shahrak_name='" + txt_name.Text + "',phone='" + txt_phone.Text + "',address='" + txt_addresss.Text + "',smsnumber1='" + txtsms1.Text + "',smsnumber2='" + txtsms2.Text + "',kontor_max=" + txtmaxkontor.Text + ",a4print=" + a4print + ",mainPageEtelaiehChaneg=" + mainPageEtelaiehChaneg + ",backup_path='" + txtbackuppath.Text + "'");
                    Payam.Show("اطلاعات شهرک با موفقیت در سیستم ثبت شد");
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            btn_save.PerformClick();
        }
    }
}