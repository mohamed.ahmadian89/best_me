﻿namespace CWMS.Management
{
    partial class frmCreateShenase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.sharj_progress = new System.Windows.Forms.ProgressBar();
            this.cmb_sharj_all = new System.Windows.Forms.ComboBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.rd_mandeh_sharj = new System.Windows.Forms.RadioButton();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.rdMablaghKolSharj = new System.Windows.Forms.RadioButton();
            this.btn_create_for_all = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_gharardad_sharj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.cmb_sharj_forOne = new System.Windows.Forms.ComboBox();
            this.rd_mandeh_moshtarek_sharj = new System.Windows.Forms.RadioButton();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.rd_mablaghkol_moshtarek_sharj = new System.Windows.Forms.RadioButton();
            this.btn_create_for_one = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_gharardad_ab = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.cmb_ab_one = new System.Windows.Forms.ComboBox();
            this.rd_ab_mande_one = new System.Windows.Forms.RadioButton();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.rd_ab_mablaghkol_one = new System.Windows.Forms.RadioButton();
            this.btn_create_forOne = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ab_progress = new System.Windows.Forms.ProgressBar();
            this.cmbAbAll = new System.Windows.Forms.ComboBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.rd_ab_mande_All = new System.Windows.Forms.RadioButton();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.rd_ab_mablaghkol_all = new System.Windows.Forms.RadioButton();
            this.btn_create_ab_For_all = new DevComponents.DotNetBar.ButtonX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.sharj_progress);
            this.groupPanel1.Controls.Add(this.cmb_sharj_all);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.rd_mandeh_sharj);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.rdMablaghKolSharj);
            this.groupPanel1.Controls.Add(this.btn_create_for_all);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(490, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(419, 331);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "قبوض شارژ یک دوره";
            // 
            // sharj_progress
            // 
            this.sharj_progress.BackColor = System.Drawing.Color.White;
            this.sharj_progress.ForeColor = System.Drawing.Color.Black;
            this.sharj_progress.Location = new System.Drawing.Point(11, 265);
            this.sharj_progress.Name = "sharj_progress";
            this.sharj_progress.Size = new System.Drawing.Size(380, 23);
            this.sharj_progress.TabIndex = 54;
            // 
            // cmb_sharj_all
            // 
            this.cmb_sharj_all.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_sharj_all.BackColor = System.Drawing.Color.White;
            this.cmb_sharj_all.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sharj_all.ForeColor = System.Drawing.Color.Black;
            this.cmb_sharj_all.FormattingEnabled = true;
            this.cmb_sharj_all.Location = new System.Drawing.Point(153, 171);
            this.cmb_sharj_all.Name = "cmb_sharj_all";
            this.cmb_sharj_all.Size = new System.Drawing.Size(69, 28);
            this.cmb_sharj_all.TabIndex = 51;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(3, 117);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(374, 48);
            this.labelX2.TabIndex = 36;
            this.labelX2.Text = "در این حالت ، شناسه پرداخت بر اساس مبلغ باقیمانده قبض که هنوز پرداخت نشده است تول" +
    "ید می شود ";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX2.WordWrap = true;
            // 
            // rd_mandeh_sharj
            // 
            this.rd_mandeh_sharj.AutoSize = true;
            this.rd_mandeh_sharj.BackColor = System.Drawing.Color.Transparent;
            this.rd_mandeh_sharj.ForeColor = System.Drawing.Color.Black;
            this.rd_mandeh_sharj.Location = new System.Drawing.Point(76, 89);
            this.rd_mandeh_sharj.Name = "rd_mandeh_sharj";
            this.rd_mandeh_sharj.Size = new System.Drawing.Size(315, 25);
            this.rd_mandeh_sharj.TabIndex = 35;
            this.rd_mandeh_sharj.Text = "شناسه پرداخت بر اساس مبلغ باقیمانده قبض تولید شود";
            this.rd_mandeh_sharj.UseVisualStyleBackColor = false;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(207, 172);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(184, 23);
            this.labelX1.TabIndex = 32;
            this.labelX1.Text = "برای کلیه قبوض موجود در دوره  :";
            // 
            // rdMablaghKolSharj
            // 
            this.rdMablaghKolSharj.AutoSize = true;
            this.rdMablaghKolSharj.BackColor = System.Drawing.Color.Transparent;
            this.rdMablaghKolSharj.Checked = true;
            this.rdMablaghKolSharj.ForeColor = System.Drawing.Color.Black;
            this.rdMablaghKolSharj.Location = new System.Drawing.Point(134, 28);
            this.rdMablaghKolSharj.Name = "rdMablaghKolSharj";
            this.rdMablaghKolSharj.Size = new System.Drawing.Size(257, 25);
            this.rdMablaghKolSharj.TabIndex = 34;
            this.rdMablaghKolSharj.TabStop = true;
            this.rdMablaghKolSharj.Text = "شناسه پرداخت بر اساس مبلغ کل تولید شود";
            this.rdMablaghKolSharj.UseVisualStyleBackColor = false;
            // 
            // btn_create_for_all
            // 
            this.btn_create_for_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_for_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_for_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_for_all.Location = new System.Drawing.Point(9, 217);
            this.btn_create_for_all.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_create_for_all.Name = "btn_create_for_all";
            this.btn_create_for_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_create_for_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_create_for_all.Size = new System.Drawing.Size(231, 31);
            this.btn_create_for_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_for_all.Symbol = "";
            this.btn_create_for_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_for_all.SymbolSize = 12F;
            this.btn_create_for_all.TabIndex = 33;
            this.btn_create_for_all.Text = "تولید شناسه قبض برای کلیه قبوض";
            this.btn_create_for_all.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(9, 47);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(368, 48);
            this.labelX4.TabIndex = 32;
            this.labelX4.Text = "مبلغی که در شناسه پرداخت قرار میگیرد ، برابر است با مبلغ کل قبض";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX4.WordWrap = true;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.txt_gharardad_sharj);
            this.groupPanel3.Controls.Add(this.labelX8);
            this.groupPanel3.Controls.Add(this.cmb_sharj_forOne);
            this.groupPanel3.Controls.Add(this.rd_mandeh_moshtarek_sharj);
            this.groupPanel3.Controls.Add(this.labelX6);
            this.groupPanel3.Controls.Add(this.rd_mablaghkol_moshtarek_sharj);
            this.groupPanel3.Controls.Add(this.btn_create_for_one);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(490, 358);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(419, 202);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 6;
            this.groupPanel3.Text = "قبض شارژ مشترک";
            // 
            // txt_gharardad_sharj
            // 
            this.txt_gharardad_sharj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_gharardad_sharj.Border.Class = "TextBoxBorder";
            this.txt_gharardad_sharj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_gharardad_sharj.DisabledBackColor = System.Drawing.Color.White;
            this.txt_gharardad_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_gharardad_sharj.Location = new System.Drawing.Point(214, 79);
            this.txt_gharardad_sharj.Name = "txt_gharardad_sharj";
            this.txt_gharardad_sharj.PreventEnterBeep = true;
            this.txt_gharardad_sharj.Size = new System.Drawing.Size(79, 28);
            this.txt_gharardad_sharj.TabIndex = 53;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(299, 82);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(77, 23);
            this.labelX8.TabIndex = 52;
            this.labelX8.Text = "برای قرارداد :";
            // 
            // cmb_sharj_forOne
            // 
            this.cmb_sharj_forOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_sharj_forOne.BackColor = System.Drawing.Color.White;
            this.cmb_sharj_forOne.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sharj_forOne.ForeColor = System.Drawing.Color.Black;
            this.cmb_sharj_forOne.FormattingEnabled = true;
            this.cmb_sharj_forOne.Location = new System.Drawing.Point(63, 79);
            this.cmb_sharj_forOne.Name = "cmb_sharj_forOne";
            this.cmb_sharj_forOne.Size = new System.Drawing.Size(69, 28);
            this.cmb_sharj_forOne.TabIndex = 51;
            // 
            // rd_mandeh_moshtarek_sharj
            // 
            this.rd_mandeh_moshtarek_sharj.AutoSize = true;
            this.rd_mandeh_moshtarek_sharj.BackColor = System.Drawing.Color.Transparent;
            this.rd_mandeh_moshtarek_sharj.ForeColor = System.Drawing.Color.Black;
            this.rd_mandeh_moshtarek_sharj.Location = new System.Drawing.Point(76, 48);
            this.rd_mandeh_moshtarek_sharj.Name = "rd_mandeh_moshtarek_sharj";
            this.rd_mandeh_moshtarek_sharj.Size = new System.Drawing.Size(315, 25);
            this.rd_mandeh_moshtarek_sharj.TabIndex = 35;
            this.rd_mandeh_moshtarek_sharj.Text = "شناسه پرداخت بر اساس مبلغ باقیمانده قبض تولید شود";
            this.rd_mandeh_moshtarek_sharj.UseVisualStyleBackColor = false;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(138, 82);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(56, 23);
            this.labelX6.TabIndex = 32;
            this.labelX6.Text = "در دوره :";
            // 
            // rd_mablaghkol_moshtarek_sharj
            // 
            this.rd_mablaghkol_moshtarek_sharj.AutoSize = true;
            this.rd_mablaghkol_moshtarek_sharj.BackColor = System.Drawing.Color.Transparent;
            this.rd_mablaghkol_moshtarek_sharj.Checked = true;
            this.rd_mablaghkol_moshtarek_sharj.ForeColor = System.Drawing.Color.Black;
            this.rd_mablaghkol_moshtarek_sharj.Location = new System.Drawing.Point(134, 16);
            this.rd_mablaghkol_moshtarek_sharj.Name = "rd_mablaghkol_moshtarek_sharj";
            this.rd_mablaghkol_moshtarek_sharj.Size = new System.Drawing.Size(257, 25);
            this.rd_mablaghkol_moshtarek_sharj.TabIndex = 34;
            this.rd_mablaghkol_moshtarek_sharj.TabStop = true;
            this.rd_mablaghkol_moshtarek_sharj.Text = "شناسه پرداخت بر اساس مبلغ کل تولید شود";
            this.rd_mablaghkol_moshtarek_sharj.UseVisualStyleBackColor = false;
            // 
            // btn_create_for_one
            // 
            this.btn_create_for_one.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_for_one.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_for_one.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_for_one.Location = new System.Drawing.Point(11, 120);
            this.btn_create_for_one.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_create_for_one.Name = "btn_create_for_one";
            this.btn_create_for_one.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_create_for_one.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_create_for_one.Size = new System.Drawing.Size(231, 31);
            this.btn_create_for_one.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_for_one.Symbol = "";
            this.btn_create_for_one.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_for_one.SymbolSize = 12F;
            this.btn_create_for_one.TabIndex = 33;
            this.btn_create_for_one.Text = "تولید شناسه قبض برای مشترک مربوطه ";
            this.btn_create_for_one.Click += new System.EventHandler(this.btn_create_for_one_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_gharardad_ab);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.Controls.Add(this.cmb_ab_one);
            this.groupPanel2.Controls.Add(this.rd_ab_mande_one);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.rd_ab_mablaghkol_one);
            this.groupPanel2.Controls.Add(this.btn_create_forOne);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(42, 358);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(419, 202);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 8;
            this.groupPanel2.Text = "قبض آب مشترک";
            // 
            // txt_gharardad_ab
            // 
            this.txt_gharardad_ab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_gharardad_ab.Border.Class = "TextBoxBorder";
            this.txt_gharardad_ab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_gharardad_ab.DisabledBackColor = System.Drawing.Color.White;
            this.txt_gharardad_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_gharardad_ab.Location = new System.Drawing.Point(214, 79);
            this.txt_gharardad_ab.Name = "txt_gharardad_ab";
            this.txt_gharardad_ab.PreventEnterBeep = true;
            this.txt_gharardad_ab.Size = new System.Drawing.Size(79, 28);
            this.txt_gharardad_ab.TabIndex = 53;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(299, 82);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(77, 23);
            this.labelX3.TabIndex = 52;
            this.labelX3.Text = "برای قرارداد :";
            // 
            // cmb_ab_one
            // 
            this.cmb_ab_one.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_ab_one.BackColor = System.Drawing.Color.White;
            this.cmb_ab_one.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ab_one.ForeColor = System.Drawing.Color.Black;
            this.cmb_ab_one.FormattingEnabled = true;
            this.cmb_ab_one.Location = new System.Drawing.Point(57, 79);
            this.cmb_ab_one.Name = "cmb_ab_one";
            this.cmb_ab_one.Size = new System.Drawing.Size(69, 28);
            this.cmb_ab_one.TabIndex = 51;
            // 
            // rd_ab_mande_one
            // 
            this.rd_ab_mande_one.AutoSize = true;
            this.rd_ab_mande_one.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab_mande_one.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_mande_one.Location = new System.Drawing.Point(76, 48);
            this.rd_ab_mande_one.Name = "rd_ab_mande_one";
            this.rd_ab_mande_one.Size = new System.Drawing.Size(315, 25);
            this.rd_ab_mande_one.TabIndex = 35;
            this.rd_ab_mande_one.Text = "شناسه پرداخت بر اساس مبلغ باقیمانده قبض تولید شود";
            this.rd_ab_mande_one.UseVisualStyleBackColor = false;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(138, 82);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(56, 23);
            this.labelX5.TabIndex = 32;
            this.labelX5.Text = "در دوره :";
            // 
            // rd_ab_mablaghkol_one
            // 
            this.rd_ab_mablaghkol_one.AutoSize = true;
            this.rd_ab_mablaghkol_one.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab_mablaghkol_one.Checked = true;
            this.rd_ab_mablaghkol_one.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_mablaghkol_one.Location = new System.Drawing.Point(134, 16);
            this.rd_ab_mablaghkol_one.Name = "rd_ab_mablaghkol_one";
            this.rd_ab_mablaghkol_one.Size = new System.Drawing.Size(257, 25);
            this.rd_ab_mablaghkol_one.TabIndex = 34;
            this.rd_ab_mablaghkol_one.TabStop = true;
            this.rd_ab_mablaghkol_one.Text = "شناسه پرداخت بر اساس مبلغ کل تولید شود";
            this.rd_ab_mablaghkol_one.UseVisualStyleBackColor = false;
            // 
            // btn_create_forOne
            // 
            this.btn_create_forOne.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_forOne.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_forOne.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_forOne.Location = new System.Drawing.Point(11, 120);
            this.btn_create_forOne.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_create_forOne.Name = "btn_create_forOne";
            this.btn_create_forOne.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_create_forOne.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_create_forOne.Size = new System.Drawing.Size(231, 31);
            this.btn_create_forOne.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_forOne.Symbol = "";
            this.btn_create_forOne.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_forOne.SymbolSize = 12F;
            this.btn_create_forOne.TabIndex = 33;
            this.btn_create_forOne.Text = "تولید شناسه قبض برای مشترک مربوطه ";
            this.btn_create_forOne.Click += new System.EventHandler(this.btn_create_forOne_Click);
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.ab_progress);
            this.groupPanel4.Controls.Add(this.cmbAbAll);
            this.groupPanel4.Controls.Add(this.labelX7);
            this.groupPanel4.Controls.Add(this.rd_ab_mande_All);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.rd_ab_mablaghkol_all);
            this.groupPanel4.Controls.Add(this.btn_create_ab_For_all);
            this.groupPanel4.Controls.Add(this.labelX10);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(42, 12);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(419, 331);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 7;
            this.groupPanel4.Text = "قبوض آب یک دوره";
            // 
            // ab_progress
            // 
            this.ab_progress.BackColor = System.Drawing.Color.White;
            this.ab_progress.ForeColor = System.Drawing.Color.Black;
            this.ab_progress.Location = new System.Drawing.Point(11, 265);
            this.ab_progress.Name = "ab_progress";
            this.ab_progress.Size = new System.Drawing.Size(380, 23);
            this.ab_progress.TabIndex = 54;
            // 
            // cmbAbAll
            // 
            this.cmbAbAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAbAll.BackColor = System.Drawing.Color.White;
            this.cmbAbAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAbAll.ForeColor = System.Drawing.Color.Black;
            this.cmbAbAll.FormattingEnabled = true;
            this.cmbAbAll.Location = new System.Drawing.Point(147, 171);
            this.cmbAbAll.Name = "cmbAbAll";
            this.cmbAbAll.Size = new System.Drawing.Size(69, 28);
            this.cmbAbAll.TabIndex = 51;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(3, 117);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(374, 48);
            this.labelX7.TabIndex = 36;
            this.labelX7.Text = "در این حالت ، شناسه پرداخت بر اساس مبلغ باقیمانده قبض که هنوز پرداخت نشده است تول" +
    "ید می شود ";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX7.WordWrap = true;
            // 
            // rd_ab_mande_All
            // 
            this.rd_ab_mande_All.AutoSize = true;
            this.rd_ab_mande_All.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab_mande_All.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_mande_All.Location = new System.Drawing.Point(76, 89);
            this.rd_ab_mande_All.Name = "rd_ab_mande_All";
            this.rd_ab_mande_All.Size = new System.Drawing.Size(315, 25);
            this.rd_ab_mande_All.TabIndex = 35;
            this.rd_ab_mande_All.Text = "شناسه پرداخت بر اساس مبلغ باقیمانده قبض تولید شود";
            this.rd_ab_mande_All.UseVisualStyleBackColor = false;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(207, 172);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(184, 23);
            this.labelX9.TabIndex = 32;
            this.labelX9.Text = "برای کلیه قبوض موجود در دوره  :";
            // 
            // rd_ab_mablaghkol_all
            // 
            this.rd_ab_mablaghkol_all.AutoSize = true;
            this.rd_ab_mablaghkol_all.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab_mablaghkol_all.Checked = true;
            this.rd_ab_mablaghkol_all.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_mablaghkol_all.Location = new System.Drawing.Point(134, 28);
            this.rd_ab_mablaghkol_all.Name = "rd_ab_mablaghkol_all";
            this.rd_ab_mablaghkol_all.Size = new System.Drawing.Size(257, 25);
            this.rd_ab_mablaghkol_all.TabIndex = 34;
            this.rd_ab_mablaghkol_all.TabStop = true;
            this.rd_ab_mablaghkol_all.Text = "شناسه پرداخت بر اساس مبلغ کل تولید شود";
            this.rd_ab_mablaghkol_all.UseVisualStyleBackColor = false;
            // 
            // btn_create_ab_For_all
            // 
            this.btn_create_ab_For_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_ab_For_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_ab_For_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_ab_For_all.Location = new System.Drawing.Point(9, 217);
            this.btn_create_ab_For_all.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_create_ab_For_all.Name = "btn_create_ab_For_all";
            this.btn_create_ab_For_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_create_ab_For_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_create_ab_For_all.Size = new System.Drawing.Size(231, 31);
            this.btn_create_ab_For_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_ab_For_all.Symbol = "";
            this.btn_create_ab_For_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_ab_For_all.SymbolSize = 12F;
            this.btn_create_ab_For_all.TabIndex = 33;
            this.btn_create_ab_For_all.Text = "تولید شناسه قبض برای کلیه قبوض";
            this.btn_create_ab_For_all.Click += new System.EventHandler(this.btn_create_ab_For_all_Click);
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(9, 47);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(368, 48);
            this.labelX10.TabIndex = 32;
            this.labelX10.Text = "مبلغی که در شناسه پرداخت قرار میگیرد ، برابر است با مبلغ کل قبض";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX10.WordWrap = true;
            // 
            // frmCreateShenase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 571);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreateShenase";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frmCreateShenase_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.RadioButton rd_mandeh_sharj;
        private System.Windows.Forms.RadioButton rdMablaghKolSharj;
        private DevComponents.DotNetBar.ButtonX btn_create_for_all;
        private System.Windows.Forms.ComboBox cmb_sharj_all;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX8;
        private System.Windows.Forms.ComboBox cmb_sharj_forOne;
        private System.Windows.Forms.RadioButton rd_mandeh_moshtarek_sharj;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.RadioButton rd_mablaghkol_moshtarek_sharj;
        private DevComponents.DotNetBar.ButtonX btn_create_for_one;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_gharardad_sharj;
        private System.Windows.Forms.ProgressBar sharj_progress;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_gharardad_ab;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.ComboBox cmb_ab_one;
        private System.Windows.Forms.RadioButton rd_ab_mande_one;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.RadioButton rd_ab_mablaghkol_one;
        private DevComponents.DotNetBar.ButtonX btn_create_forOne;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private System.Windows.Forms.ProgressBar ab_progress;
        private System.Windows.Forms.ComboBox cmbAbAll;
        private DevComponents.DotNetBar.LabelX labelX7;
        private System.Windows.Forms.RadioButton rd_ab_mande_All;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.RadioButton rd_ab_mablaghkol_all;
        private DevComponents.DotNetBar.ButtonX btn_create_ab_For_all;
        private DevComponents.DotNetBar.LabelX labelX10;
    }
}