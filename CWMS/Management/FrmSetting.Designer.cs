﻿namespace CWMS
{
    partial class FrmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtbackuppath = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.chMainTabChange = new System.Windows.Forms.CheckBox();
            this.rdbozorg = new System.Windows.Forms.RadioButton();
            this.rdforooshgah = new System.Windows.Forms.RadioButton();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtsms2 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.txtsms1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtmaxkontor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_id = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.txt_addresss = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txt_phone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_name = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.txt_ab_mohasebe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.txt_sharj_mohasebe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(22, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(750, 525);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(742, 495);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "تنظیمات کلی";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(742, 495);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "توضیحات شارژ و آب";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtbackuppath);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.pictureBox1);
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.chMainTabChange);
            this.groupPanel1.Controls.Add(this.rdbozorg);
            this.groupPanel1.Controls.Add(this.rdforooshgah);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.txtsms2);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.txtsms1);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.txtmaxkontor);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txt_id);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.btn_save);
            this.groupPanel1.Controls.Add(this.txt_addresss);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.txt_phone);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txt_name);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(8, 8);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(727, 473);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 1;
            this.groupPanel1.Text = "اطلاعات شهرک";
            // 
            // txtbackuppath
            // 
            this.txtbackuppath.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbackuppath.Border.Class = "TextBoxBorder";
            this.txtbackuppath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbackuppath.DisabledBackColor = System.Drawing.Color.White;
            this.txtbackuppath.ForeColor = System.Drawing.Color.Black;
            this.txtbackuppath.Location = new System.Drawing.Point(194, 341);
            this.txtbackuppath.Name = "txtbackuppath";
            this.txtbackuppath.PreventEnterBeep = true;
            this.txtbackuppath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtbackuppath.Size = new System.Drawing.Size(335, 24);
            this.txtbackuppath.TabIndex = 26;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(522, 342);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(189, 23);
            this.labelX11.TabIndex = 27;
            this.labelX11.Text = "مسیر پشتیبانی در هنگام بسته شدن برنامه :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.ForeColor = System.Drawing.Color.Black;
            this.pictureBox1.Location = new System.Drawing.Point(15, 238);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonX1.Location = new System.Drawing.Point(15, 209);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(101, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 23;
            this.buttonX1.Text = "انتخاب لوگو";
            // 
            // chMainTabChange
            // 
            this.chMainTabChange.AutoSize = true;
            this.chMainTabChange.BackColor = System.Drawing.Color.Transparent;
            this.chMainTabChange.ForeColor = System.Drawing.Color.Black;
            this.chMainTabChange.Location = new System.Drawing.Point(284, 314);
            this.chMainTabChange.Name = "chMainTabChange";
            this.chMainTabChange.Size = new System.Drawing.Size(390, 21);
            this.chMainTabChange.TabIndex = 22;
            this.chMainTabChange.Text = "در هر 15 ثانیه پنل اطلاعیه ها جایگزین پنل نمایش اطلاعات کاربری در صفحه اصلی شود";
            this.chMainTabChange.UseVisualStyleBackColor = false;
            // 
            // rdbozorg
            // 
            this.rdbozorg.AutoSize = true;
            this.rdbozorg.BackColor = System.Drawing.Color.Transparent;
            this.rdbozorg.Checked = true;
            this.rdbozorg.ForeColor = System.Drawing.Color.Black;
            this.rdbozorg.Location = new System.Drawing.Point(345, 265);
            this.rdbozorg.Name = "rdbozorg";
            this.rdbozorg.Size = new System.Drawing.Size(276, 21);
            this.rdbozorg.TabIndex = 21;
            this.rdbozorg.TabStop = true;
            this.rdbozorg.Text = "چاپ قبوض آب و شارژ بر اساس پرینترهای سایز بزرگ -A4\r\n";
            this.rdbozorg.UseVisualStyleBackColor = false;
            // 
            // rdforooshgah
            // 
            this.rdforooshgah.AutoSize = true;
            this.rdforooshgah.BackColor = System.Drawing.Color.Transparent;
            this.rdforooshgah.ForeColor = System.Drawing.Color.Black;
            this.rdforooshgah.Location = new System.Drawing.Point(367, 238);
            this.rdforooshgah.Name = "rdforooshgah";
            this.rdforooshgah.Size = new System.Drawing.Size(254, 21);
            this.rdforooshgah.TabIndex = 20;
            this.rdforooshgah.Text = "چاپ قبوض آب و شارژ بر اساس پرینترهای فروشگاهی";
            this.rdforooshgah.UseVisualStyleBackColor = false;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(367, 209);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(307, 23);
            this.labelX10.TabIndex = 19;
            this.labelX10.Text = "نحوه چاپ قبوض :";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(367, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(307, 23);
            this.labelX9.TabIndex = 18;
            this.labelX9.Text = "اطلاعات اولیه شهرک :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(367, 135);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(307, 23);
            this.labelX8.TabIndex = 17;
            this.labelX8.Text = "شماره پیامک های شهرک صنعتی جهت استفاده از سامانه موبایل مشترکین :";
            // 
            // txtsms2
            // 
            this.txtsms2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtsms2.Border.Class = "TextBoxBorder";
            this.txtsms2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtsms2.DisabledBackColor = System.Drawing.Color.White;
            this.txtsms2.ForeColor = System.Drawing.Color.Black;
            this.txtsms2.Location = new System.Drawing.Point(183, 165);
            this.txtsms2.Name = "txtsms2";
            this.txtsms2.PreventEnterBeep = true;
            this.txtsms2.Size = new System.Drawing.Size(119, 24);
            this.txtsms2.TabIndex = 15;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(232, 166);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(139, 23);
            this.labelX7.TabIndex = 16;
            this.labelX7.Text = "شماره پیامک 2:";
            // 
            // txtsms1
            // 
            this.txtsms1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtsms1.Border.Class = "TextBoxBorder";
            this.txtsms1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtsms1.DisabledBackColor = System.Drawing.Color.White;
            this.txtsms1.ForeColor = System.Drawing.Color.Black;
            this.txtsms1.Location = new System.Drawing.Point(386, 165);
            this.txtsms1.Name = "txtsms1";
            this.txtsms1.PreventEnterBeep = true;
            this.txtsms1.Size = new System.Drawing.Size(119, 24);
            this.txtsms1.TabIndex = 13;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(435, 166);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(139, 23);
            this.labelX6.TabIndex = 14;
            this.labelX6.Text = "شماره پیامک 1:";
            // 
            // txtmaxkontor
            // 
            this.txtmaxkontor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmaxkontor.Border.Class = "TextBoxBorder";
            this.txtmaxkontor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmaxkontor.DisabledBackColor = System.Drawing.Color.White;
            this.txtmaxkontor.ForeColor = System.Drawing.Color.Black;
            this.txtmaxkontor.Location = new System.Drawing.Point(422, 93);
            this.txtmaxkontor.Name = "txtmaxkontor";
            this.txtmaxkontor.PreventEnterBeep = true;
            this.txtmaxkontor.Size = new System.Drawing.Size(119, 24);
            this.txtmaxkontor.TabIndex = 11;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(516, 94);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(139, 23);
            this.labelX5.TabIndex = 12;
            this.labelX5.Text = "حداکثر شماره کنتور اب:";
            // 
            // txt_id
            // 
            this.txt_id.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_id.Border.Class = "TextBoxBorder";
            this.txt_id.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_id.DisabledBackColor = System.Drawing.Color.White;
            this.txt_id.ForeColor = System.Drawing.Color.Black;
            this.txt_id.Location = new System.Drawing.Point(516, 33);
            this.txt_id.Name = "txt_id";
            this.txt_id.PreventEnterBeep = true;
            this.txt_id.Size = new System.Drawing.Size(87, 24);
            this.txt_id.TabIndex = 0;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(573, 34);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(82, 23);
            this.labelX4.TabIndex = 10;
            this.labelX4.Text = "کد شهرک :";
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_save.Location = new System.Drawing.Point(15, 422);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Size = new System.Drawing.Size(223, 23);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 4;
            this.btn_save.Text = "ثبت اطلاعات شهرک";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click_1);
            // 
            // txt_addresss
            // 
            this.txt_addresss.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_addresss.Border.Class = "TextBoxBorder";
            this.txt_addresss.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_addresss.DisabledBackColor = System.Drawing.Color.White;
            this.txt_addresss.ForeColor = System.Drawing.Color.Black;
            this.txt_addresss.Location = new System.Drawing.Point(15, 63);
            this.txt_addresss.Name = "txt_addresss";
            this.txt_addresss.PreventEnterBeep = true;
            this.txt_addresss.Size = new System.Drawing.Size(409, 24);
            this.txt_addresss.TabIndex = 3;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(422, 64);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(82, 23);
            this.labelX3.TabIndex = 7;
            this.labelX3.Text = "آدرس :";
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_phone.Border.Class = "TextBoxBorder";
            this.txt_phone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_phone.DisabledBackColor = System.Drawing.Color.White;
            this.txt_phone.ForeColor = System.Drawing.Color.Black;
            this.txt_phone.Location = new System.Drawing.Point(516, 63);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.PreventEnterBeep = true;
            this.txt_phone.Size = new System.Drawing.Size(87, 24);
            this.txt_phone.TabIndex = 2;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(573, 64);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(82, 23);
            this.labelX1.TabIndex = 5;
            this.labelX1.Text = "تلفن :";
            // 
            // txt_name
            // 
            this.txt_name.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_name.Border.Class = "TextBoxBorder";
            this.txt_name.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_name.DisabledBackColor = System.Drawing.Color.White;
            this.txt_name.ForeColor = System.Drawing.Color.Black;
            this.txt_name.Location = new System.Drawing.Point(15, 33);
            this.txt_name.Name = "txt_name";
            this.txt_name.PreventEnterBeep = true;
            this.txt_name.Size = new System.Drawing.Size(409, 24);
            this.txt_name.TabIndex = 1;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(422, 34);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(82, 23);
            this.labelX2.TabIndex = 3;
            this.labelX2.Text = "نام کامل شهرک :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.txt_ab_mohasebe);
            this.groupPanel2.Controls.Add(this.labelX20);
            this.groupPanel2.Controls.Add(this.txt_sharj_mohasebe);
            this.groupPanel2.Controls.Add(this.labelX22);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(8, 11);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(727, 473);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 2;
            this.groupPanel2.Text = "اطلاعات شهرک";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonX3.Location = new System.Drawing.Point(15, 412);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(223, 23);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 4;
            this.buttonX3.Text = "ثبت اطلاعات شهرک";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // txt_ab_mohasebe
            // 
            this.txt_ab_mohasebe.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_mohasebe.Border.Class = "TextBoxBorder";
            this.txt_ab_mohasebe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_mohasebe.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_mohasebe.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_mohasebe.Location = new System.Drawing.Point(15, 248);
            this.txt_ab_mohasebe.Multiline = true;
            this.txt_ab_mohasebe.Name = "txt_ab_mohasebe";
            this.txt_ab_mohasebe.PreventEnterBeep = true;
            this.txt_ab_mohasebe.Size = new System.Drawing.Size(671, 141);
            this.txt_ab_mohasebe.TabIndex = 3;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(521, 219);
            this.labelX20.Name = "labelX20";
            this.labelX20.Size = new System.Drawing.Size(159, 23);
            this.labelX20.TabIndex = 7;
            this.labelX20.Text = "توضیحات مربوط به نحوه محاسبه آب";
            // 
            // txt_sharj_mohasebe
            // 
            this.txt_sharj_mohasebe.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sharj_mohasebe.Border.Class = "TextBoxBorder";
            this.txt_sharj_mohasebe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sharj_mohasebe.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sharj_mohasebe.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_mohasebe.Location = new System.Drawing.Point(15, 60);
            this.txt_sharj_mohasebe.Multiline = true;
            this.txt_sharj_mohasebe.Name = "txt_sharj_mohasebe";
            this.txt_sharj_mohasebe.PreventEnterBeep = true;
            this.txt_sharj_mohasebe.Size = new System.Drawing.Size(671, 141);
            this.txt_sharj_mohasebe.TabIndex = 1;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(521, 31);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(165, 23);
            this.labelX22.TabIndex = 3;
            this.labelX22.Text = "توضیحات مربوط به نحوه محاسبه شارژ";
            // 
            // FrmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 538);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSetting";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtbackuppath;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.CheckBox chMainTabChange;
        private System.Windows.Forms.RadioButton rdbozorg;
        private System.Windows.Forms.RadioButton rdforooshgah;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtsms2;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtsms1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtmaxkontor;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_id;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_addresss;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_phone;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_name;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ab_mohasebe;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sharj_mohasebe;
        private DevComponents.DotNetBar.LabelX labelX22;
    }
}