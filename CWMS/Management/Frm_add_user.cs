﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Management
{
    public partial class Frm_add_user : MyMetroForm
    {
        public Frm_add_user()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p31)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 31,"افزودن کاربر جدید" + txtusername.Text);

                if (txtusername.Text.Trim() == "")
                {
                    txtusername.Focus();
                    Payam.Show("لطفا نام کاربری را وارد نمایید");
                }
                else if (txtpassword.Text.Trim() == "")
                {
                    Payam.Show("لطفا رمز عبور را وارد نمایید");
                    txtpassword.Focus();

                }
                else if (Classes.ClsMain.ExecuteScalar("select count(*) from admin_user where username='" + txtusername.Text + "'").ToString() != "0")
                {
                    Payam.Show("نام کاربری تکراری می باشد. لطفا نام کاربری دیگری انتخاب نمایید");
                    txtusername.SelectAll();
                    txtusername.Focus();
                }
                else
                {
                    Classes.ClsMain.ExecuteNoneQuery("insert into admin_user (username,password,fullname,phone,tozihat) values (N'" + txtusername.Text + "',N'" + txtpassword.Text + "',N'" + txtFullname.Text + "',N'" + txtphone.Text + "',N'" + txttozihat.Text + "')");
                    Payam.Show("کاربر با موفقیت در سیستم ثبت شد");
                    this.Close();
                }

            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");


        }

        private void Frm_add_user_Load(object sender, EventArgs e)
        {

        }
    }
}