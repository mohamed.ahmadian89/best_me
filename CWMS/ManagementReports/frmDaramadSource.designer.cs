﻿namespace CWMS.ManagementReports
{
    partial class frmDaramadSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label maliatLabel;
            System.Windows.Forms.Label hazmojazLabel;
            System.Windows.Forms.Label hazghmojazLabel;
            System.Windows.Forms.Label aboonmahLabel;
            System.Windows.Forms.Label fazelabLabel;
            System.Windows.Forms.Label sayerLabel;
            System.Windows.Forms.Label jarimehLabel;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label17;
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAllSharj = new DevComponents.DotNetBar.ButtonX();
            this.label10 = new System.Windows.Forms.Label();
            this.daramadSharjBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.daramadDataset = new CWMS.ManagementReports.DaramadDataset();
            this.btnPerDorehSharj = new DevComponents.DotNetBar.ButtonX();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDaramadSharj = new DevComponents.DotNetBar.ButtonX();
            this.cmbSharjTo = new System.Windows.Forms.ComboBox();
            this.cmbSharjFrom = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAllAb = new DevComponents.DotNetBar.ButtonX();
            this.maliatLabel1 = new System.Windows.Forms.Label();
            this.daramadAbBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnPerDorehAb = new DevComponents.DotNetBar.ButtonX();
            this.label13 = new System.Windows.Forms.Label();
            this.hazmojazLabel1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbAbTo = new System.Windows.Forms.ComboBox();
            this.cmbAbFrom = new System.Windows.Forms.ComboBox();
            this.hazghmojazLabel1 = new System.Windows.Forms.Label();
            this.btnDaramadAb = new DevComponents.DotNetBar.ButtonX();
            this.aboonmahLabel1 = new System.Windows.Forms.Label();
            this.fazelabLabel1 = new System.Windows.Forms.Label();
            this.sayerLabel1 = new System.Windows.Forms.Label();
            this.jarimehLabel1 = new System.Windows.Forms.Label();
            this.totalLabel1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.daramadSharjTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.DaramadSharjTableAdapter();
            this.daramadAbTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.DaramadAbTableAdapter();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            maliatLabel = new System.Windows.Forms.Label();
            hazmojazLabel = new System.Windows.Forms.Label();
            hazghmojazLabel = new System.Windows.Forms.Label();
            aboonmahLabel = new System.Windows.Forms.Label();
            fazelabLabel = new System.Windows.Forms.Label();
            sayerLabel = new System.Windows.Forms.Label();
            jarimehLabel = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daramadSharjBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daramadAbBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // maliatLabel
            // 
            maliatLabel.AutoSize = true;
            maliatLabel.ForeColor = System.Drawing.Color.Black;
            maliatLabel.Location = new System.Drawing.Point(266, 199);
            maliatLabel.Name = "maliatLabel";
            maliatLabel.Size = new System.Drawing.Size(103, 26);
            maliatLabel.TabIndex = 13;
            maliatLabel.Text = "مجموع مالیات :";
            // 
            // hazmojazLabel
            // 
            hazmojazLabel.AutoSize = true;
            hazmojazLabel.ForeColor = System.Drawing.Color.Black;
            hazmojazLabel.Location = new System.Drawing.Point(266, 131);
            hazmojazLabel.Name = "hazmojazLabel";
            hazmojazLabel.Size = new System.Drawing.Size(139, 26);
            hazmojazLabel.TabIndex = 15;
            hazmojazLabel.Text = "هزینه مصارف مجاز :";
            // 
            // hazghmojazLabel
            // 
            hazghmojazLabel.AutoSize = true;
            hazghmojazLabel.ForeColor = System.Drawing.Color.Black;
            hazghmojazLabel.Location = new System.Drawing.Point(266, 165);
            hazghmojazLabel.Name = "hazghmojazLabel";
            hazghmojazLabel.Size = new System.Drawing.Size(161, 26);
            hazghmojazLabel.TabIndex = 17;
            hazghmojazLabel.Text = "هزینه مصارف غیرمجاز :";
            // 
            // aboonmahLabel
            // 
            aboonmahLabel.AutoSize = true;
            aboonmahLabel.ForeColor = System.Drawing.Color.Black;
            aboonmahLabel.Location = new System.Drawing.Point(266, 233);
            aboonmahLabel.Name = "aboonmahLabel";
            aboonmahLabel.Size = new System.Drawing.Size(111, 26);
            aboonmahLabel.TabIndex = 19;
            aboonmahLabel.Text = "مجموع آبونمان :";
            // 
            // fazelabLabel
            // 
            fazelabLabel.AutoSize = true;
            fazelabLabel.ForeColor = System.Drawing.Color.Black;
            fazelabLabel.Location = new System.Drawing.Point(266, 267);
            fazelabLabel.Name = "fazelabLabel";
            fazelabLabel.Size = new System.Drawing.Size(140, 26);
            fazelabLabel.TabIndex = 21;
            fazelabLabel.Text = "هزینه های فاضلاب :";
            // 
            // sayerLabel
            // 
            sayerLabel.AutoSize = true;
            sayerLabel.ForeColor = System.Drawing.Color.Black;
            sayerLabel.Location = new System.Drawing.Point(266, 335);
            sayerLabel.Name = "sayerLabel";
            sayerLabel.Size = new System.Drawing.Size(95, 26);
            sayerLabel.TabIndex = 23;
            sayerLabel.Text = "مجموع سایر :";
            // 
            // jarimehLabel
            // 
            jarimehLabel.AutoSize = true;
            jarimehLabel.ForeColor = System.Drawing.Color.Black;
            jarimehLabel.Location = new System.Drawing.Point(266, 301);
            jarimehLabel.Name = "jarimehLabel";
            jarimehLabel.Size = new System.Drawing.Size(103, 26);
            jarimehLabel.TabIndex = 25;
            jarimehLabel.Text = "مجموع جرایم :";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.ForeColor = System.Drawing.Color.Black;
            label16.Location = new System.Drawing.Point(260, 335);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(95, 26);
            label16.TabIndex = 29;
            label16.Text = "مجموع سایر :";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.ForeColor = System.Drawing.Color.Black;
            label17.Location = new System.Drawing.Point(260, 301);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(103, 26);
            label17.TabIndex = 30;
            label17.Text = "مجموع جرایم :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(260, 131);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(130, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "مجموع مبالغ شارژ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(260, 199);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(103, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "مجموع مالیات :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(label16);
            this.groupBox1.Controls.Add(this.btnAllSharj);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.btnPerDorehSharj);
            this.groupBox1.Controls.Add(label17);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnDaramadSharj);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbSharjTo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbSharjFrom);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(448, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(430, 429);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "درآمدهای قبوض شارژ :";
            // 
            // btnAllSharj
            // 
            this.btnAllSharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAllSharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAllSharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAllSharj.Location = new System.Drawing.Point(40, 85);
            this.btnAllSharj.Name = "btnAllSharj";
            this.btnAllSharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnAllSharj.Size = new System.Drawing.Size(152, 32);
            this.btnAllSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAllSharj.Symbol = "";
            this.btnAllSharj.SymbolColor = System.Drawing.Color.Green;
            this.btnAllSharj.SymbolSize = 9F;
            this.btnAllSharj.TabIndex = 60;
            this.btnAllSharj.Text = "  همه دوره ها";
            this.btnAllSharj.Click += new System.EventHandler(this.btnAllSharj_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadSharjBindingSource, "total", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(13, 389);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 26);
            this.label10.TabIndex = 9;
            this.label10.Text = "0";
            // 
            // daramadSharjBindingSource
            // 
            this.daramadSharjBindingSource.DataMember = "DaramadSharj";
            this.daramadSharjBindingSource.DataSource = this.daramadDataset;
            // 
            // daramadDataset
            // 
            this.daramadDataset.DataSetName = "DaramadDataset";
            this.daramadDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnPerDorehSharj
            // 
            this.btnPerDorehSharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPerDorehSharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPerDorehSharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPerDorehSharj.Location = new System.Drawing.Point(231, 85);
            this.btnPerDorehSharj.Name = "btnPerDorehSharj";
            this.btnPerDorehSharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnPerDorehSharj.Size = new System.Drawing.Size(152, 32);
            this.btnPerDorehSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPerDorehSharj.Symbol = "";
            this.btnPerDorehSharj.SymbolColor = System.Drawing.Color.Green;
            this.btnPerDorehSharj.SymbolSize = 9F;
            this.btnPerDorehSharj.TabIndex = 59;
            this.btnPerDorehSharj.Text = " فقط همین دوره";
            this.btnPerDorehSharj.Click += new System.EventHandler(this.btnPerDorehSharj_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(260, 389);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(130, 26);
            this.label9.TabIndex = 8;
            this.label9.Text = "جمع کل درآمد ها :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadSharjBindingSource, "mablagh", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(13, 131);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(154, 48);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(64, 26);
            this.label14.TabIndex = 56;
            this.label14.Text = "تا دوره :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadSharjBindingSource, "maliat", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(13, 199);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(307, 48);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(66, 26);
            this.label15.TabIndex = 55;
            this.label15.Text = "از دوره :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadSharjBindingSource, "jarimeh", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(13, 335);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 26);
            this.label7.TabIndex = 7;
            this.label7.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadSharjBindingSource, "sayer", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(13, 301);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 26);
            this.label8.TabIndex = 6;
            this.label8.Text = "0";
            // 
            // btnDaramadSharj
            // 
            this.btnDaramadSharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDaramadSharj.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDaramadSharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDaramadSharj.Location = new System.Drawing.Point(40, 46);
            this.btnDaramadSharj.Name = "btnDaramadSharj";
            this.btnDaramadSharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnDaramadSharj.Size = new System.Drawing.Size(32, 32);
            this.btnDaramadSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDaramadSharj.Symbol = "";
            this.btnDaramadSharj.SymbolColor = System.Drawing.Color.Green;
            this.btnDaramadSharj.SymbolSize = 9F;
            this.btnDaramadSharj.TabIndex = 53;
            this.btnDaramadSharj.Click += new System.EventHandler(this.btnDaramadSharj_Click);
            // 
            // cmbSharjTo
            // 
            this.cmbSharjTo.BackColor = System.Drawing.Color.White;
            this.cmbSharjTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSharjTo.ForeColor = System.Drawing.Color.Black;
            this.cmbSharjTo.FormattingEnabled = true;
            this.cmbSharjTo.Location = new System.Drawing.Point(78, 45);
            this.cmbSharjTo.Name = "cmbSharjTo";
            this.cmbSharjTo.Size = new System.Drawing.Size(69, 34);
            this.cmbSharjTo.TabIndex = 52;
            // 
            // cmbSharjFrom
            // 
            this.cmbSharjFrom.BackColor = System.Drawing.Color.White;
            this.cmbSharjFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSharjFrom.ForeColor = System.Drawing.Color.Black;
            this.cmbSharjFrom.FormattingEnabled = true;
            this.cmbSharjFrom.Location = new System.Drawing.Point(231, 45);
            this.cmbSharjFrom.Name = "cmbSharjFrom";
            this.cmbSharjFrom.Size = new System.Drawing.Size(69, 34);
            this.cmbSharjFrom.TabIndex = 51;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(maliatLabel);
            this.groupBox2.Controls.Add(this.btnAllAb);
            this.groupBox2.Controls.Add(this.maliatLabel1);
            this.groupBox2.Controls.Add(this.btnPerDorehAb);
            this.groupBox2.Controls.Add(hazmojazLabel);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.hazmojazLabel1);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cmbAbTo);
            this.groupBox2.Controls.Add(hazghmojazLabel);
            this.groupBox2.Controls.Add(this.cmbAbFrom);
            this.groupBox2.Controls.Add(this.hazghmojazLabel1);
            this.groupBox2.Controls.Add(this.btnDaramadAb);
            this.groupBox2.Controls.Add(aboonmahLabel);
            this.groupBox2.Controls.Add(this.aboonmahLabel1);
            this.groupBox2.Controls.Add(fazelabLabel);
            this.groupBox2.Controls.Add(this.fazelabLabel1);
            this.groupBox2.Controls.Add(sayerLabel);
            this.groupBox2.Controls.Add(this.sayerLabel1);
            this.groupBox2.Controls.Add(jarimehLabel);
            this.groupBox2.Controls.Add(this.jarimehLabel1);
            this.groupBox2.Controls.Add(this.totalLabel1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(430, 429);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "درآمدهای قبوض آب :";
            // 
            // btnAllAb
            // 
            this.btnAllAb.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAllAb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAllAb.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAllAb.Location = new System.Drawing.Point(44, 82);
            this.btnAllAb.Name = "btnAllAb";
            this.btnAllAb.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnAllAb.Size = new System.Drawing.Size(152, 32);
            this.btnAllAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAllAb.Symbol = "";
            this.btnAllAb.SymbolColor = System.Drawing.Color.Green;
            this.btnAllAb.SymbolSize = 9F;
            this.btnAllAb.TabIndex = 58;
            this.btnAllAb.Text = "  همه دوره ها";
            this.btnAllAb.Click += new System.EventHandler(this.btnAllAb_Click);
            // 
            // maliatLabel1
            // 
            this.maliatLabel1.AutoSize = true;
            this.maliatLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "maliat", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.maliatLabel1.ForeColor = System.Drawing.Color.Black;
            this.maliatLabel1.Location = new System.Drawing.Point(19, 199);
            this.maliatLabel1.Name = "maliatLabel1";
            this.maliatLabel1.Size = new System.Drawing.Size(22, 26);
            this.maliatLabel1.TabIndex = 14;
            this.maliatLabel1.Text = "0";
            // 
            // daramadAbBindingSource
            // 
            this.daramadAbBindingSource.DataMember = "DaramadAb";
            this.daramadAbBindingSource.DataSource = this.daramadDataset;
            // 
            // btnPerDorehAb
            // 
            this.btnPerDorehAb.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPerDorehAb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPerDorehAb.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPerDorehAb.Location = new System.Drawing.Point(235, 82);
            this.btnPerDorehAb.Name = "btnPerDorehAb";
            this.btnPerDorehAb.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnPerDorehAb.Size = new System.Drawing.Size(152, 32);
            this.btnPerDorehAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPerDorehAb.Symbol = "";
            this.btnPerDorehAb.SymbolColor = System.Drawing.Color.Green;
            this.btnPerDorehAb.SymbolSize = 9F;
            this.btnPerDorehAb.TabIndex = 57;
            this.btnPerDorehAb.Text = " فقط همین دوره";
            this.btnPerDorehAb.Click += new System.EventHandler(this.btnPerDorehAb_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(158, 45);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(64, 26);
            this.label13.TabIndex = 54;
            this.label13.Text = "تا دوره :";
            // 
            // hazmojazLabel1
            // 
            this.hazmojazLabel1.AutoSize = true;
            this.hazmojazLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "hazmojaz", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.hazmojazLabel1.ForeColor = System.Drawing.Color.Black;
            this.hazmojazLabel1.Location = new System.Drawing.Point(19, 131);
            this.hazmojazLabel1.Name = "hazmojazLabel1";
            this.hazmojazLabel1.Size = new System.Drawing.Size(22, 26);
            this.hazmojazLabel1.TabIndex = 16;
            this.hazmojazLabel1.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(311, 45);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(66, 26);
            this.label12.TabIndex = 10;
            this.label12.Text = "از دوره :";
            // 
            // cmbAbTo
            // 
            this.cmbAbTo.BackColor = System.Drawing.Color.White;
            this.cmbAbTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAbTo.ForeColor = System.Drawing.Color.Black;
            this.cmbAbTo.FormattingEnabled = true;
            this.cmbAbTo.Location = new System.Drawing.Point(82, 42);
            this.cmbAbTo.Name = "cmbAbTo";
            this.cmbAbTo.Size = new System.Drawing.Size(69, 34);
            this.cmbAbTo.TabIndex = 50;
            // 
            // cmbAbFrom
            // 
            this.cmbAbFrom.BackColor = System.Drawing.Color.White;
            this.cmbAbFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAbFrom.ForeColor = System.Drawing.Color.Black;
            this.cmbAbFrom.FormattingEnabled = true;
            this.cmbAbFrom.Location = new System.Drawing.Point(235, 42);
            this.cmbAbFrom.Name = "cmbAbFrom";
            this.cmbAbFrom.Size = new System.Drawing.Size(69, 34);
            this.cmbAbFrom.TabIndex = 49;
            // 
            // hazghmojazLabel1
            // 
            this.hazghmojazLabel1.AutoSize = true;
            this.hazghmojazLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "hazghmojaz", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.hazghmojazLabel1.ForeColor = System.Drawing.Color.Black;
            this.hazghmojazLabel1.Location = new System.Drawing.Point(19, 165);
            this.hazghmojazLabel1.Name = "hazghmojazLabel1";
            this.hazghmojazLabel1.Size = new System.Drawing.Size(22, 26);
            this.hazghmojazLabel1.TabIndex = 18;
            this.hazghmojazLabel1.Text = "0";
            // 
            // btnDaramadAb
            // 
            this.btnDaramadAb.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDaramadAb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDaramadAb.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDaramadAb.Location = new System.Drawing.Point(44, 43);
            this.btnDaramadAb.Name = "btnDaramadAb";
            this.btnDaramadAb.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnDaramadAb.Size = new System.Drawing.Size(32, 32);
            this.btnDaramadAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDaramadAb.Symbol = "";
            this.btnDaramadAb.SymbolColor = System.Drawing.Color.Green;
            this.btnDaramadAb.SymbolSize = 9F;
            this.btnDaramadAb.TabIndex = 46;
            this.btnDaramadAb.Click += new System.EventHandler(this.btnDaramadAb_Click);
            // 
            // aboonmahLabel1
            // 
            this.aboonmahLabel1.AutoSize = true;
            this.aboonmahLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "aboonmah", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.aboonmahLabel1.ForeColor = System.Drawing.Color.Black;
            this.aboonmahLabel1.Location = new System.Drawing.Point(19, 233);
            this.aboonmahLabel1.Name = "aboonmahLabel1";
            this.aboonmahLabel1.Size = new System.Drawing.Size(22, 26);
            this.aboonmahLabel1.TabIndex = 20;
            this.aboonmahLabel1.Text = "0";
            // 
            // fazelabLabel1
            // 
            this.fazelabLabel1.AutoSize = true;
            this.fazelabLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "fazelab", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.fazelabLabel1.ForeColor = System.Drawing.Color.Black;
            this.fazelabLabel1.Location = new System.Drawing.Point(19, 267);
            this.fazelabLabel1.Name = "fazelabLabel1";
            this.fazelabLabel1.Size = new System.Drawing.Size(22, 26);
            this.fazelabLabel1.TabIndex = 22;
            this.fazelabLabel1.Text = "0";
            // 
            // sayerLabel1
            // 
            this.sayerLabel1.AutoSize = true;
            this.sayerLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "sayer", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.sayerLabel1.ForeColor = System.Drawing.Color.Black;
            this.sayerLabel1.Location = new System.Drawing.Point(19, 335);
            this.sayerLabel1.Name = "sayerLabel1";
            this.sayerLabel1.Size = new System.Drawing.Size(22, 26);
            this.sayerLabel1.TabIndex = 24;
            this.sayerLabel1.Text = "0";
            // 
            // jarimehLabel1
            // 
            this.jarimehLabel1.AutoSize = true;
            this.jarimehLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "jarimeh", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.jarimehLabel1.ForeColor = System.Drawing.Color.Black;
            this.jarimehLabel1.Location = new System.Drawing.Point(19, 301);
            this.jarimehLabel1.Name = "jarimehLabel1";
            this.jarimehLabel1.Size = new System.Drawing.Size(22, 26);
            this.jarimehLabel1.TabIndex = 26;
            this.jarimehLabel1.Text = "0";
            // 
            // totalLabel1
            // 
            this.totalLabel1.AutoSize = true;
            this.totalLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.daramadAbBindingSource, "total", true,
                System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "#,#", System.Globalization.CultureInfo.InvariantCulture));
            this.totalLabel1.ForeColor = System.Drawing.Color.Black;
            this.totalLabel1.Location = new System.Drawing.Point(19, 389);
            this.totalLabel1.Name = "totalLabel1";
            this.totalLabel1.Size = new System.Drawing.Size(22, 26);
            this.totalLabel1.TabIndex = 28;
            this.totalLabel1.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(266, 389);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(130, 26);
            this.label11.TabIndex = 13;
            this.label11.Text = "جمع کل درآمد ها :";
            // 
            // daramadSharjTableAdapter
            // 
            this.daramadSharjTableAdapter.ClearBeforeFill = true;
            // 
            // daramadAbTableAdapter
            // 
            this.daramadAbTableAdapter.ClearBeforeFill = true;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(12, 447);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(152, 32);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 61;
            this.buttonX1.Text = "چاپ گزارش";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // frmDaramadSource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 482);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDaramadSource";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmDaramadSource_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daramadSharjBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daramadAbBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.BindingSource daramadSharjBindingSource;
        private DaramadDataset daramadDataset;
        private System.Windows.Forms.Label label9;
        private DaramadDatasetTableAdapters.DaramadSharjTableAdapter daramadSharjTableAdapter;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.BindingSource daramadAbBindingSource;
        private DaramadDatasetTableAdapters.DaramadAbTableAdapter daramadAbTableAdapter;
        private System.Windows.Forms.Label maliatLabel1;
        private System.Windows.Forms.Label hazmojazLabel1;
        private System.Windows.Forms.Label hazghmojazLabel1;
        private System.Windows.Forms.Label aboonmahLabel1;
        private System.Windows.Forms.Label fazelabLabel1;
        private System.Windows.Forms.Label sayerLabel1;
        private System.Windows.Forms.Label jarimehLabel1;
        private System.Windows.Forms.Label totalLabel1;
        private System.Windows.Forms.ComboBox cmbAbTo;
        private System.Windows.Forms.ComboBox cmbAbFrom;
        private DevComponents.DotNetBar.ButtonX btnDaramadAb;
        private System.Windows.Forms.ComboBox cmbSharjTo;
        private System.Windows.Forms.ComboBox cmbSharjFrom;
        private DevComponents.DotNetBar.ButtonX btnDaramadSharj;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.ButtonX btnPerDorehAb;
        private DevComponents.DotNetBar.ButtonX btnAllAb;
        private DevComponents.DotNetBar.ButtonX btnAllSharj;
        private DevComponents.DotNetBar.ButtonX btnPerDorehSharj;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}