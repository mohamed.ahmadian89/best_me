﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.ManagementReports
{
    public partial class Frm_Arzesh_afzoodeh : Form
    {
        public Frm_Arzesh_afzoodeh()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }
        decimal TotalmamjmuMablagh = 0, TotalmajmuMaliat = 0, TotalBedehi = 0, TotalKasrHezar = 0, TotalmajmuAvazrez = 0;
        List<string[]> lst_ab_errors, lst_sharj_errors;

        DaramadDataset.sharj_maliat_reportDataTable sharj_maliat_dt,ab_maliat_dt;
        

        private void Frm_Arzesh_afzoodeh_Load(object sender, EventArgs e)
        {
            txt_start.SelectedDateTime = txt_end.SelectedDateTime = DateTime.Now;
            lst_ab_errors = new List<string[]>();
            lst_sharj_errors = new List<string[]>();
            sharj_maliat_dt = new DaramadDataset.sharj_maliat_reportDataTable();
            ab_maliat_dt = new DaramadDataset.sharj_maliat_reportDataTable();

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            new Management.Frm_ArzeshAfzzoddeh_Problem(lst_sharj_errors).ShowDialog();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            new Management.Frm_ArzeshAfzzoddeh_Problem(lst_ab_errors).ShowDialog();

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            dgv_ab_result.Rows.Clear();

            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);

            ab_ghabzTableAdapter.Fill(daramadDataset.ab_ghabz, start, finish);
            //sharj_Pardakht_reportTableAdapter.FillByTarikh(daramadDataset.Sharj_Pardakht_report, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime));
            lst_ab_errors.Clear();

            Ab_part();
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            Classes.clsTax.print_maliat(ab_maliat_dt,"قبوض آب");

        }

        private void copyAlltoClipboard_sharj()
        {
            DgvResult.SelectAll();
            DataObject dataObj = DgvResult.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void copyAlltoClipboard_ab()
        {
            dgv_ab_result.SelectAll();
            DataObject dataObj = dgv_ab_result.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard_sharj();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard_ab();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            Classes.clsTax.print_maliat(sharj_maliat_dt,"قبوض شارژ");
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            DgvResult.Rows.Clear();
            Classes.ClsMain.ChangeCulture("e");
            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);
       

            sharj_ghabz_PardakhtTableAdapter.Fill(daramadDataset.sharj_ghabz_Pardakht, start, finish);
            //sharj_Pardakht_reportTableAdapter.FillByTarikh(daramadDataset.Sharj_Pardakht_report, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime));
            lst_sharj_errors.Clear();

            Sharj_part();
            Classes.ClsMain.ChangeCulture("f");

        }

        void CalcBedehiDetails(int dcode, int gharardad, ref bool status, ref decimal majuMablaghm, ref decimal majmumaliat, ref decimal majmukasr, ref decimal majmuAvazrez, bool Is_ab )
        {
            if (dcode == 0)
            {
                return;
            }
            string table_name = "sharj_ghabz";
            if (Is_ab)
                table_name = "ab_ghabz";

            System.Data.DataTable dtDoreh = Classes.ClsMain.GetDataTable("select * from " + table_name + " where dcode=" + dcode + " and gharardad=" + gharardad);
            if (dtDoreh.Rows.Count == 0)
            {
                status = false;
                return;
            }
            //else if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) == 0)
            //{
            //    //jarimeh + sayer + mablagh AS 'مجموع مبالغ', 

            //    if (Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) != Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) + Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]))
            //    {
            //        majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
            //            + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
            //        majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
            //        majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
            //    }
            //}
            //else
            //    CalcBedehiDetailsOfsharj(dcode - 1, gharardad, ref majuMablaghm, ref majmumaliat, ref majmukasr);



            // ---------------------- version 2

            //if (Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) != Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) + Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]))
            //{
            //    majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
            //        + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
            //    majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
            //    majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
            //}

            //if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
            //{

            //    CalcBedehiDetailsOfsharj(dcode - 1, gharardad, ref majuMablaghm, ref majmumaliat, ref majmukasr);
            //}

            // --------------------- version 3
            // اگر قبض به طور کامل پرداخت نشده باشد 
            if (Convert.ToInt32(dtDoreh.Rows[0]["vaziat"]) == 0)
            {
                decimal Current_mablagh = 0;

                Current_mablagh = Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
                + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
                if (Is_ab)
                    Current_mablagh += Convert.ToDecimal(dtDoreh.Rows[0]["aboonmah"]);

                decimal Current_maliat = Math.Round(Current_mablagh * Convert.ToDecimal(txt_darsad_maliat.Text) / 100);
                decimal Current_Avazrez = Math.Round(Current_mablagh * Convert.ToDecimal(txt_darsad_avarez.Text) / 100);

                //if (Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) - Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) < 1000)
                //{
                majuMablaghm += Current_mablagh;
                majmumaliat += Current_maliat;
                //majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
                majmuAvazrez += Current_Avazrez;
                if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
                {
                    CalcBedehiDetails(dcode - 1, gharardad, ref status, ref majuMablaghm, ref majmumaliat, ref majmukasr, ref majmuAvazrez,Is_ab);
                }
            }
            // در این حالت مطمئنا کاربر قبض خود را به طور جزئی پرداخت کرده است .
            // به هیچ وجه امکان نخواهد داشت که وضعیت قبض یک باشد
            // چون در این صورت بدهی نباید به دوره بعد منتقل می شد که این خلاف الگوریتم است 
            else
                status = false;

        }

        void Sharj_part()
        {
            bool SharjStatus = true;
            sharj_maliat_dt.Rows.Clear();

            List<object> lst_errors = new List<object>();

            for (int i = 0; i < daramadDataset.sharj_ghabz_Pardakht.Rows.Count; i++)
            {
                decimal mamjmuMablagh = 0, majmuMaliat = 0, majmuKasr = 0, majmuAvarez = 0;
                SharjStatus = true;
                if (Convert.ToDecimal(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]) != 0
                    || Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"])==1)
                {
                    mamjmuMablagh = 0;
                    majmuKasr = 0;
                    majmuMaliat = 0;
                    majmuAvarez = 0;

                    CalcBedehiDetails
                        (
                        Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]) - 1,
                        Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]),
                        ref SharjStatus,
                        ref mamjmuMablagh,
                        ref majmuMaliat,
                        ref majmuKasr,
                        ref majmuAvarez,false
                        );



                    if (SharjStatus == true)
                    {
                        decimal mablagh = Convert.ToDecimal(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["مجموع مبالغ"]);

                        decimal maliat = Math.Round(mablagh * Convert.ToDecimal(txt_darsad_maliat.Text) / 100);
                        decimal avarez = Math.Round(mablagh * Convert.ToDecimal(txt_darsad_avarez.Text) / 100);

                        DaramadDataset.sharj_maliat_reportRow sharj_row = sharj_maliat_dt.Newsharj_maliat_reportRow();
                        sharj_row.dcode = Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]);
                        sharj_row.gharardad = Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]);
                        sharj_row.tarikh = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"])).ToString("d");
                        sharj_row.majmu = mablagh;
                        sharj_row.maliat = maliat;
                        sharj_row.avarez = avarez;
                        sharj_row.majmu_maliat = maliat + avarez;
                        sharj_row.mablagh_kol = mablagh + maliat + avarez;

                        sharj_row.majmu_bedehi = mamjmuMablagh;
                        sharj_row.maliat_bedehi = majmuMaliat;
                        sharj_row.maliat_avarez = majmuAvarez;
                        sharj_row.majmu_maliat_bedehi = majmuMaliat + majmuAvarez;
                        sharj_row.mablagh_kol_bedehi = mamjmuMablagh + majmuMaliat + majmuAvarez;
                        sharj_row.jam_maliat_avarez = sharj_row.majmu_maliat_bedehi + sharj_row.majmu_maliat;
                        sharj_row.jam_mabalegh_kol = sharj_row.mablagh_kol_bedehi + sharj_row.mablagh_kol;
                        sharj_row.pardakhti = Convert.ToDecimal(Classes.ClsMain.ExecuteScalar("select isnull(sum(mablagh),0) from ghabz_pardakht where vaziat=1 and  gcode=" + daramadDataset.sharj_ghabz_Pardakht.Rows[i]["کد قبض"]));
                        sharj_maliat_dt.Rows.Add(sharj_row);
                        DgvResult.Rows.Add(
                            sharj_row.dcode
                            , sharj_row.gharardad
                            , sharj_row.tarikh
                            , sharj_row.majmu
                            , sharj_row.maliat
                            , sharj_row.avarez
                            , sharj_row.majmu_maliat
                            , sharj_row.mablagh_kol

                            , sharj_row.majmu_bedehi
                            , sharj_row.maliat_bedehi
                            , sharj_row.maliat_avarez
                            , sharj_row.majmu_maliat_bedehi
                            , sharj_row.mablagh_kol_bedehi


                            , sharj_row.jam_maliat_avarez
                            , sharj_row.jam_mabalegh_kol
                            ,sharj_row.pardakhti
                            );


                   


                        //TotalmamjmuMablagh += mamjmuMablagh;
                        //TotalBedehi += Convert.ToDecimal(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]);
                        //TotalmajmuMaliat += majmuMaliat;
                        //TotalKasrHezar += majmuKasr;
                    }
                    else
                    {
                        string[] info = new string[3];
                        info[0] = daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"].ToString();
                        info[1] = daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"].ToString();
                        info[2] = daramadDataset.sharj_ghabz_Pardakht.Rows[i]["کد قبض"].ToString();
                        lst_sharj_errors.Add(info);
                    }
                  
                }
            }


        }


        void Ab_part()
        {
            bool Ab_Status = true;
            ab_maliat_dt.Rows.Clear();
            
            for (int i = 0; i < daramadDataset.ab_ghabz.Rows.Count; i++)
            {
                decimal mamjmuMablagh = 0, majmuMaliat = 0, majmuKasr = 0, majmuAvarez = 0;
                Ab_Status = true;
                if (Convert.ToDecimal(daramadDataset.ab_ghabz.Rows[i]["بدهی"]) != 0 
                    || Convert.ToInt32(daramadDataset.ab_ghabz.Rows[i]["دوره"])==1)
                {
                    mamjmuMablagh = 0;
                    majmuKasr = 0;
                    majmuMaliat = 0;
                    majmuAvarez = 0;

                    CalcBedehiDetails
                        (
                        Convert.ToInt32(daramadDataset.ab_ghabz.Rows[i]["دوره"]) - 1,
                        Convert.ToInt32(daramadDataset.ab_ghabz.Rows[i]["قرارداد"]),
                        ref Ab_Status,
                        ref mamjmuMablagh,
                        ref majmuMaliat,
                        ref majmuKasr,
                        ref majmuAvarez
                        , true
                        );



                    if (Ab_Status == true)
                    {
                        decimal mablagh = Convert.ToDecimal(daramadDataset.ab_ghabz.Rows[i]["مجموع مبالغ"]);

                        decimal maliat = Math.Round(mablagh * Convert.ToDecimal(txt_darsad_maliat.Text) / 100);
                        decimal avarez = Math.Round(mablagh * Convert.ToDecimal(txt_darsad_avarez.Text) / 100);


                        DaramadDataset.sharj_maliat_reportRow ab_row = ab_maliat_dt.Newsharj_maliat_reportRow();
                        ab_row.dcode = Convert.ToInt32(daramadDataset.ab_ghabz.Rows[i]["دوره"]);
                        ab_row.gharardad = Convert.ToInt32(daramadDataset.ab_ghabz.Rows[i]["قرارداد"]);
                        ab_row.tarikh = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(daramadDataset.ab_ghabz.Rows[i]["آخرین پرداخت"])).ToString("d");
                        ab_row.majmu = mablagh;
                        ab_row.maliat = maliat;
                        ab_row.avarez = avarez;
                        ab_row.majmu_maliat = maliat + avarez;
                        ab_row.mablagh_kol = mablagh + maliat + avarez;

                        ab_row.majmu_bedehi = mamjmuMablagh;
                        ab_row.maliat_bedehi = majmuMaliat;
                        ab_row.maliat_avarez = majmuAvarez;
                        ab_row.majmu_maliat_bedehi = majmuMaliat + majmuAvarez;
                        ab_row.mablagh_kol_bedehi = mamjmuMablagh + majmuMaliat + majmuAvarez;
                 

                        ab_row.jam_maliat_avarez = ab_row.majmu_maliat_bedehi + ab_row.majmu_maliat;
                        ab_row.jam_mabalegh_kol = ab_row.mablagh_kol_bedehi + ab_row.mablagh_kol;
                        ab_row.pardakhti = Convert.ToDecimal(Classes.ClsMain.ExecuteScalar("select isnull(sum(mablagh),0) from ghabz_pardakht where vaziat=1 and  gcode=" + daramadDataset.ab_ghabz.Rows[i]["کد قبض"]));

                        ab_maliat_dt.Rows.Add(ab_row);
                        dgv_ab_result.Rows.Add(
                            ab_row.dcode
                            , ab_row.gharardad
                            , ab_row.tarikh
                            , ab_row.majmu
                            , ab_row.maliat
                            , ab_row.avarez
                            , ab_row.majmu_maliat
                            , ab_row.mablagh_kol

                            , ab_row.majmu_bedehi
                            , ab_row.maliat_bedehi
                            , ab_row.maliat_avarez
                            , ab_row.majmu_maliat_bedehi
                            , ab_row.mablagh_kol_bedehi


                            , ab_row.jam_maliat_avarez
                            , ab_row.jam_mabalegh_kol
                            , ab_row.pardakhti
                            );


                    }
                    else
                    {
                        string[] info = new string[3];
                        info[0] = daramadDataset.ab_ghabz.Rows[i]["قرارداد"].ToString();
                        info[1] = daramadDataset.ab_ghabz.Rows[i]["دوره"].ToString();
                        info[2] = daramadDataset.ab_ghabz.Rows[i]["کد قبض"].ToString();

                        lst_ab_errors.Add(info);
                    }
                    //lst_errors.Add(
                    //   daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]
                    //   , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]
                    //   , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]
                    //   , mamjmuMablagh
                    //   , majmuMaliat
                    //   , majmuKasr
                    //   , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"]
                    //   );
                }
            }


        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }
    }
}
