﻿namespace CWMS.ManagementReports
{
    partial class FrmGhabzPardakhtRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.BtnFindByTarikh = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.superTabControl2 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvSharj = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.کدقبضDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.قراردادDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.دورهDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.سایرجریمهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مبلغشارژDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مالیاتDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بدهیDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مبلغکلDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.پرداختیDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.آخرینپرداختDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.sharjghabzPardakhtBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.daramadDataset = new CWMS.ManagementReports.DaramadDataset();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.TabSharj = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.dgvAb = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.کدقبضDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.قراردادDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.دورهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.آبونمانDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.سایرجریمهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.آببهاDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مالیاتDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.فاضلاب = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بدهیDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مبلغکلDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.پرداختیDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.آخرینپرداختDataGridViewTextBoxColumn1 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.abghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.daramadDataset1 = new CWMS.ManagementReports.DaramadDataset();
            this.TabAb = new DevComponents.DotNetBar.SuperTabItem();
            this.کدقبضDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.قراردادDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.دورهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کدقبضDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بدهی = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end_doreh = new System.Windows.Forms.TextBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_start_doreh = new System.Windows.Forms.TextBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.sharj_ghabz_PardakhtTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter();
            this.ab_ghabzTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.ab_ghabzTableAdapter();
            this.groupPanel4.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).BeginInit();
            this.superTabControl2.SuspendLayout();
            this.superTabControlPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSharj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzPardakhtBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).BeginInit();
            this.groupPanel3.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset1)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel4.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.buttonX5);
            this.groupPanel4.Controls.Add(this.txt_end);
            this.groupPanel4.Controls.Add(this.labelX5);
            this.groupPanel4.Controls.Add(this.txt_start);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.BtnFindByTarikh);
            this.groupPanel4.Controls.Add(this.labelX1);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(13, 15);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(740, 82);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 67;
            this.groupPanel4.Text = "بر اساس تاریخ پرداخت قبوض";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(12, 13);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX5.Size = new System.Drawing.Size(204, 25);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Green;
            this.buttonX5.SymbolSize = 9F;
            this.buttonX5.TabIndex = 89;
            this.buttonX5.Text = "چاپ پرداختی های قبوض اب و شارژ";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(348, 15);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(107, 20);
            this.txt_end.TabIndex = 71;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(444, 9);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(89, 32);
            this.labelX5.TabIndex = 72;
            this.labelX5.Text = "تا تاریخ پرداخت :";
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(542, 15);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(107, 20);
            this.txt_start.TabIndex = 69;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(1060, 16);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(89, 32);
            this.labelX9.TabIndex = 70;
            this.labelX9.Text = "از تاریخ :";
            // 
            // BtnFindByTarikh
            // 
            this.BtnFindByTarikh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnFindByTarikh.BackColor = System.Drawing.Color.Transparent;
            this.BtnFindByTarikh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnFindByTarikh.Location = new System.Drawing.Point(235, 12);
            this.BtnFindByTarikh.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.BtnFindByTarikh.Name = "BtnFindByTarikh";
            this.BtnFindByTarikh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.BtnFindByTarikh.Size = new System.Drawing.Size(93, 27);
            this.BtnFindByTarikh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnFindByTarikh.Symbol = "";
            this.BtnFindByTarikh.SymbolColor = System.Drawing.Color.Green;
            this.BtnFindByTarikh.SymbolSize = 9F;
            this.BtnFindByTarikh.TabIndex = 40;
            this.BtnFindByTarikh.Text = "نمایش اطلاعات ";
            this.BtnFindByTarikh.Click += new System.EventHandler(this.BtnFindByTarikh_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(635, 9);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(89, 32);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "از تاریخ پرداخت  :";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.superTabControl2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(13, 103);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1138, 439);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 74;
            this.groupPanel1.Text = "بر اساس تاریخ پرداخت قبوض";
            // 
            // superTabControl2
            // 
            this.superTabControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl2.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl2.ControlBox.MenuBox.Name = "";
            this.superTabControl2.ControlBox.Name = "";
            this.superTabControl2.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl2.ControlBox.CloseBox,
            this.superTabControl2.ControlBox.MenuBox});
            this.superTabControl2.Controls.Add(this.superTabControlPanel4);
            this.superTabControl2.Controls.Add(this.superTabControlPanel5);
            this.superTabControl2.ForeColor = System.Drawing.Color.Black;
            this.superTabControl2.Location = new System.Drawing.Point(4, 14);
            this.superTabControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControl2.Name = "superTabControl2";
            this.superTabControl2.ReorderTabsEnabled = true;
            this.superTabControl2.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl2.SelectedTabIndex = 1;
            this.superTabControl2.Size = new System.Drawing.Size(1111, 395);
            this.superTabControl2.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl2.TabIndex = 71;
            this.superTabControl2.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabAb,
            this.TabSharj});
            this.superTabControl2.Text = "superTabControl2";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Controls.Add(this.dgvSharj);
            this.superTabControlPanel5.Controls.Add(this.groupPanel3);
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(1111, 368);
            this.superTabControlPanel5.TabIndex = 0;
            this.superTabControlPanel5.TabItem = this.TabSharj;
            // 
            // dgvSharj
            // 
            this.dgvSharj.AllowUserToAddRows = false;
            this.dgvSharj.AllowUserToDeleteRows = false;
            this.dgvSharj.AllowUserToResizeColumns = false;
            this.dgvSharj.AllowUserToResizeRows = false;
            this.dgvSharj.AutoGenerateColumns = false;
            this.dgvSharj.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSharj.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSharj.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.dgvSharj.ColumnHeadersHeight = 28;
            this.dgvSharj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.کدقبضDataGridViewTextBoxColumn2,
            this.قراردادDataGridViewTextBoxColumn2,
            this.دورهDataGridViewTextBoxColumn2,
            this.سایرجریمهDataGridViewTextBoxColumn,
            this.مبلغشارژDataGridViewTextBoxColumn,
            this.مجموعمبالغDataGridViewTextBoxColumn,
            this.مالیاتDataGridViewTextBoxColumn,
            this.بدهیDataGridViewTextBoxColumn,
            this.مبلغکلDataGridViewTextBoxColumn,
            this.پرداختیDataGridViewTextBoxColumn,
            this.ماندهDataGridViewTextBoxColumn,
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn,
            this.آخرینپرداختDataGridViewTextBoxColumn});
            this.dgvSharj.DataSource = this.sharjghabzPardakhtBindingSource;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle49.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSharj.DefaultCellStyle = dataGridViewCellStyle49;
            this.dgvSharj.EnableHeadersVisualStyles = false;
            this.dgvSharj.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvSharj.Location = new System.Drawing.Point(24, 9);
            this.dgvSharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvSharj.Name = "dgvSharj";
            this.dgvSharj.ReadOnly = true;
            this.dgvSharj.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle50.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSharj.RowHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this.dgvSharj.RowHeadersVisible = false;
            this.dgvSharj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSharj.Size = new System.Drawing.Size(1065, 300);
            this.dgvSharj.TabIndex = 75;
            // 
            // کدقبضDataGridViewTextBoxColumn2
            // 
            this.کدقبضDataGridViewTextBoxColumn2.DataPropertyName = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn2.HeaderText = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn2.Name = "کدقبضDataGridViewTextBoxColumn2";
            this.کدقبضDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // قراردادDataGridViewTextBoxColumn2
            // 
            this.قراردادDataGridViewTextBoxColumn2.DataPropertyName = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn2.HeaderText = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn2.Name = "قراردادDataGridViewTextBoxColumn2";
            this.قراردادDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // دورهDataGridViewTextBoxColumn2
            // 
            this.دورهDataGridViewTextBoxColumn2.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn2.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn2.Name = "دورهDataGridViewTextBoxColumn2";
            this.دورهDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // سایرجریمهDataGridViewTextBoxColumn
            // 
            this.سایرجریمهDataGridViewTextBoxColumn.DataPropertyName = "سایر-جریمه";
            dataGridViewCellStyle40.Format = "0,0";
            this.سایرجریمهDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this.سایرجریمهDataGridViewTextBoxColumn.HeaderText = "سایر-جریمه";
            this.سایرجریمهDataGridViewTextBoxColumn.Name = "سایرجریمهDataGridViewTextBoxColumn";
            this.سایرجریمهDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مبلغشارژDataGridViewTextBoxColumn
            // 
            this.مبلغشارژDataGridViewTextBoxColumn.DataPropertyName = "مبلغ شارژ";
            dataGridViewCellStyle41.Format = "0,0";
            this.مبلغشارژDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this.مبلغشارژDataGridViewTextBoxColumn.HeaderText = "مبلغ شارژ";
            this.مبلغشارژDataGridViewTextBoxColumn.Name = "مبلغشارژDataGridViewTextBoxColumn";
            this.مبلغشارژDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مجموعمبالغDataGridViewTextBoxColumn
            // 
            this.مجموعمبالغDataGridViewTextBoxColumn.DataPropertyName = "مجموع مبالغ";
            dataGridViewCellStyle42.Format = "0,0";
            this.مجموعمبالغDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this.مجموعمبالغDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ";
            this.مجموعمبالغDataGridViewTextBoxColumn.Name = "مجموعمبالغDataGridViewTextBoxColumn";
            this.مجموعمبالغDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مالیاتDataGridViewTextBoxColumn
            // 
            this.مالیاتDataGridViewTextBoxColumn.DataPropertyName = "مالیات";
            dataGridViewCellStyle43.Format = "0,0";
            this.مالیاتDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this.مالیاتDataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.مالیاتDataGridViewTextBoxColumn.Name = "مالیاتDataGridViewTextBoxColumn";
            this.مالیاتDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بدهیDataGridViewTextBoxColumn
            // 
            this.بدهیDataGridViewTextBoxColumn.DataPropertyName = "بدهی";
            dataGridViewCellStyle44.Format = "0,0";
            this.بدهیDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this.بدهیDataGridViewTextBoxColumn.HeaderText = "بدهی";
            this.بدهیDataGridViewTextBoxColumn.Name = "بدهیDataGridViewTextBoxColumn";
            this.بدهیDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مبلغکلDataGridViewTextBoxColumn
            // 
            this.مبلغکلDataGridViewTextBoxColumn.DataPropertyName = "مبلغ کل";
            dataGridViewCellStyle45.Format = "0,0";
            this.مبلغکلDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this.مبلغکلDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.مبلغکلDataGridViewTextBoxColumn.Name = "مبلغکلDataGridViewTextBoxColumn";
            this.مبلغکلDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // پرداختیDataGridViewTextBoxColumn
            // 
            this.پرداختیDataGridViewTextBoxColumn.DataPropertyName = "پرداختی";
            dataGridViewCellStyle46.Format = "0,0";
            this.پرداختیDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle46;
            this.پرداختیDataGridViewTextBoxColumn.HeaderText = "پرداختی";
            this.پرداختیDataGridViewTextBoxColumn.Name = "پرداختیDataGridViewTextBoxColumn";
            this.پرداختیDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندهDataGridViewTextBoxColumn
            // 
            this.ماندهDataGridViewTextBoxColumn.DataPropertyName = "مانده";
            dataGridViewCellStyle47.Format = "0,0";
            this.ماندهDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle47;
            this.ماندهDataGridViewTextBoxColumn.HeaderText = "مانده";
            this.ماندهDataGridViewTextBoxColumn.Name = "ماندهDataGridViewTextBoxColumn";
            this.ماندهDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn
            // 
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn.DataPropertyName = "بستانکاری جهت انتقال به دوره بعد";
            dataGridViewCellStyle48.Format = "0,0";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle48;
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn.HeaderText = "بستانکاری جهت انتقال به دوره بعد";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn.Name = "بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // آخرینپرداختDataGridViewTextBoxColumn
            // 
            this.آخرینپرداختDataGridViewTextBoxColumn.DataPropertyName = "آخرین پرداخت";
            this.آخرینپرداختDataGridViewTextBoxColumn.HeaderText = "آخرین پرداخت";
            this.آخرینپرداختDataGridViewTextBoxColumn.Name = "آخرینپرداختDataGridViewTextBoxColumn";
            this.آخرینپرداختDataGridViewTextBoxColumn.ReadOnly = true;
            this.آخرینپرداختDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.آخرینپرداختDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // sharjghabzPardakhtBindingSource
            // 
            this.sharjghabzPardakhtBindingSource.DataMember = "sharj_ghabz_Pardakht";
            this.sharjghabzPardakhtBindingSource.DataSource = this.daramadDataset;
            // 
            // daramadDataset
            // 
            this.daramadDataset.DataSetName = "DaramadDataset";
            this.daramadDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.buttonX4);
            this.groupPanel3.Controls.Add(this.buttonX1);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(24, 319);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(1065, 39);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 75;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(211, 7);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(178, 23);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 89;
            this.buttonX4.Text = "چاپ پرداختی های قبوض شارژ";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(14, 7);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(178, 23);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 88;
            this.buttonX1.Text = "چاپ گزارش - قبوض شارژ";
            this.buttonX1.Visible = false;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // TabSharj
            // 
            this.TabSharj.AttachedControl = this.superTabControlPanel5;
            this.TabSharj.GlobalItem = false;
            this.TabSharj.Name = "TabSharj";
            this.TabSharj.Text = "قبوض شارژ";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.groupPanel5);
            this.superTabControlPanel4.Controls.Add(this.dgvAb);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(1111, 368);
            this.superTabControlPanel4.TabIndex = 1;
            this.superTabControlPanel4.TabItem = this.TabAb;
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel5.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.buttonX2);
            this.groupPanel5.Controls.Add(this.buttonX10);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(26, 318);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(1066, 39);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 74;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(187, 7);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(178, 23);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 88;
            this.buttonX2.Text = "چاپ پرداختی های قبوض آب";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_2);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(3, 7);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX10.Size = new System.Drawing.Size(178, 23);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 9F;
            this.buttonX10.TabIndex = 87;
            this.buttonX10.Text = "چاپ اطلاعات قبوض آب";
            this.buttonX10.Visible = false;
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // dgvAb
            // 
            this.dgvAb.AllowUserToAddRows = false;
            this.dgvAb.AllowUserToDeleteRows = false;
            this.dgvAb.AllowUserToResizeColumns = false;
            this.dgvAb.AllowUserToResizeRows = false;
            this.dgvAb.AutoGenerateColumns = false;
            this.dgvAb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAb.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgvAb.ColumnHeadersHeight = 28;
            this.dgvAb.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.کدقبضDataGridViewTextBoxColumn3,
            this.قراردادDataGridViewTextBoxColumn1,
            this.دورهDataGridViewTextBoxColumn1,
            this.آبونمانDataGridViewTextBoxColumn,
            this.سایرجریمهDataGridViewTextBoxColumn1,
            this.آببهاDataGridViewTextBoxColumn,
            this.مجموعمبالغDataGridViewTextBoxColumn1,
            this.مالیاتDataGridViewTextBoxColumn1,
            this.فاضلاب,
            this.بدهیDataGridViewTextBoxColumn1,
            this.مبلغکلDataGridViewTextBoxColumn1,
            this.پرداختیDataGridViewTextBoxColumn1,
            this.ماندهDataGridViewTextBoxColumn1,
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1,
            this.آخرینپرداختDataGridViewTextBoxColumn1});
            this.dgvAb.DataSource = this.abghabzBindingSource;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle37.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAb.DefaultCellStyle = dataGridViewCellStyle37;
            this.dgvAb.EnableHeadersVisualStyles = false;
            this.dgvAb.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvAb.Location = new System.Drawing.Point(26, 13);
            this.dgvAb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvAb.Name = "dgvAb";
            this.dgvAb.ReadOnly = true;
            this.dgvAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("B Yekan", 8.5F);
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvAb.RowHeadersVisible = false;
            this.dgvAb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAb.Size = new System.Drawing.Size(1066, 300);
            this.dgvAb.TabIndex = 46;
            // 
            // کدقبضDataGridViewTextBoxColumn3
            // 
            this.کدقبضDataGridViewTextBoxColumn3.DataPropertyName = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn3.HeaderText = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn3.Name = "کدقبضDataGridViewTextBoxColumn3";
            this.کدقبضDataGridViewTextBoxColumn3.ReadOnly = true;
            this.کدقبضDataGridViewTextBoxColumn3.Width = 70;
            // 
            // قراردادDataGridViewTextBoxColumn1
            // 
            this.قراردادDataGridViewTextBoxColumn1.DataPropertyName = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn1.FillWeight = 50F;
            this.قراردادDataGridViewTextBoxColumn1.HeaderText = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn1.Name = "قراردادDataGridViewTextBoxColumn1";
            this.قراردادDataGridViewTextBoxColumn1.ReadOnly = true;
            this.قراردادDataGridViewTextBoxColumn1.Width = 66;
            // 
            // دورهDataGridViewTextBoxColumn1
            // 
            this.دورهDataGridViewTextBoxColumn1.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn1.Name = "دورهDataGridViewTextBoxColumn1";
            this.دورهDataGridViewTextBoxColumn1.ReadOnly = true;
            this.دورهDataGridViewTextBoxColumn1.Width = 54;
            // 
            // آبونمانDataGridViewTextBoxColumn
            // 
            this.آبونمانDataGridViewTextBoxColumn.DataPropertyName = "آبونمان";
            dataGridViewCellStyle27.Format = "0,0";
            this.آبونمانDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle27;
            this.آبونمانDataGridViewTextBoxColumn.HeaderText = "آبونمان";
            this.آبونمانDataGridViewTextBoxColumn.Name = "آبونمانDataGridViewTextBoxColumn";
            this.آبونمانDataGridViewTextBoxColumn.ReadOnly = true;
            this.آبونمانDataGridViewTextBoxColumn.Width = 64;
            // 
            // سایرجریمهDataGridViewTextBoxColumn1
            // 
            this.سایرجریمهDataGridViewTextBoxColumn1.DataPropertyName = "سایر-جریمه";
            dataGridViewCellStyle28.Format = "0,0";
            this.سایرجریمهDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle28;
            this.سایرجریمهDataGridViewTextBoxColumn1.HeaderText = "سایر-جریمه";
            this.سایرجریمهDataGridViewTextBoxColumn1.Name = "سایرجریمهDataGridViewTextBoxColumn1";
            this.سایرجریمهDataGridViewTextBoxColumn1.ReadOnly = true;
            this.سایرجریمهDataGridViewTextBoxColumn1.Width = 87;
            // 
            // آببهاDataGridViewTextBoxColumn
            // 
            this.آببهاDataGridViewTextBoxColumn.DataPropertyName = "آب بها";
            dataGridViewCellStyle29.Format = "0,0";
            this.آببهاDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle29;
            this.آببهاDataGridViewTextBoxColumn.HeaderText = "آب بها";
            this.آببهاDataGridViewTextBoxColumn.Name = "آببهاDataGridViewTextBoxColumn";
            this.آببهاDataGridViewTextBoxColumn.ReadOnly = true;
            this.آببهاDataGridViewTextBoxColumn.Width = 60;
            // 
            // مجموعمبالغDataGridViewTextBoxColumn1
            // 
            this.مجموعمبالغDataGridViewTextBoxColumn1.DataPropertyName = "مجموع مبالغ";
            dataGridViewCellStyle30.Format = "0,0";
            this.مجموعمبالغDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle30;
            this.مجموعمبالغDataGridViewTextBoxColumn1.HeaderText = "مجموع مبالغ";
            this.مجموعمبالغDataGridViewTextBoxColumn1.Name = "مجموعمبالغDataGridViewTextBoxColumn1";
            this.مجموعمبالغDataGridViewTextBoxColumn1.ReadOnly = true;
            this.مجموعمبالغDataGridViewTextBoxColumn1.Width = 85;
            // 
            // مالیاتDataGridViewTextBoxColumn1
            // 
            this.مالیاتDataGridViewTextBoxColumn1.DataPropertyName = "مالیات";
            dataGridViewCellStyle31.Format = "0,0";
            this.مالیاتDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle31;
            this.مالیاتDataGridViewTextBoxColumn1.HeaderText = "مالیات";
            this.مالیاتDataGridViewTextBoxColumn1.Name = "مالیاتDataGridViewTextBoxColumn1";
            this.مالیاتDataGridViewTextBoxColumn1.ReadOnly = true;
            this.مالیاتDataGridViewTextBoxColumn1.Width = 60;
            // 
            // فاضلاب
            // 
            this.فاضلاب.DataPropertyName = "فاضلاب";
            this.فاضلاب.HeaderText = "فاضلاب";
            this.فاضلاب.Name = "فاضلاب";
            this.فاضلاب.ReadOnly = true;
            this.فاضلاب.Width = 65;
            // 
            // بدهیDataGridViewTextBoxColumn1
            // 
            this.بدهیDataGridViewTextBoxColumn1.DataPropertyName = "بدهی";
            dataGridViewCellStyle32.Format = "0,0";
            this.بدهیDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle32;
            this.بدهیDataGridViewTextBoxColumn1.HeaderText = "بدهی";
            this.بدهیDataGridViewTextBoxColumn1.Name = "بدهیDataGridViewTextBoxColumn1";
            this.بدهیDataGridViewTextBoxColumn1.ReadOnly = true;
            this.بدهیDataGridViewTextBoxColumn1.Width = 58;
            // 
            // مبلغکلDataGridViewTextBoxColumn1
            // 
            this.مبلغکلDataGridViewTextBoxColumn1.DataPropertyName = "مبلغ کل";
            dataGridViewCellStyle33.Format = "0,0";
            this.مبلغکلDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle33;
            this.مبلغکلDataGridViewTextBoxColumn1.HeaderText = "مبلغ کل";
            this.مبلغکلDataGridViewTextBoxColumn1.Name = "مبلغکلDataGridViewTextBoxColumn1";
            this.مبلغکلDataGridViewTextBoxColumn1.ReadOnly = true;
            this.مبلغکلDataGridViewTextBoxColumn1.Width = 69;
            // 
            // پرداختیDataGridViewTextBoxColumn1
            // 
            this.پرداختیDataGridViewTextBoxColumn1.DataPropertyName = "پرداختی";
            dataGridViewCellStyle34.Format = "0,0";
            this.پرداختیDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle34;
            this.پرداختیDataGridViewTextBoxColumn1.HeaderText = "پرداختی";
            this.پرداختیDataGridViewTextBoxColumn1.Name = "پرداختیDataGridViewTextBoxColumn1";
            this.پرداختیDataGridViewTextBoxColumn1.ReadOnly = true;
            this.پرداختیDataGridViewTextBoxColumn1.Width = 69;
            // 
            // ماندهDataGridViewTextBoxColumn1
            // 
            this.ماندهDataGridViewTextBoxColumn1.DataPropertyName = "مانده";
            dataGridViewCellStyle35.Format = "0,0";
            this.ماندهDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle35;
            this.ماندهDataGridViewTextBoxColumn1.HeaderText = "مانده";
            this.ماندهDataGridViewTextBoxColumn1.Name = "ماندهDataGridViewTextBoxColumn1";
            this.ماندهDataGridViewTextBoxColumn1.ReadOnly = true;
            this.ماندهDataGridViewTextBoxColumn1.Width = 56;
            // 
            // بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1
            // 
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.DataPropertyName = "بستانکاری جهت انتقال به دوره بعد";
            dataGridViewCellStyle36.Format = "0,0";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle36;
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.HeaderText = "بستانکار";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.Name = "بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1";
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.ReadOnly = true;
            this.بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1.Width = 69;
            // 
            // آخرینپرداختDataGridViewTextBoxColumn1
            // 
            this.آخرینپرداختDataGridViewTextBoxColumn1.DataPropertyName = "آخرین پرداخت";
            this.آخرینپرداختDataGridViewTextBoxColumn1.HeaderText = "آخرین پرداخت";
            this.آخرینپرداختDataGridViewTextBoxColumn1.Name = "آخرینپرداختDataGridViewTextBoxColumn1";
            this.آخرینپرداختDataGridViewTextBoxColumn1.ReadOnly = true;
            this.آخرینپرداختDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.آخرینپرداختDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.آخرینپرداختDataGridViewTextBoxColumn1.Width = 97;
            // 
            // abghabzBindingSource
            // 
            this.abghabzBindingSource.DataMember = "ab_ghabz";
            this.abghabzBindingSource.DataSource = this.daramadDataset1;
            // 
            // daramadDataset1
            // 
            this.daramadDataset1.DataSetName = "DaramadDataset";
            this.daramadDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabAb
            // 
            this.TabAb.AttachedControl = this.superTabControlPanel4;
            this.TabAb.GlobalItem = false;
            this.TabAb.Name = "TabAb";
            this.TabAb.Text = "قبوض آب";
            // 
            // کدقبضDataGridViewTextBoxColumn
            // 
            this.کدقبضDataGridViewTextBoxColumn.DataPropertyName = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn.HeaderText = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn.Name = "کدقبضDataGridViewTextBoxColumn";
            this.کدقبضDataGridViewTextBoxColumn.ReadOnly = true;
            this.کدقبضDataGridViewTextBoxColumn.Width = 95;
            // 
            // قراردادDataGridViewTextBoxColumn
            // 
            this.قراردادDataGridViewTextBoxColumn.DataPropertyName = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn.FillWeight = 50F;
            this.قراردادDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn.Name = "قراردادDataGridViewTextBoxColumn";
            this.قراردادDataGridViewTextBoxColumn.ReadOnly = true;
            this.قراردادDataGridViewTextBoxColumn.Width = 47;
            // 
            // دورهDataGridViewTextBoxColumn
            // 
            this.دورهDataGridViewTextBoxColumn.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn.FillWeight = 50F;
            this.دورهDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn.Name = "دورهDataGridViewTextBoxColumn";
            this.دورهDataGridViewTextBoxColumn.ReadOnly = true;
            this.دورهDataGridViewTextBoxColumn.Width = 48;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "مبلغ شارژ";
            this.dataGridViewTextBoxColumn1.HeaderText = "مبلغ شارژ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 95;
            // 
            // کدقبضDataGridViewTextBoxColumn1
            // 
            this.کدقبضDataGridViewTextBoxColumn1.DataPropertyName = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn1.HeaderText = "کد قبض";
            this.کدقبضDataGridViewTextBoxColumn1.Name = "کدقبضDataGridViewTextBoxColumn1";
            this.کدقبضDataGridViewTextBoxColumn1.Width = 70;
            // 
            // بدهی
            // 
            this.بدهی.DataPropertyName = "بدهی";
            this.بدهی.HeaderText = "بدهی";
            this.بدهی.Name = "بدهی";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_end_doreh);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.txt_start_doreh);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(761, 15);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(387, 82);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 75;
            this.groupPanel2.Text = "براساس دوره";
            // 
            // txt_end_doreh
            // 
            this.txt_end_doreh.BackColor = System.Drawing.Color.White;
            this.txt_end_doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_end_doreh.Location = new System.Drawing.Point(159, 14);
            this.txt_end_doreh.Name = "txt_end_doreh";
            this.txt_end_doreh.Size = new System.Drawing.Size(69, 25);
            this.txt_end_doreh.TabIndex = 76;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(209, 10);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(39, 32);
            this.labelX2.TabIndex = 75;
            this.labelX2.Text = "تا :";
            // 
            // txt_start_doreh
            // 
            this.txt_start_doreh.BackColor = System.Drawing.Color.White;
            this.txt_start_doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_start_doreh.Location = new System.Drawing.Point(259, 14);
            this.txt_start_doreh.Name = "txt_start_doreh";
            this.txt_start_doreh.Size = new System.Drawing.Size(69, 25);
            this.txt_start_doreh.TabIndex = 74;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(1060, 16);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(89, 32);
            this.labelX3.TabIndex = 70;
            this.labelX3.Text = "از تاریخ :";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(4, 13);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(126, 27);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 40;
            this.buttonX3.Text = "نمایش اطلاعات ";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(335, 10);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(39, 32);
            this.labelX4.TabIndex = 73;
            this.labelX4.Text = "از دوره ";
            // 
            // sharj_ghabz_PardakhtTableAdapter
            // 
            this.sharj_ghabz_PardakhtTableAdapter.ClearBeforeFill = true;
            // 
            // ab_ghabzTableAdapter
            // 
            this.ab_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // FrmGhabzPardakhtRpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 557);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel4);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.5F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmGhabzPardakhtRpt";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmGhabzPardakhtRpt_Load);
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).EndInit();
            this.superTabControl2.ResumeLayout(false);
            this.superTabControlPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSharj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzPardakhtBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            this.superTabControlPanel4.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset1)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private DevComponents.DotNetBar.LabelX labelX5;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX BtnFindByTarikh;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.SuperTabControl superTabControl2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.SuperTabItem TabSharj;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAb;
        private DevComponents.DotNetBar.SuperTabItem TabAb;
        private DaramadDataset daramadDataset;
        private System.Windows.Forms.BindingSource sharjghabzPardakhtBindingSource;
        private DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter sharj_ghabz_PardakhtTableAdapter;
        private System.Windows.Forms.BindingSource abghabzBindingSource;
        private DaramadDataset daramadDataset1;
        private DaramadDatasetTableAdapters.ab_ghabzTableAdapter ab_ghabzTableAdapter;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSharj;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn قراردادDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn بدهی;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn قراردادDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn سایرجریمهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مبلغشارژDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیاتDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بدهیDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مبلغکلDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn پرداختیDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn آخرینپرداختDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.TextBox txt_start_doreh;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn قراردادDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn آبونمانDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn سایرجریمهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn آببهاDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیاتDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn فاضلاب;
        private System.Windows.Forms.DataGridViewTextBoxColumn بدهیDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مبلغکلDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn پرداختیDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn بستانکاریجهتانتقالبهدورهبعدDataGridViewTextBoxColumn1;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn آخرینپرداختDataGridViewTextBoxColumn1;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private System.Windows.Forms.TextBox txt_end_doreh;
        private DevComponents.DotNetBar.LabelX labelX2;
    }
}