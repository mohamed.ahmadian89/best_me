using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.ManagementReports
{
    public partial class FrmNahvePardakhtReport : DevComponents.DotNetBar.Metro.MetroForm
    {
        DataTable Dt_Nahve_pardakht = null;


        public FrmNahvePardakhtReport(DataTable dt, string infoText, string caption, string XVales, string YValue, string YValue2, string YValue3, string YValue4, string YValue5)
        {
            InitializeComponent();
            Dt_Nahve_pardakht = dt;
            lblTyoe.Text = infoText;
            chart1.Series[0].LegendText = grpHeader.Text = caption;
            chart1.DataSource = Dt_Nahve_pardakht;
            chart1.Series[0].XValueMember = XVales;
            chart1.Series[0].YValueMembers = YValue;
            chart1.Series[1].YValueMembers = YValue2;
            chart1.Series[2].YValueMembers = YValue3;
            chart1.Series[3].YValueMembers = YValue4;
            chart1.Series[4].YValueMembers = YValue5;


            
        }




        private void FrmNahvePardakhtReport_Load(object sender, EventArgs e)
        {





        }
    }
}