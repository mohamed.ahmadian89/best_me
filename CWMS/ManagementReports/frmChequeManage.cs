﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using FarsiLibrary.Utils;
using Snippet;
using CWMS.Classes;

namespace CWMS.ManagementReports
{
    public partial class frmChequeManage : MyMetroForm
    {
        string gFilter = "", wFilter = "", dFilter = "";
        bool loaded = false;

        public frmChequeManage()
        {
            ClsMain.ChangeCulture("f");
            InitializeComponent();
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            if (!loaded)
                loadInformationFromDB();
            changeFilter("", 1);
        }

        private void loadInformationFromDB()
        {
            try
            {
                this.chequesTableAdapter.Fill(this.chequesDataSet.Cheques);
                loaded = true;
            }
            catch (Exception ex)
            {
                Payam.Show("خطا در خواندن اطلاعات از دیتابیس. لطفا با واحد پشتیبان هماهنگ نمایید");
                ClsMain.logError(ex);
            }
        }

        void changeFilter(string filter, int type)
        {
            string finalFilter = "";
            switch (type)
            {
                case 1:
                    gFilter = filter;
                    break;
                case 2:
                    dFilter = filter;
                    break;
                case 3:
                    wFilter = filter;
                    break;
                default:
                    break;
            }
            if (gFilter != "")
                finalFilter += gFilter + " and ";
            if (dFilter != "")
                finalFilter += dFilter + " and ";
            if (wFilter != "")
                finalFilter += wFilter;
            else
                if (finalFilter != "")
                    finalFilter = finalFilter.Substring(0, finalFilter.Length - 5);
            try
            {
                chequesBindingSource.Filter = finalFilter;
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex, "خطا در اعمال فیلتر\nفیلتر فعلی\n" + chequesBindingSource.Filter + "\nفیلتر جدید\n" + finalFilter);
            }
        }

        void removeFilter()
        {
            gFilter = dFilter = wFilter = "";
            chequesBindingSource.RemoveFilter();
        }

        private void tabToday_GotFocus(object sender, EventArgs e)
        {
            changeFilter("tarikh_sarresid='" + DateTime.Today + "'", 2);
        }

        private void tabNext30Days_GotFocus(object sender, EventArgs e)
        {
            changeFilter("tarikh_sarresid<='" + new PersianCalendar().AddDays(DateTime.Now, 30) + "'", 2);
        }

        private void tabCurrentYear_GotFocus(object sender, EventArgs e)
        {
            PersianDate dt = PersianDateConverter.ToPersianDate(DateTime.Now);
            changeFilter("tarikh_sarresid<'" +
                new PersianCalendar().ToDateTime(dt.Year + 1, 1, 1, 0, 0, 0, 0).ToShortDateString() + "'", 2);
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            int temp;
            if (Int32.TryParse(txtGharardad.Text, out temp))
            {
                if (!loaded)
                    loadInformationFromDB();
                changeFilter("gharardad = " + txtGharardad.Text, 1);
            }
            else
                Payam.Show("شماره قرارداد باید مقدار عددی باشد");
        }

        private void radRecieved_CheckedChanged(object sender, EventArgs e)
        {
            changeFilter("tarikh_vosool IS NOT NULL", 3);
        }

        private void radNonRecieved_CheckedChanged(object sender, EventArgs e)
        {
            changeFilter("tarikh_vosool = null", 3);
        }

        private void radAll_CheckedChanged(object sender, EventArgs e)
        {
            changeFilter("", 3);
        }

        private void btnAdvancedSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpFrom.SelectedDateTime != null && dtpTo.SelectedDateTime != null)
                    changeFilter("(tarikh_sarresid >= '" +
                        dtpFrom.SelectedDateTime + "' and tarikh_sarresid <='" +
                        dtpTo.SelectedDateTime + "')", 2);
                else
                    Payam.Show("ابتدا و انتهای بازه زمانی را مشخص کنید");
            }
            catch (Exception ex)
            {
                Payam.Show("بازه زمانی معتبر نیست. لطفا مجددا تلاش نمایید");
                MessageBox.Show(ExcpToStr.getString(ex));
            }
        }

        private void btnAllDates_Click(object sender, EventArgs e)
        {
            changeFilter("", 2);
        }

        private void frmChequeManage_Load(object sender, EventArgs e)
        {
            dtpFrom.SelectedDateTime = dtpTo.SelectedDateTime = DateTime.Now;
        }

        private void dgvToday_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvToday.SelectedRows.Count != 0)
            {

                new ManagementReports.Cheqeue_details(dgvToday.SelectedRows[0].Cells[0].Value.ToString()).ShowDialog();
                this.chequesTableAdapter.Fill(this.chequesDataSet.Cheques);

            }
        }

        private void dataGridViewX1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewX1.SelectedRows.Count != 0)
            {
                new ManagementReports.Cheqeue_details(dataGridViewX1.SelectedRows[0].Cells[0].Value.ToString()).ShowDialog();
                this.chequesTableAdapter.Fill(this.chequesDataSet.Cheques);

            }
        }

        private void dataGridViewX2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewX2.SelectedRows.Count != 0)
            {
                new ManagementReports.Cheqeue_details(dataGridViewX2.SelectedRows[0].Cells[0].Value.ToString()).ShowDialog();
                this.chequesTableAdapter.Fill(this.chequesDataSet.Cheques);

            }
        }

        private void dataGridViewX3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewX3.SelectedRows.Count != 0)
            {
                new ManagementReports.Cheqeue_details(dataGridViewX3.SelectedRows[0].Cells[0].Value.ToString()).ShowDialog();
                this.chequesTableAdapter.Fill(this.chequesDataSet.Cheques);

            }
        }

        private void btnShowcheqeue_Click(object sender, EventArgs e)
        {
       
        }
    }
}