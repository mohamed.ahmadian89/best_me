﻿namespace CWMS.ManagementReports
{
    partial class FrmGhabzRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrpAddin = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ChPShodeh = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ChPNot = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ChPJozee = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.GrpGhobooz = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.superTabControl2 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.BtnNemoodarAb = new DevComponents.DotNetBar.ButtonX();
            this.dgv_ab_ghabz = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.Ensheab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf_gheir_mojaz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf_mojaz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fazelab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jarimeh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sayer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maliat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablagh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mande = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.TabAb = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.btnNemoodarSharj = new DevComponents.DotNetBar.ButtonX();
            this.dgv_Sharj_ghabz = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewFADateTimePickerColumn1 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.metraj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.TabSharj = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel7 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel8 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.txtEndMablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.TxtStartMablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.TxtSharjDoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.TxtAbDoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtGhabzcode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.BtnFindByCode = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.BtnFindByTarikh = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel6 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel6 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtFindGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.TabMoshtarek = new DevComponents.DotNetBar.SuperTabItem();
            this.ab_ghabzTableAdapter = new CWMS.AbDatasetTableAdapters.ab_ghabzTableAdapter();
            this.sharj_ghabzTableAdapter1 = new CWMS.MainDataSestTableAdapters.sharj_ghabzTableAdapter();
            this.GrpAddin.SuspendLayout();
            this.GrpGhobooz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).BeginInit();
            this.superTabControl2.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_ghabz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            this.superTabControlPanel5.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Sharj_ghabz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel7.SuspendLayout();
            this.groupPanel8.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.superTabControlPanel6.SuspendLayout();
            this.groupPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpAddin
            // 
            this.GrpAddin.BackColor = System.Drawing.Color.White;
            this.GrpAddin.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GrpAddin.Controls.Add(this.ChPShodeh);
            this.GrpAddin.Controls.Add(this.ChPNot);
            this.GrpAddin.Controls.Add(this.ChPJozee);
            this.GrpAddin.DisabledBackColor = System.Drawing.Color.Empty;
            this.GrpAddin.Location = new System.Drawing.Point(32, 189);
            this.GrpAddin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpAddin.Name = "GrpAddin";
            this.GrpAddin.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.GrpAddin.Size = new System.Drawing.Size(1212, 51);
            // 
            // 
            // 
            this.GrpAddin.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GrpAddin.Style.BackColorGradientAngle = 90;
            this.GrpAddin.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GrpAddin.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpAddin.Style.BorderBottomWidth = 1;
            this.GrpAddin.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GrpAddin.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpAddin.Style.BorderLeftWidth = 1;
            this.GrpAddin.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpAddin.Style.BorderRightWidth = 1;
            this.GrpAddin.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpAddin.Style.BorderTopWidth = 1;
            this.GrpAddin.Style.CornerDiameter = 4;
            this.GrpAddin.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GrpAddin.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GrpAddin.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GrpAddin.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GrpAddin.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GrpAddin.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GrpAddin.TabIndex = 78;
            // 
            // ChPShodeh
            // 
            this.ChPShodeh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChPShodeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChPShodeh.ForeColor = System.Drawing.Color.Black;
            this.ChPShodeh.Location = new System.Drawing.Point(243, 1);
            this.ChPShodeh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChPShodeh.Name = "ChPShodeh";
            this.ChPShodeh.Size = new System.Drawing.Size(96, 35);
            this.ChPShodeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChPShodeh.TabIndex = 4;
            this.ChPShodeh.Text = "پرداخت شده";
            this.ChPShodeh.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ChPShodeh.CheckedChanged += new System.EventHandler(this.ChPShodeh_CheckedChanged);
            // 
            // ChPNot
            // 
            this.ChPNot.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChPNot.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChPNot.ForeColor = System.Drawing.Color.Black;
            this.ChPNot.Location = new System.Drawing.Point(-5, 1);
            this.ChPNot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChPNot.Name = "ChPNot";
            this.ChPNot.Size = new System.Drawing.Size(133, 35);
            this.ChPNot.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChPNot.TabIndex = 3;
            this.ChPNot.Text = "پرداخت نشده";
            this.ChPNot.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ChPNot.CheckedChanged += new System.EventHandler(this.ChPNot_CheckedChanged);
            // 
            // ChPJozee
            // 
            this.ChPJozee.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ChPJozee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ChPJozee.ForeColor = System.Drawing.Color.Black;
            this.ChPJozee.Location = new System.Drawing.Point(101, 1);
            this.ChPJozee.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChPJozee.Name = "ChPJozee";
            this.ChPJozee.Size = new System.Drawing.Size(133, 35);
            this.ChPJozee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ChPJozee.TabIndex = 2;
            this.ChPJozee.Text = "پرداخت جزئی";
            this.ChPJozee.TextColor = System.Drawing.Color.DarkKhaki;
            this.ChPJozee.CheckedChanged += new System.EventHandler(this.ChPJozee_CheckedChanged);
            // 
            // GrpGhobooz
            // 
            this.GrpGhobooz.BackColor = System.Drawing.Color.White;
            this.GrpGhobooz.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GrpGhobooz.Controls.Add(this.superTabControl2);
            this.GrpGhobooz.DisabledBackColor = System.Drawing.Color.Empty;
            this.GrpGhobooz.Location = new System.Drawing.Point(35, 247);
            this.GrpGhobooz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpGhobooz.Name = "GrpGhobooz";
            this.GrpGhobooz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.GrpGhobooz.Size = new System.Drawing.Size(1212, 412);
            // 
            // 
            // 
            this.GrpGhobooz.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GrpGhobooz.Style.BackColorGradientAngle = 90;
            this.GrpGhobooz.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GrpGhobooz.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpGhobooz.Style.BorderBottomWidth = 1;
            this.GrpGhobooz.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GrpGhobooz.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpGhobooz.Style.BorderLeftWidth = 1;
            this.GrpGhobooz.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpGhobooz.Style.BorderRightWidth = 1;
            this.GrpGhobooz.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpGhobooz.Style.BorderTopWidth = 1;
            this.GrpGhobooz.Style.CornerDiameter = 4;
            this.GrpGhobooz.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GrpGhobooz.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GrpGhobooz.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GrpGhobooz.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GrpGhobooz.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GrpGhobooz.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GrpGhobooz.TabIndex = 77;
            // 
            // superTabControl2
            // 
            this.superTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.superTabControl2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl2.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl2.ControlBox.MenuBox.Name = "";
            this.superTabControl2.ControlBox.Name = "";
            this.superTabControl2.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl2.ControlBox.CloseBox,
            this.superTabControl2.ControlBox.MenuBox});
            this.superTabControl2.Controls.Add(this.superTabControlPanel5);
            this.superTabControl2.Controls.Add(this.superTabControlPanel4);
            this.superTabControl2.ForeColor = System.Drawing.Color.Black;
            this.superTabControl2.Location = new System.Drawing.Point(23, 5);
            this.superTabControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControl2.Name = "superTabControl2";
            this.superTabControl2.ReorderTabsEnabled = true;
            this.superTabControl2.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl2.SelectedTabIndex = 1;
            this.superTabControl2.Size = new System.Drawing.Size(1140, 395);
            this.superTabControl2.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl2.TabIndex = 0;
            this.superTabControl2.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabAb,
            this.TabSharj});
            this.superTabControl2.Text = "superTabControl2";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.groupPanel5);
            this.superTabControlPanel4.Controls.Add(this.dgv_ab_ghabz);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(1140, 368);
            this.superTabControlPanel4.TabIndex = 1;
            this.superTabControlPanel4.TabItem = this.TabAb;
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.buttonX10);
            this.groupPanel5.Controls.Add(this.BtnNemoodarAb);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(24, 318);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(1093, 39);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 74;
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(18, 7);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX10.Size = new System.Drawing.Size(178, 23);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 9F;
            this.buttonX10.TabIndex = 87;
            this.buttonX10.Text = "چاپ گزارش";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // BtnNemoodarAb
            // 
            this.BtnNemoodarAb.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnNemoodarAb.BackColor = System.Drawing.Color.Transparent;
            this.BtnNemoodarAb.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnNemoodarAb.Location = new System.Drawing.Point(744, 2);
            this.BtnNemoodarAb.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.BtnNemoodarAb.Name = "BtnNemoodarAb";
            this.BtnNemoodarAb.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.BtnNemoodarAb.Size = new System.Drawing.Size(337, 27);
            this.BtnNemoodarAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnNemoodarAb.Symbol = "";
            this.BtnNemoodarAb.SymbolColor = System.Drawing.Color.Green;
            this.BtnNemoodarAb.SymbolSize = 9F;
            this.BtnNemoodarAb.TabIndex = 50;
            this.BtnNemoodarAb.Text = "مشاهده قبوض آب بر نمودار ";
            this.BtnNemoodarAb.Visible = false;
            this.BtnNemoodarAb.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // dgv_ab_ghabz
            // 
            this.dgv_ab_ghabz.AllowUserToAddRows = false;
            this.dgv_ab_ghabz.AllowUserToDeleteRows = false;
            this.dgv_ab_ghabz.AllowUserToResizeColumns = false;
            this.dgv_ab_ghabz.AllowUserToResizeRows = false;
            this.dgv_ab_ghabz.AutoGenerateColumns = false;
            this.dgv_ab_ghabz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_ab_ghabz.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ab_ghabz.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_ab_ghabz.ColumnHeadersHeight = 28;
            this.dgv_ab_ghabz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcodeDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.Ensheab,
            this.masraf,
            this.masraf_gheir_mojaz,
            this.masraf_mojaz,
            this.fazelab,
            this.jarimeh,
            this.sayer,
            this.maliat,
            this.mablagh,
            this.mande,
            this.mablaghkolDataGridViewTextBoxColumn});
            this.dgv_ab_ghabz.DataSource = this.abghabzBindingSource;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ab_ghabz.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgv_ab_ghabz.EnableHeadersVisualStyles = false;
            this.dgv_ab_ghabz.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_ab_ghabz.Location = new System.Drawing.Point(25, 13);
            this.dgv_ab_ghabz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgv_ab_ghabz.Name = "dgv_ab_ghabz";
            this.dgv_ab_ghabz.ReadOnly = true;
            this.dgv_ab_ghabz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ab_ghabz.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgv_ab_ghabz.RowHeadersVisible = false;
            this.dgv_ab_ghabz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ab_ghabz.Size = new System.Drawing.Size(1092, 300);
            this.dgv_ab_ghabz.TabIndex = 46;
            this.dgv_ab_ghabz.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ab_ghabz_CellClick);
            this.dgv_ab_ghabz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_ab_ghabz_KeyDown);
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.FillWeight = 80F;
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کد";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.gcodeDataGridViewTextBoxColumn.Width = 49;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 60F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.dcodeDataGridViewTextBoxColumn.Width = 60;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 60F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            this.gharardadDataGridViewTextBoxColumn.Width = 75;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhDataGridViewTextBoxColumn.Width = 62;
            // 
            // Ensheab
            // 
            this.Ensheab.DataPropertyName = "Ensheab";
            dataGridViewCellStyle9.Format = "0,0";
            this.Ensheab.DefaultCellStyle = dataGridViewCellStyle9;
            this.Ensheab.HeaderText = "آبونمان";
            this.Ensheab.Name = "Ensheab";
            this.Ensheab.ReadOnly = true;
            this.Ensheab.Width = 72;
            // 
            // masraf
            // 
            this.masraf.DataPropertyName = "masraf";
            this.masraf.HeaderText = "میزان مصرف";
            this.masraf.Name = "masraf";
            this.masraf.ReadOnly = true;
            this.masraf.Width = 102;
            // 
            // masraf_gheir_mojaz
            // 
            this.masraf_gheir_mojaz.DataPropertyName = "masraf_gheir_mojaz";
            this.masraf_gheir_mojaz.HeaderText = "مصرف غیرمجاز";
            this.masraf_gheir_mojaz.Name = "masraf_gheir_mojaz";
            this.masraf_gheir_mojaz.ReadOnly = true;
            this.masraf_gheir_mojaz.Width = 111;
            // 
            // masraf_mojaz
            // 
            this.masraf_mojaz.DataPropertyName = "masraf_mojaz";
            this.masraf_mojaz.HeaderText = "مصرف مجاز";
            this.masraf_mojaz.Name = "masraf_mojaz";
            this.masraf_mojaz.ReadOnly = true;
            this.masraf_mojaz.Width = 94;
            // 
            // fazelab
            // 
            this.fazelab.DataPropertyName = "fazelab";
            this.fazelab.HeaderText = "فاضلاب";
            this.fazelab.Name = "fazelab";
            this.fazelab.ReadOnly = true;
            this.fazelab.Width = 73;
            // 
            // jarimeh
            // 
            this.jarimeh.DataPropertyName = "jarimeh";
            dataGridViewCellStyle10.Format = "0,0";
            this.jarimeh.DefaultCellStyle = dataGridViewCellStyle10;
            this.jarimeh.HeaderText = "جریمه";
            this.jarimeh.Name = "jarimeh";
            this.jarimeh.ReadOnly = true;
            this.jarimeh.Width = 66;
            // 
            // sayer
            // 
            this.sayer.DataPropertyName = "sayer";
            dataGridViewCellStyle11.Format = "0,0";
            this.sayer.DefaultCellStyle = dataGridViewCellStyle11;
            this.sayer.HeaderText = "سایر";
            this.sayer.Name = "sayer";
            this.sayer.ReadOnly = true;
            this.sayer.Width = 59;
            // 
            // maliat
            // 
            this.maliat.DataPropertyName = "maliat";
            dataGridViewCellStyle12.Format = "0,0";
            this.maliat.DefaultCellStyle = dataGridViewCellStyle12;
            this.maliat.HeaderText = "مالیات";
            this.maliat.Name = "maliat";
            this.maliat.ReadOnly = true;
            this.maliat.Width = 67;
            // 
            // mablagh
            // 
            this.mablagh.DataPropertyName = "mablagh";
            dataGridViewCellStyle13.Format = "0,0";
            this.mablagh.DefaultCellStyle = dataGridViewCellStyle13;
            this.mablagh.HeaderText = "مبلغ";
            this.mablagh.Name = "mablagh";
            this.mablagh.ReadOnly = true;
            this.mablagh.Width = 58;
            // 
            // mande
            // 
            this.mande.DataPropertyName = "mande";
            dataGridViewCellStyle14.Format = "0,0";
            this.mande.DefaultCellStyle = dataGridViewCellStyle14;
            this.mande.HeaderText = "مانده";
            this.mande.Name = "mande";
            this.mande.ReadOnly = true;
            this.mande.Width = 62;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            dataGridViewCellStyle15.Format = "0,0";
            this.mablaghkolDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            this.mablaghkolDataGridViewTextBoxColumn.Width = 77;
            // 
            // abghabzBindingSource
            // 
            this.abghabzBindingSource.DataMember = "ab_ghabz";
            this.abghabzBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabAb
            // 
            this.TabAb.AttachedControl = this.superTabControlPanel4;
            this.TabAb.GlobalItem = false;
            this.TabAb.Name = "TabAb";
            this.TabAb.Text = "قبوض آب";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Controls.Add(this.groupPanel3);
            this.superTabControlPanel5.Controls.Add(this.dgv_Sharj_ghabz);
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(1140, 368);
            this.superTabControlPanel5.TabIndex = 0;
            this.superTabControlPanel5.TabItem = this.TabSharj;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.buttonX5);
            this.groupPanel3.Controls.Add(this.btnNemoodarSharj);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(24, 319);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(1093, 39);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 75;
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(15, 7);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX5.Size = new System.Drawing.Size(178, 23);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Green;
            this.buttonX5.SymbolSize = 9F;
            this.buttonX5.TabIndex = 87;
            this.buttonX5.Text = "چاپ گزارش";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click_1);
            // 
            // btnNemoodarSharj
            // 
            this.btnNemoodarSharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNemoodarSharj.BackColor = System.Drawing.Color.Transparent;
            this.btnNemoodarSharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNemoodarSharj.Location = new System.Drawing.Point(741, 2);
            this.btnNemoodarSharj.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.btnNemoodarSharj.Name = "btnNemoodarSharj";
            this.btnNemoodarSharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnNemoodarSharj.Size = new System.Drawing.Size(337, 27);
            this.btnNemoodarSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnNemoodarSharj.Symbol = "";
            this.btnNemoodarSharj.SymbolColor = System.Drawing.Color.Green;
            this.btnNemoodarSharj.SymbolSize = 9F;
            this.btnNemoodarSharj.TabIndex = 80;
            this.btnNemoodarSharj.Text = "مشاهده قبوض شارژ بر نمودار ";
            this.btnNemoodarSharj.Visible = false;
            this.btnNemoodarSharj.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // dgv_Sharj_ghabz
            // 
            this.dgv_Sharj_ghabz.AllowUserToAddRows = false;
            this.dgv_Sharj_ghabz.AllowUserToDeleteRows = false;
            this.dgv_Sharj_ghabz.AllowUserToResizeColumns = false;
            this.dgv_Sharj_ghabz.AllowUserToResizeRows = false;
            this.dgv_Sharj_ghabz.AutoGenerateColumns = false;
            this.dgv_Sharj_ghabz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Sharj_ghabz.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Sharj_ghabz.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Sharj_ghabz.ColumnHeadersHeight = 28;
            this.dgv_Sharj_ghabz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.gharardadDataGridViewTextBoxColumn1,
            this.dataGridViewFADateTimePickerColumn1,
            this.metraj,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgv_Sharj_ghabz.DataSource = this.sharjghabzBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Sharj_ghabz.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_Sharj_ghabz.EnableHeadersVisualStyles = false;
            this.dgv_Sharj_ghabz.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_Sharj_ghabz.Location = new System.Drawing.Point(25, 13);
            this.dgv_Sharj_ghabz.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_Sharj_ghabz.Name = "dgv_Sharj_ghabz";
            this.dgv_Sharj_ghabz.ReadOnly = true;
            this.dgv_Sharj_ghabz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Sharj_ghabz.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_Sharj_ghabz.RowHeadersVisible = false;
            this.dgv_Sharj_ghabz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Sharj_ghabz.Size = new System.Drawing.Size(1092, 300);
            this.dgv_Sharj_ghabz.TabIndex = 40;
            this.dgv_Sharj_ghabz.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Sharj_ghabz_CellClick);
            this.dgv_Sharj_ghabz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_Sharj_ghabz_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "gcode";
            this.dataGridViewTextBoxColumn1.FillWeight = 80F;
            this.dataGridViewTextBoxColumn1.HeaderText = "کد";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "dcode";
            this.dataGridViewTextBoxColumn2.FillWeight = 60F;
            this.dataGridViewTextBoxColumn2.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // gharardadDataGridViewTextBoxColumn1
            // 
            this.gharardadDataGridViewTextBoxColumn1.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn1.FillWeight = 60F;
            this.gharardadDataGridViewTextBoxColumn1.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn1.Name = "gharardadDataGridViewTextBoxColumn1";
            this.gharardadDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewFADateTimePickerColumn1
            // 
            this.dataGridViewFADateTimePickerColumn1.DataPropertyName = "tarikh";
            this.dataGridViewFADateTimePickerColumn1.HeaderText = "تاریخ";
            this.dataGridViewFADateTimePickerColumn1.Name = "dataGridViewFADateTimePickerColumn1";
            this.dataGridViewFADateTimePickerColumn1.ReadOnly = true;
            this.dataGridViewFADateTimePickerColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewFADateTimePickerColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // metraj
            // 
            this.metraj.DataPropertyName = "metraj";
            this.metraj.HeaderText = "متراژ";
            this.metraj.Name = "metraj";
            this.metraj.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "sayer";
            this.dataGridViewTextBoxColumn3.HeaderText = "سایر";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "jarimeh";
            this.dataGridViewTextBoxColumn4.HeaderText = "جریمه";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "maliat";
            dataGridViewCellStyle2.Format = "#,#";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn5.HeaderText = "مالیات";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "mablagh";
            dataGridViewCellStyle3.Format = "#,#";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn6.HeaderText = "مبلغ";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "mande";
            dataGridViewCellStyle4.Format = "#,#";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn7.HeaderText = "مانده";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "mablaghkol";
            dataGridViewCellStyle5.Format = "#,#";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn8.FillWeight = 120F;
            this.dataGridViewTextBoxColumn8.HeaderText = "مبلغ کل";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // sharjghabzBindingSource
            // 
            this.sharjghabzBindingSource.DataMember = "sharj_ghabz";
            this.sharjghabzBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TabSharj
            // 
            this.TabSharj.AttachedControl = this.superTabControlPanel5;
            this.TabSharj.GlobalItem = false;
            this.TabSharj.Name = "TabSharj";
            this.TabSharj.Text = "قبوض شارژ";
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel6);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel7);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(32, 2);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 2;
            this.superTabControl1.Size = new System.Drawing.Size(1215, 181);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 76;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem3,
            this.superTabItem1,
            this.superTabItem2,
            this.TabMoshtarek,
            this.superTabItem4});
            this.superTabControl1.Text = "superTabControl1";
            this.superTabControl1.SelectedTabChanged += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs>(this.superTabControl1_SelectedTabChanged);
            // 
            // superTabControlPanel7
            // 
            this.superTabControlPanel7.Controls.Add(this.groupPanel8);
            this.superTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel7.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.superTabControlPanel7.Name = "superTabControlPanel7";
            this.superTabControlPanel7.Size = new System.Drawing.Size(1215, 181);
            this.superTabControlPanel7.TabIndex = 0;
            this.superTabControlPanel7.TabItem = this.superTabItem4;
            // 
            // groupPanel8
            // 
            this.groupPanel8.BackColor = System.Drawing.Color.White;
            this.groupPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel8.Controls.Add(this.buttonX4);
            this.groupPanel8.Controls.Add(this.txtEndMablagh);
            this.groupPanel8.Controls.Add(this.labelX7);
            this.groupPanel8.Controls.Add(this.TxtStartMablagh);
            this.groupPanel8.Controls.Add(this.labelX6);
            this.groupPanel8.Controls.Add(this.buttonX2);
            this.groupPanel8.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel8.Location = new System.Drawing.Point(13, 16);
            this.groupPanel8.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel8.Name = "groupPanel8";
            this.groupPanel8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel8.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel8.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel8.Style.BackColorGradientAngle = 90;
            this.groupPanel8.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel8.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderBottomWidth = 1;
            this.groupPanel8.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel8.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderLeftWidth = 1;
            this.groupPanel8.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderRightWidth = 1;
            this.groupPanel8.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderTopWidth = 1;
            this.groupPanel8.Style.CornerDiameter = 4;
            this.groupPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel8.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel8.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel8.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel8.TabIndex = 66;
            this.groupPanel8.Text = "جستجوی اطلاعات بر اساس مبلغ قبض";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(12, 49);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(385, 27);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 46;
            this.buttonX4.Text = "نمایش بیشترین مبلغ قبض به تفکیک دوره";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click_1);
            // 
            // txtEndMablagh
            // 
            this.txtEndMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtEndMablagh.Border.Class = "TextBoxBorder";
            this.txtEndMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtEndMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtEndMablagh.ForeColor = System.Drawing.Color.Black;
            this.txtEndMablagh.Location = new System.Drawing.Point(681, 20);
            this.txtEndMablagh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtEndMablagh.Name = "txtEndMablagh";
            this.txtEndMablagh.PreventEnterBeep = true;
            this.txtEndMablagh.Size = new System.Drawing.Size(137, 28);
            this.txtEndMablagh.TabIndex = 45;
            this.txtEndMablagh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEndMablagh_KeyDown);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(827, 12);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(87, 46);
            this.labelX7.TabIndex = 44;
            this.labelX7.Text = "تا مبلغ :";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtStartMablagh
            // 
            this.TxtStartMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtStartMablagh.Border.Class = "TextBoxBorder";
            this.TxtStartMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtStartMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtStartMablagh.ForeColor = System.Drawing.Color.Black;
            this.TxtStartMablagh.Location = new System.Drawing.Point(920, 20);
            this.TxtStartMablagh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TxtStartMablagh.Name = "TxtStartMablagh";
            this.TxtStartMablagh.PreventEnterBeep = true;
            this.TxtStartMablagh.Size = new System.Drawing.Size(137, 28);
            this.TxtStartMablagh.TabIndex = 43;
            this.TxtStartMablagh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtStartMablagh_KeyDown);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(1065, 12);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX6.Name = "labelX6";
            this.labelX6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX6.Size = new System.Drawing.Size(87, 46);
            this.labelX6.TabIndex = 42;
            this.labelX6.Text = "از مبلغ :";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(475, 22);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(189, 25);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 40;
            this.buttonX2.Text = "نمایش اطلاعات";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_2);
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel7;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "براساس مبلغ قبض";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.groupPanel2);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(1215, 152);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.superTabItem3;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Controls.Add(this.TxtSharjDoreh);
            this.groupPanel2.Controls.Add(this.labelX12);
            this.groupPanel2.Controls.Add(this.TxtAbDoreh);
            this.groupPanel2.Controls.Add(this.labelX13);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(13, 15);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 66;
            this.groupPanel2.Text = "جستجوی درآمدها بر اساس دوره های آب و شارژ";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(456, 48);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(409, 46);
            this.labelX2.TabIndex = 48;
            this.labelX2.Text = "برای درج چندین دوره : جدا کردن دوره ها با کاما مثال  1,3,5";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(863, 48);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX11.Name = "labelX11";
            this.labelX11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX11.Size = new System.Drawing.Size(228, 46);
            this.labelX11.TabIndex = 46;
            this.labelX11.Text = "برای مشاهده کلیه دوره ها : عدد صفر";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtSharjDoreh
            // 
            this.TxtSharjDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtSharjDoreh.Border.Class = "TextBoxBorder";
            this.TxtSharjDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSharjDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtSharjDoreh.ForeColor = System.Drawing.Color.Black;
            this.TxtSharjDoreh.Location = new System.Drawing.Point(300, 20);
            this.TxtSharjDoreh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TxtSharjDoreh.Name = "TxtSharjDoreh";
            this.TxtSharjDoreh.PreventEnterBeep = true;
            this.TxtSharjDoreh.Size = new System.Drawing.Size(259, 28);
            this.TxtSharjDoreh.TabIndex = 45;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(547, 8);
            this.labelX12.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX12.Name = "labelX12";
            this.labelX12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX12.Size = new System.Drawing.Size(193, 46);
            this.labelX12.TabIndex = 44;
            this.labelX12.Text = "دوره های شارژ با کدهای :";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtAbDoreh
            // 
            this.TxtAbDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtAbDoreh.Border.Class = "TextBoxBorder";
            this.TxtAbDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtAbDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtAbDoreh.ForeColor = System.Drawing.Color.Black;
            this.TxtAbDoreh.Location = new System.Drawing.Point(748, 20);
            this.TxtAbDoreh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TxtAbDoreh.Name = "TxtAbDoreh";
            this.TxtAbDoreh.PreventEnterBeep = true;
            this.TxtAbDoreh.Size = new System.Drawing.Size(259, 28);
            this.TxtAbDoreh.TabIndex = 43;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(992, 13);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX13.Name = "labelX13";
            this.labelX13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX13.Size = new System.Drawing.Size(171, 46);
            this.labelX13.TabIndex = 42;
            this.labelX13.Text = "دوره های آب با کدهای :";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(4, 20);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(267, 27);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 40;
            this.buttonX3.Text = "نمایش اطلاعات مربوط به دوره های وارد شده";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_1);
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel3;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "براساس دوره مربوطه";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.groupPanel1);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(1215, 152);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtGhabzcode);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.BtnFindByCode);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(13, 15);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 64;
            this.groupPanel1.Text = "براساس کد قبض";
            // 
            // txtGhabzcode
            // 
            this.txtGhabzcode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGhabzcode.Border.Class = "TextBoxBorder";
            this.txtGhabzcode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGhabzcode.DisabledBackColor = System.Drawing.Color.White;
            this.txtGhabzcode.ForeColor = System.Drawing.Color.Black;
            this.txtGhabzcode.Location = new System.Drawing.Point(861, 20);
            this.txtGhabzcode.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtGhabzcode.Name = "txtGhabzcode";
            this.txtGhabzcode.PreventEnterBeep = true;
            this.txtGhabzcode.Size = new System.Drawing.Size(196, 28);
            this.txtGhabzcode.TabIndex = 43;
            this.txtGhabzcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGhabzcode_KeyDown);
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(1051, 12);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX14.Name = "labelX14";
            this.labelX14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX14.Size = new System.Drawing.Size(81, 46);
            this.labelX14.TabIndex = 42;
            this.labelX14.Text = "کد قبض:";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // BtnFindByCode
            // 
            this.BtnFindByCode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnFindByCode.BackColor = System.Drawing.Color.Transparent;
            this.BtnFindByCode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnFindByCode.Location = new System.Drawing.Point(664, 22);
            this.BtnFindByCode.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.BtnFindByCode.Name = "BtnFindByCode";
            this.BtnFindByCode.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.BtnFindByCode.Size = new System.Drawing.Size(189, 25);
            this.BtnFindByCode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnFindByCode.Symbol = "";
            this.BtnFindByCode.SymbolColor = System.Drawing.Color.Green;
            this.BtnFindByCode.SymbolSize = 9F;
            this.BtnFindByCode.TabIndex = 40;
            this.BtnFindByCode.Text = "نمایش اطلاعات";
            this.BtnFindByCode.Click += new System.EventHandler(this.BtnFindByCode_Click);
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "براساس کد قبض";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.groupPanel4);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(1215, 152);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.txt_end);
            this.groupPanel4.Controls.Add(this.labelX5);
            this.groupPanel4.Controls.Add(this.txt_start);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.BtnFindByTarikh);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(13, 15);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 66;
            this.groupPanel4.Text = "بر اساس بازه تاریخی";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(707, 20);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(148, 24);
            this.txt_end.TabIndex = 71;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(824, 16);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(89, 32);
            this.labelX5.TabIndex = 72;
            this.labelX5.Text = "تا تاریخ :";
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(943, 20);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(148, 24);
            this.txt_start.TabIndex = 69;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(1060, 16);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(89, 32);
            this.labelX9.TabIndex = 70;
            this.labelX9.Text = "از تاریخ :";
            // 
            // BtnFindByTarikh
            // 
            this.BtnFindByTarikh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnFindByTarikh.BackColor = System.Drawing.Color.Transparent;
            this.BtnFindByTarikh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnFindByTarikh.Location = new System.Drawing.Point(456, 19);
            this.BtnFindByTarikh.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.BtnFindByTarikh.Name = "BtnFindByTarikh";
            this.BtnFindByTarikh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.BtnFindByTarikh.Size = new System.Drawing.Size(189, 27);
            this.BtnFindByTarikh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnFindByTarikh.Symbol = "";
            this.BtnFindByTarikh.SymbolColor = System.Drawing.Color.Green;
            this.BtnFindByTarikh.SymbolSize = 9F;
            this.BtnFindByTarikh.TabIndex = 40;
            this.BtnFindByTarikh.Text = "نمایش اطلاعات ";
            this.BtnFindByTarikh.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "بر اساس بازه زمانی";
            // 
            // superTabControlPanel6
            // 
            this.superTabControlPanel6.Controls.Add(this.groupPanel6);
            this.superTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel6.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.superTabControlPanel6.Name = "superTabControlPanel6";
            this.superTabControlPanel6.Size = new System.Drawing.Size(1215, 152);
            this.superTabControlPanel6.TabIndex = 0;
            this.superTabControlPanel6.TabItem = this.TabMoshtarek;
            // 
            // groupPanel6
            // 
            this.groupPanel6.BackColor = System.Drawing.Color.White;
            this.groupPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel6.Controls.Add(this.txtFindGharardad);
            this.groupPanel6.Controls.Add(this.labelX1);
            this.groupPanel6.Controls.Add(this.buttonX1);
            this.groupPanel6.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel6.Location = new System.Drawing.Point(13, 16);
            this.groupPanel6.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel6.Name = "groupPanel6";
            this.groupPanel6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel6.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel6.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel6.Style.BackColorGradientAngle = 90;
            this.groupPanel6.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel6.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderBottomWidth = 1;
            this.groupPanel6.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel6.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderLeftWidth = 1;
            this.groupPanel6.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderRightWidth = 1;
            this.groupPanel6.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderTopWidth = 1;
            this.groupPanel6.Style.CornerDiameter = 4;
            this.groupPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel6.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel6.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel6.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel6.TabIndex = 65;
            this.groupPanel6.Text = "جستجوی اطلاعات بر اساس مشترک";
            // 
            // txtFindGharardad
            // 
            this.txtFindGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFindGharardad.Border.Class = "TextBoxBorder";
            this.txtFindGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFindGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtFindGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtFindGharardad.Location = new System.Drawing.Point(861, 20);
            this.txtFindGharardad.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtFindGharardad.Name = "txtFindGharardad";
            this.txtFindGharardad.PreventEnterBeep = true;
            this.txtFindGharardad.Size = new System.Drawing.Size(196, 28);
            this.txtFindGharardad.TabIndex = 43;
            this.txtFindGharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFindGharardad_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(1065, 12);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(87, 46);
            this.labelX1.TabIndex = 42;
            this.labelX1.Text = "شماره قرارداد :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(664, 22);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(189, 25);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 40;
            this.buttonX1.Text = "نمایش اطلاعات";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // TabMoshtarek
            // 
            this.TabMoshtarek.AttachedControl = this.superTabControlPanel6;
            this.TabMoshtarek.GlobalItem = false;
            this.TabMoshtarek.Name = "TabMoshtarek";
            this.TabMoshtarek.Text = "براساس کد مشترک";
            // 
            // ab_ghabzTableAdapter
            // 
            this.ab_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // sharj_ghabzTableAdapter1
            // 
            this.sharj_ghabzTableAdapter1.ClearBeforeFill = true;
            // 
            // FrmGhabzRpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 659);
            this.Controls.Add(this.GrpAddin);
            this.Controls.Add(this.GrpGhobooz);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmGhabzRpt";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmGhabzRpt_Load);
            this.GrpAddin.ResumeLayout(false);
            this.GrpGhobooz.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).EndInit();
            this.superTabControl2.ResumeLayout(false);
            this.superTabControlPanel4.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_ghabz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            this.superTabControlPanel5.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Sharj_ghabz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel7.ResumeLayout(false);
            this.groupPanel8.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.superTabControlPanel6.ResumeLayout(false);
            this.groupPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel GrpAddin;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChPShodeh;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChPNot;
        private DevComponents.DotNetBar.Controls.CheckBoxX ChPJozee;
        private DevComponents.DotNetBar.Controls.GroupPanel GrpGhobooz;
        private DevComponents.DotNetBar.SuperTabControl superTabControl2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_ab_ghabz;
        private DevComponents.DotNetBar.SuperTabItem TabAb;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private DevComponents.DotNetBar.SuperTabItem TabSharj;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private DevComponents.DotNetBar.LabelX labelX5;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX BtnFindByTarikh;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX BtnFindByCode;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSharjDoreh;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtAbDoreh;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGhabzcode;
        private DevComponents.DotNetBar.LabelX labelX14;
        private AbDataset abDataset;
        private System.Windows.Forms.BindingSource abghabzBindingSource;
        private AbDatasetTableAdapters.ab_ghabzTableAdapter ab_ghabzTableAdapter;
        private System.Windows.Forms.BindingSource sharjghabzBindingSource;
        private MainDataSest mainDataSest;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_Sharj_ghabz;
        private MainDataSestTableAdapters.sharj_ghabzTableAdapter sharj_ghabzTableAdapter1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel6;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFindGharardad;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.SuperTabItem TabMoshtarek;
        private DevComponents.DotNetBar.ButtonX btnNemoodarSharj;
        private DevComponents.DotNetBar.ButtonX BtnNemoodarAb;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel7;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtEndMablagh;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtStartMablagh;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn1;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewFADateTimePickerColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn metraj;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ensheab;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf_gheir_mojaz;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf_mojaz;
        private System.Windows.Forms.DataGridViewTextBoxColumn fazelab;
        private System.Windows.Forms.DataGridViewTextBoxColumn jarimeh;
        private System.Windows.Forms.DataGridViewTextBoxColumn sayer;
        private System.Windows.Forms.DataGridViewTextBoxColumn maliat;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablagh;
        private System.Windows.Forms.DataGridViewTextBoxColumn mande;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
    }
}