﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.ManagementReports
{
    public partial class FrmTarefeRpt : MyMetroForm
    {
        public FrmTarefeRpt()
        {
            InitializeComponent();
        }

        private void FrmTarefeRpt_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.sharj_mablagh' table. You can move, or remove it, as needed.
            this.sharj_mablaghTableAdapter.Fill(this.mainDataSest.sharj_mablagh);
            // TODO: This line of code loads data into the 'mainDataSest.sharj_doreh' table. You can move, or remove it, as needed.
            this.sharj_dorehTableAdapter.Fill(this.mainDataSest.sharj_doreh);
            // TODO: This line of code loads data into the 'mainDataSest.ab_mablagh' table. You can move, or remove it, as needed.
            this.ab_mablaghTableAdapter.Fill(this.mainDataSest.ab_mablagh);
            // TODO: This line of code loads data into the 'abDataset.ab_doreh' table. You can move, or remove it, as needed.
            this.ab_dorehTableAdapter.Fill(this.abDataset.ab_doreh);
            // TODO: This line of code loads data into the 'mainDataSest.barge_khorooj' table. You can move, or remove it, as needed.

            chrMaliatAb.DataSource = Classes.ClsMain.GetDataTable("select top(5) * from ab_doreh order by dcode desc");
            chrMaliatSharj.DataSource = Classes.ClsMain.GetDataTable("select top(5) * from sharj_doreh order by dcode desc");
        }

        private void RefreshTarefeAbGridView()
        {
            dataGridView1.Visible  = radShowGrid.Checked;
            dataGridView2.Visible  = !radShowGrid.Checked;
            if (radShowGrid.Checked)
            {
                chrAbMablaghPerEjra.DataSource = abDataset.ab_doreh;
                chrAbMablaghPerEjra.Series[0].XValueMember = "dcode";
                chrAbMablaghPerEjra.Series[0].YValueMembers = "mablagh_ab";
            }
            else
            {
                chrAbMablaghPerEjra.DataSource = mainDataSest.ab_mablagh;
                chrAbMablaghPerEjra.Series[0].XValueMember = "tarikh_ejra";
                chrAbMablaghPerEjra.Series[0].YValueMembers = "mablagh";
            }
        }

        private void rad_Click(object sender, EventArgs e)
        {
            pnlAbCharts.Visible = pnlAb.Visible;
            pnlAb.Visible = !pnlAb.Visible;
        }

        private void radChangeChart_Click(object sender, EventArgs e)
        {
            bool flag = radDoreh.Checked;
            {
                chrAbMablaghPerDoreh.Visible = flag;
                chrAbMablaghPerEjra.Visible = !flag;
            }
        }

        private void radSharj_Click(object sender, EventArgs e)
        {
            pnlSharjCharts.Visible = pnlSharj.Visible;
            pnlSharj.Visible = !pnlSharj.Visible;
        }

        private void radSharjChangeChart_Click(object sender, EventArgs e)
        {
            bool flag = radSharjDoreh.Checked;
            chrSharjPerDoreh.Visible = flag;
            chrSharjPerEjra.Visible = !flag;
        }
    }
}