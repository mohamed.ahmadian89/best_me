using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
using CrystalDecisions.Windows.Forms;
namespace CWMS.ManagementReports
{
    public partial class FrmBedehkarBestankar : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ChapDoreh = 0;
        public FrmBedehkarBestankar()
        {
            InitializeComponent();
        }

        private void FrmBedehkarBestankar_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reportDataSet2.moshtarek_ab' table. You can move, or remove it, as needed.
            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select dcode from ab_doreh");
            for (int i = 0; i < dt_doreh.Rows.Count; i++)
            {
                cmbDoreh.Items.Add(dt_doreh.Rows[i][0].ToString());
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            //this.moshtarek_abTableAdapter.Fill(this.reportDataSet2.moshtarek_ab);
            CalcSum();
            ChapDoreh = 0;
        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            //this.moshtarek_abTableAdapter.FillByDoreh(this.reportDataSet2.moshtarek_ab, Convert.ToInt32(cmbDoreh.SelectedItem));
            CalcSum();
            ChapDoreh = Convert.ToInt32(cmbDoreh.SelectedItem);

        }
        void CalcSum()
        {
            BedehkarBindginSources.Filter = "mande<>0";
            BestankarBindingSource.Filter = "bestankari<>0";
            txtSumBedehkar.Text = reportDataSet2.moshtarek_ab.Select(p => p.mande).Sum().ToString();
            txtSumBestankar.Text = reportDataSet2.moshtarek_ab.Select(p => p.bestankari).Sum().ToString();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (dgvbestankar.Rows.Count != 0)
            {
                CrystalReportViewer repVUer = new CrystalReportViewer();
                CrysReports.Ab_Bestankar rpt = new CrysReports.Ab_Bestankar();

                Classes.clsAb.Ab_moshtarek_bestankar(repVUer, rpt, ChapDoreh);
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.Ab_Bedehkaran rpt = new CrysReports.Ab_Bedehkaran();
            int from = -1, to = -1;
            try
            {
                from = Convert.ToInt32(txtBedFrom.Text);
            }
            catch (Exception)
            {
                from = -1;
            }

            try
            {
                to = Convert.ToInt32(txtBedTo.Text);
            }
            catch (Exception)
            {
                to = -1;
            }
            Classes.clsAb.Ab_moshtarek_bedehkar(repVUer, rpt, ChapDoreh,from,to);
        }
    }
}