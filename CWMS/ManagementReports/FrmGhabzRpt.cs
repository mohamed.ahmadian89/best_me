﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;
using CrystalDecisions.Windows.Forms;

namespace CWMS.ManagementReports
{
    public partial class FrmGhabzRpt : MyMetroForm
    {
        public FrmGhabzRpt()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }

        private void FrmGhabzRpt_Load(object sender, EventArgs e)
        {
            txt_start.SelectedDateTime = txt_end.SelectedDateTime = DateTime.Now;
            // TODO: This line of code loads data into the 'abDataset.ab_ghabz' table. You can move, or remove it, as needed.
            this.ab_ghabzTableAdapter.Fill(this.abDataset.ab_ghabz);
            txt_start.SelectedDateTime = txt_end.SelectedDateTime = DateTime.Now.Date;

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            DateTime last=Convert.ToDateTime(txt_end.SelectedDateTime);
            ab_ghabzTableAdapter.FillByTarikh(abDataset.ab_ghabz, Convert.ToDateTime(txt_start.SelectedDateTime).Date,new DateTime(last.Year,last.Month,last.Day,23,59,59));
            sharj_ghabzTableAdapter1.FillByTarikh(mainDataSest.sharj_ghabz, Convert.ToDateTime(txt_start.SelectedDateTime).Date, new DateTime(last.Year, last.Month, last.Day, 23, 59, 59));

      
            ChPJozee.Checked = ChPNot.Checked = ChPShodeh.Checked = true;
        }

 




        string Filter = "";

        void MakeFilter()
        {
            string Part1 = "", Part2 = "";
            if (ChPShodeh.Checked)
                Part1 += "vaziat=1 or ";
            if (ChPNot.Checked)
                Part1 += "vaziat=0 or ";
            if (ChPJozee.Checked)
                Part1 += "vaziat=2 or ";

            if (Part1 != "")
                Part1 = Part1.Substring(0, Part1.Length - 3);
            else
                Part1 = "vaziat=-1";

            //DataView dv = new DataView((DataTable) dgv_ab_ghabz.DataSource);
            //dv.RowFilter = Part1;
            //dgv_ab_ghabz.DataSource = dv.ToTable();
            //dv.Table =(DataTable) dgv_Sharj_ghabz.DataSource;
            //dv.RowFilter = Part1;
            //dgv_Sharj_ghabz.DataSource = dv.ToTable();
            abghabzBindingSource.Filter = sharjghabzBindingSource.Filter = Part1;

 
        }




        private void RdAl_CheckedChanged(object sender, EventArgs e)
        {


        }

        private void Btnfindgharadad_Click(object sender, EventArgs e)
        {

        }

        private void ChPShodeh_CheckedChanged(object sender, EventArgs e)
        {
            MakeFilter();
        }

        private void ChPJozee_CheckedChanged(object sender, EventArgs e)
        {
            MakeFilter();

        }

        private void ChPNot_CheckedChanged(object sender, EventArgs e)
        {
            MakeFilter();

        }

        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {
            //if (superTabControl1.SelectedTabIndex == 1)
            //{
            //    GrpAddin.Visible = false;
            //    GrpGhobooz.Location = new Point(GrpGhobooz.Location.X, 160);
            //    this.Height = 540;
            //}
            //else
            //{
            //    GrpAddin.Visible = true;
            //    GrpGhobooz.Location = new Point(GrpGhobooz.Location.X, 210);
            //    this.Height = 680;

            //}

            if (superTabControl1.SelectedTabIndex == 3)
            {
                btnNemoodarSharj.Visible = BtnNemoodarAb.Visible = true;
            }
            else
                btnNemoodarSharj.Visible = BtnNemoodarAb.Visible = false;

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {

        }

        private void BtnFindByCode_Click(object sender, EventArgs e)
        {
            if (txtGhabzcode.Text.Trim() != "")
                if (CheckGhabzType(txtGhabzcode.Text) == "ab")
                {
                    ab_ghabzTableAdapter.FillByGcode(abDataset.ab_ghabz, Convert.ToInt32(txtGhabzcode.Text));
                    superTabControl2.SelectedTabIndex = 0;
                }
                else
                {
                    sharj_ghabzTableAdapter1.FillByGcode(mainDataSest.sharj_ghabz, Convert.ToInt32(txtGhabzcode.Text));
                    superTabControl2.SelectedTabIndex = 1;

                }
        }
        string CheckGhabzType(string Gcode)
        {
            if (Gcode.StartsWith("1"))
                return "ab";
            return "sharj";
        }




        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            ChPJozee.Checked = ChPNot.Checked = ChPShodeh.Checked = true;
            if (TxtAbDoreh.Text.Trim() == "0")
                ab_ghabzTableAdapter.Fill(abDataset.ab_ghabz);
            else
            {

                //--------------- One Dcode-----------------------
                try
                {
                    Convert.ToInt32(TxtAbDoreh.Text);
                    ab_ghabzTableAdapter.FillByDCode(abDataset.ab_ghabz, Convert.ToInt32(TxtAbDoreh.Text));

                }
                catch (Exception)
                {

                }
                //--------------- One Dcode-----------------------




                if (TxtAbDoreh.Text.Contains(","))
                {
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Classes.ClsMain.ConnectionStr, "select * from ab_ghabz where dcode in (" + TxtAbDoreh.Text + ") order by dcode desc");
                        da.Fill(abDataset.ab_ghabz);
                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا دوره های آب را به درستی  وارد نمایید");
                        TxtAbDoreh.Focus();
                        TxtAbDoreh.SelectAll();
                    }

                    
                }
            }















            //----------------------------- Sharj Part-----------------------------
            if (TxtSharjDoreh.Text.Trim() == "0")
                sharj_ghabzTableAdapter1.Fill(mainDataSest.sharj_ghabz);

            else
            {


                //--------------- One Dcode-----------------------
                try
                {
                    Convert.ToInt32(TxtSharjDoreh.Text);
                    sharj_ghabzTableAdapter1.FillByDoreh(mainDataSest.sharj_ghabz, Convert.ToInt32(TxtSharjDoreh.Text));

                }
                catch (Exception)
                {

                }
                //--------------- One Dcode-----------------------

                if (TxtSharjDoreh.Text.Contains(","))
                {
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter(Classes.ClsMain.ConnectionStr, "select * from sharj_ghabz where dcode in (" + TxtSharjDoreh.Text + ") order by dcode desc");
                        da.Fill(mainDataSest.sharj_ghabz);
                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا دوره های شارژ را به درستی وارد نمایید");
                        TxtSharjDoreh.SelectAll();
                        TxtSharjDoreh.Focus();
                    }
                  
                }

            }



        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            //DataTable dt=Classes.ClsMain.GetDataTable("select sum(");
            //ManagementReports.FrmDarAmadMaliatDigram frm = new FrmDarAmadMaliatDigram()
            //frm.ShowDialog();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if(txtFindGharardad.Text.Trim()!="")
            {
                ChPJozee.Checked = ChPNot.Checked = ChPShodeh.Checked = true;

                sharj_ghabzTableAdapter1.FillByGharardad(mainDataSest.sharj_ghabz, Convert.ToInt32(txtFindGharardad.Text));
                ab_ghabzTableAdapter.FillByGharardad(abDataset.ab_ghabz, Convert.ToInt32(txtFindGharardad.Text));
 
            
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            ManagementReports.FrmDarAmadMaliatDigram frm = new FrmDarAmadMaliatDigram(mainDataSest.sharj_ghabz, "قبوض شارژ مشترک", " قبوض شارژ مشترک به تفکیک دوره", "dcode", "mablaghkol");
            frm.ShowDialog();
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            ManagementReports.FrmDarAmadMaliatDigram frm = new FrmDarAmadMaliatDigram(abDataset.ab_ghabz, "قبوض آب مشترک", "قبوض آب مشترک به تفکیک دوره", "dcode", "mablaghkol");
            frm.ShowDialog();
        }

        private void dgv_Sharj_ghabz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_Sharj_ghabz.SelectedRows.Count != 0)
            {
                Frm_sharj_details frm = new Frm_sharj_details(dgv_Sharj_ghabz.SelectedRows[0].Cells[0].Value.ToString(), dgv_Sharj_ghabz.SelectedRows[0].Cells[2].Value.ToString());
                frm.ShowDialog();
            }

        }

        private void dgv_ab_ghabz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_ab_ghabz.SelectedRows.Count != 0)
            {
                Ab.Frm_ab_details frm = new Ab.Frm_ab_details(dgv_ab_ghabz.SelectedRows[0].Cells[0].Value.ToString(), dgv_ab_ghabz.SelectedRows[0].Cells[2].Value.ToString());
                frm.ShowDialog();
            }
        }

        private void buttonX2_Click_2(object sender, EventArgs e)
        {
            if (TxtStartMablagh.Text == "")
                TxtStartMablagh.Text = "0";
            if (txtEndMablagh.Text == "")
                Payam.Show("لطفا حداکثر مبلغ قبض را وارد نمایید");
            else
            {
                sharj_ghabzTableAdapter1.FillByMablagh(mainDataSest.sharj_ghabz, Convert.ToInt64(TxtStartMablagh.Text), Convert.ToInt64(txtEndMablagh.Text));
                ab_ghabzTableAdapter.FillByMabalgh(abDataset.ab_ghabz, Convert.ToDouble(TxtStartMablagh.Text), Convert.ToDouble(txtEndMablagh.Text));

              

            }
        }

        private void buttonX4_Click_1(object sender, EventArgs e)
        {
            ManagementReports.FrmDarAmadMaliatDigram frm = new FrmDarAmadMaliatDigram(Classes.ClsMain.GetDataTable("    select dcode as 'کد دوره', MAX(mablaghkol) as 'بیشترین' from sharj_ghabz group by dcode"), " بیشترین مبلغ قبض شارژ", " بیشترین مبلغ قبض شارژ", "کد دوره","بیشترین");
            frm.ShowDialog();
       
        }

        private void txtFindGharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX1.PerformClick();
        }

        private void TxtStartMablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtEndMablagh.Focus();
        }

        private void txtEndMablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX2.PerformClick();

        }

        private void txtGhabzcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BtnFindByCode.PerformClick();

        }

        private void buttonX5_Click_1(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.rptRizGozareshSharjForAll rpt = new CrysReports.rptRizGozareshSharjForAll();

            Classes.clsSharj.RizHeasbsharjForAll(mainDataSest,
                repVUer,
                rpt
                );
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.rptRizGozareshAbForAll rpt = new CrysReports.rptRizGozareshAbForAll();
            AbDatasetTableAdapters.settingTableAdapter ta =
                        new AbDatasetTableAdapters.settingTableAdapter();
            ta.Fill(abDataset.setting);

            Classes.clsAb.RizHeasbAbForAll(
                abDataset,
                repVUer,
                rpt

                );
        }

        private void dgv_ab_ghabz_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                if (dgv_ab_ghabz.SelectedRows.Count != 0)
                {
                    Ab.Frm_ab_details frm = new Ab.Frm_ab_details(dgv_ab_ghabz.SelectedRows[0].Cells[0].Value.ToString(), dgv_ab_ghabz.SelectedRows[0].Cells[2].Value.ToString());
                    frm.ShowDialog();
                }
        }

        private void dgv_Sharj_ghabz_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                if (dgv_Sharj_ghabz.SelectedRows.Count != 0)
                {
                    Frm_sharj_details frm = new Frm_sharj_details(dgv_Sharj_ghabz.SelectedRows[0].Cells[0].Value.ToString(), dgv_Sharj_ghabz.SelectedRows[0].Cells[2].Value.ToString());
                    frm.ShowDialog();
                }
        }
    }
}