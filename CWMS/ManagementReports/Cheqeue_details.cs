﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.ManagementReports
{
    public partial class Cheqeue_details : MyMetroForm
    {
        string Id = "";
        public Cheqeue_details(string cheqeID)
        {
            Classes.ClsMain.ChangeCulture("f");
            Id = cheqeID;
            InitializeComponent();
            DataTable dt_chqeueInfo=Classes.ClsMain.GetDataTable("select * from ghabz_pardakht where id=" + cheqeID);
            if(dt_chqeueInfo.Rows.Count==0)
            {
                Payam.Show("اطلاعات چک در سیستم ثبت نشده است ");
                this.Close();
                return;
            }
            txtbank.Text = dt_chqeueInfo.Rows[0]["bank"].ToString();
            txtgharardad.Text = dt_chqeueInfo.Rows[0]["gharardad"].ToString();
            lbl_coname.Text = Classes.ClsMain.ExecuteScalar("select co_name from moshtarekin where gharardad=" + txtgharardad.Text).ToString();
            txtmablagh.Text = dt_chqeueInfo.Rows[0]["mablagh"].ToString();
            txttarikh_sabt.SelectedDateTime = Convert.ToDateTime(dt_chqeueInfo.Rows[0]["tarikh_sabt"].ToString());
            ttsarresid.SelectedDateTime = Convert.ToDateTime(dt_chqeueInfo.Rows[0]["tarikh_sarresid"].ToString());
            ttsarresid.Focus();
        }

        private void Cheqeue_details_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if(txtgharardad.Text.Trim()!="")
            {
                try
                {
                    lbl_coname.Text = Classes.ClsMain.ExecuteScalar("select co_name from moshtarekin where gharardad=" + txtgharardad.Text).ToString();
                }
                catch (Exception)
                {

                    lbl_coname.Text = "";
                    Payam.Show("لطفا قرارداد را به صورت صحیح وارد نمایید");
                }
            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            if(Cpayam.Show("ایا با ذخیره تغییرات موافق هستید؟")==DialogResult.Yes)
            {
                Classes.ClsMain.ChangeCulture("e");
                Classes.ClsMain.ExecuteNoneQuery("update ghabz_pardakht set mablagh=" + txtmablagh.Text +
                    " ,bank='" + txtbank.Text + "',gharardad=" + txtgharardad.Text + ",tarikh_vosool='" + ttVosool.SelectedDateTime + "'," +
                    "tarikh_sarresid='" + ttsarresid.SelectedDateTime + "',tozihat='" + txtTozihat.Text + "' where id=" + Id
                    );
                Payam.Show("اطلاعات با موفقیت در سیتم ذخیره شد ");
                Classes.ClsMain.ChangeCulture("f");
                this.Close();
            }
        }

        private void txttarikh_sabt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtmablagh.Focus();
        }

        private void txtmablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtbank.Focus();
        }

        private void txtbank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtgharardad.Focus();
        }

        private void txtgharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ttVosool.Focus();
        }

        private void ttVosool_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtTozihat.Focus();
        }

        private void txtTozihat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnsave.PerformClick();
        }
    }
}