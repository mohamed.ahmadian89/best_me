﻿namespace CWMS.ManagementReports
{
    partial class frmDaramadRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnAdvancedSearch = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dtpTo = new FarsiLibrary.Win.Controls.FADatePicker();
            this.dtpFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.btnAll = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblTahator = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblfishtotal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.sumPeriodic = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblPOSTotal = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCashTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblIntTotal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblChqTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.abTab = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.cmbSharj = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.sharjdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.cmbAb = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.abdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.chbSharj = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chbAb = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lblSumSh = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblJozyiSh = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblSayerSh = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblMablagh = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblJarimeSh = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblBedehiSh = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblMaliatSh = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblSumAb = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lblJozyi = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lblSayer = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lblAboonman = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblJarime = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lblBedehi = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMaliat = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblGhMojaz = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblMojaz = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.sumDoreh = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPOSDoreh = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCashDoreh = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblIntrDoreh = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblChqDoreh = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.sharjTab = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.ab_dorehTableAdapter = new CWMS.AbDatasetTableAdapters.ab_dorehTableAdapter();
            this.sharj_dorehTableAdapter = new CWMS.CWMSDataSetTableAdapters.sharj_dorehTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(12, 16);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(826, 743);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 2;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.abTab,
            this.sharjTab});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.groupPanel3);
            this.superTabControlPanel3.Controls.Add(this.btnAdvancedSearch);
            this.superTabControlPanel3.Controls.Add(this.labelX3);
            this.superTabControlPanel3.Controls.Add(this.labelX2);
            this.superTabControlPanel3.Controls.Add(this.dtpTo);
            this.superTabControlPanel3.Controls.Add(this.dtpFrom);
            this.superTabControlPanel3.Controls.Add(this.btnAll);
            this.superTabControlPanel3.Controls.Add(this.groupPanel1);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(826, 714);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.abTab;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.chart1);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(3, 64);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(534, 630);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 28;
            this.groupPanel3.Text = "نمودار سهم انواع پرداخت از درآمد کل";
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 4);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(522, 587);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // btnAdvancedSearch
            // 
            this.btnAdvancedSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdvancedSearch.BackColor = System.Drawing.Color.Transparent;
            this.btnAdvancedSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdvancedSearch.Location = new System.Drawing.Point(372, 20);
            this.btnAdvancedSearch.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAdvancedSearch.Name = "btnAdvancedSearch";
            this.btnAdvancedSearch.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdvancedSearch.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAdvancedSearch.Size = new System.Drawing.Size(39, 31);
            this.btnAdvancedSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAdvancedSearch.Symbol = "";
            this.btnAdvancedSearch.SymbolColor = System.Drawing.Color.Green;
            this.btnAdvancedSearch.SymbolSize = 12F;
            this.btnAdvancedSearch.TabIndex = 27;
            this.btnAdvancedSearch.Click += new System.EventHandler(this.btnAdvancedSearch_Click);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(767, 22);
            this.labelX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(36, 19);
            this.labelX3.TabIndex = 26;
            this.labelX3.Text = "ازتاریخ :";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(543, 22);
            this.labelX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(37, 19);
            this.labelX2.TabIndex = 25;
            this.labelX2.Text = "تا تاریخ :";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(417, 22);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(120, 26);
            this.dtpTo.TabIndex = 24;
            this.dtpTo.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(641, 22);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(120, 26);
            this.dtpFrom.TabIndex = 23;
            this.dtpFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // btnAll
            // 
            this.btnAll.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAll.BackColor = System.Drawing.Color.Transparent;
            this.btnAll.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAll.Location = new System.Drawing.Point(9, 20);
            this.btnAll.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAll.Name = "btnAll";
            this.btnAll.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAll.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAll.Size = new System.Drawing.Size(144, 31);
            this.btnAll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAll.Symbol = "";
            this.btnAll.SymbolColor = System.Drawing.Color.Green;
            this.btnAll.SymbolSize = 12F;
            this.btnAll.TabIndex = 14;
            this.btnAll.Text = "ماه اخیر";
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.lblTahator);
            this.groupPanel1.Controls.Add(this.label17);
            this.groupPanel1.Controls.Add(this.lblfishtotal);
            this.groupPanel1.Controls.Add(this.label8);
            this.groupPanel1.Controls.Add(this.sumPeriodic);
            this.groupPanel1.Controls.Add(this.label10);
            this.groupPanel1.Controls.Add(this.lblPOSTotal);
            this.groupPanel1.Controls.Add(this.label12);
            this.groupPanel1.Controls.Add(this.lblCashTotal);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Controls.Add(this.lblIntTotal);
            this.groupPanel1.Controls.Add(this.label4);
            this.groupPanel1.Controls.Add(this.lblChqTotal);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(543, 64);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(280, 630);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 6;
            this.groupPanel1.Text = "به تفکیک نوع پرداخت";
            // 
            // lblTahator
            // 
            this.lblTahator.BackColor = System.Drawing.Color.Transparent;
            this.lblTahator.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblTahator.ForeColor = System.Drawing.Color.Black;
            this.lblTahator.Location = new System.Drawing.Point(3, 237);
            this.lblTahator.Name = "lblTahator";
            this.lblTahator.Size = new System.Drawing.Size(154, 25);
            this.lblTahator.TabIndex = 13;
            this.lblTahator.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(163, 237);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 21);
            this.label17.TabIndex = 12;
            this.label17.Text = "تهاتر:";
            // 
            // lblfishtotal
            // 
            this.lblfishtotal.BackColor = System.Drawing.Color.Transparent;
            this.lblfishtotal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblfishtotal.ForeColor = System.Drawing.Color.Black;
            this.lblfishtotal.Location = new System.Drawing.Point(3, 200);
            this.lblfishtotal.Name = "lblfishtotal";
            this.lblfishtotal.Size = new System.Drawing.Size(154, 25);
            this.lblfishtotal.TabIndex = 11;
            this.lblfishtotal.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(163, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 21);
            this.label8.TabIndex = 10;
            this.label8.Text = "فیش :";
            // 
            // sumPeriodic
            // 
            this.sumPeriodic.BackColor = System.Drawing.Color.Transparent;
            this.sumPeriodic.Font = new System.Drawing.Font("B Yekan", 10F);
            this.sumPeriodic.ForeColor = System.Drawing.Color.Black;
            this.sumPeriodic.Location = new System.Drawing.Point(3, 283);
            this.sumPeriodic.Name = "sumPeriodic";
            this.sumPeriodic.Size = new System.Drawing.Size(154, 25);
            this.sumPeriodic.TabIndex = 9;
            this.sumPeriodic.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(163, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 21);
            this.label10.TabIndex = 8;
            this.label10.Text = "مجموع :";
            // 
            // lblPOSTotal
            // 
            this.lblPOSTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblPOSTotal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblPOSTotal.ForeColor = System.Drawing.Color.Black;
            this.lblPOSTotal.Location = new System.Drawing.Point(3, 163);
            this.lblPOSTotal.Name = "lblPOSTotal";
            this.lblPOSTotal.Size = new System.Drawing.Size(154, 25);
            this.lblPOSTotal.TabIndex = 7;
            this.lblPOSTotal.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(163, 163);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 21);
            this.label12.TabIndex = 6;
            this.label12.Text = "پوز :";
            // 
            // lblCashTotal
            // 
            this.lblCashTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblCashTotal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblCashTotal.ForeColor = System.Drawing.Color.Black;
            this.lblCashTotal.Location = new System.Drawing.Point(3, 118);
            this.lblCashTotal.Name = "lblCashTotal";
            this.lblCashTotal.Size = new System.Drawing.Size(154, 25);
            this.lblCashTotal.TabIndex = 5;
            this.lblCashTotal.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(163, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 21);
            this.label6.TabIndex = 4;
            this.label6.Text = "واریز نقدی :";
            // 
            // lblIntTotal
            // 
            this.lblIntTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblIntTotal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblIntTotal.ForeColor = System.Drawing.Color.Black;
            this.lblIntTotal.Location = new System.Drawing.Point(3, 72);
            this.lblIntTotal.Name = "lblIntTotal";
            this.lblIntTotal.Size = new System.Drawing.Size(154, 25);
            this.lblIntTotal.TabIndex = 3;
            this.lblIntTotal.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(150, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 21);
            this.label4.TabIndex = 2;
            this.label4.Text = "پرداخت اینترنتی :";
            // 
            // lblChqTotal
            // 
            this.lblChqTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblChqTotal.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblChqTotal.ForeColor = System.Drawing.Color.Black;
            this.lblChqTotal.Location = new System.Drawing.Point(3, 26);
            this.lblChqTotal.Name = "lblChqTotal";
            this.lblChqTotal.Size = new System.Drawing.Size(154, 25);
            this.lblChqTotal.TabIndex = 1;
            this.lblChqTotal.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(163, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "چک :";
            // 
            // abTab
            // 
            this.abTab.AttachedControl = this.superTabControlPanel3;
            this.abTab.GlobalItem = false;
            this.abTab.Name = "abTab";
            this.abTab.Text = "بازه زمانی";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.buttonX1);
            this.superTabControlPanel1.Controls.Add(this.cmbSharj);
            this.superTabControlPanel1.Controls.Add(this.cmbAb);
            this.superTabControlPanel1.Controls.Add(this.chbSharj);
            this.superTabControlPanel1.Controls.Add(this.chbAb);
            this.superTabControlPanel1.Controls.Add(this.groupPanel2);
            this.superTabControlPanel1.Controls.Add(this.groupPanel4);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(826, 714);
            this.superTabControlPanel1.TabIndex = 0;
            this.superTabControlPanel1.TabItem = this.sharjTab;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(267, 20);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Size = new System.Drawing.Size(133, 31);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 4;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // cmbSharj
            // 
            this.cmbSharj.DataSource = this.sharjdorehBindingSource;
            this.cmbSharj.DisplayMember = "dcode";
            this.cmbSharj.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbSharj.ForeColor = System.Drawing.Color.Black;
            this.cmbSharj.FormattingEnabled = true;
            this.cmbSharj.ItemHeight = 18;
            this.cmbSharj.Location = new System.Drawing.Point(406, 18);
            this.cmbSharj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbSharj.Name = "cmbSharj";
            this.cmbSharj.Size = new System.Drawing.Size(121, 24);
            this.cmbSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbSharj.TabIndex = 3;
            this.cmbSharj.ValueMember = "dcode";
            this.cmbSharj.Click += new System.EventHandler(this.cmbSharj_Click);
            // 
            // sharjdorehBindingSource
            // 
            this.sharjdorehBindingSource.DataMember = "sharj_doreh";
            this.sharjdorehBindingSource.DataSource = this.cWMSDataSet;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cmbAb
            // 
            this.cmbAb.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.abdorehBindingSource, "dcode", true));
            this.cmbAb.DataSource = this.abdorehBindingSource;
            this.cmbAb.DisplayMember = "dcode";
            this.cmbAb.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbAb.ForeColor = System.Drawing.Color.Black;
            this.cmbAb.FormattingEnabled = true;
            this.cmbAb.ItemHeight = 18;
            this.cmbAb.Location = new System.Drawing.Point(621, 18);
            this.cmbAb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbAb.Name = "cmbAb";
            this.cmbAb.Size = new System.Drawing.Size(121, 24);
            this.cmbAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbAb.TabIndex = 1;
            this.cmbAb.ValueMember = "dcode";
            this.cmbAb.Click += new System.EventHandler(this.cmbAb_Click);
            // 
            // abdorehBindingSource
            // 
            this.abdorehBindingSource.DataMember = "ab_doreh";
            this.abdorehBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chbSharj
            // 
            this.chbSharj.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chbSharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chbSharj.ForeColor = System.Drawing.Color.Black;
            this.chbSharj.Location = new System.Drawing.Point(533, 21);
            this.chbSharj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chbSharj.Name = "chbSharj";
            this.chbSharj.Size = new System.Drawing.Size(72, 30);
            this.chbSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chbSharj.TabIndex = 2;
            this.chbSharj.Text = "دوره شارژ";
            // 
            // chbAb
            // 
            this.chbAb.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chbAb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chbAb.ForeColor = System.Drawing.Color.Black;
            this.chbAb.Location = new System.Drawing.Point(741, 21);
            this.chbAb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chbAb.Name = "chbAb";
            this.chbAb.Size = new System.Drawing.Size(72, 30);
            this.chbAb.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chbAb.TabIndex = 0;
            this.chbAb.Text = "دوره آب";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.lblTotal);
            this.groupPanel2.Controls.Add(this.label33);
            this.groupPanel2.Controls.Add(this.label30);
            this.groupPanel2.Controls.Add(this.label29);
            this.groupPanel2.Controls.Add(this.lblSumSh);
            this.groupPanel2.Controls.Add(this.label5);
            this.groupPanel2.Controls.Add(this.lblJozyiSh);
            this.groupPanel2.Controls.Add(this.label13);
            this.groupPanel2.Controls.Add(this.lblSayerSh);
            this.groupPanel2.Controls.Add(this.label19);
            this.groupPanel2.Controls.Add(this.lblMablagh);
            this.groupPanel2.Controls.Add(this.label22);
            this.groupPanel2.Controls.Add(this.lblJarimeSh);
            this.groupPanel2.Controls.Add(this.label24);
            this.groupPanel2.Controls.Add(this.lblBedehiSh);
            this.groupPanel2.Controls.Add(this.label26);
            this.groupPanel2.Controls.Add(this.lblMaliatSh);
            this.groupPanel2.Controls.Add(this.label28);
            this.groupPanel2.Controls.Add(this.lblSumAb);
            this.groupPanel2.Controls.Add(this.label32);
            this.groupPanel2.Controls.Add(this.lblJozyi);
            this.groupPanel2.Controls.Add(this.label34);
            this.groupPanel2.Controls.Add(this.lblSayer);
            this.groupPanel2.Controls.Add(this.label36);
            this.groupPanel2.Controls.Add(this.lblAboonman);
            this.groupPanel2.Controls.Add(this.label38);
            this.groupPanel2.Controls.Add(this.lblJarime);
            this.groupPanel2.Controls.Add(this.label40);
            this.groupPanel2.Controls.Add(this.lblBedehi);
            this.groupPanel2.Controls.Add(this.label9);
            this.groupPanel2.Controls.Add(this.lblMaliat);
            this.groupPanel2.Controls.Add(this.label15);
            this.groupPanel2.Controls.Add(this.lblGhMojaz);
            this.groupPanel2.Controls.Add(this.label18);
            this.groupPanel2.Controls.Add(this.lblMojaz);
            this.groupPanel2.Controls.Add(this.label20);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(3, 64);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(534, 630);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 6;
            this.groupPanel2.Text = "به تفکیک منبع درآمد";
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(3, 442);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(154, 25);
            this.lblTotal.TabIndex = 67;
            this.lblTotal.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(163, 442);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(47, 17);
            this.label33.TabIndex = 66;
            this.label33.Text = "جمع کل :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(7, 417);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(500, 17);
            this.label30.TabIndex = 65;
            this.label30.Text = "---------------------------------------------------------------------------------" +
    "-";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(7, 371);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(500, 17);
            this.label29.TabIndex = 64;
            this.label29.Text = "---------------------------------------------------------------------------------" +
    "-";
            // 
            // lblSumSh
            // 
            this.lblSumSh.BackColor = System.Drawing.Color.Transparent;
            this.lblSumSh.ForeColor = System.Drawing.Color.Black;
            this.lblSumSh.Location = new System.Drawing.Point(3, 392);
            this.lblSumSh.Name = "lblSumSh";
            this.lblSumSh.Size = new System.Drawing.Size(154, 25);
            this.lblSumSh.TabIndex = 63;
            this.lblSumSh.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(163, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 17);
            this.label5.TabIndex = 62;
            this.label5.Text = "مجموع :";
            // 
            // lblJozyiSh
            // 
            this.lblJozyiSh.BackColor = System.Drawing.Color.Transparent;
            this.lblJozyiSh.ForeColor = System.Drawing.Color.Black;
            this.lblJozyiSh.Location = new System.Drawing.Point(3, 255);
            this.lblJozyiSh.Name = "lblJozyiSh";
            this.lblJozyiSh.Size = new System.Drawing.Size(154, 25);
            this.lblJozyiSh.TabIndex = 61;
            this.lblJozyiSh.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(163, 255);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 17);
            this.label13.TabIndex = 60;
            this.label13.Text = "پرداخت جزئی :";
            // 
            // lblSayerSh
            // 
            this.lblSayerSh.BackColor = System.Drawing.Color.Transparent;
            this.lblSayerSh.ForeColor = System.Drawing.Color.Black;
            this.lblSayerSh.Location = new System.Drawing.Point(3, 209);
            this.lblSayerSh.Name = "lblSayerSh";
            this.lblSayerSh.Size = new System.Drawing.Size(154, 25);
            this.lblSayerSh.TabIndex = 59;
            this.lblSayerSh.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(163, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 17);
            this.label19.TabIndex = 58;
            this.label19.Text = "آبونمان :";
            // 
            // lblMablagh
            // 
            this.lblMablagh.BackColor = System.Drawing.Color.Transparent;
            this.lblMablagh.ForeColor = System.Drawing.Color.Black;
            this.lblMablagh.Location = new System.Drawing.Point(3, 26);
            this.lblMablagh.Name = "lblMablagh";
            this.lblMablagh.Size = new System.Drawing.Size(154, 25);
            this.lblMablagh.TabIndex = 57;
            this.lblMablagh.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(163, 209);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(34, 17);
            this.label22.TabIndex = 56;
            this.label22.Text = "سایر :";
            // 
            // lblJarimeSh
            // 
            this.lblJarimeSh.BackColor = System.Drawing.Color.Transparent;
            this.lblJarimeSh.ForeColor = System.Drawing.Color.Black;
            this.lblJarimeSh.Location = new System.Drawing.Point(3, 118);
            this.lblJarimeSh.Name = "lblJarimeSh";
            this.lblJarimeSh.Size = new System.Drawing.Size(154, 25);
            this.lblJarimeSh.TabIndex = 55;
            this.lblJarimeSh.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(163, 118);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 17);
            this.label24.TabIndex = 54;
            this.label24.Text = "جریمه :";
            // 
            // lblBedehiSh
            // 
            this.lblBedehiSh.BackColor = System.Drawing.Color.Transparent;
            this.lblBedehiSh.ForeColor = System.Drawing.Color.Black;
            this.lblBedehiSh.Location = new System.Drawing.Point(3, 72);
            this.lblBedehiSh.Name = "lblBedehiSh";
            this.lblBedehiSh.Size = new System.Drawing.Size(154, 25);
            this.lblBedehiSh.TabIndex = 53;
            this.lblBedehiSh.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(163, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 17);
            this.label26.TabIndex = 52;
            this.label26.Text = "بدهی :";
            // 
            // lblMaliatSh
            // 
            this.lblMaliatSh.BackColor = System.Drawing.Color.Transparent;
            this.lblMaliatSh.ForeColor = System.Drawing.Color.Black;
            this.lblMaliatSh.Location = new System.Drawing.Point(3, 163);
            this.lblMaliatSh.Name = "lblMaliatSh";
            this.lblMaliatSh.Size = new System.Drawing.Size(154, 25);
            this.lblMaliatSh.TabIndex = 51;
            this.lblMaliatSh.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(163, 163);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 17);
            this.label28.TabIndex = 50;
            this.label28.Text = "مالیات :";
            // 
            // lblSumAb
            // 
            this.lblSumAb.BackColor = System.Drawing.Color.Transparent;
            this.lblSumAb.ForeColor = System.Drawing.Color.Black;
            this.lblSumAb.Location = new System.Drawing.Point(261, 392);
            this.lblSumAb.Name = "lblSumAb";
            this.lblSumAb.Size = new System.Drawing.Size(154, 25);
            this.lblSumAb.TabIndex = 49;
            this.lblSumAb.Text = "0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(421, 392);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 17);
            this.label32.TabIndex = 48;
            this.label32.Text = "مجموع :";
            // 
            // lblJozyi
            // 
            this.lblJozyi.BackColor = System.Drawing.Color.Transparent;
            this.lblJozyi.ForeColor = System.Drawing.Color.Black;
            this.lblJozyi.Location = new System.Drawing.Point(261, 347);
            this.lblJozyi.Name = "lblJozyi";
            this.lblJozyi.Size = new System.Drawing.Size(154, 25);
            this.lblJozyi.TabIndex = 47;
            this.lblJozyi.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(421, 347);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 17);
            this.label34.TabIndex = 46;
            this.label34.Text = "پرداخت جزئی :";
            // 
            // lblSayer
            // 
            this.lblSayer.BackColor = System.Drawing.Color.Transparent;
            this.lblSayer.ForeColor = System.Drawing.Color.Black;
            this.lblSayer.Location = new System.Drawing.Point(261, 301);
            this.lblSayer.Name = "lblSayer";
            this.lblSayer.Size = new System.Drawing.Size(154, 25);
            this.lblSayer.TabIndex = 45;
            this.lblSayer.Text = "0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(421, 255);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 17);
            this.label36.TabIndex = 44;
            this.label36.Text = "آبونمان :";
            // 
            // lblAboonman
            // 
            this.lblAboonman.BackColor = System.Drawing.Color.Transparent;
            this.lblAboonman.ForeColor = System.Drawing.Color.Black;
            this.lblAboonman.Location = new System.Drawing.Point(261, 255);
            this.lblAboonman.Name = "lblAboonman";
            this.lblAboonman.Size = new System.Drawing.Size(154, 25);
            this.lblAboonman.TabIndex = 43;
            this.lblAboonman.Text = "0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(421, 301);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 17);
            this.label38.TabIndex = 42;
            this.label38.Text = "سایر :";
            // 
            // lblJarime
            // 
            this.lblJarime.BackColor = System.Drawing.Color.Transparent;
            this.lblJarime.ForeColor = System.Drawing.Color.Black;
            this.lblJarime.Location = new System.Drawing.Point(261, 209);
            this.lblJarime.Name = "lblJarime";
            this.lblJarime.Size = new System.Drawing.Size(154, 25);
            this.lblJarime.TabIndex = 41;
            this.lblJarime.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(421, 209);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(40, 17);
            this.label40.TabIndex = 40;
            this.label40.Text = "جریمه :";
            // 
            // lblBedehi
            // 
            this.lblBedehi.BackColor = System.Drawing.Color.Transparent;
            this.lblBedehi.ForeColor = System.Drawing.Color.Black;
            this.lblBedehi.Location = new System.Drawing.Point(261, 163);
            this.lblBedehi.Name = "lblBedehi";
            this.lblBedehi.Size = new System.Drawing.Size(154, 25);
            this.lblBedehi.TabIndex = 27;
            this.lblBedehi.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(421, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 17);
            this.label9.TabIndex = 26;
            this.label9.Text = "بدهی :";
            // 
            // lblMaliat
            // 
            this.lblMaliat.BackColor = System.Drawing.Color.Transparent;
            this.lblMaliat.ForeColor = System.Drawing.Color.Black;
            this.lblMaliat.Location = new System.Drawing.Point(261, 118);
            this.lblMaliat.Name = "lblMaliat";
            this.lblMaliat.Size = new System.Drawing.Size(154, 25);
            this.lblMaliat.TabIndex = 25;
            this.lblMaliat.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(421, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 17);
            this.label15.TabIndex = 24;
            this.label15.Text = "مالیات :";
            // 
            // lblGhMojaz
            // 
            this.lblGhMojaz.BackColor = System.Drawing.Color.Transparent;
            this.lblGhMojaz.ForeColor = System.Drawing.Color.Black;
            this.lblGhMojaz.Location = new System.Drawing.Point(261, 72);
            this.lblGhMojaz.Name = "lblGhMojaz";
            this.lblGhMojaz.Size = new System.Drawing.Size(154, 25);
            this.lblGhMojaz.TabIndex = 23;
            this.lblGhMojaz.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(421, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 17);
            this.label18.TabIndex = 22;
            this.label18.Text = "مصرف غیرمجاز :";
            // 
            // lblMojaz
            // 
            this.lblMojaz.BackColor = System.Drawing.Color.Transparent;
            this.lblMojaz.ForeColor = System.Drawing.Color.Black;
            this.lblMojaz.Location = new System.Drawing.Point(261, 26);
            this.lblMojaz.Name = "lblMojaz";
            this.lblMojaz.Size = new System.Drawing.Size(154, 25);
            this.lblMojaz.TabIndex = 21;
            this.lblMojaz.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(421, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 17);
            this.label20.TabIndex = 20;
            this.label20.Text = "مصرف مجاز :";
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.sumDoreh);
            this.groupPanel4.Controls.Add(this.label3);
            this.groupPanel4.Controls.Add(this.lblPOSDoreh);
            this.groupPanel4.Controls.Add(this.label7);
            this.groupPanel4.Controls.Add(this.lblCashDoreh);
            this.groupPanel4.Controls.Add(this.label11);
            this.groupPanel4.Controls.Add(this.lblIntrDoreh);
            this.groupPanel4.Controls.Add(this.label14);
            this.groupPanel4.Controls.Add(this.lblChqDoreh);
            this.groupPanel4.Controls.Add(this.label16);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(543, 64);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(280, 630);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 5;
            this.groupPanel4.Text = "به تفکیک نوع پرداخت";
            // 
            // sumDoreh
            // 
            this.sumDoreh.BackColor = System.Drawing.Color.Transparent;
            this.sumDoreh.ForeColor = System.Drawing.Color.Black;
            this.sumDoreh.Location = new System.Drawing.Point(3, 209);
            this.sumDoreh.Name = "sumDoreh";
            this.sumDoreh.Size = new System.Drawing.Size(154, 25);
            this.sumDoreh.TabIndex = 19;
            this.sumDoreh.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(163, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "مجموع :";
            // 
            // lblPOSDoreh
            // 
            this.lblPOSDoreh.BackColor = System.Drawing.Color.Transparent;
            this.lblPOSDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblPOSDoreh.Location = new System.Drawing.Point(3, 163);
            this.lblPOSDoreh.Name = "lblPOSDoreh";
            this.lblPOSDoreh.Size = new System.Drawing.Size(154, 25);
            this.lblPOSDoreh.TabIndex = 17;
            this.lblPOSDoreh.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(163, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "پوز :";
            // 
            // lblCashDoreh
            // 
            this.lblCashDoreh.BackColor = System.Drawing.Color.Transparent;
            this.lblCashDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblCashDoreh.Location = new System.Drawing.Point(3, 118);
            this.lblCashDoreh.Name = "lblCashDoreh";
            this.lblCashDoreh.Size = new System.Drawing.Size(154, 25);
            this.lblCashDoreh.TabIndex = 15;
            this.lblCashDoreh.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(163, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "واریز نقدی :";
            // 
            // lblIntrDoreh
            // 
            this.lblIntrDoreh.BackColor = System.Drawing.Color.Transparent;
            this.lblIntrDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblIntrDoreh.Location = new System.Drawing.Point(3, 72);
            this.lblIntrDoreh.Name = "lblIntrDoreh";
            this.lblIntrDoreh.Size = new System.Drawing.Size(154, 25);
            this.lblIntrDoreh.TabIndex = 13;
            this.lblIntrDoreh.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(163, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 17);
            this.label14.TabIndex = 12;
            this.label14.Text = "پرداخت اینترنتی :";
            // 
            // lblChqDoreh
            // 
            this.lblChqDoreh.BackColor = System.Drawing.Color.Transparent;
            this.lblChqDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblChqDoreh.Location = new System.Drawing.Point(3, 26);
            this.lblChqDoreh.Name = "lblChqDoreh";
            this.lblChqDoreh.Size = new System.Drawing.Size(154, 25);
            this.lblChqDoreh.TabIndex = 11;
            this.lblChqDoreh.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(163, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 17);
            this.label16.TabIndex = 10;
            this.label16.Text = "چک :";
            // 
            // sharjTab
            // 
            this.sharjTab.AttachedControl = this.superTabControlPanel1;
            this.sharjTab.GlobalItem = false;
            this.sharjTab.Name = "sharjTab";
            this.sharjTab.Text = "دوره";
            // 
            // superTabItem1
            // 
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "تعرفه های آب";
            // 
            // superTabItem3
            // 
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "تعرفه شارژ";
            // 
            // ab_dorehTableAdapter
            // 
            this.ab_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // sharj_dorehTableAdapter
            // 
            this.sharj_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // frmDaramadRpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 733);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDaramadRpt";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmDaramadRpt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            this.superTabControlPanel3.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.SuperTabItem abTab;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem sharjTab;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.ButtonX btnAll;
        private DevComponents.DotNetBar.ButtonX btnAdvancedSearch;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private FarsiLibrary.Win.Controls.FADatePicker dtpTo;
        private FarsiLibrary.Win.Controls.FADatePicker dtpFrom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbSharj;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbAb;
        private DevComponents.DotNetBar.Controls.CheckBoxX chbSharj;
        private DevComponents.DotNetBar.Controls.CheckBoxX chbAb;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.Label sumPeriodic;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblPOSTotal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblCashTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblIntTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblChqTotal;
        private System.Windows.Forms.Label label1;
        private AbDataset abDataset;
        private System.Windows.Forms.BindingSource abdorehBindingSource;
        private AbDatasetTableAdapters.ab_dorehTableAdapter ab_dorehTableAdapter;
        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource sharjdorehBindingSource;
        private CWMSDataSetTableAdapters.sharj_dorehTableAdapter sharj_dorehTableAdapter;
        private System.Windows.Forms.Label sumDoreh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPOSDoreh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCashDoreh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblIntrDoreh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblChqDoreh;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblBedehi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblMaliat;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblGhMojaz;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblMojaz;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblSumAb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lblJozyi;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblSayer;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lblAboonman;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblJarime;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblSumSh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblJozyiSh;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblSayerSh;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblMablagh;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblJarimeSh;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblBedehiSh;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblMaliatSh;
        private System.Windows.Forms.Label label28;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label lblfishtotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTahator;
        private System.Windows.Forms.Label label17;
    }
}