﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using Microsoft.Office.Interop.Excel;

namespace CWMS.ManagementReports
{
    public partial class FrmGozareshMaliati : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmGozareshMaliati()
        {
            Classes.ClsMain.ChangeCulture("f");

            InitializeComponent();
        }

        private void FrmGozareshMaliati_Load(object sender, EventArgs e)
        {

        }

        private void BtnFindByTarikh_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("f");

            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);

            sharj_ghabz_PardakhtTableAdapter.Fill(daramadDataset.sharj_ghabz_Pardakht, start, finish);
            //sharj_Pardakht_reportTableAdapter.FillByTarikh(daramadDataset.Sharj_Pardakht_report, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime));
            ab_ghabzTableAdapter.Fill(daramadDataset1.ab_ghabz, start, finish);

            decimal TotalmamjmuMablagh = 0, TotalmajmuMaliat = 0, TotalBedehi = 0, TotalKasrHezar = 0;
            bool SharjStatus = true;

            dgvsharjError.Rows.Clear();
            DgvAbError.Rows.Clear();
            dgvAbResult.Rows.Clear();
            DgvResult.Rows.Clear();


            for (int i = 0; i < daramadDataset.sharj_ghabz_Pardakht.Rows.Count; i++)
            {
                decimal mamjmuMablagh = 0, majmuMaliat = 0, majmuKasr = 0;
                SharjStatus = true;
                if (Convert.ToDecimal(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]) != 0)
                {
                    mamjmuMablagh = 0;
                    majmuKasr = 0;
                    majmuMaliat = 0;

                    CalcBedehiDetailsOfsharj
                        (
                        Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]) - 1,
                        Convert.ToInt32(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]),
                        ref SharjStatus,
                        ref mamjmuMablagh,
                        ref majmuMaliat,
                        ref majmuKasr
                        );



                    if (SharjStatus == true)
                    {
                        DgvResult.Rows.Add(
                            daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]
                            , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]
                            , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]
                            , mamjmuMablagh
                            , majmuMaliat
                            , majmuKasr
                            , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"]
                            );
                        TotalmamjmuMablagh += mamjmuMablagh;
                        TotalBedehi += Convert.ToDecimal(daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]);
                        TotalmajmuMaliat += majmuMaliat;
                        TotalKasrHezar += majmuKasr;
                    }
                    else
                        dgvsharjError.Rows.Add(
                           daramadDataset.sharj_ghabz_Pardakht.Rows[i]["دوره"]
                           , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["قرارداد"]
                           , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["بدهی"]
                           , mamjmuMablagh
                           , majmuMaliat
                           , majmuKasr
                           , daramadDataset.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"]
                           );
                }
            }
            lblTotalBedehi.Text = TotalBedehi.ToString();
            lblTotalMajmu.Text = TotalmamjmuMablagh.ToString();
            lblTotalMaliat.Text = TotalmajmuMaliat.ToString();
            lblmajmukasr.Text = TotalKasrHezar.ToString();

            TotalmamjmuMablagh = 0; TotalmajmuMaliat = 0; TotalBedehi = 0; TotalKasrHezar = 0;

            bool AbStatus = true;


            for (int i = 0; i < daramadDataset1.ab_ghabz.Rows.Count; i++)
            {
                decimal mamjmuMablagh = 0, majmuMaliat = 0, majmuKasr = 0;
                AbStatus = true;

                if (Convert.ToDecimal(daramadDataset1.ab_ghabz.Rows[i]["بدهی"]) != 0)
                {
                    mamjmuMablagh = 0;
                    majmuMaliat = 0;
                    majmuKasr = 0;

                    CalcBedehiDetailsOfAb
                        (
                        Convert.ToInt32(daramadDataset1.ab_ghabz.Rows[i]["دوره"]) - 1,
                        Convert.ToInt32(daramadDataset1.ab_ghabz.Rows[i]["قرارداد"]),
                        ref AbStatus,
                        ref mamjmuMablagh,
                        ref majmuMaliat,
                        ref majmuKasr
                        );

                    if (AbStatus == true)
                    {
                        dgvAbResult.Rows.Add(
                            daramadDataset1.ab_ghabz.Rows[i]["دوره"]
                            , daramadDataset1.ab_ghabz.Rows[i]["قرارداد"]
                            , daramadDataset1.ab_ghabz.Rows[i]["بدهی"]
                            , mamjmuMablagh
                            , majmuMaliat
                            , majmuKasr
                            , daramadDataset1.ab_ghabz.Rows[i]["آخرین پرداخت"]
                            );
                        TotalmamjmuMablagh += mamjmuMablagh;
                        TotalBedehi += Convert.ToDecimal(daramadDataset1.ab_ghabz.Rows[i]["بدهی"]);
                        TotalmajmuMaliat += majmuMaliat;
                        TotalKasrHezar += majmuKasr;
                    }
                    else
                        DgvAbError.Rows.Add(
                            daramadDataset1.ab_ghabz.Rows[i]["دوره"]
                            , daramadDataset1.ab_ghabz.Rows[i]["قرارداد"]
                            , daramadDataset1.ab_ghabz.Rows[i]["بدهی"]
                            , mamjmuMablagh
                            , majmuMaliat
                            , majmuKasr
                            , daramadDataset1.ab_ghabz.Rows[i]["آخرین پرداخت"]
                            );

                }
            }
            lblAbTotalBedehi.Text = TotalBedehi.ToString();
            lblAbMajmuMabalegh.Text = TotalmamjmuMablagh.ToString();
            lblAbMajmuMaliat.Text = TotalmajmuMaliat.ToString();
            lblmajmukasrhezar_ab.Text = TotalKasrHezar.ToString();



            decimal SharjMajmuMoshkeldar = 0;
            for (int i = 0; i < dgvsharjError.Rows.Count; i++)
            {
                SharjMajmuMoshkeldar += Convert.ToDecimal(dgvsharjError.Rows[i].Cells[2].Value);
            }
            lblMajmuMoshkeldar.Text = SharjMajmuMoshkeldar.ToString();



            decimal AbMajmuMoshkeldar = 0;
            for (int i = 0; i < DgvAbError.Rows.Count; i++)
            {
                AbMajmuMoshkeldar += Convert.ToDecimal(DgvAbError.Rows[i].Cells[2].Value);
            }
            lblAbMajmuMoshkeldar.Text = AbMajmuMoshkeldar.ToString();


        }
        void CalcBedehiDetailsOfsharj(int dcode, int gharardad, ref bool status, ref decimal majuMablaghm, ref decimal majmumaliat, ref decimal majmukasr)
        {
            if (dcode == 0)
            {
                return;
            }


            System.Data.DataTable dtDoreh = Classes.ClsMain.GetDataTable("select * from sharj_ghabz where dcode=" + dcode + " and gharardad=" + gharardad);
            if (dtDoreh.Rows.Count == 0)
            {
                status = false;
                return;
            }
            //else if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) == 0)
            //{
            //    //jarimeh + sayer + mablagh AS 'مجموع مبالغ', 

            //    if (Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) != Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) + Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]))
            //    {
            //        majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
            //            + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
            //        majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
            //        majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
            //    }
            //}
            //else
            //    CalcBedehiDetailsOfsharj(dcode - 1, gharardad, ref majuMablaghm, ref majmumaliat, ref majmukasr);



            // ---------------------- version 2

            //if (Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) != Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) + Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]))
            //{
            //    majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
            //        + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
            //    majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
            //    majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
            //}

            //if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
            //{

            //    CalcBedehiDetailsOfsharj(dcode - 1, gharardad, ref majuMablaghm, ref majmumaliat, ref majmukasr);
            //}

            // --------------------- version 3
            // اگر قبض به طور کامل پرداخت نشده باشد 
            if (Convert.ToInt32(dtDoreh.Rows[0]["vaziat"]) == 0)
            {
                //if (Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) - Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) < 1000)
                //{
                majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
                    + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
                majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
                majmukasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
                if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
                {
                    CalcBedehiDetailsOfsharj(dcode - 1, gharardad, ref status, ref majuMablaghm, ref majmumaliat, ref majmukasr);
                }
            }
            // در این حالت مطمئنا کاربر قبض خود را به طور جزئی پرداخت کرده است .
            // به هیچ وجه امکان نخواهد داشت که وضعیت قبض یک باشد
            // چون در این صورت بدهی نباید به دوره بعد منتقل می شد که این خلاف الگوریتم است 
            else
                status = false;

        }

        void CalcBedehiDetailsOfAb(int dcode, int gharardad, ref bool status, ref decimal majuMablaghm, ref decimal majmumaliat, ref decimal majmuKasr)
        {
            if (dcode == 0)
            {
                return;
            }


            System.Data.DataTable dtDoreh = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + dcode + " and gharardad=" + gharardad);
            if (dtDoreh.Rows.Count == 0)
            {
                status = false;
                return;
            }

            //if (Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) != Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) + Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]))
            //{
            //    majuMablaghm += Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"]) + Convert.ToDecimal(dtDoreh.Rows[0]["aboonmah"])
            //        + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"]) + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"]);
            //    majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
            //    majmuKasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
            //}


            //else if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
            //    CalcBedehiDetailsOfAb(dcode - 1, gharardad, ref majuMablaghm, ref majmumaliat, ref majmuKasr);
            //-------- verionn 2
            if (Convert.ToDecimal(dtDoreh.Rows[0]["mablaghkol"]) - Convert.ToDecimal(dtDoreh.Rows[0]["mande"]) < 1000)
            {
                //mablagh + jarimeh + sayer + aboonmah
                majuMablaghm +=
                    Convert.ToDecimal(dtDoreh.Rows[0]["jarimeh"])
                    + Convert.ToDecimal(dtDoreh.Rows[0]["sayer"])
                    + Convert.ToDecimal(dtDoreh.Rows[0]["mablagh"])
                    + Convert.ToDecimal(dtDoreh.Rows[0]["aboonmah"]);
                majmumaliat += Convert.ToDecimal(dtDoreh.Rows[0]["maliat"]);
                majmuKasr += Convert.ToDecimal(dtDoreh.Rows[0]["kasr_hezar"]);
                if (Convert.ToDecimal(dtDoreh.Rows[0]["bedehi"]) != 0)
                {

                    CalcBedehiDetailsOfAb(dcode - 1, gharardad, ref status, ref majuMablaghm, ref majmumaliat, ref majmuKasr);
                }
            }
            else
                status = false;








        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            dgvAbResult.SelectAll();
            Clipboard.SetDataObject(dgvAbResult);
            //exprorttoexel(dgvAbResult);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //void exprorttoexel(DataGridView dgv)
        //{
        //    try
        //    {
        //        Microsoft.Office.Interop.Excel.Application exeleapp = new Microsoft.Office.Interop.Excel.Application();
        //        Microsoft.Office.Interop.Excel.Workbook exelbook = exeleapp.Workbooks.Add(XlSheetType.xlWorksheet);
        //        Microsoft.Office.Interop.Excel.Worksheet exelworksheet = (Worksheet)(exelbook.Worksheets[1]);
        //        exelworksheet.DisplayRightToLeft = true;

        //        int clmncnt = dgv.Columns.Count;
        //        Range[] rng = new Range[clmncnt];

        //        for (int x = 0; x < clmncnt; x++)
        //        {
        //            string celladress = Convert.ToString(Convert.ToChar(Convert.ToByte(x + 65))) + "1";
        //            rng[x] = exelworksheet.get_Range(celladress, celladress);
        //            rng[x].Value2 = dgv.Columns[x].HeaderText;
        //        }

        //        int j = 2;
        //        foreach (DataRow r in daramadDataset.sharj_ghabz_Pardakht.Rows)
        //        {
        //            for (int k = 0; k < clmncnt; k++)
        //            {
        //                string celladress = Convert.ToString(Convert.ToChar(Convert.ToByte(k + 65))) + j.ToString();
        //                rng[k] = exelworksheet.get_Range(celladress, celladress);
        //                rng[k].Value2 = r[k].ToString();
        //            }
        //            j++;
        //        }
        //        exeleapp.Visible = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //}

    }


}
