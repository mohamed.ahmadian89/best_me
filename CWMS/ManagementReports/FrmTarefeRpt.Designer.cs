﻿namespace CWMS.ManagementReports
{
    partial class FrmTarefeRpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.chrMaliatAb = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.abdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.chrMaliatSharj = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.sharjdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.radSharjChart = new System.Windows.Forms.RadioButton();
            this.radSharjGrid = new System.Windows.Forms.RadioButton();
            this.pnlSharj = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjmablaghBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlSharjCharts = new System.Windows.Forms.Panel();
            this.radSharjEjra = new System.Windows.Forms.RadioButton();
            this.chrSharjPerEjra = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.radSharjDoreh = new System.Windows.Forms.RadioButton();
            this.chrSharjPerDoreh = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.radShowChart = new System.Windows.Forms.RadioButton();
            this.radShowGrid = new System.Windows.Forms.RadioButton();
            this.pnlAb = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghabDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.darsadmaliatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhtasvibDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhejraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abmablaghBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlAbCharts = new System.Windows.Forms.Panel();
            this.radEjra = new System.Windows.Forms.RadioButton();
            this.chrAbMablaghPerEjra = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.radDoreh = new System.Windows.Forms.RadioButton();
            this.chrAbMablaghPerDoreh = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.temp = new DevComponents.DotNetBar.SuperTabItem();
            this.ab_dorehTableAdapter = new CWMS.AbDatasetTableAdapters.ab_dorehTableAdapter();
            this.ab_mablaghTableAdapter = new CWMS.MainDataSestTableAdapters.ab_mablaghTableAdapter();
            this.sharj_dorehTableAdapter = new CWMS.MainDataSestTableAdapters.sharj_dorehTableAdapter();
            this.sharj_mablaghTableAdapter = new CWMS.MainDataSestTableAdapters.sharj_mablaghTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrMaliatAb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrMaliatSharj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.pnlSharj.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjmablaghBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.pnlSharjCharts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrSharjPerEjra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrSharjPerDoreh)).BeginInit();
            this.superTabControlPanel1.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            this.pnlAb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abmablaghBindingSource)).BeginInit();
            this.pnlAbCharts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrAbMablaghPerEjra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrAbMablaghPerDoreh)).BeginInit();
            this.SuspendLayout();
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(14, 10);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 1;
            this.superTabControl1.Size = new System.Drawing.Size(910, 659);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 1;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.superTabItem3,
            this.superTabItem2});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.labelX1);
            this.superTabControlPanel2.Controls.Add(this.labelX3);
            this.superTabControlPanel2.Controls.Add(this.chrMaliatAb);
            this.superTabControlPanel2.Controls.Add(this.chrMaliatSharj);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(910, 630);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX1.Location = new System.Drawing.Point(105, 606);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(272, 33);
            this.labelX1.TabIndex = 7;
            this.labelX1.Text = "درصد های مالیات مربوط به دوره های شارژ";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelX3.Location = new System.Drawing.Point(508, 606);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(272, 33);
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "درصد های مالیات مربوط به دوره های آّب";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // chrMaliatAb
            // 
            this.chrMaliatAb.BorderSkin.BackColor = System.Drawing.Color.White;
            this.chrMaliatAb.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Raised;
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Area3DStyle.PointDepth = 30;
            chartArea1.Area3DStyle.PointGapDepth = 30;
            chartArea1.Area3DStyle.WallWidth = 2;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX2.IsLabelAutoFit = false;
            chartArea1.AxisX2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY2.IsLabelAutoFit = false;
            chartArea1.AxisY2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackSecondaryColor = System.Drawing.Color.White;
            chartArea1.BorderColor = System.Drawing.Color.White;
            chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.Name = "ChartArea1";
            this.chrMaliatAb.ChartAreas.Add(chartArea1);
            this.chrMaliatAb.DataSource = this.abdorehBindingSource;
            legend1.BackColor = System.Drawing.Color.White;
            legend1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            legend1.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.chrMaliatAb.Legends.Add(legend1);
            this.chrMaliatAb.Location = new System.Drawing.Point(467, 149);
            this.chrMaliatAb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrMaliatAb.Name = "chrMaliatAb";
            this.chrMaliatAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            series1.ChartArea = "ChartArea1";
            series1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.ShadowColor = System.Drawing.Color.White;
            series1.XValueMember = "dcode";
            series1.YValueMembers = "darsad_maliat";
            this.chrMaliatAb.Series.Add(series1);
            this.chrMaliatAb.Size = new System.Drawing.Size(391, 450);
            this.chrMaliatAb.TabIndex = 2;
            this.chrMaliatAb.Text = "dsds";
            // 
            // abdorehBindingSource
            // 
            this.abdorehBindingSource.DataMember = "ab_doreh";
            this.abdorehBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chrMaliatSharj
            // 
            this.chrMaliatSharj.BorderSkin.BackColor = System.Drawing.Color.White;
            this.chrMaliatSharj.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Raised;
            chartArea2.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea2.Area3DStyle.Enable3D = true;
            chartArea2.Area3DStyle.PointDepth = 30;
            chartArea2.Area3DStyle.PointGapDepth = 30;
            chartArea2.Area3DStyle.WallWidth = 2;
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisX2.IsLabelAutoFit = false;
            chartArea2.AxisX2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisX2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisY2.IsLabelAutoFit = false;
            chartArea2.AxisY2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.AxisY2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea2.BackColor = System.Drawing.Color.White;
            chartArea2.BackSecondaryColor = System.Drawing.Color.White;
            chartArea2.BorderColor = System.Drawing.Color.White;
            chartArea2.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.Name = "ChartArea1";
            this.chrMaliatSharj.ChartAreas.Add(chartArea2);
            this.chrMaliatSharj.DataSource = this.sharjdorehBindingSource;
            legend2.BackColor = System.Drawing.Color.White;
            legend2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            legend2.IsTextAutoFit = false;
            legend2.Name = "Legend1";
            legend2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.chrMaliatSharj.Legends.Add(legend2);
            this.chrMaliatSharj.Location = new System.Drawing.Point(52, 149);
            this.chrMaliatSharj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrMaliatSharj.Name = "chrMaliatSharj";
            this.chrMaliatSharj.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            series2.ChartArea = "ChartArea1";
            series2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            series2.IsValueShownAsLabel = true;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.ShadowColor = System.Drawing.Color.White;
            series2.XValueMember = "dcode";
            series2.YValueMembers = "darsad_maliat";
            this.chrMaliatSharj.Series.Add(series2);
            this.chrMaliatSharj.Size = new System.Drawing.Size(409, 450);
            this.chrMaliatSharj.TabIndex = 1;
            this.chrMaliatSharj.Text = "dsds";
            // 
            // sharjdorehBindingSource
            // 
            this.sharjdorehBindingSource.DataMember = "sharj_doreh";
            this.sharjdorehBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "مالیات";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.groupPanel2);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(910, 630);
            this.superTabControlPanel4.TabIndex = 2;
            this.superTabControlPanel4.TabItem = this.superTabItem3;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.radSharjChart);
            this.groupPanel2.Controls.Add(this.radSharjGrid);
            this.groupPanel2.Controls.Add(this.pnlSharj);
            this.groupPanel2.Controls.Add(this.pnlSharjCharts);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(24, 4);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(851, 650);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 5;
            this.groupPanel2.Text = "نمودار میزان بدهی در کلیه دوره ها";
            // 
            // radSharjChart
            // 
            this.radSharjChart.AutoSize = true;
            this.radSharjChart.BackColor = System.Drawing.Color.Transparent;
            this.radSharjChart.Location = new System.Drawing.Point(561, 4);
            this.radSharjChart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radSharjChart.Name = "radSharjChart";
            this.radSharjChart.Size = new System.Drawing.Size(82, 21);
            this.radSharjChart.TabIndex = 13;
            this.radSharjChart.Text = "نمایش نمودار";
            this.radSharjChart.UseVisualStyleBackColor = false;
            this.radSharjChart.Click += new System.EventHandler(this.radSharj_Click);
            // 
            // radSharjGrid
            // 
            this.radSharjGrid.AutoSize = true;
            this.radSharjGrid.BackColor = System.Drawing.Color.Transparent;
            this.radSharjGrid.Checked = true;
            this.radSharjGrid.Location = new System.Drawing.Point(719, 4);
            this.radSharjGrid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radSharjGrid.Name = "radSharjGrid";
            this.radSharjGrid.Size = new System.Drawing.Size(80, 21);
            this.radSharjGrid.TabIndex = 12;
            this.radSharjGrid.TabStop = true;
            this.radSharjGrid.Text = "نمایش جدول";
            this.radSharjGrid.UseVisualStyleBackColor = false;
            this.radSharjGrid.Click += new System.EventHandler(this.radSharj_Click);
            // 
            // pnlSharj
            // 
            this.pnlSharj.BackColor = System.Drawing.Color.Transparent;
            this.pnlSharj.Controls.Add(this.dataGridView3);
            this.pnlSharj.Controls.Add(this.dataGridView5);
            this.pnlSharj.ForeColor = System.Drawing.Color.Black;
            this.pnlSharj.Location = new System.Drawing.Point(3, 48);
            this.pnlSharj.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSharj.Name = "pnlSharj";
            this.pnlSharj.Size = new System.Drawing.Size(839, 562);
            this.pnlSharj.TabIndex = 15;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.ColumnHeadersHeight = 27;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView3.DataSource = this.sharjmablaghBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(3, 4);
            this.dataGridView3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(428, 558);
            this.dataGridView3.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "tarikh_tasvib";
            this.dataGridViewTextBoxColumn2.HeaderText = "تاریخ تصویب";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 88;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "tarikh_ejra";
            this.dataGridViewTextBoxColumn3.HeaderText = "تاریخ اجرا";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "mablagh";
            this.dataGridViewTextBoxColumn4.HeaderText = "مبلغ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // sharjmablaghBindingSource
            // 
            this.sharjmablaghBindingSource.DataMember = "sharj_mablagh";
            this.sharjmablaghBindingSource.DataSource = this.mainDataSestBindingSource;
            // 
            // mainDataSestBindingSource
            // 
            this.mainDataSestBindingSource.DataSource = this.mainDataSest;
            this.mainDataSestBindingSource.Position = 0;
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToAddRows = false;
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView5.ColumnHeadersHeight = 27;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn9});
            this.dataGridView5.DataSource = this.sharjdorehBindingSource;
            this.dataGridView5.Location = new System.Drawing.Point(437, 4);
            this.dataGridView5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.ReadOnly = true;
            this.dataGridView5.RowTemplate.Height = 24;
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.Size = new System.Drawing.Size(399, 554);
            this.dataGridView5.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "dcode";
            this.dataGridViewTextBoxColumn6.HeaderText = "شماره دوره";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 80;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "mablagh_sharj";
            this.dataGridViewTextBoxColumn9.HeaderText = "مبلغ تعرفه شارژ";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // pnlSharjCharts
            // 
            this.pnlSharjCharts.BackColor = System.Drawing.Color.Transparent;
            this.pnlSharjCharts.Controls.Add(this.radSharjEjra);
            this.pnlSharjCharts.Controls.Add(this.chrSharjPerEjra);
            this.pnlSharjCharts.Controls.Add(this.radSharjDoreh);
            this.pnlSharjCharts.Controls.Add(this.chrSharjPerDoreh);
            this.pnlSharjCharts.Location = new System.Drawing.Point(3, 48);
            this.pnlSharjCharts.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSharjCharts.Name = "pnlSharjCharts";
            this.pnlSharjCharts.Size = new System.Drawing.Size(839, 562);
            this.pnlSharjCharts.TabIndex = 14;
            this.pnlSharjCharts.Visible = false;
            // 
            // radSharjEjra
            // 
            this.radSharjEjra.AutoSize = true;
            this.radSharjEjra.BackColor = System.Drawing.Color.Transparent;
            this.radSharjEjra.Location = new System.Drawing.Point(536, 30);
            this.radSharjEjra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radSharjEjra.Name = "radSharjEjra";
            this.radSharjEjra.Size = new System.Drawing.Size(103, 21);
            this.radSharjEjra.TabIndex = 5;
            this.radSharjEjra.Text = "براساس زمان اجرا";
            this.radSharjEjra.UseVisualStyleBackColor = false;
            this.radSharjEjra.Click += new System.EventHandler(this.radSharjChangeChart_Click);
            // 
            // chrSharjPerEjra
            // 
            this.chrSharjPerEjra.BackColor = System.Drawing.Color.Transparent;
            chartArea3.Area3DStyle.Enable3D = true;
            chartArea3.Area3DStyle.Inclination = 10;
            chartArea3.Area3DStyle.PointDepth = 10;
            chartArea3.Area3DStyle.PointGapDepth = 0;
            chartArea3.Area3DStyle.WallWidth = 0;
            chartArea3.BorderColor = System.Drawing.Color.Transparent;
            chartArea3.Name = "ChartArea1";
            this.chrSharjPerEjra.ChartAreas.Add(chartArea3);
            this.chrSharjPerEjra.DataSource = this.sharjmablaghBindingSource;
            this.chrSharjPerEjra.Location = new System.Drawing.Point(3, 84);
            this.chrSharjPerEjra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrSharjPerEjra.Name = "chrSharjPerEjra";
            series3.ChartArea = "ChartArea1";
            series3.Name = "Series1";
            series3.XValueMember = "tarikh_ejra";
            series3.YValueMembers = "mablagh";
            this.chrSharjPerEjra.Series.Add(series3);
            this.chrSharjPerEjra.Size = new System.Drawing.Size(836, 475);
            this.chrSharjPerEjra.TabIndex = 8;
            this.chrSharjPerEjra.Text = "chart1";
            this.chrSharjPerEjra.Visible = false;
            // 
            // radSharjDoreh
            // 
            this.radSharjDoreh.AutoSize = true;
            this.radSharjDoreh.BackColor = System.Drawing.Color.Transparent;
            this.radSharjDoreh.Checked = true;
            this.radSharjDoreh.Location = new System.Drawing.Point(713, 30);
            this.radSharjDoreh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radSharjDoreh.Name = "radSharjDoreh";
            this.radSharjDoreh.Size = new System.Drawing.Size(82, 21);
            this.radSharjDoreh.TabIndex = 4;
            this.radSharjDoreh.TabStop = true;
            this.radSharjDoreh.Text = "براساس دوره";
            this.radSharjDoreh.UseVisualStyleBackColor = false;
            this.radSharjDoreh.Click += new System.EventHandler(this.radSharjChangeChart_Click);
            // 
            // chrSharjPerDoreh
            // 
            this.chrSharjPerDoreh.BackColor = System.Drawing.Color.Transparent;
            chartArea4.Area3DStyle.Enable3D = true;
            chartArea4.Area3DStyle.Inclination = 10;
            chartArea4.Area3DStyle.PointDepth = 10;
            chartArea4.Area3DStyle.PointGapDepth = 0;
            chartArea4.Area3DStyle.WallWidth = 0;
            chartArea4.BorderColor = System.Drawing.Color.Transparent;
            chartArea4.Name = "ChartArea1";
            this.chrSharjPerDoreh.ChartAreas.Add(chartArea4);
            this.chrSharjPerDoreh.DataSource = this.sharjdorehBindingSource;
            this.chrSharjPerDoreh.Location = new System.Drawing.Point(0, 84);
            this.chrSharjPerDoreh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrSharjPerDoreh.Name = "chrSharjPerDoreh";
            series4.ChartArea = "ChartArea1";
            series4.Name = "Series1";
            series4.XValueMember = "dcode";
            series4.YValueMembers = "mablagh_sharj";
            this.chrSharjPerDoreh.Series.Add(series4);
            this.chrSharjPerDoreh.Size = new System.Drawing.Size(839, 479);
            this.chrSharjPerDoreh.TabIndex = 9;
            this.chrSharjPerDoreh.Text = "chart4";
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel4;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "تعرفه شارژ";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.groupPanel5);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(910, 630);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.radShowChart);
            this.groupPanel5.Controls.Add(this.radShowGrid);
            this.groupPanel5.Controls.Add(this.pnlAb);
            this.groupPanel5.Controls.Add(this.pnlAbCharts);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(24, 4);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.Size = new System.Drawing.Size(851, 614);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 5;
            this.groupPanel5.Text = "نمودار میزان بدهی در کلیه دوره ها";
            // 
            // radShowChart
            // 
            this.radShowChart.AutoSize = true;
            this.radShowChart.BackColor = System.Drawing.Color.Transparent;
            this.radShowChart.Location = new System.Drawing.Point(561, 4);
            this.radShowChart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radShowChart.Name = "radShowChart";
            this.radShowChart.Size = new System.Drawing.Size(82, 21);
            this.radShowChart.TabIndex = 5;
            this.radShowChart.Text = "نمایش نمودار";
            this.radShowChart.UseVisualStyleBackColor = false;
            this.radShowChart.Click += new System.EventHandler(this.rad_Click);
            // 
            // radShowGrid
            // 
            this.radShowGrid.AutoSize = true;
            this.radShowGrid.BackColor = System.Drawing.Color.Transparent;
            this.radShowGrid.Checked = true;
            this.radShowGrid.Location = new System.Drawing.Point(719, 4);
            this.radShowGrid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radShowGrid.Name = "radShowGrid";
            this.radShowGrid.Size = new System.Drawing.Size(80, 21);
            this.radShowGrid.TabIndex = 4;
            this.radShowGrid.TabStop = true;
            this.radShowGrid.Text = "نمایش جدول";
            this.radShowGrid.UseVisualStyleBackColor = false;
            this.radShowGrid.Click += new System.EventHandler(this.rad_Click);
            // 
            // pnlAb
            // 
            this.pnlAb.BackColor = System.Drawing.Color.Transparent;
            this.pnlAb.Controls.Add(this.dataGridView1);
            this.pnlAb.Controls.Add(this.dataGridView2);
            this.pnlAb.Location = new System.Drawing.Point(3, 48);
            this.pnlAb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlAb.Name = "pnlAb";
            this.pnlAb.Size = new System.Drawing.Size(839, 529);
            this.pnlAb.TabIndex = 11;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeight = 27;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dcodeDataGridViewTextBoxColumn,
            this.mablaghabDataGridViewTextBoxColumn,
            this.darsadmaliatDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.abdorehBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(437, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(399, 507);
            this.dataGridView1.TabIndex = 6;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "شماره دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.dcodeDataGridViewTextBoxColumn.Width = 80;
            // 
            // mablaghabDataGridViewTextBoxColumn
            // 
            this.mablaghabDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.mablaghabDataGridViewTextBoxColumn.DataPropertyName = "mablagh_ab";
            this.mablaghabDataGridViewTextBoxColumn.HeaderText = "مبلغ تعرفه آب";
            this.mablaghabDataGridViewTextBoxColumn.Name = "mablaghabDataGridViewTextBoxColumn";
            this.mablaghabDataGridViewTextBoxColumn.ReadOnly = true;
            this.mablaghabDataGridViewTextBoxColumn.Width = 94;
            // 
            // darsadmaliatDataGridViewTextBoxColumn
            // 
            this.darsadmaliatDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.darsadmaliatDataGridViewTextBoxColumn.DataPropertyName = "darsad_maliat";
            this.darsadmaliatDataGridViewTextBoxColumn.HeaderText = "درصد مالیات";
            this.darsadmaliatDataGridViewTextBoxColumn.Name = "darsadmaliatDataGridViewTextBoxColumn";
            this.darsadmaliatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeight = 27;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.tarikhtasvibDataGridViewTextBoxColumn,
            this.tarikhejraDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.abmablaghBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(3, 4);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(428, 507);
            this.dataGridView2.TabIndex = 7;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // tarikhtasvibDataGridViewTextBoxColumn
            // 
            this.tarikhtasvibDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tarikhtasvibDataGridViewTextBoxColumn.DataPropertyName = "tarikh_tasvib";
            this.tarikhtasvibDataGridViewTextBoxColumn.HeaderText = "تاریخ تصویب";
            this.tarikhtasvibDataGridViewTextBoxColumn.Name = "tarikhtasvibDataGridViewTextBoxColumn";
            this.tarikhtasvibDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhtasvibDataGridViewTextBoxColumn.Width = 88;
            // 
            // tarikhejraDataGridViewTextBoxColumn
            // 
            this.tarikhejraDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tarikhejraDataGridViewTextBoxColumn.DataPropertyName = "tarikh_ejra";
            this.tarikhejraDataGridViewTextBoxColumn.HeaderText = "تاریخ اجرا";
            this.tarikhejraDataGridViewTextBoxColumn.Name = "tarikhejraDataGridViewTextBoxColumn";
            this.tarikhejraDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhejraDataGridViewTextBoxColumn.Width = 75;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "مبلغ";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // abmablaghBindingSource
            // 
            this.abmablaghBindingSource.DataMember = "ab_mablagh";
            this.abmablaghBindingSource.DataSource = this.mainDataSest;
            // 
            // pnlAbCharts
            // 
            this.pnlAbCharts.BackColor = System.Drawing.Color.Transparent;
            this.pnlAbCharts.Controls.Add(this.radEjra);
            this.pnlAbCharts.Controls.Add(this.chrAbMablaghPerEjra);
            this.pnlAbCharts.Controls.Add(this.radDoreh);
            this.pnlAbCharts.Controls.Add(this.chrAbMablaghPerDoreh);
            this.pnlAbCharts.Location = new System.Drawing.Point(3, 48);
            this.pnlAbCharts.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlAbCharts.Name = "pnlAbCharts";
            this.pnlAbCharts.Size = new System.Drawing.Size(839, 536);
            this.pnlAbCharts.TabIndex = 10;
            this.pnlAbCharts.Visible = false;
            // 
            // radEjra
            // 
            this.radEjra.AutoSize = true;
            this.radEjra.BackColor = System.Drawing.Color.Transparent;
            this.radEjra.Location = new System.Drawing.Point(536, 30);
            this.radEjra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radEjra.Name = "radEjra";
            this.radEjra.Size = new System.Drawing.Size(103, 21);
            this.radEjra.TabIndex = 5;
            this.radEjra.Text = "براساس زمان اجرا";
            this.radEjra.UseVisualStyleBackColor = false;
            this.radEjra.Click += new System.EventHandler(this.radChangeChart_Click);
            // 
            // chrAbMablaghPerEjra
            // 
            this.chrAbMablaghPerEjra.BackColor = System.Drawing.Color.Transparent;
            chartArea5.Area3DStyle.Enable3D = true;
            chartArea5.Area3DStyle.Inclination = 10;
            chartArea5.Area3DStyle.PointDepth = 10;
            chartArea5.Area3DStyle.PointGapDepth = 0;
            chartArea5.Area3DStyle.WallWidth = 0;
            chartArea5.BorderColor = System.Drawing.Color.Transparent;
            chartArea5.Name = "ChartArea1";
            this.chrAbMablaghPerEjra.ChartAreas.Add(chartArea5);
            this.chrAbMablaghPerEjra.DataSource = this.abmablaghBindingSource;
            this.chrAbMablaghPerEjra.Location = new System.Drawing.Point(3, 84);
            this.chrAbMablaghPerEjra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrAbMablaghPerEjra.Name = "chrAbMablaghPerEjra";
            series5.ChartArea = "ChartArea1";
            series5.Name = "Series1";
            series5.XValueMember = "tarikh_ejra";
            series5.YValueMembers = "mablagh";
            this.chrAbMablaghPerEjra.Series.Add(series5);
            this.chrAbMablaghPerEjra.Size = new System.Drawing.Size(836, 452);
            this.chrAbMablaghPerEjra.TabIndex = 8;
            this.chrAbMablaghPerEjra.Text = "chart1";
            this.chrAbMablaghPerEjra.Visible = false;
            // 
            // radDoreh
            // 
            this.radDoreh.AutoSize = true;
            this.radDoreh.BackColor = System.Drawing.Color.Transparent;
            this.radDoreh.Checked = true;
            this.radDoreh.Location = new System.Drawing.Point(713, 30);
            this.radDoreh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDoreh.Name = "radDoreh";
            this.radDoreh.Size = new System.Drawing.Size(82, 21);
            this.radDoreh.TabIndex = 4;
            this.radDoreh.TabStop = true;
            this.radDoreh.Text = "براساس دوره";
            this.radDoreh.UseVisualStyleBackColor = false;
            this.radDoreh.Click += new System.EventHandler(this.radChangeChart_Click);
            // 
            // chrAbMablaghPerDoreh
            // 
            this.chrAbMablaghPerDoreh.BackColor = System.Drawing.Color.Transparent;
            chartArea6.Area3DStyle.Enable3D = true;
            chartArea6.Area3DStyle.Inclination = 10;
            chartArea6.Area3DStyle.PointDepth = 10;
            chartArea6.Area3DStyle.PointGapDepth = 0;
            chartArea6.Area3DStyle.WallWidth = 0;
            chartArea6.BorderColor = System.Drawing.Color.Transparent;
            chartArea6.Name = "ChartArea1";
            this.chrAbMablaghPerDoreh.ChartAreas.Add(chartArea6);
            this.chrAbMablaghPerDoreh.DataSource = this.abdorehBindingSource;
            this.chrAbMablaghPerDoreh.Location = new System.Drawing.Point(0, 84);
            this.chrAbMablaghPerDoreh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chrAbMablaghPerDoreh.Name = "chrAbMablaghPerDoreh";
            series6.ChartArea = "ChartArea1";
            series6.Name = "Series1";
            series6.XValueMember = "dcode";
            series6.YValueMembers = "mablagh_ab";
            this.chrAbMablaghPerDoreh.Series.Add(series6);
            this.chrAbMablaghPerDoreh.Size = new System.Drawing.Size(839, 441);
            this.chrAbMablaghPerDoreh.TabIndex = 9;
            this.chrAbMablaghPerDoreh.Text = "chart4";
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "تعرفه های آب";
            // 
            // temp
            // 
            this.temp.GlobalItem = false;
            this.temp.Name = "temp";
            this.temp.Text = "temp";
            // 
            // ab_dorehTableAdapter
            // 
            this.ab_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // ab_mablaghTableAdapter
            // 
            this.ab_mablaghTableAdapter.ClearBeforeFill = true;
            // 
            // sharj_dorehTableAdapter
            // 
            this.sharj_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // sharj_mablaghTableAdapter
            // 
            this.sharj_mablaghTableAdapter.ClearBeforeFill = true;
            // 
            // FrmTarefeRpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 662);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTarefeRpt";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmTarefeRpt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chrMaliatAb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrMaliatSharj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.pnlSharj.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjmablaghBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.pnlSharjCharts.ResumeLayout(false);
            this.pnlSharjCharts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrSharjPerEjra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrSharjPerDoreh)).EndInit();
            this.superTabControlPanel1.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            this.groupPanel5.PerformLayout();
            this.pnlAb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abmablaghBindingSource)).EndInit();
            this.pnlAbCharts.ResumeLayout(false);
            this.pnlAbCharts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrAbMablaghPerEjra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrAbMablaghPerDoreh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private System.Windows.Forms.RadioButton radShowChart;
        private System.Windows.Forms.RadioButton radShowGrid;
        private System.Windows.Forms.DataGridView dataGridView1;
        private AbDataset abDataset;
        private System.Windows.Forms.BindingSource abdorehBindingSource;
        private AbDatasetTableAdapters.ab_dorehTableAdapter ab_dorehTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource abmablaghBindingSource;
        private MainDataSestTableAdapters.ab_mablaghTableAdapter ab_mablaghTableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrAbMablaghPerEjra;
        private DevComponents.DotNetBar.SuperTabItem temp;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private System.Windows.Forms.BindingSource sharjdorehBindingSource;
        private MainDataSestTableAdapters.sharj_dorehTableAdapter sharj_dorehTableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrAbMablaghPerDoreh;
        private System.Windows.Forms.Panel pnlAb;
        private System.Windows.Forms.Panel pnlAbCharts;
        private System.Windows.Forms.BindingSource mainDataSestBindingSource;
        private System.Windows.Forms.RadioButton radEjra;
        private System.Windows.Forms.RadioButton radDoreh;
        private System.Windows.Forms.RadioButton radSharjChart;
        private System.Windows.Forms.RadioButton radSharjGrid;
        private System.Windows.Forms.Panel pnlSharjCharts;
        private System.Windows.Forms.RadioButton radSharjEjra;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrSharjPerEjra;
        private System.Windows.Forms.RadioButton radSharjDoreh;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrSharjPerDoreh;
        private System.Windows.Forms.Panel pnlSharj;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.BindingSource sharjmablaghBindingSource;
        private MainDataSestTableAdapters.sharj_mablaghTableAdapter sharj_mablaghTableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrMaliatAb;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrMaliatSharj;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tarikhtasvibDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tarikhejraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghabDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn darsadmaliatDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}