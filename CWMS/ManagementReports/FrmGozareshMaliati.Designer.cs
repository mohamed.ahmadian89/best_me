﻿namespace CWMS.ManagementReports
{
    partial class FrmGozareshMaliati
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.BtnFindByTarikh = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupPanel6 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvsharjError = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewFADateTimePickerColumn1 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.lblmajmukasr = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.lblTotalMaliat = new DevComponents.DotNetBar.LabelX();
            this.lblTotalMajmu = new DevComponents.DotNetBar.LabelX();
            this.lblTotalBedehi = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.lblMajmuMoshkeldar = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.DgvResult = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majmumabalegh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majmumaliat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ddkasrhezar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjtarikh = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupPanel7 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.DgvAbError = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewFADateTimePickerColumn2 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.lblAbMajmuMoshkeldar = new DevComponents.DotNetBar.LabelX();
            this.lblmajmukasrhezar_ab = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.lblAbMajmuMaliat = new DevComponents.DotNetBar.LabelX();
            this.lblAbMajmuMabalegh = new DevComponents.DotNetBar.LabelX();
            this.lblAbTotalBedehi = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvAbResult = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ddkasr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abtarikh = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.daramadDataset = new CWMS.ManagementReports.DaramadDataset();
            this.sharj_ghabz_PardakhtTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter();
            this.ab_ghabzTableAdapter = new CWMS.ManagementReports.DaramadDatasetTableAdapters.ab_ghabzTableAdapter();
            this.daramadDataset1 = new CWMS.ManagementReports.DaramadDataset();
            this.groupPanel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvsharjError)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAbError)).BeginInit();
            this.groupPanel3.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel4.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.txt_end);
            this.groupPanel4.Controls.Add(this.labelX5);
            this.groupPanel4.Controls.Add(this.txt_start);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.BtnFindByTarikh);
            this.groupPanel4.Controls.Add(this.labelX1);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(36, 8);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(1154, 88);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 68;
            this.groupPanel4.Text = "بر اساس تاریخ پرداخت قبوض";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(561, 19);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(115, 28);
            this.txt_end.TabIndex = 71;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(661, 15);
            this.labelX5.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(119, 36);
            this.labelX5.TabIndex = 72;
            this.labelX5.Text = "تا تاریخ پرداخت :";
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(917, 19);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(100, 28);
            this.txt_start.TabIndex = 69;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(1413, 19);
            this.labelX9.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(119, 36);
            this.labelX9.TabIndex = 70;
            this.labelX9.Text = "از تاریخ :";
            // 
            // BtnFindByTarikh
            // 
            this.BtnFindByTarikh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnFindByTarikh.BackColor = System.Drawing.Color.Transparent;
            this.BtnFindByTarikh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnFindByTarikh.Location = new System.Drawing.Point(19, 19);
            this.BtnFindByTarikh.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
            this.BtnFindByTarikh.Name = "BtnFindByTarikh";
            this.BtnFindByTarikh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.BtnFindByTarikh.Size = new System.Drawing.Size(252, 32);
            this.BtnFindByTarikh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnFindByTarikh.Symbol = "";
            this.BtnFindByTarikh.SymbolColor = System.Drawing.Color.Green;
            this.BtnFindByTarikh.SymbolSize = 9F;
            this.BtnFindByTarikh.TabIndex = 40;
            this.BtnFindByTarikh.Text = "نمایش اطلاعات ";
            this.BtnFindByTarikh.Click += new System.EventHandler(this.BtnFindByTarikh_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(999, 15);
            this.labelX1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(119, 36);
            this.labelX1.TabIndex = 73;
            this.labelX1.Text = "از تاریخ پرداخت  :";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 107);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1182, 632);
            this.tabControl1.TabIndex = 69;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupPanel6);
            this.tabPage1.Controls.Add(this.groupPanel2);
            this.tabPage1.Controls.Add(this.groupPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1174, 599);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "شارژ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupPanel6
            // 
            this.groupPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel6.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel6.Controls.Add(this.dgvsharjError);
            this.groupPanel6.Controls.Add(this.labelX24);
            this.groupPanel6.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel6.Location = new System.Drawing.Point(20, 415);
            this.groupPanel6.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel6.Name = "groupPanel6";
            this.groupPanel6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel6.Size = new System.Drawing.Size(987, 172);
            // 
            // 
            // 
            this.groupPanel6.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel6.Style.BackColorGradientAngle = 90;
            this.groupPanel6.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel6.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderBottomWidth = 1;
            this.groupPanel6.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel6.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderLeftWidth = 1;
            this.groupPanel6.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderRightWidth = 1;
            this.groupPanel6.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderTopWidth = 1;
            this.groupPanel6.Style.CornerDiameter = 4;
            this.groupPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel6.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel6.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel6.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel6.TabIndex = 73;
            this.groupPanel6.Text = "لطفا قراردادهای زیر را در بخش شارژ مورد بررسی قرار دهید";
            // 
            // dgvsharjError
            // 
            this.dgvsharjError.AllowUserToAddRows = false;
            this.dgvsharjError.AllowUserToDeleteRows = false;
            this.dgvsharjError.AllowUserToResizeColumns = false;
            this.dgvsharjError.AllowUserToResizeRows = false;
            this.dgvsharjError.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvsharjError.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvsharjError.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgvsharjError.ColumnHeadersHeight = 28;
            this.dgvsharjError.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewFADateTimePickerColumn1});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvsharjError.DefaultCellStyle = dataGridViewCellStyle29;
            this.dgvsharjError.EnableHeadersVisualStyles = false;
            this.dgvsharjError.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvsharjError.Location = new System.Drawing.Point(19, 6);
            this.dgvsharjError.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.dgvsharjError.Name = "dgvsharjError";
            this.dgvsharjError.ReadOnly = true;
            this.dgvsharjError.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvsharjError.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvsharjError.RowHeadersVisible = false;
            this.dgvsharjError.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvsharjError.Size = new System.Drawing.Size(947, 131);
            this.dgvsharjError.TabIndex = 72;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.FillWeight = 50F;
            this.dataGridViewTextBoxColumn6.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.FillWeight = 50F;
            this.dataGridViewTextBoxColumn7.HeaderText = "قرارداد";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle26.Format = "0,0";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn8.HeaderText = "بدهی";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle27.Format = "0,0";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn9.HeaderText = "مجموع مبالغ بدهی";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle28.Format = "0,0";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn10.HeaderText = "مجموع مالیات";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.FillWeight = 50F;
            this.dataGridViewTextBoxColumn11.HeaderText = "کسرهزار";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewFADateTimePickerColumn1
            // 
            this.dataGridViewFADateTimePickerColumn1.HeaderText = "تاریخ پرداخت";
            this.dataGridViewFADateTimePickerColumn1.Name = "dataGridViewFADateTimePickerColumn1";
            this.dataGridViewFADateTimePickerColumn1.ReadOnly = true;
            this.dataGridViewFADateTimePickerColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewFADateTimePickerColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(1413, 19);
            this.labelX24.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX24.Name = "labelX24";
            this.labelX24.Size = new System.Drawing.Size(119, 36);
            this.labelX24.TabIndex = 70;
            this.labelX24.Text = "از تاریخ :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.labelX18);
            this.groupPanel2.Controls.Add(this.lblmajmukasr);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Controls.Add(this.lblTotalMaliat);
            this.groupPanel2.Controls.Add(this.lblTotalMajmu);
            this.groupPanel2.Controls.Add(this.lblTotalBedehi);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.Controls.Add(this.lblMajmuMoshkeldar);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(20, 316);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(987, 93);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 72;
            this.groupPanel2.Text = "گزارشات آماری";
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(783, 31);
            this.labelX18.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(183, 36);
            this.labelX18.TabIndex = 84;
            this.labelX18.Text = "مجموع بدهی قراردادهای مشکل دار:";
            // 
            // lblmajmukasr
            // 
            this.lblmajmukasr.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblmajmukasr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblmajmukasr.ForeColor = System.Drawing.Color.Black;
            this.lblmajmukasr.Location = new System.Drawing.Point(32, 23);
            this.lblmajmukasr.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblmajmukasr.Name = "lblmajmukasr";
            this.lblmajmukasr.Size = new System.Drawing.Size(135, 36);
            this.lblmajmukasr.TabIndex = 83;
            this.lblmajmukasr.Text = " ";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(168, 23);
            this.labelX11.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(94, 36);
            this.labelX11.TabIndex = 82;
            this.labelX11.Text = "مجموع کسر هزار  :";
            // 
            // lblTotalMaliat
            // 
            this.lblTotalMaliat.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTotalMaliat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalMaliat.ForeColor = System.Drawing.Color.Black;
            this.lblTotalMaliat.Location = new System.Drawing.Point(286, 29);
            this.lblTotalMaliat.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblTotalMaliat.Name = "lblTotalMaliat";
            this.lblTotalMaliat.Size = new System.Drawing.Size(209, 36);
            this.lblTotalMaliat.TabIndex = 79;
            this.lblTotalMaliat.Text = " ";
            // 
            // lblTotalMajmu
            // 
            this.lblTotalMajmu.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTotalMajmu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalMajmu.ForeColor = System.Drawing.Color.Black;
            this.lblTotalMajmu.Location = new System.Drawing.Point(283, -1);
            this.lblTotalMajmu.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblTotalMajmu.Name = "lblTotalMajmu";
            this.lblTotalMajmu.Size = new System.Drawing.Size(209, 36);
            this.lblTotalMajmu.TabIndex = 78;
            this.lblTotalMajmu.Text = " ";
            // 
            // lblTotalBedehi
            // 
            this.lblTotalBedehi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTotalBedehi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalBedehi.ForeColor = System.Drawing.Color.Black;
            this.lblTotalBedehi.Location = new System.Drawing.Point(683, 4);
            this.lblTotalBedehi.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblTotalBedehi.Name = "lblTotalBedehi";
            this.lblTotalBedehi.Size = new System.Drawing.Size(209, 36);
            this.lblTotalBedehi.TabIndex = 77;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(452, 29);
            this.labelX7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(123, 36);
            this.labelX7.TabIndex = 76;
            this.labelX7.Text = "مجموع مالیات :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(467, -1);
            this.labelX6.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(123, 36);
            this.labelX6.TabIndex = 75;
            this.labelX6.Text = "مجموع مبالغ بدهی :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(867, 4);
            this.labelX2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(99, 36);
            this.labelX2.TabIndex = 74;
            this.labelX2.Text = "مجموع بدهی :";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(1413, 19);
            this.labelX4.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(119, 36);
            this.labelX4.TabIndex = 70;
            this.labelX4.Text = "از تاریخ :";
            // 
            // lblMajmuMoshkeldar
            // 
            this.lblMajmuMoshkeldar.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMajmuMoshkeldar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMajmuMoshkeldar.ForeColor = System.Drawing.Color.Black;
            this.lblMajmuMoshkeldar.Location = new System.Drawing.Point(579, 29);
            this.lblMajmuMoshkeldar.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblMajmuMoshkeldar.Name = "lblMajmuMoshkeldar";
            this.lblMajmuMoshkeldar.Size = new System.Drawing.Size(194, 36);
            this.lblMajmuMoshkeldar.TabIndex = 85;
            this.lblMajmuMoshkeldar.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.DgvResult);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(16, 17);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1150, 296);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 71;
            this.groupPanel1.Text = "بر اساس تاریخ پرداخت قبوض";
            // 
            // DgvResult
            // 
            this.DgvResult.AllowUserToAddRows = false;
            this.DgvResult.AllowUserToDeleteRows = false;
            this.DgvResult.AllowUserToResizeColumns = false;
            this.DgvResult.AllowUserToResizeRows = false;
            this.DgvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.DgvResult.ColumnHeadersHeight = 28;
            this.DgvResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dcode,
            this.gharardad,
            this.bedehi,
            this.majmumabalegh,
            this.majmumaliat,
            this.ddkasrhezar,
            this.sharjtarikh});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle35.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvResult.DefaultCellStyle = dataGridViewCellStyle35;
            this.DgvResult.EnableHeadersVisualStyles = false;
            this.DgvResult.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DgvResult.Location = new System.Drawing.Point(19, 19);
            this.DgvResult.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.DgvResult.Name = "DgvResult";
            this.DgvResult.ReadOnly = true;
            this.DgvResult.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.DgvResult.RowHeadersVisible = false;
            this.DgvResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvResult.Size = new System.Drawing.Size(1120, 237);
            this.DgvResult.TabIndex = 71;
            // 
            // dcode
            // 
            this.dcode.FillWeight = 50F;
            this.dcode.HeaderText = "دوره";
            this.dcode.Name = "dcode";
            this.dcode.ReadOnly = true;
            // 
            // gharardad
            // 
            this.gharardad.FillWeight = 50F;
            this.gharardad.HeaderText = "قرارداد";
            this.gharardad.Name = "gharardad";
            this.gharardad.ReadOnly = true;
            // 
            // bedehi
            // 
            dataGridViewCellStyle32.Format = "0,0";
            this.bedehi.DefaultCellStyle = dataGridViewCellStyle32;
            this.bedehi.HeaderText = "بدهی";
            this.bedehi.Name = "bedehi";
            this.bedehi.ReadOnly = true;
            // 
            // majmumabalegh
            // 
            dataGridViewCellStyle33.Format = "0,0";
            this.majmumabalegh.DefaultCellStyle = dataGridViewCellStyle33;
            this.majmumabalegh.HeaderText = "مجموع مبالغ بدهی";
            this.majmumabalegh.Name = "majmumabalegh";
            this.majmumabalegh.ReadOnly = true;
            // 
            // majmumaliat
            // 
            dataGridViewCellStyle34.Format = "0,0";
            this.majmumaliat.DefaultCellStyle = dataGridViewCellStyle34;
            this.majmumaliat.HeaderText = "مجموع مالیات";
            this.majmumaliat.Name = "majmumaliat";
            this.majmumaliat.ReadOnly = true;
            // 
            // ddkasrhezar
            // 
            this.ddkasrhezar.FillWeight = 50F;
            this.ddkasrhezar.HeaderText = "کسرهزار";
            this.ddkasrhezar.Name = "ddkasrhezar";
            this.ddkasrhezar.ReadOnly = true;
            // 
            // sharjtarikh
            // 
            this.sharjtarikh.HeaderText = "تاریخ پرداخت";
            this.sharjtarikh.Name = "sharjtarikh";
            this.sharjtarikh.ReadOnly = true;
            this.sharjtarikh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sharjtarikh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(1413, 19);
            this.labelX3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(119, 36);
            this.labelX3.TabIndex = 70;
            this.labelX3.Text = "از تاریخ :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupPanel7);
            this.tabPage2.Controls.Add(this.groupPanel3);
            this.tabPage2.Controls.Add(this.groupPanel5);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1174, 599);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "آب";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupPanel7
            // 
            this.groupPanel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel7.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel7.Controls.Add(this.DgvAbError);
            this.groupPanel7.Controls.Add(this.labelX8);
            this.groupPanel7.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel7.Location = new System.Drawing.Point(20, 421);
            this.groupPanel7.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel7.Name = "groupPanel7";
            this.groupPanel7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel7.Size = new System.Drawing.Size(1133, 172);
            // 
            // 
            // 
            this.groupPanel7.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel7.Style.BackColorGradientAngle = 90;
            this.groupPanel7.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel7.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderBottomWidth = 1;
            this.groupPanel7.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel7.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderLeftWidth = 1;
            this.groupPanel7.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderRightWidth = 1;
            this.groupPanel7.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderTopWidth = 1;
            this.groupPanel7.Style.CornerDiameter = 4;
            this.groupPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel7.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel7.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel7.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel7.TabIndex = 75;
            this.groupPanel7.Text = "لطفا قراردادهای زیر را در بخش آب مورد بررسی قرار دهید";
            // 
            // DgvAbError
            // 
            this.DgvAbError.AllowUserToAddRows = false;
            this.DgvAbError.AllowUserToDeleteRows = false;
            this.DgvAbError.AllowUserToResizeColumns = false;
            this.DgvAbError.AllowUserToResizeRows = false;
            this.DgvAbError.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvAbError.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAbError.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.DgvAbError.ColumnHeadersHeight = 28;
            this.DgvAbError.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewFADateTimePickerColumn2});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle41.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAbError.DefaultCellStyle = dataGridViewCellStyle41;
            this.DgvAbError.EnableHeadersVisualStyles = false;
            this.DgvAbError.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DgvAbError.Location = new System.Drawing.Point(19, 6);
            this.DgvAbError.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.DgvAbError.Name = "DgvAbError";
            this.DgvAbError.ReadOnly = true;
            this.DgvAbError.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvAbError.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.DgvAbError.RowHeadersVisible = false;
            this.DgvAbError.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvAbError.Size = new System.Drawing.Size(1086, 131);
            this.DgvAbError.TabIndex = 72;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.FillWeight = 50F;
            this.dataGridViewTextBoxColumn12.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.FillWeight = 50F;
            this.dataGridViewTextBoxColumn13.HeaderText = "قرارداد";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle38.Format = "0,0";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn14.HeaderText = "بدهی";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle39.Format = "0,0";
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn15.HeaderText = "مجموع مبالغ بدهی";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle40.Format = "0,0";
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewTextBoxColumn16.HeaderText = "مجموع مالیات";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.FillWeight = 50F;
            this.dataGridViewTextBoxColumn17.HeaderText = "کسرهزار";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewFADateTimePickerColumn2
            // 
            this.dataGridViewFADateTimePickerColumn2.HeaderText = "تاریخ پرداخت";
            this.dataGridViewFADateTimePickerColumn2.Name = "dataGridViewFADateTimePickerColumn2";
            this.dataGridViewFADateTimePickerColumn2.ReadOnly = true;
            this.dataGridViewFADateTimePickerColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewFADateTimePickerColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(1413, 19);
            this.labelX8.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(119, 36);
            this.labelX8.TabIndex = 70;
            this.labelX8.Text = "از تاریخ :";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.labelX17);
            this.groupPanel3.Controls.Add(this.lblAbMajmuMoshkeldar);
            this.groupPanel3.Controls.Add(this.lblmajmukasrhezar_ab);
            this.groupPanel3.Controls.Add(this.labelX10);
            this.groupPanel3.Controls.Add(this.lblAbMajmuMaliat);
            this.groupPanel3.Controls.Add(this.lblAbMajmuMabalegh);
            this.groupPanel3.Controls.Add(this.lblAbTotalBedehi);
            this.groupPanel3.Controls.Add(this.labelX12);
            this.groupPanel3.Controls.Add(this.labelX13);
            this.groupPanel3.Controls.Add(this.labelX14);
            this.groupPanel3.Controls.Add(this.labelX15);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(20, 328);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(1133, 88);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 74;
            this.groupPanel3.Text = "گزارشات آماری";
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(793, 23);
            this.labelX17.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(183, 36);
            this.labelX17.TabIndex = 86;
            this.labelX17.Text = "مجموع بدهی قراردادهای مشکل دار:";
            // 
            // lblAbMajmuMoshkeldar
            // 
            this.lblAbMajmuMoshkeldar.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAbMajmuMoshkeldar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbMajmuMoshkeldar.ForeColor = System.Drawing.Color.Black;
            this.lblAbMajmuMoshkeldar.Location = new System.Drawing.Point(589, 23);
            this.lblAbMajmuMoshkeldar.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblAbMajmuMoshkeldar.Name = "lblAbMajmuMoshkeldar";
            this.lblAbMajmuMoshkeldar.Size = new System.Drawing.Size(194, 36);
            this.lblAbMajmuMoshkeldar.TabIndex = 87;
            this.lblAbMajmuMoshkeldar.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblmajmukasrhezar_ab
            // 
            this.lblmajmukasrhezar_ab.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblmajmukasrhezar_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblmajmukasrhezar_ab.ForeColor = System.Drawing.Color.Black;
            this.lblmajmukasrhezar_ab.Location = new System.Drawing.Point(-2, 23);
            this.lblmajmukasrhezar_ab.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblmajmukasrhezar_ab.Name = "lblmajmukasrhezar_ab";
            this.lblmajmukasrhezar_ab.Size = new System.Drawing.Size(129, 36);
            this.lblmajmukasrhezar_ab.TabIndex = 81;
            this.lblmajmukasrhezar_ab.Text = " ";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(124, 23);
            this.labelX10.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(94, 36);
            this.labelX10.TabIndex = 80;
            this.labelX10.Text = "مجموع کسر هزار  :";
            // 
            // lblAbMajmuMaliat
            // 
            this.lblAbMajmuMaliat.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAbMajmuMaliat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbMajmuMaliat.ForeColor = System.Drawing.Color.Black;
            this.lblAbMajmuMaliat.Location = new System.Drawing.Point(250, 28);
            this.lblAbMajmuMaliat.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblAbMajmuMaliat.Name = "lblAbMajmuMaliat";
            this.lblAbMajmuMaliat.Size = new System.Drawing.Size(209, 36);
            this.lblAbMajmuMaliat.TabIndex = 79;
            this.lblAbMajmuMaliat.Text = " ";
            // 
            // lblAbMajmuMabalegh
            // 
            this.lblAbMajmuMabalegh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAbMajmuMabalegh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbMajmuMabalegh.ForeColor = System.Drawing.Color.Black;
            this.lblAbMajmuMabalegh.Location = new System.Drawing.Point(250, 3);
            this.lblAbMajmuMabalegh.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblAbMajmuMabalegh.Name = "lblAbMajmuMabalegh";
            this.lblAbMajmuMabalegh.Size = new System.Drawing.Size(209, 36);
            this.lblAbMajmuMabalegh.TabIndex = 78;
            this.lblAbMajmuMabalegh.Text = " ";
            // 
            // lblAbTotalBedehi
            // 
            this.lblAbTotalBedehi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAbTotalBedehi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAbTotalBedehi.ForeColor = System.Drawing.Color.Black;
            this.lblAbTotalBedehi.Location = new System.Drawing.Point(683, -4);
            this.lblAbTotalBedehi.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lblAbTotalBedehi.Name = "lblAbTotalBedehi";
            this.lblAbTotalBedehi.Size = new System.Drawing.Size(209, 36);
            this.lblAbTotalBedehi.TabIndex = 77;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(416, 28);
            this.labelX12.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(123, 36);
            this.labelX12.TabIndex = 76;
            this.labelX12.Text = "مجموع مالیات :";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(434, 3);
            this.labelX13.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(123, 36);
            this.labelX13.TabIndex = 75;
            this.labelX13.Text = "مجموع مبالغ بدهی :";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(867, -4);
            this.labelX14.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(99, 36);
            this.labelX14.TabIndex = 74;
            this.labelX14.Text = "مجموع بدهی :";
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(1413, 19);
            this.labelX15.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(119, 36);
            this.labelX15.TabIndex = 70;
            this.labelX15.Text = "از تاریخ :";
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel5.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.dgvAbResult);
            this.groupPanel5.Controls.Add(this.labelX16);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(20, 17);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(1133, 309);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 73;
            this.groupPanel5.Text = "بر اساس تاریخ پرداخت قبوض";
            // 
            // dgvAbResult
            // 
            this.dgvAbResult.AllowUserToAddRows = false;
            this.dgvAbResult.AllowUserToDeleteRows = false;
            this.dgvAbResult.AllowUserToResizeColumns = false;
            this.dgvAbResult.AllowUserToResizeRows = false;
            this.dgvAbResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAbResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAbResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvAbResult.ColumnHeadersHeight = 28;
            this.dgvAbResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.ddkasr,
            this.abtarikh});
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle47.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle47.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAbResult.DefaultCellStyle = dataGridViewCellStyle47;
            this.dgvAbResult.EnableHeadersVisualStyles = false;
            this.dgvAbResult.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvAbResult.Location = new System.Drawing.Point(19, 19);
            this.dgvAbResult.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.dgvAbResult.Name = "dgvAbResult";
            this.dgvAbResult.ReadOnly = true;
            this.dgvAbResult.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAbResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this.dgvAbResult.RowHeadersVisible = false;
            this.dgvAbResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAbResult.Size = new System.Drawing.Size(1103, 252);
            this.dgvAbResult.TabIndex = 71;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 50F;
            this.dataGridViewTextBoxColumn2.HeaderText = "قرارداد";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle44.Format = "0,0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn3.HeaderText = "بدهی";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle45.Format = "0,0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewTextBoxColumn4.HeaderText = "مجموع مبالغ بدهی";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle46.Format = "0,0";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn5.HeaderText = "مجموع مالیات";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // ddkasr
            // 
            this.ddkasr.FillWeight = 50F;
            this.ddkasr.HeaderText = "کسر هزار";
            this.ddkasr.Name = "ddkasr";
            this.ddkasr.ReadOnly = true;
            // 
            // abtarikh
            // 
            this.abtarikh.HeaderText = "تاریخ پرداخت";
            this.abtarikh.Name = "abtarikh";
            this.abtarikh.ReadOnly = true;
            this.abtarikh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.abtarikh.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // labelX16
            // 
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(1413, 19);
            this.labelX16.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(119, 36);
            this.labelX16.TabIndex = 70;
            this.labelX16.Text = "از تاریخ :";
            // 
            // daramadDataset
            // 
            this.daramadDataset.DataSetName = "DaramadDataset";
            this.daramadDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sharj_ghabz_PardakhtTableAdapter
            // 
            this.sharj_ghabz_PardakhtTableAdapter.ClearBeforeFill = true;
            // 
            // ab_ghabzTableAdapter
            // 
            this.ab_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // daramadDataset1
            // 
            this.daramadDataset1.DataSetName = "DaramadDataset";
            this.daramadDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // FrmGozareshMaliati
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 740);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupPanel4);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmGozareshMaliati";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "گزارش پیشرفته مالیاتی";
            this.Load += new System.EventHandler(this.FrmGozareshMaliati_Load);
            this.groupPanel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvsharjError)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAbError)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadDataset1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private DevComponents.DotNetBar.LabelX labelX5;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX BtnFindByTarikh;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DaramadDataset daramadDataset;
        private DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter sharj_ghabz_PardakhtTableAdapter;
        private DaramadDatasetTableAdapters.ab_ghabzTableAdapter ab_ghabzTableAdapter;
        private DaramadDataset daramadDataset1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvResult;
        private DevComponents.DotNetBar.LabelX labelX3;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.LabelX lblTotalMaliat;
        private DevComponents.DotNetBar.LabelX lblTotalMajmu;
        private DevComponents.DotNetBar.LabelX lblTotalBedehi;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX lblAbMajmuMaliat;
        private DevComponents.DotNetBar.LabelX lblAbMajmuMabalegh;
        private DevComponents.DotNetBar.LabelX lblAbTotalBedehi;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAbResult;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX lblmajmukasrhezar_ab;
        private DevComponents.DotNetBar.LabelX lblmajmukasr;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardad;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedehi;
        private System.Windows.Forms.DataGridViewTextBoxColumn majmumabalegh;
        private System.Windows.Forms.DataGridViewTextBoxColumn majmumaliat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ddkasrhezar;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn sharjtarikh;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ddkasr;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn abtarikh;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel6;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvsharjError;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewFADateTimePickerColumn1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel7;
        private DevComponents.DotNetBar.Controls.DataGridViewX DgvAbError;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewFADateTimePickerColumn2;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX lblMajmuMoshkeldar;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.LabelX lblAbMajmuMoshkeldar;
    }
}