﻿namespace CWMS.ManagementReports
{
    partial class FrmNahvePardakhtReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }











        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.grpHeader = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblTyoe = new DevComponents.DotNetBar.LabelX();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.grpHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpHeader
            // 
            this.grpHeader.BackColor = System.Drawing.Color.White;
            this.grpHeader.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpHeader.Controls.Add(this.lblTyoe);
            this.grpHeader.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpHeader.Location = new System.Drawing.Point(5, 20);
            this.grpHeader.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.grpHeader.Name = "grpHeader";
            this.grpHeader.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpHeader.Size = new System.Drawing.Size(875, 60);
            // 
            // 
            // 
            this.grpHeader.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpHeader.Style.BackColorGradientAngle = 90;
            this.grpHeader.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpHeader.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpHeader.Style.BorderBottomWidth = 1;
            this.grpHeader.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpHeader.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpHeader.Style.BorderLeftWidth = 1;
            this.grpHeader.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpHeader.Style.BorderRightWidth = 1;
            this.grpHeader.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpHeader.Style.BorderTopWidth = 1;
            this.grpHeader.Style.CornerDiameter = 4;
            this.grpHeader.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpHeader.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpHeader.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpHeader.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpHeader.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpHeader.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpHeader.TabIndex = 69;
            this.grpHeader.Text = "درامد های مالیاتی";
            // 
            // lblTyoe
            // 
            this.lblTyoe.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTyoe.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTyoe.Location = new System.Drawing.Point(297, 6);
            this.lblTyoe.Name = "lblTyoe";
            this.lblTyoe.Size = new System.Drawing.Size(244, 23);
            this.lblTyoe.TabIndex = 0;
            this.lblTyoe.Text = "نمایش در آمدهای مالیاتی مربوط به دوره های آب";
            this.lblTyoe.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // chart1
            // 
            this.chart1.BorderSkin.BackColor = System.Drawing.Color.White;
            this.chart1.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Raised;
            chartArea1.AlignmentOrientation = ((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations)((System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Vertical | System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal)));
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Area3DStyle.PointDepth = 30;
            chartArea1.Area3DStyle.PointGapDepth = 30;
            chartArea1.Area3DStyle.WallWidth = 2;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX2.IsLabelAutoFit = false;
            chartArea1.AxisX2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisX2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY2.IsLabelAutoFit = false;
            chartArea1.AxisY2.LabelStyle.Font = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.AxisY2.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackSecondaryColor = System.Drawing.Color.White;
            chartArea1.BorderColor = System.Drawing.Color.White;
            chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.White;
            legend1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            legend1.TitleFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(25, 97);
            this.chart1.Name = "chart1";
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn100;
            series1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.ShadowColor = System.Drawing.Color.White;
            series1.YValuesPerPoint = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn100;
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn100;
            series3.Legend = "Legend1";
            series3.Name = "Series3";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn100;
            series4.Legend = "Legend1";
            series4.Name = "Series4";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn100;
            series5.Legend = "Legend1";
            series5.Name = "Series5";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(825, 344);
            this.chart1.TabIndex = 68;
            this.chart1.Text = "dsds";
            // 
            // FrmNahvePardakhtReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 461);
            this.Controls.Add(this.grpHeader);
            this.Controls.Add(this.chart1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmNahvePardakhtReport";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmNahvePardakhtReport_Load);
            this.grpHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpHeader;
        private DevComponents.DotNetBar.LabelX lblTyoe;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}