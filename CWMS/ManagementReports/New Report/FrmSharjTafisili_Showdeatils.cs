﻿using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FarsiLibrary.Utils;

namespace CWMS.ManagementReports.New_Report
{
    public partial class FrmSharjTafisili_Showdeatils : MyMetroForm
    {
        string Type_of_gozaresh = "";
        public FrmSharjTafisili_Showdeatils(string grpinfo_t,string doreh,string type)
        {
            InitializeComponent();
            grpinfo.Text = grpinfo_t;
            lblDoreh.Text = doreh;

            if (type == "full_pardkht")
            {
                sharjTafsili_detailsTableAdapter.Fill(reportDS.sharjTafsili_details, Convert.ToInt32(doreh), 1);
            }
            else if(type=="bedehkaran")
            {
                sharjTafsili_detailsTableAdapter.FillBedehkaran(reportDS.sharjTafsili_details, Convert.ToInt32(doreh));
            }
            else
            {
                sharjTafsili_detailsTableAdapter.Fillbestankaran(reportDS.sharjTafsili_details, Convert.ToInt32(doreh));

            }
            lbl_mohtarekin_count.Text = sharjTafsilidetailsBindingSource.Count.ToString();
            Type_of_gozaresh = grpinfo_t;
        }

        private void FrmSharjTafisili_Showdeatils_Load(object sender, EventArgs e)
        {

        }

        private void txtFindByGhararda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gharardad.PerformClick();
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            sharjTafsilidetailsBindingSource.RemoveFilter();
        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text.Trim() != "")
            {
                sharjTafsilidetailsBindingSource.Filter = "قرارداد=" + txtFindByGhararda.Text;
            }
            else
                txtFindByGhararda.Focus();
        }

        private void lblDoreh_Click(object sender, EventArgs e)
        {

        }

        private void btn_print_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            CrystalReportViewer viewer = new CrystalReportViewer();
            CrysReports.bedehkar_bestankar.sharj_bedehi_bestankari
            report1 = new CrysReports.bedehkar_bestankar.sharj_bedehi_bestankari();
            ManagementReports.New_Report.ReportDSTableAdapters.settingTableAdapter ta = new ReportDSTableAdapters.settingTableAdapter();
            ta.Fill(reportDS.setting);


            report1.SetDataSource(reportDS);
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("type", Type_of_gozaresh);
            report1.SetParameterValue("doreh", lblDoreh.Text);

            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

        }

       
    }
}
