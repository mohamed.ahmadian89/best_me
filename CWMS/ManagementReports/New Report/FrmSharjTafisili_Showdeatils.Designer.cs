﻿namespace CWMS.ManagementReports.New_Report
{
    partial class FrmSharjTafisili_Showdeatils
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpinfo = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_mohtarekin_count = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.lblDoreh = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.btn_find_by_gharardad = new DevComponents.DotNetBar.ButtonX();
            this.txtFindByGhararda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dgvAb = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.قراردادDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ناممشترکDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کدقبضDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مبلغکلDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندندهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کسرهزارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بستانکاریDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjTafsilidetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportDS = new CWMS.ManagementReports.New_Report.ReportDS();
            this.btn_print = new DevComponents.DotNetBar.ButtonX();
            this.sharjTafsili_detailsTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.sharjTafsili_detailsTableAdapter();
            this.grpinfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjTafsilidetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).BeginInit();
            this.SuspendLayout();
            // 
            // grpinfo
            // 
            this.grpinfo.BackColor = System.Drawing.Color.Azure;
            this.grpinfo.CanvasColor = System.Drawing.Color.Azure;
            this.grpinfo.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpinfo.Controls.Add(this.lbl_mohtarekin_count);
            this.grpinfo.Controls.Add(this.labelX4);
            this.grpinfo.Controls.Add(this.lblDoreh);
            this.grpinfo.Controls.Add(this.labelX2);
            this.grpinfo.Controls.Add(this.btn_create_new_doreh);
            this.grpinfo.Controls.Add(this.btn_find_by_gharardad);
            this.grpinfo.Controls.Add(this.txtFindByGhararda);
            this.grpinfo.Controls.Add(this.labelX1);
            this.grpinfo.Controls.Add(this.dgvAb);
            this.grpinfo.Controls.Add(this.btn_print);
            this.grpinfo.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpinfo.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grpinfo.Location = new System.Drawing.Point(12, 13);
            this.grpinfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpinfo.Name = "grpinfo";
            this.grpinfo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpinfo.Size = new System.Drawing.Size(937, 489);
            // 
            // 
            // 
            this.grpinfo.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpinfo.Style.BackColorGradientAngle = 90;
            this.grpinfo.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpinfo.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderBottomWidth = 1;
            this.grpinfo.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpinfo.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderLeftWidth = 1;
            this.grpinfo.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderRightWidth = 1;
            this.grpinfo.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderTopWidth = 1;
            this.grpinfo.Style.CornerDiameter = 4;
            this.grpinfo.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpinfo.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpinfo.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpinfo.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpinfo.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpinfo.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpinfo.TabIndex = 6;
            this.grpinfo.Text = "مشاهده اطلاعات مشترکین";
            // 
            // lbl_mohtarekin_count
            // 
            this.lbl_mohtarekin_count.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_mohtarekin_count.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_mohtarekin_count.Font = new System.Drawing.Font("B Yekan", 13F);
            this.lbl_mohtarekin_count.ForeColor = System.Drawing.Color.Black;
            this.lbl_mohtarekin_count.Location = new System.Drawing.Point(535, -2);
            this.lbl_mohtarekin_count.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_mohtarekin_count.Name = "lbl_mohtarekin_count";
            this.lbl_mohtarekin_count.Size = new System.Drawing.Size(130, 38);
            this.lbl_mohtarekin_count.TabIndex = 96;
            this.lbl_mohtarekin_count.Text = " ";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(661, 5);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(86, 25);
            this.labelX4.TabIndex = 95;
            this.labelX4.Text = "تعداد مشترکین :";
            // 
            // lblDoreh
            // 
            this.lblDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDoreh.Font = new System.Drawing.Font("B Yekan", 13F);
            this.lblDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblDoreh.Location = new System.Drawing.Point(744, -3);
            this.lblDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.lblDoreh.Name = "lblDoreh";
            this.lblDoreh.Size = new System.Drawing.Size(130, 40);
            this.lblDoreh.TabIndex = 94;
            this.lblDoreh.Text = " ";
            this.lblDoreh.Click += new System.EventHandler(this.lblDoreh_Click);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(845, 5);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(67, 25);
            this.labelX2.TabIndex = 93;
            this.labelX2.Text = "دوره :";
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(640, 428);
            this.btn_create_new_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(272, 25);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 92;
            this.btn_create_new_doreh.Text = "نمایش اطلاعات همه مشترکین";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // btn_find_by_gharardad
            // 
            this.btn_find_by_gharardad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_by_gharardad.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_by_gharardad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_by_gharardad.Location = new System.Drawing.Point(14, 6);
            this.btn_find_by_gharardad.Margin = new System.Windows.Forms.Padding(4);
            this.btn_find_by_gharardad.Name = "btn_find_by_gharardad";
            this.btn_find_by_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find_by_gharardad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_by_gharardad.Size = new System.Drawing.Size(38, 25);
            this.btn_find_by_gharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_by_gharardad.Symbol = "";
            this.btn_find_by_gharardad.SymbolColor = System.Drawing.Color.Teal;
            this.btn_find_by_gharardad.SymbolSize = 9F;
            this.btn_find_by_gharardad.TabIndex = 91;
            this.btn_find_by_gharardad.Click += new System.EventHandler(this.btn_find_by_gharardad_Click);
            // 
            // txtFindByGhararda
            // 
            this.txtFindByGhararda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFindByGhararda.Border.Class = "TextBoxBorder";
            this.txtFindByGhararda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFindByGhararda.DisabledBackColor = System.Drawing.Color.White;
            this.txtFindByGhararda.ForeColor = System.Drawing.Color.Black;
            this.txtFindByGhararda.Location = new System.Drawing.Point(60, 4);
            this.txtFindByGhararda.Margin = new System.Windows.Forms.Padding(4);
            this.txtFindByGhararda.Name = "txtFindByGhararda";
            this.txtFindByGhararda.PreventEnterBeep = true;
            this.txtFindByGhararda.Size = new System.Drawing.Size(80, 28);
            this.txtFindByGhararda.TabIndex = 90;
            this.txtFindByGhararda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFindByGhararda_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(119, 6);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(130, 25);
            this.labelX1.TabIndex = 89;
            this.labelX1.Text = "قرارداد مشترک :";
            // 
            // dgvAb
            // 
            this.dgvAb.AllowUserToAddRows = false;
            this.dgvAb.AllowUserToDeleteRows = false;
            this.dgvAb.AllowUserToResizeColumns = false;
            this.dgvAb.AllowUserToResizeRows = false;
            this.dgvAb.AutoGenerateColumns = false;
            this.dgvAb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAb.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvAb.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAb.ColumnHeadersHeight = 28;
            this.dgvAb.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.قراردادDataGridViewTextBoxColumn,
            this.ناممشترکDataGridViewTextBoxColumn,
            this.کدقبضDataGridViewTextBoxColumn,
            this.مبلغکلDataGridViewTextBoxColumn,
            this.ماندندهDataGridViewTextBoxColumn,
            this.کسرهزارDataGridViewTextBoxColumn,
            this.بستانکاریDataGridViewTextBoxColumn});
            this.dgvAb.DataSource = this.sharjTafsilidetailsBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAb.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAb.EnableHeadersVisualStyles = false;
            this.dgvAb.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvAb.Location = new System.Drawing.Point(12, 45);
            this.dgvAb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvAb.Name = "dgvAb";
            this.dgvAb.ReadOnly = true;
            this.dgvAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAb.RowHeadersVisible = false;
            this.dgvAb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAb.Size = new System.Drawing.Size(915, 375);
            this.dgvAb.TabIndex = 88;
            // 
            // قراردادDataGridViewTextBoxColumn
            // 
            this.قراردادDataGridViewTextBoxColumn.DataPropertyName = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn.FillWeight = 40F;
            this.قراردادDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.قراردادDataGridViewTextBoxColumn.Name = "قراردادDataGridViewTextBoxColumn";
            this.قراردادDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ناممشترکDataGridViewTextBoxColumn
            // 
            this.ناممشترکDataGridViewTextBoxColumn.DataPropertyName = "نام مشترک";
            this.ناممشترکDataGridViewTextBoxColumn.FillWeight = 160F;
            this.ناممشترکDataGridViewTextBoxColumn.HeaderText = "نام مشترک";
            this.ناممشترکDataGridViewTextBoxColumn.Name = "ناممشترکDataGridViewTextBoxColumn";
            this.ناممشترکDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // کدقبضDataGridViewTextBoxColumn
            // 
            this.کدقبضDataGridViewTextBoxColumn.DataPropertyName = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.FillWeight = 60F;
            this.کدقبضDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.Name = "کدقبضDataGridViewTextBoxColumn";
            this.کدقبضDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مبلغکلDataGridViewTextBoxColumn
            // 
            this.مبلغکلDataGridViewTextBoxColumn.DataPropertyName = "مبلغ کل";
            this.مبلغکلDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.مبلغکلDataGridViewTextBoxColumn.Name = "مبلغکلDataGridViewTextBoxColumn";
            this.مبلغکلDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندندهDataGridViewTextBoxColumn
            // 
            this.ماندندهDataGridViewTextBoxColumn.DataPropertyName = "ماندنده";
            this.ماندندهDataGridViewTextBoxColumn.HeaderText = "مانده";
            this.ماندندهDataGridViewTextBoxColumn.Name = "ماندندهDataGridViewTextBoxColumn";
            this.ماندندهDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // کسرهزارDataGridViewTextBoxColumn
            // 
            this.کسرهزارDataGridViewTextBoxColumn.DataPropertyName = "کسر هزار";
            this.کسرهزارDataGridViewTextBoxColumn.FillWeight = 40F;
            this.کسرهزارDataGridViewTextBoxColumn.HeaderText = "کسر هزار";
            this.کسرهزارDataGridViewTextBoxColumn.Name = "کسرهزارDataGridViewTextBoxColumn";
            this.کسرهزارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بستانکاریDataGridViewTextBoxColumn
            // 
            this.بستانکاریDataGridViewTextBoxColumn.DataPropertyName = "بستانکاری";
            this.بستانکاریDataGridViewTextBoxColumn.HeaderText = "بستانکاری";
            this.بستانکاریDataGridViewTextBoxColumn.Name = "بستانکاریDataGridViewTextBoxColumn";
            this.بستانکاریDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjTafsilidetailsBindingSource
            // 
            this.sharjTafsilidetailsBindingSource.DataMember = "sharjTafsili_details";
            this.sharjTafsilidetailsBindingSource.DataSource = this.reportDS;
            // 
            // reportDS
            // 
            this.reportDS.DataSetName = "ReportDS";
            this.reportDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btn_print
            // 
            this.btn_print.AccessibleName = "btn_show_fullPardakhti";
            this.btn_print.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print.BackColor = System.Drawing.Color.Transparent;
            this.btn_print.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print.Location = new System.Drawing.Point(12, 428);
            this.btn_print.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_print.Name = "btn_print";
            this.btn_print.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_print.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_print.Size = new System.Drawing.Size(231, 29);
            this.btn_print.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print.Symbol = "";
            this.btn_print.SymbolColor = System.Drawing.Color.Green;
            this.btn_print.SymbolSize = 12F;
            this.btn_print.TabIndex = 87;
            this.btn_print.Text = "چاپ لیست مشترکین";
            this.btn_print.CheckedChanged += new System.EventHandler(this.btn_print_CheckedChanged);
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // sharjTafsili_detailsTableAdapter
            // 
            this.sharjTafsili_detailsTableAdapter.ClearBeforeFill = true;
            // 
            // FrmSharjTafisili_Showdeatils
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 522);
            this.Controls.Add(this.grpinfo);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSharjTafisili_Showdeatils";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "نمایش مشترکین";
            this.Load += new System.EventHandler(this.FrmSharjTafisili_Showdeatils_Load);
            this.grpinfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjTafsilidetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpinfo;
        private DevComponents.DotNetBar.ButtonX btn_print;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAb;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private DevComponents.DotNetBar.ButtonX btn_find_by_gharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFindByGhararda;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX lblDoreh;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.DataGridViewTextBoxColumn قراردادDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ناممشترکDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مبلغکلDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندندهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn کسرهزارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بستانکاریDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource sharjTafsilidetailsBindingSource;
        private ReportDS reportDS;
        private ReportDSTableAdapters.sharjTafsili_detailsTableAdapter sharjTafsili_detailsTableAdapter;
        private DevComponents.DotNetBar.LabelX lbl_mohtarekin_count;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}