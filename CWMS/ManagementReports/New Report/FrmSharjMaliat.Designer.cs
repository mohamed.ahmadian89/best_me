﻿namespace CWMS.ManagementReports.New_Report
{
    partial class FrmSharjMaliat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtend = new System.Windows.Forms.TextBox();
            this.txtStart = new System.Windows.Forms.TextBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rdAb = new System.Windows.Forms.RadioButton();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_print_tafsili = new DevComponents.DotNetBar.ButtonX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btn_search = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_pardakhti_maliat = new CWMS.MoneyTextBox();
            this.txt_pardakhti_majmu = new CWMS.MoneyTextBox();
            this.txt_pardakhti_tedad = new CWMS.MoneyTextBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_kol_tedad = new CWMS.MoneyTextBox();
            this.txt_kol_majmu = new CWMS.MoneyTextBox();
            this.txt_kol_maliat = new CWMS.MoneyTextBox();
            this.dgv_ab_result = new System.Windows.Forms.DataGridView();
            this.دورهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.تعدادDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مالیاتDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.تعداد1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مالیات1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maliatabBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportDS = new CWMS.ManagementReports.New_Report.ReportDS();
            this.dgv_sharj_result = new System.Windows.Forms.DataGridView();
            this.maliatsharjBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.maliat_abTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.maliat_abTableAdapter();
            this.maliat_sharjTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.maliat_sharjTableAdapter();
            this.مالیات1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.تعداد1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مالیاتDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.تعدادDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.دورهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maliatabBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maliatsharjBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtend);
            this.groupPanel1.Controls.Add(this.txtStart);
            this.groupPanel1.Controls.Add(this.radioButton2);
            this.groupPanel1.Controls.Add(this.rdAb);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.btn_print_tafsili);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.btn_search);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(23, 1);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1126, 86);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "مشاهده درآمد مالیاتی";
            // 
            // txtend
            // 
            this.txtend.BackColor = System.Drawing.Color.White;
            this.txtend.ForeColor = System.Drawing.Color.Black;
            this.txtend.Location = new System.Drawing.Point(857, 2);
            this.txtend.Name = "txtend";
            this.txtend.Size = new System.Drawing.Size(51, 28);
            this.txtend.TabIndex = 1;
            this.txtend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtend_KeyDown);
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.Color.White;
            this.txtStart.ForeColor = System.Drawing.Color.Black;
            this.txtStart.Location = new System.Drawing.Point(979, 2);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(51, 28);
            this.txtStart.TabIndex = 0;
            this.txtStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStart_KeyDown);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.ForeColor = System.Drawing.Color.Black;
            this.radioButton2.Location = new System.Drawing.Point(764, 24);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(53, 25);
            this.radioButton2.TabIndex = 93;
            this.radioButton2.Text = "شارژ";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // rdAb
            // 
            this.rdAb.AutoSize = true;
            this.rdAb.BackColor = System.Drawing.Color.Transparent;
            this.rdAb.Checked = true;
            this.rdAb.ForeColor = System.Drawing.Color.Black;
            this.rdAb.Location = new System.Drawing.Point(773, 2);
            this.rdAb.Name = "rdAb";
            this.rdAb.Size = new System.Drawing.Size(44, 25);
            this.rdAb.TabIndex = 92;
            this.rdAb.TabStop = true;
            this.rdAb.Text = "آب";
            this.rdAb.UseVisualStyleBackColor = false;
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(913, 5);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(43, 23);
            this.labelX1.TabIndex = 90;
            this.labelX1.Text = "تا دوره :";
            // 
            // btn_print_tafsili
            // 
            this.btn_print_tafsili.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_tafsili.BackColor = System.Drawing.Color.Transparent;
            this.btn_print_tafsili.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_tafsili.Location = new System.Drawing.Point(25, 17);
            this.btn_print_tafsili.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_print_tafsili.Name = "btn_print_tafsili";
            this.btn_print_tafsili.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_print_tafsili.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_print_tafsili.Size = new System.Drawing.Size(184, 29);
            this.btn_print_tafsili.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_tafsili.Symbol = "";
            this.btn_print_tafsili.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_tafsili.SymbolSize = 12F;
            this.btn_print_tafsili.TabIndex = 88;
            this.btn_print_tafsili.Text = "چاپ درآمد های مالیاتی";
            this.btn_print_tafsili.Click += new System.EventHandler(this.btn_print_tafsili_Click);
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(1036, 5);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(45, 23);
            this.labelX7.TabIndex = 49;
            this.labelX7.Text = "از دوره :";
            // 
            // btn_search
            // 
            this.btn_search.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_search.Location = new System.Drawing.Point(551, 17);
            this.btn_search.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_search.Name = "btn_search";
            this.btn_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_search.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_search.Size = new System.Drawing.Size(179, 29);
            this.btn_search.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_search.Symbol = "";
            this.btn_search.SymbolColor = System.Drawing.Color.Green;
            this.btn_search.SymbolSize = 12F;
            this.btn_search.TabIndex = 2;
            this.btn_search.Text = "مشاهده درامد مالیاتی";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_pardakhti_maliat);
            this.groupPanel2.Controls.Add(this.txt_pardakhti_majmu);
            this.groupPanel2.Controls.Add(this.txt_pardakhti_tedad);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.txt_kol_tedad);
            this.groupPanel2.Controls.Add(this.txt_kol_majmu);
            this.groupPanel2.Controls.Add(this.txt_kol_maliat);
            this.groupPanel2.Controls.Add(this.dgv_sharj_result);
            this.groupPanel2.Controls.Add(this.dgv_ab_result);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(23, 95);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1126, 409);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 7;
            this.groupPanel2.Text = "درامدهای مالیاتی";
            // 
            // txt_pardakhti_maliat
            // 
            this.txt_pardakhti_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhti_maliat.Border.Class = "TextBoxBorder";
            this.txt_pardakhti_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhti_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhti_maliat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakhti_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhti_maliat.Location = new System.Drawing.Point(26, 340);
            this.txt_pardakhti_maliat.Name = "txt_pardakhti_maliat";
            this.txt_pardakhti_maliat.PreventEnterBeep = true;
            this.txt_pardakhti_maliat.ReadOnly = true;
            this.txt_pardakhti_maliat.Size = new System.Drawing.Size(183, 28);
            this.txt_pardakhti_maliat.TabIndex = 105;
            this.txt_pardakhti_maliat.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakhti_maliat.Text = "0";
            // 
            // txt_pardakhti_majmu
            // 
            this.txt_pardakhti_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhti_majmu.Border.Class = "TextBoxBorder";
            this.txt_pardakhti_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhti_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhti_majmu.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakhti_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhti_majmu.Location = new System.Drawing.Point(215, 340);
            this.txt_pardakhti_majmu.Name = "txt_pardakhti_majmu";
            this.txt_pardakhti_majmu.PreventEnterBeep = true;
            this.txt_pardakhti_majmu.ReadOnly = true;
            this.txt_pardakhti_majmu.Size = new System.Drawing.Size(227, 28);
            this.txt_pardakhti_majmu.TabIndex = 104;
            this.txt_pardakhti_majmu.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakhti_majmu.Text = "0";
            // 
            // txt_pardakhti_tedad
            // 
            this.txt_pardakhti_tedad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhti_tedad.Border.Class = "TextBoxBorder";
            this.txt_pardakhti_tedad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhti_tedad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhti_tedad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakhti_tedad.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhti_tedad.Location = new System.Drawing.Point(448, 340);
            this.txt_pardakhti_tedad.Name = "txt_pardakhti_tedad";
            this.txt_pardakhti_tedad.PreventEnterBeep = true;
            this.txt_pardakhti_tedad.ReadOnly = true;
            this.txt_pardakhti_tedad.Size = new System.Drawing.Size(97, 28);
            this.txt_pardakhti_tedad.TabIndex = 103;
            this.txt_pardakhti_tedad.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakhti_tedad.Text = "0";
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(25, 20);
            this.labelX6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(543, 23);
            this.labelX6.TabIndex = 102;
            this.labelX6.Text = "در این قسمت ، مجموع مبالغ و  همچنین مالیات قبوضی را مشاهده می کنید که دارای پرداخ" +
    "تی در سیستم هستند";
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(644, 20);
            this.labelX5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(443, 23);
            this.labelX5.TabIndex = 101;
            this.labelX5.Text = "در این قسمت ، اطلاعات مربوط به کلیه قبوض صادر شده در دوره را مشاهده خواهید کرد . " +
    "";
            // 
            // txt_kol_tedad
            // 
            this.txt_kol_tedad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kol_tedad.Border.Class = "TextBoxBorder";
            this.txt_kol_tedad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kol_tedad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_kol_tedad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_kol_tedad.ForeColor = System.Drawing.Color.Black;
            this.txt_kol_tedad.Location = new System.Drawing.Point(913, 340);
            this.txt_kol_tedad.Name = "txt_kol_tedad";
            this.txt_kol_tedad.PreventEnterBeep = true;
            this.txt_kol_tedad.ReadOnly = true;
            this.txt_kol_tedad.Size = new System.Drawing.Size(97, 28);
            this.txt_kol_tedad.TabIndex = 98;
            this.txt_kol_tedad.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_kol_tedad.Text = "0";
            // 
            // txt_kol_majmu
            // 
            this.txt_kol_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kol_majmu.Border.Class = "TextBoxBorder";
            this.txt_kol_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kol_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_kol_majmu.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_kol_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_kol_majmu.Location = new System.Drawing.Point(740, 340);
            this.txt_kol_majmu.Name = "txt_kol_majmu";
            this.txt_kol_majmu.PreventEnterBeep = true;
            this.txt_kol_majmu.ReadOnly = true;
            this.txt_kol_majmu.Size = new System.Drawing.Size(167, 28);
            this.txt_kol_majmu.TabIndex = 97;
            this.txt_kol_majmu.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_kol_majmu.Text = "0";
            this.txt_kol_majmu.TextChanged += new System.EventHandler(this.txt_maj_avarez_TextChanged);
            // 
            // txt_kol_maliat
            // 
            this.txt_kol_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kol_maliat.Border.Class = "TextBoxBorder";
            this.txt_kol_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kol_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_kol_maliat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_kol_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_kol_maliat.Location = new System.Drawing.Point(551, 340);
            this.txt_kol_maliat.Name = "txt_kol_maliat";
            this.txt_kol_maliat.PreventEnterBeep = true;
            this.txt_kol_maliat.ReadOnly = true;
            this.txt_kol_maliat.Size = new System.Drawing.Size(183, 28);
            this.txt_kol_maliat.TabIndex = 93;
            this.txt_kol_maliat.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_kol_maliat.Text = "0";
            // 
            // dgv_ab_result
            // 
            this.dgv_ab_result.AutoGenerateColumns = false;
            this.dgv_ab_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ab_result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_ab_result.BackgroundColor = System.Drawing.Color.White;
            this.dgv_ab_result.ColumnHeadersHeight = 30;
            this.dgv_ab_result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.دورهDataGridViewTextBoxColumn,
            this.تعدادDataGridViewTextBoxColumn,
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn,
            this.مالیاتDataGridViewTextBoxColumn,
            this.تعداد1DataGridViewTextBoxColumn,
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn,
            this.مالیات1DataGridViewTextBoxColumn});
            this.dgv_ab_result.DataSource = this.maliatabBindingSource;
            this.dgv_ab_result.Location = new System.Drawing.Point(26, 55);
            this.dgv_ab_result.Name = "dgv_ab_result";
            this.dgv_ab_result.RowHeadersVisible = false;
            this.dgv_ab_result.Size = new System.Drawing.Size(1068, 279);
            this.dgv_ab_result.TabIndex = 100;
            // 
            // دورهDataGridViewTextBoxColumn
            // 
            this.دورهDataGridViewTextBoxColumn.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn.FillWeight = 50F;
            this.دورهDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn.Name = "دورهDataGridViewTextBoxColumn";
            // 
            // تعدادDataGridViewTextBoxColumn
            // 
            this.تعدادDataGridViewTextBoxColumn.DataPropertyName = "تعداد";
            this.تعدادDataGridViewTextBoxColumn.FillWeight = 50F;
            this.تعدادDataGridViewTextBoxColumn.HeaderText = "تعداد";
            this.تعدادDataGridViewTextBoxColumn.Name = "تعدادDataGridViewTextBoxColumn";
            // 
            // مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn
            // 
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn.DataPropertyName = "مجموع مبالغ کلیه قبوض دوره";
            dataGridViewCellStyle5.Format = "0,0";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ کلیه قبوض دوره";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn.Name = "مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مالیاتDataGridViewTextBoxColumn
            // 
            this.مالیاتDataGridViewTextBoxColumn.DataPropertyName = "مالیات";
            dataGridViewCellStyle6.Format = "0,0";
            this.مالیاتDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.مالیاتDataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.مالیاتDataGridViewTextBoxColumn.Name = "مالیاتDataGridViewTextBoxColumn";
            // 
            // تعداد1DataGridViewTextBoxColumn
            // 
            this.تعداد1DataGridViewTextBoxColumn.DataPropertyName = "تعداد1";
            this.تعداد1DataGridViewTextBoxColumn.FillWeight = 50F;
            this.تعداد1DataGridViewTextBoxColumn.HeaderText = "تعداد";
            this.تعداد1DataGridViewTextBoxColumn.Name = "تعداد1DataGridViewTextBoxColumn";
            // 
            // مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn
            // 
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.DataPropertyName = "مجموع مبالغ کلیه قبوض دارای پرداخت";
            dataGridViewCellStyle7.Format = "0,0";
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.FillWeight = 140F;
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ کلیه قبوض دارای پرداخت";
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.Name = "مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn";
            this.مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مالیات1DataGridViewTextBoxColumn
            // 
            this.مالیات1DataGridViewTextBoxColumn.DataPropertyName = "مالیات1";
            dataGridViewCellStyle8.Format = "0,0";
            this.مالیات1DataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.مالیات1DataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.مالیات1DataGridViewTextBoxColumn.Name = "مالیات1DataGridViewTextBoxColumn";
            // 
            // maliatabBindingSource
            // 
            this.maliatabBindingSource.DataMember = "maliat_ab";
            this.maliatabBindingSource.DataSource = this.reportDS;
            // 
            // reportDS
            // 
            this.reportDS.DataSetName = "ReportDS";
            this.reportDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dgv_sharj_result
            // 
            this.dgv_sharj_result.AutoGenerateColumns = false;
            this.dgv_sharj_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sharj_result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_sharj_result.BackgroundColor = System.Drawing.Color.White;
            this.dgv_sharj_result.ColumnHeadersHeight = 30;
            this.dgv_sharj_result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.دورهDataGridViewTextBoxColumn1,
            this.تعدادDataGridViewTextBoxColumn1,
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1,
            this.مالیاتDataGridViewTextBoxColumn1,
            this.تعداد1DataGridViewTextBoxColumn1,
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn,
            this.مالیات1DataGridViewTextBoxColumn1});
            this.dgv_sharj_result.DataSource = this.maliatsharjBindingSource;
            this.dgv_sharj_result.Location = new System.Drawing.Point(25, 50);
            this.dgv_sharj_result.Name = "dgv_sharj_result";
            this.dgv_sharj_result.RowHeadersVisible = false;
            this.dgv_sharj_result.Size = new System.Drawing.Size(1068, 279);
            this.dgv_sharj_result.TabIndex = 99;
            // 
            // maliatsharjBindingSource
            // 
            this.maliatsharjBindingSource.DataMember = "maliat_sharj";
            this.maliatsharjBindingSource.DataSource = this.reportDS;
            // 
            // maliat_abTableAdapter
            // 
            this.maliat_abTableAdapter.ClearBeforeFill = true;
            // 
            // maliat_sharjTableAdapter
            // 
            this.maliat_sharjTableAdapter.ClearBeforeFill = true;
            // 
            // مالیات1DataGridViewTextBoxColumn1
            // 
            this.مالیات1DataGridViewTextBoxColumn1.DataPropertyName = "مالیات1";
            dataGridViewCellStyle4.Format = "0,0";
            this.مالیات1DataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.مالیات1DataGridViewTextBoxColumn1.HeaderText = "مالیات";
            this.مالیات1DataGridViewTextBoxColumn1.Name = "مالیات1DataGridViewTextBoxColumn1";
            // 
            // مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn
            // 
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.DataPropertyName = "مجموع مبالغ کلیه قبوض دوره دارای پرداخت";
            dataGridViewCellStyle3.Format = "0,0";
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.FillWeight = 140F;
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ کلیه قبوض دوره دارای پرداخت";
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.Name = "مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn";
            this.مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // تعداد1DataGridViewTextBoxColumn1
            // 
            this.تعداد1DataGridViewTextBoxColumn1.DataPropertyName = "تعداد1";
            this.تعداد1DataGridViewTextBoxColumn1.FillWeight = 50F;
            this.تعداد1DataGridViewTextBoxColumn1.HeaderText = "تعداد";
            this.تعداد1DataGridViewTextBoxColumn1.Name = "تعداد1DataGridViewTextBoxColumn1";
            // 
            // مالیاتDataGridViewTextBoxColumn1
            // 
            this.مالیاتDataGridViewTextBoxColumn1.DataPropertyName = "مالیات";
            dataGridViewCellStyle2.Format = "0,0";
            this.مالیاتDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.مالیاتDataGridViewTextBoxColumn1.HeaderText = "مالیات";
            this.مالیاتDataGridViewTextBoxColumn1.Name = "مالیاتDataGridViewTextBoxColumn1";
            // 
            // مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1
            // 
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1.DataPropertyName = "مجموع مبالغ کلیه قبوض دوره";
            dataGridViewCellStyle1.Format = "0,0";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1.HeaderText = "مجموع مبالغ کلیه قبوض دوره";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1.Name = "مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1";
            this.مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // تعدادDataGridViewTextBoxColumn1
            // 
            this.تعدادDataGridViewTextBoxColumn1.DataPropertyName = "تعداد";
            this.تعدادDataGridViewTextBoxColumn1.FillWeight = 50F;
            this.تعدادDataGridViewTextBoxColumn1.HeaderText = "تعداد";
            this.تعدادDataGridViewTextBoxColumn1.Name = "تعدادDataGridViewTextBoxColumn1";
            // 
            // دورهDataGridViewTextBoxColumn1
            // 
            this.دورهDataGridViewTextBoxColumn1.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn1.FillWeight = 50F;
            this.دورهDataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn1.Name = "دورهDataGridViewTextBoxColumn1";
            // 
            // FrmSharjMaliat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 521);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSharjMaliat";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmSharjMaliat_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maliatabBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maliatsharjBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_print_tafsili;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btn_search;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private MoneyTextBox txt_kol_maliat;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton rdAb;
        private System.Windows.Forms.TextBox txtend;
        private System.Windows.Forms.TextBox txtStart;
        private ReportDS reportDS;
        private MoneyTextBox txt_kol_tedad;
        private MoneyTextBox txt_kol_majmu;
        private System.Windows.Forms.DataGridView dgv_sharj_result;
        private System.Windows.Forms.DataGridView dgv_ab_result;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.BindingSource maliatabBindingSource;
        private ReportDSTableAdapters.maliat_abTableAdapter maliat_abTableAdapter;
        private System.Windows.Forms.BindingSource maliatsharjBindingSource;
        private ReportDSTableAdapters.maliat_sharjTableAdapter maliat_sharjTableAdapter;
        private MoneyTextBox txt_pardakhti_maliat;
        private MoneyTextBox txt_pardakhti_majmu;
        private MoneyTextBox txt_pardakhti_tedad;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn تعدادDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیاتDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn تعداد1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغکلیهقبوضدارایپرداختDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیات1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn تعدادDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغکلیهقبوضدورهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیاتDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn تعداد1DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغکلیهقبوضدورهدارایپرداختDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مالیات1DataGridViewTextBoxColumn1;
    }
}