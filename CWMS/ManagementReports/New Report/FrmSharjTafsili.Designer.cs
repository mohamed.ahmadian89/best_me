﻿namespace CWMS.ManagementReports
{
    partial class FrmSharjTafsili
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnShowBestankar = new DevComponents.DotNetBar.ButtonX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btn_print_tafsili = new DevComponents.DotNetBar.ButtonX();
            this.btn_show_notPrdakht = new DevComponents.DotNetBar.ButtonX();
            this.btn_show_fullPardakhti = new DevComponents.DotNetBar.ButtonX();
            this.txt_total_majmjuMenhaKasr = new CWMS.MoneyTextBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.txt_total_kasr = new CWMS.MoneyTextBox();
            this.txt_pardakhtJozyee_majmu = new CWMS.MoneyTextBox();
            this.txt_pardakhtJozyee_count = new CWMS.MoneyTextBox();
            this.txt_bedehkaran_majmu = new CWMS.MoneyTextBox();
            this.txt_bedehkrarn_count = new CWMS.MoneyTextBox();
            this.txt_pardakht_majmu = new CWMS.MoneyTextBox();
            this.txt_pardakht_count = new CWMS.MoneyTextBox();
            this.txt_total_bestankar = new CWMS.MoneyTextBox();
            this.txt_total_mandeh = new CWMS.MoneyTextBox();
            this.txt_total_pardakhti = new CWMS.MoneyTextBox();
            this.txt_total_majmuMabalegh = new CWMS.MoneyTextBox();
            this.txt_total_tedadkol = new CWMS.MoneyTextBox();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cmbsharjStart = new System.Windows.Forms.ComboBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btnsave = new DevComponents.DotNetBar.ButtonX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.mainDataSest1 = new CWMS.MainDataSest();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.txt_bestankari_in_this_doreh = new CWMS.MoneyTextBox();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_bestankari_in_this_doreh);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.btnShowBestankar);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.btn_print_tafsili);
            this.groupPanel1.Controls.Add(this.btn_show_notPrdakht);
            this.groupPanel1.Controls.Add(this.btn_show_fullPardakhti);
            this.groupPanel1.Controls.Add(this.txt_total_majmjuMenhaKasr);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.labelX18);
            this.groupPanel1.Controls.Add(this.labelX12);
            this.groupPanel1.Controls.Add(this.txt_total_kasr);
            this.groupPanel1.Controls.Add(this.txt_pardakhtJozyee_majmu);
            this.groupPanel1.Controls.Add(this.txt_pardakhtJozyee_count);
            this.groupPanel1.Controls.Add(this.txt_bedehkaran_majmu);
            this.groupPanel1.Controls.Add(this.txt_bedehkrarn_count);
            this.groupPanel1.Controls.Add(this.txt_pardakht_majmu);
            this.groupPanel1.Controls.Add(this.txt_pardakht_count);
            this.groupPanel1.Controls.Add(this.txt_total_bestankar);
            this.groupPanel1.Controls.Add(this.txt_total_mandeh);
            this.groupPanel1.Controls.Add(this.txt_total_pardakhti);
            this.groupPanel1.Controls.Add(this.txt_total_majmuMabalegh);
            this.groupPanel1.Controls.Add(this.txt_total_tedadkol);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.cmbsharjStart);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.btnsave);
            this.groupPanel1.Controls.Add(this.labelX13);
            this.groupPanel1.Controls.Add(this.labelX17);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 13);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(993, 515);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 5;
            this.groupPanel1.Text = "اطلاعات تفضیلی دوره شارژ";
            // 
            // btnShowBestankar
            // 
            this.btnShowBestankar.AccessibleName = "";
            this.btnShowBestankar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowBestankar.BackColor = System.Drawing.Color.Transparent;
            this.btnShowBestankar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowBestankar.Location = new System.Drawing.Point(658, 393);
            this.btnShowBestankar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnShowBestankar.Name = "btnShowBestankar";
            this.btnShowBestankar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnShowBestankar.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnShowBestankar.Size = new System.Drawing.Size(204, 29);
            this.btnShowBestankar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowBestankar.Symbol = "";
            this.btnShowBestankar.SymbolColor = System.Drawing.Color.Green;
            this.btnShowBestankar.SymbolSize = 12F;
            this.btnShowBestankar.TabIndex = 91;
            this.btnShowBestankar.Text = "نمایش و چاپ قبوض بستانکاران";
            this.btnShowBestankar.Visible = false;
            this.btnShowBestankar.Click += new System.EventHandler(this.btnShowBestankar_Click);
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(228, 359);
            this.labelX6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(147, 23);
            this.labelX6.TabIndex = 90;
            this.labelX6.Text = "مجموع مبالغ بدهی بدهکاران :";
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(250, 325);
            this.labelX4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(124, 43);
            this.labelX4.TabIndex = 89;
            this.labelX4.Text = "تعداد بدهکاران سیستم:\r\n ";
            // 
            // btn_print_tafsili
            // 
            this.btn_print_tafsili.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_tafsili.BackColor = System.Drawing.Color.Transparent;
            this.btn_print_tafsili.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_tafsili.Location = new System.Drawing.Point(25, 5);
            this.btn_print_tafsili.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_print_tafsili.Name = "btn_print_tafsili";
            this.btn_print_tafsili.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_print_tafsili.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_print_tafsili.Size = new System.Drawing.Size(265, 29);
            this.btn_print_tafsili.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_tafsili.Symbol = "";
            this.btn_print_tafsili.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_tafsili.SymbolSize = 12F;
            this.btn_print_tafsili.TabIndex = 88;
            this.btn_print_tafsili.Text = "چاپ اطلاعات گزارش تفصیلی";
            this.btn_print_tafsili.Visible = false;
            this.btn_print_tafsili.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btn_show_notPrdakht
            // 
            this.btn_show_notPrdakht.AccessibleName = "btn_show_fullPardakhti";
            this.btn_show_notPrdakht.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show_notPrdakht.BackColor = System.Drawing.Color.Transparent;
            this.btn_show_notPrdakht.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show_notPrdakht.Location = new System.Drawing.Point(18, 393);
            this.btn_show_notPrdakht.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_show_notPrdakht.Name = "btn_show_notPrdakht";
            this.btn_show_notPrdakht.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_show_notPrdakht.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_show_notPrdakht.Size = new System.Drawing.Size(179, 29);
            this.btn_show_notPrdakht.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show_notPrdakht.Symbol = "";
            this.btn_show_notPrdakht.SymbolColor = System.Drawing.Color.Green;
            this.btn_show_notPrdakht.SymbolSize = 12F;
            this.btn_show_notPrdakht.TabIndex = 87;
            this.btn_show_notPrdakht.Text = "نمایش و چاپ قبوض بدهکاران";
            this.btn_show_notPrdakht.Click += new System.EventHandler(this.btn_show_notPrdakht_Click);
            // 
            // btn_show_fullPardakhti
            // 
            this.btn_show_fullPardakhti.AccessibleName = "";
            this.btn_show_fullPardakhti.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show_fullPardakhti.BackColor = System.Drawing.Color.Transparent;
            this.btn_show_fullPardakhti.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show_fullPardakhti.Location = new System.Drawing.Point(525, 395);
            this.btn_show_fullPardakhti.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_show_fullPardakhti.Name = "btn_show_fullPardakhti";
            this.btn_show_fullPardakhti.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_show_fullPardakhti.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_show_fullPardakhti.Size = new System.Drawing.Size(127, 29);
            this.btn_show_fullPardakhti.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show_fullPardakhti.Symbol = "";
            this.btn_show_fullPardakhti.SymbolColor = System.Drawing.Color.Green;
            this.btn_show_fullPardakhti.SymbolSize = 12F;
            this.btn_show_fullPardakhti.TabIndex = 86;
            this.btn_show_fullPardakhti.Text = "نمایش قبوض";
            this.btn_show_fullPardakhti.Click += new System.EventHandler(this.btn_show_fullPardakhti_Click);
            // 
            // txt_total_majmjuMenhaKasr
            // 
            this.txt_total_majmjuMenhaKasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_majmjuMenhaKasr.Border.Class = "TextBoxBorder";
            this.txt_total_majmjuMenhaKasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_majmjuMenhaKasr.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_majmjuMenhaKasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_majmjuMenhaKasr.ForeColor = System.Drawing.Color.Black;
            this.txt_total_majmjuMenhaKasr.Location = new System.Drawing.Point(240, 132);
            this.txt_total_majmjuMenhaKasr.Name = "txt_total_majmjuMenhaKasr";
            this.txt_total_majmjuMenhaKasr.PreventEnterBeep = true;
            this.txt_total_majmjuMenhaKasr.ReadOnly = true;
            this.txt_total_majmjuMenhaKasr.Size = new System.Drawing.Size(173, 28);
            this.txt_total_majmjuMenhaKasr.TabIndex = 83;
            this.txt_total_majmjuMenhaKasr.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_majmjuMenhaKasr.Text = "0";
            this.txt_total_majmjuMenhaKasr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(415, 137);
            this.labelX11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(537, 23);
            this.labelX11.TabIndex = 82;
            this.labelX11.Text = "منتقل خواهد شد . درنتیجه کل مجموع مبالغی که مشترکین در این دوره می بایست پرداخت ک" +
    "نند برابر است با :";
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(246, 225);
            this.labelX18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(171, 23);
            this.labelX18.TabIndex = 81;
            this.labelX18.Text = "مبلغ کل مانده بدهی در این دوره :";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(565, 101);
            this.labelX12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(89, 23);
            this.labelX12.TabIndex = 80;
            this.labelX12.Text = "می باشد که مبلغ ";
            // 
            // labelX17
            // 
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(200, 267);
            this.labelX17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX17.Name = "labelX17";
            this.labelX17.Size = new System.Drawing.Size(312, 23);
            this.labelX17.TabIndex = 79;
            this.labelX17.Text = " مبلغ بستانکاری انتقال یافته از این دوره به دوره بعد :";
            // 
            // txt_total_kasr
            // 
            this.txt_total_kasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_kasr.Border.Class = "TextBoxBorder";
            this.txt_total_kasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_kasr.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_kasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_kasr.ForeColor = System.Drawing.Color.Black;
            this.txt_total_kasr.Location = new System.Drawing.Point(432, 98);
            this.txt_total_kasr.Name = "txt_total_kasr";
            this.txt_total_kasr.PreventEnterBeep = true;
            this.txt_total_kasr.ReadOnly = true;
            this.txt_total_kasr.Size = new System.Drawing.Size(127, 28);
            this.txt_total_kasr.TabIndex = 74;
            this.txt_total_kasr.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_kasr.Text = "0";
            this.txt_total_kasr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_pardakhtJozyee_majmu
            // 
            this.txt_pardakhtJozyee_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhtJozyee_majmu.Border.Class = "TextBoxBorder";
            this.txt_pardakhtJozyee_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhtJozyee_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhtJozyee_majmu.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakhtJozyee_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhtJozyee_majmu.Location = new System.Drawing.Point(338, 447);
            this.txt_pardakhtJozyee_majmu.Name = "txt_pardakhtJozyee_majmu";
            this.txt_pardakhtJozyee_majmu.PreventEnterBeep = true;
            this.txt_pardakhtJozyee_majmu.ReadOnly = true;
            this.txt_pardakhtJozyee_majmu.Size = new System.Drawing.Size(127, 28);
            this.txt_pardakhtJozyee_majmu.TabIndex = 72;
            this.txt_pardakhtJozyee_majmu.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakhtJozyee_majmu.Text = "0";
            this.txt_pardakhtJozyee_majmu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_pardakhtJozyee_count
            // 
            this.txt_pardakhtJozyee_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakhtJozyee_count.Border.Class = "TextBoxBorder";
            this.txt_pardakhtJozyee_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakhtJozyee_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakhtJozyee_count.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakhtJozyee_count.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakhtJozyee_count.Location = new System.Drawing.Point(791, 447);
            this.txt_pardakhtJozyee_count.Name = "txt_pardakhtJozyee_count";
            this.txt_pardakhtJozyee_count.PreventEnterBeep = true;
            this.txt_pardakhtJozyee_count.ReadOnly = true;
            this.txt_pardakhtJozyee_count.Size = new System.Drawing.Size(127, 28);
            this.txt_pardakhtJozyee_count.TabIndex = 71;
            this.txt_pardakhtJozyee_count.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakhtJozyee_count.Text = "0";
            this.txt_pardakhtJozyee_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_bedehkaran_majmu
            // 
            this.txt_bedehkaran_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_bedehkaran_majmu.Border.Class = "TextBoxBorder";
            this.txt_bedehkaran_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_bedehkaran_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_bedehkaran_majmu.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_bedehkaran_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_bedehkaran_majmu.Location = new System.Drawing.Point(87, 357);
            this.txt_bedehkaran_majmu.Name = "txt_bedehkaran_majmu";
            this.txt_bedehkaran_majmu.PreventEnterBeep = true;
            this.txt_bedehkaran_majmu.ReadOnly = true;
            this.txt_bedehkaran_majmu.Size = new System.Drawing.Size(127, 28);
            this.txt_bedehkaran_majmu.TabIndex = 70;
            this.txt_bedehkaran_majmu.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_bedehkaran_majmu.Text = "0";
            this.txt_bedehkaran_majmu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_bedehkrarn_count
            // 
            this.txt_bedehkrarn_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_bedehkrarn_count.Border.Class = "TextBoxBorder";
            this.txt_bedehkrarn_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_bedehkrarn_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_bedehkrarn_count.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_bedehkrarn_count.ForeColor = System.Drawing.Color.Black;
            this.txt_bedehkrarn_count.Location = new System.Drawing.Point(87, 323);
            this.txt_bedehkrarn_count.Name = "txt_bedehkrarn_count";
            this.txt_bedehkrarn_count.PreventEnterBeep = true;
            this.txt_bedehkrarn_count.ReadOnly = true;
            this.txt_bedehkrarn_count.Size = new System.Drawing.Size(127, 28);
            this.txt_bedehkrarn_count.TabIndex = 69;
            this.txt_bedehkrarn_count.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_bedehkrarn_count.Text = "0";
            this.txt_bedehkrarn_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_pardakht_majmu
            // 
            this.txt_pardakht_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakht_majmu.Border.Class = "TextBoxBorder";
            this.txt_pardakht_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakht_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakht_majmu.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakht_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakht_majmu.Location = new System.Drawing.Point(579, 359);
            this.txt_pardakht_majmu.Name = "txt_pardakht_majmu";
            this.txt_pardakht_majmu.PreventEnterBeep = true;
            this.txt_pardakht_majmu.ReadOnly = true;
            this.txt_pardakht_majmu.Size = new System.Drawing.Size(127, 28);
            this.txt_pardakht_majmu.TabIndex = 68;
            this.txt_pardakht_majmu.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakht_majmu.Text = "0";
            this.txt_pardakht_majmu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_pardakht_count
            // 
            this.txt_pardakht_count.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_pardakht_count.Border.Class = "TextBoxBorder";
            this.txt_pardakht_count.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_pardakht_count.DisabledBackColor = System.Drawing.Color.White;
            this.txt_pardakht_count.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_pardakht_count.ForeColor = System.Drawing.Color.Black;
            this.txt_pardakht_count.Location = new System.Drawing.Point(579, 325);
            this.txt_pardakht_count.Name = "txt_pardakht_count";
            this.txt_pardakht_count.PreventEnterBeep = true;
            this.txt_pardakht_count.ReadOnly = true;
            this.txt_pardakht_count.Size = new System.Drawing.Size(127, 28);
            this.txt_pardakht_count.TabIndex = 67;
            this.txt_pardakht_count.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_pardakht_count.Text = "0";
            this.txt_pardakht_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total_bestankar
            // 
            this.txt_total_bestankar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_bestankar.Border.Class = "TextBoxBorder";
            this.txt_total_bestankar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_bestankar.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_bestankar.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_bestankar.ForeColor = System.Drawing.Color.Black;
            this.txt_total_bestankar.Location = new System.Drawing.Point(113, 264);
            this.txt_total_bestankar.Name = "txt_total_bestankar";
            this.txt_total_bestankar.PreventEnterBeep = true;
            this.txt_total_bestankar.ReadOnly = true;
            this.txt_total_bestankar.Size = new System.Drawing.Size(127, 28);
            this.txt_total_bestankar.TabIndex = 66;
            this.txt_total_bestankar.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_bestankar.Text = "0";
            this.txt_total_bestankar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total_mandeh
            // 
            this.txt_total_mandeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_mandeh.Border.Class = "TextBoxBorder";
            this.txt_total_mandeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_mandeh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_mandeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_mandeh.ForeColor = System.Drawing.Color.Black;
            this.txt_total_mandeh.Location = new System.Drawing.Point(113, 220);
            this.txt_total_mandeh.Name = "txt_total_mandeh";
            this.txt_total_mandeh.PreventEnterBeep = true;
            this.txt_total_mandeh.ReadOnly = true;
            this.txt_total_mandeh.Size = new System.Drawing.Size(127, 28);
            this.txt_total_mandeh.TabIndex = 65;
            this.txt_total_mandeh.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_mandeh.Text = "0";
            this.txt_total_mandeh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total_pardakhti
            // 
            this.txt_total_pardakhti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_pardakhti.Border.Class = "TextBoxBorder";
            this.txt_total_pardakhti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_pardakhti.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_pardakhti.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_pardakhti.ForeColor = System.Drawing.Color.Black;
            this.txt_total_pardakhti.Location = new System.Drawing.Point(525, 187);
            this.txt_total_pardakhti.Name = "txt_total_pardakhti";
            this.txt_total_pardakhti.PreventEnterBeep = true;
            this.txt_total_pardakhti.ReadOnly = true;
            this.txt_total_pardakhti.Size = new System.Drawing.Size(127, 28);
            this.txt_total_pardakhti.TabIndex = 64;
            this.txt_total_pardakhti.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_pardakhti.Text = "0";
            this.txt_total_pardakhti.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total_majmuMabalegh
            // 
            this.txt_total_majmuMabalegh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_majmuMabalegh.Border.Class = "TextBoxBorder";
            this.txt_total_majmuMabalegh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_majmuMabalegh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_majmuMabalegh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_majmuMabalegh.ForeColor = System.Drawing.Color.Black;
            this.txt_total_majmuMabalegh.Location = new System.Drawing.Point(660, 98);
            this.txt_total_majmuMabalegh.Name = "txt_total_majmuMabalegh";
            this.txt_total_majmuMabalegh.PreventEnterBeep = true;
            this.txt_total_majmuMabalegh.ReadOnly = true;
            this.txt_total_majmuMabalegh.Size = new System.Drawing.Size(127, 28);
            this.txt_total_majmuMabalegh.TabIndex = 63;
            this.txt_total_majmuMabalegh.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_majmuMabalegh.Text = "0";
            this.txt_total_majmuMabalegh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_total_tedadkol
            // 
            this.txt_total_tedadkol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_total_tedadkol.Border.Class = "TextBoxBorder";
            this.txt_total_tedadkol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_total_tedadkol.DisabledBackColor = System.Drawing.Color.White;
            this.txt_total_tedadkol.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_total_tedadkol.ForeColor = System.Drawing.Color.Black;
            this.txt_total_tedadkol.Location = new System.Drawing.Point(55, 60);
            this.txt_total_tedadkol.Name = "txt_total_tedadkol";
            this.txt_total_tedadkol.PreventEnterBeep = true;
            this.txt_total_tedadkol.ReadOnly = true;
            this.txt_total_tedadkol.Size = new System.Drawing.Size(127, 28);
            this.txt_total_tedadkol.TabIndex = 62;
            this.txt_total_tedadkol.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_total_tedadkol.Text = "0";
            this.txt_total_tedadkol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(723, 188);
            this.labelX10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(226, 23);
            this.labelX10.TabIndex = 59;
            this.labelX10.Text = "مجموع پرداختی های ثبت شده برای این دوره ";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(25, 441);
            this.labelX8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(304, 41);
            this.labelX8.TabIndex = 58;
            this.labelX8.Text = "جهت تکمیل پرداخت این قبوض به سیستم پرداخت شود .";
            this.labelX8.WordWrap = true;
            // 
            // labelX9
            // 
            this.labelX9.AutoSize = true;
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(471, 450);
            this.labelX9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(312, 23);
            this.labelX9.TabIndex = 57;
            this.labelX9.Text = "عدد از قبوض به طور کامل پرداخت نشده اند و می بایست مبلغ :";
            this.labelX9.Click += new System.EventHandler(this.labelX9_Click);
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(749, 362);
            this.labelX5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(206, 23);
            this.labelX5.TabIndex = 55;
            this.labelX5.Text = "مجموع مبالغ کل بدون احتساب کسر هزار :";
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(719, 328);
            this.labelX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(239, 23);
            this.labelX3.TabIndex = 53;
            this.labelX3.Text = "تعداد قبوضی که به طور کامل پرداخت شده اند  :";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(790, 101);
            this.labelX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(162, 23);
            this.labelX2.TabIndex = 52;
            this.labelX2.Text = "مجموع مبالغ کل قبوض این دوره:";
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(188, 60);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(115, 23);
            this.labelX1.TabIndex = 51;
            this.labelX1.Text = "تعداد کل قبوض دوره :";
            // 
            // cmbsharjStart
            // 
            this.cmbsharjStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbsharjStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.cmbsharjStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbsharjStart.ForeColor = System.Drawing.Color.Black;
            this.cmbsharjStart.FormattingEnabled = true;
            this.cmbsharjStart.Location = new System.Drawing.Point(674, 15);
            this.cmbsharjStart.Name = "cmbsharjStart";
            this.cmbsharjStart.Size = new System.Drawing.Size(69, 28);
            this.cmbsharjStart.TabIndex = 50;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(749, 18);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(195, 23);
            this.labelX7.TabIndex = 49;
            this.labelX7.Text = "لطفا دوره مورد نظر خود را انتخاب کنید :";
            // 
            // btnsave
            // 
            this.btnsave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnsave.Location = new System.Drawing.Point(476, 14);
            this.btnsave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnsave.Name = "btnsave";
            this.btnsave.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnsave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnsave.Size = new System.Drawing.Size(179, 29);
            this.btnsave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnsave.Symbol = "";
            this.btnsave.SymbolColor = System.Drawing.Color.Green;
            this.btnsave.SymbolSize = 12F;
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "نمایش اطلاعات";
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(41, 95);
            this.labelX13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(372, 33);
            this.labelX13.TabIndex = 73;
            this.labelX13.Text = "به عنوان  مجموع کسر هزار کلیه قبوض ، از آن کسر شده  و به دوره بعد  .\r\n  ";
            this.labelX13.WordWrap = true;
            this.labelX13.Click += new System.EventHandler(this.labelX13_Click);
            // 
            // mainDataSest1
            // 
            this.mainDataSest1.DataSetName = "MainDataSest";
            this.mainDataSest1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelX14
            // 
            this.labelX14.AutoSize = true;
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(684, 225);
            this.labelX14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(265, 23);
            this.labelX14.TabIndex = 92;
            this.labelX14.Text = "مبلغ بستانکاری انتقال یافته از دوره قبل به این دوره :";
            // 
            // txt_bestankari_in_this_doreh
            // 
            this.txt_bestankari_in_this_doreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_bestankari_in_this_doreh.Border.Class = "TextBoxBorder";
            this.txt_bestankari_in_this_doreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_bestankari_in_this_doreh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_bestankari_in_this_doreh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_bestankari_in_this_doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_bestankari_in_this_doreh.Location = new System.Drawing.Point(525, 221);
            this.txt_bestankari_in_this_doreh.Name = "txt_bestankari_in_this_doreh";
            this.txt_bestankari_in_this_doreh.PreventEnterBeep = true;
            this.txt_bestankari_in_this_doreh.ReadOnly = true;
            this.txt_bestankari_in_this_doreh.Size = new System.Drawing.Size(127, 28);
            this.txt_bestankari_in_this_doreh.TabIndex = 93;
            this.txt_bestankari_in_this_doreh.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_bestankari_in_this_doreh.Text = "0";
            this.txt_bestankari_in_this_doreh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FrmSharjTafsili
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 541);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSharjTafsili";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "اطلاعات تفضیلی در مورد دوره شارژ";
            this.Load += new System.EventHandler(this.FrmSharjTafsili_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private MainDataSest mainDataSest1;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX17;
        private MoneyTextBox txt_total_kasr;
        private DevComponents.DotNetBar.LabelX labelX13;
        private MoneyTextBox txt_pardakhtJozyee_majmu;
        private MoneyTextBox txt_pardakhtJozyee_count;
        private MoneyTextBox txt_bedehkaran_majmu;
        private MoneyTextBox txt_bedehkrarn_count;
        private MoneyTextBox txt_pardakht_majmu;
        private MoneyTextBox txt_pardakht_count;
        private MoneyTextBox txt_total_mandeh;
        private MoneyTextBox txt_total_pardakhti;
        private MoneyTextBox txt_total_majmuMabalegh;
        private MoneyTextBox txt_total_tedadkol;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.ComboBox cmbsharjStart;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btnsave;
        private MoneyTextBox txt_total_majmjuMenhaKasr;
        private DevComponents.DotNetBar.LabelX labelX11;
        private MoneyTextBox txt_total_bestankar;
        private DevComponents.DotNetBar.ButtonX btn_show_notPrdakht;
        private DevComponents.DotNetBar.ButtonX btn_show_fullPardakhti;
        private DevComponents.DotNetBar.ButtonX btn_print_tafsili;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnShowBestankar;
        private MoneyTextBox txt_bestankari_in_this_doreh;
        private DevComponents.DotNetBar.LabelX labelX14;
    }
}