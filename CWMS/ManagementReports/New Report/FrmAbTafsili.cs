﻿using CrystalDecisions.Windows.Forms;
using FarsiLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.ManagementReports.New_Report
{
    public partial class FrmAbTafsili : MyMetroForm
    {
        public FrmAbTafsili()
        {
            InitializeComponent();
         
        }

        private void FrmAbTafsili_Load(object sender, EventArgs e)
        {
            cmbsharjStart.DataSource = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            cmbsharjStart.DisplayMember = "dcode";
            cmbsharjStart.ValueMember = "dcode";
            if (cmbsharjStart.Items.Count == 0)
                btnsave.Enabled = false;
        }


        decimal kol_kasr_hezar=0,kol_mablagh = 0, kol_maliat = 0, kol_bedehi_kasr_hezar_last_doreh = 0, kol_majmu = 0, kol_pardakhti = 0, kol_baghimande = 0;
        private void btnsave_Click(object sender, EventArgs e)
        {
            ReportDSTableAdapters.ab_ghabzTableAdapter ta = new ReportDSTableAdapters.ab_ghabzTableAdapter();
            ta.FillBy(reportDS1.ab_ghabz, Convert.ToInt32(cmbsharjStart.SelectedValue));

            txt_total_tedadkol.Text = reportDS1.ab_ghabz.Rows.Count.ToString();
            txt_total_majmuMabalegh.Text = reportDS1.ab_ghabz.Sum(p => p.mablaghkol).ToString();
            txt_total_mandeh.Text = reportDS1.ab_ghabz.Sum(p => p.mande).ToString();
            txt_total_bestankar.Text = reportDS1.ab_ghabz.Sum(p => p.bestankari).ToString();
            txt_total_pardakhti.Text = Classes.ClsMain.ExecuteScalar("select sum(mablagh) from ghabz_pardakht where vaziat=1 and is_ab=1 and  dcode=" + cmbsharjStart.SelectedValue).ToString();
            txt_total_kasr.Text = reportDS1.ab_ghabz.Sum(p => p.kasr_hezar).ToString();

            //txtTotalMaliat.Text = reportDS1.ab_ghabz.Sum(p => p.maliat).ToString();

            //txt_total_round.Text = (
            //    Convert.ToDecimal(txt_total_majmuMabalegh.Text) - Convert.ToDecimal(txt_total_mandeh.Text)-Convert.ToDecimal(txt_total_kasr.Text)
            //    ).ToString();

            txt_bestankari_in_this_doreh.Text = Classes.ClsMain.ExecuteScalar("select isnull(sum(bestankari),0) from ab_ghabz where  dcode=" + (Convert.ToInt32(cmbsharjStart.SelectedValue) - 1).ToString()).ToString();



            txt_total_majmjuMenhaKasr.Text = (Convert.ToDecimal(txt_total_majmuMabalegh.Text) - Convert.ToDecimal(txt_total_kasr.Text)).ToString();
            

            txt_pardakhtJozyee_count.Text = reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Count().ToString();
            txt_pardakhtJozyee_majmu.Text = reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Sum(p => p.mande).ToString();


            txt_bedehkaran_cont.Text = reportDS1.ab_ghabz.Where(p => p.vaziat !=1).Count().ToString();
            txt_bedehkraran_majmu.Text = txt_total_mandeh.Text;


            txt_pardakht_count.Text = reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Count().ToString();
            txt_pardakht_majmu.Text = (reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.mablaghkol) - reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.kasr_hezar)).ToString();


            if (txt_total_bestankar.Text == "")
                txt_total_bestankar.Text = "0";

            if (Convert.ToDecimal(txt_total_bestankar.Text)>0)
            {
                btnShowBestankar.Visible = true;
            }
            Fill_kol_part();

            btn_print_tafsili.Visible = true;

        }

        void Fill_kol_part()
        {
            kol_maliat = Convert.ToDecimal(reportDS1.ab_ghabz.Sum(p => p.maliat));
            kol_bedehi_kasr_hezar_last_doreh =
               Convert.ToDecimal(reportDS1.ab_ghabz.Sum(p => p.bedehi)) + Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
                    "select isnull(sum(kasr_hezar),0) from ab_ghabz where dcode=" + (Convert.ToInt32(cmbsharjStart.SelectedValue)-1).ToString()
                    ));

            kol_kasr_hezar = Convert.ToDecimal(reportDS1.ab_ghabz.Sum(p => p.kasr_hezar));

            kol_majmu = Convert.ToDecimal(reportDS1.ab_ghabz.Sum(p => p.mablaghkol - p.kasr_hezar));
            kol_mablagh = kol_majmu - kol_bedehi_kasr_hezar_last_doreh - kol_maliat + kol_kasr_hezar;
            kol_pardakhti = Convert.ToDecimal(reportDS1.ab_ghabz.Sum(p => p.mablaghkol - p.mande - p.kasr_hezar + p.bestankari));
            kol_baghimande = kol_majmu - kol_pardakhti;
        }

        private void btn_show_fullPardakhti_Click(object sender, EventArgs e)
        {
            if (txt_pardakht_count.Text.Trim() != "0")
            {
                new ManagementReports.New_Report.FrmAbTafsili_show_details("مشترکینی که قبوض آب خود را به صورت کامل پرداخت کرده اند . ", cmbsharjStart.SelectedValue.ToString(), "full_pardkht").ShowDialog();
            }
        }

        private void btn_show_notPrdakht_Click(object sender, EventArgs e)
        {
            if (txt_bedehkaran_cont.Text.Trim() != "0")
            {
                new ManagementReports.New_Report.FrmAbTafsili_show_details("مشترکین بدهکار - بدهکاران قبوض آب", cmbsharjStart.SelectedValue.ToString(), "bedehkaran").ShowDialog();
            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (txt_total_tedadkol.Text.Trim() != "0" || txt_total_tedadkol.Text.Trim() != "")
            {
                CrystalReportViewer viewer = new CrystalReportViewer();
                CrysReports.bedehkar_bestankar.ab_sharj_Tafsili_mainReport
                report1 = new CrysReports.bedehkar_bestankar.ab_sharj_Tafsili_mainReport();

                CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
                CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
                ta.Fill(ds.setting);

                report1.SetDataSource(ds);

                report1.SetParameterValue("doreh", cmbsharjStart.SelectedValue.ToString() );
                report1.SetParameterValue("dorehname", "آب");

                report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
                report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
                report1.SetParameterValue("mamju", txt_total_majmuMabalegh.Text);
                report1.SetParameterValue("mamj_kasr", txt_total_kasr.Text);
                report1.SetParameterValue("mamu_pardakhti", txt_total_pardakhti.Text);
                report1.SetParameterValue("bestnkari_in_this_doreh", txt_bestankari_in_this_doreh.Text);
                report1.SetParameterValue("majmubestankari", txt_total_bestankar.Text);
                report1.SetParameterValue("majmMandehBedehi", txt_total_mandeh.Text);
                //report1.SetParameterValue("totalmaliat", txtTotalMaliat.Text);

                report1.SetParameterValue("ctotal", txt_total_tedadkol.Text);
                report1.SetParameterValue("cp", txt_pardakht_count.Text);
                report1.SetParameterValue("cnp", (Convert.ToInt32(txt_bedehkaran_cont.Text) - Convert.ToInt32(txt_pardakhtJozyee_count.Text)).ToString());
                report1.SetParameterValue("cpj", txt_pardakhtJozyee_count.Text);

                DataTable dt_info = Classes.ClsMain.GetDataTable("select ab_ghabz.gharardad,co_name,mande from moshtarekin inner join ab_ghabz on moshtarekin.gharardad=ab_ghabz.gharardad where dcode=" + cmbsharjStart.SelectedValue.ToString() + " and ab_ghabz.gharardad=(select top(1) gharardad from ab_ghabz where vaziat!=1 and dcode=" + cmbsharjStart.SelectedValue.ToString() + "order by mande desc)");
                if (dt_info.Rows.Count != 0)
                {
                    report1.SetParameterValue("maxbedehi", dt_info.Rows[0]["mande"].ToString());
                    report1.SetParameterValue("maxgharardad", dt_info.Rows[0]["gharardad"].ToString());
                    report1.SetParameterValue("maxname", dt_info.Rows[0]["co_name"].ToString());
                }
                else
                {
                    report1.SetParameterValue("maxbedehi", "");
                    report1.SetParameterValue("maxgharardad", "");
                    report1.SetParameterValue("maxname", "");
                }

                report1.SetParameterValue("kol_mablagh", kol_mablagh);
                report1.SetParameterValue("kol_maliat", kol_maliat);
                report1.SetParameterValue("kol_bedehi_kasr_hezar", kol_bedehi_kasr_hezar_last_doreh);
                report1.SetParameterValue("kol_total", kol_majmu);
                report1.SetParameterValue("kol_pardakhti", kol_pardakhti);
                report1.SetParameterValue("kol_baghimande", kol_baghimande);
                report1.SetParameterValue("kol_kasr_hezar", kol_kasr_hezar);

                report1.SetParameterValue("t_p", reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Count().ToString());
                report1.SetParameterValue("t_bp", reportDS1.ab_ghabz.Where(p => p.vaziat == 0).Count().ToString());
                report1.SetParameterValue("t_j", reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Count().ToString());

                report1.SetParameterValue("m_p", reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.IsmablaghkolNull() ? 0 : p.mablaghkol - p.kasr_hezar).ToString());
                report1.SetParameterValue("m_bp", reportDS1.ab_ghabz.Where(p => p.vaziat == 0).Sum(p => p.IsmablaghkolNull() ? 0 : p.mablaghkol - p.kasr_hezar).ToString());
                report1.SetParameterValue("m_j", reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Sum(p => p.IsmablaghkolNull() ? 0 : p.mablaghkol - p.kasr_hezar).ToString());



                report1.SetParameterValue("p_p", reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.mablaghkol - p.mande - p.kasr_hezar).ToString());
                report1.SetParameterValue("p_bp", reportDS1.ab_ghabz.Where(p => p.vaziat == 0).Sum(p => p.mablaghkol - p.mande - p.kasr_hezar).ToString());
                report1.SetParameterValue("p_j", reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Sum(p => p.mablaghkol - p.mande - p.kasr_hezar).ToString());

                report1.SetParameterValue("bes_p", reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.bestankari).ToString());
                report1.SetParameterValue("bes_bp", reportDS1.ab_ghabz.Where(p => p.vaziat == 0).Sum(p => p.bestankari).ToString());
                report1.SetParameterValue("bes_j", reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Sum(p => p.bestankari).ToString());


                report1.SetParameterValue("b_p", reportDS1.ab_ghabz.Where(p => p.vaziat == 1).Sum(p => p.mande).ToString());
                report1.SetParameterValue("b_bp", reportDS1.ab_ghabz.Where(p => p.vaziat == 0).Sum(p => p.mande).ToString());
                report1.SetParameterValue("b_j", reportDS1.ab_ghabz.Where(p => p.vaziat == 2).Sum(p => p.mande).ToString());


                viewer.ReportSource = report1;
                new FrmShowReport(report1).ShowDialog();
            }
        }

        private void btnShowBestankar_Click(object sender, EventArgs e)
        {
            if (txt_total_bestankar.Text.Trim() != "0")
            {
                new ManagementReports.New_Report.FrmAbTafsili_show_details("لیست مشترکین بستانکار قبوض اب", cmbsharjStart.SelectedValue.ToString(), "bestankaran").ShowDialog();
            }
        }
    }
}
