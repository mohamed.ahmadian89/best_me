﻿using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FarsiLibrary.Utils;

namespace CWMS.ManagementReports.New_Report
{
    public partial class FrmAbTafsili_show_details : MyMetroForm
    {
        string Type_of_gozaresh = "";
        public FrmAbTafsili_show_details(string grpinfo_t, string doreh, string type)
        {
            InitializeComponent();
            grpinfo.Text = grpinfo_t;
            lblDoreh.Text = doreh;

            if (type == "full_pardkht")
            {
                abTafsili_detailsTableAdapter.Fill(reportDS.abTafsili_details, Convert.ToInt32(doreh), 1);
            }
            else if(type=="bedehkaran")
            {
                abTafsili_detailsTableAdapter.FillBedehkraran(reportDS.abTafsili_details, Convert.ToInt32(doreh));
            }
            else
            {
                abTafsili_detailsTableAdapter.FillBestankaran(reportDS.abTafsili_details, Convert.ToInt32(doreh));

            }
            lbl_mohtarekin_count.Text = abTafsilidetailsBindingSource.Count.ToString();
            Type_of_gozaresh = grpinfo_t;
        }

        private void FrmAbTafsili_show_details_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reportDS.abTafsili_details' table. You can move, or remove it, as needed.

        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text.Trim() != "")
            {
                abTafsilidetailsBindingSource.Filter = "قرارداد=" + txtFindByGhararda.Text;
            }
            else
                txtFindByGhararda.Focus();
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            abTafsilidetailsBindingSource.RemoveFilter();
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            CrystalReportViewer viewer = new CrystalReportViewer();
            CrysReports.bedehkar_bestankar.ab_bedehi_bestankari
            report1 = new CrysReports.bedehkar_bestankar.ab_bedehi_bestankari();

            ManagementReports.New_Report.ReportDSTableAdapters.settingTableAdapter ta = new ReportDSTableAdapters.settingTableAdapter();
            ta.Fill(reportDS.setting);



            report1.SetDataSource(reportDS);
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("type", Type_of_gozaresh);
            report1.SetParameterValue("doreh", lblDoreh.Text);

            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();
        }

       
    }
}
