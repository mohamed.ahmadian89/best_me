﻿using CrystalDecisions.Windows.Forms;
using FarsiLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.ManagementReports.New_Report
{
    public partial class FrmSharjMaliat : MyMetroForm
    {
        public FrmSharjMaliat()
        {
            InitializeComponent();
        }

        private void FrmSharjMaliat_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reportDS.ab_ghabz' table. You can move, or remove it, as needed.
            txtStart.Focus();
            txtStart.Text = "1";
            txtend.Text = "1";
            txtStart.SelectAll();



        }


        private void txtStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtend.Focus();

        }

        private void txtend_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_search.PerformClick();

        }

        void Sharj_maliat()
        {
            string table_Start_name = "sharj";
            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select dcode from " + table_Start_name + "_doreh where dcode>=" + txtStart.Text + " and dcode<=" + txtend.Text + " order by dcode desc");
            for (int i = 0; i < dt_doreh.Rows.Count; i++)
            {

            }
        }
        void Ab_maliat()
        {

        }


        void sum(bool is_ab)
        {
           
            if(is_ab)
            {
                txt_kol_tedad.Text = reportDS.maliat_ab.Sum(p => p.IsتعدادNull() ? 0 : p.تعداد).ToString();
                txt_kol_majmu.Text = reportDS.maliat_ab.Sum(p => p.مجموع_مبالغ_کلیه_قبوض_دوره).ToString();
                txt_kol_maliat.Text= reportDS.maliat_ab.Sum(p => p.مالیات).ToString();

                txt_pardakhti_tedad.Text = reportDS.maliat_ab.Sum(p => (p.Isتعداد1Null() ? 0 : p.تعداد1)).ToString();
                txt_pardakhti_majmu.Text = reportDS.maliat_ab.Sum(p =>  (p.Isمجموع_مبالغ_کلیه_قبوض_دارای_پرداختNull()?0: p.مجموع_مبالغ_کلیه_قبوض_دارای_پرداخت)).ToString();
                txt_pardakhti_maliat.Text = reportDS.maliat_ab.Sum(p => (p.Isمالیات1Null() ? 0 : p.مالیات1)).ToString();
            }
            else
            {
                txt_kol_tedad.Text = reportDS.maliat_sharj.Sum(p => p.IsتعدادNull() ? 0 : p.تعداد).ToString();
                txt_kol_majmu.Text = reportDS.maliat_sharj.Sum(p => p.مجموع_مبالغ_کلیه_قبوض_دوره).ToString();
                txt_kol_maliat.Text = reportDS.maliat_sharj.Sum(p => p.مالیات).ToString();

                txt_pardakhti_tedad.Text = reportDS.maliat_sharj.Sum(p => (p.Isتعداد1Null()?0:p.تعداد1)).ToString();
                txt_pardakhti_majmu.Text = reportDS.maliat_sharj.Sum(p =>(p.Isمجموع_مبالغ_کلیه_قبوض_دوره_دارای_پرداختNull()?0: p.مجموع_مبالغ_کلیه_قبوض_دوره_دارای_پرداخت)).ToString();
                txt_pardakhti_maliat.Text = reportDS.maliat_sharj.Sum(p =>(p.Isمالیات1Null()?0: p.مالیات1)).ToString();
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
               

                int start = Convert.ToInt32(txtStart.Text);
                int end = Convert.ToInt32(txtend.Text);
                if (start > end)
                {
                    Payam.Show("دوره شروع می بایست از دوره پایان کمتر باشد");
                    return;
                }

                if (rdAb.Checked)
                {
                 

                    dgv_ab_result.Visible = true;
                    dgv_sharj_result.Visible = false;
                    maliat_abTableAdapter.Fill(reportDS.maliat_ab, start, end);
                    sum(true);
                }
                else
                {
                   
                    dgv_ab_result.Visible = false;
                    dgv_sharj_result.Visible = true;
                    maliat_sharjTableAdapter.Fill(reportDS.maliat_sharj, start, end);
                    sum(false);


                }



            }
            catch (Exception ert)
            {
                Payam.Show("لطفا شماره دوره ها را به درستی وارد کنید");
                txtStart.SelectAll();
                txtStart.Focus();
                Classes.ClsMain.logError(ert);
            }
        }
        private void btn_print_tafsili_Click(object sender, EventArgs e)
        {
            CrystalReportViewer viewer = new CrystalReportViewer();

            if (rdAb.Checked)
            {
                CrysReports.maliat.ab_maliat_report report1 = new CrysReports.maliat.ab_maliat_report();
                ManagementReports.New_Report.ReportDSTableAdapters.settingTableAdapter ta = new ReportDSTableAdapters.settingTableAdapter();
                ta.Fill(reportDS.setting);
                report1.SetDataSource(reportDS);
                report1.SetParameterValue("type", " مجموع درامد های مالیاتی آب از دوره: " + txtStart.Text + " الی: " + txtend.Text);
                report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
                report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));

                viewer.ReportSource = report1;
                new FrmShowReport(report1).ShowDialog();

            }
            else
            {
                CrysReports.maliat.sharj_maliat_report report1 = new CrysReports.maliat.sharj_maliat_report();
                ManagementReports.New_Report.ReportDSTableAdapters.settingTableAdapter ta = new ReportDSTableAdapters.settingTableAdapter();
                ta.Fill(reportDS.setting);
                report1.SetDataSource(reportDS);
                report1.SetParameterValue("type", " مجموع درآمد های مالیاتی شارژ از دوره: " + txtStart.Text + " الی: " + txtend.Text );
                report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
                report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
                viewer.ReportSource = report1;
                new FrmShowReport(report1).ShowDialog();
            }

        
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_maj_avarez_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
