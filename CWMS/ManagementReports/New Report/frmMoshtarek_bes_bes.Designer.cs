﻿namespace CWMS.ManagementReports.New_Report
{
    partial class frmMoshtarek_bes_bes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.superTabControl2 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.lbl_ab_status = new DevComponents.DotNetBar.LabelX();
            this.txt_ab_kasr = new CWMS.MoneyTextBox();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abbedbesallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportDS = new CWMS.ManagementReports.New_Report.ReportDS();
            this.txt_ab_mbes = new CWMS.MoneyTextBox();
            this.txt_ab_bed = new CWMS.MoneyTextBox();
            this.txt_ab_mbed = new CWMS.MoneyTextBox();
            this.txt_ab_bes = new CWMS.MoneyTextBox();
            this.TabSharj = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.lbl_sharj_status = new DevComponents.DotNetBar.LabelX();
            this.txtkasr = new CWMS.MoneyTextBox();
            this.txtmbes = new CWMS.MoneyTextBox();
            this.txtmbed = new CWMS.MoneyTextBox();
            this.txtbes = new CWMS.MoneyTextBox();
            this.txtbed = new CWMS.MoneyTextBox();
            this.dgvAb = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.دوره = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کدقبضDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بدهکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بستانکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهبدهکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهبستانکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کسرهزارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjbedbesallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TabAb = new DevComponents.DotNetBar.SuperTabItem();
            this.lblmoshtarekName = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtgharardad = new System.Windows.Forms.TextBox();
            this.btn_print_tafsili = new DevComponents.DotNetBar.ButtonX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btn_search = new DevComponents.DotNetBar.ButtonX();
            this.sharj_bed_bes_allTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.sharj_bed_bes_allTableAdapter();
            this.ab_bed_bes_allTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.ab_bed_bes_allTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).BeginInit();
            this.superTabControl2.SuspendLayout();
            this.superTabControlPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abbedbesallBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).BeginInit();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjbedbesallBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.superTabControl2);
            this.groupPanel1.Controls.Add(this.lblmoshtarekName);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txtgharardad);
            this.groupPanel1.Controls.Add(this.btn_print_tafsili);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.btn_search);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(12, 13);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(993, 549);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 6;
            this.groupPanel1.Text = "گزارش بدهکار / بستانکار";
            // 
            // superTabControl2
            // 
            this.superTabControl2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl2.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl2.ControlBox.MenuBox.Name = "";
            this.superTabControl2.ControlBox.Name = "";
            this.superTabControl2.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl2.ControlBox.CloseBox,
            this.superTabControl2.ControlBox.MenuBox});
            this.superTabControl2.Controls.Add(this.superTabControlPanel4);
            this.superTabControl2.Controls.Add(this.superTabControlPanel5);
            this.superTabControl2.ForeColor = System.Drawing.Color.Black;
            this.superTabControl2.Location = new System.Drawing.Point(10, 82);
            this.superTabControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControl2.Name = "superTabControl2";
            this.superTabControl2.ReorderTabsEnabled = true;
            this.superTabControl2.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl2.SelectedTabIndex = 1;
            this.superTabControl2.Size = new System.Drawing.Size(964, 433);
            this.superTabControl2.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl2.TabIndex = 98;
            this.superTabControl2.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabAb,
            this.TabSharj});
            this.superTabControl2.Text = "superTabControl2";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Controls.Add(this.lbl_ab_status);
            this.superTabControlPanel5.Controls.Add(this.txt_ab_kasr);
            this.superTabControlPanel5.Controls.Add(this.dataGridViewX1);
            this.superTabControlPanel5.Controls.Add(this.txt_ab_mbes);
            this.superTabControlPanel5.Controls.Add(this.txt_ab_bed);
            this.superTabControlPanel5.Controls.Add(this.txt_ab_mbed);
            this.superTabControlPanel5.Controls.Add(this.txt_ab_bes);
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(964, 433);
            this.superTabControlPanel5.TabIndex = 0;
            this.superTabControlPanel5.TabItem = this.TabSharj;
            // 
            // lbl_ab_status
            // 
            this.lbl_ab_status.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_ab_status.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_status.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_status.Location = new System.Drawing.Point(15, 376);
            this.lbl_ab_status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbl_ab_status.Name = "lbl_ab_status";
            this.lbl_ab_status.Size = new System.Drawing.Size(724, 23);
            this.lbl_ab_status.TabIndex = 107;
            // 
            // txt_ab_kasr
            // 
            this.txt_ab_kasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_kasr.Border.Class = "TextBoxBorder";
            this.txt_ab_kasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_kasr.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_kasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_kasr.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_kasr.Location = new System.Drawing.Point(9, 343);
            this.txt_ab_kasr.Name = "txt_ab_kasr";
            this.txt_ab_kasr.PreventEnterBeep = true;
            this.txt_ab_kasr.ReadOnly = true;
            this.txt_ab_kasr.Size = new System.Drawing.Size(94, 28);
            this.txt_ab_kasr.TabIndex = 106;
            this.txt_ab_kasr.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_ab_kasr.Text = "0";
            this.txt_ab_kasr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.AllowUserToResizeColumns = false;
            this.dataGridViewX1.AllowUserToResizeRows = false;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewX1.ColumnHeadersHeight = 28;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dataGridViewX1.DataSource = this.abbedbesallBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(8, 4);
            this.dataGridViewX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewX1.RowHeadersVisible = false;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(950, 335);
            this.dataGridViewX1.TabIndex = 101;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "دوره";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "کدقبض";
            this.dataGridViewTextBoxColumn2.FillWeight = 80F;
            this.dataGridViewTextBoxColumn2.HeaderText = "کدقبض";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "بدهکار";
            dataGridViewCellStyle10.Format = "0,0";
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn3.HeaderText = "بدهکار";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "بستانکار";
            dataGridViewCellStyle11.Format = "0,0";
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn4.HeaderText = "بستانکار";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "مانده بدهکار";
            dataGridViewCellStyle12.Format = "0,0";
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn5.HeaderText = "مانده بدهکار";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "مانده بستانکار";
            dataGridViewCellStyle13.Format = "0,0";
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn6.HeaderText = "مانده بستانکار";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "کسر هزار";
            this.dataGridViewTextBoxColumn7.FillWeight = 60F;
            this.dataGridViewTextBoxColumn7.HeaderText = "کسرهزار";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // abbedbesallBindingSource
            // 
            this.abbedbesallBindingSource.DataMember = "ab_bed_bes_all";
            this.abbedbesallBindingSource.DataSource = this.reportDS;
            // 
            // reportDS
            // 
            this.reportDS.DataSetName = "ReportDS";
            this.reportDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txt_ab_mbes
            // 
            this.txt_ab_mbes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_mbes.Border.Class = "TextBoxBorder";
            this.txt_ab_mbes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_mbes.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_mbes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_mbes.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_mbes.Location = new System.Drawing.Point(109, 343);
            this.txt_ab_mbes.Name = "txt_ab_mbes";
            this.txt_ab_mbes.PreventEnterBeep = true;
            this.txt_ab_mbes.ReadOnly = true;
            this.txt_ab_mbes.Size = new System.Drawing.Size(158, 28);
            this.txt_ab_mbes.TabIndex = 105;
            this.txt_ab_mbes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_ab_mbes.Text = "0";
            this.txt_ab_mbes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_ab_bed
            // 
            this.txt_ab_bed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_bed.Border.Class = "TextBoxBorder";
            this.txt_ab_bed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_bed.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_bed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_bed.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_bed.Location = new System.Drawing.Point(594, 343);
            this.txt_ab_bed.Name = "txt_ab_bed";
            this.txt_ab_bed.PreventEnterBeep = true;
            this.txt_ab_bed.ReadOnly = true;
            this.txt_ab_bed.Size = new System.Drawing.Size(173, 28);
            this.txt_ab_bed.TabIndex = 102;
            this.txt_ab_bed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_ab_bed.Text = "0";
            this.txt_ab_bed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_ab_mbed
            // 
            this.txt_ab_mbed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_mbed.Border.Class = "TextBoxBorder";
            this.txt_ab_mbed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_mbed.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_mbed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_mbed.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_mbed.Location = new System.Drawing.Point(273, 343);
            this.txt_ab_mbed.Name = "txt_ab_mbed";
            this.txt_ab_mbed.PreventEnterBeep = true;
            this.txt_ab_mbed.ReadOnly = true;
            this.txt_ab_mbed.Size = new System.Drawing.Size(161, 28);
            this.txt_ab_mbed.TabIndex = 104;
            this.txt_ab_mbed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_ab_mbed.Text = "0";
            this.txt_ab_mbed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_ab_bes
            // 
            this.txt_ab_bes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ab_bes.Border.Class = "TextBoxBorder";
            this.txt_ab_bes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ab_bes.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ab_bes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_bes.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_bes.Location = new System.Drawing.Point(440, 343);
            this.txt_ab_bes.Name = "txt_ab_bes";
            this.txt_ab_bes.PreventEnterBeep = true;
            this.txt_ab_bes.ReadOnly = true;
            this.txt_ab_bes.Size = new System.Drawing.Size(148, 28);
            this.txt_ab_bes.TabIndex = 103;
            this.txt_ab_bes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txt_ab_bes.Text = "0";
            this.txt_ab_bes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TabSharj
            // 
            this.TabSharj.AttachedControl = this.superTabControlPanel5;
            this.TabSharj.GlobalItem = false;
            this.TabSharj.Name = "TabSharj";
            this.TabSharj.Text = "قبوض آب";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.lbl_sharj_status);
            this.superTabControlPanel4.Controls.Add(this.txtkasr);
            this.superTabControlPanel4.Controls.Add(this.txtmbes);
            this.superTabControlPanel4.Controls.Add(this.txtmbed);
            this.superTabControlPanel4.Controls.Add(this.txtbes);
            this.superTabControlPanel4.Controls.Add(this.txtbed);
            this.superTabControlPanel4.Controls.Add(this.dgvAb);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(964, 404);
            this.superTabControlPanel4.TabIndex = 1;
            this.superTabControlPanel4.TabItem = this.TabAb;
            // 
            // lbl_sharj_status
            // 
            this.lbl_sharj_status.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_sharj_status.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sharj_status.ForeColor = System.Drawing.Color.Black;
            this.lbl_sharj_status.Location = new System.Drawing.Point(15, 372);
            this.lbl_sharj_status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbl_sharj_status.Name = "lbl_sharj_status";
            this.lbl_sharj_status.Size = new System.Drawing.Size(724, 23);
            this.lbl_sharj_status.TabIndex = 101;
            // 
            // txtkasr
            // 
            this.txtkasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtkasr.Border.Class = "TextBoxBorder";
            this.txtkasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtkasr.DisabledBackColor = System.Drawing.Color.White;
            this.txtkasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtkasr.ForeColor = System.Drawing.Color.Black;
            this.txtkasr.Location = new System.Drawing.Point(8, 337);
            this.txtkasr.Name = "txtkasr";
            this.txtkasr.PreventEnterBeep = true;
            this.txtkasr.ReadOnly = true;
            this.txtkasr.Size = new System.Drawing.Size(94, 28);
            this.txtkasr.TabIndex = 100;
            this.txtkasr.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtkasr.Text = "0";
            this.txtkasr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtmbes
            // 
            this.txtmbes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmbes.Border.Class = "TextBoxBorder";
            this.txtmbes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmbes.DisabledBackColor = System.Drawing.Color.White;
            this.txtmbes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtmbes.ForeColor = System.Drawing.Color.Black;
            this.txtmbes.Location = new System.Drawing.Point(108, 337);
            this.txtmbes.Name = "txtmbes";
            this.txtmbes.PreventEnterBeep = true;
            this.txtmbes.ReadOnly = true;
            this.txtmbes.Size = new System.Drawing.Size(158, 28);
            this.txtmbes.TabIndex = 99;
            this.txtmbes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtmbes.Text = "0";
            this.txtmbes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtmbed
            // 
            this.txtmbed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmbed.Border.Class = "TextBoxBorder";
            this.txtmbed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmbed.DisabledBackColor = System.Drawing.Color.White;
            this.txtmbed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtmbed.ForeColor = System.Drawing.Color.Black;
            this.txtmbed.Location = new System.Drawing.Point(272, 337);
            this.txtmbed.Name = "txtmbed";
            this.txtmbed.PreventEnterBeep = true;
            this.txtmbed.ReadOnly = true;
            this.txtmbed.Size = new System.Drawing.Size(161, 28);
            this.txtmbed.TabIndex = 98;
            this.txtmbed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtmbed.Text = "0";
            this.txtmbed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtbes
            // 
            this.txtbes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbes.Border.Class = "TextBoxBorder";
            this.txtbes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbes.DisabledBackColor = System.Drawing.Color.White;
            this.txtbes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtbes.ForeColor = System.Drawing.Color.Black;
            this.txtbes.Location = new System.Drawing.Point(439, 337);
            this.txtbes.Name = "txtbes";
            this.txtbes.PreventEnterBeep = true;
            this.txtbes.ReadOnly = true;
            this.txtbes.Size = new System.Drawing.Size(148, 28);
            this.txtbes.TabIndex = 97;
            this.txtbes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtbes.Text = "0";
            this.txtbes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtbed
            // 
            this.txtbed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbed.Border.Class = "TextBoxBorder";
            this.txtbed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbed.DisabledBackColor = System.Drawing.Color.White;
            this.txtbed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtbed.ForeColor = System.Drawing.Color.Black;
            this.txtbed.Location = new System.Drawing.Point(593, 337);
            this.txtbed.Name = "txtbed";
            this.txtbed.PreventEnterBeep = true;
            this.txtbed.ReadOnly = true;
            this.txtbed.Size = new System.Drawing.Size(173, 28);
            this.txtbed.TabIndex = 96;
            this.txtbed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtbed.Text = "0";
            this.txtbed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgvAb
            // 
            this.dgvAb.AllowUserToAddRows = false;
            this.dgvAb.AllowUserToDeleteRows = false;
            this.dgvAb.AllowUserToResizeColumns = false;
            this.dgvAb.AllowUserToResizeRows = false;
            this.dgvAb.AutoGenerateColumns = false;
            this.dgvAb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAb.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvAb.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAb.ColumnHeadersHeight = 28;
            this.dgvAb.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.دوره,
            this.کدقبضDataGridViewTextBoxColumn,
            this.بدهکارDataGridViewTextBoxColumn,
            this.بستانکارDataGridViewTextBoxColumn,
            this.ماندهبدهکارDataGridViewTextBoxColumn,
            this.ماندهبستانکارDataGridViewTextBoxColumn,
            this.کسرهزارDataGridViewTextBoxColumn});
            this.dgvAb.DataSource = this.sharjbedbesallBindingSource;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAb.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvAb.EnableHeadersVisualStyles = false;
            this.dgvAb.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvAb.Location = new System.Drawing.Point(7, 6);
            this.dgvAb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvAb.Name = "dgvAb";
            this.dgvAb.ReadOnly = true;
            this.dgvAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAb.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvAb.RowHeadersVisible = false;
            this.dgvAb.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAb.Size = new System.Drawing.Size(950, 322);
            this.dgvAb.TabIndex = 95;
            this.dgvAb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAb_KeyDown);
            // 
            // دوره
            // 
            this.دوره.DataPropertyName = "دوره";
            this.دوره.FillWeight = 40F;
            this.دوره.HeaderText = "دوره";
            this.دوره.Name = "دوره";
            this.دوره.ReadOnly = true;
            // 
            // کدقبضDataGridViewTextBoxColumn
            // 
            this.کدقبضDataGridViewTextBoxColumn.DataPropertyName = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.FillWeight = 80F;
            this.کدقبضDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.Name = "کدقبضDataGridViewTextBoxColumn";
            this.کدقبضDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بدهکارDataGridViewTextBoxColumn
            // 
            this.بدهکارDataGridViewTextBoxColumn.DataPropertyName = "بدهکار";
            dataGridViewCellStyle2.Format = "0,0";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.بدهکارDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.بدهکارDataGridViewTextBoxColumn.HeaderText = "بدهکار";
            this.بدهکارDataGridViewTextBoxColumn.Name = "بدهکارDataGridViewTextBoxColumn";
            this.بدهکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بستانکارDataGridViewTextBoxColumn
            // 
            this.بستانکارDataGridViewTextBoxColumn.DataPropertyName = "بستانکار";
            dataGridViewCellStyle3.Format = "0,0";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.بستانکارDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.بستانکارDataGridViewTextBoxColumn.HeaderText = "بستانکار";
            this.بستانکارDataGridViewTextBoxColumn.Name = "بستانکارDataGridViewTextBoxColumn";
            this.بستانکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندهبدهکارDataGridViewTextBoxColumn
            // 
            this.ماندهبدهکارDataGridViewTextBoxColumn.DataPropertyName = "مانده بدهکار";
            dataGridViewCellStyle4.Format = "0,0";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.ماندهبدهکارDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.ماندهبدهکارDataGridViewTextBoxColumn.HeaderText = "مانده بدهکار";
            this.ماندهبدهکارDataGridViewTextBoxColumn.Name = "ماندهبدهکارDataGridViewTextBoxColumn";
            this.ماندهبدهکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندهبستانکارDataGridViewTextBoxColumn
            // 
            this.ماندهبستانکارDataGridViewTextBoxColumn.DataPropertyName = "مانده بستانکار";
            dataGridViewCellStyle5.Format = "0,0";
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.ماندهبستانکارDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.ماندهبستانکارDataGridViewTextBoxColumn.HeaderText = "مانده بستانکار";
            this.ماندهبستانکارDataGridViewTextBoxColumn.Name = "ماندهبستانکارDataGridViewTextBoxColumn";
            this.ماندهبستانکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // کسرهزارDataGridViewTextBoxColumn
            // 
            this.کسرهزارDataGridViewTextBoxColumn.DataPropertyName = "کسر هزار";
            dataGridViewCellStyle6.Format = "0,0";
            this.کسرهزارDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.کسرهزارDataGridViewTextBoxColumn.FillWeight = 60F;
            this.کسرهزارDataGridViewTextBoxColumn.HeaderText = "کسرهزار";
            this.کسرهزارDataGridViewTextBoxColumn.Name = "کسرهزارDataGridViewTextBoxColumn";
            this.کسرهزارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjbedbesallBindingSource
            // 
            this.sharjbedbesallBindingSource.DataMember = "sharj_bed_bes_all";
            this.sharjbedbesallBindingSource.DataSource = this.reportDS;
            // 
            // TabAb
            // 
            this.TabAb.AttachedControl = this.superTabControlPanel4;
            this.TabAb.GlobalItem = false;
            this.TabAb.Name = "TabAb";
            this.TabAb.Text = "قبوض شارژ";
            // 
            // lblmoshtarekName
            // 
            this.lblmoshtarekName.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblmoshtarekName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblmoshtarekName.ForeColor = System.Drawing.Color.Black;
            this.lblmoshtarekName.Location = new System.Drawing.Point(461, 53);
            this.lblmoshtarekName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblmoshtarekName.Name = "lblmoshtarekName";
            this.lblmoshtarekName.Size = new System.Drawing.Size(433, 23);
            this.lblmoshtarekName.TabIndex = 97;
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(899, 53);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(64, 23);
            this.labelX1.TabIndex = 96;
            this.labelX1.Text = "نام مشترک :";
            // 
            // txtgharardad
            // 
            this.txtgharardad.BackColor = System.Drawing.Color.White;
            this.txtgharardad.ForeColor = System.Drawing.Color.Black;
            this.txtgharardad.Location = new System.Drawing.Point(777, 12);
            this.txtgharardad.Name = "txtgharardad";
            this.txtgharardad.Size = new System.Drawing.Size(100, 28);
            this.txtgharardad.TabIndex = 95;
            this.txtgharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgharardad_KeyDown);
            // 
            // btn_print_tafsili
            // 
            this.btn_print_tafsili.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_tafsili.BackColor = System.Drawing.Color.Transparent;
            this.btn_print_tafsili.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_tafsili.Location = new System.Drawing.Point(25, 12);
            this.btn_print_tafsili.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_print_tafsili.Name = "btn_print_tafsili";
            this.btn_print_tafsili.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_print_tafsili.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_print_tafsili.Size = new System.Drawing.Size(265, 29);
            this.btn_print_tafsili.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_tafsili.Symbol = "";
            this.btn_print_tafsili.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_tafsili.SymbolSize = 12F;
            this.btn_print_tafsili.TabIndex = 88;
            this.btn_print_tafsili.Text = "چاپ اطلاعات گزارش تفصیلی";
            this.btn_print_tafsili.Visible = false;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(883, 15);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(80, 23);
            this.labelX7.TabIndex = 49;
            this.labelX7.Text = "شماره قرارداد :";
            // 
            // btn_search
            // 
            this.btn_search.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_search.Location = new System.Drawing.Point(592, 12);
            this.btn_search.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_search.Name = "btn_search";
            this.btn_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_search.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_search.Size = new System.Drawing.Size(179, 29);
            this.btn_search.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_search.Symbol = "";
            this.btn_search.SymbolColor = System.Drawing.Color.Green;
            this.btn_search.SymbolSize = 12F;
            this.btn_search.TabIndex = 7;
            this.btn_search.Text = "نمایش اطلاعات";
            this.btn_search.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // sharj_bed_bes_allTableAdapter
            // 
            this.sharj_bed_bes_allTableAdapter.ClearBeforeFill = true;
            // 
            // ab_bed_bes_allTableAdapter
            // 
            this.ab_bed_bes_allTableAdapter.ClearBeforeFill = true;
            // 
            // frmMoshtarek_bes_bes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 564);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMoshtarek_bes_bes";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmSharj_bed_besForAll_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).EndInit();
            this.superTabControl2.ResumeLayout(false);
            this.superTabControlPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abbedbesallBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).EndInit();
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjbedbesallBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_print_tafsili;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btn_search;
        private ReportDS reportDS;
        private System.Windows.Forms.BindingSource sharjbedbesallBindingSource;
        private ReportDSTableAdapters.sharj_bed_bes_allTableAdapter sharj_bed_bes_allTableAdapter;
        private System.Windows.Forms.TextBox txtgharardad;
        private DevComponents.DotNetBar.LabelX lblmoshtarekName;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.SuperTabControl superTabControl2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private MoneyTextBox txt_ab_kasr;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private MoneyTextBox txt_ab_mbes;
        private MoneyTextBox txt_ab_bed;
        private MoneyTextBox txt_ab_mbed;
        private MoneyTextBox txt_ab_bes;
        private DevComponents.DotNetBar.SuperTabItem TabSharj;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private MoneyTextBox txtkasr;
        private MoneyTextBox txtmbes;
        private MoneyTextBox txtmbed;
        private MoneyTextBox txtbes;
        private MoneyTextBox txtbed;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvAb;
        private DevComponents.DotNetBar.SuperTabItem TabAb;
        private System.Windows.Forms.BindingSource abbedbesallBindingSource;
        private ReportDSTableAdapters.ab_bed_bes_allTableAdapter ab_bed_bes_allTableAdapter;
        private DevComponents.DotNetBar.LabelX lbl_sharj_status;
        private DevComponents.DotNetBar.LabelX lbl_ab_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn دوره;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بدهکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بستانکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهبدهکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهبستانکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn کسرهزارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    }
}