﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.ManagementReports.New_Report
{
    public partial class frmMoshtarek_bes_bes : MyMetroForm
    {
        public frmMoshtarek_bes_bes()
        {
            InitializeComponent();
        }

        private void FrmSharj_bed_besForAll_Load(object sender, EventArgs e)
        {
            txtgharardad.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtgharardad.Text == "")
                txtgharardad.Focus();
            else
            {
                try
                {
                    int gharardad = Convert.ToInt32(txtgharardad.Text);
                    sharj_bed_bes_allTableAdapter.FillForGharardad(reportDS.sharj_bed_bes_all,gharardad);
                    ab_bed_bes_allTableAdapter.FillforGharardad(reportDS.ab_bed_bes_all, gharardad);
                    Moshtarek_name();
                    Sharj();
                    Ab();
                }
                catch (Exception)
                {

                    Payam.Show("قرارداد وارد شده معتبر نمی باشد ");
                    txtgharardad.SelectAll();
                    txtgharardad.Focus();
                }
              
            }
          
        }

        void Moshtarek_name()
        {
            if (reportDS.sharj_bed_bes_all.Rows.Count > 0)
                lblmoshtarekName.Text = reportDS.sharj_bed_bes_all.Rows[0]["مشترک"].ToString();
            else if (reportDS.ab_bed_bes_all.Rows.Count > 0)
                lblmoshtarekName.Text = reportDS.ab_bed_bes_all.Rows[0]["مشترک"].ToString();
            else
                lblmoshtarekName.Text = "";
        }
        void Sharj()
        {
            decimal SumofBedehkar = 0, SumofBestankar = 0, SumofKasr = 0, sumofmandebed = 0, sumofmandebes = 0;
            int bedehkar = 0, bestankar = 0;
            for (int i = 0; i < reportDS.sharj_bed_bes_all.Rows.Count; i++)
            {
                decimal tafazol = Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بدهکار"]) - Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بستانکار"]);
                if (tafazol > 0)
                    reportDS.sharj_bed_bes_all.Rows[i]["مانده بدهکار"] = tafazol;
                else
                    reportDS.sharj_bed_bes_all.Rows[i]["مانده بستانکار"] = Math.Abs(tafazol);

                SumofBedehkar += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بدهکار"]);
                SumofBestankar += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بستانکار"]);
                sumofmandebed += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["مانده بدهکار"]);
                sumofmandebes += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["مانده بستانکار"]);
                SumofKasr += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["کسر هزار"]);

                //if (Convert.ToInt32(reportDS.sharj_bed_bes_all.Rows[i]["vaziat"]) != 1)
                //    bedehkar++;

                //if (Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["مانده بستانکار"]) > 0)
                //    bestankar++;
                    
            }



            txtbed.Text = SumofBedehkar.ToString();
            txtbes.Text = SumofBestankar.ToString();
            txtmbed.Text = sumofmandebed.ToString();
            txtmbes.Text = sumofmandebes.ToString();
            txtkasr.Text = SumofKasr.ToString();

            //lbl_bedehkaran_count.Text = bedehkar.ToString();
            //lbl_bestnkanarn_count.Text = bestankar.ToString();

            if (SumofBedehkar > SumofBestankar)
                lbl_sharj_status.Text = "بر اساس کلیه قبوض  شارژ صادرشده، این مشترک مبلغ " + (SumofBedehkar - SumofBestankar).ToString() + " بدهکار می باشد ";
            else
                lbl_sharj_status.Text = "بر اساس کلیه قبوض  شارژ صادرشده، این مشترک مبلغ " + (SumofBestankar - SumofBedehkar).ToString() + " بستانکار می باشد ";

        }

        void Ab()
        {
            decimal SumofBedehkar = 0, SumofBestankar = 0, SumofKasr = 0, sumofmandebed = 0, sumofmandebes = 0;
            for (int i = 0; i < reportDS.ab_bed_bes_all.Rows.Count; i++)
            {
                decimal tafazol = Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بدهکار"]) - Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بستانکار"]);
                if (tafazol > 0)
                    reportDS.ab_bed_bes_all.Rows[i]["مانده بدهکار"] = tafazol;
                else
                    reportDS.ab_bed_bes_all.Rows[i]["مانده بستانکار"] = Math.Abs(tafazol);

                SumofBedehkar += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بدهکار"]);
                SumofBestankar += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بستانکار"]);
                sumofmandebed += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["مانده بدهکار"]);
                sumofmandebes += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["مانده بستانکار"]);
                SumofKasr += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["کسر هزار"]);
            }


            txt_ab_bed.Text = SumofBedehkar.ToString();
            txt_ab_bes.Text = SumofBestankar.ToString();
            txt_ab_mbed.Text = sumofmandebed.ToString();
            txt_ab_mbes.Text = sumofmandebes.ToString();
            txt_ab_kasr.Text = SumofKasr.ToString();

            if (SumofBedehkar > SumofBestankar)
                lbl_ab_status.Text = "بر اساس کلیه قبوض آب صادرشده، این مشترک مبلغ " + (SumofBedehkar - SumofBestankar).ToString() + " بدهکار می باشد ";
            else
                lbl_ab_status.Text = "بر اساس کلیه قبوض آب صادرشده، این مشترک مبلغ " + (SumofBestankar - SumofBedehkar).ToString() + " بستانکار می باشد ";

        }

        private void txtgharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_search.PerformClick();
        }

        private void dgvAb_KeyDown(object sender, KeyEventArgs e)
        {
            
        }
    }
}
