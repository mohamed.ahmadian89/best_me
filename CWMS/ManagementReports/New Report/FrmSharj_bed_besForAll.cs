﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.ManagementReports.New_Report
{
    public partial class FrmSharj_bed_besForAll : MyMetroForm
    {
        string type;
        public FrmSharj_bed_besForAll(string type_t)
        {
            InitializeComponent();
            type = type_t;
            if (type == "ab")
            {
                grp_info.Text = "گزارش بدهکار / بستانکار دوره های آب";
                dgv_sharj.Visible = false;
                dgv_ab.Visible = true;
            }
            else
            {
                grp_info.Text = "گزارش بدهکار / بستانکار دوره های شارژ";
                dgv_sharj.Visible = true;
                dgv_ab.Visible = false;
            }


        }

        private void FrmSharj_bed_besForAll_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reportDS.sharj_bed_bes_all' table. You can move, or remove it, as needed.
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select dcode from " + type + "_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
            if (cmbDoreh.Items.Count == 0)
                btnsave.Enabled = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            decimal SumofBedehkar = 0, SumofBestankar = 0, SumofKasr = 0, sumofmandebed = 0, sumofmandebes = 0;

            if (type == "ab")
            {
                ab_bed_bes_allTableAdapter1.FillForDoreh(reportDS.ab_bed_bes_all, Convert.ToInt32(cmbDoreh.SelectedValue));
                for (int i = 0; i < reportDS.ab_bed_bes_all.Rows.Count; i++)
                {
                    decimal tafazol = Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بدهکار"]) - Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بستانکار"]);
                    if (tafazol > 0)
                        reportDS.ab_bed_bes_all.Rows[i]["مانده بدهکار"] = tafazol;
                    else
                        reportDS.ab_bed_bes_all.Rows[i]["مانده بستانکار"] = Math.Abs(tafazol);

                    SumofBedehkar += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بدهکار"]);
                    SumofBestankar += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["بستانکار"]);
                    sumofmandebed += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["مانده بدهکار"]);
                    sumofmandebes += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["مانده بستانکار"]);
                    SumofKasr += Convert.ToDecimal(reportDS.ab_bed_bes_all.Rows[i]["کسر هزار"]);
                }
            }

            else
            {
                sharj_bed_bes_allTableAdapter.FillForDoreh(reportDS.sharj_bed_bes_all, Convert.ToInt32(cmbDoreh.SelectedValue));
                for (int i = 0; i < reportDS.sharj_bed_bes_all.Rows.Count; i++)
                {
                    decimal tafazol = Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بدهکار"]) - Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بستانکار"]);
                    if (tafazol > 0)
                        reportDS.sharj_bed_bes_all.Rows[i]["مانده بدهکار"] = tafazol;
                    else
                        reportDS.sharj_bed_bes_all.Rows[i]["مانده بستانکار"] = Math.Abs(tafazol);

                    SumofBedehkar += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بدهکار"]);
                    SumofBestankar += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["بستانکار"]);
                    sumofmandebed += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["مانده بدهکار"]);
                    sumofmandebes += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["مانده بستانکار"]);
                    SumofKasr += Convert.ToDecimal(reportDS.sharj_bed_bes_all.Rows[i]["کسر هزار"]);
                }
            }




            txtbed.Text = SumofBedehkar.ToString();
            txtbes.Text = SumofBestankar.ToString();
            txtmbed.Text = sumofmandebed.ToString();
            txtmbes.Text = sumofmandebes.ToString();
            txtkasr.Text = SumofKasr.ToString();


        }

        private void btn_print_tafsili_Click(object sender, EventArgs e)
        {

        }
    }
}
