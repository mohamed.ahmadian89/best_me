﻿namespace CWMS.ManagementReports.New_Report
{
    partial class FrmSharj_bed_besForAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grp_info = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtkasr = new CWMS.MoneyTextBox();
            this.txtmbes = new CWMS.MoneyTextBox();
            this.txtmbed = new CWMS.MoneyTextBox();
            this.txtbes = new CWMS.MoneyTextBox();
            this.txtbed = new CWMS.MoneyTextBox();
            this.dgv_sharj = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.کدقبضDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مشترکDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بدهکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.بستانکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهبدهکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ماندهبستانکارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.کسرهزارDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjbedbesallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportDS = new CWMS.ManagementReports.New_Report.ReportDS();
            this.btn_print_tafsili = new DevComponents.DotNetBar.ButtonX();
            this.cmbDoreh = new System.Windows.Forms.ComboBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btnsave = new DevComponents.DotNetBar.ButtonX();
            this.sharj_bed_bes_allTableAdapter = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.sharj_bed_bes_allTableAdapter();
            this.ab_bed_bes_allTableAdapter1 = new CWMS.ManagementReports.New_Report.ReportDSTableAdapters.ab_bed_bes_allTableAdapter();
            this.dgv_ab = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abbedbesallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grp_info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjbedbesallBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abbedbesallBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grp_info
            // 
            this.grp_info.BackColor = System.Drawing.Color.White;
            this.grp_info.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_info.Controls.Add(this.dgv_ab);
            this.grp_info.Controls.Add(this.txtkasr);
            this.grp_info.Controls.Add(this.txtmbes);
            this.grp_info.Controls.Add(this.txtmbed);
            this.grp_info.Controls.Add(this.txtbes);
            this.grp_info.Controls.Add(this.txtbed);
            this.grp_info.Controls.Add(this.dgv_sharj);
            this.grp_info.Controls.Add(this.btn_print_tafsili);
            this.grp_info.Controls.Add(this.cmbDoreh);
            this.grp_info.Controls.Add(this.labelX7);
            this.grp_info.Controls.Add(this.btnsave);
            this.grp_info.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_info.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grp_info.Location = new System.Drawing.Point(12, 13);
            this.grp_info.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grp_info.Name = "grp_info";
            this.grp_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_info.Size = new System.Drawing.Size(993, 527);
            // 
            // 
            // 
            this.grp_info.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_info.Style.BackColorGradientAngle = 90;
            this.grp_info.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_info.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_info.Style.BorderBottomWidth = 1;
            this.grp_info.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_info.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_info.Style.BorderLeftWidth = 1;
            this.grp_info.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_info.Style.BorderRightWidth = 1;
            this.grp_info.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_info.Style.BorderTopWidth = 1;
            this.grp_info.Style.CornerDiameter = 4;
            this.grp_info.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_info.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_info.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_info.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_info.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_info.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_info.TabIndex = 6;
            this.grp_info.Text = "گزارش بدهکار / بستانکار";
            // 
            // txtkasr
            // 
            this.txtkasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtkasr.Border.Class = "TextBoxBorder";
            this.txtkasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtkasr.DisabledBackColor = System.Drawing.Color.White;
            this.txtkasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtkasr.ForeColor = System.Drawing.Color.Black;
            this.txtkasr.Location = new System.Drawing.Point(4, 441);
            this.txtkasr.Name = "txtkasr";
            this.txtkasr.PreventEnterBeep = true;
            this.txtkasr.ReadOnly = true;
            this.txtkasr.Size = new System.Drawing.Size(78, 28);
            this.txtkasr.TabIndex = 94;
            this.txtkasr.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtkasr.Text = "0";
            this.txtkasr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtmbes
            // 
            this.txtmbes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmbes.Border.Class = "TextBoxBorder";
            this.txtmbes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmbes.DisabledBackColor = System.Drawing.Color.White;
            this.txtmbes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtmbes.ForeColor = System.Drawing.Color.Black;
            this.txtmbes.Location = new System.Drawing.Point(83, 441);
            this.txtmbes.Name = "txtmbes";
            this.txtmbes.PreventEnterBeep = true;
            this.txtmbes.ReadOnly = true;
            this.txtmbes.Size = new System.Drawing.Size(139, 28);
            this.txtmbes.TabIndex = 93;
            this.txtmbes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtmbes.Text = "0";
            this.txtmbes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtmbed
            // 
            this.txtmbed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmbed.Border.Class = "TextBoxBorder";
            this.txtmbed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmbed.DisabledBackColor = System.Drawing.Color.White;
            this.txtmbed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtmbed.ForeColor = System.Drawing.Color.Black;
            this.txtmbed.Location = new System.Drawing.Point(228, 441);
            this.txtmbed.Name = "txtmbed";
            this.txtmbed.PreventEnterBeep = true;
            this.txtmbed.ReadOnly = true;
            this.txtmbed.Size = new System.Drawing.Size(127, 28);
            this.txtmbed.TabIndex = 92;
            this.txtmbed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtmbed.Text = "0";
            this.txtmbed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtbes
            // 
            this.txtbes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbes.Border.Class = "TextBoxBorder";
            this.txtbes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbes.DisabledBackColor = System.Drawing.Color.White;
            this.txtbes.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtbes.ForeColor = System.Drawing.Color.Black;
            this.txtbes.Location = new System.Drawing.Point(361, 441);
            this.txtbes.Name = "txtbes";
            this.txtbes.PreventEnterBeep = true;
            this.txtbes.ReadOnly = true;
            this.txtbes.Size = new System.Drawing.Size(148, 28);
            this.txtbes.TabIndex = 91;
            this.txtbes.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtbes.Text = "0";
            this.txtbes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtbed
            // 
            this.txtbed.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbed.Border.Class = "TextBoxBorder";
            this.txtbed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbed.DisabledBackColor = System.Drawing.Color.White;
            this.txtbed.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtbed.ForeColor = System.Drawing.Color.Black;
            this.txtbed.Location = new System.Drawing.Point(515, 441);
            this.txtbed.Name = "txtbed";
            this.txtbed.PreventEnterBeep = true;
            this.txtbed.ReadOnly = true;
            this.txtbed.Size = new System.Drawing.Size(147, 28);
            this.txtbed.TabIndex = 90;
            this.txtbed.Tag = "lbl_moshtarek_mablagh_kol";
            this.txtbed.Text = "0";
            this.txtbed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgv_sharj
            // 
            this.dgv_sharj.AllowUserToAddRows = false;
            this.dgv_sharj.AllowUserToDeleteRows = false;
            this.dgv_sharj.AllowUserToResizeColumns = false;
            this.dgv_sharj.AllowUserToResizeRows = false;
            this.dgv_sharj.AutoGenerateColumns = false;
            this.dgv_sharj.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sharj.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_sharj.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sharj.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_sharj.ColumnHeadersHeight = 28;
            this.dgv_sharj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.کدقبضDataGridViewTextBoxColumn,
            this.مشترکDataGridViewTextBoxColumn,
            this.بدهکارDataGridViewTextBoxColumn,
            this.بستانکارDataGridViewTextBoxColumn,
            this.ماندهبدهکارDataGridViewTextBoxColumn,
            this.ماندهبستانکارDataGridViewTextBoxColumn,
            this.کسرهزارDataGridViewTextBoxColumn});
            this.dgv_sharj.DataSource = this.sharjbedbesallBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sharj.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_sharj.EnableHeadersVisualStyles = false;
            this.dgv_sharj.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_sharj.Location = new System.Drawing.Point(16, 51);
            this.dgv_sharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgv_sharj.Name = "dgv_sharj";
            this.dgv_sharj.ReadOnly = true;
            this.dgv_sharj.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sharj.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_sharj.RowHeadersVisible = false;
            this.dgv_sharj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_sharj.Size = new System.Drawing.Size(945, 380);
            this.dgv_sharj.TabIndex = 89;
            // 
            // کدقبضDataGridViewTextBoxColumn
            // 
            this.کدقبضDataGridViewTextBoxColumn.DataPropertyName = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.FillWeight = 70F;
            this.کدقبضDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.کدقبضDataGridViewTextBoxColumn.Name = "کدقبضDataGridViewTextBoxColumn";
            this.کدقبضDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مشترکDataGridViewTextBoxColumn
            // 
            this.مشترکDataGridViewTextBoxColumn.DataPropertyName = "مشترک";
            this.مشترکDataGridViewTextBoxColumn.FillWeight = 170F;
            this.مشترکDataGridViewTextBoxColumn.HeaderText = "مشترک";
            this.مشترکDataGridViewTextBoxColumn.Name = "مشترکDataGridViewTextBoxColumn";
            this.مشترکDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بدهکارDataGridViewTextBoxColumn
            // 
            this.بدهکارDataGridViewTextBoxColumn.DataPropertyName = "بدهکار";
            this.بدهکارDataGridViewTextBoxColumn.HeaderText = "بدهکار";
            this.بدهکارDataGridViewTextBoxColumn.Name = "بدهکارDataGridViewTextBoxColumn";
            this.بدهکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // بستانکارDataGridViewTextBoxColumn
            // 
            this.بستانکارDataGridViewTextBoxColumn.DataPropertyName = "بستانکار";
            this.بستانکارDataGridViewTextBoxColumn.HeaderText = "بستانکار";
            this.بستانکارDataGridViewTextBoxColumn.Name = "بستانکارDataGridViewTextBoxColumn";
            this.بستانکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندهبدهکارDataGridViewTextBoxColumn
            // 
            this.ماندهبدهکارDataGridViewTextBoxColumn.DataPropertyName = "مانده بدهکار";
            this.ماندهبدهکارDataGridViewTextBoxColumn.HeaderText = "مانده بدهکار";
            this.ماندهبدهکارDataGridViewTextBoxColumn.Name = "ماندهبدهکارDataGridViewTextBoxColumn";
            this.ماندهبدهکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ماندهبستانکارDataGridViewTextBoxColumn
            // 
            this.ماندهبستانکارDataGridViewTextBoxColumn.DataPropertyName = "مانده بستانکار";
            this.ماندهبستانکارDataGridViewTextBoxColumn.HeaderText = "مانده بستانکار";
            this.ماندهبستانکارDataGridViewTextBoxColumn.Name = "ماندهبستانکارDataGridViewTextBoxColumn";
            this.ماندهبستانکارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // کسرهزارDataGridViewTextBoxColumn
            // 
            this.کسرهزارDataGridViewTextBoxColumn.DataPropertyName = "کسر هزار";
            this.کسرهزارDataGridViewTextBoxColumn.FillWeight = 45F;
            this.کسرهزارDataGridViewTextBoxColumn.HeaderText = "کسرهزار";
            this.کسرهزارDataGridViewTextBoxColumn.Name = "کسرهزارDataGridViewTextBoxColumn";
            this.کسرهزارDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjbedbesallBindingSource
            // 
            this.sharjbedbesallBindingSource.DataMember = "sharj_bed_bes_all";
            this.sharjbedbesallBindingSource.DataSource = this.reportDS;
            // 
            // reportDS
            // 
            this.reportDS.DataSetName = "ReportDS";
            this.reportDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btn_print_tafsili
            // 
            this.btn_print_tafsili.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_print_tafsili.BackColor = System.Drawing.Color.Transparent;
            this.btn_print_tafsili.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_print_tafsili.Location = new System.Drawing.Point(25, 12);
            this.btn_print_tafsili.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_print_tafsili.Name = "btn_print_tafsili";
            this.btn_print_tafsili.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_print_tafsili.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_print_tafsili.Size = new System.Drawing.Size(265, 29);
            this.btn_print_tafsili.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_print_tafsili.Symbol = "";
            this.btn_print_tafsili.SymbolColor = System.Drawing.Color.Green;
            this.btn_print_tafsili.SymbolSize = 12F;
            this.btn_print_tafsili.TabIndex = 88;
            this.btn_print_tafsili.Text = "چاپ اطلاعات گزارش تفصیلی";
            this.btn_print_tafsili.Visible = false;
            this.btn_print_tafsili.Click += new System.EventHandler(this.btn_print_tafsili_Click);
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDoreh.BackColor = System.Drawing.Color.White;
            this.cmbDoreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoreh.ForeColor = System.Drawing.Color.Black;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.Location = new System.Drawing.Point(668, 12);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.Size = new System.Drawing.Size(69, 28);
            this.cmbDoreh.TabIndex = 50;
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(749, 15);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(195, 23);
            this.labelX7.TabIndex = 49;
            this.labelX7.Text = "لطفا دوره مورد نظر خود را انتخاب کنید :";
            // 
            // btnsave
            // 
            this.btnsave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnsave.Location = new System.Drawing.Point(476, 12);
            this.btnsave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnsave.Name = "btnsave";
            this.btnsave.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnsave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnsave.Size = new System.Drawing.Size(179, 29);
            this.btnsave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnsave.Symbol = "";
            this.btnsave.SymbolColor = System.Drawing.Color.Green;
            this.btnsave.SymbolSize = 12F;
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "نمایش اطلاعات";
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // sharj_bed_bes_allTableAdapter
            // 
            this.sharj_bed_bes_allTableAdapter.ClearBeforeFill = true;
            // 
            // ab_bed_bes_allTableAdapter1
            // 
            this.ab_bed_bes_allTableAdapter1.ClearBeforeFill = true;
            // 
            // dgv_ab
            // 
            this.dgv_ab.AllowUserToAddRows = false;
            this.dgv_ab.AllowUserToDeleteRows = false;
            this.dgv_ab.AllowUserToResizeColumns = false;
            this.dgv_ab.AllowUserToResizeRows = false;
            this.dgv_ab.AutoGenerateColumns = false;
            this.dgv_ab.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ab.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_ab.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ab.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ab.ColumnHeadersHeight = 28;
            this.dgv_ab.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dgv_ab.DataSource = this.abbedbesallBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ab.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ab.EnableHeadersVisualStyles = false;
            this.dgv_ab.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_ab.Location = new System.Drawing.Point(16, 50);
            this.dgv_ab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgv_ab.Name = "dgv_ab";
            this.dgv_ab.ReadOnly = true;
            this.dgv_ab.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ab.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ab.RowHeadersVisible = false;
            this.dgv_ab.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ab.Size = new System.Drawing.Size(945, 380);
            this.dgv_ab.TabIndex = 95;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "کدقبض";
            this.dataGridViewTextBoxColumn1.FillWeight = 70F;
            this.dataGridViewTextBoxColumn1.HeaderText = "کدقبض";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "مشترک";
            this.dataGridViewTextBoxColumn2.FillWeight = 170F;
            this.dataGridViewTextBoxColumn2.HeaderText = "مشترک";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "بدهکار";
            this.dataGridViewTextBoxColumn3.HeaderText = "بدهکار";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "بستانکار";
            this.dataGridViewTextBoxColumn4.HeaderText = "بستانکار";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "مانده بدهکار";
            this.dataGridViewTextBoxColumn5.HeaderText = "مانده بدهکار";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "مانده بستانکار";
            this.dataGridViewTextBoxColumn6.HeaderText = "مانده بستانکار";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "کسر هزار";
            this.dataGridViewTextBoxColumn7.FillWeight = 45F;
            this.dataGridViewTextBoxColumn7.HeaderText = "کسرهزار";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // abbedbesallBindingSource
            // 
            this.abbedbesallBindingSource.DataMember = "ab_bed_bes_all";
            this.abbedbesallBindingSource.DataSource = this.reportDS;
            // 
            // FrmSharj_bed_besForAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 541);
            this.Controls.Add(this.grp_info);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSharj_bed_besForAll";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmSharj_bed_besForAll_Load);
            this.grp_info.ResumeLayout(false);
            this.grp_info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjbedbesallBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abbedbesallBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_info;
        private DevComponents.DotNetBar.ButtonX btn_print_tafsili;
        private System.Windows.Forms.ComboBox cmbDoreh;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btnsave;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_sharj;
        private ReportDS reportDS;
        private System.Windows.Forms.BindingSource sharjbedbesallBindingSource;
        private ReportDSTableAdapters.sharj_bed_bes_allTableAdapter sharj_bed_bes_allTableAdapter;
        private MoneyTextBox txtkasr;
        private MoneyTextBox txtmbes;
        private MoneyTextBox txtmbed;
        private MoneyTextBox txtbes;
        private MoneyTextBox txtbed;
        private System.Windows.Forms.DataGridViewTextBoxColumn کدقبضDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مشترکDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بدهکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn بستانکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهبدهکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ماندهبستانکارDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn کسرهزارDataGridViewTextBoxColumn;
        private ReportDSTableAdapters.ab_bed_bes_allTableAdapter ab_bed_bes_allTableAdapter1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_ab;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.BindingSource abbedbesallBindingSource;
    }
}