﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Snippet;
using CWMS.Classes;

namespace CWMS.ManagementReports
{
    public partial class frmBedehkaranRpt : MyMetroForm
    {
        public frmBedehkaranRpt()
        {
            InitializeComponent();
        }

        private void frmBedehkaranRpt_Load(object sender, EventArgs e)
        {
            // queries
            string getBedehiPerDorehAb =
                "select dcode, sum(bedehi) as total,sum(mablaghkol) as kol,  sum(mande) as bedehiOfDoreh from ab_ghabz group by dcode order by dcode desc";
            string getBadhesabListAb =
                "select gharardad, max(mande) as total, count(mande) as countBedehi from ab_ghabz where mande<>0 group by gharardad order by total desc";
            string getBedehiPerDorehSharj =
                "select dcode, sum(bedehi) as total,sum(mablaghkol) as kol,  sum(mande) as bedehiOfDoreh  from sharj_ghabz group by dcode order by dcode desc";
            string getBadhesabListSharj =
                "select gharardad, max(mande) as total, count(mande) as countBedehi from sharj_ghabz where mande<>0 group by gharardad order by total desc";

            // fetch queries
            try
            {
                dgv_perdoreh_ab.DataSource = Classes.ClsMain.GetDataTable(getBedehiPerDorehAb);
                dgv_badhesab_ab.DataSource = Classes.ClsMain.GetDataTable(getBadhesabListAb);
                dgv_perdoreh_sharj.DataSource = Classes.ClsMain.GetDataTable(getBedehiPerDorehSharj);
                dgv_badhesab_sharj.DataSource = Classes.ClsMain.GetDataTable(getBadhesabListSharj);
            }
            catch (Exception ex)
            {
                Payam.Show("خطا! لطفا با واحد پشتیبان هماهنگ نمایید");
                ClsMain.logError(ex);
            }
        }

        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {

        }
    }
}