﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;
using FarsiLibrary.Utils;

namespace CWMS.ManagementReports
{
    public partial class FrmGhabzPardakhtRpt : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmGhabzPardakhtRpt()
        {
            InitializeComponent();
        }

        private void FrmGhabzPardakhtRpt_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'daramadDataset.ab_pardakht_report' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'daramadDataset.Sharj_Pardakht_report' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'daramadDataset.Sharj_Pardakht_report' table. You can move, or remove it, as needed.
            Classes.ClsMain.ChangeCulture("f");
            DateTime now = DateTime.Now;
            txt_start.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            txt_end.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            txt_start.Focus();
            txt_end.Focus();

        }
        bool IsByTairkh = false;
        private void BtnFindByTarikh_Click(object sender, EventArgs e)
        {
            IsByTairkh = true;
            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);

            sharj_ghabz_PardakhtTableAdapter.Fill(daramadDataset.sharj_ghabz_Pardakht, start, finish);
            ab_ghabzTableAdapter.Fill(daramadDataset1.ab_ghabz, start, finish);

        }



        private void buttonX10_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.ab_pardakhti rpt = new CrysReports.ab_pardakhti();

            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);



            string pStart, Pend;
            pStart = txt_start.Text;
            Pend = txt_end.Text;
            int doreh = -1;
            try
            {
                doreh = Convert.ToInt32(txt_start_doreh.Text);
            }
            catch (Exception)
            {
                doreh = -1;
            }
            Classes.clsAb.Ab_moshtarek_pardakhti(repVUer, rpt, start, finish, pStart, Pend, IsByTairkh, doreh);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.sharj_pardakhti rpt = new CrysReports.sharj_pardakhti();




            DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);



            string pStart, Pend;
            pStart = txt_start.Text;
            Pend = txt_end.Text;

            int doreh = -1;
            try
            {
                doreh = Convert.ToInt32(txt_start_doreh.Text);
            }
            catch (Exception)
            {
                doreh = -1;
            }

            Classes.clsSharj.Sharj_moshtarek_pardakhti(repVUer, rpt, start, finish, pStart, Pend, IsByTairkh, doreh);
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            DataTable dt_pardakht = Classes.ClsMain.GetDataTable("select * from ghabz_pardakht where nahve_pardakht='چک'");
            Classes.ClsMain.ChangeCulture("e");
            for (int i = 0; i < dt_pardakht.Rows.Count; i++)
            {
                string gharardad = dt_pardakht.Rows[i]["gharardad"].ToString();
                string gcode = dt_pardakht.Rows[i]["gcode"].ToString();

                string TableName = "sharj_ghabz";
                if (Convert.ToBoolean(dt_pardakht.Rows[i]["is_ab"]))
                {
                    TableName = "ab_ghabz";
                }
                DateTime last_pardakht = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select top(1) tarikh_vosool from ghabz_pardakht where gcode=" + gcode + " order by tarikh_vosool desc"));
                Classes.ClsMain.ExecuteNoneQuery("update " + TableName + " set last_pardakht='" + last_pardakht + "' where gcode=" + gcode);

            }
            Classes.ClsMain.ChangeCulture("f");
            Payam.Show("اصلاحات انجام شد . در حال نمایش اصلاحیه");
            sharj_ghabz_PardakhtTableAdapter.FillBy(daramadDataset.sharj_ghabz_Pardakht);
            ab_ghabzTableAdapter.FillBy(daramadDataset1.ab_ghabz);

        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            //{
            //    CrystalReportViewer repVUer = new CrystalReportViewer();
            //    CrysReports.ab_pardakhti rpt = new CrysReports.ab_pardakhti();

            //    string pStart, Pend;
            //    pStart = txt_start.Text;
            //    Pend = txt_end.Text;
            //    Classes.clsAb.Ab_moshtarek_pardakhti(repVUer, rpt, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime), pStart, Pend, true);
            //}

            //{
            //    CrystalReportViewer repVUer = new CrystalReportViewer();
            //    CrysReports.sharj_pardakhti rpt = new CrysReports.sharj_pardakhti();

            //    string pStart, Pend;
            //    pStart = txt_start.Text;
            //    Pend = txt_end.Text;

            //    Classes.clsSharj.Sharj_moshtarek_pardakhti(repVUer, rpt, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime), pStart, Pend, true);
            //}
        }
        

        private void buttonX3_Click(object sender, EventArgs e)
        {
            try
            {
                
                int start_doreh = Convert.ToInt32(txt_start_doreh.Text);
                int end_doreh = Convert.ToInt32(txt_end_doreh.Text);

                IsByTairkh = false;
                sharj_ghabz_PardakhtTableAdapter.FillByDorehBazeh(daramadDataset.sharj_ghabz_Pardakht, start_doreh,end_doreh);
                //sharj_Pardakht_reportTableAdapter.FillByTarikh(daramadDataset.Sharj_Pardakht_report, Convert.ToDateTime(txt_start.SelectedDateTime), Convert.ToDateTime(txt_end.SelectedDateTime));
                ab_ghabzTableAdapter.FillByDoreh(daramadDataset1.ab_ghabz, start_doreh);
            }
            catch (Exception)
            {

                Payam.Show("لطفا دوره را به صورت صحیح وارد نمایید");
                txt_start_doreh.SelectAll();
                txt_start_doreh.Focus();
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {

            print_pardakhti("قبوض شارژ", 0);
        }
        void print_pardakhti(string type, int is_ab)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.ghabz_pardakhtTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.ghabz_pardakhtTableAdapter();
            CrysReports.pardakhti_report report1 = new CrysReports.pardakhti_report();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter tas = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            tas.Fill(ds.setting);

            if (IsByTairkh)
            {
                DateTime start = Convert.ToDateTime(txt_start.SelectedDateTime);
                start = start.AddHours(-start.Hour);

                DateTime finish = Convert.ToDateTime(txt_end.SelectedDateTime);
                finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);


                if (is_ab == 2)
                    ta1.FillBytarikh_multiple(ds.ghabz_pardakht, start, finish);
                else
                    ta1.FillByTarikh_One_type(ds.ghabz_pardakht, start, finish, (is_ab == 1 ? true : false));
            }
            else
            {


                if (is_ab == 2)
                    ta1.FillByDorehBazeForAll(ds.ghabz_pardakht, Convert.ToInt32(txt_start_doreh.Text), Convert.ToInt32(txt_end_doreh.Text));

                else
                    ta1.FillByDorehBaze(ds.ghabz_pardakht, Convert.ToInt32(txt_start_doreh.Text), Convert.ToInt32(txt_end_doreh.Text), (is_ab == 1 ? true : false));
            }
            if (ds.ghabz_pardakht.Rows.Count == 0)
            {
                Payam.Show("اطلاعاتی جهت چاپ وجود ندارد");
                return;
            }
            
            DataTable dt = ds.ghabz_pardakht.CopyToDataTable();

            ds.ghabz_pardakht.Columns.Remove("tarikh_vosool");
            ds.ghabz_pardakht.Columns.Add("tarikh_vosool");

            ds.ghabz_pardakht.Columns.Remove("tarikh_sarresid");
            ds.ghabz_pardakht.Columns.Add("tarikh_sarresid");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    ds.ghabz_pardakht.Rows[i]["tarikh_vosool"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["tarikh_vosool"])).ToString("d");

                }
                catch (Exception)
                {

                    ds.ghabz_pardakht.Rows[i]["tarikh_vosool"] = "";

                }
                try
                {
                    ds.ghabz_pardakht.Rows[i]["tarikh_sarresid"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["tarikh_sarresid"])).ToString("d");

                }
                catch (Exception)
                {

                    ds.ghabz_pardakht.Rows[i]["tarikh_sarresid"] = "";

                }
            }

            report1.SetDataSource(ds);
            string tarikh = PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d");
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("dorehname", type);
            report1.SetParameterValue("pstart", PersianDateConverter.ToPersianDate(Convert.ToDateTime(txt_start.SelectedDateTime)).ToString("d"));
            report1.SetParameterValue("pend", PersianDateConverter.ToPersianDate(Convert.ToDateTime(txt_end.SelectedDateTime)).ToString("d"));
            report1.SetParameterValue("tarikh", tarikh);



            new FrmShowReport(report1).ShowDialog();
        }

        private void buttonX2_Click_2(object sender, EventArgs e)
        {
            print_pardakhti("قبوض آب", 1);

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            print_pardakhti("قبوض آب و شارژ", 2);
        }
    }
}