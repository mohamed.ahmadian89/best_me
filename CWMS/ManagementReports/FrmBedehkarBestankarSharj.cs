using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
using CrystalDecisions.Windows.Forms;

namespace CWMS.ManagementReports
{
    public partial class FrmBedehkarBestankarSharj : DevComponents.DotNetBar.Metro.MetroForm
    {
        int ChapDoreh = 0;
        public FrmBedehkarBestankarSharj()
        {
            InitializeComponent();
        }

        private void FrmBedehkarBestankarSharj_Load(object sender, EventArgs e)
        {
            ChapDoreh = 0;
            // TODO: This line of code loads data into the 'reportDataSet2.moshtarek_sharj' table. You can move, or remove it, as needed.
            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh");
            for (int i = 0; i < dt_doreh.Rows.Count; i++)
            {
                cmbDoreh.Items.Add(dt_doreh.Rows[i][0].ToString());
            }
        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            //this.moshtarek_sharjTableAdapter.FillByDoreh(this.reportDataSet2.moshtarek_sharj,Convert.ToInt32(cmbDoreh.SelectedItem));
            CalcSum();
            ChapDoreh = Convert.ToInt32(cmbDoreh.SelectedItem);

        }
        void CalcSum()
        {
            BedehkarBindingSource.Filter = "mande<>0";
            BestankarBindingSource.Filter = "bestankari<>0";
            txtSumBedehkar.Text = reportDataSet2.moshtarek_sharj.Select(p => p.mande).Sum().ToString();
            txtSumBestankar.Text = reportDataSet2.moshtarek_sharj.Select(p => p.bestankari).Sum().ToString();
            lblinfo.Text = reportDataSet2.moshtarek_sharj.Where(p => p.mande == 0 && p.bestankari == 0).Count().ToString() + " ���ј " + " �� ��� ���� ������ ���� ��� ";
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            //this.moshtarek_sharjTableAdapter.Fill(this.reportDataSet2.moshtarek_sharj);
            CalcSum();
            ChapDoreh = 0;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.Sharj_Bedehkaran rpt = new CrysReports.Sharj_Bedehkaran();
            int from = -1, to = -1;
            try
            {
                from = Convert.ToInt32(txtBedFrom.Text);
            }
            catch (Exception)
            {
                from = -1;
            }

            try
            {
                to = Convert.ToInt32(txtBedTo.Text);
            }
            catch (Exception)
            {
                to = -1;
            }
            Classes.clsSharj.sharj_moshtarek_bedehkar(repVUer, rpt, ChapDoreh,from,to);
        }

        private void superTabControlPanel1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.Sharj_Bestankaran rpt = new CrysReports.Sharj_Bestankaran();

            Classes.clsSharj.sharj_moshtarek_bestankar(repVUer, rpt, ChapDoreh);
        }

    }
}