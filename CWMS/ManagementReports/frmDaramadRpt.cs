﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Windows.Forms.DataVisualization.Charting;
using Snippet;
using CWMS.Classes;

namespace CWMS.ManagementReports
{
    public partial class frmDaramadRpt : MyMetroForm
    {
        DataTable ghabzPardakht;
        DataTable abGhabz, sharjGhabz;
        bool byDorehCalculated;

        public frmDaramadRpt()
        {
            InitializeComponent();
        }

        private void cmbAb_Click(object sender, EventArgs e)
        {
            chbAb.Checked = true;
        }

        private void cmbSharj_Click(object sender, EventArgs e)
        {
            chbSharj.Checked = true;
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            DateTime from = DateTime.Now.AddDays(-30);
            DateTime to = DateTime.Now;
            loadData(from, to);
            fill();
        }

        private void loadData(DateTime from, DateTime to)
        {
            ClsMain.ChangeCulture("e");
            string byNahve =
                "select nahve_pardakht, SUM(mablagh) as sum from ghabz_pardakht " +
                "where tarikh_vosool between '" + from + "' and '" + to + "' " +
                "group by nahve_pardakht";
            try
            {
                ghabzPardakht = Classes.ClsMain.GetDataTable(byNahve);
                ClsMain.ChangeCulture("f");
            }
            catch (Exception ex)
            {
                Payam.Show("خطا در خواندن اطلاعات. لطفا با پشتیان هماهنگ نمایید");
                ClsMain.logError(ex);
            }
        }

        private void fill()
        {
            int sum = 0;
            lblChqTotal.Text = lblPOSTotal.Text = lblIntTotal.Text = lblCashTotal.Text = "0";
            foreach (DataRow row in ghabzPardakht.Rows)
            {
                sum += Convert.ToInt32(row["sum"].ToString());
                switch (row["nahve_pardakht"].ToString())
                {
                    case "چک":
                        lblChqTotal.Text = row["sum"].ToString();
                        break;
                    case "تهاتر":
                        lblTahator.Text = row["sum"].ToString();
                        break;
                    case "فیش":
                        lblfishtotal.Text = row["sum"].ToString();
                        break;
                    case "پوز":
                        lblPOSTotal.Text = row["sum"].ToString();
                        break;
                    case "اینترنتی":
                        lblIntTotal.Text = row["sum"].ToString();
                        break;
                    case "واریزنقدی":
                        lblCashTotal.Text = row["sum"].ToString();
                        break;
                    default:
                        break;
                }
            }
            sumPeriodic.Text = sum.ToString(); chart1.Hide(); chart1.Show();
            chart1.DataSource = ghabzPardakht;
            chart1.Series[0].XValueMember = "nahve_pardakht";
            chart1.Series[0].YValueMembers = "sum";
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (chbAb.Checked == false && chbSharj.Checked == false)
                return;
            int sharjDcode = -1;
            int abDcode = -1;
            if (chbAb.Checked)
                try
                {
                    abDcode = Convert.ToInt32(cmbAb.SelectedValue);
                }
                catch (Exception)
                {

                }
            if (chbSharj.Checked)
                try
                {
                    sharjDcode = Convert.ToInt32(cmbSharj.SelectedValue);
                }
                catch (Exception)
                {

                }

            if (!byDorehCalculated)
            {
                try
                {
                    calculateByDoreh();
                    byDorehCalculated = true;
                }
                catch (Exception ex)
                {
                    ClsMain.logError(ex);
                }
            }
            var a = abGhabz.AsDataView();
            a.RowFilter = "dcode =" + abDcode;
            foreach (DataRow row in a.ToTable().Rows)
            {
                if (Convert.ToInt32(row["vaziat"]) == 1)
                {
                    lblMojaz.Text = row["mojTotal"].ToString();
                    lblGhMojaz.Text = row["ghMojTotal"].ToString();
                    lblMaliat.Text = row["malTotal"].ToString();
                    lblJarime.Text = row["jarTatal"].ToString();
                    lblBedehi.Text = row["bedTotal"].ToString();
                    lblSayer.Text = row["sayerTotal"].ToString();
                    lblAboonman.Text = row["abnTotal"].ToString();
                }
                else
                    lblJozyi.Text = row["manTotal"].ToString();
            }

            var b = sharjGhabz.AsDataView();
            b.RowFilter = "dcode =" + sharjDcode;
            foreach (DataRow row in b.ToTable().Rows)
            {
                if (Convert.ToInt32(row["vaziat"]) == 1)
                {
                    lblMaliatSh.Text = row["malTotal"].ToString();
                    lblJarimeSh.Text = row["jarTatal"].ToString();
                    lblBedehiSh.Text = row["bedTotal"].ToString();
                    lblSayerSh.Text = row["sayerTotal"].ToString();
                    lblMablagh.Text = row["mabTotal"].ToString();
                }
                else
                    lblJozyiSh.Text = row["manTotal"].ToString();
            }

            lblTotal.Text =
                (Convert.ToInt32(lblSumAb.Text) + Convert.ToInt32(lblSumSh.Text)).ToString();

            fillByDoreh(abDcode, sharjDcode);
        }

        private void fillByDoreh(int abDcode, int sharjDcode)
        {
            int sum = 0;

            string query =
                "select is_ab, nahve_pardakht, SUM(mablagh) as sum from ghabz_pardakht " +
                "where (dcode = " + abDcode + " and is_ab='true') or (dcode=" + sharjDcode + " and is_ab='false') " +
                "group by nahve_pardakht, is_ab";
            DataTable result = Classes.ClsMain.GetDataTable(query);

            foreach (DataRow row in result.Rows)
            {
                sum += Convert.ToInt32(row["sum"].ToString());
                switch (row["nahve_pardakht"].ToString())
                {
                    case "چک":
                        lblChqTotal.Text = row["sum"].ToString();
                        break;
                    case "پوز":
                        lblPOSTotal.Text = row["sum"].ToString();
                        break;
                    case "اینترنتی":
                        lblIntTotal.Text = row["sum"].ToString();
                        break;
                    case "واریزنقدی":
                        lblCashTotal.Text = row["sum"].ToString();
                        break;
                    default:
                        break;
                }
            }
            sumDoreh.Text = sum.ToString();
        }

        private void calculateByDoreh()
        {
            string byAbDoreh =
                "select	dcode, vaziat, SUM(maliat) as malTotal, SUM(jarimeh) as jarTatal, " +
                "SUM(bedehi) as bedTotal, SUM(sayer) as sayerTotal, " +
                "SUM(masraf_mojaz_hazineh) as mojTotal, SUM(masraf_gheir_mojaz_hazineh) as ghMojTotal," +
                "SUM(aboonmah) as abnTotal, SUM(mande) as manTotal, SUM(mablaghkol) as kolTotal " +
                "from ab_ghabz where vaziat!=0 group by dcode, vaziat";
            abGhabz = Classes.ClsMain.GetDataTable(byAbDoreh);

            string bySharjDoreh =
                "select	dcode, vaziat, SUM(maliat) as malTotal, SUM(jarimeh) as jarTatal," +
                "SUM(bedehi) as bedTotal, SUM(sayer) as sayerTotal, SUM(mablagh) as mabTotal, " +
                "SUM(mande) as manTotal, SUM(mablaghkol) as kolTotal " +
                "from sharj_ghabz where vaziat!=0 group by dcode, vaziat";
            sharjGhabz = Classes.ClsMain.GetDataTable(bySharjDoreh);
        }

        private void btnAdvancedSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime from = (DateTime)dtpFrom.SelectedDateTime;
                DateTime to = (DateTime)dtpTo.SelectedDateTime;
                loadData(from, to);
                fill();
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex);
            }
        }

        private void frmDaramadRpt_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'cWMSDataSet.sharj_doreh' table. You can move, or remove it, as needed.
            this.sharj_dorehTableAdapter.Fill(this.cWMSDataSet.sharj_doreh);
            // TODO: This line of code loads data into the 'abDataset.ab_doreh' table. You can move, or remove it, as needed.
            this.ab_dorehTableAdapter.Fill(this.abDataset.ab_doreh);
            dtpFrom.SelectedDateTime =   DateTime.Now;
            dtpTo.SelectedDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
        }
    }
}