﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;

namespace CWMS.ManagementReports
{
    public partial class frmDaramadSource : MyMetroForm
    {
        int abReportState = 0; // 1=> perDoreh, 2=> Range, 3=> All
        int sharjReportState = 0;

        int abReportFrom = 0;
        int abReportTo = 0;

        int sharjReportFrom = 0;
        int sharjReportTo = 0;

        public frmDaramadSource()
        {
            InitializeComponent();
        }

        private void btnDaramadAb_Click(object sender, EventArgs e)
        {
            if (isAbEmpty())
            {
                return;
            }
            if (Convert.ToInt32(cmbAbFrom.Text) <= Convert.ToInt32(cmbAbTo.Text))
            {
                this.daramadAbTableAdapter.FillPerRange(daramadDataset.DaramadAb,
                    Convert.ToInt32(cmbAbFrom.Text), Convert.ToInt32(cmbAbTo.Text));

                abReportFrom = Convert.ToInt32(cmbAbFrom.Text);
                abReportTo = Convert.ToInt32(cmbAbTo.Text);

                if (abReportFrom == abReportTo)
                    abReportState = 1;
                else
                    abReportState = 2;
            }
        }

        private void btnPerDorehAb_Click(object sender, EventArgs e)
        {
            if (isAbEmpty())
            {
                return;
            }
            this.daramadAbTableAdapter.FillPerDoreh(daramadDataset.DaramadAb, Convert.ToInt32(cmbAbFrom.Text));
            abReportState = 1;
            abReportFrom = Convert.ToInt32(cmbAbFrom.Text);
        }

        private void btnAllAb_Click(object sender, EventArgs e)
        {
            if (isAbEmpty())
            {
                return;
            }
            this.daramadAbTableAdapter.FillAll(daramadDataset.DaramadAb);
            abReportState = 3;
        }

        private bool isAbEmpty()
        {
            if (cmbAbFrom.Items.Count == 0)
            {
                return true;
            }
            return false;
        }

        private bool isSharjEmpty()
        {
            if (cmbSharjFrom.Items.Count == 0)
            {
                return true;
            }
            return false;
        }

        private void btnDaramadSharj_Click(object sender, EventArgs e)
        {
            if (isSharjEmpty())
            {
                return;
            }

            if (Convert.ToInt32(cmbSharjFrom.Text) <= Convert.ToInt32(cmbSharjTo.Text))
            {
                this.daramadSharjTableAdapter.FillPerRange(daramadDataset.DaramadSharj,
                    Convert.ToInt32(cmbSharjFrom.Text), Convert.ToInt32(cmbSharjTo.Text));

                sharjReportFrom = Convert.ToInt32(cmbSharjFrom.Text);
                sharjReportTo = Convert.ToInt32(cmbSharjTo.Text);

                if (sharjReportFrom == sharjReportTo)
                    sharjReportState = 1;
                else
                    sharjReportState = 2;
            }
            else
                Payam.Show("دوره ابتدایی باید کوچکتر مساوی با دوره انتهایی باشد");
        }

        private void btnPerDorehSharj_Click(object sender, EventArgs e)
        {
            if (isSharjEmpty())
            {
                return;
            }
            this.daramadSharjTableAdapter.FillPerDoreh(daramadDataset.DaramadSharj, Convert.ToInt32(cmbSharjFrom.Text));
            sharjReportState = 1;
            sharjReportFrom = Convert.ToInt32(cmbSharjFrom.Text);
        }

        private void btnAllSharj_Click(object sender, EventArgs e)
        {
            if (isSharjEmpty())
            {
                return;
            }
            this.daramadSharjTableAdapter.FillAll(daramadDataset.DaramadSharj);
            sharjReportState = 3;
        }

        private void frmDaramadSource_Load(object sender, EventArgs e)
        {
            DataTable dt_sharj = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh");
            DataTable dt_ab = Classes.ClsMain.GetDataTable("select dcode from ab_doreh");

            for (int i = 0; i < dt_sharj.Rows.Count; i++)
            {
                cmbSharjFrom.Items.Add(dt_sharj.Rows[i][0].ToString());
                cmbSharjTo.Items.Add(dt_sharj.Rows[i][0].ToString());
            }

            for (int i = 0; i < dt_ab.Rows.Count; i++)
            {
                cmbAbFrom.Items.Add(dt_ab.Rows[i][0].ToString());
                cmbAbTo.Items.Add(dt_ab.Rows[i][0].ToString());
            }
            if (cmbAbTo.Items.Count != 0)
                cmbAbFrom.SelectedIndex = cmbAbTo.SelectedIndex = 0;

            if (cmbSharjFrom.Items.Count != 0)
                cmbSharjFrom.SelectedIndex = cmbSharjTo.SelectedIndex = 0;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            string titleSharj, titleAb;

            switch (sharjReportState)
            {
                case 1:
                    titleSharj = "دوره " + sharjReportFrom;
                    break;
                case 2:
                    titleSharj = "دوره " + sharjReportFrom + " تا دوره " + sharjReportTo;
                    break;
                case 3:
                    titleSharj = "تمام دوره های شارژ";
                    break;
                default:
                    Payam.Show("گزارش درآمد شارژ گرفته نشده است. لطفا ابتدا گزارش گرفته سپس دکمه چاپ را بزنید");
                    return;
            }

            switch (abReportState)
            {
                case 1:
                    titleAb = "دوره " + abReportFrom;
                    break;
                case 2:
                    titleAb = "دوره " + abReportFrom + " تا دوره " + abReportTo;
                    break;
                case 3:
                    titleAb = "تمام دوره های آب";
                    break;
                default:
                    Payam.Show("گزارش درآمد آب گرفته نشده است. لطفا ابتدا گزارش گرفته سپس دکمه چاپ را بزنید");
                    return;
            }
            CrystalReportViewer viewer = new CrystalReportViewer();
            ManagementReports.DaramadDatasetTableAdapters.settingTableAdapter taSetting = new DaramadDatasetTableAdapters.settingTableAdapter();
            taSetting.FillSetting(daramadDataset.setting);

            CrysReports.rptDaramadSource rpt = new CrysReports.rptDaramadSource();

            rpt.SetDataSource(daramadDataset);

            rpt.SetParameterValue("titleSharj", titleSharj);
            rpt.SetParameterValue("titleAb", titleAb);

            FarsiLibrary.Utils.PersianDate today = new FarsiLibrary.Utils.PersianDate(DateTime.Now);
            rpt.SetParameterValue("tarikh", today.ToString("d"));

            new FrmShowReport(rpt).ShowDialog();
        }
    }
}