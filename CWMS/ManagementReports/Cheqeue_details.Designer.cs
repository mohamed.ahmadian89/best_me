﻿namespace CWMS.ManagementReports
{
    partial class Cheqeue_details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ttsarresid = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtTozihat = new System.Windows.Forms.TextBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.ttVosool = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lbl_coname = new DevComponents.DotNetBar.LabelX();
            this.txtgharardad = new System.Windows.Forms.TextBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtbank = new System.Windows.Forms.TextBox();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtmablagh = new CWMS.MoneyTextBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txttarikh_sabt = new FarsiLibrary.Win.Controls.FADatePicker();
            this.btnsave = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.ttsarresid);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.txtTozihat);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.groupBox1);
            this.groupPanel1.Controls.Add(this.lbl_coname);
            this.groupPanel1.Controls.Add(this.txtgharardad);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txtbank);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.txtmablagh);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.txttarikh_sabt);
            this.groupPanel1.Controls.Add(this.btnsave);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(15, 15);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1220, 415);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "اطلاعات چک";
            // 
            // ttsarresid
            // 
            this.ttsarresid.Location = new System.Drawing.Point(548, 18);
            this.ttsarresid.Name = "ttsarresid";
            this.ttsarresid.Readonly = true;
            this.ttsarresid.Size = new System.Drawing.Size(150, 26);
            this.ttsarresid.TabIndex = 1;
            this.ttsarresid.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX8
            // 
            this.labelX8.AutoSize = true;
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(707, 16);
            this.labelX8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(142, 31);
            this.labelX8.TabIndex = 51;
            this.labelX8.Text = "تاریخ سررسید  چک :";
            // 
            // txtTozihat
            // 
            this.txtTozihat.BackColor = System.Drawing.Color.White;
            this.txtTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtTozihat.Location = new System.Drawing.Point(93, 262);
            this.txtTozihat.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTozihat.Multiline = true;
            this.txtTozihat.Name = "txtTozihat";
            this.txtTozihat.Size = new System.Drawing.Size(1007, 64);
            this.txtTozihat.TabIndex = 6;
            this.txtTozihat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTozihat_KeyDown);
            // 
            // labelX7
            // 
            this.labelX7.AutoSize = true;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(1122, 265);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(74, 31);
            this.labelX7.TabIndex = 49;
            this.labelX7.Text = "توضیحات :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.labelX6);
            this.groupBox1.Controls.Add(this.ttVosool);
            this.groupBox1.Controls.Add(this.labelX2);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(467, 133);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(721, 108);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "زمان وصول چک";
            // 
            // labelX6
            // 
            this.labelX6.AutoSize = true;
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(297, 47);
            this.labelX6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(126, 31);
            this.labelX6.TabIndex = 51;
            this.labelX6.Text = "وصول شده است ";
            // 
            // ttVosool
            // 
            this.ttVosool.Location = new System.Drawing.Point(428, 49);
            this.ttVosool.Name = "ttVosool";
            this.ttVosool.Size = new System.Drawing.Size(150, 26);
            this.ttVosool.TabIndex = 5;
            this.ttVosool.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.ttVosool.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ttVosool_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(584, 47);
            this.labelX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(123, 31);
            this.labelX2.TabIndex = 49;
            this.labelX2.Text = "این چک در تاریخ :";
            // 
            // lbl_coname
            // 
            this.lbl_coname.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_coname.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_coname.ForeColor = System.Drawing.Color.Black;
            this.lbl_coname.Location = new System.Drawing.Point(522, 85);
            this.lbl_coname.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbl_coname.Name = "lbl_coname";
            this.lbl_coname.Size = new System.Drawing.Size(406, 30);
            this.lbl_coname.TabIndex = 47;
            this.lbl_coname.Text = " ";
            // 
            // txtgharardad
            // 
            this.txtgharardad.BackColor = System.Drawing.Color.White;
            this.txtgharardad.ForeColor = System.Drawing.Color.Black;
            this.txtgharardad.Location = new System.Drawing.Point(938, 80);
            this.txtgharardad.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtgharardad.Name = "txtgharardad";
            this.txtgharardad.Size = new System.Drawing.Size(131, 36);
            this.txtgharardad.TabIndex = 3;
            this.txtgharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgharardad_KeyDown);
            this.txtgharardad.Leave += new System.EventHandler(this.textBox2_Leave);
            // 
            // labelX5
            // 
            this.labelX5.AutoSize = true;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(1074, 80);
            this.labelX5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(126, 31);
            this.labelX5.TabIndex = 45;
            this.labelX5.Text = "متعلق به قرارداد :";
            // 
            // txtbank
            // 
            this.txtbank.BackColor = System.Drawing.Color.White;
            this.txtbank.ForeColor = System.Drawing.Color.Black;
            this.txtbank.Location = new System.Drawing.Point(111, 76);
            this.txtbank.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtbank.Name = "txtbank";
            this.txtbank.Size = new System.Drawing.Size(264, 36);
            this.txtbank.TabIndex = 4;
            this.txtbank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbank_KeyDown);
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(397, 78);
            this.labelX4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(42, 31);
            this.labelX4.TabIndex = 43;
            this.labelX4.Text = "بانک :";
            // 
            // txtmablagh
            // 
            this.txtmablagh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtmablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtmablagh.Border.Class = "TextBoxBorder";
            this.txtmablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtmablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtmablagh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtmablagh.ForeColor = System.Drawing.Color.Black;
            this.txtmablagh.Location = new System.Drawing.Point(131, 16);
            this.txtmablagh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtmablagh.Name = "txtmablagh";
            this.txtmablagh.PreventEnterBeep = true;
            this.txtmablagh.Size = new System.Drawing.Size(243, 33);
            this.txtmablagh.TabIndex = 2;
            this.txtmablagh.Text = "0";
            this.txtmablagh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtmablagh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmablagh_KeyDown);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(380, 16);
            this.labelX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(70, 31);
            this.labelX3.TabIndex = 22;
            this.labelX3.Text = "مبلغ چک :";
            // 
            // txttarikh_sabt
            // 
            this.txttarikh_sabt.Location = new System.Drawing.Point(933, 18);
            this.txttarikh_sabt.Name = "txttarikh_sabt";
            this.txttarikh_sabt.Readonly = true;
            this.txttarikh_sabt.Size = new System.Drawing.Size(150, 26);
            this.txttarikh_sabt.TabIndex = 0;
            this.txttarikh_sabt.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.txttarikh_sabt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttarikh_sabt_KeyDown);
            // 
            // btnsave
            // 
            this.btnsave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnsave.Location = new System.Drawing.Point(93, 334);
            this.btnsave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnsave.Name = "btnsave";
            this.btnsave.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnsave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnsave.Size = new System.Drawing.Size(308, 31);
            this.btnsave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnsave.Symbol = "";
            this.btnsave.SymbolColor = System.Drawing.Color.Green;
            this.btnsave.SymbolSize = 12F;
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "ثبت تغییرات مربوط به چک";
            this.btnsave.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(1092, 16);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(107, 31);
            this.labelX1.TabIndex = 6;
            this.labelX1.Text = "تاریخ ثبت چک :";
            // 
            // Cheqeue_details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 454);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 11F);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Cheqeue_details";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Cheqeue_details_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btnsave;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private FarsiLibrary.Win.Controls.FADatePicker txttarikh_sabt;
        private DevComponents.DotNetBar.LabelX labelX4;
        private MoneyTextBox txtmablagh;
        private DevComponents.DotNetBar.LabelX lbl_coname;
        private System.Windows.Forms.TextBox txtgharardad;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.TextBox txtbank;
        private System.Windows.Forms.GroupBox groupBox1;
        private FarsiLibrary.Win.Controls.FADatePicker ttVosool;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TextBox txtTozihat;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private FarsiLibrary.Win.Controls.FADatePicker ttsarresid;
        private DevComponents.DotNetBar.LabelX labelX8;
    
    }
}