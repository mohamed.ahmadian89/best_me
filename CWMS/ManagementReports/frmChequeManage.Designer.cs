﻿namespace CWMS.ManagementReports
{
    partial class frmChequeManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpChequeList = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.radAll = new System.Windows.Forms.RadioButton();
            this.radNonRecieved = new System.Windows.Forms.RadioButton();
            this.radRecieved = new System.Windows.Forms.RadioButton();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.btnAdvancedSearch = new DevComponents.DotNetBar.ButtonX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dtpTo = new FarsiLibrary.Win.Controls.FADatePicker();
            this.dtpFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.btnAllDates = new DevComponents.DotNetBar.ButtonX();
            this.dataGridViewX3 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn27 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn28 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chequesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chequesDataSet = new CWMS.ManagementReports.ChequesDataSet();
            this.tabSearch = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dataGridViewX2 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn16 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn17 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabCurrentYear = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn5 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn6 = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabNext30Days = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgvToday = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhsabtDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.tarikhsarresidDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.tarikhvosoolDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.bankDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabToday = new DevComponents.DotNetBar.SuperTabItem();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtGharardad = new DevComponents.Editors.IntegerInput();
            this.btnAll = new DevComponents.DotNetBar.ButtonX();
            this.btnFind = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.chequesTableAdapter = new CWMS.ManagementReports.ChequesDataSetTableAdapters.ChequesTableAdapter();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.grpChequeList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chequesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chequesDataSet)).BeginInit();
            this.superTabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX2)).BeginInit();
            this.superTabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvToday)).BeginInit();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGharardad)).BeginInit();
            this.SuspendLayout();
            // 
            // grpChequeList
            // 
            this.grpChequeList.BackColor = System.Drawing.Color.White;
            this.grpChequeList.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpChequeList.Controls.Add(this.radAll);
            this.grpChequeList.Controls.Add(this.radNonRecieved);
            this.grpChequeList.Controls.Add(this.radRecieved);
            this.grpChequeList.Controls.Add(this.superTabControl1);
            this.grpChequeList.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpChequeList.Location = new System.Drawing.Point(12, 99);
            this.grpChequeList.Name = "grpChequeList";
            this.grpChequeList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpChequeList.Size = new System.Drawing.Size(976, 533);
            // 
            // 
            // 
            this.grpChequeList.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpChequeList.Style.BackColorGradientAngle = 90;
            this.grpChequeList.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpChequeList.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpChequeList.Style.BorderBottomWidth = 1;
            this.grpChequeList.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpChequeList.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpChequeList.Style.BorderLeftWidth = 1;
            this.grpChequeList.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpChequeList.Style.BorderRightWidth = 1;
            this.grpChequeList.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpChequeList.Style.BorderTopWidth = 1;
            this.grpChequeList.Style.CornerDiameter = 4;
            this.grpChequeList.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpChequeList.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpChequeList.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpChequeList.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpChequeList.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpChequeList.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpChequeList.TabIndex = 4;
            this.grpChequeList.Text = "لیست چک ها";
            // 
            // radAll
            // 
            this.radAll.AutoSize = true;
            this.radAll.BackColor = System.Drawing.Color.White;
            this.radAll.ForeColor = System.Drawing.Color.Black;
            this.radAll.Location = new System.Drawing.Point(6, 7);
            this.radAll.Name = "radAll";
            this.radAll.Size = new System.Drawing.Size(52, 25);
            this.radAll.TabIndex = 19;
            this.radAll.TabStop = true;
            this.radAll.Text = "همه";
            this.radAll.UseVisualStyleBackColor = false;
            this.radAll.Click += new System.EventHandler(this.radAll_CheckedChanged);
            // 
            // radNonRecieved
            // 
            this.radNonRecieved.AutoSize = true;
            this.radNonRecieved.BackColor = System.Drawing.Color.White;
            this.radNonRecieved.ForeColor = System.Drawing.Color.Black;
            this.radNonRecieved.Location = new System.Drawing.Point(64, 7);
            this.radNonRecieved.Name = "radNonRecieved";
            this.radNonRecieved.Size = new System.Drawing.Size(95, 25);
            this.radNonRecieved.TabIndex = 18;
            this.radNonRecieved.TabStop = true;
            this.radNonRecieved.Text = "وصول نشده";
            this.radNonRecieved.UseVisualStyleBackColor = false;
            this.radNonRecieved.Click += new System.EventHandler(this.radNonRecieved_CheckedChanged);
            // 
            // radRecieved
            // 
            this.radRecieved.AutoSize = true;
            this.radRecieved.BackColor = System.Drawing.Color.White;
            this.radRecieved.ForeColor = System.Drawing.Color.Black;
            this.radRecieved.Location = new System.Drawing.Point(165, 7);
            this.radRecieved.Name = "radRecieved";
            this.radRecieved.Size = new System.Drawing.Size(90, 25);
            this.radRecieved.TabIndex = 17;
            this.radRecieved.TabStop = true;
            this.radRecieved.Text = "وصول شده";
            this.radRecieved.UseVisualStyleBackColor = false;
            this.radRecieved.Click += new System.EventHandler(this.radRecieved_CheckedChanged);
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(3, 3);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(964, 497);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 14;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.tabToday,
            this.tabNext30Days,
            this.tabCurrentYear,
            this.tabSearch});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.btnAdvancedSearch);
            this.superTabControlPanel4.Controls.Add(this.labelX3);
            this.superTabControlPanel4.Controls.Add(this.labelX2);
            this.superTabControlPanel4.Controls.Add(this.dtpTo);
            this.superTabControlPanel4.Controls.Add(this.dtpFrom);
            this.superTabControlPanel4.Controls.Add(this.btnAllDates);
            this.superTabControlPanel4.Controls.Add(this.dataGridViewX3);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 34);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(964, 463);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.tabSearch;
            // 
            // btnAdvancedSearch
            // 
            this.btnAdvancedSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdvancedSearch.BackColor = System.Drawing.Color.Transparent;
            this.btnAdvancedSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdvancedSearch.Location = new System.Drawing.Point(486, 6);
            this.btnAdvancedSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdvancedSearch.Name = "btnAdvancedSearch";
            this.btnAdvancedSearch.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdvancedSearch.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAdvancedSearch.Size = new System.Drawing.Size(39, 24);
            this.btnAdvancedSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAdvancedSearch.Symbol = "";
            this.btnAdvancedSearch.SymbolColor = System.Drawing.Color.Green;
            this.btnAdvancedSearch.SymbolSize = 12F;
            this.btnAdvancedSearch.TabIndex = 22;
            this.btnAdvancedSearch.Click += new System.EventHandler(this.btnAdvancedSearch_Click);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(839, 6);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(117, 24);
            this.labelX3.TabIndex = 21;
            this.labelX3.Text = "سررسید چک از تاریخ :";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(657, 6);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(46, 24);
            this.labelX2.TabIndex = 20;
            this.labelX2.Text = "تا تاریخ :";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(531, 8);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(120, 20);
            this.dtpTo.TabIndex = 19;
            this.dtpTo.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(713, 8);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(120, 20);
            this.dtpFrom.TabIndex = 18;
            this.dtpFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // btnAllDates
            // 
            this.btnAllDates.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAllDates.BackColor = System.Drawing.Color.Transparent;
            this.btnAllDates.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAllDates.Location = new System.Drawing.Point(5, 6);
            this.btnAllDates.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAllDates.Name = "btnAllDates";
            this.btnAllDates.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAllDates.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAllDates.Size = new System.Drawing.Size(247, 24);
            this.btnAllDates.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAllDates.Symbol = "";
            this.btnAllDates.SymbolColor = System.Drawing.Color.Green;
            this.btnAllDates.SymbolSize = 12F;
            this.btnAllDates.TabIndex = 17;
            this.btnAllDates.Text = "نمایش چک های همه مشترکین";
            this.btnAllDates.Click += new System.EventHandler(this.btnAllDates_Click);
            // 
            // dataGridViewX3
            // 
            this.dataGridViewX3.AllowUserToAddRows = false;
            this.dataGridViewX3.AllowUserToDeleteRows = false;
            this.dataGridViewX3.AllowUserToResizeRows = false;
            this.dataGridViewX3.AutoGenerateColumns = false;
            this.dataGridViewX3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewX3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33});
            this.dataGridViewX3.DataSource = this.chequesBindingSource;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX3.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewX3.EnableHeadersVisualStyles = false;
            this.dataGridViewX3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX3.Location = new System.Drawing.Point(3, 35);
            this.dataGridViewX3.Name = "dataGridViewX3";
            this.dataGridViewX3.ReadOnly = true;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX3.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewX3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX3.Size = new System.Drawing.Size(958, 423);
            this.dataGridViewX3.TabIndex = 16;
            this.dataGridViewX3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX3_CellContentClick);
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn23.HeaderText = "شناسه";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "co_name";
            this.dataGridViewTextBoxColumn24.HeaderText = "نام شرکت";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "fish";
            this.dataGridViewTextBoxColumn25.HeaderText = "شماره چک";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn26.DataPropertyName = "tarikh_sabt";
            this.dataGridViewTextBoxColumn26.HeaderText = "تاریخ ثبت";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn26.Width = 87;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn27.DataPropertyName = "tarikh_sarresid";
            this.dataGridViewTextBoxColumn27.HeaderText = "تاریخ سررسید";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn27.Width = 110;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn28.DataPropertyName = "tarikh_vosool";
            this.dataGridViewTextBoxColumn28.HeaderText = "تاریخ وصول";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn28.Width = 97;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "bank";
            this.dataGridViewTextBoxColumn29.HeaderText = "بانک";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "mablagh";
            this.dataGridViewTextBoxColumn30.HeaderText = "مبلغ";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "gcode";
            this.dataGridViewTextBoxColumn31.HeaderText = "کد قبض";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "type";
            this.dataGridViewTextBoxColumn32.HeaderText = "قبض";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "tozihat";
            this.dataGridViewTextBoxColumn33.HeaderText = "توضیحات";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            // 
            // chequesBindingSource
            // 
            this.chequesBindingSource.DataMember = "Cheques";
            this.chequesBindingSource.DataSource = this.chequesDataSet;
            // 
            // chequesDataSet
            // 
            this.chequesDataSet.DataSetName = "ChequesDataSet";
            this.chequesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabSearch
            // 
            this.tabSearch.AttachedControl = this.superTabControlPanel4;
            this.tabSearch.GlobalItem = false;
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Text = "جستجوی پیشرفته";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.dataGridViewX2);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(964, 497);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.tabCurrentYear;
            // 
            // dataGridViewX2
            // 
            this.dataGridViewX2.AllowUserToAddRows = false;
            this.dataGridViewX2.AllowUserToDeleteRows = false;
            this.dataGridViewX2.AllowUserToResizeRows = false;
            this.dataGridViewX2.AutoGenerateColumns = false;
            this.dataGridViewX2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewX2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22});
            this.dataGridViewX2.DataSource = this.chequesBindingSource;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX2.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewX2.EnableHeadersVisualStyles = false;
            this.dataGridViewX2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX2.Location = new System.Drawing.Point(3, 5);
            this.dataGridViewX2.Name = "dataGridViewX2";
            this.dataGridViewX2.ReadOnly = true;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX2.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewX2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX2.Size = new System.Drawing.Size(958, 453);
            this.dataGridViewX2.TabIndex = 16;
            this.dataGridViewX2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX2_CellContentClick);
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn12.HeaderText = "شناسه";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "co_name";
            this.dataGridViewTextBoxColumn13.HeaderText = "نام شرکت";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "fish";
            this.dataGridViewTextBoxColumn14.HeaderText = "شماره چک";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "tarikh_sabt";
            this.dataGridViewTextBoxColumn15.HeaderText = "تاریخ ثبت";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn15.Width = 87;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "tarikh_sarresid";
            this.dataGridViewTextBoxColumn16.HeaderText = "تاریخ سررسید";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn16.Width = 110;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn17.DataPropertyName = "tarikh_vosool";
            this.dataGridViewTextBoxColumn17.HeaderText = "تاریخ وصول";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn17.Width = 97;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "bank";
            this.dataGridViewTextBoxColumn18.HeaderText = "بانک";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "mablagh";
            this.dataGridViewTextBoxColumn19.HeaderText = "مبلغ";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "gcode";
            this.dataGridViewTextBoxColumn20.HeaderText = "کد قبض";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "type";
            this.dataGridViewTextBoxColumn21.HeaderText = "قبض";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "tozihat";
            this.dataGridViewTextBoxColumn22.HeaderText = "توضیحات";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // tabCurrentYear
            // 
            this.tabCurrentYear.AttachedControl = this.superTabControlPanel3;
            this.tabCurrentYear.GlobalItem = false;
            this.tabCurrentYear.Name = "tabCurrentYear";
            this.tabCurrentYear.Text = "تا انتهای سال جاری";
            this.tabCurrentYear.Click += new System.EventHandler(this.tabCurrentYear_GotFocus);
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.dataGridViewX1);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(964, 497);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.tabNext30Days;
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToAddRows = false;
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.AllowUserToResizeRows = false;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.dataGridViewX1.DataSource = this.chequesBindingSource;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(3, 5);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(958, 453);
            this.dataGridViewX1.TabIndex = 16;
            this.dataGridViewX1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "شناسه";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "co_name";
            this.dataGridViewTextBoxColumn2.HeaderText = "نام شرکت";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "fish";
            this.dataGridViewTextBoxColumn3.HeaderText = "شماره چک";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "tarikh_sabt";
            this.dataGridViewTextBoxColumn4.HeaderText = "تاریخ ثبت";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn4.Width = 87;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "tarikh_sarresid";
            this.dataGridViewTextBoxColumn5.HeaderText = "تاریخ سررسید";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn5.Width = 110;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "tarikh_vosool";
            this.dataGridViewTextBoxColumn6.HeaderText = "تاریخ وصول";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn6.Width = 97;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "bank";
            this.dataGridViewTextBoxColumn7.HeaderText = "بانک";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "mablagh";
            this.dataGridViewTextBoxColumn8.HeaderText = "مبلغ";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "gcode";
            this.dataGridViewTextBoxColumn9.HeaderText = "کد قبض";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "type";
            this.dataGridViewTextBoxColumn10.HeaderText = "قبض";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "tozihat";
            this.dataGridViewTextBoxColumn11.HeaderText = "توضیحات";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // tabNext30Days
            // 
            this.tabNext30Days.AttachedControl = this.superTabControlPanel2;
            this.tabNext30Days.GlobalItem = false;
            this.tabNext30Days.Name = "tabNext30Days";
            this.tabNext30Days.Text = "سی روز آینده";
            this.tabNext30Days.Click += new System.EventHandler(this.tabNext30Days_GotFocus);
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.dgvToday);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 34);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(964, 463);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.tabToday;
            // 
            // dgvToday
            // 
            this.dgvToday.AllowUserToAddRows = false;
            this.dgvToday.AllowUserToDeleteRows = false;
            this.dgvToday.AllowUserToResizeRows = false;
            this.dgvToday.AutoGenerateColumns = false;
            this.dgvToday.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvToday.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvToday.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgvToday.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.fishDataGridViewTextBoxColumn,
            this.tarikhsabtDataGridViewTextBoxColumn,
            this.tarikhsarresidDataGridViewTextBoxColumn,
            this.tarikhvosoolDataGridViewTextBoxColumn,
            this.bankDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn,
            this.gcodeDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.tozihatDataGridViewTextBoxColumn});
            this.dgvToday.DataSource = this.chequesBindingSource;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvToday.DefaultCellStyle = dataGridViewCellStyle29;
            this.dgvToday.EnableHeadersVisualStyles = false;
            this.dgvToday.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvToday.Location = new System.Drawing.Point(3, 7);
            this.dgvToday.Name = "dgvToday";
            this.dgvToday.ReadOnly = true;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvToday.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvToday.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvToday.Size = new System.Drawing.Size(958, 453);
            this.dgvToday.TabIndex = 15;
            this.dgvToday.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvToday_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "شناسه";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام شرکت";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fishDataGridViewTextBoxColumn
            // 
            this.fishDataGridViewTextBoxColumn.DataPropertyName = "fish";
            this.fishDataGridViewTextBoxColumn.HeaderText = "شماره چک";
            this.fishDataGridViewTextBoxColumn.Name = "fishDataGridViewTextBoxColumn";
            this.fishDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhsabtDataGridViewTextBoxColumn
            // 
            this.tarikhsabtDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tarikhsabtDataGridViewTextBoxColumn.DataPropertyName = "tarikh_sabt";
            this.tarikhsabtDataGridViewTextBoxColumn.HeaderText = "تاریخ ثبت";
            this.tarikhsabtDataGridViewTextBoxColumn.Name = "tarikhsabtDataGridViewTextBoxColumn";
            this.tarikhsabtDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhsabtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhsabtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhsabtDataGridViewTextBoxColumn.Width = 87;
            // 
            // tarikhsarresidDataGridViewTextBoxColumn
            // 
            this.tarikhsarresidDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tarikhsarresidDataGridViewTextBoxColumn.DataPropertyName = "tarikh_sarresid";
            this.tarikhsarresidDataGridViewTextBoxColumn.HeaderText = "تاریخ سررسید";
            this.tarikhsarresidDataGridViewTextBoxColumn.Name = "tarikhsarresidDataGridViewTextBoxColumn";
            this.tarikhsarresidDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhsarresidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhsarresidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhsarresidDataGridViewTextBoxColumn.Width = 110;
            // 
            // tarikhvosoolDataGridViewTextBoxColumn
            // 
            this.tarikhvosoolDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tarikhvosoolDataGridViewTextBoxColumn.DataPropertyName = "tarikh_vosool";
            this.tarikhvosoolDataGridViewTextBoxColumn.HeaderText = "تاریخ وصول";
            this.tarikhvosoolDataGridViewTextBoxColumn.Name = "tarikhvosoolDataGridViewTextBoxColumn";
            this.tarikhvosoolDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhvosoolDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhvosoolDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhvosoolDataGridViewTextBoxColumn.Width = 97;
            // 
            // bankDataGridViewTextBoxColumn
            // 
            this.bankDataGridViewTextBoxColumn.DataPropertyName = "bank";
            this.bankDataGridViewTextBoxColumn.HeaderText = "بانک";
            this.bankDataGridViewTextBoxColumn.Name = "bankDataGridViewTextBoxColumn";
            this.bankDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "مبلغ";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کد قبض";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "قبض";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tozihatDataGridViewTextBoxColumn
            // 
            this.tozihatDataGridViewTextBoxColumn.DataPropertyName = "tozihat";
            this.tozihatDataGridViewTextBoxColumn.HeaderText = "توضیحات";
            this.tozihatDataGridViewTextBoxColumn.Name = "tozihatDataGridViewTextBoxColumn";
            this.tozihatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tabToday
            // 
            this.tabToday.AttachedControl = this.superTabControlPanel1;
            this.tabToday.GlobalItem = false;
            this.tabToday.Name = "tabToday";
            this.tabToday.Text = "امروز";
            this.tabToday.Click += new System.EventHandler(this.tabToday_GotFocus);
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txtGharardad);
            this.groupPanel1.Controls.Add(this.btnAll);
            this.groupPanel1.Controls.Add(this.btnFind);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(976, 81);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 3;
            this.groupPanel1.Text = "انتخاب مشترک ";
            // 
            // txtGharardad
            // 
            this.txtGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGharardad.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtGharardad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGharardad.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtGharardad.Location = new System.Drawing.Point(779, 11);
            this.txtGharardad.MaxValue = 9999;
            this.txtGharardad.MinValue = 1;
            this.txtGharardad.Name = "txtGharardad";
            this.txtGharardad.Size = new System.Drawing.Size(114, 29);
            this.txtGharardad.TabIndex = 14;
            this.txtGharardad.Value = 1;
            // 
            // btnAll
            // 
            this.btnAll.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAll.BackColor = System.Drawing.Color.Transparent;
            this.btnAll.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAll.Location = new System.Drawing.Point(6, 11);
            this.btnAll.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAll.Name = "btnAll";
            this.btnAll.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAll.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnAll.Size = new System.Drawing.Size(247, 24);
            this.btnAll.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAll.Symbol = "";
            this.btnAll.SymbolColor = System.Drawing.Color.Green;
            this.btnAll.SymbolSize = 12F;
            this.btnAll.TabIndex = 13;
            this.btnAll.Text = "نمایش چک های همه مشترکین";
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnFind
            // 
            this.btnFind.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFind.BackColor = System.Drawing.Color.Transparent;
            this.btnFind.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFind.Location = new System.Drawing.Point(734, 13);
            this.btnFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFind.Name = "btnFind";
            this.btnFind.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnFind.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnFind.Size = new System.Drawing.Size(39, 24);
            this.btnFind.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFind.Symbol = "";
            this.btnFind.SymbolColor = System.Drawing.Color.Green;
            this.btnFind.SymbolSize = 12F;
            this.btnFind.TabIndex = 1;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(899, 13);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(65, 24);
            this.labelX1.TabIndex = 6;
            this.labelX1.Text = "کد قرارداد :";
            // 
            // chequesTableAdapter
            // 
            this.chequesTableAdapter.ClearBeforeFill = true;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(204, 638);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(525, 31);
            this.labelX4.TabIndex = 23;
            this.labelX4.Text = "جهت نمایش جزئیات چک انتخاب شده ، بر روی آن کلیک نمایید";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(675, 643);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(25, 23);
            this.labelX19.Symbol = "";
            this.labelX19.SymbolColor = System.Drawing.Color.Orange;
            this.labelX19.TabIndex = 75;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(243, 646);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(25, 23);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.Orange;
            this.labelX5.TabIndex = 76;
            // 
            // frmChequeManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 681);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX19);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.grpChequeList);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChequeManage";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmChequeManage_Load);
            this.grpChequeList.ResumeLayout(false);
            this.grpChequeList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel4.ResumeLayout(false);
            this.superTabControlPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chequesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chequesDataSet)).EndInit();
            this.superTabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX2)).EndInit();
            this.superTabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvToday)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGharardad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpChequeList;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvToday;
        private DevComponents.DotNetBar.SuperTabItem tabToday;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem tabNext30Days;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem tabCurrentYear;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.SuperTabItem tabSearch;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btnAll;
        private DevComponents.DotNetBar.ButtonX btnFind;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.RadioButton radAll;
        private System.Windows.Forms.RadioButton radNonRecieved;
        private System.Windows.Forms.RadioButton radRecieved;
        private ChequesDataSet chequesDataSet;
        private System.Windows.Forms.BindingSource chequesBindingSource;
        private ChequesDataSetTableAdapters.ChequesTableAdapter chequesTableAdapter;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX3;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.ButtonX btnAdvancedSearch;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private FarsiLibrary.Win.Controls.FADatePicker dtpTo;
        private FarsiLibrary.Win.Controls.FADatePicker dtpFrom;
        private DevComponents.DotNetBar.ButtonX btnAllDates;
        private DevComponents.Editors.IntegerInput txtGharardad;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn26;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn27;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn15;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn16;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn4;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn5;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fishDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhsabtDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhsarresidDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhvosoolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bankDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihatDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX labelX5;
    }
}