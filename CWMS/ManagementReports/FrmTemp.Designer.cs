﻿namespace CWMS.ManagementReports
{
    partial class FrmTemp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtTozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.cmbHesab = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.groupPanel6 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.superTabControl2 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgv_ghobooz = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.superTabItem5 = new DevComponents.DotNetBar.SuperTabItem();
            this.groupPanel7 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.lbSumMablaghKol = new DevComponents.DotNetBar.LabelX();
            this.lbSumBedehi = new DevComponents.DotNetBar.LabelX();
            this.lbSumSayer = new DevComponents.DotNetBar.LabelX();
            this.lbSumMaliat = new DevComponents.DotNetBar.LabelX();
            this.lbSumMabalegh = new DevComponents.DotNetBar.LabelX();
            this.groupPanel9 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.Grpfindgharardad = new System.Windows.Forms.Panel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.textBoxX1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.rdGharardad = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.checkBoxX8 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.checkBoxX9 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.checkBoxX10 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupPanel6.SuspendLayout();
            this.groupPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).BeginInit();
            this.superTabControl2.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).BeginInit();
            this.groupPanel7.SuspendLayout();
            this.groupPanel9.SuspendLayout();
            this.Grpfindgharardad.SuspendLayout();
            this.SuspendLayout();
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(26, 16);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 2;
            this.superTabControl1.Size = new System.Drawing.Size(911, 197);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 69;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1,
            this.superTabItem2,
            this.superTabItem3});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.groupPanel2);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(911, 168);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.superTabItem3;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txtTozihat);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(200, 18);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(599, 127);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 65;
            this.groupPanel2.Text = "جستجوی درآمدها بر اساس توضیحات درج شده در قبض درامدی";
            // 
            // txtTozihat
            // 
            this.txtTozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTozihat.Border.Class = "TextBoxBorder";
            this.txtTozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txtTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtTozihat.Location = new System.Drawing.Point(29, 14);
            this.txtTozihat.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTozihat.Name = "txtTozihat";
            this.txtTozihat.PreventEnterBeep = true;
            this.txtTozihat.Size = new System.Drawing.Size(486, 24);
            this.txtTozihat.TabIndex = 41;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(19, 55);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(142, 23);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 40;
            this.buttonX3.Text = "نمایش اطلاعات درآمدی";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(513, 10);
            this.labelX2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(61, 39);
            this.labelX2.TabIndex = 39;
            this.labelX2.Text = "توضیحات :";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel3;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "براساس دوره مربوطه";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.groupPanel4);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(911, 168);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.txt_end);
            this.groupPanel4.Controls.Add(this.labelX5);
            this.groupPanel4.Controls.Add(this.txt_start);
            this.groupPanel4.Controls.Add(this.labelX9);
            this.groupPanel4.Controls.Add(this.buttonX2);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(403, 18);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(393, 136);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 66;
            this.groupPanel4.Text = "بر اساس بازه تاریخی";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(43, 17);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(111, 26);
            this.txt_end.TabIndex = 71;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(131, 17);
            this.labelX5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(67, 27);
            this.labelX5.TabIndex = 72;
            this.labelX5.Text = "تا تاریخ :";
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(220, 16);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(111, 26);
            this.txt_start.TabIndex = 69;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(308, 16);
            this.labelX9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(67, 27);
            this.labelX9.TabIndex = 70;
            this.labelX9.Text = "از تاریخ :";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(11, 59);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(142, 33);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 40;
            this.buttonX2.Text = "نمایش اطلاعات درآمدی";
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "بر اساس بازه زمانی";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.groupPanel1);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(911, 168);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.cmbHesab);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(19, 20);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(876, 127);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 64;
            this.groupPanel1.Text = "بر اساس شماره حساب";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(489, 46);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(142, 33);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 40;
            this.buttonX1.Text = "نمایش اطلاعات درآمدی";
            // 
            // cmbHesab
            // 
            this.cmbHesab.DisplayMember = "onvan";
            this.cmbHesab.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbHesab.ForeColor = System.Drawing.Color.Black;
            this.cmbHesab.FormattingEnabled = true;
            this.cmbHesab.ItemHeight = 18;
            this.cmbHesab.Location = new System.Drawing.Point(515, 5);
            this.cmbHesab.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cmbHesab.Name = "cmbHesab";
            this.cmbHesab.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbHesab.Size = new System.Drawing.Size(269, 24);
            this.cmbHesab.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbHesab.TabIndex = 38;
            this.cmbHesab.ValueMember = "id";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(780, -1);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(61, 39);
            this.labelX1.TabIndex = 39;
            this.labelX1.Text = "حساب :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "براساس کد قبض";
            // 
            // groupPanel6
            // 
            this.groupPanel6.BackColor = System.Drawing.Color.White;
            this.groupPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel6.Controls.Add(this.groupPanel5);
            this.groupPanel6.Controls.Add(this.superTabControl2);
            this.groupPanel6.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel6.Location = new System.Drawing.Point(26, 290);
            this.groupPanel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel6.Name = "groupPanel6";
            this.groupPanel6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel6.Size = new System.Drawing.Size(909, 279);
            // 
            // 
            // 
            this.groupPanel6.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel6.Style.BackColorGradientAngle = 90;
            this.groupPanel6.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel6.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderBottomWidth = 1;
            this.groupPanel6.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel6.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderLeftWidth = 1;
            this.groupPanel6.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderRightWidth = 1;
            this.groupPanel6.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderTopWidth = 1;
            this.groupPanel6.Style.CornerDiameter = 4;
            this.groupPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel6.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel6.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel6.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel6.TabIndex = 71;
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.labelX3);
            this.groupPanel5.Controls.Add(this.labelX4);
            this.groupPanel5.Controls.Add(this.labelX6);
            this.groupPanel5.Controls.Add(this.labelX7);
            this.groupPanel5.Controls.Add(this.labelX8);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(17, 277);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(855, 51);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 73;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(46, 4);
            this.labelX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(94, 30);
            this.labelX3.TabIndex = 49;
            this.labelX3.Text = "22350125256";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(146, 4);
            this.labelX4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(80, 30);
            this.labelX4.TabIndex = 48;
            this.labelX4.Text = "دوره:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(232, 4);
            this.labelX6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX6.Name = "labelX6";
            this.labelX6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX6.Size = new System.Drawing.Size(80, 30);
            this.labelX6.TabIndex = 47;
            this.labelX6.Text = "دوره:";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(318, 4);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(80, 30);
            this.labelX7.TabIndex = 46;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(404, 4);
            this.labelX8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX8.Name = "labelX8";
            this.labelX8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX8.Size = new System.Drawing.Size(80, 30);
            this.labelX8.TabIndex = 45;
            this.labelX8.Text = "دوره:";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // superTabControl2
            // 
            this.superTabControl2.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl2.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl2.ControlBox.MenuBox.Name = "";
            this.superTabControl2.ControlBox.Name = "";
            this.superTabControl2.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl2.ControlBox.CloseBox,
            this.superTabControl2.ControlBox.MenuBox});
            this.superTabControl2.Controls.Add(this.superTabControlPanel4);
            this.superTabControl2.Controls.Add(this.superTabControlPanel5);
            this.superTabControl2.ForeColor = System.Drawing.Color.Black;
            this.superTabControl2.Location = new System.Drawing.Point(17, 11);
            this.superTabControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControl2.Name = "superTabControl2";
            this.superTabControl2.ReorderTabsEnabled = true;
            this.superTabControl2.SelectedTabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl2.SelectedTabIndex = 1;
            this.superTabControl2.Size = new System.Drawing.Size(855, 251);
            this.superTabControl2.TabFont = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTabControl2.TabIndex = 0;
            this.superTabControl2.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem4,
            this.superTabItem5});
            this.superTabControl2.Text = "superTabControl2";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.dgv_ghobooz);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(855, 224);
            this.superTabControlPanel4.TabIndex = 1;
            this.superTabControlPanel4.TabItem = this.superTabItem4;
            // 
            // dgv_ghobooz
            // 
            this.dgv_ghobooz.AllowUserToAddRows = false;
            this.dgv_ghobooz.AllowUserToDeleteRows = false;
            this.dgv_ghobooz.AllowUserToResizeColumns = false;
            this.dgv_ghobooz.AllowUserToResizeRows = false;
            this.dgv_ghobooz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ghobooz.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ghobooz.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ghobooz.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ghobooz.EnableHeadersVisualStyles = false;
            this.dgv_ghobooz.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_ghobooz.Location = new System.Drawing.Point(19, 13);
            this.dgv_ghobooz.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgv_ghobooz.Name = "dgv_ghobooz";
            this.dgv_ghobooz.ReadOnly = true;
            this.dgv_ghobooz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ghobooz.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ghobooz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ghobooz.Size = new System.Drawing.Size(819, 264);
            this.dgv_ghobooz.TabIndex = 46;
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel4;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "قبوض آب";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(855, 251);
            this.superTabControlPanel5.TabIndex = 0;
            this.superTabControlPanel5.TabItem = this.superTabItem5;
            // 
            // superTabItem5
            // 
            this.superTabItem5.AttachedControl = this.superTabControlPanel5;
            this.superTabItem5.GlobalItem = false;
            this.superTabItem5.Name = "superTabItem5";
            this.superTabItem5.Text = "قبوض شارژ";
            // 
            // groupPanel7
            // 
            this.groupPanel7.BackColor = System.Drawing.Color.White;
            this.groupPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel7.Controls.Add(this.buttonX5);
            this.groupPanel7.Controls.Add(this.lbSumMablaghKol);
            this.groupPanel7.Controls.Add(this.lbSumBedehi);
            this.groupPanel7.Controls.Add(this.lbSumSayer);
            this.groupPanel7.Controls.Add(this.lbSumMaliat);
            this.groupPanel7.Controls.Add(this.lbSumMabalegh);
            this.groupPanel7.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel7.Location = new System.Drawing.Point(26, 577);
            this.groupPanel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel7.Name = "groupPanel7";
            this.groupPanel7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel7.Size = new System.Drawing.Size(909, 68);
            // 
            // 
            // 
            this.groupPanel7.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel7.Style.BackColorGradientAngle = 90;
            this.groupPanel7.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel7.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderBottomWidth = 1;
            this.groupPanel7.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel7.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderLeftWidth = 1;
            this.groupPanel7.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderRightWidth = 1;
            this.groupPanel7.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel7.Style.BorderTopWidth = 1;
            this.groupPanel7.Style.CornerDiameter = 4;
            this.groupPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel7.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel7.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel7.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel7.TabIndex = 73;
            this.groupPanel7.Text = "اطلاعات مالی قبوض آب و شارژ به صورت  تجمیعی";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(657, 7);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX5.Size = new System.Drawing.Size(243, 23);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Green;
            this.buttonX5.SymbolSize = 9F;
            this.buttonX5.TabIndex = 50;
            this.buttonX5.Text = "مشاهده اطلاعات آماری به صورت کامل :";
            // 
            // lbSumMablaghKol
            // 
            this.lbSumMablaghKol.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbSumMablaghKol.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbSumMablaghKol.ForeColor = System.Drawing.Color.Black;
            this.lbSumMablaghKol.Location = new System.Drawing.Point(46, 4);
            this.lbSumMablaghKol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSumMablaghKol.Name = "lbSumMablaghKol";
            this.lbSumMablaghKol.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbSumMablaghKol.Size = new System.Drawing.Size(94, 30);
            this.lbSumMablaghKol.TabIndex = 49;
            this.lbSumMablaghKol.Text = "22350125256";
            this.lbSumMablaghKol.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbSumBedehi
            // 
            this.lbSumBedehi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbSumBedehi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbSumBedehi.ForeColor = System.Drawing.Color.Black;
            this.lbSumBedehi.Location = new System.Drawing.Point(146, 4);
            this.lbSumBedehi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSumBedehi.Name = "lbSumBedehi";
            this.lbSumBedehi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbSumBedehi.Size = new System.Drawing.Size(80, 30);
            this.lbSumBedehi.TabIndex = 48;
            this.lbSumBedehi.Text = "دوره:";
            this.lbSumBedehi.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbSumSayer
            // 
            this.lbSumSayer.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbSumSayer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbSumSayer.ForeColor = System.Drawing.Color.Black;
            this.lbSumSayer.Location = new System.Drawing.Point(232, 4);
            this.lbSumSayer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSumSayer.Name = "lbSumSayer";
            this.lbSumSayer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbSumSayer.Size = new System.Drawing.Size(80, 30);
            this.lbSumSayer.TabIndex = 47;
            this.lbSumSayer.Text = "دوره:";
            this.lbSumSayer.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbSumMaliat
            // 
            this.lbSumMaliat.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbSumMaliat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbSumMaliat.ForeColor = System.Drawing.Color.Black;
            this.lbSumMaliat.Location = new System.Drawing.Point(318, 4);
            this.lbSumMaliat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSumMaliat.Name = "lbSumMaliat";
            this.lbSumMaliat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbSumMaliat.Size = new System.Drawing.Size(80, 30);
            this.lbSumMaliat.TabIndex = 46;
            this.lbSumMaliat.Text = "دوره:";
            this.lbSumMaliat.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbSumMabalegh
            // 
            this.lbSumMabalegh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbSumMabalegh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbSumMabalegh.ForeColor = System.Drawing.Color.Black;
            this.lbSumMabalegh.Location = new System.Drawing.Point(404, 4);
            this.lbSumMabalegh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbSumMabalegh.Name = "lbSumMabalegh";
            this.lbSumMabalegh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbSumMabalegh.Size = new System.Drawing.Size(80, 30);
            this.lbSumMabalegh.TabIndex = 45;
            this.lbSumMabalegh.Text = "دوره:";
            this.lbSumMabalegh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel9
            // 
            this.groupPanel9.BackColor = System.Drawing.Color.White;
            this.groupPanel9.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel9.Controls.Add(this.Grpfindgharardad);
            this.groupPanel9.Controls.Add(this.rdGharardad);
            this.groupPanel9.Controls.Add(this.radioButton4);
            this.groupPanel9.Controls.Add(this.checkBoxX8);
            this.groupPanel9.Controls.Add(this.checkBoxX9);
            this.groupPanel9.Controls.Add(this.checkBoxX10);
            this.groupPanel9.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel9.Location = new System.Drawing.Point(26, 221);
            this.groupPanel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel9.Name = "groupPanel9";
            this.groupPanel9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel9.Size = new System.Drawing.Size(909, 61);
            // 
            // 
            // 
            this.groupPanel9.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel9.Style.BackColorGradientAngle = 90;
            this.groupPanel9.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel9.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderBottomWidth = 1;
            this.groupPanel9.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel9.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderLeftWidth = 1;
            this.groupPanel9.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderRightWidth = 1;
            this.groupPanel9.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderTopWidth = 1;
            this.groupPanel9.Style.CornerDiameter = 4;
            this.groupPanel9.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel9.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel9.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel9.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel9.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel9.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel9.TabIndex = 75;
            // 
            // Grpfindgharardad
            // 
            this.Grpfindgharardad.BackColor = System.Drawing.Color.Transparent;
            this.Grpfindgharardad.Controls.Add(this.buttonX4);
            this.Grpfindgharardad.Controls.Add(this.textBoxX1);
            this.Grpfindgharardad.ForeColor = System.Drawing.Color.Black;
            this.Grpfindgharardad.Location = new System.Drawing.Point(525, 21);
            this.Grpfindgharardad.Name = "Grpfindgharardad";
            this.Grpfindgharardad.Size = new System.Drawing.Size(137, 29);
            this.Grpfindgharardad.TabIndex = 9;
            this.Grpfindgharardad.Visible = false;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(19, 3);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(27, 23);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 47;
            // 
            // textBoxX1
            // 
            this.textBoxX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.textBoxX1.Border.Class = "TextBoxBorder";
            this.textBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.textBoxX1.DisabledBackColor = System.Drawing.Color.White;
            this.textBoxX1.Font = new System.Drawing.Font("B Yekan", 7F);
            this.textBoxX1.ForeColor = System.Drawing.Color.Black;
            this.textBoxX1.Location = new System.Drawing.Point(52, 4);
            this.textBoxX1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.textBoxX1.Name = "textBoxX1";
            this.textBoxX1.PreventEnterBeep = true;
            this.textBoxX1.Size = new System.Drawing.Size(66, 22);
            this.textBoxX1.TabIndex = 46;
            // 
            // rdGharardad
            // 
            this.rdGharardad.AutoSize = true;
            this.rdGharardad.BackColor = System.Drawing.Color.Transparent;
            this.rdGharardad.ForeColor = System.Drawing.Color.Black;
            this.rdGharardad.Location = new System.Drawing.Point(668, 28);
            this.rdGharardad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdGharardad.Name = "rdGharardad";
            this.rdGharardad.Size = new System.Drawing.Size(192, 21);
            this.rdGharardad.TabIndex = 8;
            this.rdGharardad.Text = "نمایش اطلاعات بر اساس شماره قرارداد:";
            this.rdGharardad.UseVisualStyleBackColor = false;
            this.rdGharardad.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Checked = true;
            this.radioButton4.ForeColor = System.Drawing.Color.Black;
            this.radioButton4.Location = new System.Drawing.Point(692, 7);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(170, 21);
            this.radioButton4.TabIndex = 7;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "نمایش اطلاعات برای کلیه مشترکین";
            this.radioButton4.UseVisualStyleBackColor = false;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // checkBoxX8
            // 
            this.checkBoxX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.checkBoxX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBoxX8.ForeColor = System.Drawing.Color.Black;
            this.checkBoxX8.Location = new System.Drawing.Point(176, 25);
            this.checkBoxX8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxX8.Name = "checkBoxX8";
            this.checkBoxX8.Size = new System.Drawing.Size(72, 30);
            this.checkBoxX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checkBoxX8.TabIndex = 4;
            this.checkBoxX8.Text = "پرداخت شده";
            this.checkBoxX8.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            // 
            // checkBoxX9
            // 
            this.checkBoxX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.checkBoxX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBoxX9.ForeColor = System.Drawing.Color.Black;
            this.checkBoxX9.Location = new System.Drawing.Point(-10, 25);
            this.checkBoxX9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxX9.Name = "checkBoxX9";
            this.checkBoxX9.Size = new System.Drawing.Size(100, 30);
            this.checkBoxX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checkBoxX9.TabIndex = 3;
            this.checkBoxX9.Text = "پرداخت نشده";
            this.checkBoxX9.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            // 
            // checkBoxX10
            // 
            this.checkBoxX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.checkBoxX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.checkBoxX10.ForeColor = System.Drawing.Color.Black;
            this.checkBoxX10.Location = new System.Drawing.Point(70, 25);
            this.checkBoxX10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxX10.Name = "checkBoxX10";
            this.checkBoxX10.Size = new System.Drawing.Size(100, 30);
            this.checkBoxX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.checkBoxX10.TabIndex = 2;
            this.checkBoxX10.Text = "پرداخت جزئی";
            this.checkBoxX10.TextColor = System.Drawing.Color.DarkKhaki;
            // 
            // FrmTemp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 733);
            this.Controls.Add(this.groupPanel9);
            this.Controls.Add(this.groupPanel7);
            this.Controls.Add(this.groupPanel6);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmTemp";
            this.Text = "MetroForm";
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel6.ResumeLayout(false);
            this.groupPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl2)).EndInit();
            this.superTabControl2.ResumeLayout(false);
            this.superTabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).EndInit();
            this.groupPanel7.ResumeLayout(false);
            this.groupPanel9.ResumeLayout(false);
            this.groupPanel9.PerformLayout();
            this.Grpfindgharardad.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbHesab;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private DevComponents.DotNetBar.LabelX labelX5;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTozihat;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel6;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.SuperTabControl superTabControl2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private DevComponents.DotNetBar.SuperTabItem superTabItem5;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_ghobooz;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel7;
        private DevComponents.DotNetBar.LabelX lbSumMablaghKol;
        private DevComponents.DotNetBar.LabelX lbSumBedehi;
        private DevComponents.DotNetBar.LabelX lbSumSayer;
        private DevComponents.DotNetBar.LabelX lbSumMaliat;
        private DevComponents.DotNetBar.LabelX lbSumMabalegh;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel9;
        private System.Windows.Forms.RadioButton rdGharardad;
        private System.Windows.Forms.RadioButton radioButton4;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBoxX8;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBoxX9;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBoxX10;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private System.Windows.Forms.Panel Grpfindgharardad;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.Controls.TextBoxX textBoxX1;
    }
}