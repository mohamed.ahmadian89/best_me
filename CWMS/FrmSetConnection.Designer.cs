﻿namespace CWMS
{
    partial class FrmSetConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIP = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.Txtusername = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtpassword = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.ch_intg = new System.Windows.Forms.CheckBox();
            this.txt_db_name = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // txtIP
            // 
            this.txtIP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtIP.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtIP.Border.Class = "TextBoxBorder";
            this.txtIP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIP.DisabledBackColor = System.Drawing.Color.White;
            this.txtIP.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtIP.ForeColor = System.Drawing.Color.Black;
            this.txtIP.Location = new System.Drawing.Point(571, 42);
            this.txtIP.Name = "txtIP";
            this.txtIP.PreventEnterBeep = true;
            this.txtIP.Size = new System.Drawing.Size(145, 28);
            this.txtIP.TabIndex = 8;
            this.txtIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIP_KeyDown);
            // 
            // labelX14
            // 
            this.labelX14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(722, 40);
            this.labelX14.Name = "labelX14";
            this.labelX14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX14.Size = new System.Drawing.Size(79, 33);
            this.labelX14.TabIndex = 9;
            this.labelX14.Text = "ای پی سرور :";
            // 
            // Txtusername
            // 
            this.Txtusername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Txtusername.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.Txtusername.Border.Class = "TextBoxBorder";
            this.Txtusername.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Txtusername.DisabledBackColor = System.Drawing.Color.White;
            this.Txtusername.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Txtusername.ForeColor = System.Drawing.Color.Black;
            this.Txtusername.Location = new System.Drawing.Point(336, 42);
            this.Txtusername.Name = "Txtusername";
            this.Txtusername.PreventEnterBeep = true;
            this.Txtusername.Size = new System.Drawing.Size(145, 28);
            this.Txtusername.TabIndex = 10;
            this.Txtusername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtusername_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(486, 40);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(79, 33);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "نام کاربری :";
            // 
            // txtpassword
            // 
            this.txtpassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtpassword.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtpassword.Border.Class = "TextBoxBorder";
            this.txtpassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtpassword.DisabledBackColor = System.Drawing.Color.White;
            this.txtpassword.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtpassword.ForeColor = System.Drawing.Color.Black;
            this.txtpassword.Location = new System.Drawing.Point(336, 76);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PreventEnterBeep = true;
            this.txtpassword.Size = new System.Drawing.Size(145, 28);
            this.txtpassword.TabIndex = 12;
            this.txtpassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpassword_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(486, 74);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(79, 33);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "رمز عبور:";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.buttonX1.Location = new System.Drawing.Point(12, 49);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(276, 54);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 14;
            this.buttonX1.Text = "ثبت اطلاعات ";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // ch_intg
            // 
            this.ch_intg.AutoSize = true;
            this.ch_intg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.ch_intg.Font = new System.Drawing.Font("B Yekan", 10F);
            this.ch_intg.ForeColor = System.Drawing.Color.Black;
            this.ch_intg.Location = new System.Drawing.Point(858, 60);
            this.ch_intg.Name = "ch_intg";
            this.ch_intg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ch_intg.Size = new System.Drawing.Size(147, 25);
            this.ch_intg.TabIndex = 15;
            this.ch_intg.Text = "Integrated Security";
            this.ch_intg.UseVisualStyleBackColor = false;
            this.ch_intg.CheckedChanged += new System.EventHandler(this.ch_intg_CheckedChanged);
            // 
            // txt_db_name
            // 
            this.txt_db_name.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_db_name.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_db_name.Border.Class = "TextBoxBorder";
            this.txt_db_name.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_db_name.DisabledBackColor = System.Drawing.Color.White;
            this.txt_db_name.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_db_name.ForeColor = System.Drawing.Color.Black;
            this.txt_db_name.Location = new System.Drawing.Point(571, 76);
            this.txt_db_name.Name = "txt_db_name";
            this.txt_db_name.PreventEnterBeep = true;
            this.txt_db_name.Size = new System.Drawing.Size(145, 28);
            this.txt_db_name.TabIndex = 16;
            this.txt_db_name.Text = "cwms";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(722, 74);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(79, 29);
            this.labelX3.TabIndex = 17;
            this.labelX3.Text = "نام دیتابیس :";
            // 
            // FrmSetConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 161);
            this.Controls.Add(this.txt_db_name);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.ch_intg);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.Txtusername);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.txtIP);
            this.Controls.Add(this.labelX14);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSetConnection";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmSetConnection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.TextBoxX txtIP;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.TextBoxX Txtusername;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtpassword;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.CheckBox ch_intg;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_db_name;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}