﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class FrmPayam : DevComponents.DotNetBar.Metro.MetroForm
    {
        public FrmPayam()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }
        public FrmPayam(string gharardad)
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            txtfind.Text = gharardad;
            btnFind.PerformClick();
        }


        private void btnSend_Click(object sender, EventArgs e)
        {

            if (txtMatn.Text == "")
                txtMatn.Focus();
            else
            {
                chbarrasi.Checked = true;
                txtdate.SelectedDateTime = DateTime.Now;
                mokatebehBindingSource.EndEdit();
                mokatebehTableAdapter.Update(mainDataSest.Mokatebeh);

                Payam.Show("پیام شما با موفقیت برای مدیریت ارسال شد");

            }
        }

        private void FrmPayam_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.Mokatebeh' table. You can move, or remove it, as needed.
            txtfind.Focus();
        }

        private void txtfind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFind.PerformClick();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            mokatebehTableAdapter.FillByGharardad(mainDataSest.Mokatebeh, Convert.ToInt32(-1));

            if (txtfind.Text.Trim() == "")
                txtfind.Focus();
            else
            {
                try
                {
                    Convert.ToInt32(txtfind.Text);
                    string Gharardad = Classes.clsMoshtarekin.Exist_gharardad(txtfind.Text);
                    if (Gharardad == "")
                    {
                        txtfind.SelectAll();
                        txtfind.Focus();
                        Payam.Show("شماره قرارداد در سیستم ثبت نشده است ");
                        btnSend.Enabled = false;
                    }
                    else
                    {
                        lblMoshtarekName.Text = Gharardad;
                        lblMoshtarekName.Tag = txtfind.Text;
                        btnSend.Enabled = true;
                        mokatebehTableAdapter.FillByGharardad(mainDataSest.Mokatebeh, Convert.ToInt32(txtfind.Text));
                    }

                }
                catch
                {
                    Payam.Show("شماره قرارداد نامعتبر می باشد");
                    txtfind.Clear();
                    txtfind.Focus();
                }
            }
        }

        private void txtMatn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSend.PerformClick();
        }

        private void txtfind_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFind.PerformClick();
        }

        private void FrmPayam_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

        }
    }
}