﻿namespace CWMS
{
    partial class Frm_moshtarekin_savabegh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find_savabegh = new DevComponents.DotNetBar.ButtonX();
            this.txtsavabegh = new System.Windows.Forms.TextBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvNOtofication = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modiramelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.savabeghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moshtarekinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.moshtarekinTableAdapter = new CWMS.MainDataSestTableAdapters.moshtarekinTableAdapter();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNOtofication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btn_find_savabegh);
            this.groupPanel1.Controls.Add(this.txtsavabegh);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(19, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(859, 95);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 31;
            this.groupPanel1.Text = "جستجو ";
            // 
            // btn_find_savabegh
            // 
            this.btn_find_savabegh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_savabegh.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_savabegh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_savabegh.Location = new System.Drawing.Point(12, 43);
            this.btn_find_savabegh.Name = "btn_find_savabegh";
            this.btn_find_savabegh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_savabegh.Size = new System.Drawing.Size(216, 23);
            this.btn_find_savabegh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_savabegh.Symbol = "";
            this.btn_find_savabegh.SymbolColor = System.Drawing.Color.Green;
            this.btn_find_savabegh.SymbolSize = 9F;
            this.btn_find_savabegh.TabIndex = 33;
            this.btn_find_savabegh.Text = "جستجو در سوابق مشترکین";
            this.btn_find_savabegh.Click += new System.EventHandler(this.btn_find_savabegh_Click);
            // 
            // txtsavabegh
            // 
            this.txtsavabegh.BackColor = System.Drawing.Color.White;
            this.txtsavabegh.ForeColor = System.Drawing.Color.Black;
            this.txtsavabegh.Location = new System.Drawing.Point(234, 42);
            this.txtsavabegh.Multiline = true;
            this.txtsavabegh.Name = "txtsavabegh";
            this.txtsavabegh.Size = new System.Drawing.Size(572, 26);
            this.txtsavabegh.TabIndex = 32;
            this.txtsavabegh.TextChanged += new System.EventHandler(this.txtsavabegh_TextChanged);
            this.txtsavabegh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsavabegh_KeyDown);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(345, 12);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(488, 24);
            this.labelX3.TabIndex = 31;
            this.labelX3.Tag = "g";
            this.labelX3.Text = "لطفا قسمتی از کلمات مربوط به سوابق مشترکین را در کادر زیر وارد نمایید وبر روی دکم" +
    "ه جستجو کلیک نمایید";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.dgvNOtofication);
            this.groupPanel2.Controls.Add(this.textBox1);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(19, 113);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(859, 411);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 32;
            this.groupPanel2.Text = "جستجو ";
            // 
            // dgvNOtofication
            // 
            this.dgvNOtofication.AllowUserToAddRows = false;
            this.dgvNOtofication.AllowUserToDeleteRows = false;
            this.dgvNOtofication.AllowUserToResizeColumns = false;
            this.dgvNOtofication.AllowUserToResizeRows = false;
            this.dgvNOtofication.AutoGenerateColumns = false;
            this.dgvNOtofication.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNOtofication.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNOtofication.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvNOtofication.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.modiramelDataGridViewTextBoxColumn,
            this.savabeghDataGridViewTextBoxColumn});
            this.dgvNOtofication.DataSource = this.moshtarekinBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNOtofication.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNOtofication.EnableHeadersVisualStyles = false;
            this.dgvNOtofication.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvNOtofication.Location = new System.Drawing.Point(22, 18);
            this.dgvNOtofication.Name = "dgvNOtofication";
            this.dgvNOtofication.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNOtofication.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvNOtofication.RowHeadersVisible = false;
            this.dgvNOtofication.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvNOtofication.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNOtofication.Size = new System.Drawing.Size(805, 224);
            this.dgvNOtofication.TabIndex = 33;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.FillWeight = 120F;
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام مشترک";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modiramelDataGridViewTextBoxColumn
            // 
            this.modiramelDataGridViewTextBoxColumn.DataPropertyName = "modir_amel";
            this.modiramelDataGridViewTextBoxColumn.HeaderText = "مدیریت عامل";
            this.modiramelDataGridViewTextBoxColumn.Name = "modiramelDataGridViewTextBoxColumn";
            this.modiramelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // savabeghDataGridViewTextBoxColumn
            // 
            this.savabeghDataGridViewTextBoxColumn.DataPropertyName = "savabegh";
            this.savabeghDataGridViewTextBoxColumn.FillWeight = 180F;
            this.savabeghDataGridViewTextBoxColumn.HeaderText = "قسمتی از سوابق ";
            this.savabeghDataGridViewTextBoxColumn.Name = "savabeghDataGridViewTextBoxColumn";
            this.savabeghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // moshtarekinBindingSource
            // 
            this.moshtarekinBindingSource.DataMember = "moshtarekin";
            this.moshtarekinBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "savabegh", true));
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(22, 283);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(805, 86);
            this.textBox1.TabIndex = 32;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(648, 253);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(170, 24);
            this.labelX1.TabIndex = 31;
            this.labelX1.Tag = "g";
            this.labelX1.Text = "جزئیات مربوط به قرارداد انتخاب شده :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // moshtarekinTableAdapter
            // 
            this.moshtarekinTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_moshtarekin_savabegh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 525);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_moshtarekin_savabegh";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            
            this.Load += new System.EventHandler(this.FrmSearchInMoshtarekinSavabegh_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNOtofication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_find_savabegh;
        private System.Windows.Forms.TextBox txtsavabegh;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.TextBox textBox1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvNOtofication;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource moshtarekinBindingSource;
        private MainDataSestTableAdapters.moshtarekinTableAdapter moshtarekinTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modiramelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn savabeghDataGridViewTextBoxColumn;

    }
}