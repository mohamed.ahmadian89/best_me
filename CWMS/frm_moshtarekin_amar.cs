﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS
{
    public partial class frm_moshtarekin_amar : MyMetroForm
    {
        public frm_moshtarekin_amar()
        {
            InitializeComponent();
        }

        private void frm_moshtarekin_amar_Load(object sender, EventArgs e)
        {
            DataTable dt_moshtarekin = Classes.ClsMain.GetDataTable("exec amar_moshtrekin");
            txt_moshtarekin_count_gheir_tejari.Text = dt_moshtarekin.Rows[1][0].ToString();
            txt_metraj_gheir_tejari.Text = dt_moshtarekin.Rows[1][1].ToString();

            txt_moshtarekin_count_tejari.Text = dt_moshtarekin.Rows[0][0].ToString();
            txt_metraj_tejari.Text= dt_moshtarekin.Rows[0][1].ToString();


            txt_moshtarek_have_ab.Text = Classes.ClsMain.ExecuteScalar("select count(*) from moshtarekin where have_ab_ghabz=1").ToString();
            txt_moshtarek_have_sharj.Text = Classes.ClsMain.ExecuteScalar("select count(*) from moshtarekin where have_sharj_ghabz=1").ToString();


            txt_moshtarekin_count.Text = (Convert.ToInt32(txt_moshtarekin_count_gheir_tejari.Text) 
                + Convert.ToInt32(txt_moshtarekin_count_tejari.Text)).ToString();

            txt_metraj_all.Text = (Convert.ToDecimal(txt_metraj_gheir_tejari.Text)
                + Convert.ToDecimal(txt_metraj_tejari.Text)).ToString();


            dgv_sharj_result.DataSource= Classes.ClsMain.GetDataTable("exec [amar_moshtrekin_sharj]");
            dgv_ab_result.DataSource = Classes.ClsMain.GetDataTable("exec [amar_moshtrekin_ab]");

        }
    }
}
