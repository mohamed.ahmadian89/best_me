﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
namespace CWMS
{
    public partial class FrmJarimehAfterGhabzDeadLine : MyMetroForm
    {
        public FrmJarimehAfterGhabzDeadLine()
        {
            //Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }

        private void FrmJarimehAfterGhabzDeadLine_Load(object sender, EventArgs e)
        {
            DataTable dt_ab = Classes.clsDoreh.Get_last_Ab_Doreh();
            txt_ab_Doreh.Text = dt_ab.Rows[0]["dcode"].ToString();
            txt_ab_mohlat_pardaht.SelectedDateTime = Convert.ToDateTime(dt_ab.Rows[0]["mohlat_pardakht"]);

            DataTable dt_sharj = Classes.clsDoreh.Get_last_Sharj_Doreh();
            txt_sharj_doreh.Text = dt_sharj.Rows[0]["dcode"].ToString();
            txt_sharj_mohlat_pardakht.SelectedDateTime = Convert.ToDateTime(dt_sharj.Rows[0]["mohlat_pardakht"]);


            Show_ab_ghabz();
            Show_sharj_ghabz();


        }
        DataTable dt_sharj_ghabz = null;
        void Show_sharj_ghabz()
        {

            if (txt_sharj_mohlat_pardakht.SelectedDateTime < DateTime.Now)
            {

                dt_sharj_ghabz = Classes.ClsMain.GetDataTable("select * from sharj_ghabz where dcode=" + txt_sharj_doreh.Text
                    + " and vaziat<>1"
                    );

                dgv_sharj.Rows.Clear();
                for (int i = 0; i < dt_sharj_ghabz.Rows.Count; i++)
                {
                    dgv_sharj.Rows.Add(dt_sharj_ghabz.Rows[i]["gharardad"].ToString(), dt_sharj_ghabz.Rows[i]["mablaghkol"], dt_sharj_ghabz.Rows[i]["jarimeh"]);
                }
                txt_sharj_cnt.Text = dt_sharj_ghabz.Rows.Count.ToString();
            }
        }
        DataTable dt_ab_ghabz = null;
        void Show_ab_ghabz()
        {
            if (txt_ab_mohlat_pardaht.SelectedDateTime < DateTime.Now)
            {
                dt_ab_ghabz = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + txt_ab_Doreh.Text
                    + " and vaziat<>1"
                    );
                dgv_ab.Rows.Clear();

                for (int i = 0; i < dt_ab_ghabz.Rows.Count; i++)
                {
                    dgv_ab.Rows.Add(dt_ab_ghabz.Rows[i]["gharardad"].ToString(), dt_ab_ghabz.Rows[i]["mablaghkol"], dt_ab_ghabz.Rows[i]["jarimeh"]);
                }
                txt_ab_cnt.Text = dt_ab_ghabz.Rows.Count.ToString();
            }
        }
        long All_jarayem = 0;
        private void btn_ab_jarimeh_Click(object sender, EventArgs e)
        {
            Payam.Show("هم اکنون در حال پیاده سازی و اتمام این گزاش هستیم . به محض اتمام، به شما اطلاع خواهیم داد.");
            return;
            if (Cpayam.Show("ایا با اعمال جریمه بر روی کلیه قبوض لیست فوق موافق هستید؟") == DialogResult.Yes)
            {
                if (txt_ab_cnt.Text != "0")
                {
                    try
                    {

                        long jarimeh = Convert.ToInt64(txt_ab_mablagh_jarimhe.Text);
                        Ab_jarimeh();

                        Payam.Show("جرایم با موفقیت در  قبوض آب مشترکین اعمال شد");
                        Show_ab_ghabz();
                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا مبلغ جریمه را صحیح وارد نمایید");
                        txt_ab_mablagh_jarimhe.Clear();
                        txt_ab_mablagh_jarimhe.Focus();

                    }
                }
            }
        }


        void Ab_jarimeh()
        {
            Classes.MultipleCommand multi = new Classes.MultipleCommand("ab_jarimeh");
            long jarimeh = 0,oldJarimeh=0, mablaghkol = 0;
            int gharardad = 0, kasr_hezar = 0;
            All_jarayem = 0;
            for (int i = 0; i < dgv_ab.Rows.Count; i++)
            {


                gharardad = Convert.ToInt32(dgv_ab.Rows[i].Cells[0].Value.ToString());
                var row = dt_ab_ghabz.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                kasr_hezar = Convert.ToInt32(row["kasr_hezar"]);
                mablaghkol = Convert.ToInt64(row["mablaghkol"]);
                oldJarimeh = Convert.ToInt64(row["jarimeh"]);
               

                if (rd_ab_sabet.Checked)
                    jarimeh = Convert.ToInt64(txt_ab_mablagh_jarimhe.Text);
                else
                    jarimeh = Convert.ToInt64(txt_ab_mablagh_jarimhe.Text) * mablaghkol / 100;


                mablaghkol = mablaghkol + jarimeh + kasr_hezar;
                kasr_hezar = Convert.ToInt32(mablaghkol % 1000);
                mablaghkol = mablaghkol - kasr_hezar;


                // مالیات در این مرحله تغییری نمی کند . چرا که مالیات بر روی جرایم اعمال شده تاثیری نخواهد داشت

                multi.addToCommandCollecton("update ab_ghabz set jarimeh=" + (jarimeh + oldJarimeh).ToString() + ",mablaghkol=" + mablaghkol + ",kasr_hezar=" + kasr_hezar + " where gharardad=" + gharardad);
            }
            multi.RunCommandCollecton("ab_jarayem");
        }

        long All__sharj_jarayem = 0;
        void sharj_jarimeh()
        {
            Classes.MultipleCommand multi = new Classes.MultipleCommand("ab_jarimeh");
            long jarimeh = 0, oldJarimeh = 0, mablaghkol = 0;
            int gharardad = 0, kasr_hezar = 0;
            All__sharj_jarayem = 0;
            for (int i = 0; i < dgv_sharj.Rows.Count; i++)
            {


                gharardad = Convert.ToInt32(dgv_sharj.Rows[i].Cells[0].Value.ToString());
                var row = dt_sharj_ghabz.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                kasr_hezar = Convert.ToInt32(row["kasr_hezar"]);
                mablaghkol = Convert.ToInt64(row["mablaghkol"]);
                oldJarimeh = Convert.ToInt64(row["jarimeh"]);
                if (rd_sharj_sabet.Checked)
                    jarimeh = Convert.ToInt64(txt_sharj_jarimeh.Text);
                else
                    jarimeh = Convert.ToInt64(txt_sharj_jarimeh.Text) * mablaghkol / 100;


                mablaghkol = mablaghkol + jarimeh + kasr_hezar;
                kasr_hezar = Convert.ToInt32(mablaghkol % 1000);
                mablaghkol = mablaghkol - kasr_hezar;


                // مالیات در این مرحله تغییری نمی کند . چرا که مالیات بر روی جرایم اعمال شده تاثیری نخواهد داشت

                multi.addToCommandCollecton("update sharj_ghabz set jarimeh=" + (jarimeh + oldJarimeh).ToString() + ",mablaghkol=" + mablaghkol + ",kasr_hezar=" + kasr_hezar + " where gharardad=" + gharardad);
            }
            multi.RunCommandCollecton("sharj_jarayem");
        }

        private void btn_sharj_jarimeh_Click(object sender, EventArgs e)
        {
            Payam.Show("هم اکنون در حال پیاده سازی و اتمام این گزارش هستیم . به محض اتمام، به شما اطلاع خواهیم داد.");
            return;
            if (Cpayam.Show("ایا با اعمال جریمه بر روی کلیه قبوض لیست فوق موافق هستید؟") == DialogResult.Yes)
            {

                if (txt_sharj_cnt.Text != "0")
                {
                    try
                    {

                        long jarimeh = Convert.ToInt64(txt_sharj_jarimeh.Text);
                        sharj_jarimeh();

                        Payam.Show("جرایم با موفقیت در  قبوض شارژ مشترکین اعمال شد");
                        Show_sharj_ghabz();
                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا مبلغ جریمه را صحیح وارد نمایید");
                        txt_sharj_jarimeh.Clear();
                        txt_sharj_jarimeh.Focus();

                    }
                }
            }
        }

        private void rd_ab_sabet_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_ab_sabet.Checked)
                lbl_ab_jarimeh_type.Text = "مبلغ جریمه" + ":";
            else
                lbl_ab_jarimeh_type.Text = "درصد از مبلغ کل :";
        }

        private void rd_sharj_sabet_CheckedChanged(object sender, EventArgs e)
        {
            if(rd_sharj_sabet.Checked)
                lbl_sharj_jarimeh_type.Text = "مبلغ جریمه" + ":";
            else
                lbl_sharj_jarimeh_type.Text = "درصد از مبلغ کل :";
        }
    }
}