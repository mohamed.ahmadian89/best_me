﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;

namespace CWMS
{
    public partial class FrmSmsInform : DevComponents.DotNetBar.Metro.MetroForm
    {
        string dcode = "";
        int is_ab = 0;
        DataTable dt = null;
        public FrmSmsInform(string info, string doreh, int ab, string tablename)
        {
            InitializeComponent();
            dt = Classes.ClsMain.GetDataTable("select gharardad from " + tablename + "_ghabz where dcode=" + doreh);
            grp_send_info.Text = info;
            dcode = doreh;
            is_ab = ab;
            txtstart.Value = Convert.ToInt32(dt.AsEnumerable().Min(p => p.Field<int>("gharardad")).ToString());
            txtend.Value = Convert.ToInt32(dt.AsEnumerable().Max(p => p.Field<int>("gharardad")).ToString());


            txtend.MaxValue = txtend.Value;
            txtstart.MaxValue = txtend.Value;
            txtstart.MinValue = txtstart.Value;

        }

        private void FrmSmsInform_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.moshtarek_notify_log' table. You can move, or remove it, as needed.
            this.moshtarek_notify_logTableAdapter.FillForSharj(this.mainDataSest.moshtarek_notify_log);

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            Payam.Show("این سامانه راه اندازی نشده است . لطفا با پشتیبان تماس حاصل نمایید");
            return;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToInt32(dt.Rows[i][0]) >= txtstart.Value && Convert.ToInt32(dt.Rows[0][0]) <= txtend.Value)
                    
                    Classes.clsMoshtarekin.NotifyBySms("", dt.Rows[i][0].ToString(), "ثبت قبض");

            }

            Payam.Show("عملیات اطلاع رسانی آغاز شد");
            Classes.ClsMain.ExecuteNoneQuery("insert into moshtarek_notify_log values ('" + DateTime.Now + "'," + dcode + ",'" + "اطلاع رسانی به مشترکین از طریق سامانه پیامکی" + "'," + is_ab + ")");
            this.moshtarek_notify_logTableAdapter.FillForSharj(this.mainDataSest.moshtarek_notify_log);

        }

        private void buttonX7_Click(object sender, EventArgs e)
        {

            
            if (txttozihat.Text.Trim() == "")
                Payam.Show("لطفا متن اطلاعیه را وارد نمایید");
            else
            {
                if (Cpayam.Show("ایا با ارسال این اطلاعیه به پورتال کلیه مشترکین موافق هستید؟") == DialogResult.Yes)
                {

                    Payam.Show("با تایید شما عملیات اطلاع رسانی اغاز خواهد شد. لطفا پس از زدن دکمه تایید چند لحظه تامل فرمایید");

                    Classes.MultipleCommand multiple = new Classes.MultipleCommand("notify");
                    Classes.ClsMain.ChangeCulture("e");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dt.Rows[i][0]) >= txtstart.Value && Convert.ToInt32(dt.Rows[i][0]) <= txtend.Value)
                            Classes.clsMoshtarekin.Notify(multiple, txttozihat.Text, dt.Rows[i][0].ToString(), "ثبت قبض", dcode);

                    }

                    multiple.addToCommandCollecton("insert into moshtarek_notify_log values ('" + DateTime.Now + "'," + dcode + ",'" + txttozihat.Text + " از قرارداد" + txtstart.Value.ToString() + "  تا قرارداد:" + txtend.Value + "'," + is_ab + ")");
                    multiple.RunCommandCollecton("FrmSmsInfo - ارسال اطلاعیه به مشترکین");
                    this.moshtarek_notify_logTableAdapter.FillForSharj(this.mainDataSest.moshtarek_notify_log);
                    Classes.ClsMain.ChangeCulture("f");
                    Payam.Show("اطلاعات مربوط به این دوره با موفقیت در پنل مشترکین ثبت شد ");
                }
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtstart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtend.Focus();
        }

        private void txtend_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txttozihat.Focus();
        }

        private void txttozihat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSendInPortal.Focus();
        }
    }
}