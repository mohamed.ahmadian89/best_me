﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using System.Threading;
using System.Globalization;

namespace CWMS
{
    static class Program
    {
        public static List<string> openingWindows;
        public static FrmMain MainFrm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            openingWindows = new List<string>();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        
            MainFrm = new FrmMain();
            MyMetroForm.frmMainRefrence = MainFrm;
            Application.Run(MainFrm);
            //Application.Run(new Factor.frm_factor_abbasAbad());

            //Application.Run(new advanced_report.frm_daramad());

        }
    }
}