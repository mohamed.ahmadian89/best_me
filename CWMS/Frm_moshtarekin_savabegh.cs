using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class Frm_moshtarekin_savabegh : MyMetroForm
    {
        public Frm_moshtarekin_savabegh()
        {
            InitializeComponent();
        }

        private void FrmSearchInMoshtarekinSavabegh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.moshtarekin' table. You can move, or remove it, as needed.
            this.moshtarekinTableAdapter.Fill(this.mainDataSest.moshtarekin);

        }

        private void txtsavabegh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_savabegh.PerformClick();
        }

        private void btn_find_savabegh_Click(object sender, EventArgs e)
        {
            moshtarekinBindingSource.Filter = "savabegh like '%" + txtsavabegh.Text + "%'";
        }

        private void txtsavabegh_TextChanged(object sender, EventArgs e)
        {
            moshtarekinBindingSource.Filter = "savabegh like '%" + txtsavabegh.Text + "%'";
        }
    }
}