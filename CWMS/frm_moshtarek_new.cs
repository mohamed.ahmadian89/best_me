﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Globalization;

namespace CWMS
{
    public partial class frm_moshtarek_new : MyMetroForm
    {
        public frm_moshtarek_new()
        {
            Classes.ClsMain.ChangeCulture("f");

            InitializeComponent();
            chsakhtosaz.Checked = false;
            chtejari.Checked = false;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }





        void ClearTextBox()
        {
            ;

            //for (int i = 0; i < grp_main.Controls.Count; i++)
            //{
            //    if (grp_main.Controls[i].GetType().Name == "TextBoxX")
            //        ((TextBox)grp_main.Controls[i]).Text = "";
            //}
            //for (int i = 0; i < grp_personel.Controls.Count; i++)
            //{
            //    if (grp_personel.Controls[i].GetType().Name == "TextBoxX")
            //        ((TextBox)grp_personel.Controls[i]).Text = "";
            //}
        }

        private void FrmNewMoshtarekin_Load(object sender, EventArgs e)
        {
            txt_tarikh.SelectedDateTime=DateTime.Now;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            //int cnt = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select count(*) from moshtarekin"));
            //if (cnt > 10)
            //{
            //    Payam.Show("در نسخه دمو تنها امکان تعریف 10 مشترک وجود دارد");

            //}

            warningBox1.Visible = true;
            decimal moshtarek_metraj = 0;

            try
            {
                Convert.ToInt32(txt_gharardad.Text);
            }
            catch
            {
                warningBox1.Text = "شماره قرارداد نا معتبر است . شما مجاز به وارد کردن اعداد به عنوان شماره قرارداد هستید";
                txt_gharardad.SelectAll();
                txt_gharardad.Focus();
                return;
            }


            try
            {
                moshtarek_metraj=Convert.ToDecimal(txt_metraj.Text,CultureInfo.InvariantCulture);
            }
            catch
            {
                warningBox1.Text = "متراژ نامعتبر است در صورتی که رقم متراژ اعشاری می باشد ، لطفا از علامت نقطه استفاده کنید";
                txt_metraj.SelectAll();
                txt_metraj.Focus();
                return;
            }
            try
            {
                Convert.ToSingle(txt_zarib_fazelab.Text);

            }
            catch
            {
                warningBox1.Text = "مقدار ضریب فاضلاب باید  عدد باشد";
                txt_zarib_fazelab.SelectAll();
                txt_zarib_fazelab.Focus();
                return;
            }
            try
            {
                Convert.ToSingle(txt_havayi.Text);
            }
            catch
            {
                warningBox1.Text = "مقدار حجم مخازن هوایی باید عدد باشد";
                txt_havayi.SelectAll();
                txt_havayi.Focus();
                return;


            }
            try
            {
                Convert.ToSingle(txt_zamini.Text);
            }
            catch
            {
                warningBox1.Text = "مقدار حجم مخازن زمینی اعشاری باید باشد";
                txt_zamini.SelectAll();
                txt_zamini.Focus();
                return;
            }


            if (txt_tarikh.Text.Contains("هی"))
            {
                warningBox1.Text = "مقدار تاریخ نامعتبر می باشد . لطفا از کادر مربوطه تاریخ را انتخاب نمایید";
                txt_tarikh.Focus();
                return;
            }





            //---------------------------------------------------------
            try
            {

                if (txt_gharardad.Text.Trim() == "")
                {
                    txt_gharardad.Focus();
                    warningBox1.Text = "لطفا" + " شماره قرارداد " + " را وارد نمایید . این اطلاعات حتما باید پر شده باشد ";

                }
                else if (txt_co_name.Text.Trim() == "")
                {
                    txt_co_name.Focus();
                    warningBox1.Text = "لطفا" + " نام کارخانه / واحد تولیدی " + " را وارد نمایید . این اطلاعات حتما باید پر شده باشد ";

                }
                else if (txt_modir_amel.Text.Trim() == "")
                {
                    txt_modir_amel.Focus();
                    warningBox1.Text = "لطفا" + " نام مدیریت عامل " + " را وارد نمایید . این اطلاعات حتما باید پر شده باشد ";

                }
                else if (txt_metraj.Text.Trim() == "")
                {
                    txt_metraj.Focus();
                    warningBox1.Text = "لطفا" + " متراژ " + " را وارد نمایید . این اطلاعات حتما باید پر شده باشد ";

                }
                else if (txt_tarikh.Text.Trim() == "")
                {
                    txt_tarikh.Focus();
                    warningBox1.Text = "لطفا" + " تاریخ عقد قرارداد " + " را وارد نمایید . این اطلاعات حتما باید پر شده باشد ";

                }
                else
                {
                    string Moshtarek_coname = Classes.clsMoshtarekin.Exist_gharardad(txt_gharardad.Text);
                    if (Moshtarek_coname != "")
                    {
                        txt_gharardad.SelectAll();
                        txt_gharardad.Focus();
                        warningBox1.Text = " شماره قرارداد" + txt_gharardad.Text + "مربوط به : " + "<b>" + Moshtarek_coname + "</b>" + " می باشد. لطفا شماره قرارداد دیگری را نتخاب نمایید ";

                    }
                    else
                    {
                        int vazait = 0;
                        if (rd_faal.Checked == true)
                            vazait = 1;
                        else
                            vazait = 0;

                        Classes.clsMoshtarekin.New_moshtarek(txt_gharardad.Text, txt_modir_amel.Text, txt_co_name.Text, vazait, moshtarek_metraj, txt_havayi.Text, txt_zamini.Text, txt_masraf_mojaz.Value.ToString(), txt_size_ensheab.Value.ToString(), txt_serial_kontor.Value.ToString(), txt_zarib_fazelab.Text, txt_kargar.Value.ToString(), txt_serayedar.Value.ToString(), txt_karmand.Value.ToString(), txt_phone.Text, txt_mobile.Text, txt_tarikh.Text, txt_address.Text,chtejari.Checked,chsakhtosaz.Checked,txtKontorRaghan.Value.ToString(),txtPelak.Text,txtghataat.Text,txt_code_melli.Text,txt_code_posti.Text,txt_code_Eghtesadi.Text,txt_parvandeh.Text,txt_sh_variz_sharj.Text,txt_sh_va_ab.Text, txt_tarikh_gharar_ab.SelectedDateTime);
                        warningBox1.Visible = false;
                        Payam.Show("اطلاعات با موفقیت در سیستم ثبت شد");
                        this.Close();
                        new MainDataSestTableAdapters.moshtarekinTableAdapter().Fill(Classes.ClsMain.MoshtarekDT);

                    }






                }
            }
            catch (Exception ex)
            {
                Classes.ClsMain.LogIt(ex);
                warningBox1.Text = "بروز خطا ! خطا در سیستم ذخیره شد . لطفا با شرکت پشتیبان تماس حاصل فرمایید";
            }

        }

        private void btn_close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmNewMoshtarekin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

        }

    }
}