﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Configuration;

namespace CWMS
{
    public partial class FrmSetConnection : MyMetroForm
    {
        public FrmSetConnection()
        {
            InitializeComponent();
        }

        private void FrmSetConnection_Load(object sender, EventArgs e)
        {
//            Payam.Show("لطفا تا اتمام عملیات بروزرسانی چند لحظه ای صبر نمایید");
//           string command= @"
//update Entezamat
//  set  block=ServerAsl.block
//from [10.1.1.2].cwms.dbo.barge_khorooj_details as ServerAsl
// inner join 
// [7-PC].cwms.dbo.barge_khorooj_details as Entezamat
//  on Entezamat.id=ServerAsl.id


//insert into [7-PC].cwms.dbo.barge_khorooj_details
//select * from [10.1.1.2].cwms.dbo.barge_khorooj_details
//where gharardad not in (select gharardad from [7-PC].cwms.dbo.barge_khorooj_details)

//update ServerAsl
////  set  used=Entezamat.used,tarikh=Entezamat.tarikh
//from [7-PC].cwms.dbo.barge_khorooj_details as Entezamat
// inner join 
// [10.1.1.2].cwms.dbo.barge_khorooj_details as ServerAsl
//  on Entezamat.id=ServerAsl.id

//";
//            Classes.ClsMain.ExecuteNoneQuery(command);
//            Payam.Show("عملیات به روز رسانی با موفقیت انجام شد");
        
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            string connection = "";
            if (txtIP.Text == "majid2560")
            {
                connection = "Data Source=10.1.1.2;Initial Catalog=cwms;Integrated Security=false;user id=sa;password=123456";
            }
            else if(txtIP.Text== "es4475")
            {
                connection = "Data Source=DATIS-PC\\ESFBOZORG;Initial Catalog=cwms;Integrated Security=true";
            }
            else if(txtIP.Text== "ja6578")
            {
                connection = "Data Source=.;Initial Catalog=cwms;Integrated Security=true";
            }
            else
            {

                if (ch_intg.Checked)
                {
                    connection = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=true";

                }
                else
                {
                    connection = "Data Source=" + txtIP.Text + ";Initial Catalog=" + txt_db_name.Text + ";Integrated Security=false;user id=" + Txtusername.Text + ";password=" + txtpassword.Text;

                }
            }




            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStrings = config.ConnectionStrings;
            for (int i = 0; i < connectionStrings.ConnectionStrings.Count; i++)
            {
                connectionStrings.ConnectionStrings[i].ConnectionString = connection;
            }

            config.Save(ConfigurationSaveMode.Modified);
         
            this.Close();

        }

        private void labelX18_Click(object sender, EventArgs e)
        {

        }

        private void buttonX9_Click(object sender, EventArgs e)
        {

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {

        }

        private void txtpassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX1.PerformClick();
        }

        private void Txtusername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtpassword.Focus();
        }

        private void txtIP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Txtusername.Focus();
        }

        private void ch_intg_CheckedChanged(object sender, EventArgs e)
        {
            if (ch_intg.Checked)
            {
                txtpassword.Enabled = Txtusername.Enabled = false;
            }
            else
                txtpassword.Enabled = Txtusername.Enabled = true;

        }
    }
}