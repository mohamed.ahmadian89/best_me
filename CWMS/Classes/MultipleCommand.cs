﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CWMS.Classes
{
   public class MultipleCommand
    {
        string commandLis = "", TransactonName;
        public MultipleCommand()
        {
            TransactonName = "start";
        }

        public MultipleCommand(string transName)
        {
            TransactonName = transName;
        }
        public void addToCommandCollecton(string command)
        {
            if (command.Trim().EndsWith(";") == false)
                command += ";";
            commandLis += command;

        }
        public void ResetCommandCollecton()
        {
            commandLis = "";
        }

        public bool RunCommandCollecton(string Location)
        {
            Classes.ClsMain.ChangeCulture("e");
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            con.ConnectionString = ClsMain.ConnectionStr;
            con.Open();
            SqlTransaction transaction = con.BeginTransaction(TransactonName);
            com.Connection = con;
            com.Transaction = transaction;
            try
            {
                //------------------------------------------------------------------
                string[] CommandParted = commandLis.Split(';');
                string tempCommand = "";
                for (int i = 0; i < CommandParted.Length; i++)
                {
                    tempCommand += CommandParted[i] + ";";
                    if (i % 400 == 0)
                    {
                        com.CommandText = tempCommand;
                        com.ExecuteNonQuery();
                        tempCommand = "";
                    }
                }
                if (tempCommand != "")
                {
                    com.CommandText = tempCommand;
                    com.ExecuteNonQuery();
                }


                transaction.Commit();
                con.Close();
                Classes.ClsMain.ChangeCulture("f");

                return true;
            }
            catch (Exception eee)
            {
                transaction.Rollback();
                Classes.ClsMain.logError(eee, Location);
                Classes.ClsMain.ChangeCulture("f");

                return false;

            }

            //------------------------------------------------------------------

        }
    }
}
