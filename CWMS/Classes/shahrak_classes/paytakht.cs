﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FarsiLibrary.Utils;

namespace CWMS.Classes.shahrak_classes
{





    public class paytakht
    {

        public static void Gozaresh_asnad_hesabdari_ab(int ab_dcode)
        {
            CrysReports.paytakht.ab_doreh_hesabdari report1 = new CrysReports.paytakht.ab_doreh_hesabdari();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_ab_report(ds.moshtarekin_for_report, ab_dcode);
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.ab_ghabz, ab_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }


    


        public static void Gozaresh_asnad_hesabdari_sharj(int sharj_dcode)

        {
            CrysReports.paytakht.sharj_doreh_hesabdari report1;
            report1 = new CrysReports.paytakht.sharj_doreh_hesabdari();

            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_sharj_report(ds.moshtarekin_for_report, sharj_dcode);
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.sharj_ghabz, sharj_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }

        public static void Gozaresh_karbordi_sharj(int sharj_dcode)
        {
            CrysReports.paytakht.sharj_doreh_report report1 = new CrysReports.paytakht.sharj_doreh_report();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_sharj_report(ds.moshtarekin_for_report, sharj_dcode);
            Gozaresh_karbordi_sharj_Persian_date(ds, "tarikh_gharardad");
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.sharj_ghabz, sharj_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }



        static void Gozaresh_karbordi_sharj_Persian_date(CrysReports.ReportDataSet ds, string field)
        {
            DataTable dt_moshtarekin_temp = ds.moshtarekin_for_report.CopyToDataTable();
            ds.moshtarekin_for_report.Columns.Remove(field);
            ds.moshtarekin_for_report.Columns.Add(field);

            for (int i = 0; i < ds.moshtarekin_for_report.Rows.Count; i++)
            {
                try
                {
                    ds.moshtarekin_for_report.Rows[i][field] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_moshtarekin_temp.Rows[i][field])).ToString("d");
                }
                catch (Exception)
                {
                    ds.moshtarekin_for_report.Rows[i][field] = "";
                }
            }
        }
        public static void Gozaresh_karbordi_ab(int ab_dcode)
        {
            CrysReports.paytakht.ab_doreh_report report1 = new CrysReports.paytakht.ab_doreh_report();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_ab_report(ds.moshtarekin_for_report, ab_dcode);
            Gozaresh_karbordi_sharj_Persian_date(ds, "tarikh_gharardad_ab");
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.ab_ghabz, ab_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }











        public static void Calculate_hazineh_masraf(bool dar_hal_sakht, int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab,
   ref decimal mablagh_mojaz, ref decimal mablagh_gheir_mojaz, ref decimal hazineh_mablagh, DataTable Jarime_masraf_mazad
     )
        {
            //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------

            mablagh_mojaz = masraf_mojaz * tarefe_ab;
            mablagh_gheir_mojaz = Convert.ToDecimal(Get_Masrafe_gheir_mojaz_hazineh(mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, Jarime_masraf_mazad));
            hazineh_mablagh = mablagh_mojaz + mablagh_gheir_mojaz;



        }
        public static long Get_Masrafe_gheir_mojaz_hazineh(int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab, DataTable Jarime_masraf_mazad)
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]) * masraf_gheir_mojaz * tarefe_ab;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(masraf_gheir_mojaz), Convert.ToDecimal(mizan_masraf), Jarime_masraf_mazad, tarefe_ab));

        }
        public static decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return GetRoundedInteger(sum);
        }



        public static void Moshtarek_ensheab_hazineh(int mizan_masraf, decimal mablagh_fazelab_doreh, decimal zarib_Aboonman, ref decimal hazineh_ensheab, bool is_updating_ghabz)
        {
            // میزان کل ممصرف کاربر 
            // ضرب در ضریب ابونمان 
            // ضرب در مبلغ فاضلاب
            //hazineh_ensheab = Convert.ToDecimal(Dt_Hazineh_ensheab.Rows[Dt_Hazineh_ensheab.Rows.Count - 1]["hazineh"]);
            //if (is_updating_ghabz == false)
            //{
            hazineh_ensheab = GetRoundedInteger(mizan_masraf * mablagh_fazelab_doreh * zarib_Aboonman);
            //}
        }
        public static decimal GetRoundedInteger(decimal number)
        {
            return Convert.ToDecimal(Math.Round(number));
        }
        public static void Calculate_masraf_omoomi(ref decimal hazineh_masraf_omoomi, decimal mablagh_masraf_omoomi, decimal metraj)
        {
            // متراژ 
            // ضرب در 
            // هزینه مصرف عمومی

            hazineh_masraf_omoomi = GetRoundedInteger(mablagh_masraf_omoomi * metraj);
        }
        public static void Calculate_Fazelab(ref decimal hazineh_fazelab, int mizan_masraf, decimal zarib_fazelab, decimal mablagh_fazelab_doreh)
        {
            // میزان کل مصرف مجاز و غیر و مجاز باهم
            // ضرب در ضریب فاضلاب به ازا هر شخص
            // ضرب در مبلغ فاضلاب تعریف شده در دوره

            hazineh_fazelab = GetRoundedInteger(mizan_masraf * zarib_fazelab * mablagh_fazelab_doreh);
        }

        public static void Get_hazineh_Sharj(ref decimal hazineh_mablagh, DateTime doreh_start_time, decimal Tarefe, decimal metraj, int tedad_rooz)
        {
            int TedadRoozHayeSal = new FarsiLibrary.Utils.PersianCalendar().GetDaysInYear(
                                   new PersianDate(doreh_start_time).Year);
            hazineh_mablagh = Math.Round(metraj * tedad_rooz * ((decimal)Tarefe / TedadRoozHayeSal));
        }


    }
}
