﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CrystalDecisions.Windows.Forms;


namespace CWMS.Classes.shahrak_classes
{
    public class abbas_abad_factor
    {
        public DataTable dt_ghabz;
        public int index, Factor_ID, end_gharardad, start_gharardad, dcode, gcode, gharardad, ghabz_type, operation_count, success, fail, has_pardakhti;

        public decimal takhfif, darsad_maliat, majmu_mabalegh, maliat, mablaghkol;
        public DateTime tarikh;
        public string table_name, sharh;
        public List<string> error_gharardad;
        public MultipleCommand multi;
        public static int Get_Max_factor_num()
        {
            return Convert.ToInt32(ClsMain.ExecuteScalar("select isnull((select max(factor) from factors),0)+1"));
        }

        public static bool Has_factor(string gcode)
        {
            int cnt = Convert.ToInt32(ClsMain.ExecuteScalar("select count(*) from factors where gcode=" + gcode));
            return (cnt == 0 ? false : true);
        }


        public abbas_abad_factor()
        {
            takhfif = darsad_maliat = majmu_mabalegh = maliat = mablaghkol = 0;
            index = Factor_ID = operation_count = end_gharardad = start_gharardad = dcode = gcode = gharardad = ghabz_type = success = fail = has_pardakhti = 0;
            table_name = "";
            error_gharardad = new List<string>();
        }

        public abbas_abad_factor(int Factor_ID_t, int start_gharardad_t, int end_gharardad_t, int dcode_t,
            decimal takhfif_t, decimal darsad_maliat_t, string table_name_t, int ghabz_type_t, string sharh_t
            )
        {
            Factor_ID = Factor_ID_t;
            start_gharardad = start_gharardad_t;
            end_gharardad = end_gharardad_t;
            dcode = dcode_t;
            takhfif = takhfif_t;

            darsad_maliat = darsad_maliat_t;
            table_name = table_name_t;
            ghabz_type = ghabz_type_t;
            sharh = sharh_t;
            error_gharardad = new List<string>();

        }



        public static void Check_ab_factor(clsGhabz ghabzInfo, string table_name, int ghabz_type)
        {
            //================== فاکتور ============================
            DataTable dt_ghabz_info = Classes.ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + ghabzInfo.gcode);
            string khadamat_name = "آب";

            Classes.clsFactor factor_class = new Classes.clsFactor(
                Classes.clsFactor.Get_Max_factor_num(), Convert.ToInt32(ghabzInfo.gharardad),
                Convert.ToInt32(ghabzInfo.gharardad), Convert.ToInt32(ghabzInfo.doreh),
                0, ghabzInfo.darsad_maliat, table_name, ghabz_type, "ارائه خدمات " + khadamat_name + " دوره" + ghabzInfo.doreh);
            factor_class.dt_ghabz = dt_ghabz_info;
            factor_class.tarikh = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select tarikh from factors where gcode=" + ghabzInfo.gcode));
            factor_class.Start_genete_ghabz();
            //================== فاکتور ============================
        }



        public static void Check_sharj_factor(clsSharjGhabz ghabzInfo, string table_name, int ghabz_type)
        {
            //================== فاکتور ============================

            DataTable dt_ghabz_info = Classes.ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + ghabzInfo.gcode);
            string khadamat_name = "شارژ";

            Classes.clsFactor factor_class = new Classes.clsFactor(
                Classes.clsFactor.Get_Max_factor_num(), Convert.ToInt32(ghabzInfo.gharardad),
                Convert.ToInt32(ghabzInfo.gharardad), Convert.ToInt32(ghabzInfo.doreh),
                0, ghabzInfo.darsad_maliat, table_name, ghabz_type, "ارائه خدمات " + khadamat_name + " دوره" + ghabzInfo.doreh);
            factor_class.dt_ghabz = dt_ghabz_info;
            factor_class.tarikh = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select tarikh from factors where gcode=" + ghabzInfo.gcode));
            factor_class.Start_genete_ghabz();
            //================== فاکتور ============================
        }


        public void Start_genete_ghabz()
        {
            for (index = 0; index < dt_ghabz.Rows.Count; index++)
            {
                Clear_all();
                init_factor_fields();
                calc_Majmu_mabalegh();
                if (Check_has_pardakhti() == false)
                    continue;
                calc_Maliat();
                calc_Mablghkol();
                generate_Factor();

            }
        }





        public void Clear_all()
        {
            majmu_mabalegh = maliat = mablaghkol = 0;
            gcode = gharardad = has_pardakhti = 0;
            error_gharardad.Clear();
        }

        public bool Check_has_pardakhti()
        {
            // به طور کلی قبوضی که از دوره قبل دارای بستانکاری باشند
            // حتی اگر در دوره فعلی هیچ پرداختی برای آنها ثبت نشده باشد 
            // وضعیت قبض آنها به صورت پرداخت جزئی خواهد بود 
            // همچنین فیلد زیر نیز در صورت داشتن هرگونه پرداختی
            // حال چه از طریق پنل پرداخت ها و چه از طریق بستانکاری از دوره قبل
            // دارای مقدار درست - ترو خواهد بود
            int cnt = Convert.ToInt32(
                Classes.ClsMain.ExecuteScalar(
                    "select count(*) from ghabz_pardakht where gcode=" + gcode.ToString()
                    )
                );
            if (cnt > 0)
            {
                has_pardakhti = 1;
                try
                {
                    tarikh = Convert.ToDateTime(ClsMain.ExecuteScalar("select top(1) tarikh_vosool from ghabz_pardakht where gcode=" + gcode + " order by tarikh_vosool asc"));

                }
                catch (Exception ert)
                {
                    Classes.ClsMain.logError(ert);
                    Classes.ClsMain.ExecuteNoneQuery("insert into errors (errorText) values ('" + gcode + "')");
                    // در صورت نداشتن مقداری در جدول پرداختی ها
                    // یعنی در حالتی که قبض به صورت بستانکاری پرداخت شده باشد
                    // تاریخ صدور قبض به عنوان تاریخ فاکتور در نظر گرفته می شود
                    tarikh = Convert.ToDateTime(dt_ghabz.Rows[index]["tarikh"]);
                }
            }

            return (cnt == 0 ? false : true);

        }

        public void init_factor_fields()
        {
            gharardad = Convert.ToInt32(dt_ghabz.Rows[index]["gharardad"]);
            has_pardakhti = 0;
            gcode = Convert.ToInt32(dt_ghabz.Rows[index]["gcode"]);
            if (ghabz_type == 0)
                sharh = "ارائه خدمات آب دوره  " + dcode;
            else
                sharh = "ارائه خدمات شارژ دوره  " + dcode;

        }

        public void get_All_Ghabz()
        {
            dt_ghabz = Classes.ClsMain.GetDataTable("select * from " + table_name + " where dcode=" + dcode + " and  gharardad>=" + start_gharardad + " and gharardad<=" + end_gharardad);
        }

        public void calc_Majmu_mabalegh()
        {
            if (ghabz_type == 1)
            {
                int dcode_counter = dcode;
                majmu_mabalegh =
                       Convert.ToDecimal(dt_ghabz.Rows[index]["mablagh"]) + Convert.ToDecimal(dt_ghabz.Rows[index]["sayer"]) + Convert.ToDecimal(dt_ghabz.Rows[index]["jarimeh"]);
                dcode_counter--;
                DataTable dt_ghabz_from_Previos_doreh;
                while (dcode_counter >= 1)
                {
                    int has_factor_in_doreh =
                        Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select count(*) from factors where dcode=" + dcode_counter + " and gharardad=" + gharardad + " and ghabz_type=1"));
                    if (has_factor_in_doreh == 0)
                    {
                        dt_ghabz_from_Previos_doreh = Classes.ClsMain.GetDataTable("select * from sharj_ghabz where dcode=" + dcode_counter + " and  gharardad=" + gharardad);
                        if (dt_ghabz_from_Previos_doreh.Rows.Count != 0)
                        {
                            majmu_mabalegh +=
                           Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["mablagh"]) + Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["sayer"]) + Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["jarimeh"]) ;
                            sharh += "--" + dcode_counter;
                        }
                    }
                    else
                        break;

                    dcode_counter--;
                }




            }
            else
            {
                int ab_dcode_counter = dcode;
                majmu_mabalegh =
                    Convert.ToDecimal(dt_ghabz.Rows[index]["mablagh"]) +
                    Convert.ToDecimal(dt_ghabz.Rows[index]["sayer"]) +
                    Convert.ToDecimal(dt_ghabz.Rows[index]["jarimeh"]) +
                    Convert.ToDecimal(dt_ghabz.Rows[index]["aboonmah"]) +
                    Convert.ToDecimal(dt_ghabz.Rows[index]["masraf_omoomi"]) +
                    Convert.ToDecimal(dt_ghabz.Rows[0]["hazineh_taviz_kontor"]) + 
                    Convert.ToDecimal(dt_ghabz.Rows[index]["fazelab"]);



                ab_dcode_counter--;
                DataTable dt_ghabz_from_Previos_doreh;
                while (ab_dcode_counter >= 1)
                {
                    int has_factor_in_doreh =
                        Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select count(*) from factors where dcode=" + ab_dcode_counter + " and gharardad=" + gharardad + " and ghabz_type=0"));
                    if (has_factor_in_doreh == 0)
                    {
                        dt_ghabz_from_Previos_doreh = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + ab_dcode_counter + " and  gharardad=" + gharardad);
                        if (dt_ghabz_from_Previos_doreh.Rows.Count != 0)
                        {
                            majmu_mabalegh +=
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["mablagh"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["sayer"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["jarimeh"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["aboonmah"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["masraf_omoomi"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["hazineh_taviz_kontor"]) +
                            Convert.ToDecimal(dt_ghabz_from_Previos_doreh.Rows[0]["fazelab"]);
                            sharh += "--" + ab_dcode_counter;
                        }
                    }
                    else
                        break;

                    ab_dcode_counter--;
                }

            }
        }
        public void calc_Maliat()
        {
            maliat = Math.Round(Convert.ToDecimal((majmu_mabalegh - takhfif) * darsad_maliat / 100));
        }

        public void calc_Mablghkol()
        {
            mablaghkol = maliat + majmu_mabalegh - takhfif;
        }


        public void Factor_generation_list()
        {
            Classes.ClsMain.ChangeCulture("e");
            string sal = new FarsiLibrary.Utils.PersianDate(tarikh).Year.ToString().Substring(2);

            string exist_factor_id =
                Classes.ClsMain.ExecuteScalar("select isnull((select factor from factors where gcode=" + gcode + "),-1)").ToString();
            int temp_Factor_ID = Factor_ID;
            string deleteCommand = "";
            // اگر فاکتور قبلا صادر شده باشد در نتیجه  می بایست ان را حذف و مجددا صادر کرد
            if (exist_factor_id != "-1")
            {
                Factor_ID = Convert.ToInt32(exist_factor_id);
                deleteCommand = "delete from factors where gcode=" + gcode + ";";
            }

            multi.addToCommandCollecton(deleteCommand + "insert into factors ([factor],[sal],[gharardad],[dcode],[gcode]" +
        ",[tarikh],[darsad_maliat],[majmu_mabalegh],[takhfif]" +
        ",[maliat],[mablaghkol],[sharh],[has_pardakhti],[ghabz_type]) values (" +

         +Factor_ID + "," + sal + "," + gharardad + "," + dcode + "," + gcode + ",'" +
         tarikh + "'," + darsad_maliat + "," + majmu_mabalegh + "," + takhfif + "," + maliat + "," + mablaghkol +
          ",'" + sharh + "'," + has_pardakhti + "," + ghabz_type + ")");




        }


        public void generate_Factor()
        {
            Classes.ClsMain.ChangeCulture("e");
            string sal = new FarsiLibrary.Utils.PersianDate(tarikh).Year.ToString().Substring(2);

            try
            {
                string exist_factor_id =
                    Classes.ClsMain.ExecuteScalar("select isnull((select factor from factors where gcode=" + gcode + "),-1)").ToString();
                int temp_Factor_ID = Factor_ID;
                string deleteCommand = "";
                // اگر فاکتور قبلا صادر شده باشد در نتیجه  می بایست ان را حذف و مجددا صادر کرد
                if (exist_factor_id != "-1")
                {
                    Factor_ID = Convert.ToInt32(exist_factor_id);
                    deleteCommand = "delete from factors where gcode=" + gcode + ";";
                }

                ClsMain.ExecuteNoneQuery(deleteCommand + "insert into factors ([factor],[sal],[gharardad],[dcode],[gcode]" +
           ",[tarikh],[darsad_maliat],[majmu_mabalegh],[takhfif]" +
           ",[maliat],[mablaghkol],[sharh],[has_pardakhti],[ghabz_type]) values (" +

            +Factor_ID + "," + sal + "," + gharardad + "," + dcode + "," + gcode + ",'" +
            tarikh + "'," + darsad_maliat + "," + majmu_mabalegh + "," + takhfif + "," + maliat + "," + mablaghkol +
             ",N'" + sharh + "'," + has_pardakhti + "," + ghabz_type + ")");

                success++;
                if (exist_factor_id == "-1")
                {

                    Factor_ID++;
                }
                else
                {
                    Factor_ID = temp_Factor_ID;
                }

            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert);
                fail++;
                error_gharardad.Add(gharardad.ToString());
            }
            operation_count++;

        }






        public static void factor_history(string dcode, string ghabz_type, string tozihat, DateTime tarikh, string start, string finish, int success, int fail)
        {

            Classes.ClsMain.ExecuteNoneQuery("insert into factor_history values ('" + tarikh + "'," + dcode + ",'" + ghabz_type + "','" + tozihat + "'," + start + "," + finish + "," + success + "," + fail + ")");
        }




        public static void Print_Factors(CWMS.Management.user_Dataset uds, System.Windows.Forms.ProgressBar sharj_progress)
        {
            try
            {


                long sharj_takhfif = 0;
                long ab_takhfif = 0;
                int gharardad = 0, sharj_doreh, ab_doreh;

                sharj_progress.Minimum = 0;
                sharj_progress.Maximum = uds.factor.Rows.Count;
                sharj_progress.Value = 0;

                decimal total_sharj_mabalegh_first = 0, sharj_maliat = 0, sharj_mablaghkol = 0;
                decimal ab_maliat = 0, total_ab_mabalegh_first = 0, ab_mablaghkol = 0;
                int Factor_ID = 0;
                string sharj_sharh = "", ab_sharh = "";
                CrystalReportViewer viewer = new CrystalReportViewer();
                CrysReports.Sharj_ab_factor_by_bg
                report1 = new CrysReports.Sharj_ab_factor_by_bg();
                DataTable dt_moshtarek_info;
                for (int i = 0; i < uds.factor.Rows.Count; i++)
                {
                    try
                    {
                        gharardad = Convert.ToInt32(uds.factor.Rows[i]["gharardad"]);
                        sharj_doreh = Convert.ToInt32(uds.factor.Rows[i]["sharj_dcode"]);
                        sharj_sharh = uds.factor.Rows[i]["sharj_sharh"].ToString();
                        sharj_maliat = Convert.ToDecimal(uds.factor.Rows[i]["sharj_maliat"]);
                        sharj_mablaghkol = Convert.ToDecimal(uds.factor.Rows[i]["sharj_mablaghkol"]);
                        ab_doreh = Convert.ToInt32(uds.factor.Rows[i]["ab_dcode"]);
                        ab_sharh = uds.factor.Rows[i]["ab_sharh"].ToString();
                        ab_maliat = Convert.ToDecimal(uds.factor.Rows[i]["ab_maliat"]);
                        ab_mablaghkol = Convert.ToDecimal(uds.factor.Rows[i]["ab_mablaghkol"]);

                        total_sharj_mabalegh_first = Convert.ToDecimal(uds.factor.Rows[i]["sharj_majmu_mabalegh"]);
                        total_ab_mabalegh_first = Convert.ToDecimal(uds.factor.Rows[i]["ab_majmu_mabalegh"]);

                        sharj_takhfif = Convert.ToInt64(uds.factor.Rows[i]["sharj_takhfif"]);
                        ab_takhfif = Convert.ToInt64(uds.factor.Rows[i]["ab_takhfif"]);

                        Factor_ID = Convert.ToInt32(uds.factor.Rows[i]["factor"]);



                        dt_moshtarek_info = Classes.ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);




                        report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(uds.factor.Rows[i]["tarikh"])).ToString("d"));
                        report1.SetParameterValue("sharj_sharh", sharj_sharh);

                        report1.SetParameterValue("sharj_doreh", sharj_doreh);
                        report1.SetParameterValue("gharardad", gharardad);


                        report1.SetParameterValue("m_co_name", dt_moshtarek_info.Rows[0]["co_name"].ToString());
                        report1.SetParameterValue("m_shomare_eghtesadi", dt_moshtarek_info.Rows[0]["code_eghtesadi"].ToString());
                        report1.SetParameterValue("m_shenase_melli", dt_moshtarek_info.Rows[0]["code_melli"].ToString());
                        report1.SetParameterValue("m_code_posti", dt_moshtarek_info.Rows[0]["code_posti"].ToString());

                        report1.SetParameterValue("m_address", dt_moshtarek_info.Rows[0]["address"].ToString());
                        report1.SetParameterValue("m_phone", dt_moshtarek_info.Rows[0]["phone"].ToString());


                        //-------------------------Sharj--------------------------------------


                        report1.SetParameterValue("sharj_mablaghkol_first", total_sharj_mabalegh_first);
                        report1.SetParameterValue("sharj_takhfif", sharj_takhfif);
                        report1.SetParameterValue("sharj_mablaghkol_pasaz_takhfif", total_sharj_mabalegh_first - sharj_takhfif);
                        report1.SetParameterValue("sharj_maliat", sharj_maliat);
                        report1.SetParameterValue("sharj_total_mabalegh", sharj_mablaghkol);


                        //-------------------------ab--------------------------------------
                        report1.SetParameterValue("ab_sharh", ab_sharh);
                        report1.SetParameterValue("ab_doreh", ab_doreh);


                        report1.SetParameterValue("ab_mablaghkol_first", total_ab_mabalegh_first);
                        report1.SetParameterValue("ab_takhfif", ab_takhfif);
                        report1.SetParameterValue("ab_mablaghkol_pasaz_takhfif", total_ab_mabalegh_first - ab_takhfif);
                        report1.SetParameterValue("ab_maliat", ab_maliat);
                        report1.SetParameterValue("ab_total_mabalegh", ab_mablaghkol);

                        report1.SetParameterValue("total_Mableghghabazmaliat", total_sharj_mabalegh_first + total_ab_mabalegh_first);

                        report1.SetParameterValue("total_maliat", sharj_maliat + ab_maliat);

                        report1.SetParameterValue("total_takhfifi", sharj_takhfif + ab_takhfif);
                        report1.SetParameterValue("total_jam_factor", total_sharj_mabalegh_first + total_ab_mabalegh_first - (sharj_takhfif + ab_takhfif));


                        decimal Total_total_total = sharj_mablaghkol + ab_mablaghkol;

                        report1.SetParameterValue("total_total",
                         Total_total_total
                            );


                        report1.SetParameterValue("total_horoof", Classes.clsNumber.Number_to_Str((Total_total_total - (Total_total_total % 1000)).ToString()));
                        report1.SetParameterValue("kasr_hezar", (Total_total_total % 1000));
                        report1.SetParameterValue("factor_number", Factor_ID);



                        viewer.ReportSource = report1;

                        report1.PrintToPrinter(1, true, 0, 0);
                        sharj_progress.Value += 1;


                    }
                    catch (Exception ert)
                    {
                        Payam.Show("خطا در صدور فاکتور");
                        Classes.ClsMain.logError(ert);
                        continue;
                    }

                }


            }
            catch (Exception ert)
            {

                ClsMain.logError(ert);
                Payam.Show("متاسفانه سیستم دچار مشکل شده است . لطفا باشرکت پشتیبان تماس حاصل فرمایید");


                throw;
            }
        }


        //public static void Print_Factor(int start_or_ab, int end_or_sharj, System.Windows.Forms.ProgressBar sharj_progress, string type)
        //{

        //    // این نسخه در واقع دارای پس زمینه است که اطلاعات بر روی فاکتور پیش فرض نوشته می شود
        //    // در این نسخه فرض بر این است که کاربر اصلا بدهکاری ندارد و صرفا کتور بر مبنای هزینه های دوری جاری می باشد
        //    try
        //    {

        //        int sharj_doreh = 0;
        //        int ab_doreh = 0;


        //        CWMS.Management.user_Dataset uds = new Management.user_Dataset();
        //        CWMS.Management.user_DatasetTableAdapters.factorTableAdapter ta = new Management.user_DatasetTableAdapters.factorTableAdapter();

        //        if (type == "ByCode")
        //            ta.FillByDoreh(uds.factor,start_or_ab, end_or_sharj);
        //        else
        //            ta.FillByFactor(uds.factor, start_or_ab, end_or_sharj);


        //        long sharj_takhfif = 0;
        //        long ab_takhfif = 0;
        //        int gharardad = 0;

        //        sharj_progress.Minimum = 0;
        //        sharj_progress.Maximum = uds.factor.Rows.Count;
        //        sharj_progress.Value = 0;

        //        decimal total_sharj_mabalegh_first = 0, sharj_maliat = 0;
        //        decimal ab_maliat = 0, total_ab_mabalegh_first = 0;
        //        int Factor_ID = 0;
        //        string sharj_sharh = "", ab_sharh = "";
        //        CrystalReportViewer viewer = new CrystalReportViewer();
        //        CrysReports.Sharj_ab_factor_by_bg
        //        report1 = new CrysReports.Sharj_ab_factor_by_bg();
        //        DataTable dt_moshtarek_info;
        //        for (int i = 0; i < uds.factor.Rows.Count; i++)
        //        {
        //            try
        //            {
        //                gharardad = Convert.ToInt32(uds.factor.Rows[i]["gharardad"]);
        //                sharj_doreh = Convert.ToInt32(uds.factor.Rows[i]["sharj_dcode"]);
        //                ab_doreh = Convert.ToInt32(uds.factor.Rows[i]["ab_dcode"]);
        //                sharj_sharh = uds.factor.Rows[i]["sharj_sharh"].ToString();
        //                ab_sharh = uds.factor.Rows[i]["ab_sharh"].ToString();
        //                total_sharj_mabalegh_first = Convert.ToDecimal(uds.factor.Rows[i]["sharj_majmu_mabalegh"]);
        //                total_ab_mabalegh_first = Convert.ToDecimal(uds.factor.Rows[i]["ab_majmu_mabalegh"]);

        //                sharj_takhfif = Convert.ToInt64(uds.factor.Rows[i]["sharj_takhfif"]);
        //                ab_takhfif = Convert.ToInt64(uds.factor.Rows[i]["ab_takhfif"]);

        //                Factor_ID = Convert.ToInt32(uds.factor.Rows[i]["factor"]);



        //                dt_moshtarek_info = Classes.ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);




        //                report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(uds.factor.Rows[i]["tarikh"])).ToString("d"));
        //                report1.SetParameterValue("sharj_sharh", sharj_sharh);

        //                report1.SetParameterValue("sharj_doreh", sharj_doreh);
        //                report1.SetParameterValue("gharardad", gharardad);


        //                report1.SetParameterValue("m_co_name", dt_moshtarek_info.Rows[0]["co_name"].ToString());
        //                report1.SetParameterValue("m_shomare_eghtesadi", dt_moshtarek_info.Rows[0]["code_eghtesadi"].ToString());
        //                report1.SetParameterValue("m_shenase_melli", dt_moshtarek_info.Rows[0]["code_melli"].ToString());
        //                report1.SetParameterValue("m_code_posti", dt_moshtarek_info.Rows[0]["code_posti"].ToString());

        //                report1.SetParameterValue("m_address", dt_moshtarek_info.Rows[0]["address"].ToString());
        //                report1.SetParameterValue("m_phone", dt_moshtarek_info.Rows[0]["phone"].ToString());


        //                //-------------------------Sharj--------------------------------------


        //                report1.SetParameterValue("sharj_mablaghkol_first", total_sharj_mabalegh_first);
        //                report1.SetParameterValue("sharj_takhfif", sharj_takhfif);
        //                report1.SetParameterValue("sharj_mablaghkol_pasaz_takhfif", total_sharj_mabalegh_first - sharj_takhfif);
        //                sharj_maliat = Math.Round(Convert.ToDecimal((total_sharj_mabalegh_first - sharj_takhfif) * 9 / 100));
        //                report1.SetParameterValue("sharj_maliat", sharj_maliat);
        //                report1.SetParameterValue("sharj_total_mabalegh", sharj_maliat + total_sharj_mabalegh_first - sharj_takhfif);


        //                //-------------------------ab--------------------------------------
        //                report1.SetParameterValue("ab_sharh", ab_sharh);
        //                report1.SetParameterValue("ab_doreh", ab_doreh);


        //                report1.SetParameterValue("ab_mablaghkol_first", total_ab_mabalegh_first);
        //                report1.SetParameterValue("ab_takhfif", ab_takhfif);
        //                report1.SetParameterValue("ab_mablaghkol_pasaz_takhfif", total_ab_mabalegh_first - ab_takhfif);
        //                ab_maliat = Math.Round(Convert.ToDecimal((total_ab_mabalegh_first - ab_takhfif) * 9 / 100));
        //                report1.SetParameterValue("ab_maliat", ab_maliat);
        //                report1.SetParameterValue("ab_total_mabalegh", ab_maliat + total_ab_mabalegh_first - ab_takhfif);

        //                report1.SetParameterValue("total_Mableghghabazmaliat", total_sharj_mabalegh_first + total_ab_mabalegh_first);

        //                report1.SetParameterValue("total_maliat", sharj_maliat + ab_maliat);

        //                report1.SetParameterValue("total_takhfifi", sharj_takhfif + ab_takhfif);
        //                report1.SetParameterValue("total_jam_factor", total_sharj_mabalegh_first + total_ab_mabalegh_first - (sharj_takhfif + ab_takhfif));


        //                report1.SetParameterValue("total_total",

        //                    (total_sharj_mabalegh_first + total_ab_mabalegh_first) +
        //                    (sharj_maliat + ab_maliat) -
        //                    (sharj_takhfif + ab_takhfif)

        //                    );
        //                report1.SetParameterValue("factor_number", Factor_ID);



        //                viewer.ReportSource = report1;

        //                report1.PrintToPrinter(1, true, 0, 0);
        //                sharj_progress.Value += 1;


        //            }
        //            catch (Exception ert)
        //            {
        //                Classes.ClsMain.logError(ert);
        //                continue;
        //            }

        //        }


        //    }
        //    catch (Exception ert)
        //    {

        //        ClsMain.logError(ert);
        //        Payam.Show("متاسفانه سیستم دچار مشکل شده است . لطفا باشرکت پشتیبان تماس حاصل فرمایید");


        //        throw;
        //    }


        //}

    }
}
