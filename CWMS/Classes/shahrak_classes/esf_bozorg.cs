﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FarsiLibrary.Utils;

namespace CWMS.Classes.shahrak_classes
{
    public class esf_bozorg
    {

        public static void Gozaresh_asnad_hesabdari_ab(int ab_dcode)
        {
            CrysReports.esf_bozorg.ab_doreh_hesabdari_bedehkar report1 = new CrysReports.esf_bozorg.ab_doreh_hesabdari_bedehkar();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_ab_report(ds.moshtarekin_for_report, ab_dcode);
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.ab_ghabz, ab_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("doreh_mah_sal", get_dorh_mah_sal("ab_doreh", ab_dcode.ToString()));


            new FrmShowReport(report1).ShowDialog();

            CrysReports.esf_bozorg.ab_doreh_hesabdari_bestankar report_bestankar = new CrysReports.esf_bozorg.ab_doreh_hesabdari_bestankar();
            report_bestankar.SetDataSource(ds);
            report_bestankar.SetParameterValue("doreh_mah_sal", get_dorh_mah_sal("ab_doreh", ab_dcode.ToString()));

            new FrmShowReport(report_bestankar).ShowDialog();
        }

        public static void Gozaresh_asnad_hesabdari_sharj(int sharj_dcode)

        {
            CrysReports.esf_bozorg.sharj_pishnevis_bedehkar report1;
            report1 = new CrysReports.esf_bozorg.sharj_pishnevis_bedehkar();

            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_sharj_report(ds.moshtarekin_for_report, sharj_dcode);
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.sharj_ghabz, sharj_dcode);
            report1.SetDataSource(ds);

            report1.SetParameterValue("doreh_mah_sal", get_dorh_mah_sal("sharj_doreh", sharj_dcode.ToString()));

            CrystalDecisions.Windows.Forms.CrystalReportViewer cr = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            cr.ReportSource = report1;
            cr.ExportReport();

            CrysReports.esf_bozorg.sharj_doreh_hesabdari_bestankar report_bestankar = new CrysReports.esf_bozorg.sharj_doreh_hesabdari_bestankar();
            report_bestankar.SetDataSource(ds);
            report_bestankar.SetParameterValue("doreh_mah_sal", get_dorh_mah_sal("sharj_doreh", sharj_dcode.ToString()));
            cr.ReportSource = report_bestankar;
            cr.ExportReport();


        }

        static string get_dorh_mah_sal(string table_name, string dcode)
        {
            try
            {
                DateTime dt_start = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select start_time from " + table_name + " where dcode=" + dcode));
                return new PersianDate(dt_start).ToString("M");

            }
            catch (Exception)
            {
                return "";
            }

        }




        static void Gozaresh_karbordi_sharj_Persian_date(CrysReports.ReportDataSet ds, string field)
        {
            DataTable dt_moshtarekin_temp = ds.moshtarekin_for_report.CopyToDataTable();
            ds.moshtarekin_for_report.Columns.Remove(field);
            ds.moshtarekin_for_report.Columns.Add(field);

            for (int i = 0; i < ds.moshtarekin_for_report.Rows.Count; i++)
            {
                try
                {
                    ds.moshtarekin_for_report.Rows[i][field] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_moshtarekin_temp.Rows[i][field])).ToString("d");
                }
                catch (Exception)
                {
                    ds.moshtarekin_for_report.Rows[i][field] = "";
                }
            }
        }

        public static void Gozaresh_karbordi_ab(int ab_dcode)
        {
            CrysReports.esf_bozorg.ab_doreh_report report1 = new CrysReports.esf_bozorg.ab_doreh_report();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_ab_report(ds.moshtarekin_for_report, ab_dcode);
            Gozaresh_karbordi_sharj_Persian_date(ds, "tarikh_gharardad_ab");
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.ab_ghabz, ab_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }





        public static void Gozaresh_karbordi_sharj(int sharj_dcode)
        {
            CrysReports.esf_bozorg.sharj_doreh_report report1 = new CrysReports.esf_bozorg.sharj_doreh_report();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSetTableAdapters.settingTableAdapter();
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter ta_moshtarek = new CrysReports.ReportDataSetTableAdapters.moshtarekin_for_reportTableAdapter();
            ta_moshtarek.Fill_for_sharj_report(ds.moshtarekin_for_report, sharj_dcode);
            Gozaresh_karbordi_sharj_Persian_date(ds, "tarikh_gharardad");
            ta_setting.Fill(ds.setting);
            ta_sharj.FillByDoreh(ds.sharj_ghabz, sharj_dcode);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            new FrmShowReport(report1).ShowDialog();
        }


        public static void Print_Multi_Ghabz(int sharj_dcode, int ab_dcode, int FirstGharardad, int LastGharardad, bool Sort_by_gharardad, int moshtarek_active_state, System.Windows.Forms.ProgressBar prog)
        {
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();

            CrystalDecisions.CrystalReports.Engine.ReportClass report1;
            if (Classes.ClsMain.ShahrakSetting.id == 1)
            {
                report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_by_bg();
            }
            else
            {
                report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_no_bg();
            }
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSet ds_for_moshtarekin = new CrysReports.ReportDataSet();

            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_ab = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter ta_moshtarekin = new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter();


            //ta_ab.FillByDoreh(ds.ab_ghabz, ab_dcode);
            //ta_sharj.FillBy_dcode_gharardad(ds.sharj_ghabz, dcode, gharardad);
            //ta_sharj.FillByDoreh(ds.sharj_ghabz, sharj_dcode);

            if (Sort_by_gharardad)
                ta_moshtarekin.FillByGharardad_Gharardad_sort(ds_for_moshtarekin.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));
            else
                ta_moshtarekin.FillByGharardad_Address_Sort(ds_for_moshtarekin.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));




            string mohlat_pardakht = "";
            decimal ja_mabalegh_horoof = 0;
            for (int i = 0; i < ds_for_moshtarekin.moshtarekin.Rows.Count; i++)
            {
                mohlat_pardakht = ""; ja_mabalegh_horoof = 0;
                ta_moshtarekin.FillBy_gharardad(ds.moshtarekin, Convert.ToInt32(ds_for_moshtarekin.moshtarekin.Rows[i]["gharardad"]));
                ta_ab.FillBy_doreh_gharardad(ds.ab_ghabz, Convert.ToInt32(ds_for_moshtarekin.moshtarekin.Rows[i]["gharardad"]), ab_dcode);
                ta_sharj.FillBy_dcode_gharardad(ds.sharj_ghabz, sharj_dcode, Convert.ToInt32(ds_for_moshtarekin.moshtarekin.Rows[i]["gharardad"]));
                report1.SetDataSource(ds);


                if (moshtarek_active_state == 1)
                    // فقط برای مشترکین فعال قبض چاپ شود
                    if (Convert.ToBoolean(ds.moshtarekin.Rows[i]["vaziat"]) == false)
                        continue;

                if (ds.sharj_ghabz.Rows.Count != 0)
                {
                    mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");
                    ja_mabalegh_horoof += Convert.ToDecimal(ds.sharj_ghabz.Rows[0]["mablaghkol"]);
                }

                if (ds.ab_ghabz.Rows.Count != 0)
                {
                    mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");
                    ja_mabalegh_horoof += Convert.ToDecimal(ds.ab_ghabz.Rows[0]["mablaghkol"]);

                    try
                    {
                        report1.SetParameterValue("gheraat_start", PersianDateConverter.ToPersianDate(Convert.ToDateTime(
                           ds.ab_ghabz.Rows[0]["tarikh_gheraat_ghabli"].ToString())).ToString("d"));
                    }
                    catch
                    {
                        report1.SetParameterValue("gheraat_start", "");
                    }


                    try
                    {
                        report1.SetParameterValue("gheraat_end", PersianDateConverter.ToPersianDate(Convert.ToDateTime(
                  ds.ab_ghabz.Rows[0]["tarikh_gheraat"].ToString())).ToString("d"));
                    }
                    catch (Exception)
                    {
                        report1.SetParameterValue("gheraat_end", "");
                    }

                    if (Convert.ToInt32(ds.ab_ghabz.Rows[0]["hazineh_taviz_kontor"]) == 0)
                        report1.SetParameterValue("ab_taviz_kontor_label", "");
                    else
                        report1.SetParameterValue("ab_taviz_kontor_label", "هزینه تعویض کنتور آب:");


                }
                else
                {
                    report1.SetParameterValue("gheraat_start", "");
                    report1.SetParameterValue("gheraat_end", "");
                    report1.SetParameterValue("ab_taviz_kontor_label", "");


                }


                report1.SetParameterValue("mohlat_pardakht", mohlat_pardakht.ToString());
                report1.SetParameterValue("ja_mabalegh_horoof", clsNumber.Number_to_Str(ja_mabalegh_horoof.ToString()).ToString());
                report1.SetParameterValue("jam_mabalegh", ja_mabalegh_horoof.ToString());

                report1.PrintToPrinter(1, true, 0, 0);


                //viewer.ReportSource = report1;
            }

            //new FrmShowReport(report1).ShowDialog();



        }


        public static void Print_Ghabz(int gharardad, int sharj_dcode, int ab_dcode)
        {
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();

            CrystalDecisions.CrystalReports.Engine.ReportClass report1;
            if (Classes.ClsMain.ShahrakSetting.id == 1)
            {
                report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_by_bg();
            }
            else
            {
                report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_no_bg();
            }
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta_ab = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter ta_moshtarekin = new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter();

            ta_ab.FillBy_doreh_gharardad(ds.ab_ghabz, gharardad, ab_dcode);
            ta_sharj.FillBy_dcode_gharardad(ds.sharj_ghabz, sharj_dcode, gharardad);
            ta_moshtarekin.FillByGharardad_Gharardad_sort(ds.moshtarekin, gharardad, gharardad);

            report1.SetDataSource(ds);

            string mohlat_pardakht = "";
            decimal ja_mabalegh_horoof = 0;
            if (ds.sharj_ghabz.Rows.Count != 0)
            {
                mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");
                ja_mabalegh_horoof += Convert.ToDecimal(ds.sharj_ghabz.Rows[0]["mablaghkol"]);
            }

            if (ds.ab_ghabz.Rows.Count != 0)
            {
                mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");
                ja_mabalegh_horoof += Convert.ToDecimal(ds.ab_ghabz.Rows[0]["mablaghkol"]);

                try
                {
                    report1.SetParameterValue("gheraat_start", PersianDateConverter.ToPersianDate(Convert.ToDateTime(
                       ds.ab_ghabz.Rows[0]["tarikh_gheraat_ghabli"].ToString())).ToString("d"));
                }
                catch
                {
                    report1.SetParameterValue("gheraat_start", "");
                }


                try
                {
                    report1.SetParameterValue("gheraat_end", PersianDateConverter.ToPersianDate(Convert.ToDateTime(
              ds.ab_ghabz.Rows[0]["tarikh_gheraat"].ToString())).ToString("d"));
                }
                catch (Exception)
                {
                    report1.SetParameterValue("gheraat_end", "");
                }

                if (Convert.ToInt32(ds.ab_ghabz.Rows[0]["hazineh_taviz_kontor"]) == 0)
                    report1.SetParameterValue("ab_taviz_kontor_label", "");
                else
                    report1.SetParameterValue("ab_taviz_kontor_label", "هزینه تعویض کنتور آب:");

            }
            else
            {
                report1.SetParameterValue("gheraat_start", "");
                report1.SetParameterValue("gheraat_end", "");
                report1.SetParameterValue("ab_taviz_kontor_label", "");

            }


            report1.SetParameterValue("mohlat_pardakht", mohlat_pardakht.ToString());
            report1.SetParameterValue("ja_mabalegh_horoof", clsNumber.Number_to_Str(ja_mabalegh_horoof.ToString()).ToString());
            report1.SetParameterValue("jam_mabalegh", ja_mabalegh_horoof.ToString());



            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();



        }


        public static void Get_hazineh_Sharj(ref decimal hazineh_mablagh, decimal metraj, ref decimal sharj_takhifif, DateTime? tarikh_ghararadad, DateTime doreh_start, DateTime doreh_end, string gcode)
        {
            decimal result = 0;
            if (metraj < 1000)
            {
                result = 350000;
            }
            else if (metraj >= 1000 && metraj < 5000)
            {
                result = 350000 + (metraj - 1000) * (decimal)165;
            }
            else if (metraj >= 5000 && metraj < 20000)
            {
                result = 1010000 + (metraj - 5000) * (decimal)125;

            }
            else if (metraj >= 20000 && metraj < 100000)
            {
                result = 2885000 + (metraj - 20000) * (decimal)83;

            }
            else
            {
                result = 9525000 + (metraj - 100000) * (decimal)50;

            }


            hazineh_mablagh = GetRoundedInteger(result);
            // دخالت دادن تعداد روزهای دوره در فرمول 

            decimal days_between_doreh = ClsMain.Days_between_tarikh_ha(
               doreh_end,
              doreh_start);


            decimal hazineh_per_day = GetRoundedInteger(hazineh_mablagh / 30);
            DateTime tarikh_gharardad_moshtarek = Convert.ToDateTime(tarikh_ghararadad);
            FarsiLibrary.Utils.PersianCalendar pcal = new PersianCalendar();
            int YearCount = 0;


            hazineh_mablagh = hazineh_per_day * days_between_doreh;
            bool Continue_operation = true;
            sharj_takhifif = 0;


            while (Continue_operation)
            {
                tarikh_gharardad_moshtarek = pcal.AddYears(tarikh_gharardad_moshtarek, 1);
                YearCount++;

                if (tarikh_gharardad_moshtarek < doreh_start)
                {
                    continue;
                }

                // یا تاریخ قرارداد مشترک بین دو تاریخ شروع و پایان دوره ات 
                // و یا اینکه تاریخ پایان دوره را نیز رد کرده است

                if (tarikh_gharardad_moshtarek <= doreh_end)
                {
                    int temp_days_from_start = (tarikh_gharardad_moshtarek - doreh_start).Days + 1;
                    int temp_days_from_end = (doreh_end - tarikh_gharardad_moshtarek).Days;
                    // چون تاریخ قرارداد مشترک در فاصله تاریخ تا شروع دوره محاسبه شده ، در نتیجه در فاصله میان تاریخ تا پایان دوره مجددا مورد محسابه قرار نمی گیرد


                    if (YearCount > 3)
                    {
                        sharj_takhifif = 0;
                        //hazineh_mablagh -= sharj_takhifif;
                    }
                    else
                    {

                        if (YearCount == 3)
                        {
                            // تخفیف سال سوم
                            sharj_takhifif += GetRoundedInteger((hazineh_per_day * temp_days_from_start) / 3);
                            hazineh_mablagh -= sharj_takhifif;
                        }
                        else if (YearCount == 2)
                        {
                            // تخفیف سال دوم
                            sharj_takhifif += GetRoundedInteger((hazineh_per_day * temp_days_from_start) * 2 / 3);
                            // تخفیف سال سوم
                            sharj_takhifif += GetRoundedInteger((hazineh_per_day * temp_days_from_end) / 3);
                            hazineh_mablagh -= sharj_takhifif;
                        }
                        else if (YearCount == 1)
                        {
                            // تخفیف سال اول
                            sharj_takhifif += GetRoundedInteger((hazineh_per_day * temp_days_from_start) * 2 / 3);
                            // تخفیف سال دوم
                            sharj_takhifif += GetRoundedInteger((hazineh_per_day * temp_days_from_end) * 2 / 3);
                            hazineh_mablagh -= sharj_takhifif;
                        }

                    }
                    break;

                }
                else
                {

                    if (YearCount == 1 || YearCount == 2)
                    {
                        sharj_takhifif = GetRoundedInteger(hazineh_mablagh * 2 / 3);
                        hazineh_mablagh -= sharj_takhifif;
                    }
                    else if (YearCount == 3)
                    {
                        sharj_takhifif = GetRoundedInteger(hazineh_mablagh / 3);
                        hazineh_mablagh -= sharj_takhifif;
                    }
                    else
                    {
                        sharj_takhifif = 0;
                        //hazineh_mablagh -= sharj_takhifif;
                    }

                    break;

                }


            }


        }

        public static void Calculate_hazineh_masraf(bool dar_hal_sakht, int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab,
            ref decimal mablagh_mojaz, ref decimal mablagh_gheir_mojaz, ref decimal hazineh_mablagh, DataTable Jarime_masraf_mazad
                )
        {
            //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------

            mablagh_mojaz = masraf_mojaz * tarefe_ab;
            mablagh_gheir_mojaz = Convert.ToDecimal(Get_Masrafe_gheir_mojaz_hazineh(mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, Jarime_masraf_mazad));
            hazineh_mablagh = mablagh_mojaz + mablagh_gheir_mojaz;

        }

        public static decimal GetRoundedInteger(decimal number)
        {
            return Convert.ToDecimal(Math.Round(number));
        }

        public static void Calculate_masraf_omoomi(ref decimal hazineh_masraf_omoomi)
        {
            hazineh_masraf_omoomi = 0;
        }
        public static void Calculate_Fazelab(ref decimal hazineh_fazelab)
        {
            hazineh_fazelab = 0;
        }
        public static void Moshtarek_ensheab_hazineh(DataTable Dt_Hazineh_ensheab, ref decimal hazineh_ensheab, bool is_updating_ghabz)
        {
            if (is_updating_ghabz == false)
            {
                // هزینه انشعاب در این شهرک به صورت ثابت می باشد که مبلغ ان در ایک رکورد در این جدول قرار خواهد گرفت.
                try
                {
                    hazineh_ensheab = Convert.ToDecimal(Dt_Hazineh_ensheab.Rows[0]["hazineh"]);

                }
                catch (Exception)
                {
                    hazineh_ensheab = 100000;
                }
            }
        }


        public static long Get_Masrafe_gheir_mojaz_hazineh(int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab, DataTable Jarime_masraf_mazad)
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]) * masraf_gheir_mojaz * tarefe_ab;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(masraf_gheir_mojaz), Convert.ToDecimal(mizan_masraf), Jarime_masraf_mazad, tarefe_ab));

        }
        public static decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return GetRoundedInteger(sum);

        }


        public static void Print_ab_sharj_ghabz(string dcode
            , string gharardad, bool is_by_image)


        {
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = null;






            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta_sharj = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();

            //ta.FillByGCode(ds.ab_ghabz, Convert.ToInt32(gcode));
            ta_sharj.FillBy_dcode_gharardad(ds.sharj_ghabz, Convert.ToInt32(dcode), Convert.ToInt32(gharardad));
            ta.FillBy_doreh_gharardad(ds.ab_ghabz, Convert.ToInt32(gharardad), Convert.ToInt32(dcode));






            string tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["tarikh"])).ToString("d");
            string mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");


            ds.ab_ghabz.Columns.Remove("tarikh");
            ds.ab_ghabz.Columns.Remove("mohlat_pardakht");


            ds.ab_ghabz.Columns.Add("tarikh");
            ds.ab_ghabz.Columns.Add("mohlat_pardakht");

            ds.ab_ghabz[0]["tarikh"] = tarikh;
            ds.ab_ghabz[0]["mohlat_pardakht"] = mohlat_pardakht;



            string mablaghMAndehoroof = clsNumber.Number_to_Str(ds.ab_ghabz.Rows[0]["mande"].ToString());



            report1.SetDataSource(ds);
            DataView Dt_moshtarek_info = new DataView(ClsMain.MoshtarekDT);
            Dt_moshtarek_info.RowFilter = "gharardad=" + gharardad;
            report1.SetParameterValue("co_name", Dt_moshtarek_info[0]["co_name"].ToString());
            report1.SetParameterValue("metraj", Dt_moshtarek_info[0]["metraj"].ToString());
            report1.SetParameterValue("code_posti", Dt_moshtarek_info[0]["code_posti"].ToString());
            report1.SetParameterValue("address", Dt_moshtarek_info[0]["address"].ToString());
            report1.SetParameterValue("size_ensheab", Dt_moshtarek_info[0]["size_ensheab"].ToString());
            report1.SetParameterValue("masrafe_mojaz", Dt_moshtarek_info[0]["masrafe_mojaz"].ToString());
            report1.SetParameterValue("shenase_variz", Dt_moshtarek_info[0]["shenase_variz"].ToString());


            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from ab_doreh where dcode=" + dcode);
            report1.SetParameterValue("tarikh_start", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d"));
            report1.SetParameterValue("tarikh_end", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d"));

            report1.SetParameterValue("mandehHoroof", mablaghMAndehoroof);
            viewer.ReportSource = report1;

            viewer.PrintReport();
            //new FrmShowReport(report1).ShowDialog();

        }

    }
}
