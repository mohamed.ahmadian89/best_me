﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FarsiLibrary.Utils;

namespace CWMS.Classes.shahrak_classes
{
    public class abbas_abad
    {
        public static void Calculate_hazineh_masraf(bool dar_hal_sakht, int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab,
      ref decimal mablagh_mojaz, ref decimal mablagh_gheir_mojaz, ref decimal hazineh_mablagh, DataTable Jarime_masraf_mazad
        )
        {
            //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


            if (dar_hal_sakht)
            {
                mablagh_mojaz = masraf_mojaz * tarefe_ab;
                mablagh_gheir_mojaz = masraf_gheir_mojaz * tarefe_ab;
                hazineh_mablagh = mablagh_mojaz + mablagh_gheir_mojaz;

            }
            else
            {
                mablagh_mojaz = masraf_mojaz * tarefe_ab;
                mablagh_gheir_mojaz = Convert.ToDecimal(Get_Masrafe_gheir_mojaz_hazineh(mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, Jarime_masraf_mazad));
                hazineh_mablagh = mablagh_mojaz + mablagh_gheir_mojaz;
            }


        }
        public static long Get_Masrafe_gheir_mojaz_hazineh(int mizan_masraf, int masraf_mojaz, int masraf_gheir_mojaz, int tarefe_ab, DataTable Jarime_masraf_mazad)
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]) * masraf_gheir_mojaz * tarefe_ab;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(masraf_gheir_mojaz), Convert.ToDecimal(mizan_masraf), Jarime_masraf_mazad, tarefe_ab));

        }
        public static decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return GetRoundedInteger(sum);
        }




        public static void Moshtarek_ensheab_hazineh(decimal size_ensheab, DataTable Dt_Hazineh_ensheab, ref decimal hazineh_ensheab, bool is_updating_ghabz)
        {
            if (is_updating_ghabz == false)
            {
                bool found = false;
                for (int i = 0; i < Dt_Hazineh_ensheab.Rows.Count; i++)
                {
                    if (size_ensheab <= Convert.ToDecimal(Dt_Hazineh_ensheab.Rows[i]["tasize"]))
                    {
                        hazineh_ensheab = Convert.ToDecimal(Dt_Hazineh_ensheab.Rows[i]["hazineh"]);
                        found = true;
                        break;
                    }
                }


                if (found == false)
                    hazineh_ensheab = Convert.ToDecimal(Dt_Hazineh_ensheab.Rows[Dt_Hazineh_ensheab.Rows.Count - 1]["hazineh"]);
            }
        }
        public static decimal GetRoundedInteger(decimal number)
        {
            return Convert.ToDecimal(Math.Round(number));
        }
        public static void Calculate_masraf_omoomi(ref decimal hazineh_masraf_omoomi)
        {
            hazineh_masraf_omoomi = 0;
        }
        public static void Calculate_Fazelab(ref decimal hazineh_fazelab)
        {

            hazineh_fazelab = 0;
        }


        public static void Get_hazineh_Sharj(ref decimal hazineh_mablagh, DateTime doreh_start_time, decimal Tarefe, decimal metraj, int tedad_rooz)
        {
            int TedadRoozHayeSal = new FarsiLibrary.Utils.PersianCalendar().GetDaysInYear(
                                   new PersianDate(doreh_start_time).Year);
            hazineh_mablagh = GetRoundedInteger(metraj * tedad_rooz * ((decimal)Tarefe / TedadRoozHayeSal));
        }

    }
}
