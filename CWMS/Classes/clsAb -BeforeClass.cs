﻿using FarsiLibrary.Utils;
using Snippet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace CWMS.Classes
{
    class clsAb1
    {
        public static string Dcode = "1";
        public static int allMoshtarekCount = 0;
        public static int Success_Tedad_ghobooz_saderShode = 0, Fail_Tedad_ghobooz_saderShode = 0;


      

        public static void RizHeasbAbForAll(AbDataset dt_ab, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshAbForAll report1)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();



            report1.SetDataSource(dt_ab);
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();


        }


        public static void RizHeasb(string start, string end, string gharardad, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshAb report1, string typeOfReport)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillBySomeDoreh(ds.ab_ghabz, Convert.ToInt32(start), Convert.ToInt32(end), Convert.ToInt32(gharardad));


            report1.SetDataSource(ds);

            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;

            new FrmShowReport(report1).ShowDialog();


        }




        public static void ChapAbDoreh(string dcode, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Ab_doreh report1, string typeOfReport, string gharardad)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.Ab_DorehTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.Ab_DorehTableAdapter();
            if (dcode == "all")
                ta.Fill(ds.Ab_Doreh, Convert.ToInt32(gharardad));
            else
                ta.FillByDcode(ds.Ab_Doreh, Convert.ToInt32(gharardad), Convert.ToInt32(dcode));

            ArrayList dt_times = new ArrayList();
            for (int i = 0; i < ds.Ab_Doreh.Rows.Count; i++)
            {
                dt_times.Add(ds.Ab_Doreh.Rows[i]["tarikh_vosool"]);
            }
            ds.Ab_Doreh.Columns.Remove("tarikh_vosool");
            ds.Ab_Doreh.Columns.Add("tarikh_vosool");

            for (int i = 0; i < ds.Ab_Doreh.Rows.Count; i++)
            {
                if (dt_times[i].ToString() == "")
                    ds.Ab_Doreh.Rows[i]["tarikh_vosool"] = "";
                else
                    ds.Ab_Doreh.Rows[i]["tarikh_vosool"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_times[i])).ToString("d");
            }



            report1.SetDataSource(ds);



            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("mablaghkolh", Mablaghhorrof);
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //ExportOptions exoption = new ExportOptions();
            //if (typeOfReport == "print")
            //{
            //    viewer.PrintReport();
            //}

            //else
            //{
            //    exoption.ExportFormatType = ExportFormatType.PortableDocFormat;
            //    viewer.ExportReport();
            //}
        }




        public static void ChapPishSodoor(
      CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.CheckGheraat report1)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.kontor_khanTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.kontor_khanTableAdapter();
            ta.Fill(ds.kontor_khan);
            report1.SetDataSource(ds);
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }


        public static void ChapAbGhabzVertical(string gcode, string gharardad,
    CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.ab_ghabzPrint report1)
        {




            System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
            doctoprint.PrinterSettings.PrinterName = "ghabzPrinter";
            int i = 0;
            for (i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count; i++)
            {
                int rawKind = 0;
                if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == "CHEQUEPRINT")
                {
                    rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                    report1.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)rawKind;
                    break;
                }
            }

            PageMargins p = new PageMargins();
            p.topMargin = p.bottomMargin = 0;
            report1.PrintOptions.ApplyPageMargins(p);


            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillByGCode(ds.ab_ghabz, Convert.ToInt32(gcode));


            string tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["tarikh"])).ToString("d");
            string mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");


            ds.ab_ghabz.Columns.Remove("tarikh");
            ds.ab_ghabz.Columns.Remove("mohlat_pardakht");


            ds.ab_ghabz.Columns.Add("tarikh");
            ds.ab_ghabz.Columns.Add("mohlat_pardakht");

            ds.ab_ghabz[0]["tarikh"] = tarikh;
            ds.ab_ghabz[0]["mohlat_pardakht"] = mohlat_pardakht;



            string mablaghMAndehoroof = clsNumber.Number_to_Str(ds.ab_ghabz.Rows[0]["mande"].ToString());



            report1.SetDataSource(ds);
            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel,metraj,pelak,address,ghataat from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("mandehHoroof", mablaghMAndehoroof);
            report1.SetParameterValue("pshodeh", (Convert.ToInt64(ds.ab_ghabz.Rows[0]["mablaghkol"]) - Convert.ToInt64(ds.ab_ghabz.Rows[0]["mande"])).ToString());
            report1.SetParameterValue("masahat", Dt_moshtarek_info.Rows[0]["metraj"].ToString());
            report1.SetParameterValue("pelak", Dt_moshtarek_info.Rows[0]["pelak"].ToString());
            report1.SetParameterValue("address", Dt_moshtarek_info.Rows[0]["address"].ToString());
            report1.SetParameterValue("ghataat", Dt_moshtarek_info.Rows[0]["ghataat"].ToString());


            //string Doreh = ds.ab_ghabz.Rows[0]["dcode"].ToString();
            //DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from ab_doreh where dcode=" + Doreh);
            //int days = (Convert.ToDateTime(dt_doreh.Rows[0][1]) - Convert.ToDateTime(dt_doreh.Rows[0][0])).Days;
            int days = Convert.ToInt32(ds.ab_ghabz.Rows[0]["tedadRooz"]);
            report1.SetParameterValue("dcodeinfo", "( " + days + " روزه " + " )");
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

        }




        public static void ChapAbGhabzSmall(string gcode, string gharardad, string mablaghhoroof,
                    CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.AbGhabzSmall report1)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillByGCode(ds.ab_ghabz, Convert.ToInt32(gcode));

            ds.ab_ghabz.Rows[0]["tarikh"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["tarikh"])).ToString("d");
            ds.ab_ghabz.Rows[0]["mohlat_pardakht"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

            report1.SetDataSource(ds);
            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("tarikh", PersianDate.Now.ToString("D"));
            //report1.SetParameterValue("gozaresh_type", "تک قبض شارژ");
            report1.SetParameterValue("mablaghkolh", mablaghhoroof);

            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

        }





        public static int Get_day_ta_shoru_doreh_badd()
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime Last_doreh_end_Last_doreh_end_time = Get_last_ab_doreh_endTime();

            return (Last_doreh_end_Last_doreh_end_time - DateTime.Now).Days + 1;


        }

        public static DateTime Get_last_ab_doreh_endTime()
        {
            return Convert.ToDateTime(ClsMain.ExecuteScalar("select top(1) end_time from ab_doreh order by dcode desc"));
        }


        public static DataTable Get_last_Ab_mablagh_tasvib_shodeh()
        {
            Classes.ClsMain.ChangeCulture("e");
            return Classes.ClsMain.GetDataTable("select top(1) * from ab_mablagh where tarikh_ejra<='" + DateTime.Now + "'  order by tarikh_ejra desc");

        }




        public static int Check_new_ab_doreh_should_created_and_return_NewDcode()
        {
            try
            {
                DataTable DtLastAbDoreh = Classes.clsDoreh.Get_last_Ab_Doreh();
                if (DtLastAbDoreh.Rows.Count == 0)
                    return 1;

                DateTime NewDoreh_StartTime = Convert.ToDateTime(DtLastAbDoreh.Rows[0]["end_time"]);
                if (DateTime.Now >= NewDoreh_StartTime)
                {
                    return Convert.ToInt32(DtLastAbDoreh.Rows[0]["dcode"]) + 1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Classes.ClsMain.LogIt(ex);
                return -1;
            }

        }



        public static Boolean IsAbInfoExist()
        {
            if (ClsMain.ExecuteScalar("select count(*) from ab_info").ToString() == "0")
                return false;
            return true;
        }


        //static long GetRoundedInteger(float number)
        //{
        //    return Convert.ToInt64(Math.Round(Convert.ToDecimal(number)));
        //}
        static long GetRoundedInteger(decimal number)
        {
            return Convert.ToInt64(Math.Round(number));
        }





        public static void UpdateDorehInfoAtEndOfSodoorProcess(DateTime Start_time, DateTime End_time, int Success_Tedad_ghobooz_saderShode, int Fail_Tedad_ghobooz_saderShode, string dcode)
        {
            ClsMain.ExecuteNoneQuery("update ab_doreh set success=" + Success_Tedad_ghobooz_saderShode.ToString() + ",fails=" + Fail_Tedad_ghobooz_saderShode + ",ghabz_sodoor=1 where dcode=" + dcode);
        }





        public static void Update_ghabz(Classes.MultipleCommand Multiple, string gharardad, string dcode, string tarefe, string mablagh, string MohltPardakht, string MablaghKol, string darsad_maliat, string maliat, string Bedehi, string aboonmah, string Ensheab, string kasr_hezar, string masraf_mojaz, string masraf_gheir_mojaz, string masraf_mojaz_hazineh, string masraf_gheir_mojaz_hazineh, bool dor_kamel, bool taviz, bool adam_gheraat, bool ghat, bool darhalsakht, bool kharab, string masraf, string kontor_start, string kontor_end, long sayer, long jarimeh, string jarimeh_tozihat, string sayer_tozihat, string darsad_fazelab, string fazelab, bool ISTejari, DateTime Tarikh_gheraaat)
        {
            try
            {

                int tejari = 0;
                if (ISTejari)
                    tejari = 1;

                string gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(dcode), true, Convert.ToInt32(gharardad));

                decimal pardakhti = Convert.ToDecimal(ClsMain.ExecuteScalar("select isnull(sum(mablagh),0) from ghabz_pardakht"
                + " where gcode=" + gcode + " and vaziat=1"));


                decimal mande = Convert.ToDecimal(MablaghKol) - pardakhti;



                Multiple.addToCommandCollecton("INSERT INTO ab_ghabz (gcode,[gharardad],[dcode],[tarikh],[mablagh],tarefe,mohlat_pardakht,mablaghkol" +
               ",darsad_maliat,maliat,bedehi,aboonmah,Ensheab,mande,kasr_hezar,masraf_mojaz,masraf_gheir_mojaz,masraf_mojaz_hazineh,masraf_gheir_mojaz_hazineh,kharab,dor_kamel,taviz,adam_gheraat,ghat,darhalsakht,masraf,kontor_start,kontor_end,sayer,jarimeh,sayer_tozihat,jarimeh_tozihat,darsad_fazelab,fazelab,tejari,tarikh_gheraat)   values (" + gcode + "," + gharardad + "," + dcode + ",'" +
               DateTime.Now + "'," + mablagh + "," + tarefe + ",'" + MohltPardakht + "'," + MablaghKol.ToString() + "," +
               darsad_maliat.ToString() + "," + maliat.ToString() + "," + Bedehi + "," + aboonmah + "," + Ensheab + "," +
               (mande).ToString() + "," + kasr_hezar.ToString() + "," +
               masraf_mojaz + "," + masraf_gheir_mojaz + "," + masraf_mojaz_hazineh + "," + masraf_gheir_mojaz_hazineh + "," +
               GetValidbool(kharab) + "," + GetValidbool(dor_kamel) + "," + GetValidbool(taviz) + "," + GetValidbool(adam_gheraat) + "," + GetValidbool(ghat) + "," + GetValidbool(darhalsakht) + "," + masraf + "," +
               kontor_start + "," + kontor_end + "," + sayer + "," + jarimeh + ",'" + sayer_tozihat + "','" + jarimeh_tozihat + "'," +
                   darsad_fazelab + "," + fazelab + "," + tejari + ",'" + Tarikh_gheraaat + "'"


               + ");");




            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }


        public static void Sodoor_ghabz(Classes.MultipleCommand Multiple, string gharardad, string dcode, string tarefe, string mablagh, string MohltPardakht, string MablaghKol, string darsad_maliat, string maliat, string Bedehi, string aboonmah, string Ensheab, string kasr_hezar, string masraf_mojaz, string masraf_gheir_mojaz, string masraf_mojaz_hazineh, string masraf_gheir_mojaz_hazineh, bool dor_kamel, bool taviz, bool adam_gheraat, bool ghat, bool darhalsakht, bool kharab, string masraf, string kontor_start, string kontor_end, long sayer, long jarimeh, string jarimeh_tozihat, string sayer_tozihat, string darsad_fazelab, string fazelab, bool ISTejari, DateTime Tarikh_gheraaat, decimal bestankari, string tozihat, int vaziat_ghabz, decimal Mandeh, int tedadRooz)
        {
            try
            {
                int tejari = 0;
                if (ISTejari)
                    tejari = 1;

                string gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(dcode), true, Convert.ToInt32(gharardad));

                Multiple.addToCommandCollecton("INSERT INTO ab_ghabz (gcode,[gharardad],[dcode],[tarikh],[mablagh],tarefe,mohlat_pardakht,mablaghkol" +
               ",darsad_maliat,maliat,bedehi,aboonmah,Ensheab,mande,kasr_hezar,masraf_mojaz,masraf_gheir_mojaz,masraf_mojaz_hazineh,"
                + "masraf_gheir_mojaz_hazineh,kharab,dor_kamel,taviz,adam_gheraat,ghat,darhalsakht,masraf,kontor_start,kontor_end," +
                "sayer,jarimeh,sayer_tozihat,jarimeh_tozihat,darsad_fazelab,fazelab,tejari,tarikh_gheraat,vaziat,bestankari,tozihat,tedadRooz)   values ("
                + gcode + "," + gharardad + "," + dcode + ",'" +
               DateTime.Now + "'," + mablagh + "," + tarefe + ",'" + MohltPardakht + "'," + MablaghKol.ToString() + "," +
               darsad_maliat.ToString() + "," + maliat.ToString() + "," + Bedehi + "," + aboonmah + "," + Ensheab + "," +
               Mandeh + "," + kasr_hezar.ToString() + "," +
               masraf_mojaz + "," + masraf_gheir_mojaz + "," + masraf_mojaz_hazineh + "," + masraf_gheir_mojaz_hazineh + "," +
               GetValidbool(kharab) + "," + GetValidbool(dor_kamel) + "," + GetValidbool(taviz) + "," + GetValidbool(adam_gheraat) + "," + GetValidbool(ghat) + "," + GetValidbool(darhalsakht) + "," + masraf + "," +
               kontor_start + "," + kontor_end + "," + sayer + "," + jarimeh + ",'" + sayer_tozihat + "','" + jarimeh_tozihat + "'," +
                   darsad_fazelab + "," + fazelab + "," + tejari + ",'" + Tarikh_gheraaat + "',"
                + vaziat_ghabz + "," + bestankari + ",'" + tozihat + "'," + tedadRooz

               + ");");




            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }


        public static decimal Moshtarek_ensheab_hazineh(float EnsheabSize, DataTable Dt_Hazineh = null)
        {

            if (Dt_Hazineh == null)
            {
                Dt_Hazineh = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");
            }
            //این فرمول هزینه را بر اساس میزان انشعاب بدست می 
            //یعنی تا یک سایز مشخص ، هزینه ان را حساب می کند

            for (int i = 0; i < Dt_Hazineh.Rows.Count; i++)
            {
                if (EnsheabSize <= Convert.ToSingle(Dt_Hazineh.Rows[i]["tasize"]))
                    return Convert.ToDecimal(Dt_Hazineh.Rows[i]["hazineh"]);
            }
            return Convert.ToDecimal(Dt_Hazineh.Rows[Dt_Hazineh.Rows.Count - 1]["hazineh"]);
        }



        public static DataTable Get_Ab_ghabz_info(string gcode)
        {
            try
            {
                return Classes.ClsMain.GetDataTable("select * from ab_ghabz where gcode=" + gcode);
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex);
                Payam.Show("خطا در خواندن اطلاعات قبوض آب. لطفا با پشتیبان هماهنگ نمایید");
                return new DataTable();
            }
        }
        public static void Lock_Ab_Ghobooz_of_Doreh_Ghabl(string CodeOfDorehGhabl)
        {
            Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set locked=1 where dcode=" + CodeOfDorehGhabl);

        }




        public static void Tak_Ghabz(string dcode, string gharardad, int Ab_Doreh_Tarefe, float Darsad_malit, float ZaribTejari, float ZaraibDarHalSakht, DateTime Ab_Doreh_Mohlat_pardakht,
            long Bedehi, long sayer, long jarimeh, string sayer_tozihat, string jarime_tozihat, int kars_hezar, int Kontor_start, int kontor_end, int Aboonman,
            bool ghat, bool adam_gheraat, bool kharab, bool taviz, bool dor_kamel, bool Tejari, bool DarHaleSakht, float Zarib_fazelab, DateTime tarikh_gheraat)
        {


            clsAb.Dcode = dcode;
            DataTable dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);
            DataTable dt_Jarime_masraf_mazad = ClsMain.GetDataTable("select * from Jarime_masraf_mazad");
            DataTable dt_all_Hazineh = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");

            //------------------------------------------------------------------
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            con.ConnectionString = ClsMain.ConnectionStr;
            com.Connection = con;
            con.Open();


            //------------------------------------------------------------------



            try
            {

                string Moshtarek_Gharardad = "";
                decimal Moshtarek_EnsheabSize = 0;
                long bestankari = 0;
                string tozihat = "";
                long mandeh = 0;
                int Moshtarek_Top_Kontor_ragham = 0, Moshtarek_SaghfMasraf = 0;
                int vaziat_ghabz = 0;



                //..بدست اوردن اطلاعات اولیه مشترک 
                Moshtarek_Gharardad = dt_all_moshtarek.Rows[0]["gharardad"].ToString();
                Moshtarek_EnsheabSize = Convert.ToDecimal(dt_all_moshtarek.Rows[0]["size_ensheab"]);
                Moshtarek_Top_Kontor_ragham = Convert.ToInt32(dt_all_moshtarek.Rows[0]["kontor_ragham"]);
                Moshtarek_SaghfMasraf = Convert.ToInt32(dt_all_moshtarek.Rows[0]["masrafe_mojaz"]);




                int mizan_masraf = 0, masraf_mojaz = 0, masraf_gheir_mojaz = 0;
                long Mablagh = 0, Mablagh_mojaz = 0, Mablagh_gheir_Mojaz = 0;
                int kontor_feeli = kontor_end;


                //----------------- محاسبه میزان مصرف --------------------------

                if (ghat || adam_gheraat || kharab)
                {
                    mizan_masraf = 0;
                    kontor_feeli = Kontor_start;
                }
                else
                {
                    if (taviz)
                    {
                        Kontor_start = 0;
                        mizan_masraf = kontor_feeli = kontor_end;
                    }
                    else if (dor_kamel)
                    {

                        mizan_masraf = kontor_feeli + (Moshtarek_Top_Kontor_ragham - Kontor_start);
                    }


                    else
                    {
                        //------حالت عادی که کنتور درست کار می کنه ------------
                        mizan_masraf = kontor_feeli - Kontor_start;
                    }
                }




                if (mizan_masraf > Moshtarek_SaghfMasraf)
                {
                    masraf_gheir_mojaz = mizan_masraf - Moshtarek_SaghfMasraf;
                    masraf_mojaz = Moshtarek_SaghfMasraf;
                }
                else
                {
                    masraf_mojaz = mizan_masraf;
                }

                //----------------- محاسبه میزان مصرف --------------------------



                //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------
                Mablagh_mojaz = masraf_mojaz * Ab_Doreh_Tarefe;
                Mablagh_gheir_Mojaz = Get_Masrafe_gheir_mojaz_hazineh(masraf_gheir_mojaz, dt_Jarime_masraf_mazad, Ab_Doreh_Tarefe, masraf_mojaz);
                Mablagh = Mablagh_mojaz + Mablagh_gheir_Mojaz;
                //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


                //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----
                if (kharab)
                {
                    Mablagh = Mablagh_mojaz = AVG_of_Last2_abGhabz(Convert.ToInt32(clsAb.Dcode), Moshtarek_Gharardad);
                    Mablagh_gheir_Mojaz = 0;
                }
                //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----



                //------------------------------------------------------------------            
                // محاسبه ابونماه با توجه به سایز انشعاب
                long HazinehEnsheab = GetRoundedInteger(Moshtarek_ensheab_hazineh(Convert.ToSingle(Moshtarek_EnsheabSize), dt_all_Hazineh));
                //------------------------------------------------------------------            

                //TODO: نحوه اعمال مالیات -- قبل از اعمال ضرایب یا بعد از آن
                //مبلغ مالیات ، ضریب تجاری و ضریب در حال ساخت و ساز در مرحله دوم صدور قبض آب محاسبه خواهد شد 



                //--------------------- محاسبه ضریب تجاری ---------------------------
                if (Convert.ToBoolean(Tejari) == true)
                    Mablagh = GetRoundedInteger(Convert.ToDecimal(Mablagh * ZaribTejari));


                //--------------------- محاسبه ضریب ساخت و ساز --------------------------
                if (Convert.ToBoolean(DarHaleSakht) == true)
                    Mablagh = GetRoundedInteger(Convert.ToDecimal(Mablagh * ZaraibDarHalSakht));


                // مبلغ اب برابر است با اب بها بعلاوه هزینه انشعاب




                //------------------------------------------------------------------            
                //محاسبه بدهی کاربر بر اساس وضعیت قبوض دوره قبل






                //------------------------------------------------------------------                            
                //محاسبه مبلغ نهایی
                long Moshtarek_Mablagh_Kol = GetRoundedInteger(Convert.ToDecimal(Mablagh + jarimeh + sayer));


                //------------------------------------------------------------------                              
                long Maliat = GetRoundedInteger(Convert.ToDecimal(Moshtarek_Mablagh_Kol * Darsad_malit / 100));
                //-----------------------------------------------------------------          


                long MablaghFazelab = HazinehFazelab(Convert.ToDecimal(Moshtarek_Mablagh_Kol), Convert.ToSingle((Zarib_fazelab)));



                Moshtarek_Mablagh_Kol += Maliat + MablaghFazelab + Bedehi;





                //------------------------------------------------------------------            

                if (Cpayam.Show("مبلغ محاسبه شده جدید برابر است با : " + Moshtarek_Mablagh_Kol + " . آیا تایید میکنید؟") != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }



                //------------------------------------------------------------------            
                if (bestankari > Moshtarek_Mablagh_Kol)
                {
                    kars_hezar = 0;
                    bestankari = bestankari - Moshtarek_Mablagh_Kol;
                    tozihat = "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + Moshtarek_Mablagh_Kol.ToString();

                    Moshtarek_Mablagh_Kol = mandeh = Maliat = 0;
                    vaziat_ghabz = 1;
                }
                else
                {
                    Moshtarek_Mablagh_Kol -= bestankari;
                    if (bestankari != 0)
                        tozihat = "کسر قسمتی از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + bestankari.ToString();
                    bestankari = 0;
                    kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
                    Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;
                    mandeh = Moshtarek_Mablagh_Kol;

                }




                kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
                Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;
                ClsMain.ChangeCulture("e");
                Classes.MultipleCommand Multiple = new MultipleCommand("Soodor_TakGhabz");
                Multiple.addToCommandCollecton("delete from ab_ghabz where dcode=" + dcode + " and gharardad=" + gharardad + ";");
                try
                {
                    Update_ghabz(Multiple, Moshtarek_Gharardad, clsAb.Dcode, Ab_Doreh_Tarefe.ToString(), Mablagh.ToString(),
                        Ab_Doreh_Mohlat_pardakht.ToString(), Moshtarek_Mablagh_Kol.ToString(), Darsad_malit.ToString(), Maliat.ToString(), Bedehi.ToString(),
                        HazinehEnsheab.ToString(), Moshtarek_EnsheabSize.ToString(), kars_hezar.ToString(),
                        masraf_mojaz.ToString(), masraf_gheir_mojaz.ToString(), Mablagh_mojaz.ToString(), Mablagh_gheir_Mojaz.ToString(), dor_kamel, taviz, adam_gheraat, ghat, DarHaleSakht, kharab, mizan_masraf.ToString(), Kontor_start.ToString(), kontor_feeli.ToString(), sayer, jarimeh, jarime_tozihat, sayer_tozihat, Zarib_fazelab.ToString(), MablaghFazelab.ToString(), Tejari, tarikh_gheraat);

                    Success_Tedad_ghobooz_saderShode++;
                    if (Multiple.RunCommandCollecton("فرایند صدور تک قبض اب  - clsAb.Tak_Ghabz "))
                        Payam.Show("اطلاعات قبض با موفقیت در سیستم ذخیره شد");
                    else
                        Payam.Show("بروز خطا در هنگام صدور قبض");

                }
                catch (Exception GhbazError)
                {
                    Classes.ClsMain.logError(GhbazError);
                    Payam.Show("بروز خطا در هنگام صدور قبض");

                }



                con.Close();
                Classes.ClsMain.ChangeCulture("f");

            }
            catch (Exception ex)
            {

                ClsMain.logError(ex, "صدور تک قبض برای مشترک");
                Payam.Show("بروز خطا در هنگام صدور قبض");
            }
        }

        public static long HazinehFazelab(decimal Moshtarek_Mablagh_Kol, float Zarib_fazelab)
        {
            return GetRoundedInteger(Convert.ToDecimal(Moshtarek_Mablagh_Kol * (decimal)Zarib_fazelab / 100));
        }



        static decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return sum;
        }


        //--------------------------------------------------
        //..TODO:در مورد نحوه هزینه مصرف غیر مجاز اینجوری حساب می کنیم که به ازا متر مکعب اضافه هزینه تصاعدی می شود نه برای کل مصرف کاربر
        // این محاسبه به صورت پلکانی خواهد بود . یعنی بر اساس میزان درصد مصرف مجاز که اضافه صورت گرفته 
        // مبلغی با توجه به جدول مصارف مازاد در نظر گرفته می شود
        public static long Get_Masrafe_gheir_mojaz_hazineh(int Masrafe_gheirMojaz, DataTable dtinfo, int Tarefe, int masraf_mojaz)
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(dtinfo.Rows[0]["zarib_ab_baha"]) * Masrafe_gheirMojaz * Tarefe;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(Masrafe_gheirMojaz), Convert.ToDecimal(masraf_mojaz + Masrafe_gheirMojaz), dtinfo, Tarefe));

        }


        static long AVG_of_Last2_abGhabz(int dcode, string gharardad)
        {
            if (dcode == 0 || dcode == 1)
                return 0;
            else if (dcode == 2)
                return Convert.ToInt64(ClsMain.ExecuteScalar("select mablaghkol from ab_ghabz where gharardad=" + gharardad + " and dcode=1"));
            else
                return Convert.ToInt64(ClsMain.ExecuteScalar("select isnull(avg(mablaghkol),0) from ab_ghabz where gharardad=" + gharardad + " and dcode in (" + (dcode - 1).ToString() + "," + (dcode - 2).ToString() + ")"));

        }








        public string Create_ghobooz_Ab_Background(ArrayList JustForOneGharardad = null)
        {

            Classes.ClsMain.ChangeCulture("e");

            //========== try catch  متغیرهای مورد نیاز داخل بلاک های =========
            DateTime start_time;
            DataTable dt_all_Hazineh;
            DataTable dt_all_moshtarek;
            DataTable Dt_all_ghabzof_Previous_doreh;
            DataTable dt_Ab_info;
            DateTime Ab_Doreh_Mohlat_pardakht;

            DataTable dt_Jarime_masraf_mazad;
            DataTable dt_Kontor_first_time = null;
            Ab.MainAbDataset.kontor_khanDataTable dt_kontor_khan = new Ab.MainAbDataset.kontor_khanDataTable();
            Ab.MainAbDatasetTableAdapters.kontor_khanTableAdapter da_kontor = new Ab.MainAbDatasetTableAdapters.kontor_khanTableAdapter();
            da_kontor.Fill(dt_kontor_khan);


            int Ab_Doreh_Tarefe;
            decimal Darsad_malit;

            //==================================================================

            //بدست اوردن هزینه انشعاب
            dt_all_Hazineh = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");
            //------------------------------------------------------------------

            //بدست اوردن اطلاعات مشترکین

            if (JustForOneGharardad == null)
                dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where have_ab_ghabz=1");
            else
                dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + JustForOneGharardad[0]);

            //------------------------------------------------------------------





            clsAb.Dcode = clsDoreh.Get_last_Ab_dcode().ToString();
            start_time = DateTime.Now;



            //بدست اوردن  اطلاعات مربوط به قبوض دوره قبل
            //------------------------------------------------------------------
            if (JustForOneGharardad == null)
                Dt_all_ghabzof_Previous_doreh = ClsMain.GetDataTable("select gharardad,mande,kasr_hezar,kontor_end,bestankari from ab_ghabz  where dcode=" + (Convert.ToInt32(clsAb.Dcode) - 1).ToString());
            else
                Dt_all_ghabzof_Previous_doreh = ClsMain.GetDataTable("select gharardad,mande,kasr_hezar,kontor_end,bestankari from ab_ghabz  where gharardad=" + JustForOneGharardad[0] + " and dcode=" + (Convert.ToInt32(clsAb.Dcode) - 1).ToString());



            //----------------------------------------------------------------
            //بدست اوردن اطلاعات کلی در مورد همه دوره های اب
            dt_Ab_info = ClsMain.GetDataTable("select* from ab_info");
            //------------------------------------------------------------------



            //----------------------------------------------------------------
            //بدست اوردن مقادیر اولیه کنتورهای اب از جدول موقت 
            if (clsAb.Dcode == "1")
                dt_Kontor_first_time = ClsMain.GetDataTable("select * from kontor_first_time_values");
            //------------------------------------------------------------------

            //----------------------------------------------------------------
            //بدست آوردن جریمه مصرف مازاد 
            dt_Jarime_masraf_mazad = ClsMain.GetDataTable("select * from Jarime_masraf_mazad order by ta_darsad asc");
            //------------------------------------------------------------------



            //------------------------------------------------------------------
            //بدست اوردن اطلاعات مربوط به دوره 
            DataTable dt_doreh_info = ClsMain.GetDataTable("select * from ab_doreh where dcode=" + clsAb.Dcode);
            Ab_Doreh_Mohlat_pardakht = Convert.ToDateTime(dt_doreh_info.Rows[0]["mohlat_pardakht"]);
            Darsad_malit = Convert.ToDecimal(dt_doreh_info.Rows[0]["darsad_maliat"]);
            Ab_Doreh_Tarefe = Convert.ToInt32(dt_doreh_info.Rows[0]["mablagh_ab"]);
            decimal ZaribTejari = Convert.ToDecimal(dt_doreh_info.Rows[0]["zaribtejari"]);
            decimal ZaraibDarHalSakht = Convert.ToDecimal(dt_doreh_info.Rows[0]["ZaribSakhtoSaz"]);


            //------------------------------------------------------------------





            //پیماش کلیه مشترکین موجود در لیست موجود صدور قبوض مربوطه
            allMoshtarekCount = dt_all_moshtarek.Rows.Count;

            Classes.MultipleCommand Multiple = new MultipleCommand("Soodor_Ghobboz_ab");

            try
            {

                string Moshtarek_Gharardad = "";
                decimal Moshtarek_EnsheabSize = 0, Zarib_fazelab = 0;
                decimal bestankari = 0;
                decimal Bedehi = 0;
                decimal mandeh = 0;
                int kars_hezar = 0;
                int Kontor_start = 0;
                int Moshtarek_Top_Kontor_ragham = 0, Moshtarek_SaghfMasraf = 0;
                int mizan_masraf = 0, masraf_mojaz = 0, masraf_gheir_mojaz = 0;
                decimal Mablagh = 0, Mablagh_mojaz = 0, Mablagh_gheir_Mojaz = 0;
                int kontor_feeli = 0;
                DateTime Tarikh_gheraaat = DateTime.Now;
                Ab.MainAbDataset.kontor_khanRow row = null;
                string tozihat = "";
                int vaziat_ghabz = 0;
                int Tedad_rooz_doreh = (Convert.ToDateTime(dt_doreh_info.Rows[0]["end_time"]) - Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"])).Days;

                for (int i = 0; i < dt_all_moshtarek.Rows.Count; i++)
                {
                    //------------------------------------------------------------------
                    //----------------------------- مقداردهی اولیه -------------------
                    kars_hezar = 0;
                    Bedehi = 0;
                    bestankari = 0;
                    Moshtarek_Top_Kontor_ragham = Moshtarek_SaghfMasraf = 0;
                    Mablagh = Mablagh_mojaz = Mablagh_gheir_Mojaz = 0;
                    mizan_masraf = masraf_mojaz = masraf_gheir_mojaz = 0;
                    Kontor_start = kontor_feeli = 0;
                    tozihat = "";
                    //.. شروع صدور قبض به ازا تک تک کارخانه دارها 
                    Moshtarek_Gharardad = dt_all_moshtarek.Rows[i]["gharardad"].ToString();
                    Moshtarek_EnsheabSize = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["size_ensheab"]);
                    Moshtarek_Top_Kontor_ragham = Convert.ToInt32(dt_all_moshtarek.Rows[i]["kontor_ragham"]);
                    Moshtarek_SaghfMasraf = Convert.ToInt32(dt_all_moshtarek.Rows[i]["masrafe_mojaz"]);
                    Zarib_fazelab = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["zarib_fazelab"]);
                    vaziat_ghabz = 0;
                    row = null;

                    //------------------------------------------------------------------




                    //----------------------------- قرائت و اطلاعات مربوط به دوره قبلی آب -------------------
                    if (Dt_all_ghabzof_Previous_doreh.Rows.Count == 0)
                    {
                        //-- برای اولین بار می بایست از جدول دیگری خوانده شود که کاربر از منوی مدیریت می تواند محتویات ان را تعیین کند
                        try
                        {
                            var Kontor_firstTime_row = dt_Kontor_first_time.AsEnumerable().Where(p => p.Field<int>("gharardad").ToString() == Moshtarek_Gharardad).First();
                            Kontor_start = Convert.ToInt32(Kontor_firstTime_row["kontor_start"]);
                            Bedehi = Convert.ToInt64(Kontor_firstTime_row["bedehi"]);
                            bestankari = 0;
                            kars_hezar = 0;
                        }
                        catch
                        {
                            Kontor_start = kars_hezar = 0;
                            Bedehi = 0;
                            bestankari = 0;
                        }



                    }
                    else
                    {
                        try
                        {
                            var Ab_ghabz_Of_Previous_doreh = Dt_all_ghabzof_Previous_doreh.AsEnumerable().Where(p => p.Field<int>("gharardad").ToString() == Moshtarek_Gharardad).First();
                            Kontor_start = Convert.ToInt32(Ab_ghabz_Of_Previous_doreh["kontor_end"]);
                            Bedehi = Convert.ToDecimal(Ab_ghabz_Of_Previous_doreh["mande"]);
                            kars_hezar = Convert.ToInt32(Ab_ghabz_Of_Previous_doreh["kasr_hezar"]);
                            bestankari = Convert.ToDecimal(Ab_ghabz_Of_Previous_doreh["bestankari"]);
                        }
                        catch
                        {
                            Kontor_start = kars_hezar = 0;
                            Bedehi = 0;
                            bestankari = 0;
                        }

                    }
                    //------------------------------------------------------------------

                    //-------------------------------------------------------------------------------------------------------------------------------------                    
                    //-- خواندن قرائت جدید مشترک از جدول کنتور خوان  - که می تواند به صورت دستی وارد شده باشد یا از طریق دستگاه کنتورخوان
                    try
                    {
                        row = (Ab.MainAbDataset.kontor_khanRow)dt_kontor_khan.Where(p => p.gharardad == Convert.ToInt32(Moshtarek_Gharardad)).Select(p => p).First();

                    }
                    catch (Exception ert)
                    {
                        ClsMain.logError(ert, "خطا در خواندن مقدار کنتورخوان مربوط به مشترک " + Moshtarek_Gharardad + "در دوره اب " + clsAb.Dcode + "در هنگام صدور قبوض کلی");
                        ClsMain.ExecuteNoneQuery("insert into ab_ghabz_fails values (" + clsAb.Dcode + "," + Moshtarek_Gharardad + ",'عدم وجود قرائت مشترک در فایل کنتور خوان یا لیست دستی قرائت های دوره')");
                        Classes.ClsMain.ChangeCulture("e");
                        continue;
                    }
                    //-------------------------------------------------------------------------------------------------------------------------------------                    

                    Tarikh_gheraaat = row.tarikh;

                    kontor_feeli = row.shomareh_kontor;

                    //----------------- محاسبه میزان مصرف --------------------------

                    if (row.ghat || row.adam_gheraat || row.kharab)
                    {
                        mizan_masraf = 0;
                        kontor_feeli = Kontor_start;
                    }
                    else
                    {
                        if (row.taviz)
                        {
                            Kontor_start = 0;
                            kontor_feeli = row.shomareh_kontor;
                            mizan_masraf = kontor_feeli;
                        }
                        else if (row.dor_kamel)
                        {

                            mizan_masraf = kontor_feeli + (Moshtarek_Top_Kontor_ragham - Kontor_start);
                        }


                        else
                        {
                            //------حالت عادی که کنتور درست کار می کنه ------------
                            mizan_masraf = kontor_feeli - Kontor_start;
                        }
                    }



                    if (mizan_masraf > Moshtarek_SaghfMasraf)
                    {
                        masraf_gheir_mojaz = mizan_masraf - Moshtarek_SaghfMasraf;
                        masraf_mojaz = Moshtarek_SaghfMasraf;
                    }
                    else
                    {
                        masraf_mojaz = mizan_masraf;
                        masraf_gheir_mojaz = 0;
                    }

                    //----------------- محاسبه میزان مصرف --------------------------



                    //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------
                    Mablagh_mojaz = masraf_mojaz * Ab_Doreh_Tarefe;
                    Mablagh_gheir_Mojaz = Get_Masrafe_gheir_mojaz_hazineh(masraf_gheir_mojaz, dt_Jarime_masraf_mazad, Ab_Doreh_Tarefe, masraf_mojaz);
                    Mablagh = Mablagh_mojaz + Mablagh_gheir_Mojaz;
                    //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


                    //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----
                    if (row.kharab)
                    {
                        Mablagh = Mablagh_mojaz = AVG_of_Last2_abGhabz(Convert.ToInt32(clsAb.Dcode), Moshtarek_Gharardad);
                        Mablagh_gheir_Mojaz = 0;
                    }
                    //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----



                    //------------------------------------------------------------------            
                    // محاسبه ابونماه با توجه به سایز انشعاب
                    long HazinehEnsheab = GetRoundedInteger(Moshtarek_ensheab_hazineh(Convert.ToSingle(Moshtarek_EnsheabSize), dt_all_Hazineh));
                    //------------------------------------------------------------------            

                    //TODO: نحوه اعمال مالیات -- قبل از اعمال ضرایب یا بعد از آن
                    //مبلغ مالیات ، ضریب تجاری و ضریب در حال ساخت و ساز در مرحله دوم صدور قبض آب محاسبه خواهد شد 

                    bool ISTejari = Convert.ToBoolean(dt_all_moshtarek.Rows[i]["tejari"]);

                    //--------------------- محاسبه ضریب تجاری ---------------------------
                    if (ISTejari == true)
                        Mablagh = GetRoundedInteger(Mablagh * ZaribTejari);


                    //--------------------- محاسبه ضریب ساخت و ساز --------------------------
                    if (Convert.ToBoolean(dt_all_moshtarek.Rows[i]["darhalsakht"]) == true)
                        Mablagh = GetRoundedInteger(Mablagh * ZaraibDarHalSakht);



                    // مبلغ اب برابر است با اب بها بعلاوه ابونمان
                    Mablagh = Mablagh + HazinehEnsheab;
                    //------------------------------------------------------------------            
                    //محاسبه بدهی کاربر بر اساس وضعیت قبوض دوره قبل






                    //------------------------------------------------------------------                            
                    //محاسبه مبلغ نهایی
                    decimal Moshtarek_Mablagh_Kol = GetRoundedInteger(Convert.ToDecimal(Mablagh + kars_hezar));


                    //------------------------------------------------------------------                              
                    decimal Maliat = GetRoundedInteger(Moshtarek_Mablagh_Kol * Darsad_malit / 100);
                    //------------------------------------------------------------------          

                    decimal MablaghFazelab = GetRoundedInteger(Moshtarek_Mablagh_Kol * Zarib_fazelab / 100);

                    Moshtarek_Mablagh_Kol += Maliat + MablaghFazelab + Bedehi;


                    //------------------------------------------------------------------            



                    // چون در این تابع فرایند بروزرسانی و صدور تک قبض نیز انجام می شود
                    // در حالتی بروز رسانی امکان دارد که کاربر پرداخت هایی قبلا انجام داده باشد
                    // در نتیجه در هنگام صدور قبض جدید آب برای کاربر  می بایست مبالغ پرداختی کاربر نیز
                    // به عنوان بستانکاری به این مبلغ افزوده شود 


                    if (JustForOneGharardad != null)
                    {
                        decimal AllPardakhtiForThisdoreh = Convert.ToDecimal(ClsMain.ExecuteScalar("select isnull(sum(mablagh),0) from ghabz_pardakht where dcode=" + clsAb.Dcode + " and gharardad=" + JustForOneGharardad[0]));
                        bestankari += AllPardakhtiForThisdoreh;
                    }




                    //------------------------------------------------------------------            
                    if (bestankari > Moshtarek_Mablagh_Kol)
                    {
                        kars_hezar = 0;
                        bestankari = bestankari - Moshtarek_Mablagh_Kol;
                        tozihat = "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + Moshtarek_Mablagh_Kol.ToString();
                        Moshtarek_Mablagh_Kol = mandeh = Maliat = 0;
                        vaziat_ghabz = 1;
                    }
                    else
                    {
                        Moshtarek_Mablagh_Kol -= bestankari;
                        if (bestankari != 0)
                            tozihat = "کسر قسمتی از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + bestankari.ToString();
                        bestankari = 0;
                        kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
                        Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;
                        mandeh = Moshtarek_Mablagh_Kol;

                    }




                    kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
                    Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;



                    Sodoor_ghabz(Multiple, Moshtarek_Gharardad, clsAb.Dcode, Ab_Doreh_Tarefe.ToString(), Mablagh.ToString(),
                        Ab_Doreh_Mohlat_pardakht.ToString(), Moshtarek_Mablagh_Kol.ToString(), Darsad_malit.ToString(), Maliat.ToString(), Bedehi.ToString(),
                        HazinehEnsheab.ToString(), Moshtarek_EnsheabSize.ToString(), kars_hezar.ToString(),
                        masraf_mojaz.ToString(), masraf_gheir_mojaz.ToString(), Mablagh_mojaz.ToString(), Mablagh_gheir_Mojaz.ToString(),
                        row.dor_kamel, row.taviz, row.adam_gheraat, row.ghat, row.dar_hal_sakht,
                        row.kharab, mizan_masraf.ToString(), Kontor_start.ToString(), kontor_feeli.ToString(),
                        0, 0, "", "", Zarib_fazelab.ToString(), MablaghFazelab.ToString(), ISTejari, Tarikh_gheraaat, bestankari,
                        tozihat, vaziat_ghabz, mandeh, Tedad_rooz_doreh);

                    if (JustForOneGharardad != null)
                    {
                        Multiple.RunCommandCollecton("Tak ghabz Ab ");
                        Payam.Show("اطلاعات مربوط به قبض با موفقیت در سیستم ذخیره شد ");
                    }
                }



                Classes.ClsMain.ChangeCulture("e");
                if (Multiple.RunCommandCollecton("صدور قبوض اب  - cls_ab.Create_ghobooz_Ab_Background"))
                {
                    DateTime End_time = DateTime.Now;
                    clsAb.UpdateDorehInfoAtEndOfSodoorProcess(start_time, End_time, 0, 0, clsAb.Dcode);
                    Classes.ClsMain.ChangeCulture("e");
                    return "success";
                }
                else
                {
                    Classes.ClsMain.ChangeCulture("e");
                    return "fail";
                }







            }
            catch (Exception ex)
            {

                ClsMain.logError(ex, "ijad ghobooz avalie AB");
                Payam.Show("بروز خطا در هنگام صدور قبض");
                return "fail";
            }
        }




        static int GetValidbool(bool val)
        {
            if (val)
                return 1;
            return 0;
        }

        public static int Get_last_doreh_dcode()
        {
            object code_doreh = ClsMain.ExecuteScalar("select max(dcode) from ab_doreh");
            if (code_doreh.ToString().Trim() == "")
                return -1;
            return Convert.ToInt32(code_doreh);

        }
        public static int Get_new_doreh_dcode()
        {
            string code_doreh = ClsMain.ExecuteScalar("select isnull(max(dcode),0) from ab_doreh").ToString();

            return Convert.ToInt32(code_doreh) + 1;
        }
        //public static int Check_new_sharj_doreh_should_created()
        //{
        //    try
        //    {
        //        int vaziat = 0;

        //        int code_doreh = Get_new_doreh_dcode();
        //        //.. درواقع مقدار یک مشخص می کند که هیچ دوره ای قبل وجود نداشته و این اولین دوره می باشد

        //        {

        //            if (code_doreh != 1)
        //            {
        //                //. اضافه کردن دوره ها به سیستم


        //                DateTime StartTime = DateTime.Now, EndTime = DateTime.Now, Mohlat = DateTime.Now;
        //                StartTime = Get_last_ab_doreh_endTime(string dcode);
        //                //.. تاریخ پایان دوره فعلی ، تاریخ شروع دوره جدید می باشد


        //                if (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 0, 0) >= StartTime)
        //                {
        //                    int[] Baze_result = Get_baze_bein_doreh_va_mohlat();
        //                    int TedadMah_Bazeh_bein_doreh = Baze_result[0], TedadRooz_mohlat_pardakht_ghabz = Baze_result[1];


        //                    float mablagh = Get_last_ab_doreh_Mablagh_sharj();
        //                    if (mablagh == -1)
        //                        return 2;


        //                    PersianCalendar pc = new PersianCalendar();
        //                    EndTime = DateTime.Now.AddMonths(TedadMah_Bazeh_bein_doreh);
        //                    Mohlat = DateTime.Now.AddDays(TedadRooz_mohlat_pardakht_ghabz);
        //                    Ijad_Doreh_jadid_ab(code_doreh, StartTime, EndTime, Mohlat, mablagh);
        //                    vaziat = 1;
        //                }


        //            }
        //        }


        //        return vaziat;
        //    }
        //    catch (Exception ex)
        //    {
        //        Classes.ClsMain.LogIt(ex);
        //        return -1;
        //    }

        //}

        public static float Get_last_ab_doreh_Mablagh_sharj()
        {
            object lastmablgh = ClsMain.ExecuteScalar("select top(1) mablagh from ab_mablagh order by id");
            if (lastmablgh.ToString().Trim() == "")
                return -1;
            return Convert.ToSingle(lastmablgh);
        }
        public static DateTime Get_last_ab_doreh_endTime(string currentDcode)
        {
            return Convert.ToDateTime(ClsMain.ExecuteScalar("select end_time from ab_doreh where dcode=" + (Convert.ToInt32(currentDcode) - 1).ToString()));
        }
        public static int[] Get_baze_bein_doreh_va_mohlat()
        {
            int[] res = new int[2];
            DataTable dt = ClsMain.GetDataTable("select tedad_mah_dar_doreh,mohlat_pardakht from ab_info where id=1");
            res[0] = (int)dt.Rows[0]["tedad_mah_dar_doreh"];
            res[1] = (int)dt.Rows[0]["mohlat_pardakht"];
            return res;
        }


        public static void Ijad_Doreh_jadid_ab(int dcode, DateTime startTime, DateTime EndTime, DateTime MohlatPardakht, float Mablgh)
        {
            ClsMain.ExecuteNoneQuery("insert into ab_doreh (dcode,start_time,end_time,mohlat_pardakht,mablagh_ab)"
                + " values (" + dcode + ",'" + startTime + "','" + EndTime + "','" + MohlatPardakht + "'," + Mablgh + ")");
        }
        public static void Ijad_Doreh_jadid_ab_V2(int dcode, string startTime, string EndTime, string MohlatPardakht, float Mablgh, string darsad_maliat, string SakhtoSazZarib, string TejariZarib)
        {
            ClsMain.ExecuteNoneQuery("insert into ab_doreh (dcode,start_time,end_time,mohlat_pardakht,mablagh_ab,darsad_maliat,ZaribTejari,ZaribSakhtoSaz)"
                + " values (" + dcode + ",'" + startTime + "','" + EndTime + "','" + MohlatPardakht + "'," + Mablgh + "," + darsad_maliat + "," + TejariZarib + "," + SakhtoSazZarib + ")");
        }

        public static DataTable GetAbDoreh()
        {
            return ClsMain.GetDataTable("select * from ab_doreh");
        }

        public static int GetEnsheabHazienh(float Size)
        {
            int hazineh = 0;
            bool found = false;
            DataTable dtinfo = ClsMain.GetDataTable("select * from EnsheabHazineh");
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {
                if (hazineh <= Convert.ToSingle(dtinfo.Rows[i]["tasize"]))
                {
                    hazineh = Convert.ToInt32(dtinfo.Rows[i]["hazineh"]);
                    found = true;
                    break;
                }
            }

            if (found == false)
                return Convert.ToInt32(dtinfo.Rows[dtinfo.Rows.Count]["hazineh"]);

            return hazineh;
        }


    }
}