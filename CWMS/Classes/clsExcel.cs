﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using ClosedXML.Excel;
namespace CWMS.Classes
{
    public class clsExcel
    {
        public clsExcel()
        {

        }

        public static void save_Excel_file(DataTable dt_info, string datatsheet_name)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Excel Files (*.xlsx)|*.xlsx";
            sd.DefaultExt = "xlsx";
            sd.AddExtension = true;

            if (sd.ShowDialog() == DialogResult.OK)
            {

                wb.Worksheets.Add(dt_info, datatsheet_name);
                wb.Worksheet(datatsheet_name).RightToLeft = true;
                wb.SaveAs(sd.FileName);
            }
        }

        public static DataTable Read_Excel_file(string path)
        {
            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(path);
            ClosedXML.Excel.IXLWorksheet ws1 = workbook.Worksheet(1);
            DataTable dt = new DataTable();
            bool firstRow = true;
            Classes.ClsMain.ChangeCulture("e");
            foreach (IXLRow row in ws1.Rows())
            {
                //Use the first row to add columns to DataTable.
                if (firstRow)
                {
                    foreach (IXLCell cell in row.Cells())
                    {
                        dt.Columns.Add(cell.Value.ToString());
                    }
                    firstRow = false;
                }
                else
                {
                    //Add rows to DataTable.
                    dt.Rows.Add();
                    int i = 0;
                    foreach (IXLCell cell in row.Cells())
                    {
                        dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                        i++;
                    }
                }


            }
            Classes.ClsMain.ChangeCulture("f");
            return dt;
        }



    }
}
