﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FarsiLibrary.Utils;
namespace CWMS.Classes
{
    public class clsSharjGhabz
    {


        public clsSharjGhabz()
        {
            ClearAllOfGhabz();
            New_or_Update_TakGhabz_Just_for_1_Moshtarek = -1;
            is_Updating_ghabz = false;
            doreh = -10;


        }
        public clsSharjGhabz(int New_TakGhabz_Just_for_1_Moshtarek_temp)
        {
            ClearAllOfGhabz();
            New_or_Update_TakGhabz_Just_for_1_Moshtarek = New_TakGhabz_Just_for_1_Moshtarek_temp;
            is_Updating_ghabz = false;
            Pardakhti_in_Doreh = 0;
            doreh = -10;
        }

        public bool tejari,
          is_Updating_ghabz = false,
          mablaghKol_is_zero_but_has_bestankari = false,
          was_bestankar_in_sodoorGhabz = false
          ;



        public int Tarefe,
            doreh,
             gharardad,
            kasr_hezar_from_last_doreh,
            kasr_hezar_from_current_doreh,
            vaziat_ghabz,
            tedad_rooz,
            New_or_Update_TakGhabz_Just_for_1_Moshtarek
            ;

        public Decimal darsad_maliat,
            zarib_tejari,
            bedehkar,
            bestankar,

            mandeh,
            metraj,
            sharj_takhfif,
            hazineh_mablagh,
            hazineh_maliat,
            hazineh_sayer,
            hazineh_jarimeh,
            Pardakhti_in_Doreh,

            mablagh_kol

            ;
        public string tozihat, tozihat_sayer, tozihat_jarimeh, shenase_ghabz, shenase_prdakht, gcode;

        public DateTime mohlat_pardakht_ghabz,
                 start_Process,
                 End_Process,
            tarikh_sodoor;
        public DateTime?

                 last_pardakht
                    ;


        public DataTable dt_all_moshtarek
            , sharj_bedehi_from_Past
            , Dt_all_ghabzof_Previous_doreh
            , dt_Sharj_info
             , dt_doreh_info

            ;
        public void finish_Process()
        {
            Classes.ClsMain.ChangeCulture("e");
            End_Process = DateTime.Now;
            ClsMain.ExecuteNoneQuery("update sharj_doreh set tarikh_ijad='" + DateTime.Now + "',ghabz_sodoor=1 where dcode=" + doreh);

        }

        public string Generate_AllGhabz_of_GhabzList(MultipleCommand Multiple)
        {
            if (Multiple.RunCommandCollecton("صدور قبوض شارژ - مرحله نهایی"))
            {
                if (New_or_Update_TakGhabz_Just_for_1_Moshtarek == -1)
                    finish_Process();
                return "success";
            }

            return "fail";

        }
        int Get_Database_Boolean_Value(bool value)
        {
            if (value)
                return 1;
            return 0;
        }

        public void Add_MoshtarekGhabz_to_GhabzList(MultipleCommand Multiple)
        {
            try
            {

                string PreDeletecommand = "";
                if (is_Updating_ghabz)
                {
                    PreDeletecommand = "delete from sharj_ghabz where gcode=" + gcode + ";";
                }

                Multiple.addToCommandCollecton(PreDeletecommand + "INSERT INTO sharj_ghabz (gcode,[gharardad],[dcode],[tarikh],[mablagh]" +
                ",tarefe,metraj,mohlat_pardakht,mablaghkol,darsad_maliat,maliat,bedehi"
                + ",tedadRooz,kasr_hezar,sayer,sayer_tozihat,mande,tejari,jarimeh,vaziat,tozihat,bestankari,was_bestankar_in_sodoorGhabz,last_pardakht,shenase_ghabz,shenase_pardakht,sharj_takhfif,has_pardakhti)   values ("
                + gcode + "," + gharardad + "," + doreh + ",'" + tarikh_sodoor + "'," + hazineh_mablagh + "," +
                Tarefe + "," + metraj + ",'" + mohlat_pardakht_ghabz + "'," + mablagh_kol + "," +
                darsad_maliat + "," + hazineh_maliat + "," + bedehkar + "," + tedad_rooz +
                "," + kasr_hezar_from_current_doreh + "," + hazineh_sayer + ",'" + tozihat_sayer + "'," + mandeh
                + "," + Get_Database_Boolean_Value(tejari) + "," + hazineh_jarimeh + "," + vaziat_ghabz + ",'" + tozihat + "'," + bestankar +
                "," + Get_Database_Boolean_Value(was_bestankar_in_sodoorGhabz) + ",'" + last_pardakht + "','" + shenase_ghabz + "','" + shenase_prdakht + "'," + sharj_takhfif
                + "," + (vaziat_ghabz == 0 ? 0 : 1) +
                ")");
            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }


        public decimal Get_All_Ghabz_Pardakhti(int Tghraradad, int Tdoreh)
        {
            return Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
                        "select isnull(sum(mablagh),0) from ghabz_pardakht"
                        + " where gharardad=" + Tghraradad + " and dcode=" + Tdoreh + " and is_ab=0 and vaziat=1"));


        }


        public void Check_Bedehkar_Bestankar_Status()
        {

            // در حالت ویرایش مبلغ بستانکاری از دوره قبل خوانده نمی شود
            // از فرم اطلاعات قبض می گیرد که ان مبلغ برابر باست با مقدار ذخیره شده در فیلد بستانکاری قبض همان دوره

            /*
            متغیر بستانکار تا اینجا فقط بستانکاری از دوره قبل در ان ذخیره شده است م. 
            */
            clsBedehkar_Bestankar cls = new clsBedehkar_Bestankar(mablagh_kol, gcode, bestankar, tozihat, is_Updating_ghabz);
            cls.Check_Bedehkar_Bestankar_Status();
            bestankar = cls.bestankar;
            mandeh = cls.mandeh;
            vaziat_ghabz = cls.vaziat_ghabz;
            kasr_hezar_from_current_doreh = cls.kasr_hezar_from_current_doreh;
            tozihat = cls.tozihat;
            was_bestankar_in_sodoorGhabz = cls.was_bestankar_in_sodoorGhabz;


            //Pardakhti_in_Doreh = Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
            //"select isnull(sum(mablagh),0) from ghabz_pardakht"
            //+ " where gcode=" + gcode + " and vaziat=1"
            //));


            //bestankar += Pardakhti_in_Doreh;

            //if (bestankar >= mablagh_kol)
            //{

            //    bestankar -= mablagh_kol;
            //    mandeh = 0;
            //    kasr_hezar_from_current_doreh = 0;

            //    // قبض کاربر به طور کامل پرداخت شده است 
            //    vaziat_ghabz = 1;

            //    tozihat += " || " + "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + mablagh_kol.ToString();

            //    // بدین معنا که در حال انجام فرایند صدور قبض برای همه مشترکین هستیم
            //    // و کاربر مبلغ بستاانکاری از مبلغ قبض بیشتر شده 
            //    // در این نسخه مبلغ قبض را صفر نمی کنیم بلکه ما به التفاوت را به عنوان بستانکاری ربای کاربر ثبت می کنیم
            //    if (is_Updating_ghabz == false)
            //        was_bestankar_in_sodoorGhabz = true;
            //    // این بدین معنی است که کاربر در هنگام صدور قبوض دوره بستانکار  بوده است  . 

            //}
            //else
            //{
            //    // در این الگوریتم مبلغ کل ثابت خواهد بود
            //    // و بروی فیلد مانده مانور خواهیم داد

            //    if (bestankar != 0)
            //    {
            //        tozihat += " || " + "کسر  " + bestankar.ToString() + " ریال " + "   از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل ";
            //        vaziat_ghabz = 2;
            //    }

            //    mandeh = mablagh_kol - bestankar;
            //    bestankar = 0;


            //    if (mandeh < 1000)
            //    {
            //        kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh);
            //        mandeh = 0;
            //        vaziat_ghabz = 1;
            //    }
            //    else
            //    {

            //        kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh % 1000);
            //        mandeh -= kasr_hezar_from_current_doreh;

            //    }


            //    // فقط در حالت ویرایش قبض . چراکه در هنگام صدور قبض کلی یا قبض تکی ، هیچ پرداختی برای کاربر در سیستم وجود ندارد . 
            //    if (Pardakhti_in_Doreh != 0)
            //        vaziat_ghabz = 2;



            //}




        }

        public void Add_MablaghKol_Jarimeh()
        {
            mablagh_kol += hazineh_jarimeh;
        }
        public void Add_MablaghKol_Sayer()
        {
            mablagh_kol += hazineh_sayer;
        }
        public void Add_MablaghKol_Hazineh_Mablagh()
        {
            mablagh_kol += hazineh_mablagh;
        }

        public void Add_MablaghKol_Hazineh_Maliat()
        {
            mablagh_kol += hazineh_maliat;
        }

        public void Add_MablaghKol_Kasr_HezarFrom_LastDoreh()
        {
            mablagh_kol += kasr_hezar_from_last_doreh;
        }

        public void Add_MablaghKol_Bedehi()
        {
            mablagh_kol += bedehkar;
        }

        public void Calculate_Maliat()
        {
            hazineh_maliat = Math.Round(Convert.ToDecimal((mablagh_kol) * darsad_maliat / 100));
        }




        public void Get_last_sharj_ghabz_of_Moshtarek()
        {
            // در صورتی که در حال ویرایش یک قبض هستیم ، فیلدهای بدهکار بستانکار و کسر هزار از دوره قبل از طریق خود فرم مقدار دهی شده اند 
            // در نتیجه نیازی نیست که مابقی دستورات این تابع اجرا شود 
            if (is_Updating_ghabz)
                return;


        

            if (Dt_all_ghabzof_Previous_doreh.Rows.Count == 0)
            {
                //-- برای اولین بار می بایست از جدول دیگری خوانده شود که کاربر از منوی مدیریت می تواند محتویات ان را تعیین کند
                try
                {
                    var Sharj_firstTime_row = sharj_bedehi_from_Past.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                    bedehkar = Convert.ToInt64(Sharj_firstTime_row["bedehi"]);
                    kasr_hezar_from_last_doreh = 0;
                    bestankar = Convert.ToInt64(Sharj_firstTime_row["bestankar"]);
                    // ان شاءد الله در تغییرات بعدی
                    // بهتر است که به جدول فوق بستانکاری نیز اضافه شود تا در اولین صدور قبض
                    // امکان ثبت بستانکاری با بدهکاری برای کاربر وجود داشته باشد


                }
                catch
                {
                    kasr_hezar_from_last_doreh = 0;
                    bedehkar = 0;
                    bestankar = 0;

                }


            }
            else
            {
                try
                {
                    var Sharj_ghabz_Of_Previous_doreh = Dt_all_ghabzof_Previous_doreh.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                    bedehkar = Convert.ToInt64(Sharj_ghabz_Of_Previous_doreh["mande"]);
                    kasr_hezar_from_last_doreh = Convert.ToInt32(Sharj_ghabz_Of_Previous_doreh["kasr_hezar"]);
                    bestankar = Convert.ToInt64(Sharj_ghabz_Of_Previous_doreh["bestankari"]);

                }
                catch (Exception)
                {

                    kasr_hezar_from_last_doreh = 0;
                    bedehkar = 0;
                    bestankar = 0;
                }

            }
        }


        public void Check_tejari_state()
        {
            if (tejari)
            {
                hazineh_mablagh = Math.Round(hazineh_mablagh * zarib_tejari);
            }
        }


        public void Calculate_Sharj_Hazineh(DateTime? tarikh_gharardad, string gcode)
        {

            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                Classes.shahrak_classes.abbas_abad.Get_hazineh_Sharj(ref hazineh_mablagh
                    , Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"]), Tarefe, metraj, tedad_rooz);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
            {
                Classes.shahrak_classes.paytakht.Get_hazineh_Sharj(ref hazineh_mablagh
                    , Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"]), Tarefe, metraj, tedad_rooz);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
            {
                Classes.shahrak_classes.esf_bozorg.Get_hazineh_Sharj(ref hazineh_mablagh, metraj, ref sharj_takhfif, tarikh_gharardad, Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"]), Convert.ToDateTime(dt_doreh_info.Rows[0]["end_time"]), gcode);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
            {
                Classes.shahrak_classes.shams_abad.Get_hazineh_Sharj(ref hazineh_mablagh
                    , Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"]), Tarefe, metraj, tedad_rooz);
            }
        }

        public void Ghabz_code__gcode()
        {
            gcode = ClsMain.CodeGhabzGenerate(doreh, false, gharardad);
        }

        public void Ghabz_Shenase()
        {

            try
            {
                shenase_ghabz = shenase_prdakht = "";
                int year = Convert.ToInt32(new FarsiLibrary.Utils.PersianDate(tarikh_sodoor).Year);
                shenase_ghabz = ShenasehGhabz.Ghabz.Shenase_Ghabz(gharardad.ToString(), ClsMain.SherkartCode_MelliAccount, "6");
                if (mandeh == 0)

                    shenase_prdakht = "0";
                else
                    shenase_prdakht = ShenasehGhabz.Ghabz.Shenase_Parakht(shenase_ghabz, (GetRoundedInteger(mandeh / 1000)).ToString(), year, doreh);
            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert);
                shenase_ghabz = shenase_prdakht = "";

            }


        }


        static decimal GetRoundedInteger(decimal number)
        {
            return Convert.ToDecimal(Math.Round(number));
        }


        public void Get_Moshtarek_Info(int i)
        {
            gharardad = Convert.ToInt32(dt_all_moshtarek.Rows[i]["gharardad"]);

            // منطق کار اینکه وضعت تجاری بودن یک مشترک رو از فرم دریافت کنیم  
            if (is_Updating_ghabz == false)
            {
                tejari = Convert.ToBoolean(dt_all_moshtarek.Rows[i]["tejari"]);
                metraj = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["metraj"]);

            }


        }


        public void MoshtarekinList_and_LastGhobooz()
        {
            string Dt_all_ghabzof_Previous_dorehCommand = "select gharardad,mande,kasr_hezar,bestankari from sharj_ghabz  where dcode=" + (doreh - 1).ToString()
                , MoshtarekinCommad = "select * from moshtarekin";
            if (New_or_Update_TakGhabz_Just_for_1_Moshtarek == -1)
            {
                MoshtarekinCommad += " where have_sharj_ghabz=1";
            }
            else
            {
                Dt_all_ghabzof_Previous_dorehCommand += " and gharardad=" + New_or_Update_TakGhabz_Just_for_1_Moshtarek;
                MoshtarekinCommad += " where gharardad=" + New_or_Update_TakGhabz_Just_for_1_Moshtarek;
            }
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                MoshtarekinCommad += " and metraj<>0 and ISNULL(tarikh_gharardad,1)<>1";
            }


            dt_all_moshtarek = ClsMain.GetDataTable(MoshtarekinCommad);
            Dt_all_ghabzof_Previous_doreh = ClsMain.GetDataTable(Dt_all_ghabzof_Previous_dorehCommand);



        }


        public void Start_Process()
        {
            start_Process = DateTime.Now;
        }


        public void Get_day_Length_of_doreh()
        {
            tedad_rooz = ClsMain.Days_between_tarikh_ha(
               Convert.ToDateTime(dt_doreh_info.Rows[0]["end_time"]),
               Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"])
               );
        }
        public void Set_Doreh_Field_info()
        {
            if (is_Updating_ghabz == false)
            {
                // در هنگام ویرایش امکان تغییر تعداد روزهای دوره وجود دارد . در نتیجه فقط در هنگام صدور قبض جدید از تابع زیر استفاده میکنیم
                Get_day_Length_of_doreh();
                Tarefe = Convert.ToInt32(dt_doreh_info.Rows[0]["mablagh_sharj"]);
                darsad_maliat = Convert.ToInt32(dt_doreh_info.Rows[0]["darsad_maliat"]);

            }

            doreh = Convert.ToInt32(dt_doreh_info.Rows[0]["dcode"]);
            mohlat_pardakht_ghabz = Convert.ToDateTime(dt_doreh_info.Rows[0]["mohlat_pardakht"]);
            zarib_tejari = Convert.ToDecimal(dt_doreh_info.Rows[0]["ZaribTejari"]);

        }

        public void Fill_Necessary_Information(int new_sharj_doreh = 0)
        {
            // در صورتی که این تابع مقدار ورودی داشته باشد 
            // قصد داریم برای یک دوره خاص برای مشترک قبض صادر کنیم
            dt_Sharj_info = ClsMain.GetDataTable("select top(1) * from sharj_info");

            if (is_Updating_ghabz == false)
            {
                if (new_sharj_doreh == 0)
                    dt_doreh_info = clsDoreh.Get_last_Sharj_Doreh();
                else
                    dt_doreh_info = clsDoreh.Get_last_Sharj_Doreh(new_sharj_doreh);
                    
            }
            else
            {
                if (doreh == -10)
                    throw new Exception("فیلد دوره مقدار دهی نشده است ");
                dt_doreh_info = clsDoreh.Get_last_Sharj_Doreh(doreh);
            }
            Set_Doreh_Field_info();


            if (doreh == 1)
                sharj_bedehi_from_Past = ClsMain.GetDataTable("select * from sharj_bedehi_bestankar_First_time");

        }



        public void ClearAllOfGhabz()
        {
            if (is_Updating_ghabz == false)
            {
                
                gharardad =
                kasr_hezar_from_current_doreh =
                kasr_hezar_from_last_doreh =
                vaziat_ghabz = 0;

                metraj =
                bedehkar =
                bestankar =

                sharj_takhfif =
                mandeh =
                hazineh_mablagh =
                hazineh_maliat =
                hazineh_jarimeh =
                hazineh_sayer =
                Pardakhti_in_Doreh =
                mablagh_kol = 0;


                tejari = was_bestankar_in_sodoorGhabz = false;
                tozihat = tozihat_jarimeh = tozihat_sayer = "";
                tarikh_sodoor = DateTime.Now;
                last_pardakht = null;
                shenase_ghabz = shenase_prdakht = "";
                gcode = "";
            }

        }


    }
}
