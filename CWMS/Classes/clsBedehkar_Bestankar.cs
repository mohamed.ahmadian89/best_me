﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMS.Classes
{
    class clsBedehkar_Bestankar
    {
        public decimal Pardakhti_in_Doreh, bestankar, mablagh_kol, mandeh, bestankar_FromLastDoreh;
        public int kasr_hezar_from_current_doreh, vaziat_ghabz, gharardad;
        public string tozihat, gcode;
        public bool is_Updating_ghabz, was_bestankar_in_sodoorGhabz;

        public bool IsChangeInGhabzPardakht = false;


        public clsBedehkar_Bestankar(decimal mablagh_kol_t, string gcode_t, decimal bestankar_FromLastDoreh_t, string tozihat_t, bool is_Updating_ghabz_t = false)
        {

            mablagh_kol = mablagh_kol_t;
            gcode = gcode_t;
            bestankar_FromLastDoreh = bestankar_FromLastDoreh_t;
            kasr_hezar_from_current_doreh = 0;
            is_Updating_ghabz = is_Updating_ghabz_t;
            string tablename = clsDoreh.gcode_tableName(gcode);
            // این بدین مفهوم است که تابع از جایی فراخوین شده که به توضیحات قبض دسترسی نداریم و باید از جدول بخوانیم
            if (tozihat_t == "GetIT")
                tozihat_t = Classes.ClsMain.ExecuteScalar("select tozihat from " + tablename + " where gcode=" + gcode).ToString();
            tozihat = tozihat_t;
            vaziat_ghabz = 0;
        }

        public clsBedehkar_Bestankar(string gcode_t)
        {
            string tablename = clsDoreh.gcode_tableName(gcode_t);
            System.Data.DataTable dt_ghabzInfo = ClsMain.GetDataTable("select * from " + tablename + " where gcode=" + gcode_t);
            mablagh_kol = Convert.ToDecimal(dt_ghabzInfo.Rows[0]["mablaghkol"]);
            gcode = gcode_t;
            tozihat = dt_ghabzInfo.Rows[0]["tozihat"].ToString();
            bestankar_FromLastDoreh = Convert.ToDecimal(ClsMain.ExecuteScalar("select bestankari from " + tablename + " where gcode=" + gcode_t + " and dcode=" + (clsDoreh.gcode_dcode(gcode_t) - 1).ToString()));
            kasr_hezar_from_current_doreh = 0;
            is_Updating_ghabz = false;
            vaziat_ghabz = 0;

        }


        public void Check_Bedehkar_Bestankar_Status()
        {


            /*
             این تابع مجموع پرداختی های کاربر را بدست می اورد 
             در نتیجه تنها پارامتر مورد نیاز ان بستانکاری از دوره قبل می باشد 

             */
          


            Pardakhti_in_Doreh = Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
            "select isnull(sum(mablagh),0) from ghabz_pardakht"
            + " where gcode=" + gcode + " and vaziat=1"
            ));


            bestankar = Pardakhti_in_Doreh + bestankar_FromLastDoreh;

            if (bestankar >= mablagh_kol)
            {

                bestankar -= mablagh_kol;
                mandeh = 0;
                kasr_hezar_from_current_doreh = 0;

                // قبض کاربر به طور کامل پرداخت شده است 
                vaziat_ghabz = 1;
                if (IsChangeInGhabzPardakht)
                    tozihat += System.Environment.NewLine + "پرداخت کامل قبض بواسطه مجموع پرداختی ها + بستانکاری از دوره قبل";

                // بدین معنا که در حال انجام فرایند صدور قبض برای همه مشترکین هستیم
                // و کاربر مبلغ بستاانکاری از مبلغ قبض بیشتر شده 
                // در این نسخه مبلغ قبض را صفر نمی کنیم بلکه ما به التفاوت را به عنوان بستانکاری ربای کاربر ثبت می کنیم
                if (is_Updating_ghabz == false)
                    was_bestankar_in_sodoorGhabz = true;
                // این بدین معنی است که کاربر در هنگام صدور قبوض دوره بستانکار  بوده است  . 

            }
            else
            {
                // در این الگوریتم مبلغ کل ثابت خواهد بود
                // و بروی فیلد مانده مانور خواهیم داد

                if (bestankar != 0 || Pardakhti_in_Doreh != 0)
                {
                    vaziat_ghabz = 2;
                }

                mandeh = mablagh_kol - bestankar;
                bestankar = 0;

                if (mandeh < 1000)
                {
                    kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh);
                    mandeh = 0;
                    vaziat_ghabz = 1;
                }
                else
                {

                    kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh % 1000);
                    mandeh -= kasr_hezar_from_current_doreh;

                }





            }




        }
    }
}
