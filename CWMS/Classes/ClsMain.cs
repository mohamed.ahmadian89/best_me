﻿using System;

using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;
using Snippet;
using System.Windows.Forms;

namespace CWMS.Classes
{






    public class CodeGhabzGenerateException : Exception
    {
        public CodeGhabzGenerateException(string exceptionCode)
            : base(exceptionCode)
        { }
    }






    class ClsMain
    {



        public static string Shahrak = "صنعتی عباس آباد";

        public static FrmMain FrmMainAccess = null;
        //public static string ConnectionStr = @"Data Source=.;Initial Catalog=CWMS;Integrated Security=True";
        public static string ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.con1"].ConnectionString;
        public static string SherkartCode_MelliAccount = System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.SherkatCode"].ConnectionString;

        public static MainDataSest.moshtarekinDataTable MoshtarekDT = null;

        public static CrysReports.ReportDataSet.settingRow ShahrakSetting = null;
        public static CrysReports.ReportDataSet.settingDataTable ShahrakSetting_data_table = null;

        //public static MainDataSest.settingRow ShahrakSetting = (MainDataSest.settingRow)ClsMain.GetDataTable("select top(1)* from setting").Rows[0];

        public static bool NewTakGabzStat = false;

        public static int Days_between_tarikh_ha(DateTime End, DateTime Start, string gcode = "-1")
        {
            int result = 0;
            try
            {
                TimeSpan Difference = End - Start;
                result = Difference.Days + 1;
            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert, gcode);
                result = -1;
            }
            return result;

        }


        public static void IsChequeTime()
        {

            ClsMain.ChangeCulture("e");
            int cheqcount = Convert.ToInt32(ClsMain.ExecuteScalar("select count(*) from ghabz_pardakht where tarikh_sarresid>='" + DateTime.Now.Date + "' and tarikh_sarresid<='" + DateTime.Now.AddDays(2).Date + "'"));
            if (cheqcount != 0)
                Payam.Show("کاربر گرامی ! طی  دو روز آینده شما " + cheqcount.ToString() + " چک دارید. جهت بررسی به قسمت مدیریت چک ها مراجعه کنید ");
            ClsMain.ChangeCulture("f");

        }


        public ClsMain()
        {

        }

        public static void Add_mablagh_to_ghabz(string gcode, decimal mablahkol, decimal bestankari_from_last_doreh, string table_name, string gharardad, decimal mablagh, string dcode)
        {
            Classes.ClsMain.ExecuteNoneQuery("insert into ghabz_pardakht (gharardad,is_ab,dcode,tarikh_sabt,tarikh_vosool,mablagh,gcode,tozihat,nahve_pardakht,vaziat) values "
                         + "(" + gharardad + "," + (table_name.StartsWith("ab") ? 1 : 0) + "," + dcode + ",'" + DateTime.Now + "','" + DateTime.Now + "'," + mablagh + "," + gcode + ",'پرداخت به صورت کلی','بانک'"
                         +
                         ",1)"

                         );

            Classes.clsBedehkar_Bestankar cls = new Classes.clsBedehkar_Bestankar(
                         mablahkol,
                         gcode,
                         bestankari_from_last_doreh
                         , "");
            cls.Check_Bedehkar_Bestankar_Status();
            Classes.ClsMain.ExecuteNoneQuery("update " + table_name + " set mande=" + cls.mandeh + ",vaziat=" + cls.vaziat_ghabz + ",bestankari=" + cls.bestankar + ",kasr_hezar=" + cls.kasr_hezar_from_current_doreh + "  where gcode=" + gcode);


        }

        public struct ResultSharj
        {
            public bool Alert;
            public int Dcode;
        }

        /// <summary>
        /// Generate GhabzCode
        /// </summary>
        /// <param name="dcode">کد دوره حداکثر 3 رقمی</param>
        /// <param name="isAb">قبض آب ، ترو ، قبض شارژ فالس</param>
        /// <param name="gharardad">شماره قراردادحداکثر5رقمی</param>
        /// <returns>عدد کد قبض تولید شده</returns>
        /// <exception cref="CodeGhabzGenerateException">CodeGhabzGenerateException</exception>
        public static string CodeGhabzGenerate(int dcode, bool isAb, int gharardad)
        {
            if (dcode.ToString().Length > 3)
            {
                throw new CodeGhabzGenerateException("طول کد دوره بیشتر از 3 است. حداکثر مقدار ممکن 999 میباشد");
            }
            if (gharardad.ToString().Length > 5)
            {
                throw new CodeGhabzGenerateException("کد قرارداد باید 1 تا 5 رقمی باشد");
            }

            string dcodePart = dcode.ToString();
            string gharardadPart = gharardad.ToString();
            while (dcodePart.Length < 3)
                dcodePart = dcodePart.Insert(0, "0");
            while (gharardadPart.Length < 5)
                gharardadPart = gharardadPart.Insert(0, "0");


            if (isAb)
                return Convert.ToString("1" + dcodePart + gharardadPart);
            else
                return Convert.ToString("2" + dcodePart + gharardadPart);


        }

        public static string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "همین الان";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "یک دقیقه قبل";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0}" + " دقیقه قبل ",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "یک ساعت قبل";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0}" + " ساعت قبل ",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "دیروز";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0}" + " روز قبل ",
                dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0}" + " هفته قبل ",
                Math.Ceiling((double)dayDiff / 7));
            }
            return null;
        }

        public static void LogIt(Exception ex)
        {
            ClsMain.ExecuteNoneQuery(@"insert into logs values  ('" + ClsMain.Shahrak + "','" + new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString() + "','" + System.Text.RegularExpressions.Regex.Escape(ex.ToString().Replace("'", "").Replace("(", "").Replace(")", "")) + "')");
        }

        public static string GetValidMiladiDateTimeAsString(string DateTimeValue)
        {
            return FarsiLibrary.Utils.PersianDateConverter.ToGregorianDateTime(DateTimeValue).ToString("MM/dd/yyyy");
        }

        public static DateTime GetValidMiladiDateTimeAsDateTime(string DateTimeValue)
        {
            return FarsiLibrary.Utils.PersianDateConverter.ToGregorianDateTime(DateTimeValue);
        }

        public static DataTable ProcessInMoshtarekinData(string Command)
        {
            DataView dv = new DataView(Classes.ClsMain.MoshtarekDT);
            dv.RowFilter = Command;
            return dv.ToTable();
        }

        public static void ExecuteStoredProcedure(string SPname)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionStr;
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = SPname;
            com.ExecuteNonQuery();
            con.Close();
        }

        public static void ExecuteNoneQuery_UploadWeb(string Command, string connetionString)
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = connetionString;

            try
            {
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandText = Command;
                com.CommandTimeout = 500;
                com.ExecuteNonQuery();
                con.Close();
              
            }
            catch (Exception ex)
            {

                MessageBox.Show("خطای دیتابیس. لطفا با پشتیبان هماهنگ نمایید");
                Classes.ClsMain.logError(ex);
            }
            finally
            {
                con.Close();
            }

        }




        public static void ExecuteNoneQuery_remotely(string Command, string connetionString,string type,DateTime start_it,int count)
        {

            SqlConnection con = new SqlConnection();
            con.ConnectionString = connetionString;

            try
            {
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandText = Command;
                com.CommandTimeout = 500;
                com.ExecuteNonQuery();
                con.Close();
                if(type=="addlist")
                {
                    MessageBox.Show("برگه های جدید با موفقیت در سامانه نگهبانی ذخیره شدند");
                    clsBargeh.client_delete_addlist(0);
                    clsBargeh.Log_transfer_report("برگه های جدید" + "--" + count, start_it, DateTime.Now, 1);
                }
                else if (type == "deletelist")
                {
                    clsBargeh.client_delete_deletelist(0);
                    MessageBox.Show("درخواست های مربوطه با موفقیت از سامانه نگهبانی حذف شدند");
                    clsBargeh.Log_transfer_report("برگه های حذف شده" + "--" + count, start_it, DateTime.Now, 1);
                }
                else if (type == "blocklist")
                {
                    Classes.clsBargeh.client_delete_blocklist(0);
                    MessageBox.Show("اطلاعات مربوط به برگه های غیرفعال/فعال با موفقیت در بانک اطلاعاتی نگهبانی ذخیره شد");
                    clsBargeh.Log_transfer_report("برگه های فعال " + "--" + count, start_it, DateTime.Now, 1);
                    Classes.ClsMain.ExecuteNoneQuery(clsBargeh.should_reload_data_in_client_command.Substring(1));

                }



            }
            catch (Exception ex)
            {

                MessageBox.Show("خطای دیتابیس. لطفا با پشتیبان هماهنگ نمایید");
                Classes.ClsMain.logError(ex);
            }
            finally
            {
                con.Close();
            }

        }




        public static void ExecuteNoneQuery(string Command, string connetionString = "")
        {

            SqlConnection con = new SqlConnection();
            if (connetionString == "")
                con.ConnectionString = ConnectionStr;
            else
                con.ConnectionString = connetionString;

            try
            {
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandText = Command;
                com.CommandTimeout = 500;
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                if (connetionString == "")
                    Payam.Show("خطای دیتابیس. لطفا با پشتیبان هماهنگ نمایید");
                else
                    MessageBox.Show("خطای دیتابیس. لطفا با پشتیبان هماهنگ نمایید");

                Classes.ClsMain.logError(ex);
            }
            finally
            {
                con.Close();
            }

        }

        public static void ChangeCulture(string type)
        {
            if (type == "f")
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("fa-IR");
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
        }

        public static object ExecuteScalar(string Command, string connetionString = "")
        {
            SqlConnection con = new SqlConnection();
            if (connetionString == "")
                con.ConnectionString = ConnectionStr;
            else
                con.ConnectionString = connetionString;
            try
            {
                con.Open();
                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandText = Command;
                object Result = com.ExecuteScalar();
                con.Close();
                return Result;
            }
            catch (Exception ex)
            {
                Payam.Show("خطای دیتابیس. لطفا با پشتیبان هماهنگ نمایید");
                Classes.ClsMain.logError(ex);
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        public static SqlCommand GetCommandObject(string Command)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionStr;
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = Command;
            return com;
        }

        public static SqlDataReader GetDataReader(string Command)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionStr;
            con.Open();
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = Command;
            SqlDataReader dr = com.ExecuteReader();
            return dr;
        }

        public static DataTable GetDataTable(string Command, string connetionString = "")
        {
            SqlConnection con = new SqlConnection();
            if (connetionString == "")
                con.ConnectionString = ConnectionStr;
            else
                con.ConnectionString = connetionString;
            try
            {
                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandText = Command;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = com;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex, "getDatatable : " + Command);
                Payam.Show("خطا در خواندن اطلاعات دیتابیس. لطفا با پشتیبان هماهنگ نمایید");
                return new DataTable();
            }
            finally
            {
                con.Close();
            }
        }

        public static string GetCurrentPersianDate()
        {
            return System.DateTime.Now.ToShortTimeString();
        }

        public static void logError(Exception ex)
        {
            logError(ex, ex.Source);
        }

        public static void logError(Exception ex, string location)
        {
            Classes.ClsMain.ChangeCulture("e");

            //ExecuteNoneQuery("insert into errors (errorText,exceptionType,date,location)" +
            //    "values('" + ExcpToStr.getLongString(ex) + "', '" + ex.GetType().ToString() + "', '" + DateTime.Now + "', '" + location + "')");
            string Command = "insert into errors (errorText,exceptionType,date,location) values(@error, @type, @date, @location)";

            SqlCommand com = new SqlCommand(Command);

            com.Parameters.AddWithValue("@error", ExcpToStr.getLongString(ex));
            com.Parameters.AddWithValue("@type", ex.GetType().ToString());
            com.Parameters.AddWithValue("@date", DateTime.Now);
            com.Parameters.AddWithValue("@location", location);

            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionStr;
            con.Open();
            com.Connection = con;
            com.ExecuteNonQuery();
            con.Close();

            Classes.ClsMain.ChangeCulture("f");

        }

        static CrysReports.ReportDataSet settingRDS;
        static CrysReports.ReportDataSet2 settingRDS2;

        public static CrysReports.ReportDataSet getReportDatasetWithSetting()
        {
            if (settingRDS != null)
                return (CrysReports.ReportDataSet)settingRDS.Copy();
            else
            {
                using
                    (CrysReports.ReportDataSetTableAdapters.settingTableAdapter ta =
                        new CrysReports.ReportDataSetTableAdapters.settingTableAdapter())
                {
                    settingRDS = new CrysReports.ReportDataSet();
                    ta.Fill(settingRDS.setting);
                    return (CrysReports.ReportDataSet)settingRDS;
                }
            }
        }

        public static CrysReports.ReportDataSet2 getReportDatasetWithSetting2()
        {
            if (settingRDS2 != null)
                return (CrysReports.ReportDataSet2)settingRDS2.Copy();
            else
            {
                using
                    (CrysReports.ReportDataSet2TableAdapters.settingTableAdapter ta =
                        new CrysReports.ReportDataSet2TableAdapters.settingTableAdapter())
                {
                    settingRDS2 = new CrysReports.ReportDataSet2();
                    ta.Fill(settingRDS2.setting);
                    return (CrysReports.ReportDataSet2)settingRDS2.Copy();
                }
            }
        }

        public static bool isClosed(string frmName)
        {
            if (!Program.openingWindows.Contains(frmName))
                return true;
            return false;
        }

        public static void openForm(string frmName)
        {
            Program.openingWindows.Add(frmName);
        }

        public static void closeForm(string frmName)
        {
            Program.openingWindows.Remove(frmName);
        }
    }
}