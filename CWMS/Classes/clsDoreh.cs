﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CWMS.Classes
{
    class clsDoreh
    {
        public static int Get_last_Sharj_dcode()
        {
            return Convert.ToInt32(ClsMain.ExecuteScalar("select isnull(max(dcode),0) from sharj_doreh"));
        }

        public static string gcode_tableName(string gcode)
        {
            if (gcode.StartsWith("1"))
                return "ab_ghabz";
            return "sharj_ghabz";
        }
        public static int gcode_dcode(string gcode)
        {
            return Convert.ToInt32(gcode.ToString().Substring(1, 3));
        }

        public static DataTable Get_last_Sharj_Doreh(int dcode = -1)
        {
            if (dcode != -1)
                return Classes.ClsMain.GetDataTable("select top(1) * from sharj_doreh where dcode=" + dcode + " order by dcode desc");
            return Classes.ClsMain.GetDataTable("select top(1) * from sharj_doreh order by dcode desc");

        }


        public static int Get_last_Ab_dcode()
        {
            return Convert.ToInt32(ClsMain.ExecuteScalar("select isnull(max(dcode),0) from ab_doreh"));
        }

        public static int Get_last_Ab_dcode_of_moshtarek(int gharardad)
        {
            return Convert.ToInt32(ClsMain.ExecuteScalar("select isnull(max(dcode),0) from ab_ghabz where gharardad=" + gharardad));
        }

        public static DataTable Get_last_Ab_Doreh(int doreh = -1)
        {
            if (doreh != -1)
                return Classes.ClsMain.GetDataTable("select  * from ab_doreh where dcode=" + doreh);
            return Classes.ClsMain.GetDataTable("select top(1) * from ab_doreh order by dcode desc");

        }

        public static DataTable Get_Sharj_info()
        {
            return Classes.ClsMain.GetDataTable("select top(1) * from sharj_info order by id desc");
        }

        public static DataTable Get_Ab_info()
        {
            return Classes.ClsMain.GetDataTable("select top(1) * from ab_info order by id desc");
        }

        public static Doreh_lenght_result Doreh_Length(DateTime StartTime, int Manth_betweeb_Doreh, int Add_Dayes_For_Mohalt_Pardakht)
        {
            Classes.ClsMain.ChangeCulture("f");
            Doreh_lenght_result Result = new Doreh_lenght_result();



            FarsiLibrary.Utils.PersianCalendar p = new FarsiLibrary.Utils.PersianCalendar();
            Result.End_Of_Doreh = p.AddMonths(StartTime, Manth_betweeb_Doreh);
            Result.Mohlat_pardakht = p.AddMonths(StartTime, Manth_betweeb_Doreh).AddDays(Add_Dayes_For_Mohalt_Pardakht);



            return Result;



        }

        public struct Doreh_lenght_result
        {
            public DateTime End_Of_Doreh;
            public DateTime Mohlat_pardakht;
        }




    }
}
