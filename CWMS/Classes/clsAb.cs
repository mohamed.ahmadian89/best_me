﻿using FarsiLibrary.Utils;
using Snippet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using CrystalDecisions.Shared;
using System.Diagnostics;

namespace CWMS.Classes
{
    class clsAb
    {
        public static string Dcode = "1";
        public static int allMoshtarekCount = 0;
        public static int Success_Tedad_ghobooz_saderShode = 0, Fail_Tedad_ghobooz_saderShode = 0;



        public string Generate_Ghabz(clsGhabz ghabzInfo = null)
        {
            int temp_doreh = 0, temp_gharardad = 0;
            Classes.ClsMain.ChangeCulture("e");
            if (ghabzInfo == null)
                ghabzInfo = new clsGhabz();
            if (ghabzInfo != null)
                // شرط زیر فقط هنگامی درست خواهد بود که قصد داریم برای یک مشترک قبض برا ی یک دوره خاص صادر کنیم
                if (ghabzInfo.tarefe_ab == -10)
                {
                    temp_doreh = ghabzInfo.doreh;
                    temp_gharardad = ghabzInfo.gharardad;
                    ghabzInfo = new clsGhabz();
                    ghabzInfo.doreh = temp_doreh;
                    ghabzInfo.New_or_Update_TakGhabz_Just_for_1_Moshtarek = temp_gharardad;
                    ghabzInfo.gharardad = temp_gharardad;

                }


            ghabzInfo.Start_Process();

            // دریافت کلیه اطلاعات مورد نیاز  از بانک اطلاعاتی جهت صدور قبض 

            ghabzInfo.Fill_Necessary_Information(temp_doreh);




            // -------------- لیست مشترکینی که برای انها قبض صادر خواهد شد 

            ghabzInfo.MoshtarekinList_and_LastGhobooz();


            Classes.MultipleCommand Multiple = new MultipleCommand("Soodor_Ghobboz_ab");
            try
            {

                for (int i = 0; i < ghabzInfo.dt_all_moshtarek.Rows.Count; i++)
                {
                    //------------------------------------------------------------------
                    // کلیه فیلد ها به غیراز فیلدهای مربوط به دوره اطلاعاتشان پاک خواهد شد 
                    ghabzInfo.ClearAllOfGhabz();

                    // مقداردهی اولیه متغیرهای کلاس با اطلاعات مشترک
                    ghabzInfo.Get_Moshtarek_Info(i);

                    ghabzInfo.Ghabz_code__gcode();

                    // بدست اوردن اطلاعات قبض دوره قبل مشترک

                    ghabzInfo.Get_last_abGhabz_of_Moshtarek();

                    //-------------------------------------------------------------------------------------------------------------------------------------                    
                    //-- خواندن قرائت جدید مشترک از جدول کنتور خوان  - که می تواند به صورت دستی وارد شده باشد یا از طریق دستگاه کنتورخوان

                    if (ghabzInfo.have_value_for_moshtarek__in_kontorKhan())
                    {
                        ghabzInfo.Calculate_mizan_masraf();
                    }
                    else
                        continue;
                    //----------------- محاسبه میزان مصرف --------------------------

                    //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------
                    ghabzInfo.Calculate_hazineh_masraf();
                    //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


                    //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----
                    if (ghabzInfo.kontor_khan_state_of_moshtarek.kharab)
                    {
                        if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
                            ghabzInfo.Calculate_hazineh_masraf_of_kharab_state(AVG_of_Last2_abGhabz(ghabzInfo.doreh, ghabzInfo.gharardad));
                        else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
                            ghabzInfo.Calculate_hazineh_masraf_of_kharab_state(
                                shahrak_classes.shams_abad.Hazineh_ab_In_kharab_kontor_status(
                            ghabzInfo.gharardad, ghabzInfo.doreh, ghabzInfo.tarefe_ab)
                                );
                        else
                            ghabzInfo.Calculate_hazineh_masraf_of_kharab_state(AVG_of_Last2_abGhabz(ghabzInfo.doreh, ghabzInfo.gharardad));

                    }




                    //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----


                    // محاسبه آبونمان
                    ghabzInfo.Moshtarek_Aboonman();
                    //------------------------------------------------------------------            

                    // اعمال ضرایب تجاری و ساخت وساز به مبلغ آب
                    ghabzInfo.Check_darhal_sakht_state();
                    ghabzInfo.Check_tejari_state();
                    //------------------------------------------------------------------            

                    // مصارف عمومی و فاضلاب 
                    //------------------------------------------------------------------            
                    ghabzInfo.Calculate_masraf_omoomi();
                    ghabzInfo.Calculate_Fazelab();
                    ghabzInfo.Calculate_Taviz_kontor();

                    //------------------------------------------------------------------            

                    //جمع کلیه هزینه های دوره جاری 
                    ghabzInfo.Add_MablaghKol_Masraf_omoomi();
                    ghabzInfo.Add_MablaghKol_Hazineh_Ensheab();
                    ghabzInfo.Add_MablaghKol_Hazineh_Mablagh();
                    ghabzInfo.Add_MablaghKol_Jarimeh();
                    ghabzInfo.Add_MablaghKol_Sayer();
                    ghabzInfo.Add_MablaghKol_Hazineh_Fazelab();
                    ghabzInfo.Add_MablaghKol_Hazineh_Taviz_kontor();


                    //-------------------------------------------------------------------
                    ghabzInfo.Calculate_Maliat();
                    ghabzInfo.Add_MablaghKol_Hazineh_Maliat();
                    //-------------------------------------------------------------------

                    // هزینه های انتقال یافته از دوره قبل به این دوره
                    ghabzInfo.Add_MablaghKol_Kasr_HezarFrom_LastDoreh();
                    ghabzInfo.Add_MablaghKol_Bedehi();
                    //-------------------------------------------------------------------

                    ghabzInfo.Check_Bedehkar_Bestankar_Status();
                    ghabzInfo.Ghabz_Shenase();



                    //------------------------------------------------------------------            
                    ghabzInfo.Add_MoshtarekGhabz_to_GhabzList(Multiple);
                    //clsFactor.Check_ab_factor(ghabzInfo, "ab_ghabz", 0);



                }


                return ghabzInfo.Generate_AllGhabz_of_GhabzList(Multiple);


            }
            catch (Exception ex)
            {

                ClsMain.logError(ex, "ijad ghobooz avalie AB");
                Payam.Show("بروز خطا در هنگام صدور قبض");
                return "fail";
            }
        }




        public static void print_Ghabz_Multi(System.ComponentModel.BackgroundWorker BGWorker, System.Windows.Forms.ProgressBar prog, string dcode, int FirstGharardad, int LastGharardad, int moshtarek_active_state, bool Sort_by_gharardad, bool print_ghabz_which_not_printed)
        {



            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSet dsTemp = ClsMain.getReportDatasetWithSetting();

            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter MoshtarekinTa = new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter();
            ta1.FillByGharardadRange(dsTemp.ab_ghabz, Convert.ToInt32(dcode), Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));

            if (Sort_by_gharardad)
                MoshtarekinTa.FillByGharardad_Gharardad_sort(ds.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));
            else
                MoshtarekinTa.FillByGharardad_Address_Sort(ds.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));


            string tarikh = "";
            string mohlat_pardakht = "";

            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = null;
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
            {
                report1 = new CrysReports.abbas_abad.ab_ghabz_no_bg();
            }
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
            {
                report1 = new CrysReports.paytakht.ab_ghabz_by_bg();
            }
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
            {
                report1 = new CrysReports.shams_abad.ab_ghabz_by_bg();
            }



            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);

            int gharardad = 0;
            string LostGharardad = "";


            System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
            doctoprint.PrinterSettings.PrinterName = "ghabzPrinter";
            for (int i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count; i++)
            {
                int rawKind = 0;
                if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == "CHEQUEPRINT")
                {
                    rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                    report1.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)rawKind;
                    break;
                }
            }


            PageMargins p22 = new PageMargins();
            p22.topMargin = p22.bottomMargin = p22.leftMargin = p22.rightMargin = 0;
            report1.PrintOptions.ApplyPageMargins(p22);

            if (dsTemp.ab_ghabz.Rows.Count == 0)
            {
                Payam.Show("اطلاعاتی جهت چاپ وجود ندارد  ");
                return;
            }





            for (int i = 0; i < ds.moshtarekin.Rows.Count; i++)
            {
                if (BGWorker.CancellationPending)
                {
                    return;
                }
                try
                {

                    gharardad = Convert.ToInt32(ds.moshtarekin.Rows[i]["gharardad"].ToString());
                    DataTable dt_ghabz = dsTemp.ab_ghabz.Where(p => p.gharardad == gharardad).CopyToDataTable();
                    if (dt_ghabz.Rows.Count == 0)
                        continue;

                    if (print_ghabz_which_not_printed)
                    {
                        if (Convert.ToBoolean(dt_ghabz.Rows[0]["printed"]))
                            continue;
                    }

                    CrysReports.ReportDataSet.moshtarekinRow MoshtarekInfo = ds.moshtarekin[i];

                    if (moshtarek_active_state == 1)
                        // فقط برای مشترکین فعال قبض چاپ شود
                        if (Convert.ToBoolean(MoshtarekInfo.vaziat) == false)
                            continue;


                    tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_ghabz.Rows[0]["tarikh"])).ToString("d");
                    mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

                    dt_ghabz.Columns.Remove("tarikh");
                    dt_ghabz.Columns.Remove("mohlat_pardakht");


                    dt_ghabz.Columns.Add("tarikh");
                    dt_ghabz.Columns.Add("mohlat_pardakht");

                    dt_ghabz.Rows[0]["tarikh"] = tarikh;
                    dt_ghabz.Rows[0]["mohlat_pardakht"] = mohlat_pardakht;

                    report1.SetDataSource(dt_ghabz);


                    report1.SetParameterValue("modir_amel", (MoshtarekInfo.Ismodir_amelNull() ? "" : MoshtarekInfo.modir_amel));
                    report1.SetParameterValue("pelak", (MoshtarekInfo.IspelakNull() ? "" : MoshtarekInfo.pelak));
                    report1.SetParameterValue("address", (MoshtarekInfo.IsaddressNull() ? "" : MoshtarekInfo.address));
                    report1.SetParameterValue("co_name", (MoshtarekInfo.Isco_nameNull() ? "" : MoshtarekInfo.co_name));
                    report1.SetParameterValue("ghataat", (MoshtarekInfo.IsghataatNull() ? "" : MoshtarekInfo.ghataat));
                    report1.SetParameterValue("masahat", (MoshtarekInfo.IsmetrajNull() ? "" : MoshtarekInfo.metraj.ToString()));

                    report1.SetParameterValue("pshodeh", "0");
                    report1.SetParameterValue("mandehHoroof", clsNumber.Number_to_Str(dt_ghabz.Rows[0]["mande"].ToString()));


                    string Doreh = dt_ghabz.Rows[0]["dcode"].ToString();
                    DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from ab_doreh where dcode=" + Doreh);

                    report1.SetParameterValue("dcodeinfo", ("از " + new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][0])).ToString("d") + " الی " +
                        new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d")).ToString()
                        );

                    report1.PrintToPrinter(1, true, 0, 0);
                    prog.Value++;
                    Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set printed=1 where gcode=" + dt_ghabz.Rows[0]["gcode"].ToString());

                }
                catch (Exception ert)
                {
                    LostGharardad += gharardad + ",";
                    ClsMain.logError(ert);
                }

            }

            if (LostGharardad.Length != 0)
            {
                Payam.Show("متاسفانه قبوض برای مشترکین زیر صادر نشده است . جهت بررسی خطا به سامانه خطا مراجعه کنید");
                Payam.Show(LostGharardad.Substring(0, LostGharardad.Length - 2));
            }





        }







        public static void RizHeasbAbForAll(AbDataset dt_ab, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshAbForAll report1)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();



            report1.SetDataSource(dt_ab);
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();


        }


        public static void RizHeasb(string start, string end, string gharardad, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshAb report1, string typeOfReport)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillBySomeDoreh(ds.ab_ghabz, Convert.ToInt32(start), Convert.ToInt32(end), Convert.ToInt32(gharardad));
            DataTable dt = ds.ab_ghabz.CopyToDataTable();
            ds.ab_ghabz.Columns.Remove("last_pardakht");
            ds.ab_ghabz.Columns.Add("last_pardakht");
            for (int i = 0; i < ds.ab_ghabz.Rows.Count; i++)
            {
                if (Convert.ToBoolean(ds.ab_ghabz.Rows[i]["has_pardakhti"]))
                {

                    try
                    {
                        ds.ab_ghabz.Rows[i]["last_pardakht"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["last_pardakht"])).ToString("d");

                    }
                    catch (Exception)
                    {
                        ds.ab_ghabz.Rows[i]["last_pardakht"] = "";
                    }
                }
                else
                    ds.ab_ghabz.Rows[i]["last_pardakht"] = "";

            }

            report1.SetDataSource(ds);

            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;

            new FrmShowReport(report1).ShowDialog();


        }




        public static void ChapAbDoreh(string dcode, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Ab_doreh report1, string typeOfReport, string gharardad)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.Ab_DorehTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.Ab_DorehTableAdapter();
            if (dcode == "all")
                ta.Fill(ds.Ab_Doreh, Convert.ToInt32(gharardad));
            else
                ta.FillByDcode(ds.Ab_Doreh, Convert.ToInt32(gharardad), Convert.ToInt32(dcode));

            ArrayList dt_times = new ArrayList();
            for (int i = 0; i < ds.Ab_Doreh.Rows.Count; i++)
            {
                dt_times.Add(ds.Ab_Doreh.Rows[i]["tarikh_vosool"]);
            }
            ds.Ab_Doreh.Columns.Remove("tarikh_vosool");
            ds.Ab_Doreh.Columns.Add("tarikh_vosool");

            for (int i = 0; i < ds.Ab_Doreh.Rows.Count; i++)
            {
                if (dt_times[i].ToString() == "")
                    ds.Ab_Doreh.Rows[i]["tarikh_vosool"] = "";
                else
                    ds.Ab_Doreh.Rows[i]["tarikh_vosool"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_times[i])).ToString("d");
            }



            report1.SetDataSource(ds);



            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("mablaghkolh", Mablaghhorrof);
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //ExportOptions exoption = new ExportOptions();
            //if (typeOfReport == "print")
            //{
            //    viewer.PrintReport();
            //}

            //else
            //{
            //    exoption.ExportFormatType = ExportFormatType.PortableDocFormat;
            //    viewer.ExportReport();
            //}
        }////-


        public static void Ab_moshtarek_pardakhti(
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.ab_pardakhti report1, DateTime start, DateTime end
            , string pstart, string pend, bool ISByTarikh, int doreh
            )
        {
            ManagementReports.DaramadDatasetTableAdapters.ab_ghabzTableAdapter ta = new ManagementReports.DaramadDatasetTableAdapters.ab_ghabzTableAdapter();
            ManagementReports.DaramadDataset ds = new ManagementReports.DaramadDataset();
            ManagementReports.DaramadDatasetTableAdapters.settingTableAdapter tasetting = new ManagementReports.DaramadDatasetTableAdapters.settingTableAdapter();
            tasetting.FillSetting(ds.setting);

            if (ISByTarikh)
                ta.Fill(ds.ab_ghabz, start, end);
            else
                ta.FillByDoreh(ds.ab_ghabz, doreh);

            DataTable dt = ds.ab_ghabz.CopyToDataTable();

            ds.ab_ghabz.Columns.Remove("آخرین پرداخت");
            ds.ab_ghabz.Columns.Add("آخرین پرداخت");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ds.ab_ghabz.Rows[i]["آخرین پرداخت"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["آخرین پرداخت"])).ToString("d");
            }





            report1.SetDataSource(ds);



            report1.SetParameterValue("shahrak_name", ClsMain.ExecuteScalar("select top(1) shahrak_name from setting"));
            report1.SetParameterValue("pstart", pstart);
            report1.SetParameterValue("pend", pend);
            report1.SetParameterValue("gtarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));


            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }


        public static void Ab_moshtarek_bestankar(
CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Ab_Bestankar report1, int doreh)
        {
            CrysReports.ReportDataSet2TableAdapters.moshtarek_abTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.moshtarek_abTableAdapter();
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();

            if (doreh == 0)
            {
                ta.FillBestankarForAll(ds.moshtarek_ab);
            }
            else
            {
                ta.FillForBestankar(ds.moshtarek_ab, doreh);
            }
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));


            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }

        public static void Ab_moshtarek_bedehkar(
CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Ab_Bedehkaran report1, int doreh, int from, int to)
        {
            CrysReports.ReportDataSet2TableAdapters.moshtarek_abTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.moshtarek_abTableAdapter();
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            if (from == -1 || to == -1)
            {
                if (doreh == 0)
                {
                    ta.FillBedehkarForAll(ds.moshtarek_ab);

                }
                else
                {
                    ta.FillForBedehkar(ds.moshtarek_ab, doreh);
                }
            }
            else
            {
                if (doreh == 0)
                {
                    ta.FillBedehkarForAllByGharardad(ds.moshtarek_ab, from, to);

                }
                else
                {
                    ta.FillForBedehkarByGhraradad(ds.moshtarek_ab, doreh, from, to);
                }
            }

            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }




        public static void ChapGheraat_For_KontorKhan_ha(
    CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.ChapGheraat report1, int doreh)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.ChapGheraatTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.ChapGheraatTableAdapter();
            ta.FillByDcode(ds.ChapGheraat, doreh);
            report1.SetDataSource(ds);
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }


        public static void ChapPishSodoor(
      CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.CheckGheraat report1)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.kontor_khanTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.kontor_khanTableAdapter();
            ta.Fill(ds.kontor_khan);
            report1.SetDataSource(ds);
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }


        public static void print_Ghabz(string gcode, string gharardad, bool is_by_image)

        {
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = null;
            if (is_by_image)
            {
                if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                {
                    report1 = new CrysReports.abbas_abad.ab_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                {
                    report1 = new CrysReports.paytakht.ab_ghabz_by_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                {
                    report1 = new CrysReports.esf_bozorg.ab_ghabz_by_bg();

                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
                {
                    report1 = new CrysReports.shams_abad.ab_ghabz_by_bg();
                }
            }
            else
            {
                if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                {
                    report1 = new CrysReports.abbas_abad.ab_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                {
                    report1 = new CrysReports.paytakht.ab_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                {
                    report1 = new CrysReports.esf_bozorg.ab_ghabz_no_bg();

                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
                {
                    report1 = new CrysReports.shams_abad.ab_ghabz_by_bg();
                }
            }

            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillByGCode(ds.ab_ghabz, Convert.ToInt32(gcode));


            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                if (Classes.ClsMain.ShahrakSetting.id == 2)
                {
                    report1 = new CrysReports.abbas_abad.ab_ghabz_by_bg();
                    try
                    {
                        ds.ghabz_image.Rows.Clear();
                        string shenase_ghabz = ds.ab_ghabz[0]["shenase_ghabz"].ToString();
                        string shenase_pardakht = ds.ab_ghabz[0]["shenase_pardakht"].ToString();
                        while (shenase_ghabz.Length < 13)
                            shenase_ghabz = "0" + shenase_ghabz;


                        while (shenase_pardakht.Length < 13)
                            shenase_pardakht = "0" + shenase_pardakht;
                        string barcode_number = shenase_ghabz + shenase_pardakht;
                        // add the column in table to store the image of Byte array type 
                        ds.ghabz_image.Rows.Add(clsBargeh.imageToByteArray(Classes.ISBN.GetImage(barcode_number.ToString(), 210, 45, 1)));
                    }
                    catch (Exception)
                    {

                    }
                }
                }

           
            
            string tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["tarikh"])).ToString("d");
            string mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");


            ds.ab_ghabz.Columns.Remove("tarikh");
            ds.ab_ghabz.Columns.Remove("mohlat_pardakht");


            ds.ab_ghabz.Columns.Add("tarikh");
            ds.ab_ghabz.Columns.Add("mohlat_pardakht");

            ds.ab_ghabz[0]["tarikh"] = tarikh;
            ds.ab_ghabz[0]["mohlat_pardakht"] = mohlat_pardakht;



            string mablaghMAndehoroof = clsNumber.Number_to_Str(ds.ab_ghabz.Rows[0]["mande"].ToString());
            report1.SetDataSource(ds);
            DataView Dt_moshtarek_info = new DataView(ClsMain.MoshtarekDT);
            Dt_moshtarek_info.RowFilter = "gharardad=" + gharardad;
            report1.SetParameterValue("co_name", Dt_moshtarek_info[0]["co_name"].ToString());
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info[0]["modir_amel"].ToString());
            report1.SetParameterValue("masahat", Dt_moshtarek_info[0]["metraj"].ToString());
            report1.SetParameterValue("pelak", Dt_moshtarek_info[0]["pelak"].ToString());
            report1.SetParameterValue("address", Dt_moshtarek_info[0]["address"].ToString());
            report1.SetParameterValue("ghataat", Dt_moshtarek_info[0]["ghataat"].ToString());

            report1.SetParameterValue("shahrak_name", ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("mandehHoroof", mablaghMAndehoroof);
            report1.SetParameterValue("pshodeh", (Convert.ToInt64(ds.ab_ghabz.Rows[0]["mablaghkol"]) - Convert.ToInt64(ds.ab_ghabz.Rows[0]["mande"])).ToString());



            string Doreh = ds.ab_ghabz.Rows[0]["dcode"].ToString();
            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from ab_doreh where dcode=" + Doreh);
            //int days = (Convert.ToDateTime(dt_doreh.Rows[0][1]) - Convert.ToDateTime(dt_doreh.Rows[0][0])).Days;
            int days = Convert.ToInt32(ds.ab_ghabz.Rows[0]["tedadRooz"]);
            report1.SetParameterValue("dcodeinfo", "از " + new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][0])).ToString("d") + " الی " +
                new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d")
                );


            viewer.ReportSource = report1;

            viewer.PrintReport();
            //new FrmShowReport(report1).ShowDialog();

        }




        public static void ChapAbGhabzSmall(string gcode, string gharardad, string mablaghhoroof,
                    CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.AbGhabzSmall report1)
        {
            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.ab_ghabzTableAdapter();
            ta.FillByGCode(ds.ab_ghabz, Convert.ToInt32(gcode));

            ds.ab_ghabz.Rows[0]["tarikh"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["tarikh"])).ToString("d");
            ds.ab_ghabz.Rows[0]["mohlat_pardakht"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.ab_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

            report1.SetDataSource(ds);
            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("tarikh", PersianDate.Now.ToString("D"));
            //report1.SetParameterValue("gozaresh_type", "تک قبض شارژ");
            report1.SetParameterValue("mablaghkolh", mablaghhoroof);

            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

        }





        public static int Get_day_ta_shoru_doreh_badd()
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime Last_doreh_end_Last_doreh_end_time = Get_last_ab_doreh_endTime();

            return (Last_doreh_end_Last_doreh_end_time - DateTime.Now).Days + 1;


        }

        public static DateTime Get_last_ab_doreh_endTime()
        {
            return Convert.ToDateTime(ClsMain.ExecuteScalar("select top(1) end_time from ab_doreh order by dcode desc"));
        }


        public static DataTable Get_last_Ab_mablagh_tasvib_shodeh()
        {
            Classes.ClsMain.ChangeCulture("e");
            return Classes.ClsMain.GetDataTable("select top(1) * from ab_mablagh where tarikh_ejra<='" + DateTime.Now + "'  order by tarikh_ejra desc");

        }




        public static int Check_new_ab_doreh_should_created_and_return_NewDcode()
        {
            try
            {
                DataTable DtLastAbDoreh = Classes.clsDoreh.Get_last_Ab_Doreh();
                if (DtLastAbDoreh.Rows.Count == 0)
                    return 1;

                DateTime NewDoreh_StartTime = Convert.ToDateTime(DtLastAbDoreh.Rows[0]["end_time"]);
                if (DateTime.Now >= NewDoreh_StartTime)
                {
                    return Convert.ToInt32(DtLastAbDoreh.Rows[0]["dcode"]) + 1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Classes.ClsMain.LogIt(ex);
                return -1;
            }

        }



        public static Boolean IsAbInfoExist()
        {
            if (ClsMain.ExecuteScalar("select count(*) from ab_info").ToString() == "0")
                return false;
            return true;
        }


        //static long GetRoundedInteger(float number)
        //{
        //    return Convert.ToInt64(Math.Round(Convert.ToDecimal(number)));
        //}
        static long GetRoundedInteger(decimal number)
        {
            return Convert.ToInt64(Math.Round(number));
        }





        public static void UpdateDorehInfoAtEndOfSodoorProcess(DateTime Start_time, DateTime End_time, int Success_Tedad_ghobooz_saderShode, int Fail_Tedad_ghobooz_saderShode, string dcode)
        {
            ClsMain.ExecuteNoneQuery("update ab_doreh set success=" + Success_Tedad_ghobooz_saderShode.ToString() + ",fails=" + Fail_Tedad_ghobooz_saderShode + ",ghabz_sodoor=1 where dcode=" + dcode);
        }






        public static decimal Moshtarek_ensheab_hazineh(float EnsheabSize, DataTable Dt_Hazineh = null)
        {

            if (Dt_Hazineh == null)
            {
                Dt_Hazineh = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");
            }
            //این فرمول هزینه را بر اساس میزان انشعاب بدست می 
            //یعنی تا یک سایز مشخص ، هزینه ان را حساب می کند

            for (int i = 0; i < Dt_Hazineh.Rows.Count; i++)
            {
                if (EnsheabSize <= Convert.ToSingle(Dt_Hazineh.Rows[i]["tasize"]))
                    return Convert.ToDecimal(Dt_Hazineh.Rows[i]["hazineh"]);
            }
            return Convert.ToDecimal(Dt_Hazineh.Rows[Dt_Hazineh.Rows.Count - 1]["hazineh"]);
        }



        public static DataTable Get_Ab_ghabz_info(string gcode)
        {
            try
            {
                return Classes.ClsMain.GetDataTable("select * from ab_ghabz where gcode=" + gcode);
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex);
                Payam.Show("خطا در خواندن اطلاعات قبوض آب. لطفا با پشتیبان هماهنگ نمایید");
                return new DataTable();
            }
        }
        public static void Lock_Ab_Ghobooz_of_Doreh_Ghabl(string CodeOfDorehGhabl)
        {
            Classes.ClsMain.ExecuteNoneQuery("update ab_ghabz set locked=1 where dcode=" + CodeOfDorehGhabl);

        }




        //public static void Tak_Ghabz(string dcode, string gharardad, int Ab_Doreh_Tarefe, float Darsad_malit, float ZaribTejari, float ZaraibDarHalSakht, DateTime Ab_Doreh_Mohlat_pardakht,
        //    long Bedehi, long sayer, long jarimeh, string sayer_tozihat, string jarime_tozihat, int kars_hezar, int Kontor_start, int kontor_end, int Aboonman,
        //    bool ghat, bool adam_gheraat, bool kharab, bool taviz, bool dor_kamel, bool Tejari, bool DarHaleSakht, float Zarib_fazelab, DateTime tarikh_gheraat)
        //{


        //    clsAb.Dcode = dcode;
        //    DataTable dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);
        //    DataTable dt_Jarime_masraf_mazad = ClsMain.GetDataTable("select * from Jarime_masraf_mazad");
        //    DataTable dt_all_Hazineh = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");

        //    //------------------------------------------------------------------
        //    SqlConnection con = new SqlConnection();
        //    SqlCommand com = new SqlCommand();
        //    con.ConnectionString = ClsMain.ConnectionStr;
        //    com.Connection = con;
        //    con.Open();


        //    //------------------------------------------------------------------



        //    try
        //    {

        //        string Moshtarek_Gharardad = "";
        //        decimal Moshtarek_EnsheabSize = 0;
        //        long bestankari = 0;
        //        string tozihat = "";
        //        long mandeh = 0;
        //        int Moshtarek_Top_Kontor_ragham = 0, Moshtarek_SaghfMasraf = 0;
        //        int vaziat_ghabz = 0;



        //        //..بدست اوردن اطلاعات اولیه مشترک 
        //        Moshtarek_Gharardad = dt_all_moshtarek.Rows[0]["gharardad"].ToString();
        //        Moshtarek_EnsheabSize = Convert.ToDecimal(dt_all_moshtarek.Rows[0]["size_ensheab"]);
        //        Moshtarek_Top_Kontor_ragham = Convert.ToInt32(dt_all_moshtarek.Rows[0]["kontor_ragham"]);
        //        Moshtarek_SaghfMasraf = Convert.ToInt32(dt_all_moshtarek.Rows[0]["masrafe_mojaz"]);




        //        int mizan_masraf = 0, masraf_mojaz = 0, masraf_gheir_mojaz = 0;
        //        long Mablagh = 0, Mablagh_mojaz = 0, Mablagh_gheir_Mojaz = 0;
        //        int kontor_feeli = kontor_end;


        //        //----------------- محاسبه میزان مصرف --------------------------

        //        if (ghat || adam_gheraat || kharab)
        //        {
        //            mizan_masraf = 0;
        //            kontor_feeli = Kontor_start;
        //        }
        //        else
        //        {
        //            if (taviz)
        //            {
        //                Kontor_start = 0;
        //                mizan_masraf = kontor_feeli = kontor_end;
        //            }
        //            else if (dor_kamel)
        //            {

        //                mizan_masraf = kontor_feeli + (Moshtarek_Top_Kontor_ragham - Kontor_start);
        //            }


        //            else
        //            {
        //                //------حالت عادی که کنتور درست کار می کنه ------------
        //                mizan_masraf = kontor_feeli - Kontor_start;
        //            }
        //        }




        //        if (mizan_masraf > Moshtarek_SaghfMasraf)
        //        {
        //            masraf_gheir_mojaz = mizan_masraf - Moshtarek_SaghfMasraf;
        //            masraf_mojaz = Moshtarek_SaghfMasraf;
        //        }
        //        else
        //        {
        //            masraf_mojaz = mizan_masraf;
        //        }

        //        //----------------- محاسبه میزان مصرف --------------------------



        //        //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------
        //        Mablagh_mojaz = masraf_mojaz * Ab_Doreh_Tarefe;
        //        Mablagh_gheir_Mojaz = Get_Masrafe_gheir_mojaz_hazineh(masraf_gheir_mojaz, dt_Jarime_masraf_mazad, Ab_Doreh_Tarefe, masraf_mojaz);
        //        Mablagh = Mablagh_mojaz + Mablagh_gheir_Mojaz;
        //        //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


        //        //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----
        //        if (kharab)
        //        {
        //            Mablagh = Mablagh_mojaz = Convert.ToInt64(AVG_of_Last2_abGhabz(Convert.ToInt32(clsAb.Dcode), Convert.ToInt32(Moshtarek_Gharardad)));
        //            Mablagh_gheir_Mojaz = 0;
        //        }
        //        //-------------------------------- در صورت خراب بودن می بایست میانگین مبلغ دو دوره قبل برای طرف حساب شود -----



        //        //------------------------------------------------------------------            
        //        // محاسبه ابونماه با توجه به سایز انشعاب
        //        long HazinehEnsheab = GetRoundedInteger(Moshtarek_ensheab_hazineh(Convert.ToSingle(Moshtarek_EnsheabSize), dt_all_Hazineh));
        //        //------------------------------------------------------------------            

        //        //TODO: نحوه اعمال مالیات -- قبل از اعمال ضرایب یا بعد از آن
        //        //مبلغ مالیات ، ضریب تجاری و ضریب در حال ساخت و ساز در مرحله دوم صدور قبض آب محاسبه خواهد شد 



        //        //--------------------- محاسبه ضریب تجاری ---------------------------
        //        if (Convert.ToBoolean(Tejari) == true)
        //            Mablagh = GetRoundedInteger(Convert.ToDecimal(Mablagh * ZaribTejari));


        //        //--------------------- محاسبه ضریب ساخت و ساز --------------------------
        //        if (Convert.ToBoolean(DarHaleSakht) == true)
        //            Mablagh = GetRoundedInteger(Convert.ToDecimal(Mablagh * ZaraibDarHalSakht));


        //        // مبلغ اب برابر است با اب بها بعلاوه هزینه انشعاب




        //        //------------------------------------------------------------------            
        //        //محاسبه بدهی کاربر بر اساس وضعیت قبوض دوره قبل






        //        //------------------------------------------------------------------                            
        //        //محاسبه مبلغ نهایی
        //        long Moshtarek_Mablagh_Kol = GetRoundedInteger(Convert.ToDecimal(Mablagh + jarimeh + sayer));


        //        //------------------------------------------------------------------                              
        //        long Maliat = GetRoundedInteger(Convert.ToDecimal(Moshtarek_Mablagh_Kol * Darsad_malit / 100));
        //        //-----------------------------------------------------------------          


        //        long MablaghFazelab = HazinehFazelab(Convert.ToDecimal(Moshtarek_Mablagh_Kol), Convert.ToSingle((Zarib_fazelab)));



        //        Moshtarek_Mablagh_Kol += Maliat + MablaghFazelab + Bedehi;





        //        //------------------------------------------------------------------            

        //        if (Cpayam.Show("مبلغ محاسبه شده جدید برابر است با : " + Moshtarek_Mablagh_Kol + " . آیا تایید میکنید؟") != System.Windows.Forms.DialogResult.Yes)
        //        {
        //            return;
        //        }



        //        //------------------------------------------------------------------            
        //        if (bestankari > Moshtarek_Mablagh_Kol)
        //        {
        //            kars_hezar = 0;
        //            bestankari = bestankari - Moshtarek_Mablagh_Kol;
        //            tozihat = "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + Moshtarek_Mablagh_Kol.ToString();

        //            Moshtarek_Mablagh_Kol = mandeh = Maliat = 0;
        //            vaziat_ghabz = 1;
        //        }
        //        else
        //        {
        //            Moshtarek_Mablagh_Kol -= bestankari;
        //            if (bestankari != 0)
        //                tozihat = "کسر قسمتی از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + bestankari.ToString();
        //            bestankari = 0;
        //            kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
        //            Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;
        //            mandeh = Moshtarek_Mablagh_Kol;

        //        }




        //        kars_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
        //        Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kars_hezar;
        //        ClsMain.ChangeCulture("e");
        //        Classes.MultipleCommand Multiple = new MultipleCommand("Soodor_TakGhabz");
        //        Multiple.addToCommandCollecton("delete from ab_ghabz where dcode=" + dcode + " and gharardad=" + gharardad + ";");
        //        try
        //        {
        //            Update_ghabz(Multiple, Moshtarek_Gharardad, clsAb.Dcode, Ab_Doreh_Tarefe.ToString(), Mablagh.ToString(),
        //                Ab_Doreh_Mohlat_pardakht.ToString(), Moshtarek_Mablagh_Kol.ToString(), Darsad_malit.ToString(), Maliat.ToString(), Bedehi.ToString(),
        //                HazinehEnsheab.ToString(), Moshtarek_EnsheabSize.ToString(), kars_hezar.ToString(),
        //                masraf_mojaz.ToString(), masraf_gheir_mojaz.ToString(), Mablagh_mojaz.ToString(), Mablagh_gheir_Mojaz.ToString(), dor_kamel, taviz, adam_gheraat, ghat, DarHaleSakht, kharab, mizan_masraf.ToString(), Kontor_start.ToString(), kontor_feeli.ToString(), sayer, jarimeh, jarime_tozihat, sayer_tozihat, Zarib_fazelab.ToString(), MablaghFazelab.ToString(), Tejari, tarikh_gheraat);

        //            Success_Tedad_ghobooz_saderShode++;
        //            if (Multiple.RunCommandCollecton("فرایند صدور تک قبض اب  - clsAb.Tak_Ghabz "))
        //                Payam.Show("اطلاعات قبض با موفقیت در سیستم ذخیره شد");
        //            else
        //                Payam.Show("بروز خطا در هنگام صدور قبض");

        //        }
        //        catch (Exception GhbazError)
        //        {
        //            Classes.ClsMain.logError(GhbazError);
        //            Payam.Show("بروز خطا در هنگام صدور قبض");

        //        }



        //        con.Close();
        //        Classes.ClsMain.ChangeCulture("f");

        //    }
        //    catch (Exception ex)
        //    {

        //        ClsMain.logError(ex, "صدور تک قبض برای مشترک");
        //        Payam.Show("بروز خطا در هنگام صدور قبض");
        //    }
        //}



        static decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return sum;
        }


        //--------------------------------------------------
        //..TODO:در مورد نحوه هزینه مصرف غیر مجاز اینجوری حساب می کنیم که به ازا متر مکعب اضافه هزینه تصاعدی می شود نه برای کل مصرف کاربر
        // این محاسبه به صورت پلکانی خواهد بود . یعنی بر اساس میزان درصد مصرف مجاز که اضافه صورت گرفته 
        // مبلغی با توجه به جدول مصارف مازاد در نظر گرفته می شود
        public static long Get_Masrafe_gheir_mojaz_hazineh(int Masrafe_gheirMojaz, DataTable dtinfo, int Tarefe, int masraf_mojaz)
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(dtinfo.Rows[0]["zarib_ab_baha"]) * Masrafe_gheirMojaz * Tarefe;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(Masrafe_gheirMojaz), Convert.ToDecimal(masraf_mojaz + Masrafe_gheirMojaz), dtinfo, Tarefe));

        }


        static decimal AVG_of_Last2_abGhabz(int dcode, int gharardad)
        {
            if (dcode == 0 || dcode == 1)
                return 0;
            else if (dcode == 2)
                return GetRoundedInteger(Convert.ToDecimal(ClsMain.ExecuteScalar("select mablaghkol from ab_ghabz where gharardad=" + gharardad + " and dcode=1")));
            else
                return GetRoundedInteger(Convert.ToDecimal(ClsMain.ExecuteScalar("select isnull(avg(mablaghkol),0) from ab_ghabz where gharardad=" + gharardad + " and dcode in (" + (dcode - 1).ToString() + "," + (dcode - 2).ToString() + ")")));

        }








        static int GetValidbool(bool val)
        {
            if (val)
                return 1;
            return 0;
        }

        public static int Get_last_doreh_dcode()
        {
            object code_doreh = ClsMain.ExecuteScalar("select max(dcode) from ab_doreh");
            if (code_doreh.ToString().Trim() == "")
                return -1;
            return Convert.ToInt32(code_doreh);

        }






        public static int Get_new_doreh_dcode()
        {
            string code_doreh = ClsMain.ExecuteScalar("select isnull(max(dcode),0) from ab_doreh").ToString();

            return Convert.ToInt32(code_doreh) + 1;
        }
        //public static int Check_new_sharj_doreh_should_created()
        //{
        //    try
        //    {
        //        int vaziat = 0;

        //        int code_doreh = Get_new_doreh_dcode();
        //        //.. درواقع مقدار یک مشخص می کند که هیچ دوره ای قبل وجود نداشته و این اولین دوره می باشد

        //        {

        //            if (code_doreh != 1)
        //            {
        //                //. اضافه کردن دوره ها به سیستم


        //                DateTime StartTime = DateTime.Now, EndTime = DateTime.Now, Mohlat = DateTime.Now;
        //                StartTime = Get_last_ab_doreh_endTime(string dcode);
        //                //.. تاریخ پایان دوره فعلی ، تاریخ شروع دوره جدید می باشد


        //                if (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 0, 0) >= StartTime)
        //                {
        //                    int[] Baze_result = Get_baze_bein_doreh_va_mohlat();
        //                    int TedadMah_Bazeh_bein_doreh = Baze_result[0], TedadRooz_mohlat_pardakht_ghabz = Baze_result[1];


        //                    float mablagh = Get_last_ab_doreh_Mablagh_sharj();
        //                    if (mablagh == -1)
        //                        return 2;


        //                    PersianCalendar pc = new PersianCalendar();
        //                    EndTime = DateTime.Now.AddMonths(TedadMah_Bazeh_bein_doreh);
        //                    Mohlat = DateTime.Now.AddDays(TedadRooz_mohlat_pardakht_ghabz);
        //                    Ijad_Doreh_jadid_ab(code_doreh, StartTime, EndTime, Mohlat, mablagh);
        //                    vaziat = 1;
        //                }


        //            }
        //        }


        //        return vaziat;
        //    }
        //    catch (Exception ex)
        //    {
        //        Classes.ClsMain.LogIt(ex);
        //        return -1;
        //    }

        //}

        public static float Get_last_ab_doreh_Mablagh_sharj()
        {
            object lastmablgh = ClsMain.ExecuteScalar("select top(1) mablagh from ab_mablagh order by id");
            if (lastmablgh.ToString().Trim() == "")
                return -1;
            return Convert.ToSingle(lastmablgh);
        }
        public static DateTime Get_last_ab_doreh_endTime(string currentDcode)
        {
            return Convert.ToDateTime(ClsMain.ExecuteScalar("select end_time from ab_doreh where dcode=" + (Convert.ToInt32(currentDcode) - 1).ToString()));
        }
        public static int[] Get_baze_bein_doreh_va_mohlat()
        {
            int[] res = new int[2];
            DataTable dt = ClsMain.GetDataTable("select tedad_mah_dar_doreh,mohlat_pardakht from ab_info where id=1");
            res[0] = (int)dt.Rows[0]["tedad_mah_dar_doreh"];
            res[1] = (int)dt.Rows[0]["mohlat_pardakht"];
            return res;
        }


        public static void Ijad_Doreh_jadid_ab_V2(int dcode, DateTime startTime, DateTime EndTime, DateTime MohlatPardakht, decimal Mablgh, string darsad_maliat, string SakhtoSazZarib, string TejariZarib, decimal Mablagh_fazelab, decimal zarib_aboonman, decimal masraf_omoomi, decimal hazineh_taviz_kontor, int doreh_should_check_in_kharab_stat = 0)
        {
            Classes.ClsMain.ChangeCulture("e");
            ClsMain.ExecuteNoneQuery(
                "insert into ab_doreh (dcode,start_time,end_time,mohlat_pardakht,mablagh_ab,darsad_maliat,ZaribTejari,ZaribSakhtoSaz,zarib_aboonman,mablagh_fazelab,masraf_omoomi,hazineh_taviz_kontor)"
                + " values (" + dcode + ",'" + startTime + "','" + EndTime + "','" + MohlatPardakht + "'," +
                Mablgh + "," + darsad_maliat + "," +
                TejariZarib + "," + SakhtoSazZarib + "," + zarib_aboonman + "," + Mablagh_fazelab + "," + masraf_omoomi + "," + hazineh_taviz_kontor + ")");

            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
            {
                ClsMain.ExecuteNoneQuery("insert into shams_abad_ab_Additional_info  (dcode,doreh_should_check_in_kharab_stat) values (" + dcode + "," + doreh_should_check_in_kharab_stat + ")");
            }
            Classes.ClsMain.ChangeCulture("f");

        }

        public static DataTable GetAbDoreh()
        {
            return ClsMain.GetDataTable("select * from ab_doreh");
        }

        public static int GetEnsheabHazienh(float Size)
        {
            int hazineh = 0;
            bool found = false;
            DataTable dtinfo = ClsMain.GetDataTable("select * from EnsheabHazineh");
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {
                if (hazineh <= Convert.ToSingle(dtinfo.Rows[i]["tasize"]))
                {
                    hazineh = Convert.ToInt32(dtinfo.Rows[i]["hazineh"]);
                    found = true;
                    break;
                }
            }

            if (found == false)
                return Convert.ToInt32(dtinfo.Rows[dtinfo.Rows.Count]["hazineh"]);

            return hazineh;
        }


    }
}