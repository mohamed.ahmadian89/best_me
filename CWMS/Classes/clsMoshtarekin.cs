﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using FarsiLibrary.Utils;
using CrystalDecisions.Shared;
using System.Globalization;

namespace CWMS.Classes
{
    class clsMoshtarekin
    {

        public static Management.user_Dataset.admin_userRow UserInfo = null;

        public static void ChapMoshtarekin(bool By_ab_gharardad = false)
        {
            CrystalDecisions.CrystalReports.Engine.ReportClass report1;
            if (By_ab_gharardad)
            {
                report1 = new CrysReports.rptMoshtarekin_by_ab_gharardad();
            }
            else
                report1 = new CrysReports.rptMoshtarekin();



            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.moshtarekinTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.moshtarekinTableAdapter();
            CrysReports.ReportDataSet2TableAdapters.settingTableAdapter ta_setting = new CrysReports.ReportDataSet2TableAdapters.settingTableAdapter();
            ta_setting.Fill(ds.setting);
            if (By_ab_gharardad)
            {
                ta.Fill_just_have_ab(ds.moshtarekin);
                DataTable dt_temp_moshtarekin = ds.moshtarekin.CopyToDataTable();

                ds.moshtarekin.Columns.Remove("tarikh_gharardad_ab");
                ds.moshtarekin.Columns.Add("tarikh_gharardad_ab");
                for (int i = 0; i < ds.moshtarekin.Rows.Count; i++)
                {
                    ds.moshtarekin.Rows[i]["tarikh_gharardad_ab"] = new FarsiLibrary.Utils.PersianDate(
                        Convert.ToDateTime(dt_temp_moshtarekin.Rows[i]["tarikh_gharardad_ab"])
                        ).ToString("d");
                }
            }
            else
                ta.Fill(ds.moshtarekin);
            report1.SetDataSource(ds);
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            new FrmShowReport(report1).ShowDialog();

        }


        public static void Report_Access(string username, string part, string action = "")
        {
            ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("insert into admin_user_access_report values ('" + DateTime.Now + "','" + username + "'," + part.ToString() + ",N'" + action + "')");
            ClsMain.ChangeCulture("f");

        }


        public static void Report_Access(string username, int part, string action = "")
        {
            ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("insert into admin_user_access_report values ('" + DateTime.Now + "','" + username + "'," + part.ToString() + ",N'" + action + "')");
            ClsMain.ChangeCulture("f");

        }



        public static bool checkLoginInfo(string username, string password)
        {
            Management.user_Dataset.admin_userDataTable dt = new Management.user_Dataset.admin_userDataTable();
            Management.user_DatasetTableAdapters.admin_userTableAdapter da = new Management.user_DatasetTableAdapters.admin_userTableAdapter();
            da.CheckLoginInfo(dt, username, password);

            if (dt.Rows.Count != 0)
            {
                UserInfo = (Management.user_Dataset.admin_userRow)dt.Rows[0];
                return true;
            }
            return false;
        }





        public static void Notify(Classes.MultipleCommand multiple, string matn, string gharardad, string subject, string dcode)
        {

            multiple.addToCommandCollecton(string.Format("insert into moshtarek_notification values ('{0}','{1}','{2}',0,{3},{4},0,0)", DateTime.Now, subject, matn, gharardad, dcode));


        }
        public static void Notify(string matn, string gharardad, string subject)
        {
            Classes.ClsMain.ChangeCulture("e");
            ClsMain.ExecuteNoneQuery(string.Format("insert into moshtarek_notification values ('{0}','{1}','{2}',0,{3},{4},0,0)", DateTime.Now, subject, matn, gharardad, clsSharj.Get_last_doreh_dcode().ToString()));
            Classes.ClsMain.ChangeCulture("f");

        }

        public static void NotifyBySms(string matn, string gharardad, string subject)
        {
            ;

        }

        static string shahrak_id()
        {
            string result = "";
            if (ClsMain.ShahrakSetting.shahrak_ID < 10)
                result = "00" + ClsMain.ShahrakSetting.shahrak_ID;
            else if (ClsMain.ShahrakSetting.shahrak_ID >= 10 && ClsMain.ShahrakSetting.shahrak_ID < 100)
                result = "0" + ClsMain.ShahrakSetting.shahrak_ID;
            else
                result =  ClsMain.ShahrakSetting.shahrak_ID.ToString();
            return result;

        }


        public static string GetNewActiveKey()
        {
            bool valid = false;
            Random rand = new Random();
            DataTable dt_all_key = ClsMain.GetDataTable("select * from activation_key");
            DataView dv = dt_all_key.AsDataView();
            string newkey = "";
            while (valid == false)
            {
                newkey = shahrak_id() +  rand.Next(1001, 9999).ToString();
                dv.RowFilter = "active_key=" + newkey;
                if (dv.Count == 0)
                    valid = true;
            }
            return newkey;
        }




        public static DataRow Akharin_ghabz_sharj(decimal gharardad, string dcode)
        {
            DataTable dt_info = ClsMain.GetDataTable("select * from sharj_ghabz where gharardad=" + gharardad.ToString(CultureInfo.InvariantCulture) + " and dcode=" + dcode);
            if (dt_info.Rows.Count == 0)
                return null;
            return dt_info.Rows[0];
        }
        public static DataRow Akharin_ghabz_ab(decimal gharardad, string dcode)
        {
            DataTable dt_info = ClsMain.GetDataTable("select * from ab_ghabz where gharardad=" + gharardad.ToString(CultureInfo.InvariantCulture) + " and dcode=" + dcode);
            if (dt_info.Rows.Count == 0)
                return null;
            return dt_info.Rows[0];
        }

        public static void New_moshtarek(string gharardad, string modir_amel, string co_name, int Vaziat, decimal metraj, string havayi, string zamini, string masrafeMojaz, string sizeEnsheab, string serialKontor, string zaribFazelab, string kargar, string serayedar, string karmand, string phone, string mobile, string tarikh, string address, bool tejarib, bool sakhtosazb, string Kontor_ragham, string pelak, string ghataat, string Code_melli, string Code_posti, String Code_eghtesai, string parvandeh, string shenase_variz_sharj, string shenase_variz_ab, DateTime? tarikh_gharardad_ab)
        {
            Classes.ClsMain.ChangeCulture("e");
            tarikh = ClsMain.GetValidMiladiDateTimeAsString(tarikh);
            int tejari = 0, sakhtosaz = 0;
            if (tejarib == true)
                tejari = 1;
            if (sakhtosazb == true)
                sakhtosaz = 1;
            string Com = "", ComByParam = "";
            if (tarikh_gharardad_ab == null)
            {
                Com = "insert into moshtarekin (gharardad,modir_amel,co_name,vaziat,metraj,havayi,zamini,masrafe_mojaz,size_ensheab,serial_kontor,zarib_fazelab,karger,serayedar,karmand,phone,mobile,tarikh_gharardad,address,tejari,darhalsakht,kontor_ragham,pelak,ghataat,code_melli,code_posti,code_eghtesadi,parvandeh,shenase_variz_sharj,shenase_variz_ab,have_ab_ghabz) values ({0},N'{1}',N'{2}',{3},{4},{5},{6},{7},{8},N'{9}',{10},{11},{12},{13},N'{14}',N'{15}','{16}',N'{17}',{18},{19},{20},'{21}','{22}',N'{23}',N'{24}',N'{25}',N'{26}',N'{27}',N'{28}',{29})";
                ComByParam = string.Format(CultureInfo.InvariantCulture, Com, gharardad, modir_amel, co_name, Vaziat, metraj, havayi, zamini, masrafeMojaz, sizeEnsheab, serialKontor, zaribFazelab, kargar, serayedar, karmand, phone, mobile, tarikh, address, tejari, sakhtosaz, Kontor_ragham, pelak, ghataat, Code_melli, Code_posti, Code_eghtesai, parvandeh, shenase_variz_sharj, shenase_variz_ab, 0);
            }
            else
            {
                Com = "insert into moshtarekin (gharardad,modir_amel,co_name,vaziat,metraj,havayi,zamini,masrafe_mojaz,size_ensheab,serial_kontor,zarib_fazelab,karger,serayedar,karmand,phone,mobile,tarikh_gharardad,address,tejari,darhalsakht,kontor_ragham,pelak,ghataat,code_melli,code_posti,code_eghtesadi,parvandeh,shenase_variz_sharj,shenase_variz_ab,tarikh_gharardad_ab) values ({0},N'{1}',N'{2}',{3},{4},{5},{6},{7},{8},N'{9}',{10},{11},{12},{13},N'{14}',N'{15}','{16}',N'{17}',{18},{19},{20},'{21}','{22}',N'{23}',N'{24}',N'{25}',N'{26}',N'{27}',N'{28}','{29}')";
                ComByParam = string.Format(CultureInfo.InvariantCulture, Com, gharardad, modir_amel, co_name, Vaziat, metraj, havayi, zamini, masrafeMojaz, sizeEnsheab, serialKontor, zaribFazelab, kargar, serayedar, karmand, phone, mobile, tarikh, address, tejari, sakhtosaz, Kontor_ragham, pelak, ghataat, Code_melli, Code_posti, Code_eghtesai, parvandeh, shenase_variz_sharj, shenase_variz_ab, tarikh_gharardad_ab);
            }


            ClsMain.ExecuteNoneQuery(ComByParam);
            Classes.ClsMain.ChangeCulture("f");

        }
        public static string Exist_gharardad(string gharardad)
        {
            string res = Convert.ToString(ClsMain.ExecuteScalar("select co_name from moshtarekin where gharardad=" + gharardad));
            return res.Trim();
        }

        public static string Check_gcode_for_doreh(string gharardad, string dcode)
        {
            string res = Convert.ToString(ClsMain.ExecuteScalar("select gcode from sharj_ghabz where gharardad=" + gharardad + " and dcode=" + dcode));
            return res.Trim();
        }




        public static double Sharj_bedehi(string gharardad)
        {
            double res = 0;
            res = Convert.ToDouble(ClsMain.ExecuteScalar("select sum(mablaghkol) from sharj_ghabz where gharardad=" + gharardad + " and (vaziat=0 or vaziat=2)"));
            return res;
        }
        public static void Pardakht_All_sharj_ghabz_ghabli(string gcode)
        {
            //..در واقع ابتدا کد مشترک پیدا می شه سپس تمام قبوض قبلی مشترک پرداخت شده می شه 
            clsSharj.Set_PardakhtShodeh_For_All_Previose_Ghabz(gcode);
        }
        public static void Set_PardakhtNaShodeh_For_All_Previose_Ghabz(string gcode)
        {
            //..در واقع ابتدا کد مشترک پیدا می شه سپس تمام قبوض قبلی مشترک پرداخت شده می شه 
            clsSharj.Set_PardakhtNaShodeh_For_All_Previose_Ghabz(gcode);
        }




    }
}
