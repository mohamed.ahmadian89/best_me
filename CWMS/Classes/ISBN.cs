﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarcodeLib;
using System.Drawing;
using System.Windows.Forms;
using Snippet;

namespace CWMS.Classes
{
    class ISBN
    {
        public static Image GetImage(string input, int width, int height, int ghabz_type = 0)
        {
            //try
            //{
            //    Barcode b = new Barcode((input * 10 + 9780000000000).ToString(), TYPE.ISBN);
            //    return b.Encode(TYPE.ISBN, (input * 10 + 9780000000000).ToString(), width, height);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ExcpToStr.getLongString(ex));
            //    return null;
            //}
            try
            {
                Barcode b = new Barcode((input).ToString(), TYPE.ISBN);
                if(ghabz_type==1)
                {
                    b.IncludeLabel = true;
                    b.BackColor = Color.LightSteelBlue;
                }
                else if(ghabz_type==2)
                {
                    b.IncludeLabel = true;
                    b.BackColor = Color.LightYellow;
                }


                return b.Encode(TYPE.CODE128C, (input).ToString(), width, height);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ExcpToStr.getLongString(ex));
                return null;
            }
        }

        public static bool GenerateAndSave(Int64 input, int width, int height, string path)
        {
            //{
            //    try
            //    {
            //        Barcode b = new Barcode((input * 10 + 9780000000000).ToString(), TYPE.ISBN);
            //        b.Encode(TYPE.ISBN, (input * 10 + 9780000000000).ToString(), width, height);
            //        b.SaveImage(path, SaveTypes.JPG);
            //        return true;
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ExcpToStr.getLongString(ex));
            //        return false;
            //    }

            try
            {
                Barcode b = new Barcode((input).ToString(), TYPE.ISBN);
                b.Encode(TYPE.CODE128, (input).ToString(), width, height);
                b.SaveImage(path, SaveTypes.JPG);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ExcpToStr.getLongString(ex));
                return false;
            }
        }
    }
}
