﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Common;
using System.Collections.Specialized;
using System.Data.SqlClient;
namespace CWMS.Classes
{
    public static class clsInternet
    {
        public static string ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.web_connection"].ConnectionString;

        public static bool uploading_moshtarekin_data = false, uploading_ab_ghabz = false, uploading_sharj_ghabz=false, uploading_setting=false,downloading_bargeh_khorooj=false, uploading_bargeh_khorooj=false;
        public static bool Check_connection(string connectionstr)
        {
            bool result = true;
            try
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionstr);
                con.Open();
                Payam.Show("ارتباط با پورتال تحت وب  با موفقیت برقرار شد");
                clsInternet.ConnectionStr = connectionstr;
                con.Close();
            }
            catch (Exception)
            {
                Payam.Show("متاسفانه ارتباط با پورتال تحت وب امکان پذیر نمی باشد");
                result = false;
            }
            return result;
        }


        static string Current_database_name()
        {
            return  ClsMain.ConnectionStr.Split(';')[1].ToString().Split('=')[1].ToString().Trim();
        }

        public static string Get_table_Script_data(string database_name, string tableName)
        {
            var server = new Server(new ServerConnection(new SqlConnection(ClsMain.ConnectionStr)));
            server.ConnectionContext.Connect();
            var database = server.Databases[database_name];
            var output = new StringBuilder();

            var table = database.Tables[tableName];




            var scripter = new Scripter(server) { Options = { ScriptData = true, ScriptSchema = false } };
            var script = scripter.EnumScript(new SqlSmoObject[] { table });
            foreach (var line in script)
                output.AppendLine(line);
            return output.ToString();
        }


        public static string Get_table_Script_data_for_ghabz(string database_name, string tableName,string dcode)
        {
            var server = new Server(new ServerConnection(new SqlConnection(ClsMain.ConnectionStr)));
            server.ConnectionContext.Connect();
            var database = server.Databases[database_name];
            var output = new StringBuilder();
           
            var table = database.Tables[tableName];
            
            var scripter = new Scripter(server) { Options = { ScriptData = true, ScriptSchema = false } };
            var script = scripter.EnumScript(new SqlSmoObject[] { table });
            foreach (var line in script)
                output.AppendLine(line);
            return output.ToString();
        }

        public static void Upload_Setting()
        {
            uploading_moshtarekin_data = true;
            //string database = clsInternet.ConnectionStr.Split(';')[1].ToString().Split('=')[1].ToString();
            string script = Get_table_Script_data(Current_database_name(), "setting");
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("truncate table setting;" + script, clsInternet.ConnectionStr);
            System.Windows.Forms.MessageBox.Show("اطلاعات مربوط به تنظیمات سیستم با موفقیت در پورتال اینترنتی بارگزاری شد");
            uploading_moshtarekin_data = false;
        }

        public static void Download_bargeh_khorooj()
        {
            downloading_bargeh_khorooj = true;
            //string max_datetime = ClsMain.ExecuteScalar("select max(tarikh) from barge_khorooj").ToString();
            System.Data.DataTable dt_darkhast=Classes.ClsMain.GetDataTable("select * from barge_khorooj_list", clsInternet.ConnectionStr);
            for (int i = 0; i < dt_darkhast.Rows.Count; i++)
            {
                clsBargeh.DarkhastJadidByMmoshtarek_web(
                    dt_darkhast.Rows[i]["gharardad"].ToString(),
                    dt_darkhast.Rows[i]["tedad"].ToString(),
                    dt_darkhast.Rows[i]["tarikh"].ToString()
                    );
            }
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("truncate table barge_khorooj_list;" , clsInternet.ConnectionStr);

            System.Windows.Forms.MessageBox.Show("کلیه درخواست های جدید مشترکین با موفقیت در سیستم ثبت شدند");
            downloading_bargeh_khorooj = false;
        }

        public static void ulpoad_bargeh_khorooj()
        {
            uploading_bargeh_khorooj = true;
            string script = Get_table_Script_data(Current_database_name(), "barge_khorooj");
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb( script, clsInternet.ConnectionStr);
            System.Windows.Forms.MessageBox.Show("کلیه درخواست های جدید مشترکین با موفقیت در پورتال اینترنتی ثبت شدند");
            uploading_bargeh_khorooj = false;
        }



        public static void Upload_Moshtarekin_data()
        {
            uploading_moshtarekin_data = true;
            //string database = clsInternet.ConnectionStr.Split(';')[1].ToString().Split('=')[1].ToString();
            string script = Get_table_Script_data(Current_database_name(), "moshtarekin");
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("truncate table moshtarekin;" + script, clsInternet.ConnectionStr);
            System.Windows.Forms.MessageBox.Show("اطلاعات کلیه مشترکین با موفقیت در پورتال اینترنتی بارگزاری شد");
            uploading_moshtarekin_data = false;
        }
        public static void Upload_ab_ghabz(object dcode)
        {
            uploading_ab_ghabz = true;
            Classes.ClsMain.ExecuteNoneQuery("truncate table internet_ab_ghabz;insert into internet_ab_ghabz select * from ab_ghabz where dcode=" + dcode);
            string script = Get_table_Script_data_for_ghabz(Current_database_name(), "internet_ab_ghabz",dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from ab_ghabz where dcode=" + dcode.ToString() + ";" + script.Replace("internet_", ""), clsInternet.ConnectionStr);

            Classes.ClsMain.ExecuteNoneQuery("truncate table internet_ghabz_pardakht;insert into internet_ghabz_pardakht select * from ghabz_pardakht where dcode=" + dcode + " and is_ab=1");
            string script_pardakht = Get_table_Script_data_for_ghabz(Current_database_name(), "internet_ghabz_pardakht", dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from ghabz_pardakht where dcode=" + dcode.ToString() + " and is_ab=1;" + script_pardakht.Replace("internet_", ""), clsInternet.ConnectionStr);

            Classes.ClsMain.ExecuteNoneQuery("select * from ab_doreh");
            string script_doreh = Get_table_Script_data_for_ghabz(Current_database_name(), "ab_doreh", dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from ab_doreh;" + script_doreh, clsInternet.ConnectionStr);



            System.Windows.Forms.MessageBox.Show("اطلاعات قبوض آب دوره مربوطه با موفقیت در پورتال اینترنتی بارگزاری شد");
            uploading_ab_ghabz = false;
        }

        public static void Upload_sharj_ghabz(object dcode)
        {
            uploading_sharj_ghabz = true;
            Classes.ClsMain.ExecuteNoneQuery("truncate table internet_sharj_ghabz;insert into internet_sharj_ghabz select * from sharj_ghabz where dcode=" + dcode);
            string script = Get_table_Script_data_for_ghabz(Current_database_name(), "internet_sharj_ghabz", dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from sharj_ghabz where dcode=" + dcode.ToString() + ";" + script.Replace("internet_",""), clsInternet.ConnectionStr);

            Classes.ClsMain.ExecuteNoneQuery("truncate table internet_ghabz_pardakht;insert into internet_ghabz_pardakht select * from ghabz_pardakht where dcode=" + dcode + " and is_ab=0");
            string script_pardakht = Get_table_Script_data_for_ghabz(Current_database_name(), "internet_ghabz_pardakht", dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from ghabz_pardakht where dcode=" + dcode.ToString() + " and is_ab=0;" + script_pardakht.Replace("internet_", ""), clsInternet.ConnectionStr);

            Classes.ClsMain.ExecuteNoneQuery("select * from sharj_doreh");
            string script_doreh = Get_table_Script_data_for_ghabz(Current_database_name(), "sharj_doreh", dcode.ToString());
            Classes.ClsMain.ExecuteNoneQuery_UploadWeb("delete from sharj_doreh;" + script_doreh, clsInternet.ConnectionStr);



            System.Windows.Forms.MessageBox.Show("اطلاعات قبوض شارژ دوره مربوطه با موفقیت در پورتال اینترنتی بارگزاری شد");
            uploading_sharj_ghabz = false;
        }

    }
}
