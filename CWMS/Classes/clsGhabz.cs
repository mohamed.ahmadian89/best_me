﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
namespace CWMS.Classes
{
    public class clsGhabz
    {

        public clsGhabz()
        {
            ClearAllOfGhabz();
            New_or_Update_TakGhabz_Just_for_1_Moshtarek = -1;
            is_Updating_ghabz = false;
            doreh = -1;
        }
        public clsGhabz(int New_TakGhabz_Just_for_1_Moshtarek_temp)
        {

            ClearAllOfGhabz();
            New_or_Update_TakGhabz_Just_for_1_Moshtarek = New_TakGhabz_Just_for_1_Moshtarek_temp;
            is_Updating_ghabz = false;
            doreh = -1;
        }

        public void Start_Process()
        {
            start_Process = DateTime.Now;
        }

        public void finish_Process()
        {
            Classes.ClsMain.ChangeCulture("e");
            End_Process = DateTime.Now;
            ClsMain.ExecuteNoneQuery("update ab_doreh set tarikh_ijad='" + DateTime.Now + "',ghabz_sodoor=1 where dcode=" + doreh);

        }

        public void Get_Moshtarek_Info(int i)
        {
            gharardad = Convert.ToInt32(dt_all_moshtarek.Rows[i]["gharardad"]);
            size_ensheab = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["size_ensheab"]);
            akharin_ragham_kontor = Convert.ToInt32(dt_all_moshtarek.Rows[i]["kontor_ragham"]);

            DateTime? tarikh = null;
            try
            {
                tarikh = Convert.ToDateTime(dt_all_moshtarek.Rows[i]["tarikh_gharardad"]);
            }
            catch (Exception)
            {
                tarikh = null;
            }



            saghf_masraf = Convert.ToInt32(dt_all_moshtarek.Rows[i]["masrafe_mojaz"]);


            // در شهرک صنعتی اصفهان مصرف مجاز در هر ماه است
            // در نتیجه مصرف مجاز /  30 ضرب در تعداد روز های دوره
            // برابر است با سقف مصرفی که کاربر می تواند از ان استفاده کند 






            //if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            //{
            //    saghf_masraf = Convert.ToInt32(GetRoundedInteger(saghf_masraf / 30 * tedad_rooz));
            //}

            metraj = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["metraj"]);
            // در هنگام ویرایش این فیلد از طریق فرم مقدار دهی می شود
            if (is_Updating_ghabz == false)
            {
                zarib_fazelab = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["zarib_fazelab"]);
                tejari = Convert.ToBoolean(dt_all_moshtarek.Rows[i]["tejari"]);

            }

        }


        public void Set_Doreh_Field_info()
        {
            if (is_Updating_ghabz == false)
            {
                doreh = Convert.ToInt32(dt_doreh_info.Rows[0]["dcode"]);
                tarefe_ab = Convert.ToInt32(dt_doreh_info.Rows[0]["mablagh_ab"]);
                darsad_maliat = Convert.ToDecimal(dt_doreh_info.Rows[0]["darsad_maliat"]);
            }
            mohlat_pardakht_ghabz = Convert.ToDateTime(dt_doreh_info.Rows[0]["mohlat_pardakht"]);
            mablagh_fazelab_doreh = Convert.ToDecimal(dt_doreh_info.Rows[0]["mablagh_fazelab"]);
            mablagh_masraf_omoomi = Convert.ToDecimal(dt_doreh_info.Rows[0]["masraf_omoomi"]);

            zarib_Aboonman = Convert.ToDecimal(dt_doreh_info.Rows[0]["zarib_aboonman"]);

            Get_day_Length_of_doreh();
            zarib_tejari = Convert.ToDecimal(dt_doreh_info.Rows[0]["zaribtejari"]);
            zarib_sakhto_saz = Convert.ToDecimal(dt_doreh_info.Rows[0]["ZaribSakhtoSaz"]);
        }


        public void Get_last_abGhabz_of_Moshtarek()
        {
            // در حالت به روزرسانی
            // اطلاعات زیر به صورت مستقیم مقدار دهی شده و نیاز به خواندن از دیتابیس نمی باشد
            if (is_Updating_ghabz)
                return;

            if (Dt_all_ghabzof_Previous_doreh.Rows.Count == 0)
            {
                //-- برای اولین بار می بایست از جدول دیگری خوانده شود که کاربر از منوی مدیریت می تواند محتویات ان را تعیین کند
              
                    try
                    {
                        var Kontor_firstTime_row = ab_bedehi_from_Past.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                        kontor_ghabli = Convert.ToInt32(Kontor_firstTime_row["kontor_start"]);
                        bedehkar = Convert.ToInt64(Kontor_firstTime_row["bedehi"]);
                        bestankar = Convert.ToInt64(Kontor_firstTime_row["bestankar"]);
                        kasr_hezar_from_last_doreh = 0;
                        // نحوه محاسبات آب در شهرک صنعتی اصفهان بزرگ به فاصله تاریخی میان دوره وابسته است 
                        // در واقع مصرف مجاز به صورت ماهیانه می باشد 
                        if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                            try
                            {
                                tarikh_gheraat_ghabli = Convert.ToDateTime(Kontor_firstTime_row["tarikh_gheraat"]);
                            }
                            catch (Exception)
                            {
                                tarikh_gheraat_ghabli = DateTime.Now;
                            }
                    }
                    catch
                    {
                        kontor_ghabli = kasr_hezar_from_last_doreh = 0;
                        bedehkar = bestankar = 0;
                        tarikh_gheraat_ghabli = DateTime.Now;
                    }
            


            }
            else
            {
                try
                {
                    var Ab_ghabz_Of_Previous_doreh = Dt_all_ghabzof_Previous_doreh.AsEnumerable().Where(p => p.Field<int>("gharardad") == gharardad).First();
                    kontor_ghabli = Convert.ToInt32(Ab_ghabz_Of_Previous_doreh["kontor_end"]);
                    bedehkar = Convert.ToDecimal(Ab_ghabz_Of_Previous_doreh["mande"]);
                    kasr_hezar_from_last_doreh = Convert.ToInt32(Ab_ghabz_Of_Previous_doreh["kasr_hezar"]);
                    bestankar = Convert.ToDecimal(Ab_ghabz_Of_Previous_doreh["bestankari"]);
                    try
                    {
                        tarikh_gheraat_ghabli = Convert.ToDateTime(Ab_ghabz_Of_Previous_doreh["tarikh_gheraat"]);
                    }
                    catch (Exception)
                    {
                        tarikh_gheraat_ghabli = DateTime.Now;
                    }

                }
                catch
                {
                    kontor_ghabli = kasr_hezar_from_last_doreh = 0;
                    bedehkar = 0;
                    bestankar = 0;
                    tarikh_gheraat_ghabli = DateTime.Now;
                }

            }
        }



        public void Get_day_Length_of_doreh()
        {
            tedad_rooz = ClsMain.Days_between_tarikh_ha(
                Convert.ToDateTime(dt_doreh_info.Rows[0]["end_time"]),
                Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"])
                );
        }

        public void Fill_Necessary_Information(int new_ab_doreh = 0)
        {
            if (is_Updating_ghabz == false)
            {
                Ab.MainAbDatasetTableAdapters.kontor_khanTableAdapter da_kontor = new Ab.MainAbDatasetTableAdapters.kontor_khanTableAdapter();
                da_kontor.Fill(dt_kontor_khan);

                if (new_ab_doreh == 0)
                    dt_doreh_info = clsDoreh.Get_last_Ab_Doreh();
                else
                    dt_doreh_info = clsDoreh.Get_last_Ab_Doreh(new_ab_doreh);

            }
            else
                dt_doreh_info = clsDoreh.Get_last_Ab_Doreh(doreh);

            Jarime_masraf_mazad = ClsMain.GetDataTable("select * from Jarime_masraf_mazad");
            Dt_Hazineh_ensheab = ClsMain.GetDataTable("select * from EnsheabHazineh order by tasize asc");
            dt_Ab_info = ClsMain.GetDataTable("select * from ab_info");

            Set_Doreh_Field_info();




        }

        public void MoshtarekinList_and_LastGhobooz()
        {
            string
                Dt_all_ghabzof_Previous_doreh_Command = "select gharardad,mande,kasr_hezar,kontor_end,bestankari,tarikh_gheraat_ghabli,tarikh_gheraat from ab_ghabz  where dcode=" + (doreh - 1).ToString(),
                MoshtarekinCom = "select * from moshtarekin ";
            if (New_or_Update_TakGhabz_Just_for_1_Moshtarek != -1)
            {
                Dt_all_ghabzof_Previous_doreh_Command += " and gharardad=" + New_or_Update_TakGhabz_Just_for_1_Moshtarek;
                MoshtarekinCom += " where gharardad=" + New_or_Update_TakGhabz_Just_for_1_Moshtarek;
            }
            else
                MoshtarekinCom += " where have_ab_ghabz=1 ";

            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                MoshtarekinCom += " and ISNULL(tarikh_gharardad_ab,1)<>1";
            }


            dt_all_moshtarek = ClsMain.GetDataTable(MoshtarekinCom);
            Dt_all_ghabzof_Previous_doreh = ClsMain.GetDataTable(Dt_all_ghabzof_Previous_doreh_Command);

            dt_Kontor_first_time = null;

            if (doreh == 1)
                ab_bedehi_from_Past = ClsMain.GetDataTable("select * from ab_bedehi_bestankar_First_time");

        }


        DateTime makeValiddatetime(string dd)
        {
            string ValidPersianDateTime = dd.Substring(0, 4) + "/" + dd.Substring(4, 2) + "/" + dd.Substring(6, 2);
            return FarsiLibrary.Utils.PersianDateConverter.ToGregorianDateTime(ValidPersianDateTime);
        }
        public bool have_value_for_moshtarek__in_kontorKhan()
        {
            if (is_Updating_ghabz)
                return true;

            try
            {
                kontor_khan_state_of_moshtarek = (Ab.MainAbDataset.kontor_khanRow)dt_kontor_khan.Where(p => p.gharardad == gharardad).Select(p => p).First();
                return true;
            }
            catch (Exception ert)
            {

                ClsMain.logError(ert, "خطا در خواندن مقدار کنتورخوان مربوط به مشترک " + gharardad + "در دوره اب " + clsAb.Dcode + "در هنگام صدور قبوض کلی");
                ClsMain.ExecuteNoneQuery("insert into ab_ghabz_fails values (" + clsAb.Dcode + "," + gharardad + ",'عدم وجود قرائت مشترک در فایل کنتور خوان یا لیست دستی قرائت های دوره')");
                Classes.ClsMain.ChangeCulture("e");
                kontor_khan_state_of_moshtarek = null;
                return false;
            }
        }

        public void Add_MablaghKol_Jarimeh()
        {
            mablagh_kol += hazineh_jarimeh;
        }
        public void Add_MablaghKol_Sayer()
        {
            mablagh_kol += hazineh_sayer;
        }

        public void Add_MablaghKol_Masraf_omoomi()
        {
            mablagh_kol += hazineh_masraf_omoomi;
        }

        public void Add_MablaghKol_Hazineh_Mablagh()
        {
            mablagh_kol += hazineh_mablagh;
        }

        public void Add_MablaghKol_Hazineh_Taviz_kontor()
        {
            mablagh_kol += hazineh_taviz_kontor;
        }

        public void Add_MablaghKol_Hazineh_Ensheab()
        {
            mablagh_kol += hazineh_ensheab;
        }
        public void Add_MablaghKol_Hazineh_Maliat()
        {
            mablagh_kol += hazineh_maliat;
        }

        public void Add_MablaghKol_Hazineh_Fazelab()
        {
            mablagh_kol += hazineh_fazelab;
        }

        public void Add_MablaghKol_Bedehi()
        {
            mablagh_kol += bedehkar;
        }

        public void Add_MablaghKol_Kasr_HezarFrom_LastDoreh()
        {
            mablagh_kol += kasr_hezar_from_last_doreh;
        }

        public void Calculate_masraf_omoomi()
        {
            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                shahrak_classes.abbas_abad.Calculate_masraf_omoomi(ref hazineh_masraf_omoomi);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
            {
                shahrak_classes.paytakht.Calculate_masraf_omoomi(ref hazineh_masraf_omoomi, mablagh_masraf_omoomi, metraj);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
            {
                shahrak_classes.esf_bozorg.Calculate_masraf_omoomi(ref hazineh_masraf_omoomi);
            }
        }


        public void Moshtarek_Aboonman()
        {

            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                shahrak_classes.abbas_abad.Moshtarek_ensheab_hazineh(size_ensheab, Dt_Hazineh_ensheab, ref hazineh_ensheab, is_Updating_ghabz);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
            {
                shahrak_classes.paytakht.Moshtarek_ensheab_hazineh(mizan_masraf, mablagh_fazelab_doreh, zarib_Aboonman, ref hazineh_ensheab, is_Updating_ghabz);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
            {
                shahrak_classes.esf_bozorg.Moshtarek_ensheab_hazineh(Dt_Hazineh_ensheab, ref hazineh_ensheab, is_Updating_ghabz);
            }
        }

        public void Check_tejari_state()
        {
            if (tejari)
            {
                hazineh_mablagh = (hazineh_mablagh) * zarib_tejari;

                //if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                //{
                //    hazineh_mablagh = (hazineh_mablagh) * zarib_tejari;
                //}
                //else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                //{
                //    hazineh_mablagh = (hazineh_mablagh) * zarib_tejari;
                //}
                //else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                //{
                //    // در شهرک بزرگ اصفهان تجاری یا در حال ساخت و ساز بودن تاثیری در مبلغ قبض ندارد
                //    ;
                //}
            }
        }
        public void Check_darhal_sakht_state()
        {
            if (dar_hal_sakht)
            {
                hazineh_mablagh = (hazineh_mablagh) * zarib_sakhto_saz;

                //if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                //{
                //    hazineh_mablagh = (hazineh_mablagh) * zarib_sakhto_saz;
                //}
                //else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                //{
                //    hazineh_mablagh = (hazineh_mablagh) * zarib_sakhto_saz;
                //}
                //else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                //{
                //    // در شهرک بزرگ اصفهان تجاری یا در حال ساخت و ساز بودن تاثیری در مبلغ قبض ندارد
                //    ;
                //}
            }
        }

        static decimal GetRoundedInteger(decimal number)
        {
            return Convert.ToDecimal(Math.Round(number));
        }




        public void Check_Bedehkar_Bestankar_Status()
        {
            // در حالت ویرایش مبلغ بستانکاری از دوره قبل خوانده نمی شود
            // از فرم اطلاعات قبض می گیرد که ان مبلغ برابر باست با مقدار ذخیره شده در فیلد بستانکاری قبض همان دوره


            clsBedehkar_Bestankar cls = new clsBedehkar_Bestankar(mablagh_kol, gcode, bestankar, tozihat, is_Updating_ghabz);
            cls.Check_Bedehkar_Bestankar_Status();
            bestankar = cls.bestankar;
            mandeh = cls.mandeh;
            vaziat_ghabz = cls.vaziat_ghabz;
            kasr_hezar_from_current_doreh = cls.kasr_hezar_from_current_doreh;
            tozihat = cls.tozihat;
            was_bestankar_in_sodoorGhabz = cls.was_bestankar_in_sodoorGhabz;




            //decimal Pardakhti_in_Doreh = Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
            //"select isnull(sum(mablagh),0) from ghabz_pardakht"
            //+ " where gcode=" + gcode + " and vaziat=1"
            //));


            //bestankar += Pardakhti_in_Doreh;

            //if (bestankar >= mablagh_kol)
            //{

            //    bestankar -= mablagh_kol;
            //    mandeh = 0;
            //    kasr_hezar_from_current_doreh = 0;

            //    // قبض کاربر به طور کامل پرداخت شده است 
            //    vaziat_ghabz = 1;

            //    tozihat += " || " + "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + mablagh_kol.ToString();

            //    // بدین معنا که در حال انجام فرایند صدور قبض برای همه مشترکین هستیم
            //    // و کاربر مبلغ بستاانکاری از مبلغ قبض بیشتر شده 
            //    // در این نسخه مبلغ قبض را صفر نمی کنیم بلکه ما به التفاوت را به عنوان بستانکاری ربای کاربر ثبت می کنیم
            //    if (is_Updating_ghabz == false)
            //        was_bestankar_in_sodoorGhabz = true;
            //    // این بدین معنی است که کاربر در هنگام صدور قبوض دوره بستانکار  بوده است  . 

            //}
            //else
            //{
            //    // در این الگوریتم مبلغ کل ثابت خواهد بود
            //    // و بروی فیلد مانده مانور خواهیم داد

            //    if (bestankar != 0)
            //        tozihat += " || " + "کسر  " + bestankar.ToString() + " ریال " + "   از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل ";


            //    mandeh = mablagh_kol - bestankar;
            //    bestankar = 0;


            //    if (mandeh < 1000)
            //    {
            //        kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh);
            //        mandeh = 0;
            //        vaziat_ghabz = 1;
            //    }
            //    else
            //    {

            //        kasr_hezar_from_current_doreh = Convert.ToInt32(mandeh % 1000);
            //        mandeh -= kasr_hezar_from_current_doreh;

            //    }

            //    if (Pardakhti_in_Doreh != 0)
            //        vaziat_ghabz = 2;



            //}



            //// یعنی در حال صدور قبوض به صورت کلی یا تکی هستیم
            //if (is_Updating_ghabz == false)
            //{
            //    bestankar += Convert.ToDecimal(Classes.ClsMain.ExecuteScalar(
            //        "select isnull(sum(mablagh),0) from ghabz_pardakht"
            //        + " where gharardad=" + gharardad + " and dcode=" + doreh + " and is_ab=1 and vaziat=1"

            //        ));

            //    // برای حالاتی که در اون به هر دلیلی می خواهیم 
            //    // قبضی رامجداد صادر کنیم که  چندین پرداخیت برای ان ثبت شده است 
            //    // در نتیجه بستانکاری ان قبض برابر است با بستانکاری از قبض دوره قبل
            //    // بعلاوه مجموع پرداختی ها در این دوره 
            //}


            //if (bestankar >= mablagh_kol)
            //{
            //    kasr_hezar_from_current_doreh = 0;
            //    bestankar -= mablagh_kol;
            //    bestankari_from_last_doreh = bestankar;
            //    if (is_Updating_ghabz == false)
            //    {
            //        was_bestankar_in_sodoorGhabz = true;
            //        tozihat += " || " + "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + mablagh_kol.ToString() + " شامل مبلغ کل ، آبونمان،جریمه، سایر و .... ";
            //    }


            //    mablagh_kol = mandeh = hazineh_maliat =
            //        hazineh_fazelab = hazineh_mablagh =
            //        bedehkar = hazineh_jarimeh = hazineh_sayer =
            //        hazineh_ensheab = kasr_hezar_from_last_doreh =
            //        0;
            //    vaziat_ghabz = 1;



            //}
            //else
            //{

            //    mablagh_kol -= bestankar;
            //    if (bestankar != 0)
            //        if (is_Updating_ghabz == false)
            //            tozihat += " || " + "کسر  " + bestankar.ToString() + " ریال " + "   از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل ";
            //    bestankar = 0;
            //    bestankari_from_last_doreh = bestankar;

            //    kasr_hezar_from_current_doreh = Convert.ToInt32(mablagh_kol % 1000);
            //    mandeh = mablagh_kol = mablagh_kol - kasr_hezar_from_current_doreh;




            //}

        }




        public void Calculate_Taviz_kontor()
        {
            if (is_Updating_ghabz == true)
                return;

            if (kontor_khan_state_of_moshtarek.taviz)
            {
                if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                {
                    hazineh_taviz_kontor = Convert.ToDecimal(dt_doreh_info.Rows[0]["hazineh_taviz_kontor"]);
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
                {
                    hazineh_taviz_kontor = Convert.ToDecimal(dt_doreh_info.Rows[0]["hazineh_taviz_kontor"]);
                }
            }

        }

        public void Calculate_Fazelab()
        {
            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                shahrak_classes.abbas_abad.Calculate_Fazelab(ref hazineh_fazelab);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
            {
                shahrak_classes.paytakht.Calculate_Fazelab(ref hazineh_fazelab, mizan_masraf, zarib_fazelab, mablagh_fazelab_doreh);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
            {
                shahrak_classes.esf_bozorg.Calculate_Fazelab(ref hazineh_fazelab);
            }
        }

        public void Calculate_Maliat()
        {
            hazineh_maliat = GetRoundedInteger((mablagh_kol) * darsad_maliat / 100);
        }


        public void Calculate_hazineh_masraf_of_kharab_state(decimal hazineh)
        {
            hazineh_mablagh = mablagh_mojaz = hazineh;
            mablagh_gheir_mojaz = 0;
        }

        public void Calculate_hazineh_masraf()
        {
            //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------


            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                shahrak_classes.abbas_abad.Calculate_hazineh_masraf(
                    dar_hal_sakht, mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, ref mablagh_mojaz,
                    ref mablagh_gheir_mojaz, ref hazineh_mablagh, Jarime_masraf_mazad);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
            {
                shahrak_classes.paytakht.Calculate_hazineh_masraf(
                    dar_hal_sakht, mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, ref mablagh_mojaz,
                    ref mablagh_gheir_mojaz, ref hazineh_mablagh, Jarime_masraf_mazad);
            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
            {
                shahrak_classes.esf_bozorg.Calculate_hazineh_masraf(
                    dar_hal_sakht, mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, ref mablagh_mojaz,
                    ref mablagh_gheir_mojaz, ref hazineh_mablagh, Jarime_masraf_mazad);

            }
            else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
            {
                shahrak_classes.shams_abad.Calculate_hazineh_masraf(
                    dar_hal_sakht, mizan_masraf, masraf_mojaz, masraf_gheir_mojaz, tarefe_ab, ref mablagh_mojaz,
                    ref mablagh_gheir_mojaz, ref hazineh_mablagh, Jarime_masraf_mazad);

            }
        }

        public decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return sum;
        }


        //--------------------------------------------------
        //..TODO:در مورد نحوه هزینه مصرف غیر مجاز اینجوری حساب می کنیم که به ازا متر مکعب اضافه هزینه تصاعدی می شود نه برای کل مصرف کاربر
        // این محاسبه به صورت پلکانی خواهد بود . یعنی بر اساس میزان درصد مصرف مجاز که اضافه صورت گرفته 
        // مبلغی با توجه به جدول مصارف مازاد در نظر گرفته می شود
        public long Get_Masrafe_gheir_mojaz_hazineh()
        {
            if (masraf_mojaz == 0)
                return Convert.ToInt64(Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]) * masraf_gheir_mojaz * tarefe_ab;

            return Convert.ToInt64(PelekaniTarefe(Convert.ToDecimal(masraf_mojaz), Convert.ToDecimal(masraf_gheir_mojaz), Convert.ToDecimal(mizan_masraf), Jarime_masraf_mazad, tarefe_ab));

        }


        public void Calculate_mizan_masraf()
        {

            kontor_feli = kontor_khan_state_of_moshtarek.shomareh_kontor;
            dar_hal_sakht = kontor_khan_state_of_moshtarek.dar_hal_sakht;

            if (kontor_khan_state_of_moshtarek.ghat ||
                kontor_khan_state_of_moshtarek.adam_gheraat ||
                kontor_khan_state_of_moshtarek.kharab)
            {
                mizan_masraf = 0;
                kontor_feli = kontor_ghabli;
            }
            else
            {
                if (kontor_khan_state_of_moshtarek.taviz)
                {
                    kontor_ghabli = 0;
                    kontor_feli = kontor_khan_state_of_moshtarek.shomareh_kontor;
                    mizan_masraf = kontor_feli;
                }
                else if (kontor_khan_state_of_moshtarek.dor_kamel)
                {

                    mizan_masraf = kontor_feli + (akharin_ragham_kontor - kontor_ghabli);
                }
                else
                {
                    //------حالت عادی که کنتور درست کار می کنه ------------
                    mizan_masraf = kontor_feli - kontor_ghabli;
                }
            }


            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                saghf_masraf = Convert.ToInt32(GetRoundedInteger(((decimal)saghf_masraf / 30) *
                    ((kontor_khan_state_of_moshtarek.tarikh - tarikh_gheraat_ghabli).Days + 1)
                    ));
            }
            // در شهرک صنعتی شمس اباد، سقف مجاز مشترک به صورت روزانه می باشد . در نتیجه می بایست در تعداد روزهای دوره ضرب شود.
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
            {
                saghf_masraf *= tedad_rooz;
            }



            if (mizan_masraf > saghf_masraf)
            {
                masraf_gheir_mojaz = mizan_masraf - saghf_masraf;
                masraf_mojaz = saghf_masraf;
            }
            else
            {
                masraf_mojaz = mizan_masraf;
                masraf_gheir_mojaz = 0;
            }

            // برای کلیه شهرک های دیگر
            // مصرف مجاز بر اساس طول دوره است نه در ماه


        }


        public void ClearAllOfGhabz()
        {
            if (is_Updating_ghabz == false)
            {
                gharardad =

                kontor_feli =
                kontor_ghabli =
                kasr_hezar_from_current_doreh =
                kasr_hezar_from_last_doreh =
                akharin_ragham_kontor =
                saghf_masraf =
                masraf_mojaz =
                masraf_gheir_mojaz =
                mizan_masraf =
                vaziat_ghabz = 0;

                zarib_fazelab =
                size_ensheab =
                bedehkar =
                bestankar =


                mandeh =
                mablagh_mojaz =
                mablagh_gheir_mojaz =
                hazineh_mablagh =
                hazineh_maliat =
                hazineh_fazelab =
                hazineh_ensheab =
                hazineh_jarimeh =
                hazineh_taviz_kontor =
                hazineh_sayer =
                hazineh_masraf_omoomi =
                mablagh_kol = 0;


                tejari = dar_hal_sakht = mablaghKol_is_zero_but_has_bestankari = was_bestankar_in_sodoorGhabz = false;
                tozihat = tozihat_jarimeh = tozihat_sayer = "";
                kontor_khan_state_of_moshtarek = null;
                last_pardakht = null;
                tarikh_sodoor = DateTime.Now;
                shenase_prdakht = shenase_ghabz = "";
                gcode = "";

            }
        }


        int Get_Database_Boolean_Value(bool value)
        {
            if (value)
                return 1;
            return 0;
        }

        public string Generate_AllGhabz_of_GhabzList(MultipleCommand Multiple)
        {
            if (Multiple.RunCommandCollecton("صدور قبوض اب  - cls_ab.Create_ghobooz_Ab_Background"))
            {
                if (New_or_Update_TakGhabz_Just_for_1_Moshtarek == -1)
                    finish_Process();
                return "success";
            }

            return "fail";

        }

        public void Ghabz_Shenase()
        {
            try
            {
                shenase_ghabz = shenase_prdakht = "";
                int year = Convert.ToInt32(new FarsiLibrary.Utils.PersianDate(tarikh_sodoor).Year);
                shenase_ghabz = ShenasehGhabz.Ghabz.Shenase_Ghabz(gharardad.ToString(), ClsMain.SherkartCode_MelliAccount, "1");
                if (mandeh == 0)

                    shenase_prdakht = "0";
                else
                    shenase_prdakht = ShenasehGhabz.Ghabz.Shenase_Parakht(shenase_ghabz, (GetRoundedInteger(mandeh / 1000)).ToString(), year, doreh);
            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert);
                shenase_ghabz = shenase_prdakht = "";

            }

        }

        public void Ghabz_code__gcode()
        {
            gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(doreh), true, Convert.ToInt32(gharardad));

        }
        public void Add_MoshtarekGhabz_to_GhabzList(MultipleCommand Multiple)
        {
            try
            {

                string PreDeletecommand = "";
                if (is_Updating_ghabz)
                {
                    PreDeletecommand = "delete from ab_ghabz where gcode=" + gcode + "";
                }



                Multiple.addToCommandCollecton(PreDeletecommand + "INSERT INTO ab_ghabz (gcode,[gharardad],[dcode],[tarikh],[mablagh],tarefe,mohlat_pardakht,mablaghkol" +
         ",darsad_maliat,maliat,bedehi,aboonmah,Ensheab,mande,kasr_hezar,masraf_mojaz,"
          + "masraf_gheir_mojaz,masraf_mojaz_hazineh,"
          + "masraf_gheir_mojaz_hazineh,kharab,dor_kamel,taviz,adam_gheraat,ghat,darhalsakht,masraf,kontor_start,kontor_end," +
          "sayer,jarimeh,sayer_tozihat,jarimeh_tozihat,darsad_fazelab,fazelab,tejari,tarikh_gheraat,vaziat,bestankari,tozihat,tedadRooz,was_bestankar_in_sodoorGhabz,last_pardakht,shenase_ghabz,shenase_pardakht,hazineh_taviz_kontor,masraf_omoomi" + (tarikh_gheraat_ghabli != DateTime.MinValue ? ",tarikh_gheraat_ghabli" : "") + ")   values ("
          + gcode + "," + gharardad + "," + doreh + ",'" +
         tarikh_sodoor + "'," + hazineh_mablagh + "," + tarefe_ab + ",'" +
         mohlat_pardakht_ghabz + "'," + mablagh_kol + "," +
         darsad_maliat + "," + hazineh_maliat + "," + bedehkar + "," +
         hazineh_ensheab + "," + size_ensheab + "," +
         mandeh + "," + kasr_hezar_from_current_doreh + "," +
         masraf_mojaz + "," + masraf_gheir_mojaz + "," +
         mablagh_mojaz +
         "," + mablagh_gheir_mojaz + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.kharab) + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.dor_kamel) + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.taviz) + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.adam_gheraat) + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.ghat) + "," +
         Get_Database_Boolean_Value(kontor_khan_state_of_moshtarek.dar_hal_sakht) +
         "," + mizan_masraf + "," +
         kontor_ghabli + "," + kontor_feli + "," + hazineh_sayer + ","
         + hazineh_jarimeh + ",'" + tozihat_sayer + "','" + tozihat_jarimeh + "'," +
             zarib_fazelab + "," + hazineh_fazelab + "," +
             Get_Database_Boolean_Value(tejari) + ",'" +
             kontor_khan_state_of_moshtarek.tarikh + "',"
          + vaziat_ghabz + "," + bestankar + ",'" + tozihat + "'," +
            tedad_rooz + "," +
          Get_Database_Boolean_Value(was_bestankar_in_sodoorGhabz)
          + ",'" + last_pardakht + "','" + shenase_ghabz + "','" + shenase_prdakht + "'," + hazineh_taviz_kontor + "," + hazineh_masraf_omoomi
          + (tarikh_gheraat_ghabli != DateTime.MinValue ? ",'" + tarikh_gheraat_ghabli + "'" : "")
          + ");");

                // در صورتی که در دوره های بالا باشیم و برای مشترک تا به حال قبضی صادر نشده باشد 
                // تاریخ فعلی سیستم به عنوان تاریخ قرائت قبلی ان ثبت می شود









            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }





        public bool tejari,
            dar_hal_sakht,
            is_Updating_ghabz = false,
            mablaghKol_is_zero_but_has_bestankari = false,
            was_bestankar_in_sodoorGhabz = false
            ;



        public int tarefe_ab,
            doreh,
            gharardad,


            kontor_feli,
            kontor_ghabli,
            kasr_hezar_from_last_doreh,
            kasr_hezar_from_current_doreh,
            akharin_ragham_kontor,
            saghf_masraf,
            masraf_mojaz,
            masraf_gheir_mojaz,
            mizan_masraf,
            vaziat_ghabz,
            tedad_rooz,
            New_or_Update_TakGhabz_Just_for_1_Moshtarek
            ;

        public Decimal darsad_maliat,
            zarib_tejari,
            zarib_sakhto_saz,
            zarib_Aboonman,
            zarib_fazelab,
            size_ensheab,
            bedehkar,

            bestankar,
            mandeh,
            mablagh_fazelab_doreh,
            metraj,
            mablagh_masraf_omoomi,


            mablagh_mojaz,
            mablagh_gheir_mojaz,

            hazineh_mablagh,
            hazineh_maliat,
            hazineh_fazelab,
            hazineh_ensheab,
            hazineh_sayer,
            hazineh_jarimeh,
            hazineh_taviz_kontor,
            hazineh_masraf_omoomi,



            mablagh_kol

            ;
        public string tozihat, tozihat_sayer, tozihat_jarimeh, shenase_ghabz, shenase_prdakht, gcode;

        public DateTime mohlat_pardakht_ghabz,
                 start_Process,
                 End_Process
            , tarikh_sodoor, tarikh_gheraat_ghabli
                    ;

        public DateTime? last_pardakht;

        public DataTable dt_doreh_info
        , Jarime_masraf_mazad
        , Dt_Hazineh_ensheab
        , dt_all_moshtarek
        , dt_Ab_info,
        Dt_all_ghabzof_Previous_doreh,
        dt_Kontor_first_time,
            ab_bedehi_from_Past
            ;

        public Ab.MainAbDataset.kontor_khanRow kontor_khan_state_of_moshtarek = null;
        Ab.MainAbDataset.kontor_khanDataTable dt_kontor_khan = new Ab.MainAbDataset.kontor_khanDataTable();

    }
}
