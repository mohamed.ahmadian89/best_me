﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using FarsiLibrary.Win;
using System.Threading;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Collections;
using System.Linq;
using System.Globalization;
namespace CWMS.Classes
{
    class clsSharj
    {
        public static string Dcode = "1";

        public static int allMoshtarekCount = 0;


        void esf_bozorg_condition(clsSharjGhabz ghabzInfo, ref DateTime? tarikh, int i)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                try
                {
                    tarikh = Convert.ToDateTime(ghabzInfo.dt_all_moshtarek.Rows[i]["tarikh_gharardad"]);
                }
                catch (Exception)
                {
                    tarikh = null;

                }
            }
        }

        bool shamsAbad_condition(clsSharjGhabz ghabzInfo)
        {

            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
            {
                if (new PersianDate(Convert.ToDateTime(ghabzInfo.dt_doreh_info.Rows[0]["start_time"])).Year == 1395)
                {

                    if (ghabzInfo.is_Updating_ghabz == false)
                    {
                        if (ghabzInfo.bedehkar == 0 && ghabzInfo.doreh != 1)
                            return false;
                        ghabzInfo.bedehkar = ghabzInfo.bestankar = ghabzInfo.kasr_hezar_from_last_doreh = 0;
                    }
                    else
                    {
                        ghabzInfo.kasr_hezar_from_last_doreh = 0;
                    }
                }

            }
            return true;
        }

        public string Generate_Ghabz(clsSharjGhabz ghabzInfo = null)
        {
            int temp_doreh = 0, temp_gharardad = 0;
            Classes.ClsMain.ChangeCulture("e");
            if (ghabzInfo == null)
                ghabzInfo = new clsSharjGhabz();

            if (ghabzInfo != null)
                // شرط زیر فقط هنگامی درست خواهد بود که قصد داریم برای یک مشترک قبض برا ی یک دوره خاص صادر کنیم
                if (ghabzInfo.Tarefe == -10)
                {
                    temp_doreh = ghabzInfo.doreh;
                    temp_gharardad = ghabzInfo.gharardad;
                    ghabzInfo = new clsSharjGhabz();
                    ghabzInfo.doreh = temp_doreh;
                    ghabzInfo.New_or_Update_TakGhabz_Just_for_1_Moshtarek = temp_gharardad;
                    ghabzInfo.gharardad = temp_gharardad;
                }


            ghabzInfo.Start_Process();
            ghabzInfo.Fill_Necessary_Information(temp_doreh);
            /*
            کد دوره  
            مبلغ شارژ
            مهلت پرداخت 
            تعداد روزهای دوره
            درصد مالیات 
            */

            ghabzInfo.MoshtarekinList_and_LastGhobooz();

            Classes.MultipleCommand Multiple = new MultipleCommand("Soodor_Ghobboz_Sharj");

            try
            {


                for (int i = 0; i < ghabzInfo.dt_all_moshtarek.Rows.Count; i++)
                {
                    Classes.ClsMain.ChangeCulture("e");
                    ghabzInfo.ClearAllOfGhabz();
                    ghabzInfo.Get_Moshtarek_Info(i);
                    ghabzInfo.Ghabz_code__gcode();


                    DateTime? tarikh = null;
                    esf_bozorg_condition(ghabzInfo, ref tarikh, i);



                    ghabzInfo.Calculate_Sharj_Hazineh(tarikh, ghabzInfo.gcode.ToString());
                    ghabzInfo.Check_tejari_state();

                    ghabzInfo.Get_last_sharj_ghabz_of_Moshtarek();


                    // 
                    if (shamsAbad_condition(ghabzInfo) == false)
                        continue;

                    //-----------------------------------------------------------
                    // هزینه های دوره جاری 

                    // عناصری که در فرمول مالیات مورد محاسبه قرار می گیرند.
                    ghabzInfo.Add_MablaghKol_Hazineh_Mablagh();
                    ghabzInfo.Add_MablaghKol_Jarimeh();
                    ghabzInfo.Add_MablaghKol_Sayer();
                    //-----------------------------------------------------------
                    ghabzInfo.Calculate_Maliat();
                    ghabzInfo.Add_MablaghKol_Hazineh_Maliat();
                    //-----------------------------------------------------------

                    //-----------------------------------------------------------
                    // هزینه های مربوط به دوره قبل
                    ghabzInfo.Add_MablaghKol_Bedehi();
                    ghabzInfo.Add_MablaghKol_Kasr_HezarFrom_LastDoreh();
                    //-----------------------------------------------------------

                    ghabzInfo.Check_Bedehkar_Bestankar_Status();
                    ghabzInfo.Ghabz_Shenase();
                    ghabzInfo.Add_MoshtarekGhabz_to_GhabzList(Multiple);
                    //clsFactor.Check_sharj_factor(ghabzInfo, "sharj_ghabz", 1);


                }
                return ghabzInfo.Generate_AllGhabz_of_GhabzList(Multiple);


            }

            catch (Exception ex)
            {
                ClsMain.logError(ex, "صدور قبوض شارژ");
                Payam.Show("بروز خطا در هنگام صدور قبض");
                return "fail";
            }

        }





        public static void RizHeasbsharjForAll(MainDataSest mainds, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshSharjForAll report1)
        {
            MainDataSestTableAdapters.settingTableAdapter ta = new MainDataSestTableAdapters.settingTableAdapter();
            ta.Fill(mainds.setting);


            report1.SetDataSource(mainds);

            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;

            new FrmShowReport(report1).ShowDialog();


        }
        public static void RizHeasb(string start, string end, string gharardad, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.rptRizGozareshSharj report1, string typeOfReport)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            ta.FillBySomeDoreh(ds.sharj_ghabz, Convert.ToInt32(start), Convert.ToInt32(end), Convert.ToInt32(gharardad));

            DataTable dt = ds.sharj_ghabz.CopyToDataTable();


            ds.sharj_ghabz.Columns.Remove("last_pardakht");
            ds.sharj_ghabz.Columns.Add("last_pardakht");
            for (int i = 0; i < ds.sharj_ghabz.Rows.Count; i++)
            {
                if (Convert.ToBoolean(ds.sharj_ghabz.Rows[i]["has_pardakhti"]))
                {
                    try
                    {
                        ds.sharj_ghabz.Rows[i]["last_pardakht"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["last_pardakht"])).ToString("d");

                    }
                    catch (Exception)
                    {
                        ds.sharj_ghabz.Rows[i]["last_pardakht"] = "";
                    }
                }
                else
                    ds.sharj_ghabz.Rows[i]["last_pardakht"] = "";

            }



            report1.SetDataSource(ds);

            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel,address from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
            report1.SetParameterValue("address", Dt_moshtarek_info.Rows[0]["address"].ToString());

            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;




            new FrmShowReport(report1).ShowDialog();


        }

        public static void Sharj_moshtarek_pardakhti(
CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.sharj_pardakhti report1, DateTime start, DateTime end, string pstart, string pend, bool isByTaikh, int doreh)
        {
            ManagementReports.DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter ta = new ManagementReports.DaramadDatasetTableAdapters.sharj_ghabz_PardakhtTableAdapter();
            ManagementReports.DaramadDataset ds = new ManagementReports.DaramadDataset();
            ManagementReports.DaramadDatasetTableAdapters.settingTableAdapter tasetting = new ManagementReports.DaramadDatasetTableAdapters.settingTableAdapter();
            tasetting.FillSetting(ds.setting);
            if (isByTaikh)
                ta.Fill(ds.sharj_ghabz_Pardakht, start, end);
            else
                ta.FillByDoreh(ds.sharj_ghabz_Pardakht, doreh);

            DataTable dt = ds.sharj_ghabz_Pardakht.CopyToDataTable();

            ds.sharj_ghabz_Pardakht.Columns.Remove("آخرین پرداخت");
            ds.sharj_ghabz_Pardakht.Columns.Add("آخرین پرداخت");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    ds.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"] = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt.Rows[i]["آخرین پرداخت"])).ToString("d");

                }
                catch (Exception)
                {

                    ds.sharj_ghabz_Pardakht.Rows[i]["آخرین پرداخت"] = "";

                }
            }

            report1.SetDataSource(ds);
            report1.SetParameterValue("shahrak_name", ClsMain.ExecuteScalar("select top(1) shahrak_name from setting"));
            report1.SetParameterValue("pstart", pstart);
            report1.SetParameterValue("pend", pend);
            report1.SetParameterValue("gtarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));


            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }


        //        public static void Sharj_moshtarek_pardakhti(
        //CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.ab_pardakhti report1, DateTime start, DateTime end)
        //        {
        //            ManagementReports.DaramadDatasetTableAdapters.ab_pardakht_reportTableAdapter ta = new ManagementReports.DaramadDatasetTableAdapters.ab_pardakht_reportTableAdapter();
        //            ManagementReports.DaramadDataset ds = new ManagementReports.DaramadDataset();

        //            ta.FillByTarikh(ds.ab_pardakht_report, start, end);

        //            report1.SetDataSource(ds);
        //            viewer.ReportSource = report1;
        //            new FrmShowReport(report1).ShowDialog();

        //            //viewer.PrintReport();
        //        }


        public static void sharj_moshtarek_bedehkar(
CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Sharj_Bedehkaran report1, int doreh, int from, int to)
        {
            CrysReports.ReportDataSet2TableAdapters.moshtarek_sharjTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.moshtarek_sharjTableAdapter();
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            if (from == -1 || to == -1)
            {
                if (doreh == 0)
                {
                    ta.FillBedehkatForAll(ds.moshtarek_sharj);

                }
                else
                {
                    ta.FillForBedehkar(ds.moshtarek_sharj, doreh);
                }
            }
            else
            {
                if (doreh == 0)
                {
                    ta.FillBedehkarForAllByGharardad(ds.moshtarek_sharj, from, to);

                }
                else
                {
                    ta.FillForBedehkarByGharardad(ds.moshtarek_sharj, doreh, from, to);
                }
            }

            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }

        public static void sharj_moshtarek_bestankar(
CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Sharj_Bestankaran report1, int doreh)
        {
            CrysReports.ReportDataSet2TableAdapters.moshtarek_sharjTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.moshtarek_sharjTableAdapter();
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();

            if (doreh == 0)
            {
                ta.FillBestankarForAll(ds.moshtarek_sharj);

            }
            else
            {
                ta.FillForBestankar(ds.moshtarek_sharj, doreh);
            }

            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate().ToString("d"));
            viewer.ReportSource = report1;
            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();
        }

        public static void print_Ghabz_Multi(System.ComponentModel.BackgroundWorker BGWorker, System.Windows.Forms.ProgressBar prog, string dcode, int FirstGharardad, int LastGharardad, int moshtarek_active_state, bool Sort_by_gharardad, bool print_ghabz_which_not_printed)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSet dsTemp = ClsMain.getReportDatasetWithSetting();

            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter MoshtarekinTa = new CrysReports.ReportDataSetTableAdapters.moshtarekinTableAdapter();
            ta1.FillByGharardadRange(dsTemp.sharj_ghabz, Convert.ToInt32(dcode), Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));





            if (Sort_by_gharardad)
                MoshtarekinTa.FillByGharardad_Gharardad_sort(ds.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));
            else
                MoshtarekinTa.FillByGharardad_Address_Sort(ds.moshtarekin, Convert.ToInt32(FirstGharardad), Convert.ToInt32(LastGharardad));

            string tarikh = "";
            string mohlat_pardakht = "";


            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = null;
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
            {
                report1 = new CrysReports.abbas_abad.sharj_ghabz_no_bg();
            }
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
            {
                report1 = new CrysReports.paytakht.sharj_ghabz_by_bg();
            }
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                report1 = new CrysReports.esf_bozorg.sharj_ghabz_by_bg();

            }
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.shams_abad)
            {
                report1 = new CrysReports.shams_abad.sharj_ghabz_by_bg();

            }





            report1.SetParameterValue("shahrak_name", ClsMain.ShahrakSetting.shahrak_name);

            int gharardad = 0;
            string LostGharardad = "";


            System.Drawing.Printing.PrintDocument doctoprint = new System.Drawing.Printing.PrintDocument();
            doctoprint.PrinterSettings.PrinterName = "ghabzPrinter";
            for (int i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count; i++)
            {
                int rawKind = 0;
                if (doctoprint.PrinterSettings.PaperSizes[i].PaperName == "CHEQUEPRINT")
                {
                    rawKind = Convert.ToInt32(doctoprint.PrinterSettings.PaperSizes[i].GetType().GetField("kind", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes[i]));
                    report1.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)rawKind;
                    break;
                }
            }

            PageMargins p22 = new PageMargins();
            p22.topMargin = p22.bottomMargin = p22.leftMargin = p22.rightMargin = 0;
            report1.PrintOptions.ApplyPageMargins(p22);

            if (dsTemp.sharj_ghabz.Rows.Count == 0)
            {
                Payam.Show("اطلاعاتی جهت چاپ وجود ندارد  ");
                return;
            }


            for (int i = 0; i < ds.moshtarekin.Rows.Count; i++)
            {
                if (BGWorker.CancellationPending)
                {
                    return;
                }

                try
                {
                    gharardad = Convert.ToInt32(ds.moshtarekin.Rows[i]["gharardad"].ToString());
                    DataTable dt_ghabz = dsTemp.sharj_ghabz.Where(p => p.gharardad == gharardad).CopyToDataTable();

                    if (print_ghabz_which_not_printed)
                    {
                        if (Convert.ToBoolean(dt_ghabz.Rows[0]["printed"]))
                            continue;
                    }

                    tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_ghabz.Rows[0]["tarikh"])).ToString("d");
                    mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

                    dt_ghabz.Columns.Remove("tarikh");
                    dt_ghabz.Columns.Remove("mohlat_pardakht");


                    dt_ghabz.Columns.Add("tarikh");
                    dt_ghabz.Columns.Add("mohlat_pardakht");

                    dt_ghabz.Rows[0]["tarikh"] = tarikh;
                    dt_ghabz.Rows[0]["mohlat_pardakht"] = mohlat_pardakht;


                    report1.SetDataSource(dt_ghabz);


                    CrysReports.ReportDataSet.moshtarekinRow MoshtarekInfo = ds.moshtarekin[i];

                    if (moshtarek_active_state == 1)
                        if (Convert.ToBoolean(MoshtarekInfo.vaziat) == false)
                            continue;
                    report1.SetParameterValue("modir_amel", (MoshtarekInfo.Ismodir_amelNull() ? "" : MoshtarekInfo.modir_amel));
                    report1.SetParameterValue("pelak", (MoshtarekInfo.IspelakNull() ? "" : MoshtarekInfo.pelak));
                    report1.SetParameterValue("address", (MoshtarekInfo.IsaddressNull() ? "" : MoshtarekInfo.address));
                    report1.SetParameterValue("co_name", (MoshtarekInfo.Isco_nameNull() ? "" : MoshtarekInfo.co_name));
                    report1.SetParameterValue("pshodeh", "0");
                    report1.SetParameterValue("mandehHoroof", clsNumber.Number_to_Str(dt_ghabz.Rows[0]["mande"].ToString()));


                    string Doreh = dt_ghabz.Rows[0]["dcode"].ToString();
                    DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from sharj_doreh where dcode=" + Doreh);

                    report1.SetParameterValue("dcodeinfo", ("از " + new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][0])).ToString("d") + " الی " +
                        new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d")).ToString()
                        );





                    report1.PrintToPrinter(1, true, 0, 0);
                    prog.Value++;
                    Classes.ClsMain.ExecuteNoneQuery("update sharj_ghabz set printed=1 where gcode=" + dt_ghabz.Rows[0]["gcode"].ToString());
                }
                catch (Exception ert)
                {
                    LostGharardad += gharardad + ",";
                    ClsMain.logError(ert);
                }
            }
            if (LostGharardad.Length != 0)
            {
                Payam.Show("متاسفانه قبوض برای مشترکین زیر صادر نشده است . جهت بررسی خطا به سامانه خطا مراجعه کنید");
                Payam.Show(LostGharardad.Substring(0, LostGharardad.Length - 2));
            }

        }




        public static void print_Ghabz(string gcode, string gharardad, string typeOfReport, bool Is_by_image)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();

            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = null;

            if (Is_by_image)
            {
                if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                {
                    report1 = new CrysReports.abbas_abad.sharj_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                {
                    report1 = new CrysReports.paytakht.sharj_ghabz_by_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                {
                    report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_by_bg();

                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
                {
                    report1 = new CrysReports.shams_abad.sharj_ghabz_by_bg();

                }
            }
            else
            {
                if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
                {
                    report1 = new CrysReports.abbas_abad.sharj_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.paytakht)
                {
                    report1 = new CrysReports.paytakht.sharj_ghabz_no_bg();
                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.esf_bozorg)
                {
                    report1 = new CrysReports.esf_bozorg.sharj_ab_ghabz_by_bg();

                }
                else if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.shams_abad)
                {
                    report1 = new CrysReports.shams_abad.sharj_ghabz_by_bg();

                }
            }


            ta1.FillByGCode(ds.sharj_ghabz, Convert.ToInt32(gcode));

            string mablaghMAndehoroof = clsNumber.Number_to_Str(ds.sharj_ghabz.Rows[0]["mande"].ToString());

          


            // این قسمت به صورت موقت می باشد و باید حذف شود 
            if (ClsMain.ShahrakSetting.shahrak_ID == Shahrak_IDS.abbas_abad)
            {
                if (Classes.ClsMain.ShahrakSetting.id == 2)
                {
                    report1 = new CrysReports.abbas_abad.sharj_ghabz_by_bg();
                    try
                    {
                        ds.ghabz_image.Rows.Clear();
                        string shenase_ghabz = ds.sharj_ghabz[0]["shenase_ghabz"].ToString();
                        string shenase_pardakht = ds.sharj_ghabz[0]["shenase_pardakht"].ToString();
                        while (shenase_ghabz.Length < 13)
                            shenase_ghabz = "0" + shenase_ghabz;


                        while (shenase_pardakht.Length < 13)
                            shenase_pardakht = "0" + shenase_pardakht;
                        string barcode_number = shenase_ghabz + shenase_pardakht;
                        // add the column in table to store the image of Byte array type 
                        ds.ghabz_image.Rows.Add(clsBargeh.imageToByteArray(Classes.ISBN.GetImage(barcode_number.ToString(), 210, 45, 2)));

                    }
                    catch (Exception)
                    {

                    }
                }
            }



            report1.SetDataSource(ds);
            string tarikh = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["tarikh"])).ToString("d");
            string mohlat_pardakht = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

            ds.sharj_ghabz.Columns.Remove("tarikh");
            ds.sharj_ghabz.Columns.Remove("mohlat_pardakht");


            ds.sharj_ghabz.Columns.Add("tarikh");
            ds.sharj_ghabz.Columns.Add("mohlat_pardakht");

            ds.sharj_ghabz[0]["tarikh"] = tarikh;
            ds.sharj_ghabz[0]["mohlat_pardakht"] = mohlat_pardakht;


            DataView Dt_moshtarek_info = new DataView(ClsMain.MoshtarekDT);
            Dt_moshtarek_info.RowFilter = "gharardad=" + gharardad;

            report1.SetParameterValue("co_name", Dt_moshtarek_info[0]["co_name"].ToString());
            report1.SetParameterValue("pelak", Dt_moshtarek_info[0]["pelak"].ToString());
            report1.SetParameterValue("address", Dt_moshtarek_info[0]["address"].ToString());
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info[0]["modir_amel"].ToString());

            report1.SetParameterValue("shahrak_name", ClsMain.ShahrakSetting.shahrak_name);

            report1.SetParameterValue("mandehHoroof", mablaghMAndehoroof);
            report1.SetParameterValue("pshodeh", (Convert.ToInt64(ds.sharj_ghabz.Rows[0]["mablaghkol"]) - Convert.ToInt64(ds.sharj_ghabz.Rows[0]["mande"])).ToString());



            string Doreh = ds.sharj_ghabz.Rows[0]["dcode"].ToString();
            DataTable dt_doreh = Classes.ClsMain.GetDataTable("select start_time,end_time from sharj_doreh where dcode=" + Doreh);

            report1.SetParameterValue("dcodeinfo", ("از " + new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][0])).ToString("d") + " الی " +
                new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_doreh.Rows[0][1])).ToString("d")).ToString()
                );
            CrystalDecisions.Windows.Forms.CrystalReportViewer viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            viewer.ReportSource = report1;
            viewer.PrintReport();
            //new FrmShowReport(report1).ShowDialog();


        }

        public static void ChapSharjGhabz_Small(string gcode, string gharardad, string Mablaghhorrof, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.SharjGhabzSmall report1)
        {

            CrysReports.ReportDataSet ds = new CrysReports.ReportDataSet();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            ta.FillByGCode(ds.sharj_ghabz, Convert.ToInt32(gcode));

            ds.sharj_ghabz.Rows[0]["tarikh"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["tarikh"])).ToString("d");
            ds.sharj_ghabz.Rows[0]["mohlat_pardakht"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(ds.sharj_ghabz.Rows[0]["mohlat_pardakht"])).ToString("d");

            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("tarikh", PersianDate.Now.ToString("D"));
            //report1.SetParameterValue("gozaresh_type", "تک قبض شارژ");
            report1.SetParameterValue("mablaghkolh", Mablaghhorrof);

            viewer.ReportSource = report1;

            new FrmShowReport(report1).ShowDialog();

            //viewer.PrintReport();

        }


        public static string Create_ghobooz_sharj_Background()
        {


            try
            {
                clsSharj.Dcode = clsDoreh.Get_last_Sharj_dcode().ToString();
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex);
                return "fail";
            }


            DateTime start_time = DateTime.Now;
            //----------------------------------------------------------------
            //بدست اوردن اطلاعات مشترکین
            DataTable dt_all_moshtarek;
            dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where have_sharj_ghabz=1");
            if (dt_all_moshtarek.Rows.Count == 0)
                throw new NoMoshtarekException();
            //------------------------------------------------------------------


            DataTable sharj_bedehi_from_Past = null;
            //----------------------------------------------------------------
            //بدست اوردن مقادیر اولیه بدهی شارژ از جدول موقت 
            if (clsAb.Dcode == "1")
                sharj_bedehi_from_Past = ClsMain.GetDataTable("select * from sharj_bedehi_from_Past");
            //------------------------------------------------------------------



            //----------------------------------------------------------------
            //بدست اوردن  اطلاعات مربوط به قبوض دوره قبل
            DataTable Dt_all_ghabzof_Previous_doreh;
            Dt_all_ghabzof_Previous_doreh = ClsMain.GetDataTable("select gharardad,mande,kasr_hezar,bestankari from sharj_ghabz  where dcode=" + (Convert.ToInt32(clsSharj.Dcode) - 1).ToString());


            //------------------------------------------------------------------
            //----------------------------------------------------------------
            //بدست اوردن اطلاعات کلی در مورد همه دوره های اب
            DataTable dt_Sharj_info;
            try
            {
                dt_Sharj_info = ClsMain.GetDataTable("select top(1) * from sharj_info ");
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex, "sharj_info صدور قبوض شارز - خطا در خواندن ");
                return "fail";
            }
            //------------------------------------------------------------------




            //------------------------------------------------------------------
            //بدست اوردن اطلاعات مربوط به دوره 
            DataTable dt_doreh_info;
            try
            {
                dt_doreh_info = ClsMain.GetDataTable("select * from sharj_doreh where dcode=" + clsSharj.Dcode);
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex, " - عدم دسترسی به اطلاعات دوره -  فرایند صدور قبوض شارژ ");
                return "fail";
            }

            DateTime Sharj_Doreh_Mohlat_pardakht;
            try
            {
                Sharj_Doreh_Mohlat_pardakht = Convert.ToDateTime(dt_doreh_info.Rows[0]["mohlat_pardakht"]);
            }
            catch (Exception ex)
            {
                ClsMain.logError(ex, "فرایند صدور قبوض شارژ - مشکل در مهلت پرداخت");
                return "fail";
            }



            float Darsad_malit = Convert.ToSingle(dt_doreh_info.Rows[0]["darsad_maliat"]);
            decimal Sharj_Doreh_Tarefe_Per_day = Convert.ToDecimal(dt_doreh_info.Rows[0]["mablagh_sharj"]);
            float ZaribTejari = Convert.ToSingle(dt_doreh_info.Rows[0]["zaribtejari"]);
            int Tedad_rooz_doreh = ClsMain.Days_between_tarikh_ha(
                Convert.ToDateTime(dt_doreh_info.Rows[0]["end_time"]),
                Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"])
                );


            //------------------------------------------------------------------







            //------------------------------------------------------------------
            //فیلتر کردن مشترکین بر اساس قوانینی که در اینده تعیین خواهد شد 


            //------------------------------------------------------------------


            Classes.MultipleCommand multipe = new MultipleCommand("SoddorGhobooz");


            long Bedehi = 0, bestankari = 0;
            int kasr_hezar = 0;
            long Moshtarek_Mablagh_sharj = 0;
            long mandeh = 0;
            string tozihat = "";
            int vaziat_ghabz = 0;
            try
            {
                //------------------------------------------------------------------            
                //پیماش کلیه مشترکین موجود در لیست موجود صدور قبوض مربوطه
                allMoshtarekCount = dt_all_moshtarek.Rows.Count;
                for (int i = 0; i < dt_all_moshtarek.Rows.Count; i++)
                {
                    bestankari = 0;
                    Bedehi = 0;
                    kasr_hezar = 0;
                    Moshtarek_Mablagh_sharj = 0;
                    mandeh = 0;
                    bestankari = 0;
                    tozihat = "";
                    vaziat_ghabz = 0;
                    //.. شروع صدور قبض به ازا تک تک کارخانه دارها 
                    string Moshtarek_Gharardad = dt_all_moshtarek.Rows[i]["gharardad"].ToString();
                    decimal Moshtarek_Metraj = Convert.ToDecimal(dt_all_moshtarek.Rows[i]["metraj"]);
                    bool ISTejari = Convert.ToBoolean(dt_all_moshtarek.Rows[i]["tejari"]);

                    decimal tempMablagh = Moshtarek_Metraj * Tedad_rooz_doreh *
                       Convert.ToDecimal((Sharj_Doreh_Tarefe_Per_day / new FarsiLibrary.Utils.PersianCalendar().GetDaysInYear(new PersianDate(Convert.ToDateTime(dt_doreh_info.Rows[0]["start_time"])).Year)));

                    if (ISTejari)
                    {
                        Moshtarek_Mablagh_sharj = GetRoundedInteger(tempMablagh * (decimal)ZaribTejari);
                    }
                    else
                        Moshtarek_Mablagh_sharj = GetRoundedInteger(tempMablagh);







                    //----------------------------- قرائت و اطلاعات مربوط به دوره قبلی شارژ -------------------
                    if (Dt_all_ghabzof_Previous_doreh.Rows.Count == 0)
                    {
                        //-- برای اولین بار می بایست از جدول دیگری خوانده شود که کاربر از منوی مدیریت می تواند محتویات ان را تعیین کند
                        try
                        {
                            var Sharj_firstTime_row = sharj_bedehi_from_Past.AsEnumerable().Where(p => p.Field<int>("gharardad").ToString() == Moshtarek_Gharardad).First();
                            Bedehi = Convert.ToInt64(Sharj_firstTime_row["bedehi"]);
                            kasr_hezar = 0;
                        }
                        catch
                        {
                            kasr_hezar = 0;
                            Bedehi = 0;
                            bestankari = 0;
                        }



                    }
                    else
                    {
                        try
                        {
                            var Sharj_ghabz_Of_Previous_doreh = Dt_all_ghabzof_Previous_doreh.AsEnumerable().Where(p => p.Field<int>("gharardad").ToString() == Moshtarek_Gharardad).First();
                            Bedehi = Convert.ToInt64(Sharj_ghabz_Of_Previous_doreh["mande"]);
                            kasr_hezar = Convert.ToInt32(Sharj_ghabz_Of_Previous_doreh["kasr_hezar"]);
                            bestankari = Convert.ToInt64(Sharj_ghabz_Of_Previous_doreh["bestankari"]);

                        }
                        catch (Exception)
                        {

                            kasr_hezar = 0;
                            Bedehi = 0;
                            bestankari = 0;
                        }

                    }
                    //------------------------------------------------------------------









                    //------------------------------------------------------------------            
                    long Maliat = GetRoundedInteger(Convert.ToDecimal(Moshtarek_Mablagh_sharj * Darsad_malit / 100));
                    //------------------------------------------------------------------    






                    //محاسبه مبلغ نهایی
                    long Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_sharj + Bedehi + kasr_hezar + Maliat;



                    //------------------------------------------------------------------            
                    if (bestankari >= Moshtarek_Mablagh_Kol)
                    {
                        kasr_hezar = 0;
                        bestankari -= Moshtarek_Mablagh_Kol;
                        tozihat = "کسر کامل مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل " + " معادل " + Moshtarek_Mablagh_Kol.ToString();
                        Moshtarek_Mablagh_Kol = mandeh = Maliat = 0;
                        vaziat_ghabz = 1;
                    }
                    else
                    {
                        Moshtarek_Mablagh_Kol -= bestankari;
                        if (bestankari != 0)
                            tozihat = "کسر " + bestankari.ToString() + " ریال " + " از مبلغ قبض بواسطه مبلغ بستانکاری از دوره قبل ";
                        bestankari = 0;
                        kasr_hezar = Convert.ToInt32(Moshtarek_Mablagh_Kol % 1000);
                        Moshtarek_Mablagh_Kol = Moshtarek_Mablagh_Kol - kasr_hezar;

                        mandeh = Moshtarek_Mablagh_Kol;

                    }








                    Classes.ClsMain.ChangeCulture("e");
                    Sodoor_ghabz(multipe, Moshtarek_Gharardad, clsSharj.Dcode, Sharj_Doreh_Tarefe_Per_day.ToString(), Moshtarek_Mablagh_sharj.ToString(),
                        Moshtarek_Metraj.ToString(CultureInfo.InvariantCulture), Sharj_Doreh_Mohlat_pardakht, Moshtarek_Mablagh_Kol.ToString(), Darsad_malit,
                        Maliat, Bedehi.ToString(), Tedad_rooz_doreh.ToString(), kasr_hezar, 0, "", ISTejari, 0, bestankari, tozihat, vaziat_ghabz, mandeh);



                }

                if (multipe.RunCommandCollecton("clssharj.Create_ghobooz_sharj_Background"))
                {
                    Payam.Show("عملیات صدور قبض شارژ با موفقیت انجام شد");
                    return "success";
                }
                else
                {
                    Payam.Show("خطا در انجام صدور قبوض شارژ. لطفا با پشتیبان هماهنگ نمایید");
                    return "fail";
                }

            }
            catch (Exception ex)
            {

                ClsMain.logError(ex, "sodoor ghobooz sharj transaction");
                Payam.Show("خطا در انجام صدور قبوض شارژ. لطفا با پشتیبان هماهنگ نمایید");
                return "fail";
            }




        }



        public static void ChapSharjDoreh(string dcode, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.Sharj_doreh report1, string typeOfReport, string gharardad)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.Sharj_DorehTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.Sharj_DorehTableAdapter();
            if (dcode == "all")
                ta.Fill(ds.Sharj_Doreh, Convert.ToInt32(gharardad));
            else
                ta.FillByDCode(ds.Sharj_Doreh, Convert.ToInt32(gharardad), Convert.ToInt32(dcode));

            ArrayList dt_times = new ArrayList();
            for (int i = 0; i < ds.Sharj_Doreh.Rows.Count; i++)
            {
                dt_times.Add(ds.Sharj_Doreh.Rows[i]["tarikh_vosool"]);
            }
            ds.Sharj_Doreh.Columns.Remove("tarikh_vosool");
            ds.Sharj_Doreh.Columns.Add("tarikh_vosool");

            for (int i = 0; i < ds.Sharj_Doreh.Rows.Count; i++)
            {
                if (dt_times[i].ToString() == "")
                    ds.Sharj_Doreh.Rows[i]["tarikh_vosool"] = "";
                else
                    ds.Sharj_Doreh.Rows[i]["tarikh_vosool"] = PersianDateConverter.ToPersianDate(Convert.ToDateTime(dt_times[i])).ToString("d");
            }



            report1.SetDataSource(ds);



            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());

            //report1.SetParameterValue("mablaghkolh", Mablaghhorrof);
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            report1.SetParameterValue("tarikh", PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d"));
            viewer.ReportSource = report1;
            ExportOptions exoption = new ExportOptions();

            new FrmShowReport(report1).ShowDialog();

            //if (typeOfReport == "print")
            //{
            //    viewer.PrintReport();
            //}

            //else
            //{
            //    exoption.ExportFormatType = ExportFormatType.PortableDocFormat;
            //    viewer.ExportReport();
            //}
        }




        public static DataTable Get_last_Sharj_mablagh_tasvib_shodeh()
        {
            Classes.ClsMain.ChangeCulture("e");
            return Classes.ClsMain.GetDataTable("select top(1) * from sharj_mablagh where tarikh_ejra<='" + DateTime.Now + "'  order by tarikh_ejra desc");

        }
        public static void Lock_Sharj_Ghobooz_of_Doreh_Ghabl(string CodeOfDorehGhabl)
        {
            Classes.ClsMain.ExecuteNoneQuery("update sharj_ghabz set locked=1 where dcode=" + CodeOfDorehGhabl);

        }


        static long GetRoundedInteger(float number)
        {
            return Convert.ToInt64(Math.Round(Convert.ToDecimal(number)));
        }
        static long GetRoundedInteger(decimal number)
        {
            return Convert.ToInt64(Math.Round(number));
        }





        public static void Create_Tak_Ghabz(bool IsNewGhabz, string docde, string gharardad, bool IsTejari, long Bedehi, int kasr_hezar,
            decimal Moshtarek_Metraj, long jarimeh, string tozihat, int Tedad_rooz_doreh, long sayer,
            string sayer_tozihat, float Darsad_malit, long Moshtarek_Mablagh_Kol, long Moshtarek_Mablagh_sharj, long Maliat, long Bestankari, long mandeh, int vaziat_ghabz)
        {

            clsSharj.Dcode = docde;
            DataTable dt_all_moshtarek = ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);
            DataTable dt_doreh_info = ClsMain.GetDataTable("select * from sharj_doreh where dcode=" + clsSharj.Dcode);
            DateTime Sharj_Doreh_Mohlat_pardakht = Convert.ToDateTime(dt_doreh_info.Rows[0]["mohlat_pardakht"]);
            float Sharj_Doreh_Tarefe_Per_day = Convert.ToSingle(dt_doreh_info.Rows[0]["mablagh_sharj"]);
            float ZaribTejari = Convert.ToSingle(dt_doreh_info.Rows[0]["zaribtejari"]);
            //------------------------------------------------------------------

            Classes.MultipleCommand multiple = new MultipleCommand("tak_ghabz");
            try
            {
                Classes.ClsMain.ChangeCulture("e");
                if (IsNewGhabz)
                {
                    New_ghabz(multiple, gharardad, clsSharj.Dcode, Sharj_Doreh_Tarefe_Per_day.ToString(), Moshtarek_Mablagh_sharj.ToString(), Moshtarek_Metraj.ToString(CultureInfo.InvariantCulture), Sharj_Doreh_Mohlat_pardakht, Moshtarek_Mablagh_Kol.ToString(), Darsad_malit, Maliat, Bedehi.ToString(), Tedad_rooz_doreh.ToString(), kasr_hezar, sayer, sayer_tozihat, IsTejari, jarimeh, Bestankari, tozihat, vaziat_ghabz, mandeh);

                }
                else
                {
                    Update_ghabz(multiple, gharardad, clsSharj.Dcode, Sharj_Doreh_Tarefe_Per_day.ToString(), Moshtarek_Mablagh_sharj.ToString(), Moshtarek_Metraj.ToString(CultureInfo.InvariantCulture), Sharj_Doreh_Mohlat_pardakht, Moshtarek_Mablagh_Kol.ToString(), Darsad_malit, Maliat, Bedehi.ToString(), Tedad_rooz_doreh.ToString(), kasr_hezar, sayer, sayer_tozihat, IsTejari, jarimeh, Bestankari, mandeh, tozihat);

                }

                Classes.ClsMain.ChangeCulture("f");
            }
            catch (Exception GhbazError)
            {
                Payam.Show("عملیات صدور قبض با خطا مواجه شد");
                Classes.ClsMain.logError(GhbazError);
            }
            if (multiple.RunCommandCollecton("clsSharj.Create_Tak_Ghabz"))
                Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
            else
                Payam.Show("عملیات صدور قبض با خطا مواجه شد");


        }





        //-----------------------------------------



        public static void New_ghabz(Classes.MultipleCommand multile, string gharardad,
            string dcode, string tarefe, string mablagh,
            string metraj, DateTime MohltPardakht,
            string MablaghKol, float darsad_maliat, long maliat,
            string Bedehi, string Tedad_rooz_doreh, int kasr_hezar,
            long sayer, string sayer_tozihat, bool ISTejari, long jarimeh,
            long bestankari, string tozihat, int vaziat_ghabz, long mandeh)
        {
            try
            {
                int tejari = 0;
                if (ISTejari)
                    tejari = 1;
                string gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(dcode),
                    false, Convert.ToInt32(gharardad));

                multile.addToCommandCollecton("INSERT INTO sharj_ghabz   (gcode,[gharardad],[dcode],[tarikh],[mablagh],tarefe,metraj,mohlat_pardakht,mablaghkol,darsad_maliat,maliat,bedehi,tedadRooz,kasr_hezar,sayer,sayer_tozihat,mande,tejari,jarimeh,vaziat,tozihat,bestankari)   values (" + gcode + "," + gharardad + "," + dcode + ",'" + DateTime.Now + "'," + mablagh + "," + tarefe + "," + metraj + ",'" + MohltPardakht + "'," + MablaghKol.ToString() + "," + darsad_maliat.ToString() + "," + maliat.ToString() + "," + Bedehi + "," + Tedad_rooz_doreh + "," + kasr_hezar.ToString() + "," + sayer.ToString() + ",'" + sayer_tozihat + "'," + mandeh + "," + tejari + "," + jarimeh + "," + vaziat_ghabz + ",'" + tozihat + "'," + bestankari + ")");


            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }







        public static void Update_ghabz(Classes.MultipleCommand multile, string gharardad, string dcode, string tarefe, string mablagh, string metraj, DateTime MohltPardakht, string MablaghKol, float darsad_maliat, long maliat, string Bedehi, string Tedad_rooz_doreh, int kasr_hezar, long sayer, string sayer_tozihat, bool ISTejari, long jarimeh, long bestankari, long mandeh, string tozihat)
        {
            try
            {
                int tejari = 0;
                if (ISTejari)
                    tejari = 1;

                string gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(dcode),
                    false, Convert.ToInt32(gharardad));
                int vaziat_ghabz = 0;


                if (mandeh == 0)
                    vaziat_ghabz = 1;
                else if (mandeh > 0 && mandeh != Convert.ToInt64(MablaghKol))
                    vaziat_ghabz = 2;



                string Command = "update sharj_ghabz set "
                    + "[gharardad]=" + gharardad + ",[dcode]=" + dcode + ",[mablagh]=" + mablagh + ",tarefe=" + tarefe
                    + ",metraj=" + metraj + ",mohlat_pardakht='" + MohltPardakht + "',mablaghkol=" + MablaghKol + ",darsad_maliat=" + darsad_maliat.ToString()
                    + ",maliat=" + maliat + ",bedehi=" + Bedehi + ",tedadRooz=" + Tedad_rooz_doreh + ",kasr_hezar=" + kasr_hezar
                    + ",sayer=" + sayer + ",sayer_tozihat='" + sayer_tozihat + "',tejari=" + tejari + ",jarimeh=" + jarimeh + ",bestankari=" + bestankari
                    + ",vaziat=" + vaziat_ghabz + ",mande=" + mandeh + ",tozihat='" + tozihat + "' where gcode=" + gcode;

                multile.addToCommandCollecton(Command);
            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }




        public static void Sodoor_ghabz(Classes.MultipleCommand multile, string gharardad, string dcode, string tarefe, string mablagh, string metraj, DateTime MohltPardakht, string MablaghKol, float darsad_maliat, long maliat, string Bedehi, string Tedad_rooz_doreh, int kasr_hezar, long sayer, string sayer_tozihat, bool ISTejari, long jarimeh, long bestankari, string tozihat, int vaziat_ghabz, long Mandeh)
        {
            try
            {
                int tejari = 0;
                if (ISTejari)
                    tejari = 1;
                string gcode = ClsMain.CodeGhabzGenerate(Convert.ToInt32(dcode),
                    false, Convert.ToInt32(gharardad));
                multile.addToCommandCollecton("INSERT INTO sharj_ghabz (gcode,[gharardad],[dcode],[tarikh],[mablagh],tarefe,metraj,mohlat_pardakht,mablaghkol,darsad_maliat,maliat,bedehi,tedadRooz,kasr_hezar,sayer,sayer_tozihat,mande,tejari,jarimeh,vaziat,tozihat,bestankari)   values (" + gcode + "," + gharardad + "," + dcode + ",'" + DateTime.Now + "'," + mablagh + "," + tarefe + "," + metraj + ",'" + MohltPardakht + "'," + MablaghKol.ToString() + "," + darsad_maliat.ToString() + "," + maliat.ToString() + "," + Bedehi + "," + Tedad_rooz_doreh + "," + kasr_hezar.ToString() + "," + sayer.ToString() + ",'" + sayer_tozihat + "'," + Mandeh.ToString() + "," + tejari + "," + jarimeh + "," + vaziat_ghabz + ",'" + tozihat + "'," + bestankari + ")");
            }
            catch (Exception ex)
            {
                if (ex is CodeGhabzGenerateException)
                    Payam.Show(ex.Message);
                else
                    ClsMain.logError(ex, ex.Source);
            }
        }

        public static DataTable Get_Ghobooz_pardakhtShode_info(string dcode)
        {

            return ClsMain.GetDataTable(" SELECT  SUM(mablaghkol) as majmoo ,count(mablaghkol) as tedad FROM  sharj_ghabz WHERE dcode=" + dcode.ToString() + " AND (vaziat = 1)");
        }

        public static Boolean IsSharjInfoExist()
        {
            if (ClsMain.ExecuteScalar("select count(*) from sharj_info").ToString() == "0")
                return false;
            return true;
        }

        public static DataTable GetSharjInfo()
        {
            return ClsMain.GetDataTable("select * from sharj_info");

        }

        public static DataTable GetSharjDoreh()
        {
            return ClsMain.GetDataTable("select * from sharj_doreh");

        }
        public static int GetSharjDoreh_Count()
        {
            return Convert.ToInt32(ClsMain.ExecuteScalar("select count(*) from sharj_doreh"));

        }

        public static DataTable Get_Ghobooz_pardakhtNashodeShode_info(string dcode)
        {

            return ClsMain.GetDataTable(" SELECT  SUM(mablaghkol) as majmoo ,count(mablaghkol) as tedad FROM  sharj_ghabz WHERE dcode=" + dcode.ToString() + " AND (vaziat = 0)");
        }

        public static DataTable Get_Ghobooz_DarHalBarrasi_info(string dcode)
        {

            return ClsMain.GetDataTable(" SELECT  SUM(mablaghkol) as majmoo ,count(mablaghkol) as tedad FROM  sharj_ghabz WHERE dcode=" + dcode.ToString() + " AND (vaziat = 2)");
        }



        public static int Success_Tedad_ghobooz_saderShode = 0, Fail_Tedad_ghobooz_saderShode = 0;



        public static float MoshtarekBedehi(string Gharardad)
        {
            float Bedehi = 0;

            DataTable DtLastGhabz = ClsMain.GetDataTable("select top(1) * from sharj_ghabz where gharardad=" + Gharardad + " order by gcode desc");
            if (DtLastGhabz.Rows.Count == 0)
                return 0;

            //--------------------- در صورتی که وضعیت قبض قبلی مشترک پرداخت نشده باشد
            if (Convert.ToInt32(DtLastGhabz.Rows[0]["vaziat"]) == 2)
            {
                //---------------- در صورتی که مشترک قبض را به طور کامل پرداخت نکرده باشد ، یعنی پرداخت داشته باشد اما وضعیت کلی قبض به صورت پرداخت شده نباشد
                //---------------- در این قسمت کلیه پرداخت هایی که توسط اپراتور تایید شده باشد ، مجموع انها حساب شده  
                //------------- و از مبلغ کل بدهی کاربر که قرار است بدهی دور بعد انتقال پیدا کند کم می شود
                float SumOfPardakhti = Convert.ToSingle(ClsMain.ExecuteScalar("select ISNULL(sum(mablagh),0) from ghabz_pardakht where is_ab=0 and vaziat=1 and gcode=" + DtLastGhabz.Rows[0]["gcode"].ToString()));
                Bedehi = Convert.ToSingle(DtLastGhabz.Rows[0]["mablaghkol"]) - SumOfPardakhti;

            }
            else if (Convert.ToInt32(DtLastGhabz.Rows[0]["vaziat"]) == 0)
            {
                Bedehi = Convert.ToSingle(DtLastGhabz.Rows[0]["mablaghkol"]);

            }

            return Bedehi;
        }





        public static void UpdateDorehInfoAtEndOfSodoorProcess(DateTime Start_time, DateTime End_time, string dcode)
        {
            ClsMain.ExecuteNoneQuery("update sharj_doreh set ghabz_sodoor=1 where dcode=" + dcode);

        }
















        public static int Check_new_sharj_doreh_should_created_and_return_NewDcode()
        {
            try
            {

                DataTable DtLastSharjDoreh = Classes.clsDoreh.Get_last_Sharj_Doreh();
                if (DtLastSharjDoreh.Rows.Count == 0)
                    return 1;

                DateTime NewDoreh_StartTime = Convert.ToDateTime(DtLastSharjDoreh.Rows[0]["end_time"]);
                if (DateTime.Now >= NewDoreh_StartTime)
                {
                    return Convert.ToInt32(DtLastSharjDoreh.Rows[0]["dcode"]) + 1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Classes.ClsMain.LogIt(ex);
                return -1;
            }


        }




        public static void Ijad_Doreh_jadid_sharj_V2(int dcode, DateTime? startTime, DateTime? EndTime, DateTime? MohlatPardakht, float Mablgh, string darsad_maliat, string tejari)
        {
            ClsMain.ExecuteNoneQuery("insert into sharj_doreh (dcode,start_time,end_time,mohlat_pardakht,mablagh_sharj,darsad_maliat,ZaribTejari,visiblebymoshtarek)"
                + " values (" + dcode + ",'" + startTime + "','" + EndTime + "','" + MohlatPardakht + "'," + Mablgh + "," + darsad_maliat + "," + tejari + ",0)");
        }


        public static int Get_last_doreh_dcode()
        {
            object code_doreh = ClsMain.ExecuteScalar("select max(dcode) from sharj_doreh");
            if (code_doreh.ToString().Trim() == "")
                return -1;
            return Convert.ToInt32(code_doreh);

        }



        public static DateTime Get_last_sharj_doreh_endTime()
        {
            return Convert.ToDateTime(ClsMain.ExecuteScalar("select top(1) end_time from sharj_doreh order by dcode desc"));
        }




        public static int Get_day_ta_shoru_doreh_badd()
        {
            FarsiLibrary.Utils.PersianCalendar pc = new FarsiLibrary.Utils.PersianCalendar();
            DateTime Last_doreh_end_Last_doreh_end_time = Get_last_sharj_doreh_endTime();

            return (Last_doreh_end_Last_doreh_end_time - DateTime.Now).Days + 1;


        }






        //.. بخش قرارداد

        public static DataTable get_akharin_ghabz(string dcode, string gharardad)
        {
            return ClsMain.GetDataTable("select * from sharj_ghabz where dcode=" + dcode + " and gharardad=" + gharardad);

        }
        public static DataTable get_akharin_ghabz(string gcode)
        {
            return ClsMain.GetDataTable("select * from sharj_ghabz where gcode=" + gcode);

        }



        public static void Delete_ghabz_sharj(string gcode)
        {
            ClsMain.ExecuteNoneQuery("delete from ghabz_pardakht where gcode=" + gcode);
            ClsMain.ExecuteNoneQuery("delete from sharj_ghabz where gcode=" + gcode);

        }

        public static void Set_PardakhtShodeh_For_All_Previose_Ghabz(string gcode)
        {
            ClsMain.ExecuteNoneQuery("exec Sharj_ghabz_Vaziat_Pardakht_Ghobooz_Ghabli " + gcode);

        }
        public static void Set_PardakhtNaShodeh_For_All_Previose_Ghabz(string gcode)
        {
            ClsMain.ExecuteNoneQuery("exec Sharj_ghabz_Vaziat_PardakhtNashode_Ghobooz_Ghabli " + gcode);

        }

    }
}
