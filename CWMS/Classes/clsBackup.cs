﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace CWMS.Classes
{
    class clsBackup
    {
        public static bool  BackupDatabase(SqlConnection con, string databaseName, string backupPath ,string backupName)
        {
          
            try
            {
                long inde = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull(max(id),1) from  backup_history"));
                FarsiLibrary.Utils.PersianDate Alan = new FarsiLibrary.Utils.PersianDate(DateTime.Now);
                string backupDirectory = Alan.Year + " - " + Alan.Month + " - " + Alan.Day + " -- " + Alan.Hour + " - " + Alan.Minute;

                string Fullpath = backupPath + "\\" + backupDirectory;
                if (System.IO.Directory.Exists(Fullpath))
                    System.IO.Directory.Delete(Fullpath, true);

                System.IO.Directory.CreateDirectory(Fullpath);

                con.Open();
                using (var cmd = new SqlCommand(string.Format(
                    "backup database {0} to disk = {1} with stats = 1",
                    QuoteIdentifier(databaseName),
                    QuoteString(Fullpath + "\\" + backupName  + (inde + 1).ToString() + ".nbk")), con))
                {
                    cmd.ExecuteNonQuery();
                }
                con.Close();
                return true;
                    
            }
            catch (Exception ex)
            {
                Classes.ClsMain.logError(ex, "ClsBackUp Fails");
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        private static string QuoteIdentifier(string name) { return "[" + name.Replace("]", "]]") + "]"; }

        private static string QuoteString(string text) { return "'" + text.Replace("'", "''") + "'"; }
    }
}
