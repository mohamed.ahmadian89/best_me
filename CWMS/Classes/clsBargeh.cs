﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FarsiLibrary.Utils;
using System.IO;
using System.Windows.Forms;

namespace CWMS.Classes
{
    class clsBargeh
    {
        public static string Client_connection_string = System.Configuration.ConfigurationManager.ConnectionStrings["CWMS.Properties.Settings.Client_connectionString"].ConnectionString;
        public static bool AddList_started = false, BlockList_started = false,
            DeletedList_started = false, moshtarekin_started = false, AllChangedFromServer_started = false, AllChangedFromClient_started = false;

        public static string should_reload_data_in_client_command = ";update Bargeh_info set should_update=1";

        public static void Log_transfer_report(string action, DateTime start_it, DateTime end_it, int status)
        {
            Classes.ClsMain.ChangeCulture("e");
            ClsMain.ExecuteNoneQuery("insert into bargeh_transfer values ('" + DateTime.Now + "','" + start_it + "','" + end_it + "'," + status + ",'" + action + "')");
        }



        static void GenerateBarcode(string gharardad)
        {

            if (System.IO.Directory.Exists(Application.StartupPath + "\\bargeh\\" + gharardad) == true)
                System.IO.Directory.Delete(Application.StartupPath + "\\bargeh\\" + gharardad);

            System.IO.Directory.CreateDirectory(Application.StartupPath + "\\bargeh\\" + gharardad);

            DataTable dt_bargeh = Classes.ClsMain.GetDataTable("select top(1) start,finish from barge_khorooj where gharardad=" + gharardad);
            if (dt_bargeh.Rows.Count != 0)
            {
                for (long i = Convert.ToInt64(dt_bargeh.Rows[0]["start"]); i < Convert.ToInt64(dt_bargeh.Rows[0]["finish"]); i++)
                {
                    Classes.ISBN.GenerateAndSave(i, 200, 5, Application.StartupPath + "\\bargeh\\" + gharardad + "\\" + i.ToString() + ".jpg");
                }
            }
        }


        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }


        public static void ChapBargeh(string gharardad, string darkhastID, CrystalDecisions.Windows.Forms.CrystalReportViewer viewer, CrysReports.BargehKhorooj report1, int start, int end)
        {
            CrysReports.ReportDataSet2 ds = new CrysReports.ReportDataSet2();
            CrysReports.ReportDataSet2TableAdapters.barge_khorooj_detailsTableAdapter ta = new CrysReports.ReportDataSet2TableAdapters.barge_khorooj_detailsTableAdapter();
            ta.FillByDarkhastRange(ds.barge_khorooj_details, Convert.ToInt32(darkhastID), start, end);
            //GenerateBarcode(gharardad);
            for (int i = 0; i < ds.barge_khorooj_details.Rows.Count; i++)
            {
                try
                {
                    //FilStr = new FileStream(System.Windows.Forms.Application.StartupPath + "\\bargeh\\" + gharardad + "\\" + ds.barge_khorooj_details.Rows[i]["id"].ToString() + ".jpg", FileMode.Open);
                    //BinaryReader BinRed = new BinaryReader(FilStr);
                    //Byte[] b = BinRed.ReadBytes((int)BinRed.BaseStream.Length);
                    Byte[] b = imageToByteArray(Classes.ISBN.GetImage(ds.barge_khorooj_details.Rows[i]["id"].ToString(), 200, 5));
                    ds.bargeh_image.Rows.Add(ds.barge_khorooj_details.Rows[i]["id"].ToString(), b);
                }
                catch (Exception ed)
                {

                    Payam.Show("متاسفانه در چاپ برخی از برگه های خروج مشکل پیش امده است  لطفا با پشتیبان نرم افزار هماهنگ فرمایید");
                    ClsMain.logError(ed);
                }


            }

            report1.SetDataSource(ds);
            DataTable Dt_moshtarek_info = Classes.ClsMain.GetDataTable("select co_name,modir_amel from moshtarekin where gharardad=" + gharardad);
            report1.SetParameterValue("co_name", Dt_moshtarek_info.Rows[0]["co_name"].ToString());
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ShahrakSetting.shahrak_name);
            report1.SetParameterValue("modir_amel", Dt_moshtarek_info.Rows[0]["modir_amel"].ToString());
            viewer.ReportSource = report1;
            viewer.PrintReport();
            Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj set printed=1 where id=" + darkhastID);
        }




        public static DataTable GetBrageForMoshtarek(string gharardad)
        {
            return ClsMain.GetDataTable("select * from barge_khorooj where gharardad=" + gharardad + " order by tarikh desc");
        }
        public static void RadDarkhast(string DarkhastCode, string tozihat, string gharardad)
        {
            if (tozihat.Trim() == "")
                tozihat = "عدم ذکر هیچ گونه دلیل برای رد درخواست شما";
            ClsMain.ExecuteNoneQuery("update barge_khorooj set tozihat='" + tozihat + "',barrasi=0 where id=" + DarkhastCode);
            clsMoshtarekin.Notify(tozihat, gharardad, "بررسی درخواست برگه خروج  ");
        }
        public static void TqayidDarkhast(string DarkhastCode, string tozihat, string gharardad, string tedad)
        {
            if (tozihat.Trim() == "")
                tozihat = "درخواست شما بررسی و مورد تایید قرار گرفت .   ";
            Int64 start = GetBareh_Start_number();
            Int64 finish = (start + Convert.ToInt64(tedad) - 1);

            ClsMain.ExecuteNoneQuery("update barge_khorooj set tozihat=N'" + tozihat + "',barrasi=1,start=" + start.ToString() + ",finish=" + finish.ToString() + " where id=" + DarkhastCode);
            clsMoshtarekin.Notify(tozihat, gharardad, "بررسی درخواست برگه خروج");
            Sodoor_bargeh(start, finish, gharardad, Convert.ToInt32(GetBareh_Darkhast_code(gharardad)));
            UpdateBareh_Start_number((start + Convert.ToInt64(tedad)).ToString());

        }

        public static void HazDarkhast(string DarkhastCode, string tozihat, string gharardad)
        {
            try
            {
                if (tozihat.Trim() == "")
                    tozihat = "درخواست شما از سیستم حذف شد";
                ClsMain.ExecuteNoneQuery("delete from  barge_khorooj_details where darkhast=" + DarkhastCode);


                ClsMain.ExecuteNoneQuery("delete from  barge_khorooj where id=" + DarkhastCode);
                clsMoshtarekin.Notify(tozihat, gharardad, "حذف درخواست برگه خروج");
            }
            catch (Exception ert)
            {
                ClsMain.logError(ert);
                Payam.Show("عملیات حذف با خطا مواجه شد . لطفا موضوع را با پشتیبان در جریان بگذارید .");

            }


        }


        public static void DarkhastJadidByMmoshtarek_web(string gharardad, string tedad,string tarikh)
        {
            Classes.ClsMain.ChangeCulture("e");
            ClsMain.ExecuteNoneQuery("insert into barge_khorooj (gharardad,tedad,tarikh) values (" + gharardad + "," + tedad.ToString() + ",'" + tarikh + "')");
        }

        public static void DarkhastJadidByAdmin(string gharardad, string tedad)
        {
            int start = GetBareh_Start_number();
            int finish = (start + Convert.ToInt32(tedad) - 1);
            int darkhast_id = 0;
            string tozih = "ثبت درخواست صدور برگه خروج توسط مدیریت";
            Classes.ClsMain.ChangeCulture("e");
            ClsMain.ExecuteNoneQuery("insert into barge_khorooj values (" + gharardad + "," + tedad.ToString() + ",'" + DateTime.Now + "',N'" + tozih + "',1," + clsSharj.Get_last_doreh_dcode().ToString() + "," + start.ToString() + "," + finish.ToString() + ",0)");

            darkhast_id = Convert.ToInt32(GetBareh_Darkhast_code(gharardad));
            Sodoor_bargeh(start, finish, gharardad, darkhast_id);
            UpdateBareh_Start_number((finish + 1).ToString());
            Classes.ClsMain.ChangeCulture("f");

            //==== client addlist
            clsBargeh.client_new_addlist(start, finish, gharardad, darkhast_id);


            //return new int[] { start, finish, darkhast_id };
        }

        public static int GetBareh_Start_number()
        {
            int start = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select counter from Bargeh_info"));
            return start;
        }

        public static string GetBareh_Darkhast_code(string gharardad)
        {
            return Classes.ClsMain.ExecuteScalar("select top(1) id from Barge_khorooj where gharardad=" + gharardad + " order by id desc").ToString();

        }

        public static bool GetBareh_Status(string id)
        {
            int res = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select barrasi from barge_khorooj where id=" + id));
            if (res == 0)
                return false;

            return true;

        }

        public static void UpdateBareh_Start_number(string number)
        {

            Classes.ClsMain.ExecuteScalar("update Bargeh_info set counter=" + number);

        }
        public static void UpdateBareh_Tozihat(string Tozihat, string id)
        {
            Classes.ClsMain.ExecuteScalar("update barge_khorooj set tozihat=N'" + Tozihat + "' where id=" + id);

        }


        public static void Sodoor_bargeh(Int64 start, Int64 finish, string gharardad, int darkhast)
        {
            Classes.ClsMain.ExecuteNoneQuery("exec Sodoor_barge_khorooj " + start.ToString() + "," + finish.ToString() + "," + darkhast + "," + gharardad);
        }


        public static void block_unblock_bargeh(int Bargeh_ID, int darkhast_id, int block)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj_details set block=" + block + " where id=" + Bargeh_ID);
            clsBargeh.client_new_Blocklist(darkhast_id, Bargeh_ID, block);
            Classes.ClsMain.ChangeCulture("f");

        }





        //============================= Client OPeration =============================

        public static void client_delete_deletelist(int darkhast_id)
        {
            if (darkhast_id == 0)
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_deletelist");
            else
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_deletelist where darkhast_id=" + darkhast_id);
        }

        public static void client_delete_addlist(int darkhast_id)
        {
            if (darkhast_id == 0)
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_addlist");
            else
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_addlist where darkhast_id=" + darkhast_id);
        }


        public static void client_delete_blocklist(int darkhast_id)
        {
            if (darkhast_id == 0)
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_blocklist");
            else
                Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_blocklist where darkhast_id=" + darkhast_id);
        }


        public static void client_new_Blocklist(int darkhast_id, int bergeh_id, int block)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("insert into bargeh_client_blocklist values (" + darkhast_id + ",'" + DateTime.Now + "'," + bergeh_id + "," + block + ");");
            Classes.ClsMain.ChangeCulture("f");
        }

        public static void client_new_addlist(int start, int finish, string gharardad, int darkhast)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("insert into bargeh_client_addlist values (" + darkhast + "," + gharardad + ",'" + DateTime.Now + "'," + start + "," + finish + ")");
            Classes.ClsMain.ChangeCulture("f");

        }
        public static void client_new_Deletelist(int darkhast_id)
        {
            Classes.ClsMain.ChangeCulture("e");
            Classes.ClsMain.ExecuteNoneQuery("insert into bargeh_client_deletelist values (" + darkhast_id + ",'" + DateTime.Now + "');"
                 + "delete from bargeh_client_addlist where darkhast_id=" + darkhast_id + ";"
                 + "delete from bargeh_client_blocklist where darkhast_id=" + darkhast_id + ";"
                 );
            Classes.ClsMain.ChangeCulture("f");

        }



        //public static void client_Delete_addlist(int darkhast_id)
        //{
        //    Classes.ClsMain.ChangeCulture("e");
        //    Classes.ClsMain.ExecuteNoneQuery("delete from bargeh_client_addlist where darkhast_id=" + darkhast_id);
        //    Classes.ClsMain.ChangeCulture("f");

        //}

        //public static int Client_delete_darkhast(int darkhast_id)
        //{
        //    return Convert.ToInt32(Classes.ClsMain.ExecuteScalar("exec Sodoor_barge_khorooj " + darkhast_id, clsBargeh.Client_connection_string));
        //}

        public static void Send_DeleteList_to_Client(object Online)
        {
            DateTime start_it = DateTime.Now;
            DataTable dt_DeleteList = ClsMain.GetDataTable("select * from bargeh_client_deletelist");
            if (dt_DeleteList.Rows.Count == 0)
            {
                MessageBox.Show("هیچ درخواست جهت حذف وجود ندارد");
                Classes.clsBargeh.DeletedList_started = false;
                return;
            }
            int count = dt_DeleteList.Rows.Count;

            string Command = "delete from barge_khorooj_details where darkhast in (";
            for (int i = 0; i < dt_DeleteList.Rows.Count; i++)
            {
                Command += dt_DeleteList.Rows[i]["darkhast_id"].ToString() + ",";
            }
            Command = Command.Remove(Command.Length - 1) + ")";
            if ((bool)Online)
            {
                Classes.ClsMain.ExecuteNoneQuery_remotely(Command + should_reload_data_in_client_command, clsBargeh.Client_connection_string
                    ,"deletelist",start_it, count
                    );
                

            }
            else
            {
                FolderBrowserDialog opendiag = new FolderBrowserDialog();
                if (opendiag.ShowDialog() == DialogResult.OK)
                {
                    if (opendiag.SelectedPath.Contains("Bargeh_khorooj") == true)
                    {
                        opendiag.SelectedPath = opendiag.SelectedPath.Replace("\\Bargeh_khorooj", "");
                    }
                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");

                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\deletelist") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\deletelist");

                    System.IO.File.WriteAllText(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\deletelist\\delete.sql", Command);
                    MessageBox.Show("پوشه حاوی اطلاعات مربوط به برگه های حذف شده جهت انتقال به نگهبانی با موفقیت ایجاد شد .");
                    System.Diagnostics.Process.Start(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");
                    Classes.clsBargeh.client_delete_deletelist(0);


                }
            }
            Classes.clsBargeh.DeletedList_started = false;

        }


        public static void Send_Moshtarekin_to_Client(object Online)
        {
            DateTime start_it = DateTime.Now;
            int count = 0;
            DataTable dt_moshtarekin = ClsMain.GetDataTable("select gharardad,co_name,emza from moshtarekin");
            if (dt_moshtarekin.Rows.Count == 0)
            {
                MessageBox.Show("اطلاعات مشترکین از سیستم حذف شده است . لطفا سریعا با پشتیبان تماس حاصل فرمایید");
                Classes.clsBargeh.moshtarekin_started = false;
                return;
            }
            string Command = "truncate table moshtarekin;";
            count = dt_moshtarekin.Rows.Count;
            for (int i = 0; i < dt_moshtarekin.Rows.Count; i++)
            {
                Command += "insert into moshtarekin (gharardad,co_name,emza) values (" + dt_moshtarekin.Rows[i]["gharardad"] + ",N'" + dt_moshtarekin.Rows[i]["co_name"] +
                    "','" + dt_moshtarekin.Rows[i]["emza"] + "');";

            }
            if ((bool)Online)
            {
                Classes.ClsMain.ExecuteNoneQuery(Command.Remove(Command.Length - 1) + should_reload_data_in_client_command, clsBargeh.Client_connection_string);
                MessageBox.Show("اطلاعات مشترکین با موفقیت در بانک اطلاعاتی نگهبانی ذخیره شد");
                Log_transfer_report("مشترکین", start_it, DateTime.Now, 1);
            }
            else
            {
                FolderBrowserDialog opendiag = new FolderBrowserDialog();
                if (opendiag.ShowDialog() == DialogResult.OK)
                {
                    if (opendiag.SelectedPath.Contains("Bargeh_khorooj") == true)
                    {
                        opendiag.SelectedPath = opendiag.SelectedPath.Replace("\\Bargeh_khorooj", "");
                    }
                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");

                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\moshtarekin") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\moshtarekin");

                    System.IO.File.WriteAllText(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\moshtarekin\\moshtarekin.sql", Command);
                    Payam.Show("پوشه حاوی اطلاعات مشترکین جهت انتقال به نگهبانی با موفقیت ایجاد شد .");
                    System.Diagnostics.Process.Start(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");


                }
            }
            Classes.clsBargeh.moshtarekin_started = false;
        }


        public static void Send_BlockList_to_Client(object Online)
        {
            DateTime start_it = DateTime.Now;
            DataTable dt_BlockList = ClsMain.GetDataTable("select bargeh_id,block from bargeh_client_blocklist");
            if (dt_BlockList.Rows.Count == 0)
            {
                MessageBox.Show("هیچ درخواستی جدیدی جهت انتقال  برگه های غیرفعال / فعال به سامانه نگهبانی وجود ندارد");
                Classes.clsBargeh.BlockList_started = false;
                return;
            }
            int count = dt_BlockList.Rows.Count;
            string block_Command = "update  barge_khorooj_details set block=1 where id in (";
            string unblock_Command = "update  barge_khorooj_details set block=0 where id in (";

            string Block_IDS = "", UnBlock_IDS = "";
            for (int i = 0; i < dt_BlockList.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dt_BlockList.Rows[i]["block"]))
                    Block_IDS += dt_BlockList.Rows[i]["bargeh_id"] + ",";
                else
                    UnBlock_IDS += dt_BlockList.Rows[i]["bargeh_id"] + ",";

            }

            if ((bool)Online)
            {
                if (Block_IDS.Length != 0)
                {
                    block_Command += Block_IDS.Remove(Block_IDS.Length - 1) + ")";
                    Classes.ClsMain.ExecuteNoneQuery_remotely(block_Command, clsBargeh.Client_connection_string
                        , "blocklist", start_it, count);
                }
                if (UnBlock_IDS.Length != 0)
                {
                    unblock_Command += UnBlock_IDS.Remove(UnBlock_IDS.Length - 1) + ")";
                    Classes.ClsMain.ExecuteNoneQuery_remotely(unblock_Command, clsBargeh.Client_connection_string
                        ,"blocklist",start_it, count);
                }

                Classes.ClsMain.ExecuteNoneQuery(should_reload_data_in_client_command.Substring(1));
             

            }
            else
            {
                FolderBrowserDialog opendiag = new FolderBrowserDialog();
                if (opendiag.ShowDialog() == DialogResult.OK)
                {
                    if (opendiag.SelectedPath.Contains("Bargeh_khorooj") == true)
                    {
                        opendiag.SelectedPath = opendiag.SelectedPath.Replace("\\Bargeh_khorooj","");
                    }
                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");

                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\blocklist") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\blocklist");

                    if (Block_IDS.Length != 0)
                    {
                        block_Command += Block_IDS.Remove(Block_IDS.Length - 1) + ")";
                        System.IO.File.WriteAllText(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\blocklist\\block.sql", block_Command);
                    }
                    if (UnBlock_IDS.Length != 0)
                    {
                        unblock_Command += UnBlock_IDS.Remove(UnBlock_IDS.Length - 1) + ")";
                        System.IO.File.WriteAllText(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\blocklist\\unblock.sql", unblock_Command);
                    }


                    Payam.Show("پوشه حاوی اطلاعات مربوط به برگه های فعال/غیرفعال جهت انتقال به نگهبانی با موفقیت ایجاد شد .");
                    System.Diagnostics.Process.Start(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");
                    Classes.clsBargeh.client_delete_blocklist(0);


                }
            }
            Classes.clsBargeh.BlockList_started = false;

        }




        public static void Send_AddList_to_Client(object Online)
        {
            DateTime start_it = DateTime.Now;
            DataTable dt_addList = ClsMain.GetDataTable("select * from bargeh_client_addlist");
            if (dt_addList.Rows.Count == 0)
            {
                MessageBox.Show("هیچ درخواستی جدیدی جهت انتقال به سامانه نگهبانی وجود ندارد");
                clsBargeh.AddList_started = false;
                return;
            }
            string Command = "";
            int count = 0;
            for (int i = 0; i < dt_addList.Rows.Count; i++)
            {
                Command += "exec Sodoor_barge_khorooj " + dt_addList.Rows[i]["start"] + "," + dt_addList.Rows[i]["finish"] +
                    "," + dt_addList.Rows[i]["darkhast_id"] + "," + dt_addList.Rows[i]["gharardad"] + ";";
                count += (Convert.ToInt32(dt_addList.Rows[i]["finish"]) - Convert.ToInt32(dt_addList.Rows[i]["start"]));
            }
            if ((bool)Online)
            {
                Classes.ClsMain.ExecuteNoneQuery_remotely(Command + should_reload_data_in_client_command, clsBargeh.Client_connection_string
                    , "addlist",start_it,count
                    );

            }
            else
            {
                FolderBrowserDialog opendiag = new FolderBrowserDialog();
                if (opendiag.ShowDialog() == DialogResult.OK)
                {
                    if (opendiag.SelectedPath.Contains("Bargeh_khorooj") == true)
                    {
                        opendiag.SelectedPath = opendiag.SelectedPath.Replace("\\Bargeh_khorooj", "");
                    }
                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");

                    if (Directory.Exists(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\addlist") == false)
                        Directory.CreateDirectory(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\addlist");

                    System.IO.File.WriteAllText(opendiag.SelectedPath + "\\" + "Bargeh_khorooj" + "\\addlist\\add.sql", Command);
                    Payam.Show("پوشه حاوی اطلاعات مربوط به برگه های جدید جهت انتقال به نگهبانی با موفقیت ایجاد شد .");
                    System.Diagnostics.Process.Start(opendiag.SelectedPath + "\\" + "Bargeh_khorooj");
                    Classes.clsBargeh.client_delete_addlist(0);


                }
            }
            clsBargeh.AddList_started = false;
        }
        public static bool Check_connection()
        {
            bool result = true;
            try
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(clsBargeh.Client_connection_string);
                con.Open();
                con.Close();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }



        public static void Get_Used_bargeh_from_Negahbani(object Online)
        {
            Classes.ClsMain.ChangeCulture("e");
            string max_used_date = ClsMain.ExecuteScalar("select isnull(max(used_date), ( select MIN(create_date) from barge_khorooj_details) ) from barge_khorooj_details").ToString();
            if ((bool)Online)
            {
                DataTable dt_used_bareh = Classes.ClsMain.GetDataTable("select id,used_date from barge_khorooj_details where used=1 and used_date>='" + max_used_date + "' ", clsBargeh.Client_connection_string);
                if (dt_used_bareh.Rows.Count == 0)
                {
                    MessageBox.Show("هیچ تغییر جدید در سامانه نگهبانی رخ نداده است . اطلاعات سرور اصلی با سامانه نگهبانی کاملا یکسان می باشد");
                }
                else
                {

                    for (int i = 0; i < dt_used_bareh.Rows.Count; i++)
                    {
                        Classes.ClsMain.ExecuteNoneQuery("update barge_khorooj_details set used=1,used_date='" +
                            dt_used_bareh.Rows[i]["used_date"].ToString() + "' where id=" + dt_used_bareh.Rows[i]["id"].ToString());

                    }
                    MessageBox.Show("کلیه اطلاعات مربوط به برگه های استفاده شده با موفقیت در سرور ذخیره شدند.");
                }
            }
            else
            {
                Payam.Show("امکان دریافت اطلاعات به صورت افلاین در این نسخه وجود ندارد");
            }
            AllChangedFromClient_started = false;

        }


    }

}
