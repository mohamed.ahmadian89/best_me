﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FarsiLibrary.Utils;

namespace CWMS.Classes
{
    class clsTax
    {
        public static void print_maliat(CWMS.ManagementReports.DaramadDataset.sharj_maliat_reportDataTable maliat_dt,string type)
        {
            CrysReports.ReportDataSet ds = ClsMain.getReportDatasetWithSetting();
            CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter ta1 = new CrysReports.ReportDataSetTableAdapters.sharj_ghabzTableAdapter();
            CrysReports.sharj_arzesh_afzoodeh report1 = new CrysReports.sharj_arzesh_afzoodeh();

            report1.SetDataSource(maliat_dt.CopyToDataTable());


            string tarikh = PersianDateConverter.ToPersianDate(DateTime.Now).ToString("d");
            report1.SetParameterValue("shahrak_name", Classes.ClsMain.ExecuteScalar("select top(1) shahrak_name from setting").ToString());
            report1.SetParameterValue("tarikh", tarikh);
            report1.SetParameterValue("type", type);

            new FrmShowReport(report1).ShowDialog();


        }
    }
}
