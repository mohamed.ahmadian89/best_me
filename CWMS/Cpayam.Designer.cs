﻿namespace CWMS
{
    partial class Cpayam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnYes = new DevComponents.DotNetBar.ButtonX();
            this.lblpayam = new DevComponents.DotNetBar.LabelX();
            this.btnNo = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // btnYes
            // 
            this.btnYes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnYes.BackColor = System.Drawing.Color.Transparent;
            this.btnYes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnYes.Location = new System.Drawing.Point(332, 71);
            this.btnYes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnYes.Name = "btnYes";
            this.btnYes.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnYes.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnYes.Size = new System.Drawing.Size(127, 24);
            this.btnYes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnYes.Symbol = "";
            this.btnYes.SymbolColor = System.Drawing.Color.Green;
            this.btnYes.SymbolSize = 12F;
            this.btnYes.TabIndex = 3;
            this.btnYes.Text = "تایید";
            this.btnYes.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // lblpayam
            // 
            this.lblpayam.BackColor = System.Drawing.Color.Azure;
            // 
            // 
            // 
            this.lblpayam.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblpayam.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblpayam.ForeColor = System.Drawing.Color.Black;
            this.lblpayam.Location = new System.Drawing.Point(25, 4);
            this.lblpayam.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblpayam.Name = "lblpayam";
            this.lblpayam.Size = new System.Drawing.Size(604, 59);
            this.lblpayam.TabIndex = 2;
            this.lblpayam.Text = "اطلاعات با موفقیت در سیستم ثبت شد";
            this.lblpayam.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblpayam.WordWrap = true;
            this.lblpayam.Click += new System.EventHandler(this.lblpayam_Click);
            // 
            // btnNo
            // 
            this.btnNo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNo.BackColor = System.Drawing.Color.Transparent;
            this.btnNo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnNo.Location = new System.Drawing.Point(199, 71);
            this.btnNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNo.Name = "btnNo";
            this.btnNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnNo.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btnNo.Size = new System.Drawing.Size(127, 24);
            this.btnNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnNo.Symbol = "";
            this.btnNo.SymbolColor = System.Drawing.Color.Maroon;
            this.btnNo.SymbolSize = 12F;
            this.btnNo.TabIndex = 4;
            this.btnNo.Text = "انصراف";
            // 
            // Cpayam
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 108);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.lblpayam);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Cpayam";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Cpayam_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnYes;
        private DevComponents.DotNetBar.LabelX lblpayam;
        private DevComponents.DotNetBar.ButtonX btnNo;
    }
}