﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Factor
{
    public partial class frm_factor_abbasAbad : MyMetroForm
    {

        DataTable dt_ab_doreh, dt_sharj_doreh;
        public frm_factor_abbasAbad()
        {
            InitializeComponent();
            dt_ab_doreh = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            dt_sharj_doreh = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");

        }
        string GetLast_Factor_number()
        {
            try
            {
                string sal = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(txt_factor_date.SelectedDateTime)).Year.ToString().Substring(2);
                string last = Classes.ClsMain.ExecuteScalar("select isnull(max(factor),0)+1 from factors where sal=" + sal).ToString();
                return last;
            }
            catch (Exception)
            {

                return "1";
            }

        }
        private void frm_factor_Load(object sender, EventArgs e)
        {
            txtfrom.Focus();
            txt_factor_date.SelectedDateTime = DateTime.Now;
            txt_maliat.Text = "9";
            txt_factor_num.Text = GetLast_Factor_number();
            DataTable dt_moshtarek = Classes.ClsMain.GetDataTable("select min(gharardad),max(gharardad) from moshtarekin");
            if (dt_moshtarek.Rows.Count > 0)
            {
                txtfrom.Text = dt_moshtarek.Rows[0][0].ToString();
                txtTo.Text = dt_moshtarek.Rows[0][1].ToString();
            }
            else
            {
                Payam.Show("به علت عدم وجود اطلاعات مشترکین در پایگاه داده ، سامانه قادر به صدور فاکتور نمی باشد");
            }
        }

        private void cmb_doreh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_doreh.Items.Count != 0)
            {
                txt_sharh.Text = "ارائه خدمات " + cmb_ghabz_type.Text + " دوره " + cmb_doreh.Text;
            }
        }


        void Make_factor()
        {
            // این نسخه در واقع دارای پس زمینه است که اطلاعات بر روی فاکتور پیش فرض نوشته می شود
            // در این نسخه فرض بر این است که کاربر اصلا بدهکاری ندارد و صرفا کتور بر مبنای هزینه های دوری جاری می باشد

            Classes.shahrak_classes.abbas_abad_factor factor_class = new Classes.shahrak_classes.abbas_abad_factor();

            try
            {
                factor_class.Factor_ID = Convert.ToInt32(txt_factor_num.Text);
            }
            catch (Exception)
            {
                Payam.Show("شماره فاکتور نامعتبر می باشد");
                return;

            }

            try
            {
                factor_class.start_gharardad = Convert.ToInt32(txtfrom.Text);

            }
            catch (Exception)
            {

                Payam.Show("شماره قرارداد نامعتبر می باشد.");
                txtfrom.Focus();
                txtfrom.SelectAll();
                return;
            }



            try
            {
                factor_class.end_gharardad = Convert.ToInt32(txtTo.Text);
            }
            catch (Exception)
            {
                Payam.Show("شماره قرارداد نامعتبر می باشد.");
                txtTo.Focus();
                txtTo.SelectAll();
                return;
            }



            try
            {
                factor_class.dcode = Convert.ToInt32(cmb_doreh.SelectedValue.ToString());
            }
            catch (Exception)
            {
                Payam.Show("دوره انتخاب شده نامعتبر می باشد. لطفا دوره مربوطه  را انتخاب نمایید");
                return;
            }


            try
            {
                factor_class.takhfif = Convert.ToDecimal(txt_takhfif.Text);
            }
            catch (Exception)
            {
                Payam.Show("لطفا مبلغ تخفیف  را به درستی وارد نمایید");
                txt_takhfif.Focus();
                txt_takhfif.SelectAll();
                return;
            }

            try
            {
                factor_class.tarikh = Convert.ToDateTime(txt_factor_date.SelectedDateTime);
            }
            catch (Exception)
            {
                Payam.Show("لطفا تاریخ صدور فاکتور  را به درستی وارد نمایید");
                txt_factor_date.Focus();
                return;
            }



            try
            {
                factor_class.darsad_maliat = Convert.ToDecimal(txt_maliat.Text);
            }
            catch (Exception)
            {
                Payam.Show("لطفا درصد مالیات  را به درستی وارد نمایید");
                txt_maliat.Focus();
                return;
            }



            //ghabz_tyep =0 آب   one:شارژ


       



            factor_class.table_name = "sharj_ghabz";
            factor_class.ghabz_type = 1;
            if (cmb_ghabz_type.SelectedIndex == 0)
            {
                factor_class.table_name = "ab_ghabz";
                factor_class.ghabz_type = 0;
            }
            factor_class.sharh = txt_sharh.Text;

            factor_class.get_All_Ghabz();

            sharj_progress.Minimum = 0;
            sharj_progress.Maximum = factor_class.dt_ghabz.Rows.Count;
            sharj_progress.Value = 0;

            Classes.ClsMain.ChangeCulture("e");
            //Classes.MultipleCommand Multiple = new Classes.MultipleCommand("Soodor_Factors");
            for (int i = 0; i < factor_class.dt_ghabz.Rows.Count; i++)
            {
                factor_class.Clear_all();
                factor_class.index = i;

                factor_class.init_factor_fields();
                factor_class.calc_Majmu_mabalegh();

                // صدور فاکتور صرفا برای قبض های دارای پرداختی 
                if (factor_class.Check_has_pardakhti() == false)
                    continue;
                factor_class.calc_Maliat();
                factor_class.calc_Mablghkol();
                factor_class.generate_Factor();
                //factor_class.Factor_generation_list();

                try
                {

                    sharj_progress.Value += 1;
                }
                catch (Exception)
                {
                    sharj_progress.Value = sharj_progress.Maximum;
                }

            }



            string toz = "";
            if (factor_class.error_gharardad.Count == 0)
            {
                toz = "صدور فاکتور برای تمامی " + factor_class.dt_ghabz.Rows.Count.ToString() + " قرارداد ";
            }
            else
            {
                toz = "عدم صدور فاکتور برای قراردادهای :";
                for (int i = 0; i < factor_class.error_gharardad.Count; i++)
                {
                    toz += factor_class.error_gharardad[i].ToString() + ",";
                }
            }

            Classes.ClsMain.ChangeCulture("e");
            Payam.Show("عملیات  صدور فاکتور با موفقیت به پایان رسید ");

            toz += " -- " + new FarsiLibrary.Utils.PersianDate(factor_class.tarikh).ToString("D");

            Classes.clsFactor.factor_history(factor_class.dcode.ToString(), cmb_ghabz_type.Text, toz, DateTime.Now, txtfrom.Text, txtTo.Text, factor_class.success, factor_class.fail);
            Classes.ClsMain.ChangeCulture("f");

            new Management.Frm_Sodoor_factor_Report(factor_class.dt_ghabz.Rows.Count.ToString(), factor_class.operation_count.ToString(), factor_class.error_gharardad).ShowDialog();
            sharj_progress.Value = sharj_progress.Maximum;

            

        }

        private void btn_create_factor_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("ایا با صدور فاکتور موافق هستید؟") == DialogResult.Yes)
            {
                //backgroundWorker1.RunWorkerAsync();
                Make_factor();
                txt_factor_num.Text = GetLast_Factor_number();
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
           
        }

        private void cmb_ghabz_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_ghabz_type.SelectedIndex == 0)
                cmb_doreh.DataSource = dt_ab_doreh;
            else
                cmb_doreh.DataSource = dt_ab_doreh;
            cmb_doreh.DisplayMember = "dcode";
            cmb_doreh.ValueMember = "dcode";
            if (cmb_doreh.Items.Count != 0)
            {
                txt_sharh.Text = "ارائه خدمات " + cmb_ghabz_type.Text + " دوره " + cmb_doreh.Text;
            }
        }
    }
}
