﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Factor
{
    public partial class Frm_chap_factor : MyMetroForm
    {
        public Frm_chap_factor()
        {
            InitializeComponent();
        }

        private void Frm_chap_factor_Load(object sender, EventArgs e)
        {
            cmb_sharj_dcode.DataSource = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");
            cmb_sharj_dcode.DisplayMember = "dcode";
            cmb_sharj_dcode.ValueMember = "dcode";

            cmb_ab_dcode.DataSource = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            cmb_ab_dcode.DisplayMember = "dcode";
            cmb_ab_dcode.ValueMember = "dcode";
            cmb_halat_grp_doreh.SelectedIndex = 0;
        }

        private void labelX2_Click(object sender, EventArgs e)
        {

        }

        private void ch_sharj_dcode_CheckedChanged(object sender, EventArgs e)
        {
            if (ch_sharj_dcode.Checked)
            {
                cmb_sharj_dcode.Visible = true;

            }
            else
            {
                cmb_sharj_dcode.Visible = false;
            }
        }

        private void ch_ab_dcode_CheckedChanged(object sender, EventArgs e)
        {
            if (ch_ab_dcode.Checked)
            {
                cmb_ab_dcode.Visible = true;

            }
            else
            {
                cmb_ab_dcode.Visible = false;
            }
        }

        void Chap_factor(bool Is_Negah)
        {

            DataTable dt_factors_sharj, dt_factors_ab;
            dt_factors_ab = dt_factors_sharj = new DataTable();
            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = new CrystalDecisions.CrystalReports.Engine.ReportClass();

            string has_pardakht = "";
            if (cmb_halat_grp_doreh.Text == "فاکتورهای پرداختی")
            {
                has_pardakht = " and has_pardakhti=1";
            }

            if (Is_Negah == false)
            {

            }
            else
            {

                bool First_init = false, Second_init = false;

                if (ch_sharj_dcode.Checked)
                {
                    dt_factors_sharj = Classes.ClsMain.GetDataTable(
                        "select * from factors where  ghabz_type=1 and dcode=" + cmb_sharj_dcode.Text + has_pardakht);
                    First_init = true;
                }

                if (ch_ab_dcode.Checked)
                {
                    dt_factors_ab = Classes.ClsMain.GetDataTable(
                        "select * from factors where  ghabz_type=0 and dcode=" + cmb_ab_dcode.Text + has_pardakht);
                    if (First_init == false)
                        First_init = true;
                    else
                        Second_init = true;
                }


                Fill_report(dt_factors_sharj, dt_factors_ab, true, report1, First_init, Second_init);

            }
        }

        private void btn_chap_negah_Click(object sender, EventArgs e)
        {
            Chap_factor(true);
        }

        void Fill_report(DataTable dt_info, DataTable dt_info_2, bool Is_negah_factor, CrystalDecisions.CrystalReports.Engine.ReportClass report1, bool First_init, bool Second_init)
        {
            List<string> Printed_Factor_from_dt_info_2 = new List<string>();
            DataView dv = new DataView(dt_info_2);
            CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();

            bool Factor_Is_done = false;

            if (First_init == false)
                return;

            for (int i = 0; i < dt_info.Rows.Count; i++)
            {
                Factor_Is_done = false;
                string gharardad = dt_info.Rows[i]["gharardad"].ToString();
                if (Second_init)
                {
                    dv.RowFilter = "gharardad=" + gharardad;
                    // این بدان معناست که کاربر علاوه بر فاکتور شارژ فاکتور آب هم دارد
                    if (dv.Count > 0)
                    {
                        Printed_Factor_from_dt_info_2.Add(gharardad.ToString());

                        if (Is_negah_factor)
                        {
                            report1 = new CrysReports.main_factor_negah_tupple();
                            report1.SetParameterValue("factor_number", dt_info.Rows[i]["factor"].ToString() + "-" + dv[0]["factor"].ToString());

                        }
                        else
                            report1 = new CrysReports.main_factor_tupple();



                        decimal majmu = Convert.ToDecimal(dt_info.Rows[i]["majmu_mabalegh"]);
                        decimal takhfif = Convert.ToDecimal(dt_info.Rows[i]["takhfif"]);
                        decimal maliat = Convert.ToDecimal(dt_info.Rows[i]["maliat"]);
                        decimal Mablahkol = Convert.ToDecimal(dt_info.Rows[i]["mablaghkol"]);

                        report1.SetParameterValue("tarikh",
                            new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_info.Rows[i]["tarikh"])).ToString("d")
                            + " - " +
                            new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dv[0]["tarikh"])).ToString("d")
                            );
                        report1.SetParameterValue("majmu_mabalegh", majmu);
                        report1.SetParameterValue("takhfif", takhfif);
                        report1.SetParameterValue("majmu_mabalegh_takhfif", majmu - takhfif);
                        report1.SetParameterValue("maliat", maliat);
                        report1.SetParameterValue("mablaghkol", Mablahkol);
                        report1.SetParameterValue("sharh", dt_info.Rows[i]["sharh"].ToString());
                        report1.SetParameterValue("doreh", dt_info.Rows[i]["dcode"].ToString());

                        decimal majmu_1 = Convert.ToDecimal(dv[0]["majmu_mabalegh"]);
                        decimal takhfif_1 = Convert.ToDecimal(dv[0]["takhfif"]);
                        decimal maliat_1 = Convert.ToDecimal(dv[0]["maliat"]);
                        decimal Mablahkol_1 = Convert.ToDecimal(dv[0]["mablaghkol"]);


                        report1.SetParameterValue("majmu_mabalegh_1", majmu_1);
                        report1.SetParameterValue("takhfif_1", takhfif_1);
                        report1.SetParameterValue("majmu_mabalegh_takhfif_1", majmu_1 - takhfif_1);
                        report1.SetParameterValue("maliat_1", maliat_1);
                        report1.SetParameterValue("mablaghkol_1", Mablahkol_1);
                        report1.SetParameterValue("sharh_1", dv[0]["sharh"].ToString());
                        report1.SetParameterValue("doreh_1", dv[0]["dcode"].ToString());

                        report1.SetParameterValue("majmu_mabalegh_2", majmu_1 + majmu);
                        report1.SetParameterValue("takhfif_2", takhfif_1 + takhfif);
                        report1.SetParameterValue("majmu_mabalegh_takhfif_2", majmu_1 - takhfif_1 + majmu - takhfif);
                        report1.SetParameterValue("maliat_2", maliat_1 + maliat);

                        Decimal mablaghkol_2 = Mablahkol_1 + Mablahkol;
                        decimal kasr_hezar = (mablaghkol_2 % 1000);
                        report1.SetParameterValue("mablaghkol_2", mablaghkol_2 - kasr_hezar);
                        report1.SetParameterValue("kasr_hezar", kasr_hezar);
                        report1.SetParameterValue("mablaghkol_horoof", Classes.clsNumber.Number_to_Str((mablaghkol_2 - kasr_hezar).ToString()).ToString() + "  ریال ");

                        Factor_Is_done = true;

                    }
                }

                if(Factor_Is_done==false)
                {
                    if (Is_negah_factor)
                    {
                        report1 = new CrysReports.main_factor_negah();
                        report1.SetParameterValue("factor_number", dt_info.Rows[i]["factor"]);

                    }
                    else
                        report1 = new CrysReports.main_factor();


                    decimal majmu = Convert.ToDecimal(dt_info.Rows[i]["majmu_mabalegh"]);
                    decimal takhfif = Convert.ToDecimal(dt_info.Rows[i]["takhfif"]);
                    decimal maliat = Convert.ToDecimal(dt_info.Rows[i]["maliat"]);
                    decimal Mablahkol = Convert.ToDecimal(dt_info.Rows[i]["mablaghkol"]);
                    report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_info.Rows[i]["tarikh"])).ToString("d"));

                    report1.SetParameterValue("majmu_mabalegh", majmu);
                    report1.SetParameterValue("takhfif", takhfif);
                    report1.SetParameterValue("majmu_mabalegh_takhfif", majmu - takhfif);
                    report1.SetParameterValue("maliat", maliat);
                    report1.SetParameterValue("mablaghkol", Mablahkol);
                    report1.SetParameterValue("sharh", dt_info.Rows[i]["sharh"].ToString());
                    report1.SetParameterValue("doreh", dt_info.Rows[i]["dcode"].ToString());


                    decimal kasr_hezar = (Mablahkol % 1000);
                    report1.SetParameterValue("mablaghkol", Mablahkol - kasr_hezar);
                    report1.SetParameterValue("kasr_hezar", kasr_hezar);
                    report1.SetParameterValue("mablaghkol_horoof", Classes.clsNumber.Number_to_Str((Mablahkol - kasr_hezar).ToString()).ToString() + "  ریال ");
                    Factor_Is_done = true;

                }


                DataTable dt_moshtarek_info = Classes.ClsMain.MoshtarekDT.Where(p => p.gharardad == Convert.ToInt32(gharardad)).CopyToDataTable();
                report1.SetParameterValue("gharardad", gharardad);
                report1.SetParameterValue("m_co_name", dt_moshtarek_info.Rows[0]["co_name"].ToString());
                report1.SetParameterValue("m_shomare_eghtesadi", dt_moshtarek_info.Rows[0]["code_eghtesadi"].ToString());
                report1.SetParameterValue("m_shenase_melli", dt_moshtarek_info.Rows[0]["code_melli"].ToString());
                report1.SetParameterValue("m_code_posti", dt_moshtarek_info.Rows[0]["code_posti"].ToString());
                report1.SetParameterValue("m_address", dt_moshtarek_info.Rows[0]["address"].ToString());
                report1.SetParameterValue("m_phone", dt_moshtarek_info.Rows[0]["phone"].ToString());


                crystalReportViewer1.ReportSource = report1;
                crystalReportViewer1.Refresh();
                crystalReportViewer1.PrintReport();

            }

            //از این قسمت به بعد ، کلیه قرارداد های جدول دوم که در جدول اول یافت نشدند به صورت تکی چاپ خواهند شد

            for (int i = 0; i < dt_info_2.Rows.Count; i++)
            {
                string gharardad = dt_info_2.Rows[i]["gharardad"].ToString();
                if (Printed_Factor_from_dt_info_2.Contains(gharardad))
                    continue;
                Printed_Factor_from_dt_info_2.Add(gharardad);

                if (Is_negah_factor)
                {
                    report1 = new CrysReports.main_factor_negah();
                    report1.SetParameterValue("factor_number", dt_info_2.Rows[i]["factor"]);

                }
                else
                    report1 = new CrysReports.main_factor();


                decimal majmu = Convert.ToDecimal(dt_info_2.Rows[i]["majmu_mabalegh"]);
                decimal takhfif = Convert.ToDecimal(dt_info_2.Rows[i]["takhfif"]);
                decimal maliat = Convert.ToDecimal(dt_info_2.Rows[i]["maliat"]);
                decimal Mablahkol = Convert.ToDecimal(dt_info_2.Rows[i]["mablaghkol"]);


                report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt_info_2.Rows[i]["tarikh"])).ToString("d"));
                report1.SetParameterValue("majmu_mabalegh", majmu);
                report1.SetParameterValue("takhfif", takhfif);
                report1.SetParameterValue("majmu_mabalegh_takhfif", majmu - takhfif);
                report1.SetParameterValue("maliat", maliat);
                report1.SetParameterValue("mablaghkol", Mablahkol);
                report1.SetParameterValue("sharh", dt_info_2.Rows[i]["sharh"].ToString());
                report1.SetParameterValue("doreh", dt_info_2.Rows[i]["dcode"].ToString());


                decimal kasr_hezar = (Mablahkol % 1000);
                report1.SetParameterValue("mablaghkol", Mablahkol - kasr_hezar);
                report1.SetParameterValue("kasr_hezar", kasr_hezar);
                report1.SetParameterValue("mablaghkol_horoof", Classes.clsNumber.Number_to_Str((Mablahkol - kasr_hezar).ToString()).ToString() + "  ریال ");




                DataTable dt_moshtarek_info = Classes.ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + gharardad);
                report1.SetParameterValue("gharardad", gharardad);
                report1.SetParameterValue("m_co_name", dt_moshtarek_info.Rows[0]["co_name"].ToString());
                report1.SetParameterValue("m_shomare_eghtesadi", dt_moshtarek_info.Rows[0]["code_eghtesadi"].ToString());
                report1.SetParameterValue("m_shenase_melli", dt_moshtarek_info.Rows[0]["code_melli"].ToString());
                report1.SetParameterValue("m_code_posti", dt_moshtarek_info.Rows[0]["code_posti"].ToString());
                report1.SetParameterValue("m_address", dt_moshtarek_info.Rows[0]["address"].ToString());
                report1.SetParameterValue("m_phone", dt_moshtarek_info.Rows[0]["phone"].ToString());


                crystalReportViewer1.ReportSource = report1;
                crystalReportViewer1.Refresh();
                crystalReportViewer1.PrintReport();


            }

        }

        private void btn_chap_darayi_Click(object sender, EventArgs e)
        {
            Chap_factor(false);
        }
    }








}
