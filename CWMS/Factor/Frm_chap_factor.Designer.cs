﻿namespace CWMS.Factor
{
    partial class Frm_chap_factor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_tarikh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txt_start_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cmb_hahat_grp_tarikh = new System.Windows.Forms.ComboBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btn_chap_negah_tarikh = new DevComponents.DotNetBar.ButtonX();
            this.btb_chap_darayi_tarikh = new DevComponents.DotNetBar.ButtonX();
            this.cmb_factor_type = new System.Windows.Forms.ComboBox();
            this.grp_doreh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cmb_halat_grp_doreh = new System.Windows.Forms.ComboBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.ch_ab_dcode = new System.Windows.Forms.CheckBox();
            this.cmb_ab_dcode = new System.Windows.Forms.ComboBox();
            this.ch_sharj_dcode = new System.Windows.Forms.CheckBox();
            this.btn_chap_negah = new DevComponents.DotNetBar.ButtonX();
            this.btn_chap_darayi = new DevComponents.DotNetBar.ButtonX();
            this.cmb_sharj_dcode = new System.Windows.Forms.ComboBox();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.grp_tarikh.SuspendLayout();
            this.grp_doreh.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_tarikh
            // 
            this.grp_tarikh.BackColor = System.Drawing.Color.White;
            this.grp_tarikh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_tarikh.Controls.Add(this.txt_end_date);
            this.grp_tarikh.Controls.Add(this.labelX3);
            this.grp_tarikh.Controls.Add(this.txt_start_date);
            this.grp_tarikh.Controls.Add(this.labelX6);
            this.grp_tarikh.Controls.Add(this.cmb_hahat_grp_tarikh);
            this.grp_tarikh.Controls.Add(this.labelX5);
            this.grp_tarikh.Controls.Add(this.labelX2);
            this.grp_tarikh.Controls.Add(this.btn_chap_negah_tarikh);
            this.grp_tarikh.Controls.Add(this.btb_chap_darayi_tarikh);
            this.grp_tarikh.Controls.Add(this.cmb_factor_type);
            this.grp_tarikh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_tarikh.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grp_tarikh.Location = new System.Drawing.Point(22, 247);
            this.grp_tarikh.Margin = new System.Windows.Forms.Padding(4);
            this.grp_tarikh.Name = "grp_tarikh";
            this.grp_tarikh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_tarikh.Size = new System.Drawing.Size(665, 208);
            // 
            // 
            // 
            this.grp_tarikh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_tarikh.Style.BackColorGradientAngle = 90;
            this.grp_tarikh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_tarikh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_tarikh.Style.BorderBottomWidth = 1;
            this.grp_tarikh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_tarikh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_tarikh.Style.BorderLeftWidth = 1;
            this.grp_tarikh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_tarikh.Style.BorderRightWidth = 1;
            this.grp_tarikh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_tarikh.Style.BorderTopWidth = 1;
            this.grp_tarikh.Style.CornerDiameter = 4;
            this.grp_tarikh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_tarikh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_tarikh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_tarikh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_tarikh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_tarikh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_tarikh.TabIndex = 172;
            this.grp_tarikh.Text = "چاپ فاکتور بر اساس بازه زمانی";
            // 
            // txt_end_date
            // 
            this.txt_end_date.Location = new System.Drawing.Point(112, 22);
            this.txt_end_date.Name = "txt_end_date";
            this.txt_end_date.Size = new System.Drawing.Size(148, 24);
            this.txt_end_date.TabIndex = 218;
            this.txt_end_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(230, 18);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(89, 32);
            this.labelX3.TabIndex = 219;
            this.labelX3.Text = "تا تاریخ :";
            // 
            // txt_start_date
            // 
            this.txt_start_date.Location = new System.Drawing.Point(348, 22);
            this.txt_start_date.Name = "txt_start_date";
            this.txt_start_date.Size = new System.Drawing.Size(121, 24);
            this.txt_start_date.TabIndex = 216;
            this.txt_start_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(466, 18);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(119, 32);
            this.labelX6.TabIndex = 217;
            this.labelX6.Text = "از تاریخ صدور فاکتور :";
            // 
            // cmb_hahat_grp_tarikh
            // 
            this.cmb_hahat_grp_tarikh.BackColor = System.Drawing.Color.White;
            this.cmb_hahat_grp_tarikh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_hahat_grp_tarikh.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_hahat_grp_tarikh.ForeColor = System.Drawing.Color.Black;
            this.cmb_hahat_grp_tarikh.FormattingEnabled = true;
            this.cmb_hahat_grp_tarikh.Items.AddRange(new object[] {
            "فاکتورهای پرداختی",
            "کلیه فاکتورها",
            ""});
            this.cmb_hahat_grp_tarikh.Location = new System.Drawing.Point(128, 75);
            this.cmb_hahat_grp_tarikh.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_hahat_grp_tarikh.Name = "cmb_hahat_grp_tarikh";
            this.cmb_hahat_grp_tarikh.Size = new System.Drawing.Size(132, 31);
            this.cmb_hahat_grp_tarikh.TabIndex = 215;
            this.cmb_hahat_grp_tarikh.Visible = false;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(244, 60);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(92, 58);
            this.labelX5.TabIndex = 214;
            this.labelX5.Text = "حالت فاکتور :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(474, 60);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(61, 58);
            this.labelX2.TabIndex = 212;
            this.labelX2.Text = "نوع فاکتور :";
            this.labelX2.Click += new System.EventHandler(this.labelX2_Click);
            // 
            // btn_chap_negah_tarikh
            // 
            this.btn_chap_negah_tarikh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_chap_negah_tarikh.BackColor = System.Drawing.Color.Transparent;
            this.btn_chap_negah_tarikh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_chap_negah_tarikh.Location = new System.Drawing.Point(13, 123);
            this.btn_chap_negah_tarikh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_chap_negah_tarikh.Name = "btn_chap_negah_tarikh";
            this.btn_chap_negah_tarikh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_chap_negah_tarikh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_chap_negah_tarikh.Size = new System.Drawing.Size(165, 45);
            this.btn_chap_negah_tarikh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_chap_negah_tarikh.Symbol = "";
            this.btn_chap_negah_tarikh.SymbolColor = System.Drawing.Color.Green;
            this.btn_chap_negah_tarikh.SymbolSize = 12F;
            this.btn_chap_negah_tarikh.TabIndex = 208;
            this.btn_chap_negah_tarikh.Text = "چاپ فاکتور ( نگاه )";
            // 
            // btb_chap_darayi_tarikh
            // 
            this.btb_chap_darayi_tarikh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btb_chap_darayi_tarikh.BackColor = System.Drawing.Color.Transparent;
            this.btb_chap_darayi_tarikh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btb_chap_darayi_tarikh.Location = new System.Drawing.Point(214, 123);
            this.btb_chap_darayi_tarikh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btb_chap_darayi_tarikh.Name = "btb_chap_darayi_tarikh";
            this.btb_chap_darayi_tarikh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btb_chap_darayi_tarikh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btb_chap_darayi_tarikh.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btb_chap_darayi_tarikh.Size = new System.Drawing.Size(155, 45);
            this.btb_chap_darayi_tarikh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btb_chap_darayi_tarikh.Symbol = "";
            this.btb_chap_darayi_tarikh.SymbolColor = System.Drawing.Color.Green;
            this.btb_chap_darayi_tarikh.SymbolSize = 12F;
            this.btb_chap_darayi_tarikh.TabIndex = 207;
            this.btb_chap_darayi_tarikh.Text = "چاپ فاکتور ( دارایی )";
            // 
            // cmb_factor_type
            // 
            this.cmb_factor_type.BackColor = System.Drawing.Color.White;
            this.cmb_factor_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_factor_type.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_factor_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_factor_type.FormattingEnabled = true;
            this.cmb_factor_type.Items.AddRange(new object[] {
            "آب",
            "شارژ",
            "هردو"});
            this.cmb_factor_type.Location = new System.Drawing.Point(357, 75);
            this.cmb_factor_type.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_factor_type.Name = "cmb_factor_type";
            this.cmb_factor_type.Size = new System.Drawing.Size(109, 31);
            this.cmb_factor_type.TabIndex = 206;
            this.cmb_factor_type.Visible = false;
            // 
            // grp_doreh
            // 
            this.grp_doreh.BackColor = System.Drawing.Color.White;
            this.grp_doreh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_doreh.Controls.Add(this.cmb_halat_grp_doreh);
            this.grp_doreh.Controls.Add(this.labelX1);
            this.grp_doreh.Controls.Add(this.ch_ab_dcode);
            this.grp_doreh.Controls.Add(this.cmb_ab_dcode);
            this.grp_doreh.Controls.Add(this.ch_sharj_dcode);
            this.grp_doreh.Controls.Add(this.btn_chap_negah);
            this.grp_doreh.Controls.Add(this.btn_chap_darayi);
            this.grp_doreh.Controls.Add(this.cmb_sharj_dcode);
            this.grp_doreh.Controls.Add(this.labelX4);
            this.grp_doreh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_doreh.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grp_doreh.Location = new System.Drawing.Point(22, 13);
            this.grp_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.grp_doreh.Name = "grp_doreh";
            this.grp_doreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_doreh.Size = new System.Drawing.Size(665, 208);
            // 
            // 
            // 
            this.grp_doreh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_doreh.Style.BackColorGradientAngle = 90;
            this.grp_doreh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_doreh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderBottomWidth = 1;
            this.grp_doreh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_doreh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderLeftWidth = 1;
            this.grp_doreh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderRightWidth = 1;
            this.grp_doreh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh.Style.BorderTopWidth = 1;
            this.grp_doreh.Style.CornerDiameter = 4;
            this.grp_doreh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_doreh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_doreh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_doreh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_doreh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_doreh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_doreh.TabIndex = 171;
            this.grp_doreh.Text = "چاپ فاکتور بر اساس دوره";
            // 
            // cmb_halat_grp_doreh
            // 
            this.cmb_halat_grp_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_halat_grp_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_halat_grp_doreh.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_halat_grp_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_halat_grp_doreh.FormattingEnabled = true;
            this.cmb_halat_grp_doreh.Items.AddRange(new object[] {
            "فاکتورهای پرداختی",
            "کلیه فاکتورها"});
            this.cmb_halat_grp_doreh.Location = new System.Drawing.Point(406, 129);
            this.cmb_halat_grp_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_halat_grp_doreh.Name = "cmb_halat_grp_doreh";
            this.cmb_halat_grp_doreh.Size = new System.Drawing.Size(132, 31);
            this.cmb_halat_grp_doreh.TabIndex = 213;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(529, 114);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(92, 58);
            this.labelX1.TabIndex = 212;
            this.labelX1.Text = "حالت فاکتور :";
            // 
            // ch_ab_dcode
            // 
            this.ch_ab_dcode.AutoSize = true;
            this.ch_ab_dcode.BackColor = System.Drawing.Color.Transparent;
            this.ch_ab_dcode.Font = new System.Drawing.Font("B Yekan", 11F);
            this.ch_ab_dcode.ForeColor = System.Drawing.Color.Black;
            this.ch_ab_dcode.Location = new System.Drawing.Point(157, 75);
            this.ch_ab_dcode.Name = "ch_ab_dcode";
            this.ch_ab_dcode.Size = new System.Drawing.Size(162, 27);
            this.ch_ab_dcode.TabIndex = 211;
            this.ch_ab_dcode.Text = "چاپ فاکتورهای آب دوره ";
            this.ch_ab_dcode.UseVisualStyleBackColor = false;
            this.ch_ab_dcode.CheckedChanged += new System.EventHandler(this.ch_ab_dcode_CheckedChanged);
            // 
            // cmb_ab_dcode
            // 
            this.cmb_ab_dcode.BackColor = System.Drawing.Color.White;
            this.cmb_ab_dcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ab_dcode.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_ab_dcode.ForeColor = System.Drawing.Color.Black;
            this.cmb_ab_dcode.FormattingEnabled = true;
            this.cmb_ab_dcode.Location = new System.Drawing.Point(49, 73);
            this.cmb_ab_dcode.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_ab_dcode.Name = "cmb_ab_dcode";
            this.cmb_ab_dcode.Size = new System.Drawing.Size(65, 31);
            this.cmb_ab_dcode.TabIndex = 210;
            this.cmb_ab_dcode.Visible = false;
            // 
            // ch_sharj_dcode
            // 
            this.ch_sharj_dcode.AutoSize = true;
            this.ch_sharj_dcode.BackColor = System.Drawing.Color.Transparent;
            this.ch_sharj_dcode.Font = new System.Drawing.Font("B Yekan", 11F);
            this.ch_sharj_dcode.ForeColor = System.Drawing.Color.Black;
            this.ch_sharj_dcode.Location = new System.Drawing.Point(445, 77);
            this.ch_sharj_dcode.Name = "ch_sharj_dcode";
            this.ch_sharj_dcode.Size = new System.Drawing.Size(172, 27);
            this.ch_sharj_dcode.TabIndex = 209;
            this.ch_sharj_dcode.Text = "چاپ فاکتورهای شارژ دوره ";
            this.ch_sharj_dcode.UseVisualStyleBackColor = false;
            this.ch_sharj_dcode.CheckedChanged += new System.EventHandler(this.ch_sharj_dcode_CheckedChanged);
            // 
            // btn_chap_negah
            // 
            this.btn_chap_negah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_chap_negah.BackColor = System.Drawing.Color.Transparent;
            this.btn_chap_negah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_chap_negah.Location = new System.Drawing.Point(13, 124);
            this.btn_chap_negah.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_chap_negah.Name = "btn_chap_negah";
            this.btn_chap_negah.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_chap_negah.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_chap_negah.Size = new System.Drawing.Size(165, 45);
            this.btn_chap_negah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_chap_negah.Symbol = "";
            this.btn_chap_negah.SymbolColor = System.Drawing.Color.Green;
            this.btn_chap_negah.SymbolSize = 12F;
            this.btn_chap_negah.TabIndex = 208;
            this.btn_chap_negah.Text = "چاپ فاکتور ( نگاه )";
            this.btn_chap_negah.Click += new System.EventHandler(this.btn_chap_negah_Click);
            // 
            // btn_chap_darayi
            // 
            this.btn_chap_darayi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_chap_darayi.BackColor = System.Drawing.Color.Transparent;
            this.btn_chap_darayi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_chap_darayi.Location = new System.Drawing.Point(214, 124);
            this.btn_chap_darayi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_chap_darayi.Name = "btn_chap_darayi";
            this.btn_chap_darayi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_chap_darayi.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_chap_darayi.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btn_chap_darayi.Size = new System.Drawing.Size(155, 45);
            this.btn_chap_darayi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_chap_darayi.Symbol = "";
            this.btn_chap_darayi.SymbolColor = System.Drawing.Color.Green;
            this.btn_chap_darayi.SymbolSize = 12F;
            this.btn_chap_darayi.TabIndex = 207;
            this.btn_chap_darayi.Text = "چاپ فاکتور ( دارایی )";
            this.btn_chap_darayi.Click += new System.EventHandler(this.btn_chap_darayi_Click);
            // 
            // cmb_sharj_dcode
            // 
            this.cmb_sharj_dcode.BackColor = System.Drawing.Color.White;
            this.cmb_sharj_dcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sharj_dcode.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_sharj_dcode.ForeColor = System.Drawing.Color.Black;
            this.cmb_sharj_dcode.FormattingEnabled = true;
            this.cmb_sharj_dcode.Location = new System.Drawing.Point(357, 73);
            this.cmb_sharj_dcode.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_sharj_dcode.Name = "cmb_sharj_dcode";
            this.cmb_sharj_dcode.Size = new System.Drawing.Size(65, 31);
            this.cmb_sharj_dcode.TabIndex = 206;
            this.cmb_sharj_dcode.Visible = false;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(29, 20);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(614, 58);
            this.labelX4.TabIndex = 176;
            this.labelX4.Text = "کاربر گرامی ، شما می توانید با زدن علامت تایید هریک از بخش های شارژ یا اب ، نسبت " +
    "به چاپ فاکتورهای مربوطه اقدام نمایید\r\n";
            // 
            // Frm_chap_factor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 235);
            this.Controls.Add(this.grp_tarikh);
            this.Controls.Add(this.grp_doreh);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Frm_chap_factor";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سامانه چاپ فاکتور";
            this.Load += new System.EventHandler(this.Frm_chap_factor_Load);
            this.grp_tarikh.ResumeLayout(false);
            this.grp_doreh.ResumeLayout(false);
            this.grp_doreh.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_doreh;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btn_chap_negah;
        private DevComponents.DotNetBar.ButtonX btn_chap_darayi;
        private System.Windows.Forms.ComboBox cmb_sharj_dcode;
        private System.Windows.Forms.ComboBox cmb_halat_grp_doreh;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.CheckBox ch_ab_dcode;
        private System.Windows.Forms.ComboBox cmb_ab_dcode;
        private System.Windows.Forms.CheckBox ch_sharj_dcode;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_tarikh;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btn_chap_negah_tarikh;
        private DevComponents.DotNetBar.ButtonX btb_chap_darayi_tarikh;
        private System.Windows.Forms.ComboBox cmb_factor_type;
        private System.Windows.Forms.ComboBox cmb_hahat_grp_tarikh;
        private DevComponents.DotNetBar.LabelX labelX5;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end_date;
        private DevComponents.DotNetBar.LabelX labelX3;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start_date;
        private DevComponents.DotNetBar.LabelX labelX6;
    }
}