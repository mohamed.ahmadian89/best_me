﻿namespace CWMS.Factor
{
    partial class frm_factor_report_Amari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_end_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_start_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_sum_pardakhti_ab = new DevComponents.DotNetBar.LabelX();
            this.txt_count_pardakhti_ab = new DevComponents.DotNetBar.LabelX();
            this.txt_sum_all_ab = new DevComponents.DotNetBar.LabelX();
            this.txt_count_all_ab = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_sum_pardakhti_sharj = new DevComponents.DotNetBar.LabelX();
            this.txt_count_pardakhti_sharj = new DevComponents.DotNetBar.LabelX();
            this.txt_sum_all_sharj = new DevComponents.DotNetBar.LabelX();
            this.txt_count_all_sharj = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_end_date);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txt_start_date);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.buttonX8);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(14, 17);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(957, 84);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 67;
            // 
            // txt_end_date
            // 
            this.txt_end_date.Location = new System.Drawing.Point(404, 26);
            this.txt_end_date.Name = "txt_end_date";
            this.txt_end_date.Size = new System.Drawing.Size(131, 24);
            this.txt_end_date.TabIndex = 76;
            this.txt_end_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(505, 22);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(89, 32);
            this.labelX1.TabIndex = 77;
            this.labelX1.Text = "تا تاریخ :";
            // 
            // txt_start_date
            // 
            this.txt_start_date.Location = new System.Drawing.Point(602, 26);
            this.txt_start_date.Name = "txt_start_date";
            this.txt_start_date.Size = new System.Drawing.Size(104, 24);
            this.txt_start_date.TabIndex = 74;
            this.txt_start_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(703, 22);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(119, 32);
            this.labelX3.TabIndex = 75;
            this.labelX3.Text = "از تاریخ صدور فاکتور :";
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Transparent;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Location = new System.Drawing.Point(123, 17);
            this.buttonX8.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX8.Size = new System.Drawing.Size(241, 42);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.Green;
            this.buttonX8.SymbolSize = 9F;
            this.buttonX8.TabIndex = 73;
            this.buttonX8.Text = "نمایش اطلاعات";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_sum_pardakhti_ab);
            this.groupPanel2.Controls.Add(this.txt_count_pardakhti_ab);
            this.groupPanel2.Controls.Add(this.txt_sum_all_ab);
            this.groupPanel2.Controls.Add(this.txt_count_all_ab);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.labelX4);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(520, 113);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(451, 343);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 68;
            this.groupPanel2.Text = "قبوض آب";
            // 
            // txt_sum_pardakhti_ab
            // 
            this.txt_sum_pardakhti_ab.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_sum_pardakhti_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sum_pardakhti_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_sum_pardakhti_ab.Location = new System.Drawing.Point(20, 123);
            this.txt_sum_pardakhti_ab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_sum_pardakhti_ab.Name = "txt_sum_pardakhti_ab";
            this.txt_sum_pardakhti_ab.Size = new System.Drawing.Size(154, 32);
            this.txt_sum_pardakhti_ab.TabIndex = 86;
            this.txt_sum_pardakhti_ab.Text = "0";
            // 
            // txt_count_pardakhti_ab
            // 
            this.txt_count_pardakhti_ab.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_count_pardakhti_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_count_pardakhti_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_count_pardakhti_ab.Location = new System.Drawing.Point(20, 92);
            this.txt_count_pardakhti_ab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_count_pardakhti_ab.Name = "txt_count_pardakhti_ab";
            this.txt_count_pardakhti_ab.Size = new System.Drawing.Size(154, 32);
            this.txt_count_pardakhti_ab.TabIndex = 85;
            this.txt_count_pardakhti_ab.Text = "0";
            // 
            // txt_sum_all_ab
            // 
            this.txt_sum_all_ab.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_sum_all_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sum_all_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_sum_all_ab.Location = new System.Drawing.Point(20, 36);
            this.txt_sum_all_ab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_sum_all_ab.Name = "txt_sum_all_ab";
            this.txt_sum_all_ab.Size = new System.Drawing.Size(154, 32);
            this.txt_sum_all_ab.TabIndex = 84;
            this.txt_sum_all_ab.Text = "0";
            // 
            // txt_count_all_ab
            // 
            this.txt_count_all_ab.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_count_all_ab.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_count_all_ab.ForeColor = System.Drawing.Color.Black;
            this.txt_count_all_ab.Location = new System.Drawing.Point(20, 5);
            this.txt_count_all_ab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_count_all_ab.Name = "txt_count_all_ab";
            this.txt_count_all_ab.Size = new System.Drawing.Size(154, 32);
            this.txt_count_all_ab.TabIndex = 83;
            this.txt_count_all_ab.Text = "0";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(239, 123);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(176, 32);
            this.labelX5.TabIndex = 82;
            this.labelX5.Text = "مجموع کل فاکتورهای پرداختی :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(257, 92);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(158, 32);
            this.labelX6.TabIndex = 81;
            this.labelX6.Text = "تعداد کل فاکتورهای پرداختی :";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(296, 36);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(119, 32);
            this.labelX4.TabIndex = 80;
            this.labelX4.Text = "مجموع کل فاکتورها :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(296, 5);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(119, 32);
            this.labelX2.TabIndex = 79;
            this.labelX2.Text = "تعداد کل فاکتورها:";
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.txt_sum_pardakhti_sharj);
            this.groupPanel3.Controls.Add(this.txt_count_pardakhti_sharj);
            this.groupPanel3.Controls.Add(this.txt_sum_all_sharj);
            this.groupPanel3.Controls.Add(this.txt_count_all_sharj);
            this.groupPanel3.Controls.Add(this.labelX11);
            this.groupPanel3.Controls.Add(this.labelX12);
            this.groupPanel3.Controls.Add(this.labelX13);
            this.groupPanel3.Controls.Add(this.labelX14);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(14, 127);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(451, 343);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 69;
            this.groupPanel3.Text = "قبوض شارژ";
            // 
            // txt_sum_pardakhti_sharj
            // 
            this.txt_sum_pardakhti_sharj.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_sum_pardakhti_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sum_pardakhti_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_sum_pardakhti_sharj.Location = new System.Drawing.Point(20, 123);
            this.txt_sum_pardakhti_sharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_sum_pardakhti_sharj.Name = "txt_sum_pardakhti_sharj";
            this.txt_sum_pardakhti_sharj.Size = new System.Drawing.Size(154, 32);
            this.txt_sum_pardakhti_sharj.TabIndex = 86;
            this.txt_sum_pardakhti_sharj.Text = "0";
            // 
            // txt_count_pardakhti_sharj
            // 
            this.txt_count_pardakhti_sharj.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_count_pardakhti_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_count_pardakhti_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_count_pardakhti_sharj.Location = new System.Drawing.Point(20, 92);
            this.txt_count_pardakhti_sharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_count_pardakhti_sharj.Name = "txt_count_pardakhti_sharj";
            this.txt_count_pardakhti_sharj.Size = new System.Drawing.Size(154, 32);
            this.txt_count_pardakhti_sharj.TabIndex = 85;
            this.txt_count_pardakhti_sharj.Text = "0";
            // 
            // txt_sum_all_sharj
            // 
            this.txt_sum_all_sharj.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_sum_all_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sum_all_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_sum_all_sharj.Location = new System.Drawing.Point(20, 36);
            this.txt_sum_all_sharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_sum_all_sharj.Name = "txt_sum_all_sharj";
            this.txt_sum_all_sharj.Size = new System.Drawing.Size(154, 32);
            this.txt_sum_all_sharj.TabIndex = 84;
            this.txt_sum_all_sharj.Text = "0";
            // 
            // txt_count_all_sharj
            // 
            this.txt_count_all_sharj.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.txt_count_all_sharj.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_count_all_sharj.ForeColor = System.Drawing.Color.Black;
            this.txt_count_all_sharj.Location = new System.Drawing.Point(20, 5);
            this.txt_count_all_sharj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_count_all_sharj.Name = "txt_count_all_sharj";
            this.txt_count_all_sharj.Size = new System.Drawing.Size(154, 32);
            this.txt_count_all_sharj.TabIndex = 83;
            this.txt_count_all_sharj.Text = "0";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(239, 123);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(176, 32);
            this.labelX11.TabIndex = 82;
            this.labelX11.Text = "مجموع کل فاکتورهای پرداختی :";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(257, 92);
            this.labelX12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(158, 32);
            this.labelX12.TabIndex = 81;
            this.labelX12.Text = "تعداد کل فاکتورهای پرداختی :";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(296, 36);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(119, 32);
            this.labelX13.TabIndex = 80;
            this.labelX13.Text = "مجموع کل فاکتورها :";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(296, 5);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(119, 32);
            this.labelX14.TabIndex = 79;
            this.labelX14.Text = "تعداد کل فاکتورها:";
            // 
            // frm_factor_report_Amari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 498);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "frm_factor_report_Amari";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "محاسبات آماری";
            this.Load += new System.EventHandler(this.frm_factor_report_Amari_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end_date;
        private DevComponents.DotNetBar.LabelX labelX1;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start_date;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX txt_sum_pardakhti_ab;
        private DevComponents.DotNetBar.LabelX txt_count_pardakhti_ab;
        private DevComponents.DotNetBar.LabelX txt_sum_all_ab;
        private DevComponents.DotNetBar.LabelX txt_count_all_ab;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX txt_sum_pardakhti_sharj;
        private DevComponents.DotNetBar.LabelX txt_count_pardakhti_sharj;
        private DevComponents.DotNetBar.LabelX txt_sum_all_sharj;
        private DevComponents.DotNetBar.LabelX txt_count_all_sharj;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX14;
    }
}