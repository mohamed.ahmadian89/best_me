﻿namespace CWMS.Factor
{
    partial class frm_factor_abbasAbad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.grpByGharardad = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_factor_num = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_factor_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.sharj_progress = new System.Windows.Forms.ProgressBar();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_doreh = new DevComponents.DotNetBar.LabelX();
            this.cmb_ghabz_type = new System.Windows.Forms.ComboBox();
            this.btn_create_factor = new DevComponents.DotNetBar.ButtonX();
            this.vd = new DevComponents.DotNetBar.LabelX();
            this.txt_maliat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.txt_takhfif = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtTo = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txtfrom = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmb_doreh = new System.Windows.Forms.ComboBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_sharh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupPanel1.SuspendLayout();
            this.grpByGharardad.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.grpByGharardad);
            this.groupPanel1.Controls.Add(this.sharj_progress);
            this.groupPanel1.Controls.Add(this.groupPanel2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1067, 386);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 5;
            this.groupPanel1.Text = "صدور فاکتور";
            // 
            // grpByGharardad
            // 
            this.grpByGharardad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpByGharardad.Controls.Add(this.txt_factor_num);
            this.grpByGharardad.Controls.Add(this.labelX10);
            this.grpByGharardad.Controls.Add(this.txt_factor_date);
            this.grpByGharardad.Controls.Add(this.labelX11);
            this.grpByGharardad.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpByGharardad.Location = new System.Drawing.Point(40, 17);
            this.grpByGharardad.Name = "grpByGharardad";
            this.grpByGharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpByGharardad.Size = new System.Drawing.Size(968, 49);
            // 
            // 
            // 
            this.grpByGharardad.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpByGharardad.Style.BackColorGradientAngle = 90;
            this.grpByGharardad.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpByGharardad.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderBottomWidth = 1;
            this.grpByGharardad.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpByGharardad.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderLeftWidth = 1;
            this.grpByGharardad.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderRightWidth = 1;
            this.grpByGharardad.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderTopWidth = 1;
            this.grpByGharardad.Style.CornerDiameter = 4;
            this.grpByGharardad.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpByGharardad.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpByGharardad.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpByGharardad.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpByGharardad.TabIndex = 167;
            // 
            // txt_factor_num
            // 
            this.txt_factor_num.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_factor_num.Border.Class = "TextBoxBorder";
            this.txt_factor_num.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_factor_num.DisabledBackColor = System.Drawing.Color.White;
            this.txt_factor_num.ForeColor = System.Drawing.Color.Black;
            this.txt_factor_num.Location = new System.Drawing.Point(532, 7);
            this.txt_factor_num.Name = "txt_factor_num";
            this.txt_factor_num.PreventEnterBeep = true;
            this.txt_factor_num.Size = new System.Drawing.Size(76, 28);
            this.txt_factor_num.TabIndex = 178;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(614, 10);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(98, 23);
            this.labelX10.TabIndex = 176;
            this.labelX10.Text = "شماره فاکتور جدید:";
            // 
            // txt_factor_date
            // 
            this.txt_factor_date.Location = new System.Drawing.Point(201, 11);
            this.txt_factor_date.Name = "txt_factor_date";
            this.txt_factor_date.Size = new System.Drawing.Size(115, 24);
            this.txt_factor_date.TabIndex = 172;
            this.txt_factor_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(300, 12);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(118, 23);
            this.labelX11.TabIndex = 171;
            this.labelX11.Text = "تاریخ صدور فاکتور:";
            // 
            // sharj_progress
            // 
            this.sharj_progress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.sharj_progress.ForeColor = System.Drawing.Color.Black;
            this.sharj_progress.Location = new System.Drawing.Point(40, 315);
            this.sharj_progress.Name = "sharj_progress";
            this.sharj_progress.Size = new System.Drawing.Size(968, 23);
            this.sharj_progress.TabIndex = 55;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.lbl_doreh);
            this.groupPanel2.Controls.Add(this.cmb_ghabz_type);
            this.groupPanel2.Controls.Add(this.btn_create_factor);
            this.groupPanel2.Controls.Add(this.vd);
            this.groupPanel2.Controls.Add(this.txt_maliat);
            this.groupPanel2.Controls.Add(this.labelX13);
            this.groupPanel2.Controls.Add(this.txt_takhfif);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.txtTo);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.labelX8);
            this.groupPanel2.Controls.Add(this.txtfrom);
            this.groupPanel2.Controls.Add(this.cmb_doreh);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.txt_sharh);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(40, 75);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(968, 234);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 37;
            this.groupPanel2.Text = "جزئیات مربوط به فاکتور";
            // 
            // lbl_doreh
            // 
            this.lbl_doreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_doreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_doreh.ForeColor = System.Drawing.Color.Black;
            this.lbl_doreh.Location = new System.Drawing.Point(600, 71);
            this.lbl_doreh.Name = "lbl_doreh";
            this.lbl_doreh.Size = new System.Drawing.Size(73, 28);
            this.lbl_doreh.TabIndex = 176;
            this.lbl_doreh.Text = "دوره :";
            // 
            // cmb_ghabz_type
            // 
            this.cmb_ghabz_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_ghabz_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.cmb_ghabz_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ghabz_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_ghabz_type.FormattingEnabled = true;
            this.cmb_ghabz_type.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_ghabz_type.Location = new System.Drawing.Point(689, 71);
            this.cmb_ghabz_type.Name = "cmb_ghabz_type";
            this.cmb_ghabz_type.Size = new System.Drawing.Size(97, 28);
            this.cmb_ghabz_type.TabIndex = 175;
            this.cmb_ghabz_type.SelectedIndexChanged += new System.EventHandler(this.cmb_ghabz_type_SelectedIndexChanged);
            // 
            // btn_create_factor
            // 
            this.btn_create_factor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_factor.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_factor.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_factor.Location = new System.Drawing.Point(27, 140);
            this.btn_create_factor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_create_factor.Name = "btn_create_factor";
            this.btn_create_factor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_create_factor.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_create_factor.Size = new System.Drawing.Size(181, 38);
            this.btn_create_factor.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_factor.Symbol = "";
            this.btn_create_factor.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_factor.SymbolSize = 12F;
            this.btn_create_factor.TabIndex = 174;
            this.btn_create_factor.Text = "صدور فاکتور";
            this.btn_create_factor.Click += new System.EventHandler(this.btn_create_factor_Click);
            // 
            // vd
            // 
            this.vd.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.vd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.vd.ForeColor = System.Drawing.Color.Black;
            this.vd.Location = new System.Drawing.Point(244, 74);
            this.vd.Name = "vd";
            this.vd.Size = new System.Drawing.Size(45, 23);
            this.vd.TabIndex = 56;
            this.vd.Text = "ریال";
            // 
            // txt_maliat
            // 
            this.txt_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_maliat.Border.Class = "TextBoxBorder";
            this.txt_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_maliat.Location = new System.Drawing.Point(117, 71);
            this.txt_maliat.Name = "txt_maliat";
            this.txt_maliat.PreventEnterBeep = true;
            this.txt_maliat.Size = new System.Drawing.Size(41, 28);
            this.txt_maliat.TabIndex = 170;
            this.txt_maliat.Text = "0";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(164, 74);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(74, 23);
            this.labelX13.TabIndex = 169;
            this.labelX13.Text = "درصد مالیات :";
            // 
            // txt_takhfif
            // 
            this.txt_takhfif.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_takhfif.Border.Class = "TextBoxBorder";
            this.txt_takhfif.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_takhfif.DisabledBackColor = System.Drawing.Color.White;
            this.txt_takhfif.ForeColor = System.Drawing.Color.Black;
            this.txt_takhfif.Location = new System.Drawing.Point(304, 71);
            this.txt_takhfif.Name = "txt_takhfif";
            this.txt_takhfif.PreventEnterBeep = true;
            this.txt_takhfif.Size = new System.Drawing.Size(97, 28);
            this.txt_takhfif.TabIndex = 55;
            this.txt_takhfif.Text = "0";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(342, 74);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(134, 23);
            this.labelX1.TabIndex = 54;
            this.labelX1.Text = "مبلغ تخفیف:";
            // 
            // txtTo
            // 
            this.txtTo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTo.Border.Class = "TextBoxBorder";
            this.txtTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTo.DisabledBackColor = System.Drawing.Color.White;
            this.txtTo.ForeColor = System.Drawing.Color.Black;
            this.txtTo.Location = new System.Drawing.Point(502, 36);
            this.txtTo.Name = "txtTo";
            this.txtTo.PreventEnterBeep = true;
            this.txtTo.Size = new System.Drawing.Size(97, 28);
            this.txtTo.TabIndex = 1;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(576, 36);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(97, 28);
            this.labelX6.TabIndex = 34;
            this.labelX6.Text = "الی قرارداد :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(802, 105);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(69, 28);
            this.labelX8.TabIndex = 53;
            this.labelX8.Text = "شرح خدمات:";
            // 
            // txtfrom
            // 
            this.txtfrom.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfrom.Border.Class = "TextBoxBorder";
            this.txtfrom.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfrom.DisabledBackColor = System.Drawing.Color.White;
            this.txtfrom.ForeColor = System.Drawing.Color.Black;
            this.txtfrom.Location = new System.Drawing.Point(689, 36);
            this.txtfrom.Name = "txtfrom";
            this.txtfrom.PreventEnterBeep = true;
            this.txtfrom.Size = new System.Drawing.Size(97, 28);
            this.txtfrom.TabIndex = 0;
            // 
            // cmb_doreh
            // 
            this.cmb_doreh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_doreh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.cmb_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_doreh.FormattingEnabled = true;
            this.cmb_doreh.Location = new System.Drawing.Point(502, 71);
            this.cmb_doreh.Name = "cmb_doreh";
            this.cmb_doreh.Size = new System.Drawing.Size(97, 28);
            this.cmb_doreh.TabIndex = 166;
            this.cmb_doreh.SelectedIndexChanged += new System.EventHandler(this.cmb_doreh_SelectedIndexChanged);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(774, 36);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(97, 28);
            this.labelX2.TabIndex = 29;
            this.labelX2.Text = "از قرارداد :";
            // 
            // txt_sharh
            // 
            this.txt_sharh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sharh.Border.Class = "TextBoxBorder";
            this.txt_sharh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sharh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sharh.ForeColor = System.Drawing.Color.Black;
            this.txt_sharh.Location = new System.Drawing.Point(117, 105);
            this.txt_sharh.Name = "txt_sharh";
            this.txt_sharh.PreventEnterBeep = true;
            this.txt_sharh.Size = new System.Drawing.Size(669, 28);
            this.txt_sharh.TabIndex = 0;
            this.txt_sharh.Text = "ارائه خدمات دوره";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(798, 71);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(73, 28);
            this.labelX5.TabIndex = 165;
            this.labelX5.Text = "نوع فاکتور :";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // frm_factor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 461);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_factor";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سامانه صدور فاکتور";
            this.Load += new System.EventHandler(this.frm_factor_Load);
            this.groupPanel1.ResumeLayout(false);
            this.grpByGharardad.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpByGharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_factor_num;
        private DevComponents.DotNetBar.LabelX labelX10;
        private FarsiLibrary.Win.Controls.FADatePicker txt_factor_date;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.ProgressBar sharj_progress;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX lbl_doreh;
        private System.Windows.Forms.ComboBox cmb_ghabz_type;
        private DevComponents.DotNetBar.ButtonX btn_create_factor;
        private DevComponents.DotNetBar.LabelX vd;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_maliat;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_takhfif;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTo;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfrom;
        private System.Windows.Forms.ComboBox cmb_doreh;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sharh;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}