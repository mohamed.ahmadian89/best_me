﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions;

namespace CWMS
{
    public partial class FrmShowFactor : MyMetroForm
    {
        //public FrmShowReport(CrysReports.Sharj_doreh cr)
        //{
        //    InitializeComponent();
        //    crystalReportViewer1.ReportSource = cr;
        //}
        string Main_factor_id = "";
        string gcode;
        int ghabz_type = 0;
        public FrmShowFactor(string factor_id)
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            System.Data.DataTable dt_factor_info = Classes.ClsMain.GetDataTable("select * from factors where id=" + factor_id);
            txt_factor_id.Text = Main_factor_id = factor_id;
            txt_doreh.Text = dt_factor_info.Rows[0]["dcode"].ToString();
            gcode = dt_factor_info.Rows[0]["gcode"].ToString();
            ghabz_type = Convert.ToInt32(dt_factor_info.Rows[0]["ghabz_type"]);


            txt_majmu.Text = dt_factor_info.Rows[0]["majmu_mabalegh"].ToString();
            txt_takhfif.Text = dt_factor_info.Rows[0]["takhfif"].ToString();
            txt_maliat.Text = dt_factor_info.Rows[0]["maliat"].ToString();
            txt_mablaghkol.Text = dt_factor_info.Rows[0]["mablaghkol"].ToString();
            txt_sharh.Text = dt_factor_info.Rows[0]["sharh"].ToString();

            txt_darsad_maliat.Text = dt_factor_info.Rows[0]["darsad_maliat"].ToString();
            txt_factor_date.SelectedDateTime = Convert.ToDateTime(dt_factor_info.Rows[0]["tarikh"]);
            txt_factor_num.Text = dt_factor_info.Rows[0]["factor"].ToString();


            System.Data.DataTable dt_moshtarek_info = Classes.ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + dt_factor_info.Rows[0]["gharardad"].ToString());
            txt_moshtarek_fullname.Text = dt_moshtarek_info.Rows[0]["co_name"].ToString();
            txt_moshtarek_gharardad.Text = dt_moshtarek_info.Rows[0]["gharardad"].ToString();
            txt_moshtarek_codeposti.Text = dt_moshtarek_info.Rows[0]["code_posti"].ToString();
            txt_moshtarek_shenaseMelli.Text = dt_moshtarek_info.Rows[0]["code_melli"].ToString();
            txt_moshtarek_codeEghtesadi.Text = dt_moshtarek_info.Rows[0]["code_eghtesadi"].ToString();
            txt_moshtarek_address.Text = dt_moshtarek_info.Rows[0]["address"].ToString();
            txt_moshtarek_phone.Text = dt_moshtarek_info.Rows[0]["phone"].ToString();



        }




        private void FrmShowReport_Load(object sender, EventArgs e)
        {
        }



        private void btn_show_Click(object sender, EventArgs e)
        {
            try
            {
                Convert.ToDateTime(txt_factor_date.SelectedDateTime);
            }
            catch (Exception)
            {
                Payam.Show("لطفا تاریخ فاکتور را به درستی انتخاب نمایید");
                txt_factor_date.Focus();
                return;
            }


            if (Check_data())
            {

                Classes.ClsMain.ChangeCulture("e");
                Classes.ClsMain.ExecuteNoneQuery("update factors set  majmu_mabalegh=" + txt_majmu.Text + ",takhfif=" + txt_takhfif.Text +
                    ",maliat=" + txt_maliat.Text + ",mablaghkol=" + txt_mablaghkol.Text + ",dcode=" + txt_doreh.Text +
                    ",sharh='" + txt_sharh.Text + "'," +
                     "tarikh='" + txt_factor_date.SelectedDateTime + "',darsad_maliat=" + txt_darsad_maliat.Text
                    + ",factor=" + txt_factor_num.Text
                    + " where id=" + txt_factor_id.Text);
                Payam.Show("اطلاعات فاکتور با موفقیت بروزرسانی شد");
                Classes.ClsMain.ChangeCulture("f");

            }
            else
            {
                Payam.Show("لطفا اطلاعات را به درستی وارد نمایید");
            }

        }



        void Chap_factor(bool Is_Negah, bool Is_tupple)
        {

            Check_data();
            CrystalDecisions.CrystalReports.Engine.ReportClass report1 = new CrystalDecisions.CrystalReports.Engine.ReportClass();
            if (Is_Negah == false)
            {
                if (Is_tupple)
                    report1 = new CrysReports.main_factor_tupple();
                else
                    report1 = new CrysReports.main_factor();
            }
            else
            {
                if (Is_tupple)
                    report1 = new CrysReports.main_factor_negah_tupple();
                else
                    report1 = new CrysReports.main_factor_negah();
            }

            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(txt_factor_date.SelectedDateTime)).ToString("d"));

            report1.SetParameterValue("doreh", txt_doreh.Text);
            report1.SetParameterValue("gharardad", txt_moshtarek_gharardad.Text);


            report1.SetParameterValue("m_co_name", txt_moshtarek_fullname.Text);
            report1.SetParameterValue("m_shomare_eghtesadi", txt_moshtarek_codeEghtesadi.Text);
            report1.SetParameterValue("m_shenase_melli", txt_moshtarek_shenaseMelli.Text);
            report1.SetParameterValue("m_code_posti", txt_moshtarek_codeposti.Text);

            report1.SetParameterValue("m_address", txt_moshtarek_address.Text);
            report1.SetParameterValue("m_phone", txt_moshtarek_phone.Text);




            decimal majmu = Convert.ToDecimal(txt_majmu.Text);
            decimal takhfif = Convert.ToDecimal(txt_takhfif.Text);
            decimal maliat = Convert.ToDecimal(txt_maliat.Text);
            decimal Mablahkol = Convert.ToDecimal(txt_mablaghkol.Text);
            report1.SetParameterValue("majmu_mabalegh", majmu);
            report1.SetParameterValue("takhfif", takhfif);
            report1.SetParameterValue("majmu_mabalegh_takhfif", majmu - takhfif);
            report1.SetParameterValue("maliat", maliat);
            report1.SetParameterValue("mablaghkol", Mablahkol);
            report1.SetParameterValue("sharh", txt_sharh.Text);
            if (Is_Negah)
                report1.SetParameterValue("factor_number", txt_factor_num.Text);

            if (Is_tupple)
            {
                DataTable dt_Another_factor = Classes.ClsMain.GetDataTable(Tupple_command);
                if (Is_Negah)
                    report1.SetParameterValue("factor_number", txt_factor_num.Text + " / " + dt_Another_factor.Rows[0]["factor"].ToString());


                decimal majmu_1 = Convert.ToDecimal(dt_Another_factor.Rows[0]["majmu_mabalegh"]);
                decimal takhfif_1 = Convert.ToDecimal(dt_Another_factor.Rows[0]["takhfif"]);
                decimal maliat_1 = Convert.ToDecimal(dt_Another_factor.Rows[0]["maliat"]);
                decimal Mablahkol_1 = Convert.ToDecimal(dt_Another_factor.Rows[0]["mablaghkol"]);


                report1.SetParameterValue("majmu_mabalegh_1", majmu_1);
                report1.SetParameterValue("takhfif_1", takhfif_1);
                report1.SetParameterValue("majmu_mabalegh_takhfif_1", majmu_1 - takhfif_1);
                report1.SetParameterValue("maliat_1", maliat_1);
                report1.SetParameterValue("mablaghkol_1", Mablahkol_1);
                report1.SetParameterValue("sharh_1", dt_Another_factor.Rows[0]["sharh"].ToString());
                report1.SetParameterValue("doreh_1", dt_Another_factor.Rows[0]["dcode"].ToString());



                report1.SetParameterValue("majmu_mabalegh_2", majmu_1 + majmu);
                report1.SetParameterValue("takhfif_2", takhfif_1 + takhfif);
                report1.SetParameterValue("majmu_mabalegh_takhfif_2", majmu_1 - takhfif_1 + majmu - takhfif);
                report1.SetParameterValue("maliat_2", maliat_1 + maliat);

                Decimal mablaghkol_2 = Mablahkol_1 + Mablahkol;
                decimal kasr_hezar = (mablaghkol_2 % 1000);
                report1.SetParameterValue("mablaghkol_2", mablaghkol_2 - kasr_hezar);
                report1.SetParameterValue("kasr_hezar", kasr_hezar);
                report1.SetParameterValue("mablaghkol_horoof", Classes.clsNumber.Number_to_Str((mablaghkol_2 - kasr_hezar).ToString()).ToString() + "  ریال ");



            }
            else
            {
                report1.SetParameterValue("mablaghkol_horoof", Classes.clsNumber.Number_to_Str(Mablahkol.ToString()).ToString() + "  ریال ");
                report1.SetParameterValue("kasr_hezar", Mablahkol % 1000);
            }



            crystalReportViewer1.ReportSource = report1;
            crystalReportViewer1.Refresh();
            crystalReportViewer1.PrintReport();
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            //Chap_factor(false);
            if (buttonX3.Text == "چاپ فاکتور")
            {
                buttonX3.Text = "بستن کادر";
                grp_chap.Visible = true;
            }
            else
            {
                buttonX3.Text = "چاپ فاکتور";
                grp_chap.Visible = false;
            }

        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            if (txt_factor_id.Text.Trim() != "")
            {


                if (Cpayam.Show("آیا با حذف فاکتور موافق هستید؟") == DialogResult.Yes)
                {

                    Classes.ClsMain.ExecuteNoneQuery("delete from factors where id=" + txt_factor_id.Text);
                    Payam.Show("فاکتور با موفقیت از سیستم حذف شد");
                    this.Close();
                }
            }
        }

        private void groupPanel3_Click(object sender, EventArgs e)
        {

        }

        private void txt_sharj_mablaghkol_Leave(object sender, EventArgs e)
        {
            Check_data();
        }
        bool Check_data()
        {
            bool result = true;
            try
            {
                decimal mamju = Convert.ToDecimal(txt_majmu.Text) - Convert.ToDecimal(txt_takhfif.Text);
                decimal maliat = Math.Round(Convert.ToDecimal(mamju * Convert.ToDecimal(txt_darsad_maliat.Text) / 100));
                txt_maliat.Text = maliat.ToString();
                txt_mablaghkol.Text = (mamju + maliat).ToString();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }



        private void txt_sharj_takhfif_Leave(object sender, EventArgs e)
        {
            Check_data();
        }



        private void buttonX4_Click(object sender, EventArgs e)
        {
            try
            {
                if (ghabz_type == 0)
                    new Ab.Frm_ab_details(gcode, txt_moshtarek_gharardad.Text).ShowDialog();
                else
                    new Frm_sharj_details(gcode, txt_moshtarek_gharardad.Text).ShowDialog();

            }
            catch (Exception)
            {

                Payam.Show("امکان نمایش اطلاعات قبض وجود ندارد");
            }
        }

        private void btn_chap_negah_Click(object sender, EventArgs e)
        {
            //Chap_factor(true);

        }
        string Tupple_command = "";
        private void rd_one_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_one.Checked)
            {
                lbl_doreh_type.Visible = false;
                cmb_doreh_type.Visible = false;
            }
            else
            {
                lbl_doreh_type.Visible = true;
                cmb_doreh_type.Visible = true;
                DataTable dt_info;

                if (ghabz_type == 0)
                {
                    lbl_doreh_type.Text = "اطلاعات فاکتور شارژ مربوط به دوره :";
                    dt_info = Classes.ClsMain.GetDataTable("select dcode from factors where gharardad=" + txt_moshtarek_gharardad.Text
                        + " and ghabz_type=1"
                        );
                    if (dt_info.Rows.Count == 0)
                    {
                        Payam.Show("برای این مشترک هیچ فاکتور شارژی صادر نشده است ");
                        rd_one.Checked = true;
                        lbl_doreh_type.Visible = false;
                        cmb_doreh_type.Visible = false;
                    }
                    else
                    {
                        cmb_doreh_type.DataSource = dt_info;
                        cmb_doreh_type.DisplayMember = "dcode";
                        cmb_doreh_type.ValueMember = "dcode";
                        cmb_doreh_type.SelectedIndex = 0;

                    }
                }
                else
                {
                    lbl_doreh_type.Text = "اطلاعات فاکتور آّب مربوط به دوره :";
                    dt_info = Classes.ClsMain.GetDataTable("select dcode from factors where gharardad=" + txt_moshtarek_gharardad.Text
                       + " and ghabz_type=0"
                       );
                    if (dt_info.Rows.Count == 0)
                    {
                        Payam.Show("برای این مشترک هیچ فاکتور آبی صادر نشده است ");
                        rd_one.Checked = true;
                    }
                    else
                    {
                        cmb_doreh_type.DataSource = dt_info;
                        cmb_doreh_type.DisplayMember = "dcode";
                        cmb_doreh_type.ValueMember = "dcode";
                        

                    }

                }



            }
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {

        }

        private void btn_chap_negah_Click_1(object sender, EventArgs e)
        {
            if (rd_one.Checked)
            {
                Chap_factor(true, false);
            }
            else
            {
                Tupple_command = "select * from factors where ghabz_type=" + (ghabz_type == 0 ? 1 : 0).ToString()
                    + " and gharardad=" + txt_moshtarek_gharardad.Text + " and dcode=" + cmb_doreh_type.Text.ToString();
                Chap_factor(true, true);
            }
        }

        private void grpByGharardad_Click(object sender, EventArgs e)
        {

        }

        private void cmb_doreh_type_SelectedIndexChanged(object sender, EventArgs e)
        {

           
        }

        private void btn_chap_darayi_Click(object sender, EventArgs e)
        {

            if (rd_one.Checked)
            {
                Chap_factor(false, false);
            }
            else
            {
                Tupple_command = "select * from factors where ghabz_type=" + (ghabz_type == 0 ? 1 : 0).ToString()
                    + " and gharardad=" + txt_moshtarek_gharardad.Text + " and dcode=" + cmb_doreh_type.Text.ToString();
                Chap_factor(false, true);
            }
        }
    }
}