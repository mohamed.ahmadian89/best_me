﻿namespace CWMS
{
    partial class FrmShowFactor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_show = new DevComponents.DotNetBar.ButtonX();
            this.grp_chap = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_chap_negah = new DevComponents.DotNetBar.ButtonX();
            this.btn_chap_darayi = new DevComponents.DotNetBar.ButtonX();
            this.cmb_doreh_type = new System.Windows.Forms.ComboBox();
            this.lbl_doreh_type = new DevComponents.DotNetBar.LabelX();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rd_one = new System.Windows.Forms.RadioButton();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_moshtarek_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_address = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_phone = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_shenaseMelli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX17 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_codeposti = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_codeEghtesadi = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.txt_moshtarek_fullname = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_darsad_maliat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_factor_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txt_factor_num = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.txt_factor_id = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.grpByGharardad = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_mablaghkol = new CWMS.MoneyTextBox();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.txt_doreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.txt_maliat = new CWMS.MoneyTextBox();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txt_majmu = new CWMS.MoneyTextBox();
            this.txt_takhfif = new CWMS.MoneyTextBox();
            this.txt_sharh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.grp_chap.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.grpByGharardad.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_show
            // 
            this.btn_show.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_show.BackColor = System.Drawing.Color.Transparent;
            this.btn_show.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_show.Location = new System.Drawing.Point(310, 432);
            this.btn_show.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_show.Name = "btn_show";
            this.btn_show.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_show.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_show.Size = new System.Drawing.Size(222, 31);
            this.btn_show.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_show.Symbol = "";
            this.btn_show.SymbolColor = System.Drawing.Color.Green;
            this.btn_show.SymbolSize = 12F;
            this.btn_show.TabIndex = 170;
            this.btn_show.Text = "اعمال تغییرات";
            this.btn_show.Click += new System.EventHandler(this.btn_show_Click);
            // 
            // grp_chap
            // 
            this.grp_chap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grp_chap.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grp_chap.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_chap.Controls.Add(this.btn_chap_negah);
            this.grp_chap.Controls.Add(this.btn_chap_darayi);
            this.grp_chap.Controls.Add(this.cmb_doreh_type);
            this.grp_chap.Controls.Add(this.lbl_doreh_type);
            this.grp_chap.Controls.Add(this.radioButton1);
            this.grp_chap.Controls.Add(this.rd_one);
            this.grp_chap.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_chap.Location = new System.Drawing.Point(18, 129);
            this.grp_chap.Name = "grp_chap";
            this.grp_chap.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_chap.Size = new System.Drawing.Size(403, 295);
            // 
            // 
            // 
            this.grp_chap.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_chap.Style.BackColorGradientAngle = 90;
            this.grp_chap.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_chap.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_chap.Style.BorderBottomWidth = 1;
            this.grp_chap.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_chap.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_chap.Style.BorderLeftWidth = 1;
            this.grp_chap.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_chap.Style.BorderRightWidth = 1;
            this.grp_chap.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_chap.Style.BorderTopWidth = 1;
            this.grp_chap.Style.CornerDiameter = 4;
            this.grp_chap.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_chap.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_chap.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_chap.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_chap.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_chap.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_chap.TabIndex = 192;
            this.grp_chap.Text = "تنظیمات چاپ";
            this.grp_chap.Visible = false;
            // 
            // btn_chap_negah
            // 
            this.btn_chap_negah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_chap_negah.BackColor = System.Drawing.Color.Transparent;
            this.btn_chap_negah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_chap_negah.Location = new System.Drawing.Point(20, 165);
            this.btn_chap_negah.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_chap_negah.Name = "btn_chap_negah";
            this.btn_chap_negah.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_chap_negah.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_chap_negah.Size = new System.Drawing.Size(165, 45);
            this.btn_chap_negah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_chap_negah.Symbol = "";
            this.btn_chap_negah.SymbolColor = System.Drawing.Color.Green;
            this.btn_chap_negah.SymbolSize = 12F;
            this.btn_chap_negah.TabIndex = 202;
            this.btn_chap_negah.Text = "چاپ فاکتور ( نگاه )";
            this.btn_chap_negah.Click += new System.EventHandler(this.btn_chap_negah_Click_1);
            // 
            // btn_chap_darayi
            // 
            this.btn_chap_darayi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_chap_darayi.BackColor = System.Drawing.Color.Transparent;
            this.btn_chap_darayi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_chap_darayi.Location = new System.Drawing.Point(221, 165);
            this.btn_chap_darayi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_chap_darayi.Name = "btn_chap_darayi";
            this.btn_chap_darayi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_chap_darayi.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_chap_darayi.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btn_chap_darayi.Size = new System.Drawing.Size(155, 45);
            this.btn_chap_darayi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_chap_darayi.Symbol = "";
            this.btn_chap_darayi.SymbolColor = System.Drawing.Color.Green;
            this.btn_chap_darayi.SymbolSize = 12F;
            this.btn_chap_darayi.TabIndex = 201;
            this.btn_chap_darayi.Text = "چاپ فاکتور ( دارایی )";
            this.btn_chap_darayi.Click += new System.EventHandler(this.btn_chap_darayi_Click);
            // 
            // cmb_doreh_type
            // 
            this.cmb_doreh_type.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmb_doreh_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.cmb_doreh_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_doreh_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_doreh_type.FormattingEnabled = true;
            this.cmb_doreh_type.Location = new System.Drawing.Point(82, 98);
            this.cmb_doreh_type.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_doreh_type.Name = "cmb_doreh_type";
            this.cmb_doreh_type.Size = new System.Drawing.Size(65, 26);
            this.cmb_doreh_type.TabIndex = 200;
            this.cmb_doreh_type.Visible = false;
            this.cmb_doreh_type.SelectedIndexChanged += new System.EventHandler(this.cmb_doreh_type_SelectedIndexChanged);
            // 
            // lbl_doreh_type
            // 
            this.lbl_doreh_type.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_doreh_type.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_doreh_type.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_doreh_type.ForeColor = System.Drawing.Color.Black;
            this.lbl_doreh_type.Location = new System.Drawing.Point(154, 101);
            this.lbl_doreh_type.Name = "lbl_doreh_type";
            this.lbl_doreh_type.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_doreh_type.Size = new System.Drawing.Size(222, 23);
            this.lbl_doreh_type.TabIndex = 197;
            this.lbl_doreh_type.Text = "فاکتور شارژ مربوط به دوره :";
            this.lbl_doreh_type.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.ForeColor = System.Drawing.Color.Black;
            this.radioButton1.Location = new System.Drawing.Point(44, 58);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(332, 22);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.Text = "در برگه فاکتور،  علاوه بر فاکتور فعلی ، اطلاعات فاکتور زیر نیز چاپ شود\r\n";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // rd_one
            // 
            this.rd_one.AutoSize = true;
            this.rd_one.BackColor = System.Drawing.Color.Transparent;
            this.rd_one.Checked = true;
            this.rd_one.ForeColor = System.Drawing.Color.Black;
            this.rd_one.Location = new System.Drawing.Point(128, 29);
            this.rd_one.Name = "rd_one";
            this.rd_one.Size = new System.Drawing.Size(248, 22);
            this.rd_one.TabIndex = 0;
            this.rd_one.TabStop = true;
            this.rd_one.Text = "در برگه فاکتور،  فقط اطلاعات فاکتور فعلی چاپ شود";
            this.rd_one.UseVisualStyleBackColor = false;
            this.rd_one.CheckedChanged += new System.EventHandler(this.rd_one_CheckedChanged);
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.txt_moshtarek_gharardad);
            this.groupPanel3.Controls.Add(this.labelX21);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_address);
            this.groupPanel3.Controls.Add(this.labelX20);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_phone);
            this.groupPanel3.Controls.Add(this.labelX19);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_shenaseMelli);
            this.groupPanel3.Controls.Add(this.labelX17);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_codeposti);
            this.groupPanel3.Controls.Add(this.labelX16);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_codeEghtesadi);
            this.groupPanel3.Controls.Add(this.labelX15);
            this.groupPanel3.Controls.Add(this.txt_moshtarek_fullname);
            this.groupPanel3.Controls.Add(this.labelX18);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(14, 56);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(1119, 133);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 193;
            this.groupPanel3.Text = "اطلاعات مشترک";
            this.groupPanel3.Click += new System.EventHandler(this.groupPanel3_Click);
            // 
            // txt_moshtarek_gharardad
            // 
            this.txt_moshtarek_gharardad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_gharardad.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_gharardad.Location = new System.Drawing.Point(864, 41);
            this.txt_moshtarek_gharardad.Name = "txt_moshtarek_gharardad";
            this.txt_moshtarek_gharardad.PreventEnterBeep = true;
            this.txt_moshtarek_gharardad.ReadOnly = true;
            this.txt_moshtarek_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_gharardad.Size = new System.Drawing.Size(110, 26);
            this.txt_moshtarek_gharardad.TabIndex = 199;
            // 
            // labelX21
            // 
            this.labelX21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(983, 43);
            this.labelX21.Name = "labelX21";
            this.labelX21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX21.Size = new System.Drawing.Size(90, 23);
            this.labelX21.TabIndex = 200;
            this.labelX21.Text = "شماره قرارداد:";
            // 
            // txt_moshtarek_address
            // 
            this.txt_moshtarek_address.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_address.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_address.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_address.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_address.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_address.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_address.Location = new System.Drawing.Point(64, 73);
            this.txt_moshtarek_address.Name = "txt_moshtarek_address";
            this.txt_moshtarek_address.PreventEnterBeep = true;
            this.txt_moshtarek_address.ReadOnly = true;
            this.txt_moshtarek_address.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_address.Size = new System.Drawing.Size(910, 26);
            this.txt_moshtarek_address.TabIndex = 197;
            // 
            // labelX20
            // 
            this.labelX20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(983, 71);
            this.labelX20.Name = "labelX20";
            this.labelX20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX20.Size = new System.Drawing.Size(90, 23);
            this.labelX20.TabIndex = 198;
            this.labelX20.Text = "آدرس:";
            // 
            // txt_moshtarek_phone
            // 
            this.txt_moshtarek_phone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_phone.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_phone.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_phone.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_phone.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_phone.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_phone.Location = new System.Drawing.Point(535, 40);
            this.txt_moshtarek_phone.Name = "txt_moshtarek_phone";
            this.txt_moshtarek_phone.PreventEnterBeep = true;
            this.txt_moshtarek_phone.ReadOnly = true;
            this.txt_moshtarek_phone.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_phone.Size = new System.Drawing.Size(110, 26);
            this.txt_moshtarek_phone.TabIndex = 195;
            // 
            // labelX19
            // 
            this.labelX19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(636, 42);
            this.labelX19.Name = "labelX19";
            this.labelX19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX19.Size = new System.Drawing.Size(90, 23);
            this.labelX19.TabIndex = 196;
            this.labelX19.Text = "تلفن:";
            // 
            // txt_moshtarek_shenaseMelli
            // 
            this.txt_moshtarek_shenaseMelli.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_shenaseMelli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_shenaseMelli.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_shenaseMelli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_shenaseMelli.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_shenaseMelli.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_shenaseMelli.Location = new System.Drawing.Point(64, 10);
            this.txt_moshtarek_shenaseMelli.Name = "txt_moshtarek_shenaseMelli";
            this.txt_moshtarek_shenaseMelli.PreventEnterBeep = true;
            this.txt_moshtarek_shenaseMelli.ReadOnly = true;
            this.txt_moshtarek_shenaseMelli.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_shenaseMelli.Size = new System.Drawing.Size(110, 26);
            this.txt_moshtarek_shenaseMelli.TabIndex = 193;
            // 
            // labelX17
            // 
            this.labelX17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX17.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX17.ForeColor = System.Drawing.Color.Black;
            this.labelX17.Location = new System.Drawing.Point(162, 12);
            this.labelX17.Name = "labelX17";
            this.labelX17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX17.Size = new System.Drawing.Size(90, 23);
            this.labelX17.TabIndex = 194;
            this.labelX17.Text = "شناسه ملی :";
            // 
            // txt_moshtarek_codeposti
            // 
            this.txt_moshtarek_codeposti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_codeposti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_codeposti.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_codeposti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_codeposti.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_codeposti.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_codeposti.Location = new System.Drawing.Point(284, 10);
            this.txt_moshtarek_codeposti.Name = "txt_moshtarek_codeposti";
            this.txt_moshtarek_codeposti.PreventEnterBeep = true;
            this.txt_moshtarek_codeposti.ReadOnly = true;
            this.txt_moshtarek_codeposti.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_codeposti.Size = new System.Drawing.Size(110, 26);
            this.txt_moshtarek_codeposti.TabIndex = 191;
            // 
            // labelX16
            // 
            this.labelX16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX16.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.ForeColor = System.Drawing.Color.Black;
            this.labelX16.Location = new System.Drawing.Point(382, 12);
            this.labelX16.Name = "labelX16";
            this.labelX16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX16.Size = new System.Drawing.Size(90, 23);
            this.labelX16.TabIndex = 192;
            this.labelX16.Text = "کدپستی:";
            // 
            // txt_moshtarek_codeEghtesadi
            // 
            this.txt_moshtarek_codeEghtesadi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_codeEghtesadi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_codeEghtesadi.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_codeEghtesadi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_codeEghtesadi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_codeEghtesadi.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_codeEghtesadi.Location = new System.Drawing.Point(535, 10);
            this.txt_moshtarek_codeEghtesadi.Name = "txt_moshtarek_codeEghtesadi";
            this.txt_moshtarek_codeEghtesadi.PreventEnterBeep = true;
            this.txt_moshtarek_codeEghtesadi.ReadOnly = true;
            this.txt_moshtarek_codeEghtesadi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_codeEghtesadi.Size = new System.Drawing.Size(110, 26);
            this.txt_moshtarek_codeEghtesadi.TabIndex = 189;
            // 
            // labelX15
            // 
            this.labelX15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(633, 12);
            this.labelX15.Name = "labelX15";
            this.labelX15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX15.Size = new System.Drawing.Size(90, 23);
            this.labelX15.TabIndex = 190;
            this.labelX15.Text = "کداقتصادی :";
            // 
            // txt_moshtarek_fullname
            // 
            this.txt_moshtarek_fullname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_moshtarek_fullname.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_moshtarek_fullname.Border.Class = "TextBoxBorder";
            this.txt_moshtarek_fullname.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_moshtarek_fullname.DisabledBackColor = System.Drawing.Color.White;
            this.txt_moshtarek_fullname.ForeColor = System.Drawing.Color.Black;
            this.txt_moshtarek_fullname.Location = new System.Drawing.Point(746, 10);
            this.txt_moshtarek_fullname.Name = "txt_moshtarek_fullname";
            this.txt_moshtarek_fullname.PreventEnterBeep = true;
            this.txt_moshtarek_fullname.ReadOnly = true;
            this.txt_moshtarek_fullname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_moshtarek_fullname.Size = new System.Drawing.Size(228, 26);
            this.txt_moshtarek_fullname.TabIndex = 186;
            // 
            // labelX18
            // 
            this.labelX18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(983, 12);
            this.labelX18.Name = "labelX18";
            this.labelX18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX18.Size = new System.Drawing.Size(90, 23);
            this.labelX18.TabIndex = 187;
            this.labelX18.Text = "نام و نام خانوادگی :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txt_darsad_maliat);
            this.groupPanel2.Controls.Add(this.txt_factor_date);
            this.groupPanel2.Controls.Add(this.txt_factor_num);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Controls.Add(this.txt_factor_id);
            this.groupPanel2.Controls.Add(this.labelX13);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(14, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1119, 46);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 191;
            // 
            // txt_darsad_maliat
            // 
            this.txt_darsad_maliat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_darsad_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_darsad_maliat.Border.Class = "TextBoxBorder";
            this.txt_darsad_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_darsad_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_darsad_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_darsad_maliat.Location = new System.Drawing.Point(335, 6);
            this.txt_darsad_maliat.Name = "txt_darsad_maliat";
            this.txt_darsad_maliat.PreventEnterBeep = true;
            this.txt_darsad_maliat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_darsad_maliat.Size = new System.Drawing.Size(92, 26);
            this.txt_darsad_maliat.TabIndex = 191;
            // 
            // txt_factor_date
            // 
            this.txt_factor_date.Location = new System.Drawing.Point(14, 8);
            this.txt_factor_date.Name = "txt_factor_date";
            this.txt_factor_date.Size = new System.Drawing.Size(171, 24);
            this.txt_factor_date.TabIndex = 172;
            this.txt_factor_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txt_factor_num
            // 
            this.txt_factor_num.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_factor_num.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_factor_num.Border.Class = "TextBoxBorder";
            this.txt_factor_num.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_factor_num.DisabledBackColor = System.Drawing.Color.White;
            this.txt_factor_num.ForeColor = System.Drawing.Color.Black;
            this.txt_factor_num.Location = new System.Drawing.Point(541, 7);
            this.txt_factor_num.Name = "txt_factor_num";
            this.txt_factor_num.PreventEnterBeep = true;
            this.txt_factor_num.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_factor_num.Size = new System.Drawing.Size(92, 26);
            this.txt_factor_num.TabIndex = 189;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(621, 9);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(90, 23);
            this.labelX2.TabIndex = 190;
            this.labelX2.Text = "شماره فاکتور:";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(169, 9);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(118, 23);
            this.labelX11.TabIndex = 171;
            this.labelX11.Text = "تاریخ صدور فاکتور:";
            // 
            // txt_factor_id
            // 
            this.txt_factor_id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_factor_id.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_factor_id.Border.Class = "TextBoxBorder";
            this.txt_factor_id.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_factor_id.DisabledBackColor = System.Drawing.Color.White;
            this.txt_factor_id.ForeColor = System.Drawing.Color.Black;
            this.txt_factor_id.Location = new System.Drawing.Point(900, 7);
            this.txt_factor_id.Name = "txt_factor_id";
            this.txt_factor_id.PreventEnterBeep = true;
            this.txt_factor_id.ReadOnly = true;
            this.txt_factor_id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_factor_id.Size = new System.Drawing.Size(74, 26);
            this.txt_factor_id.TabIndex = 186;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(361, 9);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(134, 23);
            this.labelX13.TabIndex = 169;
            this.labelX13.Text = "درصد مالیات :";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(947, 9);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(149, 23);
            this.labelX1.TabIndex = 187;
            this.labelX1.Text = "شناسه نرم افزاری فاکتور:";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(538, 431);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.buttonX1.Size = new System.Drawing.Size(222, 31);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Maroon;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 188;
            this.buttonX1.Text = "حذف فاکتور";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click_1);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(112, 431);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX3.Size = new System.Drawing.Size(192, 31);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 12F;
            this.buttonX3.TabIndex = 171;
            this.buttonX3.Text = "چاپ فاکتور";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_1);
            // 
            // grpByGharardad
            // 
            this.grpByGharardad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpByGharardad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpByGharardad.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpByGharardad.Controls.Add(this.txt_mablaghkol);
            this.grpByGharardad.Controls.Add(this.buttonX4);
            this.grpByGharardad.Controls.Add(this.txt_doreh);
            this.grpByGharardad.Controls.Add(this.labelX22);
            this.grpByGharardad.Controls.Add(this.txt_maliat);
            this.grpByGharardad.Controls.Add(this.labelX12);
            this.grpByGharardad.Controls.Add(this.labelX9);
            this.grpByGharardad.Controls.Add(this.txt_majmu);
            this.grpByGharardad.Controls.Add(this.txt_takhfif);
            this.grpByGharardad.Controls.Add(this.txt_sharh);
            this.grpByGharardad.Controls.Add(this.labelX5);
            this.grpByGharardad.Controls.Add(this.labelX3);
            this.grpByGharardad.Controls.Add(this.labelX8);
            this.grpByGharardad.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpByGharardad.Location = new System.Drawing.Point(14, 211);
            this.grpByGharardad.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.grpByGharardad.Name = "grpByGharardad";
            this.grpByGharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpByGharardad.Size = new System.Drawing.Size(1119, 213);
            // 
            // 
            // 
            this.grpByGharardad.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpByGharardad.Style.BackColorGradientAngle = 90;
            this.grpByGharardad.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpByGharardad.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderBottomWidth = 1;
            this.grpByGharardad.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpByGharardad.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderLeftWidth = 1;
            this.grpByGharardad.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderRightWidth = 1;
            this.grpByGharardad.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpByGharardad.Style.BorderTopWidth = 1;
            this.grpByGharardad.Style.CornerDiameter = 4;
            this.grpByGharardad.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpByGharardad.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpByGharardad.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpByGharardad.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpByGharardad.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpByGharardad.TabIndex = 169;
            this.grpByGharardad.Text = "جزئیات فاکتور";
            this.grpByGharardad.Click += new System.EventHandler(this.grpByGharardad_Click);
            // 
            // txt_mablaghkol
            // 
            this.txt_mablaghkol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mablaghkol.Border.Class = "TextBoxBorder";
            this.txt_mablaghkol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mablaghkol.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mablaghkol.ForeColor = System.Drawing.Color.Black;
            this.txt_mablaghkol.Location = new System.Drawing.Point(143, 102);
            this.txt_mablaghkol.Margin = new System.Windows.Forms.Padding(4);
            this.txt_mablaghkol.Name = "txt_mablaghkol";
            this.txt_mablaghkol.Size = new System.Drawing.Size(171, 26);
            this.txt_mablaghkol.TabIndex = 204;
            this.txt_mablaghkol.Text = "0";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(917, 139);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX4.Size = new System.Drawing.Size(170, 31);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 12F;
            this.buttonX4.TabIndex = 202;
            this.buttonX4.Text = "مشاهده قبض مربوطه";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // txt_doreh
            // 
            this.txt_doreh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_doreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_doreh.Border.Class = "TextBoxBorder";
            this.txt_doreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_doreh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_doreh.Location = new System.Drawing.Point(143, 60);
            this.txt_doreh.Name = "txt_doreh";
            this.txt_doreh.PreventEnterBeep = true;
            this.txt_doreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_doreh.Size = new System.Drawing.Size(76, 26);
            this.txt_doreh.TabIndex = 201;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(201, 59);
            this.labelX22.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelX22.Name = "labelX22";
            this.labelX22.Size = new System.Drawing.Size(70, 29);
            this.labelX22.TabIndex = 192;
            this.labelX22.Text = "دوره :";
            // 
            // txt_maliat
            // 
            this.txt_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_maliat.Border.Class = "TextBoxBorder";
            this.txt_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_maliat.Location = new System.Drawing.Point(453, 28);
            this.txt_maliat.Margin = new System.Windows.Forms.Padding(4);
            this.txt_maliat.Name = "txt_maliat";
            this.txt_maliat.ReadOnly = true;
            this.txt_maliat.Size = new System.Drawing.Size(171, 26);
            this.txt_maliat.TabIndex = 187;
            this.txt_maliat.Text = "0";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(330, 102);
            this.labelX12.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(115, 29);
            this.labelX12.TabIndex = 190;
            this.labelX12.Text = "مجموع مبالغ کل :";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(612, 27);
            this.labelX9.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(70, 29);
            this.labelX9.TabIndex = 186;
            this.labelX9.Text = "مالیات :";
            // 
            // txt_majmu
            // 
            this.txt_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_majmu.Border.Class = "TextBoxBorder";
            this.txt_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_majmu.Location = new System.Drawing.Point(758, 28);
            this.txt_majmu.Margin = new System.Windows.Forms.Padding(4);
            this.txt_majmu.Name = "txt_majmu";
            this.txt_majmu.Size = new System.Drawing.Size(171, 26);
            this.txt_majmu.TabIndex = 185;
            this.txt_majmu.Text = "0";
            this.txt_majmu.Leave += new System.EventHandler(this.txt_sharj_mablaghkol_Leave);
            // 
            // txt_takhfif
            // 
            this.txt_takhfif.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_takhfif.Border.Class = "TextBoxBorder";
            this.txt_takhfif.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_takhfif.DisabledBackColor = System.Drawing.Color.White;
            this.txt_takhfif.ForeColor = System.Drawing.Color.Black;
            this.txt_takhfif.Location = new System.Drawing.Point(143, 28);
            this.txt_takhfif.Margin = new System.Windows.Forms.Padding(4);
            this.txt_takhfif.Name = "txt_takhfif";
            this.txt_takhfif.Size = new System.Drawing.Size(171, 26);
            this.txt_takhfif.TabIndex = 184;
            this.txt_takhfif.Text = "0";
            this.txt_takhfif.Leave += new System.EventHandler(this.txt_sharj_takhfif_Leave);
            // 
            // txt_sharh
            // 
            this.txt_sharh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sharh.Border.Class = "TextBoxBorder";
            this.txt_sharh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sharh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sharh.ForeColor = System.Drawing.Color.Black;
            this.txt_sharh.Location = new System.Drawing.Point(398, 60);
            this.txt_sharh.Name = "txt_sharh";
            this.txt_sharh.PreventEnterBeep = true;
            this.txt_sharh.Size = new System.Drawing.Size(531, 26);
            this.txt_sharh.TabIndex = 176;
            this.txt_sharh.Text = "ارائه خدمات شارژ دوره ";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(295, 27);
            this.labelX5.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(70, 29);
            this.labelX5.TabIndex = 174;
            this.labelX5.Text = "تخفیف:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(936, 27);
            this.labelX3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(70, 29);
            this.labelX3.TabIndex = 172;
            this.labelX3.Text = "مجموع مبالغ :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(872, 62);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(134, 23);
            this.labelX8.TabIndex = 177;
            this.labelX8.Text = "شرح خدمات:";
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.crystalReportViewer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.ForeColor = System.Drawing.Color.Black;
            this.crystalReportViewer1.Location = new System.Drawing.Point(728, 432);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ShowCloseButton = false;
            this.crystalReportViewer1.ShowCopyButton = false;
            this.crystalReportViewer1.ShowExportButton = false;
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.ShowLogo = false;
            this.crystalReportViewer1.ShowParameterPanelButton = false;
            this.crystalReportViewer1.ShowRefreshButton = false;
            this.crystalReportViewer1.ShowTextSearchButton = false;
            this.crystalReportViewer1.ShowZoomButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(29, 28);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // FrmShowFactor
            // 
            this.AcceptButton = this.btn_show;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 476);
            this.Controls.Add(this.grp_chap);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.buttonX3);
            this.Controls.Add(this.grpByGharardad);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.btn_show);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmShowFactor";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmShowReport_Load);
            this.grp_chap.ResumeLayout(false);
            this.grp_chap.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.grpByGharardad.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpByGharardad;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX btn_show;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sharh;
        private MoneyTextBox txt_majmu;
        private MoneyTextBox txt_takhfif;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_factor_id;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_factor_num;
        private DevComponents.DotNetBar.LabelX labelX2;
        private MoneyTextBox txt_maliat;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private FarsiLibrary.Win.Controls.FADatePicker txt_factor_date;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_codeEghtesadi;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_fullname;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_shenaseMelli;
        private DevComponents.DotNetBar.LabelX labelX17;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_codeposti;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_address;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_phone;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_moshtarek_gharardad;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_doreh;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private MoneyTextBox txt_mablaghkol;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_darsad_maliat;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_chap;
        private DevComponents.DotNetBar.LabelX lbl_doreh_type;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rd_one;
        private DevComponents.DotNetBar.ButtonX btn_chap_negah;
        private DevComponents.DotNetBar.ButtonX btn_chap_darayi;
        private System.Windows.Forms.ComboBox cmb_doreh_type;
    }
}