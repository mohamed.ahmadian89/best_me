﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Factor
{
    public partial class frm_factor_report_Amari : MyMetroForm
    {
        public frm_factor_report_Amari()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
          
        }

        private void frm_factor_report_Amari_Load(object sender, EventArgs e)
        {
            txt_start_date.SelectedDateTime = txt_end_date.SelectedDateTime = DateTime.Now;

        }

        private void buttonX8_Click(object sender, EventArgs e)
        {

            txt_count_all_sharj.Text =
            txt_sum_all_sharj.Text =
            txt_count_pardakhti_ab.Text =
            txt_sum_pardakhti_ab.Text =
            txt_count_all_ab.Text =
            txt_sum_all_ab.Text =
            txt_count_pardakhti_sharj.Text =
            txt_sum_pardakhti_sharj.Text = "0";
            Classes.ClsMain.ChangeCulture("e");
            try
            {
                DataTable dt_info = Classes.ClsMain.GetDataTable(
                    "select ghabz_type,has_pardakhti, isnull(sum(mablaghkol),0) as majmu,COUNT(id)as tedad from factors " +
                    "where tarikh >= '" + txt_start_date.SelectedDateTime + "' and tarikh <= '" + txt_end_date.SelectedDateTime + "'"

                  + "group by ghabz_type, has_pardakhti");
                Classes.ClsMain.ChangeCulture("f");

                // قبض اب و با پرداخت
                var result = (from p in dt_info.AsEnumerable()
                              where p.Field<bool>("ghabz_type") == false && p.Field<bool>("has_pardakhti") == true
                              select p).ToList();
                if (result.Count != 0)
                {
                    txt_count_pardakhti_ab.Text = result[0]["tedad"].ToString();
                    txt_sum_pardakhti_ab.Text = result[0]["majmu"].ToString();

                }

                //کلیه قبوض آب
                var result2 = (from p in dt_info.AsEnumerable()
                               where p.Field<bool>("ghabz_type") == false
                               select p).ToList();
                if (result2.Count != 0)
                {
                    txt_count_all_ab.Text = result2[0]["tedad"].ToString();
                    txt_sum_all_ab.Text = result2[0]["majmu"].ToString();
                }

                // قبض شارژ و با پرداخت
                var result3 = (from p in dt_info.AsEnumerable()
                               where p.Field<bool>("ghabz_type") == true && p.Field<bool>("has_pardakhti") == true
                               select p).ToList();
                if (result3.Count != 0)
                {
                    txt_count_pardakhti_sharj.Text = result3[0]["tedad"].ToString();
                    txt_sum_pardakhti_sharj.Text = result3[0]["majmu"].ToString();
                }
                //کلیه قبوض شارژ
                var result4 = (from p in dt_info.AsEnumerable()
                               where p.Field<bool>("ghabz_type") == true
                               select p).ToList();

                if (result4.Count != 0)
                {
                    txt_count_all_sharj.Text = result4[0]["tedad"].ToString();
                    txt_sum_all_sharj.Text = result4[0]["majmu"].ToString();
                }

            }
            catch
            {
                Payam.Show("متاسفانه سیستم  دچار خطا شده است ");
            }
        }
    }
}
