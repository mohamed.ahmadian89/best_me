﻿namespace CWMS.Management
{
    partial class frm_factors_report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.factor1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.user_Dataset = new CWMS.Management.user_Dataset();
            this.factor1TableAdapter = new CWMS.Management.user_DatasetTableAdapters.factor1TableAdapter();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.TxtSharjDoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel7 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel8 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.txtEndMablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.TxtStartMablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem4 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel6 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel6 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtFindGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel4 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.txt_end_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_start_date = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem5 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel5 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.cmb_sharj_doreh = new System.Windows.Forms.ComboBox();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.سب = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel10 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.cmb_factor_delte_type = new System.Windows.Forms.ComboBox();
            this.cmb_factor_delete_doreh = new System.Windows.Forms.ComboBox();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.buttonX15 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX14 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem8 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel9 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.buttonX13 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem6 = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel8 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.cmb_ab_doreh = new System.Windows.Forms.ComboBox();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.superTabItem7 = new DevComponents.DotNetBar.SuperTabItem();
            this.grp_content = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX16 = new DevComponents.DotNetBar.ButtonX();
            this.dgvFactor = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majmumabaleghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.takhfifDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maliatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.haspardakhtiDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_maliat = new CWMS.MoneyTextBox();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_mablaghkol = new CWMS.MoneyTextBox();
            this.txt_takhfif = new CWMS.MoneyTextBox();
            this.txt_majmu = new CWMS.MoneyTextBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.lbl_factor_count = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.txtgharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.rd_ab = new System.Windows.Forms.RadioButton();
            this.rd_sharj = new System.Windows.Forms.RadioButton();
            this.rd_ab_sharj = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.factor1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).BeginInit();
            this.superTabControlPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.superTabControlPanel7.SuspendLayout();
            this.groupPanel8.SuspendLayout();
            this.superTabControlPanel6.SuspendLayout();
            this.groupPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel4.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.superTabControlPanel5.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.superTabControlPanel10.SuspendLayout();
            this.superTabControlPanel9.SuspendLayout();
            this.superTabControlPanel8.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.grp_content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactor)).BeginInit();
            this.SuspendLayout();
            // 
            // factor1BindingSource
            // 
            this.factor1BindingSource.DataMember = "factor1";
            this.factor1BindingSource.DataSource = this.user_Dataset;
            // 
            // user_Dataset
            // 
            this.user_Dataset.DataSetName = "user_Dataset";
            this.user_Dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // factor1TableAdapter
            // 
            this.factor1TableAdapter.ClearBeforeFill = true;
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.groupPanel2);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(987, 152);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.superTabItem3;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.TxtSharjDoreh);
            this.groupPanel2.Controls.Add(this.labelX13);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(13, 15);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(955, 114);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 66;
            this.groupPanel2.Text = "جستجوی درآمدها بر اساس دوره های آب و شارژ";
            // 
            // TxtSharjDoreh
            // 
            this.TxtSharjDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtSharjDoreh.Border.Class = "TextBoxBorder";
            this.TxtSharjDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtSharjDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtSharjDoreh.ForeColor = System.Drawing.Color.Black;
            this.TxtSharjDoreh.Location = new System.Drawing.Point(473, 21);
            this.TxtSharjDoreh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TxtSharjDoreh.Name = "TxtSharjDoreh";
            this.TxtSharjDoreh.PreventEnterBeep = true;
            this.TxtSharjDoreh.Size = new System.Drawing.Size(259, 20);
            this.TxtSharjDoreh.TabIndex = 45;
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(720, 9);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX13.Name = "labelX13";
            this.labelX13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX13.Size = new System.Drawing.Size(193, 46);
            this.labelX13.TabIndex = 44;
            this.labelX13.Text = "دوره های شارژ با کدهای :";
            this.labelX13.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(177, 21);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(267, 27);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 40;
            this.buttonX3.Text = "نمایش اطلاعات مربوط به دوره های وارد شده";
            // 
            // superTabItem3
            // 
            this.superTabItem3.AttachedControl = this.superTabControlPanel3;
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "براساس دوره مربوطه";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(987, 152);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "براساس کد قبض";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(987, 152);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "بر اساس بازه زمانی";
            // 
            // superTabControlPanel7
            // 
            this.superTabControlPanel7.Controls.Add(this.groupPanel8);
            this.superTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel7.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.superTabControlPanel7.Name = "superTabControlPanel7";
            this.superTabControlPanel7.Size = new System.Drawing.Size(987, 181);
            this.superTabControlPanel7.TabIndex = 0;
            this.superTabControlPanel7.TabItem = this.superTabItem4;
            // 
            // groupPanel8
            // 
            this.groupPanel8.BackColor = System.Drawing.Color.White;
            this.groupPanel8.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel8.Controls.Add(this.buttonX5);
            this.groupPanel8.Controls.Add(this.txtEndMablagh);
            this.groupPanel8.Controls.Add(this.labelX19);
            this.groupPanel8.Controls.Add(this.TxtStartMablagh);
            this.groupPanel8.Controls.Add(this.labelX20);
            this.groupPanel8.Controls.Add(this.buttonX6);
            this.groupPanel8.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel8.Location = new System.Drawing.Point(13, 16);
            this.groupPanel8.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel8.Name = "groupPanel8";
            this.groupPanel8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel8.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel8.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel8.Style.BackColorGradientAngle = 90;
            this.groupPanel8.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel8.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderBottomWidth = 1;
            this.groupPanel8.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel8.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderLeftWidth = 1;
            this.groupPanel8.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderRightWidth = 1;
            this.groupPanel8.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel8.Style.BorderTopWidth = 1;
            this.groupPanel8.Style.CornerDiameter = 4;
            this.groupPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel8.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel8.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel8.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel8.TabIndex = 66;
            this.groupPanel8.Text = "جستجوی اطلاعات بر اساس مبلغ قبض";
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(12, 49);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX5.Size = new System.Drawing.Size(385, 27);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.Green;
            this.buttonX5.SymbolSize = 9F;
            this.buttonX5.TabIndex = 46;
            this.buttonX5.Text = "نمایش بیشترین مبلغ قبض به تفکیک دوره";
            // 
            // txtEndMablagh
            // 
            this.txtEndMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtEndMablagh.Border.Class = "TextBoxBorder";
            this.txtEndMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtEndMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtEndMablagh.ForeColor = System.Drawing.Color.Black;
            this.txtEndMablagh.Location = new System.Drawing.Point(681, 20);
            this.txtEndMablagh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtEndMablagh.Name = "txtEndMablagh";
            this.txtEndMablagh.PreventEnterBeep = true;
            this.txtEndMablagh.Size = new System.Drawing.Size(137, 20);
            this.txtEndMablagh.TabIndex = 45;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(827, 12);
            this.labelX19.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX19.Name = "labelX19";
            this.labelX19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX19.Size = new System.Drawing.Size(87, 46);
            this.labelX19.TabIndex = 44;
            this.labelX19.Text = "تا مبلغ :";
            this.labelX19.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // TxtStartMablagh
            // 
            this.TxtStartMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtStartMablagh.Border.Class = "TextBoxBorder";
            this.TxtStartMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtStartMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtStartMablagh.ForeColor = System.Drawing.Color.Black;
            this.TxtStartMablagh.Location = new System.Drawing.Point(920, 20);
            this.TxtStartMablagh.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TxtStartMablagh.Name = "TxtStartMablagh";
            this.TxtStartMablagh.PreventEnterBeep = true;
            this.TxtStartMablagh.Size = new System.Drawing.Size(137, 20);
            this.TxtStartMablagh.TabIndex = 43;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(1065, 12);
            this.labelX20.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX20.Name = "labelX20";
            this.labelX20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX20.Size = new System.Drawing.Size(87, 46);
            this.labelX20.TabIndex = 42;
            this.labelX20.Text = "از مبلغ :";
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(475, 22);
            this.buttonX6.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX6.Size = new System.Drawing.Size(189, 25);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Green;
            this.buttonX6.SymbolSize = 9F;
            this.buttonX6.TabIndex = 40;
            this.buttonX6.Text = "نمایش اطلاعات";
            // 
            // superTabItem4
            // 
            this.superTabItem4.AttachedControl = this.superTabControlPanel7;
            this.superTabItem4.GlobalItem = false;
            this.superTabItem4.Name = "superTabItem4";
            this.superTabItem4.Text = "براساس مبلغ قبض";
            // 
            // superTabControlPanel6
            // 
            this.superTabControlPanel6.Controls.Add(this.groupPanel6);
            this.superTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel6.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.superTabControlPanel6.Name = "superTabControlPanel6";
            this.superTabControlPanel6.Size = new System.Drawing.Size(987, 29);
            this.superTabControlPanel6.TabIndex = 0;
            // 
            // groupPanel6
            // 
            this.groupPanel6.BackColor = System.Drawing.Color.White;
            this.groupPanel6.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel6.Controls.Add(this.txtFindGharardad);
            this.groupPanel6.Controls.Add(this.labelX18);
            this.groupPanel6.Controls.Add(this.buttonX4);
            this.groupPanel6.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel6.Location = new System.Drawing.Point(13, 16);
            this.groupPanel6.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel6.Name = "groupPanel6";
            this.groupPanel6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel6.Size = new System.Drawing.Size(1187, 114);
            // 
            // 
            // 
            this.groupPanel6.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel6.Style.BackColorGradientAngle = 90;
            this.groupPanel6.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel6.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderBottomWidth = 1;
            this.groupPanel6.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel6.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderLeftWidth = 1;
            this.groupPanel6.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderRightWidth = 1;
            this.groupPanel6.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel6.Style.BorderTopWidth = 1;
            this.groupPanel6.Style.CornerDiameter = 4;
            this.groupPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel6.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel6.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel6.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel6.TabIndex = 65;
            this.groupPanel6.Text = "جستجوی اطلاعات بر اساس مشترک";
            // 
            // txtFindGharardad
            // 
            this.txtFindGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFindGharardad.Border.Class = "TextBoxBorder";
            this.txtFindGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFindGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtFindGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtFindGharardad.Location = new System.Drawing.Point(861, 20);
            this.txtFindGharardad.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtFindGharardad.Name = "txtFindGharardad";
            this.txtFindGharardad.PreventEnterBeep = true;
            this.txtFindGharardad.Size = new System.Drawing.Size(196, 20);
            this.txtFindGharardad.TabIndex = 43;
            // 
            // labelX18
            // 
            this.labelX18.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX18.ForeColor = System.Drawing.Color.Black;
            this.labelX18.Location = new System.Drawing.Point(1065, 12);
            this.labelX18.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelX18.Name = "labelX18";
            this.labelX18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX18.Size = new System.Drawing.Size(87, 46);
            this.labelX18.TabIndex = 42;
            this.labelX18.Text = "شماره قرارداد :";
            this.labelX18.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(664, 22);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(189, 25);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 40;
            this.buttonX4.Text = "نمایش اطلاعات";
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel4);
            this.superTabControl1.Controls.Add(this.superTabControlPanel5);
            this.superTabControl1.Controls.Add(this.superTabControlPanel9);
            this.superTabControl1.Controls.Add(this.superTabControlPanel8);
            this.superTabControl1.Controls.Add(this.superTabControlPanel10);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(13, 7);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 2;
            this.superTabControl1.Size = new System.Drawing.Size(1043, 118);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 171;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem5,
            this.سب,
            this.superTabItem7,
            this.superTabItem6,
            this.superTabItem8});
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel4
            // 
            this.superTabControlPanel4.Controls.Add(this.groupPanel1);
            this.superTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel4.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel4.Name = "superTabControlPanel4";
            this.superTabControlPanel4.Size = new System.Drawing.Size(1043, 89);
            this.superTabControlPanel4.TabIndex = 0;
            this.superTabControlPanel4.TabItem = this.superTabItem5;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.rd_ab_sharj);
            this.groupPanel1.Controls.Add(this.rd_sharj);
            this.groupPanel1.Controls.Add(this.rd_ab);
            this.groupPanel1.Controls.Add(this.buttonX7);
            this.groupPanel1.Controls.Add(this.txt_end_date);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txt_start_date);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.buttonX8);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(14, 8);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1016, 69);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 66;
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Transparent;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Location = new System.Drawing.Point(199, 34);
            this.buttonX7.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX7.Size = new System.Drawing.Size(257, 27);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.Symbol = "";
            this.buttonX7.SymbolColor = System.Drawing.Color.Green;
            this.buttonX7.SymbolSize = 9F;
            this.buttonX7.TabIndex = 78;
            this.buttonX7.Text = "نمایش فاکتورهای دارای پرداخت";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // txt_end_date
            // 
            this.txt_end_date.Location = new System.Drawing.Point(523, 7);
            this.txt_end_date.Name = "txt_end_date";
            this.txt_end_date.Size = new System.Drawing.Size(148, 24);
            this.txt_end_date.TabIndex = 76;
            this.txt_end_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(641, 3);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(89, 32);
            this.labelX1.TabIndex = 77;
            this.labelX1.Text = "تا تاریخ :";
            // 
            // txt_start_date
            // 
            this.txt_start_date.Location = new System.Drawing.Point(759, 7);
            this.txt_start_date.Name = "txt_start_date";
            this.txt_start_date.Size = new System.Drawing.Size(121, 24);
            this.txt_start_date.TabIndex = 74;
            this.txt_start_date.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(877, 3);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(119, 32);
            this.labelX3.TabIndex = 75;
            this.labelX3.Text = "از تاریخ صدور فاکتور :";
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Transparent;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Location = new System.Drawing.Point(267, 5);
            this.buttonX8.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX8.Size = new System.Drawing.Size(189, 27);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.Green;
            this.buttonX8.SymbolSize = 9F;
            this.buttonX8.TabIndex = 73;
            this.buttonX8.Text = "نمایش کلیه فاکتورها";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // superTabItem5
            // 
            this.superTabItem5.AttachedControl = this.superTabControlPanel4;
            this.superTabItem5.GlobalItem = false;
            this.superTabItem5.Name = "superTabItem5";
            this.superTabItem5.Text = "براساس بازه زمانی";
            // 
            // superTabControlPanel5
            // 
            this.superTabControlPanel5.Controls.Add(this.groupPanel3);
            this.superTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel5.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel5.Name = "superTabControlPanel5";
            this.superTabControlPanel5.Size = new System.Drawing.Size(1043, 89);
            this.superTabControlPanel5.TabIndex = 1;
            this.superTabControlPanel5.TabItem = this.سب;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.labelX14);
            this.groupPanel3.Controls.Add(this.cmb_sharj_doreh);
            this.groupPanel3.Controls.Add(this.buttonX9);
            this.groupPanel3.Controls.Add(this.buttonX10);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(14, 9);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(1016, 69);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 172;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(769, 16);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(62, 27);
            this.labelX14.TabIndex = 199;
            this.labelX14.Text = "دوره شارژ:";
            // 
            // cmb_sharj_doreh
            // 
            this.cmb_sharj_doreh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmb_sharj_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_sharj_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sharj_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_sharj_doreh.FormattingEnabled = true;
            this.cmb_sharj_doreh.Location = new System.Drawing.Point(674, 18);
            this.cmb_sharj_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_sharj_doreh.Name = "cmb_sharj_doreh";
            this.cmb_sharj_doreh.Size = new System.Drawing.Size(87, 28);
            this.cmb_sharj_doreh.TabIndex = 199;
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Transparent;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(187, 19);
            this.buttonX9.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX9.Size = new System.Drawing.Size(257, 27);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.Symbol = "";
            this.buttonX9.SymbolColor = System.Drawing.Color.Green;
            this.buttonX9.SymbolSize = 9F;
            this.buttonX9.TabIndex = 78;
            this.buttonX9.Text = "نمایش فاکتورهای دارای پرداخت";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Transparent;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(462, 19);
            this.buttonX10.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX10.Size = new System.Drawing.Size(189, 27);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 9F;
            this.buttonX10.TabIndex = 73;
            this.buttonX10.Text = "نمایش کلیه فاکتورها";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // سب
            // 
            this.سب.AttachedControl = this.superTabControlPanel5;
            this.سب.GlobalItem = false;
            this.سب.Name = "سب";
            this.سب.Text = "براساس دوره شارژ";
            // 
            // superTabControlPanel10
            // 
            this.superTabControlPanel10.Controls.Add(this.cmb_factor_delte_type);
            this.superTabControlPanel10.Controls.Add(this.cmb_factor_delete_doreh);
            this.superTabControlPanel10.Controls.Add(this.labelX9);
            this.superTabControlPanel10.Controls.Add(this.labelX10);
            this.superTabControlPanel10.Controls.Add(this.buttonX15);
            this.superTabControlPanel10.Controls.Add(this.buttonX14);
            this.superTabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel10.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel10.Name = "superTabControlPanel10";
            this.superTabControlPanel10.Size = new System.Drawing.Size(1043, 89);
            this.superTabControlPanel10.TabIndex = 0;
            this.superTabControlPanel10.TabItem = this.superTabItem8;
            // 
            // cmb_factor_delte_type
            // 
            this.cmb_factor_delte_type.BackColor = System.Drawing.Color.White;
            this.cmb_factor_delte_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_factor_delte_type.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_factor_delte_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_factor_delte_type.FormattingEnabled = true;
            this.cmb_factor_delte_type.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_factor_delte_type.Location = new System.Drawing.Point(304, 11);
            this.cmb_factor_delte_type.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_factor_delte_type.Name = "cmb_factor_delte_type";
            this.cmb_factor_delte_type.Size = new System.Drawing.Size(132, 31);
            this.cmb_factor_delte_type.TabIndex = 214;
            this.cmb_factor_delte_type.SelectedIndexChanged += new System.EventHandler(this.@__SelectedIndexChanged);
            // 
            // cmb_factor_delete_doreh
            // 
            this.cmb_factor_delete_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_factor_delete_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_factor_delete_doreh.Font = new System.Drawing.Font("B Yekan", 11F);
            this.cmb_factor_delete_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_factor_delete_doreh.FormattingEnabled = true;
            this.cmb_factor_delete_doreh.Items.AddRange(new object[] {
            "فاکتورهای پرداختی",
            "کلیه فاکتورها"});
            this.cmb_factor_delete_doreh.Location = new System.Drawing.Point(304, 50);
            this.cmb_factor_delete_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_factor_delete_doreh.Name = "cmb_factor_delete_doreh";
            this.cmb_factor_delete_doreh.Size = new System.Drawing.Size(132, 31);
            this.cmb_factor_delete_doreh.TabIndex = 216;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(417, 3);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(92, 43);
            this.labelX9.TabIndex = 215;
            this.labelX9.Text = "نوع فاکتور :";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(417, 35);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(92, 58);
            this.labelX10.TabIndex = 217;
            this.labelX10.Text = "دوره :";
            // 
            // buttonX15
            // 
            this.buttonX15.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX15.BackColor = System.Drawing.Color.Transparent;
            this.buttonX15.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX15.Location = new System.Drawing.Point(22, 30);
            this.buttonX15.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX15.Name = "buttonX15";
            this.buttonX15.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX15.Size = new System.Drawing.Size(251, 27);
            this.buttonX15.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX15.Symbol = "";
            this.buttonX15.SymbolColor = System.Drawing.Color.Maroon;
            this.buttonX15.SymbolSize = 9F;
            this.buttonX15.TabIndex = 76;
            this.buttonX15.Text = "حذف فاکتورهای مربوطه";
            this.buttonX15.Click += new System.EventHandler(this.buttonX15_Click);
            // 
            // buttonX14
            // 
            this.buttonX14.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX14.BackColor = System.Drawing.Color.Transparent;
            this.buttonX14.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX14.Location = new System.Drawing.Point(724, 30);
            this.buttonX14.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX14.Name = "buttonX14";
            this.buttonX14.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX14.Size = new System.Drawing.Size(251, 27);
            this.buttonX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX14.Symbol = "";
            this.buttonX14.SymbolColor = System.Drawing.Color.Maroon;
            this.buttonX14.SymbolSize = 9F;
            this.buttonX14.TabIndex = 75;
            this.buttonX14.Text = "حذف کلیه فاکتورها";
            this.buttonX14.Click += new System.EventHandler(this.buttonX14_Click);
            // 
            // superTabItem8
            // 
            this.superTabItem8.AttachedControl = this.superTabControlPanel10;
            this.superTabItem8.GlobalItem = false;
            this.superTabItem8.Name = "superTabItem8";
            this.superTabItem8.Text = "سامانه حذف فاکتورها";
            // 
            // superTabControlPanel9
            // 
            this.superTabControlPanel9.Controls.Add(this.buttonX13);
            this.superTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel9.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel9.Name = "superTabControlPanel9";
            this.superTabControlPanel9.Size = new System.Drawing.Size(1043, 89);
            this.superTabControlPanel9.TabIndex = 0;
            this.superTabControlPanel9.TabItem = this.superTabItem6;
            // 
            // buttonX13
            // 
            this.buttonX13.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX13.BackColor = System.Drawing.Color.Transparent;
            this.buttonX13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX13.Location = new System.Drawing.Point(133, 31);
            this.buttonX13.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX13.Name = "buttonX13";
            this.buttonX13.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX13.Size = new System.Drawing.Size(775, 27);
            this.buttonX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX13.Symbol = "";
            this.buttonX13.SymbolColor = System.Drawing.Color.Green;
            this.buttonX13.SymbolSize = 9F;
            this.buttonX13.TabIndex = 74;
            this.buttonX13.Text = "جهت مشاهده سامانه پیشرفته چاپ فاکتورها بر روی این دکمه کلیک نمایید";
            this.buttonX13.Click += new System.EventHandler(this.buttonX13_Click);
            // 
            // superTabItem6
            // 
            this.superTabItem6.AttachedControl = this.superTabControlPanel9;
            this.superTabItem6.GlobalItem = false;
            this.superTabItem6.Name = "superTabItem6";
            this.superTabItem6.Text = "سامانه چاپ فاکتورها";
            // 
            // superTabControlPanel8
            // 
            this.superTabControlPanel8.Controls.Add(this.groupPanel4);
            this.superTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel8.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.superTabControlPanel8.Name = "superTabControlPanel8";
            this.superTabControlPanel8.Size = new System.Drawing.Size(1043, 89);
            this.superTabControlPanel8.TabIndex = 0;
            this.superTabControlPanel8.TabItem = this.superTabItem7;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.labelX12);
            this.groupPanel4.Controls.Add(this.cmb_ab_doreh);
            this.groupPanel4.Controls.Add(this.buttonX11);
            this.groupPanel4.Controls.Add(this.buttonX12);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(12, 10);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(1016, 69);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 173;
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(773, 17);
            this.labelX12.Margin = new System.Windows.Forms.Padding(4);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(62, 27);
            this.labelX12.TabIndex = 199;
            this.labelX12.Text = "دوره آب:";
            // 
            // cmb_ab_doreh
            // 
            this.cmb_ab_doreh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmb_ab_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_ab_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ab_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_ab_doreh.FormattingEnabled = true;
            this.cmb_ab_doreh.Location = new System.Drawing.Point(678, 16);
            this.cmb_ab_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_ab_doreh.Name = "cmb_ab_doreh";
            this.cmb_ab_doreh.Size = new System.Drawing.Size(87, 28);
            this.cmb_ab_doreh.TabIndex = 199;
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.Transparent;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Location = new System.Drawing.Point(194, 17);
            this.buttonX11.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX11.Size = new System.Drawing.Size(257, 27);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.Symbol = "";
            this.buttonX11.SymbolColor = System.Drawing.Color.Green;
            this.buttonX11.SymbolSize = 9F;
            this.buttonX11.TabIndex = 78;
            this.buttonX11.Text = "نمایش فاکتورهای دارای پرداخت";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.BackColor = System.Drawing.Color.Transparent;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Location = new System.Drawing.Point(469, 17);
            this.buttonX12.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX12.Size = new System.Drawing.Size(189, 27);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.Symbol = "";
            this.buttonX12.SymbolColor = System.Drawing.Color.Green;
            this.buttonX12.SymbolSize = 9F;
            this.buttonX12.TabIndex = 73;
            this.buttonX12.Text = "نمایش کلیه فاکتورها";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // superTabItem7
            // 
            this.superTabItem7.AttachedControl = this.superTabControlPanel8;
            this.superTabItem7.GlobalItem = false;
            this.superTabItem7.Name = "superTabItem7";
            this.superTabItem7.Text = "بر اساس دوره آب";
            // 
            // grp_content
            // 
            this.grp_content.BackColor = System.Drawing.Color.White;
            this.grp_content.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_content.Controls.Add(this.buttonX16);
            this.grp_content.Controls.Add(this.dgvFactor);
            this.grp_content.Controls.Add(this.txt_maliat);
            this.grp_content.Controls.Add(this.labelX8);
            this.grp_content.Controls.Add(this.txt_mablaghkol);
            this.grp_content.Controls.Add(this.txt_takhfif);
            this.grp_content.Controls.Add(this.txt_majmu);
            this.grp_content.Controls.Add(this.labelX7);
            this.grp_content.Controls.Add(this.labelX5);
            this.grp_content.Controls.Add(this.labelX6);
            this.grp_content.Controls.Add(this.lbl_factor_count);
            this.grp_content.Controls.Add(this.labelX4);
            this.grp_content.Controls.Add(this.buttonX2);
            this.grp_content.Controls.Add(this.buttonX1);
            this.grp_content.Controls.Add(this.txtgharardad);
            this.grp_content.Controls.Add(this.labelX2);
            this.grp_content.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_content.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grp_content.Location = new System.Drawing.Point(14, 134);
            this.grp_content.Margin = new System.Windows.Forms.Padding(4);
            this.grp_content.Name = "grp_content";
            this.grp_content.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_content.Size = new System.Drawing.Size(1042, 402);
            // 
            // 
            // 
            this.grp_content.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_content.Style.BackColorGradientAngle = 90;
            this.grp_content.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_content.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_content.Style.BorderBottomWidth = 1;
            this.grp_content.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_content.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_content.Style.BorderLeftWidth = 1;
            this.grp_content.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_content.Style.BorderRightWidth = 1;
            this.grp_content.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_content.Style.BorderTopWidth = 1;
            this.grp_content.Style.CornerDiameter = 4;
            this.grp_content.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_content.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_content.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_content.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_content.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_content.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_content.TabIndex = 170;
            // 
            // buttonX16
            // 
            this.buttonX16.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX16.BackColor = System.Drawing.Color.Transparent;
            this.buttonX16.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX16.Location = new System.Drawing.Point(199, 5);
            this.buttonX16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX16.Name = "buttonX16";
            this.buttonX16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX16.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX16.Size = new System.Drawing.Size(175, 29);
            this.buttonX16.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX16.Symbol = "";
            this.buttonX16.SymbolColor = System.Drawing.Color.Green;
            this.buttonX16.SymbolSize = 12F;
            this.buttonX16.TabIndex = 198;
            this.buttonX16.Text = "خروجی اکسل";
            this.buttonX16.Click += new System.EventHandler(this.buttonX16_Click);
            // 
            // dgvFactor
            // 
            this.dgvFactor.AllowUserToAddRows = false;
            this.dgvFactor.AllowUserToDeleteRows = false;
            this.dgvFactor.AllowUserToResizeColumns = false;
            this.dgvFactor.AllowUserToResizeRows = false;
            this.dgvFactor.AutoGenerateColumns = false;
            this.dgvFactor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFactor.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFactor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFactor.ColumnHeadersHeight = 45;
            this.dgvFactor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.factorDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.tarikhh,
            this.majmumabaleghDataGridViewTextBoxColumn,
            this.takhfifDataGridViewTextBoxColumn,
            this.maliatDataGridViewTextBoxColumn,
            this.mablaghkolDataGridViewTextBoxColumn,
            this.haspardakhtiDataGridViewCheckBoxColumn,
            this.gcodeDataGridViewTextBoxColumn});
            this.dgvFactor.DataSource = this.factor1BindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFactor.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFactor.EnableHeadersVisualStyles = false;
            this.dgvFactor.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvFactor.Location = new System.Drawing.Point(18, 40);
            this.dgvFactor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvFactor.Name = "dgvFactor";
            this.dgvFactor.ReadOnly = true;
            this.dgvFactor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFactor.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFactor.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvFactor.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFactor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFactor.Size = new System.Drawing.Size(1005, 315);
            this.dgvFactor.TabIndex = 197;
            this.dgvFactor.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFactor_CellDoubleClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.FillWeight = 70F;
            this.idDataGridViewTextBoxColumn.HeaderText = "شناسه ";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // factorDataGridViewTextBoxColumn
            // 
            this.factorDataGridViewTextBoxColumn.DataPropertyName = "factor";
            this.factorDataGridViewTextBoxColumn.HeaderText = "فاکتور";
            this.factorDataGridViewTextBoxColumn.Name = "factorDataGridViewTextBoxColumn";
            this.factorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhh
            // 
            this.tarikhh.HeaderText = "تاریخ";
            this.tarikhh.Name = "tarikhh";
            this.tarikhh.ReadOnly = true;
            // 
            // majmumabaleghDataGridViewTextBoxColumn
            // 
            this.majmumabaleghDataGridViewTextBoxColumn.DataPropertyName = "majmu_mabalegh";
            this.majmumabaleghDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ";
            this.majmumabaleghDataGridViewTextBoxColumn.Name = "majmumabaleghDataGridViewTextBoxColumn";
            this.majmumabaleghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // takhfifDataGridViewTextBoxColumn
            // 
            this.takhfifDataGridViewTextBoxColumn.DataPropertyName = "takhfif";
            this.takhfifDataGridViewTextBoxColumn.HeaderText = "تخفیف";
            this.takhfifDataGridViewTextBoxColumn.Name = "takhfifDataGridViewTextBoxColumn";
            this.takhfifDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maliatDataGridViewTextBoxColumn
            // 
            this.maliatDataGridViewTextBoxColumn.DataPropertyName = "maliat";
            this.maliatDataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.maliatDataGridViewTextBoxColumn.Name = "maliatDataGridViewTextBoxColumn";
            this.maliatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // haspardakhtiDataGridViewCheckBoxColumn
            // 
            this.haspardakhtiDataGridViewCheckBoxColumn.DataPropertyName = "has_pardakhti";
            this.haspardakhtiDataGridViewCheckBoxColumn.HeaderText = "دارای پرداخت";
            this.haspardakhtiDataGridViewCheckBoxColumn.Name = "haspardakhtiDataGridViewCheckBoxColumn";
            this.haspardakhtiDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // txt_maliat
            // 
            this.txt_maliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_maliat.Border.Class = "TextBoxBorder";
            this.txt_maliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_maliat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_maliat.ForeColor = System.Drawing.Color.Black;
            this.txt_maliat.Location = new System.Drawing.Point(440, 363);
            this.txt_maliat.Margin = new System.Windows.Forms.Padding(4);
            this.txt_maliat.Name = "txt_maliat";
            this.txt_maliat.Size = new System.Drawing.Size(137, 26);
            this.txt_maliat.TabIndex = 190;
            this.txt_maliat.Text = "0";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(554, 363);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(92, 27);
            this.labelX8.TabIndex = 189;
            this.labelX8.Text = "مجموع مالیات :";
            // 
            // txt_mablaghkol
            // 
            this.txt_mablaghkol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mablaghkol.Border.Class = "TextBoxBorder";
            this.txt_mablaghkol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mablaghkol.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mablaghkol.ForeColor = System.Drawing.Color.Black;
            this.txt_mablaghkol.Location = new System.Drawing.Point(18, 363);
            this.txt_mablaghkol.Margin = new System.Windows.Forms.Padding(4);
            this.txt_mablaghkol.Name = "txt_mablaghkol";
            this.txt_mablaghkol.Size = new System.Drawing.Size(175, 26);
            this.txt_mablaghkol.TabIndex = 188;
            this.txt_mablaghkol.Text = "0";
            // 
            // txt_takhfif
            // 
            this.txt_takhfif.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_takhfif.Border.Class = "TextBoxBorder";
            this.txt_takhfif.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_takhfif.DisabledBackColor = System.Drawing.Color.White;
            this.txt_takhfif.ForeColor = System.Drawing.Color.Black;
            this.txt_takhfif.Location = new System.Drawing.Point(255, 363);
            this.txt_takhfif.Margin = new System.Windows.Forms.Padding(4);
            this.txt_takhfif.Name = "txt_takhfif";
            this.txt_takhfif.Size = new System.Drawing.Size(101, 26);
            this.txt_takhfif.TabIndex = 187;
            this.txt_takhfif.Text = "0";
            // 
            // txt_majmu
            // 
            this.txt_majmu.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_majmu.Border.Class = "TextBoxBorder";
            this.txt_majmu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_majmu.DisabledBackColor = System.Drawing.Color.White;
            this.txt_majmu.ForeColor = System.Drawing.Color.Black;
            this.txt_majmu.Location = new System.Drawing.Point(654, 364);
            this.txt_majmu.Margin = new System.Windows.Forms.Padding(4);
            this.txt_majmu.Name = "txt_majmu";
            this.txt_majmu.Size = new System.Drawing.Size(137, 26);
            this.txt_majmu.TabIndex = 186;
            this.txt_majmu.Text = "0";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(155, 362);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(92, 27);
            this.labelX7.TabIndex = 180;
            this.labelX7.Text = "مجموع کل:";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(340, 363);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(92, 27);
            this.labelX5.TabIndex = 179;
            this.labelX5.Text = "مجموع تخفیف :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(768, 363);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(92, 27);
            this.labelX6.TabIndex = 178;
            this.labelX6.Text = "مجموع مبالغ :";
            // 
            // lbl_factor_count
            // 
            this.lbl_factor_count.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_factor_count.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_factor_count.Font = new System.Drawing.Font("B Yekan", 11F);
            this.lbl_factor_count.ForeColor = System.Drawing.Color.Black;
            this.lbl_factor_count.Location = new System.Drawing.Point(868, 362);
            this.lbl_factor_count.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_factor_count.Name = "lbl_factor_count";
            this.lbl_factor_count.Size = new System.Drawing.Size(112, 28);
            this.lbl_factor_count.TabIndex = 173;
            this.lbl_factor_count.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(967, 363);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(56, 27);
            this.labelX4.TabIndex = 176;
            this.labelX4.Text = "تعداد کل:";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(18, 4);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX2.Size = new System.Drawing.Size(175, 29);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 12F;
            this.buttonX2.TabIndex = 175;
            this.buttonX2.Text = "نمایش کلیه فاکتورها";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(844, 6);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.buttonX1.Size = new System.Drawing.Size(38, 29);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 174;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // txtgharardad
            // 
            this.txtgharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtgharardad.Border.Class = "TextBoxBorder";
            this.txtgharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtgharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtgharardad.ForeColor = System.Drawing.Color.Black;
            this.txtgharardad.Location = new System.Drawing.Point(895, 7);
            this.txtgharardad.Name = "txtgharardad";
            this.txtgharardad.PreventEnterBeep = true;
            this.txtgharardad.Size = new System.Drawing.Size(76, 26);
            this.txtgharardad.TabIndex = 173;
            this.txtgharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgharardad_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(957, 7);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(62, 27);
            this.labelX2.TabIndex = 172;
            this.labelX2.Text = "قرارداد:";
            // 
            // rd_ab
            // 
            this.rd_ab.AutoSize = true;
            this.rd_ab.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab.Location = new System.Drawing.Point(944, 34);
            this.rd_ab.Name = "rd_ab";
            this.rd_ab.Size = new System.Drawing.Size(44, 25);
            this.rd_ab.TabIndex = 79;
            this.rd_ab.Text = "آب";
            this.rd_ab.UseVisualStyleBackColor = false;
            // 
            // rd_sharj
            // 
            this.rd_sharj.AutoSize = true;
            this.rd_sharj.BackColor = System.Drawing.Color.Transparent;
            this.rd_sharj.Location = new System.Drawing.Point(877, 34);
            this.rd_sharj.Name = "rd_sharj";
            this.rd_sharj.Size = new System.Drawing.Size(53, 25);
            this.rd_sharj.TabIndex = 80;
            this.rd_sharj.Text = "شارژ";
            this.rd_sharj.UseVisualStyleBackColor = false;
            // 
            // rd_ab_sharj
            // 
            this.rd_ab_sharj.AutoSize = true;
            this.rd_ab_sharj.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab_sharj.Checked = true;
            this.rd_ab_sharj.Location = new System.Drawing.Point(788, 34);
            this.rd_ab_sharj.Name = "rd_ab_sharj";
            this.rd_ab_sharj.Size = new System.Drawing.Size(81, 25);
            this.rd_ab_sharj.TabIndex = 81;
            this.rd_ab_sharj.TabStop = true;
            this.rd_ab_sharj.Text = "آب و شارژ";
            this.rd_ab_sharj.UseVisualStyleBackColor = false;
            // 
            // frm_factors_report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 548);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.grp_content);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "frm_factors_report";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_factors_report_Load);
            ((System.ComponentModel.ISupportInitialize)(this.factor1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_Dataset)).EndInit();
            this.superTabControlPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.superTabControlPanel7.ResumeLayout(false);
            this.groupPanel8.ResumeLayout(false);
            this.superTabControlPanel6.ResumeLayout(false);
            this.groupPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel4.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.superTabControlPanel5.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.superTabControlPanel10.ResumeLayout(false);
            this.superTabControlPanel9.ResumeLayout(false);
            this.superTabControlPanel8.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.grp_content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_content;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvFactor;
        private MoneyTextBox txt_maliat;
        private DevComponents.DotNetBar.LabelX labelX8;
        private MoneyTextBox txt_mablaghkol;
        private MoneyTextBox txt_majmu;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX lbl_factor_count;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtgharardad;
        private DevComponents.DotNetBar.LabelX labelX2;
        private user_Dataset user_Dataset;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjmajmumabaleghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjtakhfifDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abmajmumabaleghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abtakhfifDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjdcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abdcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjsharhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn absharhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjmaliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abmaliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjmablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abmablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharjgcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn abgcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource factor1BindingSource;
        private user_DatasetTableAdapters.factor1TableAdapter factor1TableAdapter;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtSharjDoreh;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel7;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel8;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtEndMablagh;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.Controls.TextBoxX TxtStartMablagh;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.SuperTabItem superTabItem4;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel6;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFindGharardad;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.SuperTabItem سب;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel8;
        private DevComponents.DotNetBar.SuperTabItem superTabItem7;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end_date;
        private DevComponents.DotNetBar.LabelX labelX1;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start_date;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.SuperTabItem superTabItem5;
        private MoneyTextBox txt_takhfif;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private System.Windows.Forms.ComboBox cmb_ab_doreh;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private System.Windows.Forms.ComboBox cmb_sharj_doreh;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel9;
        private DevComponents.DotNetBar.ButtonX buttonX13;
        private DevComponents.DotNetBar.SuperTabItem superTabItem6;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel10;
        private DevComponents.DotNetBar.ButtonX buttonX14;
        private DevComponents.DotNetBar.SuperTabItem superTabItem8;
        private DevComponents.DotNetBar.ButtonX buttonX15;
        private System.Windows.Forms.ComboBox cmb_factor_delte_type;
        private System.Windows.Forms.ComboBox cmb_factor_delete_doreh;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.ButtonX buttonX16;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn factorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tarikhh;
        private System.Windows.Forms.DataGridViewTextBoxColumn majmumabaleghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn takhfifDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn haspardakhtiDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.RadioButton rd_ab_sharj;
        private System.Windows.Forms.RadioButton rd_sharj;
        private System.Windows.Forms.RadioButton rd_ab;
    }
}