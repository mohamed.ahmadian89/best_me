﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Management
{
    public partial class frm_factors_report : MyMetroForm
    {

        bool Factor_search_type_is_pardkhti = false;
        public frm_factors_report()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            cmb_sharj_doreh.DataSource = Classes.ClsMain.GetDataTable("select dcode from sharj_doreh order by dcode desc");
            cmb_sharj_doreh.DisplayMember = "dcode";
            cmb_sharj_doreh.ValueMember = "dcode";

            cmb_ab_doreh.DataSource = Classes.ClsMain.GetDataTable("select dcode from ab_doreh order by dcode desc");
            cmb_ab_doreh.DisplayMember = "dcode";
            cmb_ab_doreh.ValueMember = "dcode";
            cmb_factor_delte_type.SelectedIndex = 0;
            cmb_factor_delete_doreh.DataSource = cmb_ab_doreh.DataSource;
            cmb_factor_delete_doreh.DisplayMember = "dcode";
            cmb_factor_delete_doreh.ValueMember = "dcode";
        }

        private void frm_factors_report_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'user_Dataset.factor1' table. You can move, or remove it, as needed.
            DateTime time_now = DateTime.Now;
            txt_start_date.SelectedDateTime = txt_end_date.SelectedDateTime = new DateTime(time_now.Year, time_now.Month, time_now.Day, 0, 0, 0);
            txt_end_date.SelectedDateTime = new DateTime(time_now.Year, time_now.Month, time_now.Day, 23, 59, 59);

        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            rd_ab_sharj.Checked = true;
            factor1TableAdapter.Fill_doreh(user_Dataset.factor1, Convert.ToInt32(cmb_sharj_doreh.SelectedValue), true);
            show_report();
            Factor_search_type_is_pardkhti = false;

        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            rd_ab_sharj.Checked = true;
            factor1TableAdapter.Fill_pardakhti_in_doreh(user_Dataset.factor1, Convert.ToInt32(cmb_sharj_doreh.SelectedValue), true);
            Factor_search_type_is_pardkhti = true;

            show_report_byPardakhti();
        }

        private void buttonX12_Click(object sender, EventArgs e)
        {

            rd_ab_sharj.Checked = true;

            factor1TableAdapter.Fill_doreh(user_Dataset.factor1, Convert.ToInt32(cmb_ab_doreh.SelectedValue), false);
            show_report();
            Factor_search_type_is_pardkhti = false;

        }


        void show_report_byPardakhti_for_gharardad()
        {
            Changedatetime(dgvFactor);
            try
            {
                int gharardad = Convert.ToInt32(txtgharardad.Text);
                if (rd_ab_sharj.Checked)
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
                else
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
            }
            catch (Exception)
            {

                txt_majmu.Text = txt_takhfif.Text = txt_mablaghkol.Text = "0";
                lbl_factor_count.Text = "0";

            }



        }

        void show_report_byPardakhti()
        {
            Changedatetime(dgvFactor);


            try
            {
                if (rd_ab_sharj.Checked)
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
                else
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.has_pardakhti == true && p.ghabz_type == rd_sharj.Checked).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }

            }
            catch (Exception)
            {

                txt_majmu.Text = txt_takhfif.Text = txt_mablaghkol.Text = "0";
                lbl_factor_count.Text = "0";

            }




        }



        void show_report_gharardad()
        {
            Changedatetime(dgvFactor);
            try
            {
                int gharardad = Convert.ToInt32(txtgharardad.Text);
                if (rd_ab_sharj.Checked)
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
                else
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.ghabz_type == rd_sharj.Checked).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.ghabz_type == rd_sharj.Checked).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.ghabz_type == rd_sharj.Checked).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p => p.gharardad == gharardad && p.ghabz_type == rd_sharj.Checked).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
            }
            catch (Exception)
            {

                txt_majmu.Text = txt_takhfif.Text = txt_mablaghkol.Text = txt_maliat.Text = "0";
                lbl_factor_count.Text = "0";

            }



        }

        void show_report()
        {
            Changedatetime(dgvFactor);


            try
            {
                if (rd_ab_sharj.Checked)
                {
                    txt_majmu.Text = user_Dataset.factor1.Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                }
                else
                {
                    txt_majmu.Text = user_Dataset.factor1.Where(p=> p.ghabz_type == rd_sharj.Checked).Sum(p => p.majmu_mabalegh).ToString();
                    txt_maliat.Text = user_Dataset.factor1.Where(p=>p.ghabz_type == rd_sharj.Checked).Sum(p => p.maliat).ToString();
                    txt_takhfif.Text = user_Dataset.factor1.Where(p=>p.ghabz_type == rd_sharj.Checked).Sum(p => p.takhfif).ToString();
                    txt_mablaghkol.Text = user_Dataset.factor1.Where(p=>p.ghabz_type == rd_sharj.Checked).Sum(p => p.mablaghkol).ToString();
                    lbl_factor_count.Text = user_Dataset.factor1.Rows.Count.ToString();
                    
                }

            }
            catch (Exception)
            {

                txt_majmu.Text = txt_takhfif.Text = txt_mablaghkol.Text = txt_maliat.Text = "0";
                lbl_factor_count.Text = "0";

            }




        }


        private void buttonX11_Click(object sender, EventArgs e)
        {
            rd_ab_sharj.Checked = true;

            factor1TableAdapter.Fill_pardakhti_in_doreh(user_Dataset.factor1, Convert.ToInt32(cmb_ab_doreh.SelectedValue), false);
            Factor_search_type_is_pardkhti = true;

            show_report_byPardakhti();
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            DateTime start = Convert.ToDateTime(txt_start_date.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end_date.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);
            if (rd_ab_sharj.Checked)
            {
                factor1TableAdapter.Fill_baze(user_Dataset.factor1, start,
                    finish);
                show_report();
            }
            else if (rd_ab.Checked)
            {
                factor1TableAdapter.FillBazeFor(user_Dataset.factor1, start, finish, false);
                show_report_gharardad();
            }
            else
            {
                factor1TableAdapter.FillBazeFor(user_Dataset.factor1, start, finish, true);
                show_report_gharardad();
            }

            Factor_search_type_is_pardkhti = false;
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            DateTime start = Convert.ToDateTime(txt_start_date.SelectedDateTime);
            start = start.AddHours(-start.Hour);

            DateTime finish = Convert.ToDateTime(txt_end_date.SelectedDateTime);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23, 59, 59);
            if (rd_ab_sharj.Checked)
            {
                factor1TableAdapter.Fill_pardakhti_in_baze(user_Dataset.factor1,
                    start, finish);
                show_report_byPardakhti();

            }
            else if (rd_ab.Checked)
            {
                factor1TableAdapter.FillPardakhti_Baze_for(user_Dataset.factor1,
                    start, finish, false);
                show_report_byPardakhti_for_gharardad();

            }
            else
            {
                factor1TableAdapter.FillPardakhti_Baze_for(user_Dataset.factor1,
                    start, finish, true);
                show_report_byPardakhti_for_gharardad();
            }
            Factor_search_type_is_pardkhti = true;
        }

        private void dgvFactor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                new FrmShowFactor(dgvFactor.Rows[e.RowIndex].Cells[0].Value.ToString()).ShowDialog();

            }
        }

        private void buttonX13_Click(object sender, EventArgs e)
        {
            new Factor.Frm_chap_factor().ShowDialog();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (txtgharardad.Text.Trim() == "")
            {
                factor1BindingSource.RemoveFilter();
            }
            else
            {
                factor1BindingSource.Filter = "gharardad=" + txtgharardad.Text;

            }
            if (Factor_search_type_is_pardkhti == true)
            {
                show_report_byPardakhti_for_gharardad();
            }
            else
                show_report_gharardad();
        }

        private void txtgharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonX1.PerformClick();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            factor1BindingSource.RemoveFilter();

        }

        private void buttonX14_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("آیا با حذف کلیه فاکتورها موافق هستید") == DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from factors;delete from factor_history");
                Payam.Show("کلیه فاکتورها با موفقیت از سیستم حذف شد");
            }
        }

        private void __SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_factor_delte_type.SelectedIndex == 0)
            {
                cmb_factor_delete_doreh.DataSource = cmb_ab_doreh.DataSource;

            }
            else
            {
                cmb_factor_delete_doreh.DataSource = cmb_sharj_doreh.DataSource;
            }
            cmb_factor_delete_doreh.DisplayMember = "dcode";
            cmb_factor_delete_doreh.ValueMember = "dcode";



        }

        private void buttonX15_Click(object sender, EventArgs e)
        {
            int doreh = 0;
            try
            {
                doreh = Convert.ToInt32(cmb_factor_delete_doreh.SelectedValue.ToString());
            }
            catch (Exception)
            {
                Payam.Show("لطفا دوره مربوطه را انتخاب نمایید");

            }

            if (Cpayam.Show("آیا با حذف فاکتورهای مربوطه  موافق هستید") == DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from factors where dcode=" + cmb_factor_delete_doreh.SelectedValue.ToString()
                    + " and ghabz_type=" + cmb_factor_delte_type.SelectedIndex
                    );
                Payam.Show("کلیه فاکتورهای مربوطه با موفقیت از سیستم حذف شد");
            }
        }

        void Changedatetime(System.Windows.Forms.DataGridView dv)
        {

            for (int i = 0; i < user_Dataset.factor1.Rows.Count; i++)
            {
                try
                {
                    dv.Rows[i].Cells[4].Value = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(
                        user_Dataset.factor1.Rows[i]["tarikh"])).ToString("d");
                }
                catch
                {
                    ;
                }

            }
        }

        private void copyAlltoClipboard()
        {

            dgvFactor.SelectAll();
            DataObject dataObj = dgvFactor.GetClipboardContent();

            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }
        private void buttonX16_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("f");
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            Classes.ClsMain.ChangeCulture("e");

        }

       
    }
}
