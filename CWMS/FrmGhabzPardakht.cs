﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Diagnostics;
using CrystalDecisions.Windows.Forms;

namespace CWMS.Sharj
{
    public partial class FrmGhabzPardakht : MyMetroForm
    {
        bool Is_ab_fish = false;
        string GhabzCode = "", DorehCode = "", gharardad;
        long bestankari_from_last_doreh = 0;


        public FrmGhabzPardakht(bool IS_this_for_Ab, string GCode, string Dcode, string gharardadtemp, string mablaghkol,
            string mablaghkolhorrof, long bestankari_from_last_doreh_temp, string TarikhSoddore = "")
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            Is_ab_fish = IS_this_for_Ab;

            this.ghabz_pardakhtTableAdapter1.FillByGhabzCode(this.mainAbDataset1.ghabz_pardakht, Convert.ToInt32(GCode));
            txtGhabzCodeMain.Text = txt_ghabz_code.Text = GhabzCode = GCode;
            txtDorehMain.Text = txtdoreh.Text = DorehCode = Dcode;
            txtGharardadMain.Text = txtGharardad.Text = gharardad = gharardadtemp;
            txtMablaghKol.Text = mablaghkol;
            txtMablaghHoroof.Text = mablaghkolhorrof;

            txtTarikhSoddor.Text = TarikhSoddore;

            bestankari_from_last_doreh = bestankari_from_last_doreh_temp;
            txtBestankarFromLAstDoreh.Text = bestankari_from_last_doreh.ToString();
            CalcPardakhtiha(true);


        }
        private void FrmGhabzPardakht_Load(object sender, EventArgs e)
        {
            CheckSumOfPardakhti();

        }

        private void cmbahveh_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblSarresid.Visible = txtSarresid.Visible = false;
            if (cmbahveh.SelectedItem == null)
                return;
            if (cmbahveh.SelectedItem.ToString() == "دستی" )
            {
                lblNehveh.Text = "";
                txtfish.Visible = false;
            }
            else if (cmbahveh.SelectedItem.ToString() == "پوز")
            {
                lblNehveh.Text = "شماره ارجاع";
                txtfish.Visible = true;
            }
            else if (cmbahveh.SelectedItem.ToString() == "چک")
            {
                lblNehveh.Text = "سریال چک";
                LblSarresid.Visible = txtSarresid.Visible = txtfish.Visible = true;
            }
            else if (cmbahveh.SelectedItem.ToString() == "اینترنت")
            {
                lblNehveh.Text = "کد رهگیری";
                txtfish.Visible = true;
            }
            else if (cmbahveh.SelectedItem.ToString() == "فیش" || cmbahveh.SelectedItem.ToString() == "بانک")
            {
                lblNehveh.Text = "شماره فیش";
                txtfish.Visible = true;
            }

        }


        bool Adding = false;
        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (!Adding)
            {

                Adding = true;
                ghabzpardakhtBindingSource.AddNew();
                TxtTarikhVosool.SelectedDateTime = txtTarikhSAbt.SelectedDateTime = DateTime.Now;
                txt_ghabz_code.Text = GhabzCode;
                txtdoreh.Text = DorehCode;
                txtGharardad.Text = gharardad;
                ch_vaziat.Checked = false;
                ch_vaziat.Checked = true;
                Is_ab_checkBox.Checked = Is_ab_fish;
                cmbahveh.SelectedIndex = 0;
                cmbahveh.Focus();
            }
        }

        private void FrmGhabzPardakht_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.MainFrm.Update_Ab_sharj_panel();


        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (cmbahveh.SelectedItem == null || cmbahveh.SelectedIndex == -1)
            {
                Payam.Show("لطفا نحوه پرداخت را مشخص کنید");
                cmbahveh.Focus();
                return;
            }
            if (cmbahveh.SelectedItem.ToString() == "چک")
            {

                if (txtSarresid.SelectedDateTime == null)
                {
                    Payam.Show("لظفا تاریخ سررسید چک را به درستی وارد کنید");
                    return;
                }

                //TxtTarikhVosool.SelectedDateTime = null;

            }



            try
            {
                Convert.ToInt64(txtPardakht.Text);
            }
            catch (Exception)
            {
                Payam.Show("لطفا مبلغ پرداخت را به صورت صحیح وارد نمایید");
                txtPardakht.Clear();
                txtPardakht.Focus();
                return;
            }


            Adding = false;
            ghabzpardakhtBindingSource.EndEdit();
            ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            CalcPardakhtiha();
            CheckSumOfPardakhti();
            //if (Adding)
            //{
            //    Adding = false;
            //    long AllPardakht = CheckSumOfPardakhti();
            //    if (AllPardakht > Convert.ToInt64(txtMablaghKol.Text))
            //    {
            //        if (Cpayam.Show("مجموع مبلغ پرداختی " + (AllPardakht - Convert.ToInt64(txtMablaghKol.Text)).ToString() + " ریال بیشتر از مبلغ کل می باشد . ایا موافقید؟ ") == DialogResult.Yes)
            //        {
            //            ghabzpardakhtBindingSource.EndEdit();
            //            ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            //            CalcPardakhtiha();
            //        }
            //    }
            //    else
            //    {
            //        ghabzpardakhtBindingSource.EndEdit();
            //        ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            //        CalcPardakhtiha();
            //    }
            //}
            //else
            //{
            //    ghabzpardakhtBindingSource.EndEdit();
            //    ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            //    CalcPardakhtiha();
            //    //string Pardakhtcode = dgvPardakht.SelectedRows[0].Cells[0].Value.ToString();
            //    //long pardakht = 0;
            //    //for (int i = 0; i < mainAbDataset1.ghabz_pardakht.Rows.Count; i++)
            //    //{
            //    //    if (mainAbDataset1.ghabz_pardakht.Rows[i]["id"].ToString() != Pardakhtcode)
            //    //        pardakht += Convert.ToInt64(mainAbDataset1.ghabz_pardakht.Rows[i]["mablagh"]);
            //    //}



            //    //if (pardakht + Convert.ToInt64(txtPardakht.Text) > Convert.ToInt64(txtMablaghKol.Text))
            //    //{
            //    //    if (Cpayam.Show("مجموع مبلغ پرداختی " + (pardakht + Convert.ToInt64(txtPardakht.Text) - Convert.ToInt64(txtMablaghKol.Text)).ToString()
            //    //        + " ریال بیشتر از مبلغ کل می باشد . ایا موافقید؟ ") == DialogResult.Yes)
            //    //    {
            //    //        ghabzpardakhtBindingSource.EndEdit();
            //    //        ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            //    //        CalcPardakhtiha();

            //    //    }
            //    //}
            //    //else
            //    //{
            //    //    ghabzpardakhtBindingSource.EndEdit();
            //    //    ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
            //    //    CalcPardakhtiha();

            //    //}
            //}

        }

        void CheckSumOfPardakhti()
        {
            decimal pardakht = 0, bs_from_last = 0;
            try
            {
                bs_from_last = Convert.ToDecimal(txtBestankarFromLAstDoreh.Text);
            }
            catch (Exception)
            {
                bs_from_last = 0;
            }
            for (int i = 0; i < mainAbDataset1.ghabz_pardakht.Rows.Count; i++)
            {
                pardakht += Convert.ToDecimal(mainAbDataset1.ghabz_pardakht.Rows[i]["mablagh"]);
            }
            txtPardakhti.Text=(pardakht).ToString();
        }

        void CalcPardakhtiha(bool FirstLoad = false)
        {


            Classes.clsBedehkar_Bestankar cls = new
                Classes.clsBedehkar_Bestankar(Convert.ToInt64(txtMablaghKol.Text), txtGhabzCodeMain.Text, bestankari_from_last_doreh, "GetIT", false);

            cls.Check_Bedehkar_Bestankar_Status();
            txtBaghimadeh.Text = cls.mandeh.ToString();
            txtBestankar.Text = cls.bestankar.ToString();
          

            if (FirstLoad)
                return;

            Classes.clsPardakhti.update_ghabzState_afterPardakht(cls);



        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (ghabzpardakhtBindingSource.Count > 0)
            {
                if (dgvPardakht.SelectedRows.Count != 0)
                {
                    if (Cpayam.Show("آیا مطمئن هستید که میخواهید این پرداختی را حذف کنید؟") == System.Windows.Forms.DialogResult.Yes)
                    {
                        ghabzpardakhtBindingSource.RemoveCurrent();
                        ghabzpardakhtBindingSource.EndEdit();
                        ghabz_pardakhtTableAdapter1.Update(mainAbDataset1.ghabz_pardakht);
                        CalcPardakhtiha();
                        CheckSumOfPardakhti();

                    }
                }
                else
                {
                    Payam.Show("لطفا یکی از موارد پرداختی را انتخاب کنید");
                }
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (Is_ab_fish)
            {
                CrystalReportViewer repVUer = new CrystalReportViewer();
                CrysReports.Ab_doreh rpt = new CrysReports.Ab_doreh();
                Classes.clsAb.ChapAbDoreh(txtDorehMain.Text,
                        repVUer,
                        rpt, "print", txtGharardad.Text
                        );
            }
            else
            {

                CrystalReportViewer repVUer = new CrystalReportViewer();
                CrysReports.Sharj_doreh rpt = new CrysReports.Sharj_doreh();
                Classes.clsSharj.ChapSharjDoreh(txtDorehMain.Text,
                        repVUer,
                        rpt, "print", txtGharardad.Text
                        );
            }

        }

        private void TxtTarikhVosool_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmbahveh.Focus();



        }

        private void cmbahveh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtPardakht.Focus();

        }

        private void txtPardakht_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtfish.Focus();
        }

        private void txtfish_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
                txtbank.Focus();
        }



        private void txtbank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtTozihat.Focus();
        }

        private void txtTozihat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_save.PerformClick();
        }

        private void txtPardakht_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtfish.Visible == true)
                    txtfish.Focus();
                else
                    txtbank.Focus();
            }
        }

        private void txtPardakht_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblMablaghHorrof.Text = Classes.clsNumber.Number_to_Str(txtPardakht.Text) + " ریال " + " معادل " + Classes.clsNumber.Number_to_Str(txtPardakht.Text.Trim().Substring(0, txtPardakht.Text.Length - 1)) + " تومان ";

            }
            catch (Exception)
            {

                lblMablaghHorrof.Text = "";
            }
        }

        private void FrmGhabzPardakht_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}