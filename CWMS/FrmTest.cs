﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CWMS
{
    public partial class FrmTest : Form
    {
        public FrmTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //Classes.clsReport.AllPardakhtForAll(null, null, "شارژ",pardakht_allForAllMoshtarek1);
            //crystalReportViewer1.ReportSource = pardakht_allForAllMoshtarek1;
            //crystalReportViewer1.PrintReport();
        }

        private void FrmTest_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'abDataset.Jarime_masraf_mazad' table. You can move, or remove it, as needed.
            this.jarime_masraf_mazadTableAdapter.Fill(this.abDataset.Jarime_masraf_mazad);
            // TODO: This line of code loads data into the 'cWMSDataSet.admin_user_access_type' table. You can move, or remove it, as needed.
            this.admin_user_access_typeTableAdapter.Fill(this.cWMSDataSet.admin_user_access_type);

        }

        private void button1_Click_1(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtfind_TextChanged(object sender, EventArgs e)
        {
            adminuseraccesstypeBindingSource.Filter = "action like '%" + txtfind.Text + "%'";
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            DgvResult.Rows.Clear();
            decimal masraf_mojaz = 0, masraf_gheir_mojaz = 0, masraf_kol = 0;
            masraf_mojaz = Convert.ToDecimal(txtmasraf_mojaz.Text);
            masraf_gheir_mojaz = Convert.ToDecimal(txtmasraf_gheir_mojaz.Text);
            masraf_kol = Convert.ToDecimal(txtmasraf_kol.Text);


            decimal FirstDarsad = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;

            decimal Tarefe = 1;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                Payam.Show("هزینه مصرف غیر مجاز:" + (FirstZarib * Tarefe * masraf_gheir_mojaz));
                DgvResult.Rows.Add(FirstDarsad, (FirstDarsad * masraf_mojaz / 100), FirstZarib, masraf_gheir_mojaz, (FirstZarib * Tarefe * masraf_gheir_mojaz));
                return;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < abDataset.Jarime_masraf_mazad.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {

                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    DgvResult.Rows.Add(CurrentDarsad + "/100"  ,(CurrentDarsad / 100 * masraf_mojaz), CurrentZarib, ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad), ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe);

                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;

                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        DgvResult.Rows.Add(CurrentDarsad,(masraf_gheir_mojaz - lastMasrafDarsad), zarib, (masraf_gheir_mojaz - lastMasrafDarsad), (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib);
                        break;
                    }
                }
               
            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
                DgvResult.Rows.Add(Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["ta_darsad"]), (masraf_gheir_mojaz - Last),
                Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]), (masraf_gheir_mojaz - Last), (masraf_gheir_mojaz - Last) * Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe

                );
            }

            lbltotal.Text = sum.ToString();

            Payam.Show(PelekaniTarefe(masraf_mojaz, masraf_gheir_mojaz, masraf_kol, (DataTable)abDataset.Jarime_masraf_mazad, Tarefe).ToString());
        }

         decimal PelekaniTarefe(decimal masraf_mojaz, decimal masraf_gheir_mojaz, decimal masraf_kol, DataTable dtinfo, decimal Tarefe)
        {

            decimal FirstDarsad = Convert.ToDecimal(dtinfo.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(dtinfo.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                sum = (FirstZarib * Tarefe * masraf_gheir_mojaz);
                return sum;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < dtinfo.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(dtinfo.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(dtinfo.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {
                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;
                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(dtinfo.Rows[dtinfo.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
            }
            return sum;
        }



        private void txtmasraf_gheir_mojaz_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtmasraf_kol.Text = (Convert.ToInt32(txtmasraf_gheir_mojaz.Text) + Convert.ToInt32(txtmasraf_mojaz.Text)).ToString();
            }
            catch (Exception)
            {

                txtmasraf_kol.Text = "0";
            }
        }

        private void txtmasraf_mojaz_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtmasraf_kol.Text = (Convert.ToInt32(txtmasraf_gheir_mojaz.Text) + Convert.ToInt32(txtmasraf_mojaz.Text)).ToString();
            }
            catch (Exception)
            {

                txtmasraf_kol.Text = "0";
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            decimal k = Convert.ToDecimal(textBox1.Text);
        }

        private void txtmasraf_kol_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Days_between_tarikh_ha(DateTime.Now.AddDays(1000), DateTime.Now).ToString());
        }
        public static int Days_between_tarikh_ha(DateTime End, DateTime Start)
        {
            System.Globalization.PersianCalendar persia = new System.Globalization.PersianCalendar();



            DateTime StartDateTime = persia.ToDateTime(Start.Year, Start.Month, Start.Day, 0, 0, 0, 0);
            DateTime EndDateTime = persia.ToDateTime(End.Year, End.Month, End.Day, 0, 0, 0, 0);

            TimeSpan Difference = EndDateTime - StartDateTime;

            return Difference.Days;


        }

        private void button4_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

            string[] tarikh = fa_date.Text.Split('/');
            System.Globalization.PersianCalendar persia = new System.Globalization.PersianCalendar();

            DateTime End = DateTime.Now;

            DateTime StartDateTime = persia.ToDateTime(
                Convert.ToInt32(tarikh[0])
                , Convert.ToInt32(tarikh[1]), Convert.ToInt32(tarikh[2]), 0, 0, 0, 0);
            DateTime EndDateTime = persia.ToDateTime(End.Year, End.Month, End.Day, 0, 0, 0, 0);

            TimeSpan Difference = EndDateTime - StartDateTime;
            Payam.Show(Difference.Days.ToString());
            Classes.ClsMain.ChangeCulture("f");

        }

        private void button5_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(maskedTextBox1.Text);
            MessageBox.Show(number.ToString());
        }
    }
}
