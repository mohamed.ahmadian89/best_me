﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS
{
    public partial class frm_moshtarek_emza : MyMetroForm
    {
        public frm_moshtarek_emza(int gharardad)
        {
            InitializeComponent();
            moshtarekinTableAdapter.FillByGharardad(mainDataSest.moshtarekin, gharardad);
        }

        private void frm_moshtarek_emza_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.moshtarekin' table. You can move, or remove it, as needed.

        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog()==DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                moshtarekinBindingSource.EndEdit();
                moshtarekinTableAdapter.Update(mainDataSest.moshtarekin);
            }
        }
    }
}
