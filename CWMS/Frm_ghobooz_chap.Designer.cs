﻿namespace CWMS
{
    partial class Frm_ghobooz_chap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.grpinfo = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rd_all = new System.Windows.Forms.RadioButton();
            this.rd_print_ghabz_which_not_printed = new System.Windows.Forms.RadioButton();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rd_address = new System.Windows.Forms.RadioButton();
            this.rd_gharardad = new System.Windows.Forms.RadioButton();
            this.ch_faal_company = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.TxtEnd = new DevComponents.Editors.IntegerInput();
            this.TxtStart = new DevComponents.Editors.IntegerInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cmb_ghabz_doreh = new System.Windows.Forms.ComboBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.cmb_ghabz_type = new System.Windows.Forms.ComboBox();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.grp_doreh_for_esf = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.ch_have_sharj_ghabz = new System.Windows.Forms.CheckBox();
            this.ch_have_ab_gahbz = new System.Windows.Forms.CheckBox();
            this.cmb_sharj = new System.Windows.Forms.ComboBox();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.cmb_ab = new System.Windows.Forms.ComboBox();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.grpinfo.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStart)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.grp_doreh_for_esf.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // grpinfo
            // 
            this.grpinfo.BackColor = System.Drawing.Color.White;
            this.grpinfo.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpinfo.Controls.Add(this.groupPanel1);
            this.grpinfo.Controls.Add(this.groupPanel3);
            this.grpinfo.Controls.Add(this.ch_faal_company);
            this.grpinfo.Controls.Add(this.progressBar1);
            this.grpinfo.Controls.Add(this.btnPrint);
            this.grpinfo.Controls.Add(this.TxtEnd);
            this.grpinfo.Controls.Add(this.TxtStart);
            this.grpinfo.Controls.Add(this.labelX1);
            this.grpinfo.Controls.Add(this.labelX4);
            this.grpinfo.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpinfo.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grpinfo.Location = new System.Drawing.Point(12, 110);
            this.grpinfo.Name = "grpinfo";
            this.grpinfo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpinfo.Size = new System.Drawing.Size(871, 319);
            // 
            // 
            // 
            this.grpinfo.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpinfo.Style.BackColorGradientAngle = 90;
            this.grpinfo.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpinfo.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderBottomWidth = 1;
            this.grpinfo.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpinfo.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderLeftWidth = 1;
            this.grpinfo.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderRightWidth = 1;
            this.grpinfo.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpinfo.Style.BorderTopWidth = 1;
            this.grpinfo.Style.CornerDiameter = 4;
            this.grpinfo.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpinfo.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpinfo.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpinfo.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpinfo.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpinfo.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpinfo.TabIndex = 4;
            this.grpinfo.Text = "چاپ قبوض مشترکین";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.rd_all);
            this.groupPanel1.Controls.Add(this.rd_print_ghabz_which_not_printed);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(20, 61);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(352, 114);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 19;
            this.groupPanel1.Text = "چاپ قبوض مربوط به قرارداد های فوق ";
            // 
            // rd_all
            // 
            this.rd_all.AutoSize = true;
            this.rd_all.BackColor = System.Drawing.Color.Transparent;
            this.rd_all.Checked = true;
            this.rd_all.ForeColor = System.Drawing.Color.Black;
            this.rd_all.Location = new System.Drawing.Point(155, 12);
            this.rd_all.Name = "rd_all";
            this.rd_all.Size = new System.Drawing.Size(153, 25);
            this.rd_all.TabIndex = 16;
            this.rd_all.TabStop = true;
            this.rd_all.Text = "کلیه مشترکین این دوره ";
            this.rd_all.UseVisualStyleBackColor = false;
            // 
            // rd_print_ghabz_which_not_printed
            // 
            this.rd_print_ghabz_which_not_printed.AutoSize = true;
            this.rd_print_ghabz_which_not_printed.BackColor = System.Drawing.Color.Transparent;
            this.rd_print_ghabz_which_not_printed.ForeColor = System.Drawing.Color.Black;
            this.rd_print_ghabz_which_not_printed.Location = new System.Drawing.Point(27, 43);
            this.rd_print_ghabz_which_not_printed.Name = "rd_print_ghabz_which_not_printed";
            this.rd_print_ghabz_which_not_printed.Size = new System.Drawing.Size(281, 25);
            this.rd_print_ghabz_which_not_printed.TabIndex = 17;
            this.rd_print_ghabz_which_not_printed.Text = "مشترکینی که قبض آنها تا به حال چاپ نشده است ";
            this.rd_print_ghabz_which_not_printed.UseVisualStyleBackColor = false;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.rd_address);
            this.groupPanel3.Controls.Add(this.rd_gharardad);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(472, 61);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(352, 114);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 18;
            this.groupPanel3.Text = "مرتب سازی ";
            // 
            // rd_address
            // 
            this.rd_address.AutoSize = true;
            this.rd_address.BackColor = System.Drawing.Color.Transparent;
            this.rd_address.Checked = true;
            this.rd_address.ForeColor = System.Drawing.Color.Black;
            this.rd_address.Location = new System.Drawing.Point(58, 12);
            this.rd_address.Name = "rd_address";
            this.rd_address.Size = new System.Drawing.Size(250, 25);
            this.rd_address.TabIndex = 16;
            this.rd_address.TabStop = true;
            this.rd_address.Text = "مرتب سازی شده بر اساس آدرس مشترکین";
            this.rd_address.UseVisualStyleBackColor = false;
            // 
            // rd_gharardad
            // 
            this.rd_gharardad.AutoSize = true;
            this.rd_gharardad.BackColor = System.Drawing.Color.Transparent;
            this.rd_gharardad.ForeColor = System.Drawing.Color.Black;
            this.rd_gharardad.Location = new System.Drawing.Point(51, 43);
            this.rd_gharardad.Name = "rd_gharardad";
            this.rd_gharardad.Size = new System.Drawing.Size(257, 25);
            this.rd_gharardad.TabIndex = 17;
            this.rd_gharardad.Text = "مرتب سازی شده بر اساس قرارداد مشترکین";
            this.rd_gharardad.UseVisualStyleBackColor = false;
            // 
            // ch_faal_company
            // 
            this.ch_faal_company.AutoSize = true;
            this.ch_faal_company.BackColor = System.Drawing.Color.Transparent;
            this.ch_faal_company.Font = new System.Drawing.Font("B Yekan", 9F);
            this.ch_faal_company.ForeColor = System.Drawing.Color.Black;
            this.ch_faal_company.Location = new System.Drawing.Point(316, 193);
            this.ch_faal_company.Name = "ch_faal_company";
            this.ch_faal_company.Size = new System.Drawing.Size(202, 22);
            this.ch_faal_company.TabIndex = 15;
            this.ch_faal_company.Text = "فقط مشترکین فعال قبوض آنها چاپ شود";
            this.ch_faal_company.UseVisualStyleBackColor = false;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.Black;
            this.progressBar1.Location = new System.Drawing.Point(20, 262);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(833, 23);
            this.progressBar1.TabIndex = 13;
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Font = new System.Drawing.Font("B Yekan", 10F);
            this.btnPrint.Location = new System.Drawing.Point(322, 221);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnPrint.Size = new System.Drawing.Size(191, 35);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.Symbol = "";
            this.btnPrint.SymbolColor = System.Drawing.Color.Green;
            this.btnPrint.SymbolSize = 9F;
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "چاپ قبوض";
            this.btnPrint.Click += new System.EventHandler(this.btnSetNotFaal_Click);
            // 
            // TxtEnd
            // 
            this.TxtEnd.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtEnd.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TxtEnd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtEnd.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TxtEnd.Font = new System.Drawing.Font("B Yekan", 10F);
            this.TxtEnd.ForeColor = System.Drawing.Color.Black;
            this.TxtEnd.Location = new System.Drawing.Point(280, 16);
            this.TxtEnd.MinValue = 1;
            this.TxtEnd.Name = "TxtEnd";
            this.TxtEnd.ShowUpDown = true;
            this.TxtEnd.Size = new System.Drawing.Size(66, 28);
            this.TxtEnd.TabIndex = 1;
            this.TxtEnd.Value = 1;
            this.TxtEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtEnd_KeyDown);
            // 
            // TxtStart
            // 
            this.TxtStart.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtStart.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TxtStart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtStart.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TxtStart.Font = new System.Drawing.Font("B Yekan", 10F);
            this.TxtStart.ForeColor = System.Drawing.Color.Black;
            this.TxtStart.Location = new System.Drawing.Point(452, 16);
            this.TxtStart.MinValue = 1;
            this.TxtStart.Name = "TxtStart";
            this.TxtStart.ShowUpDown = true;
            this.TxtStart.Size = new System.Drawing.Size(66, 28);
            this.TxtStart.TabIndex = 0;
            this.TxtStart.Value = 1;
            this.TxtStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtStart_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(322, 13);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(122, 33);
            this.labelX1.TabIndex = 12;
            this.labelX1.Text = "تا شماره قرارداد:";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(475, 13);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(138, 33);
            this.labelX4.TabIndex = 11;
            this.labelX4.Text = "از شماره قرارداد:";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.buttonX1.Location = new System.Drawing.Point(50, 82);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(200, 35);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 21;
            this.buttonX1.Text = " کلیه قبوض";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(-65, 29);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(804, 33);
            this.labelX7.TabIndex = 20;
            this.labelX7.Text = "در صورتی که تمایل دارید، قبوض مربوطه به طور کلی در یک فایل اکسل دریافت نمایید ، ب" +
    "ر روی دکمه روبرو کلیک نمایید";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.cmb_ghabz_doreh);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.cmb_ghabz_type);
            this.groupPanel2.Controls.Add(this.labelX3);
            this.groupPanel2.Controls.Add(this.labelX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel2.Location = new System.Drawing.Point(12, 1);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(870, 103);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 19;
            this.groupPanel2.Text = "اتتخاب نوع قبض و دوره مربوطه ";
            // 
            // cmb_ghabz_doreh
            // 
            this.cmb_ghabz_doreh.BackColor = System.Drawing.Color.White;
            this.cmb_ghabz_doreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ghabz_doreh.ForeColor = System.Drawing.Color.Black;
            this.cmb_ghabz_doreh.FormattingEnabled = true;
            this.cmb_ghabz_doreh.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_ghabz_doreh.Location = new System.Drawing.Point(265, 21);
            this.cmb_ghabz_doreh.Name = "cmb_ghabz_doreh";
            this.cmb_ghabz_doreh.Size = new System.Drawing.Size(66, 28);
            this.cmb_ghabz_doreh.TabIndex = 17;
            this.cmb_ghabz_doreh.SelectedIndexChanged += new System.EventHandler(this.cmb_ghabz_doreh_SelectedIndexChanged);
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(274, 19);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(138, 33);
            this.labelX5.TabIndex = 16;
            this.labelX5.Text = "دوره مربوطه :";
            // 
            // labelX6
            // 
            this.labelX6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(289, 19);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(138, 33);
            this.labelX6.TabIndex = 15;
            // 
            // cmb_ghabz_type
            // 
            this.cmb_ghabz_type.BackColor = System.Drawing.Color.White;
            this.cmb_ghabz_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ghabz_type.ForeColor = System.Drawing.Color.Black;
            this.cmb_ghabz_type.FormattingEnabled = true;
            this.cmb_ghabz_type.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_ghabz_type.Location = new System.Drawing.Point(455, 21);
            this.cmb_ghabz_type.Name = "cmb_ghabz_type";
            this.cmb_ghabz_type.Size = new System.Drawing.Size(121, 28);
            this.cmb_ghabz_type.TabIndex = 14;
            this.cmb_ghabz_type.SelectedIndexChanged += new System.EventHandler(this.cmb_ghabz_type_SelectedIndexChanged);
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(499, 19);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(138, 33);
            this.labelX3.TabIndex = 13;
            this.labelX3.Text = "نوع قبض: ";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(514, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(138, 33);
            this.labelX2.TabIndex = 12;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.buttonX3);
            this.groupPanel4.Controls.Add(this.buttonX2);
            this.groupPanel4.Controls.Add(this.buttonX1);
            this.groupPanel4.Controls.Add(this.labelX7);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel4.Location = new System.Drawing.Point(12, 435);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(871, 149);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 22;
            this.groupPanel4.Text = "گزارشگیری بر مبنای اکسل";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.buttonX3.Location = new System.Drawing.Point(638, 82);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(200, 35);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 23;
            this.buttonX3.Text = " قبوض پرداخت نشده";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.buttonX2.Location = new System.Drawing.Point(336, 82);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(200, 35);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 22;
            this.buttonX2.Text = " قبوض پرداخت شده";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // grp_doreh_for_esf
            // 
            this.grp_doreh_for_esf.BackColor = System.Drawing.Color.White;
            this.grp_doreh_for_esf.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_doreh_for_esf.Controls.Add(this.ch_have_sharj_ghabz);
            this.grp_doreh_for_esf.Controls.Add(this.ch_have_ab_gahbz);
            this.grp_doreh_for_esf.Controls.Add(this.cmb_sharj);
            this.grp_doreh_for_esf.Controls.Add(this.labelX10);
            this.grp_doreh_for_esf.Controls.Add(this.cmb_ab);
            this.grp_doreh_for_esf.Controls.Add(this.labelX8);
            this.grp_doreh_for_esf.Controls.Add(this.labelX9);
            this.grp_doreh_for_esf.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_doreh_for_esf.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grp_doreh_for_esf.Location = new System.Drawing.Point(12, 1);
            this.grp_doreh_for_esf.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grp_doreh_for_esf.Name = "grp_doreh_for_esf";
            this.grp_doreh_for_esf.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_doreh_for_esf.Size = new System.Drawing.Size(870, 103);
            // 
            // 
            // 
            this.grp_doreh_for_esf.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_doreh_for_esf.Style.BackColorGradientAngle = 90;
            this.grp_doreh_for_esf.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_doreh_for_esf.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh_for_esf.Style.BorderBottomWidth = 1;
            this.grp_doreh_for_esf.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_doreh_for_esf.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh_for_esf.Style.BorderLeftWidth = 1;
            this.grp_doreh_for_esf.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh_for_esf.Style.BorderRightWidth = 1;
            this.grp_doreh_for_esf.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_doreh_for_esf.Style.BorderTopWidth = 1;
            this.grp_doreh_for_esf.Style.CornerDiameter = 4;
            this.grp_doreh_for_esf.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_doreh_for_esf.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_doreh_for_esf.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_doreh_for_esf.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_doreh_for_esf.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_doreh_for_esf.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_doreh_for_esf.TabIndex = 23;
            this.grp_doreh_for_esf.Text = "اتتخاب نوع قبض و دوره مربوطه ";
            // 
            // ch_have_sharj_ghabz
            // 
            this.ch_have_sharj_ghabz.AutoSize = true;
            this.ch_have_sharj_ghabz.BackColor = System.Drawing.Color.Transparent;
            this.ch_have_sharj_ghabz.Checked = true;
            this.ch_have_sharj_ghabz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ch_have_sharj_ghabz.ForeColor = System.Drawing.Color.Black;
            this.ch_have_sharj_ghabz.Location = new System.Drawing.Point(107, 36);
            this.ch_have_sharj_ghabz.Name = "ch_have_sharj_ghabz";
            this.ch_have_sharj_ghabz.Size = new System.Drawing.Size(136, 25);
            this.ch_have_sharj_ghabz.TabIndex = 22;
            this.ch_have_sharj_ghabz.Text = "قبض شارژ چاپ شود ";
            this.ch_have_sharj_ghabz.UseVisualStyleBackColor = false;
            // 
            // ch_have_ab_gahbz
            // 
            this.ch_have_ab_gahbz.AutoSize = true;
            this.ch_have_ab_gahbz.BackColor = System.Drawing.Color.Transparent;
            this.ch_have_ab_gahbz.Checked = true;
            this.ch_have_ab_gahbz.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ch_have_ab_gahbz.ForeColor = System.Drawing.Color.Black;
            this.ch_have_ab_gahbz.Location = new System.Drawing.Point(556, 36);
            this.ch_have_ab_gahbz.Name = "ch_have_ab_gahbz";
            this.ch_have_ab_gahbz.Size = new System.Drawing.Size(127, 25);
            this.ch_have_ab_gahbz.TabIndex = 21;
            this.ch_have_ab_gahbz.Text = "قبض آب چاپ شود ";
            this.ch_have_ab_gahbz.UseVisualStyleBackColor = false;
            // 
            // cmb_sharj
            // 
            this.cmb_sharj.BackColor = System.Drawing.Color.White;
            this.cmb_sharj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_sharj.ForeColor = System.Drawing.Color.Black;
            this.cmb_sharj.FormattingEnabled = true;
            this.cmb_sharj.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_sharj.Location = new System.Drawing.Point(265, 12);
            this.cmb_sharj.Name = "cmb_sharj";
            this.cmb_sharj.Size = new System.Drawing.Size(66, 28);
            this.cmb_sharj.TabIndex = 20;
            // 
            // labelX10
            // 
            this.labelX10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(265, 9);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(138, 33);
            this.labelX10.TabIndex = 19;
            this.labelX10.Text = "دوره شارژ:";
            // 
            // cmb_ab
            // 
            this.cmb_ab.BackColor = System.Drawing.Color.White;
            this.cmb_ab.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ab.ForeColor = System.Drawing.Color.Black;
            this.cmb_ab.FormattingEnabled = true;
            this.cmb_ab.Items.AddRange(new object[] {
            "آب",
            "شارژ"});
            this.cmb_ab.Location = new System.Drawing.Point(717, 14);
            this.cmb_ab.Name = "cmb_ab";
            this.cmb_ab.Size = new System.Drawing.Size(66, 28);
            this.cmb_ab.TabIndex = 17;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(700, 14);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(138, 33);
            this.labelX8.TabIndex = 16;
            this.labelX8.Text = "دوره آب :";
            // 
            // labelX9
            // 
            this.labelX9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(337, 12);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(138, 33);
            this.labelX9.TabIndex = 15;
            // 
            // Frm_ghobooz_chap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 611);
            this.Controls.Add(this.grp_doreh_for_esf);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.grpinfo);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ghobooz_chap";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_ghobooz_chap_FormClosing);
            this.Load += new System.EventHandler(this.Frm_ghobooz_chap_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_ghobooz_chap_KeyDown);
            this.grpinfo.ResumeLayout(false);
            this.grpinfo.PerformLayout();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStart)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.grp_doreh_for_esf.ResumeLayout(false);
            this.grp_doreh_for_esf.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpinfo;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.Editors.IntegerInput TxtEnd;
        private DevComponents.Editors.IntegerInput TxtStart;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox ch_faal_company;
        private System.Windows.Forms.RadioButton rd_address;
        private System.Windows.Forms.RadioButton rd_gharardad;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.RadioButton rd_all;
        private System.Windows.Forms.RadioButton rd_print_ghabz_which_not_printed;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.ComboBox cmb_ghabz_doreh;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.ComboBox cmb_ghabz_type;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_doreh_for_esf;
        private System.Windows.Forms.ComboBox cmb_sharj;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.ComboBox cmb_ab;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.CheckBox ch_have_sharj_ghabz;
        private System.Windows.Forms.CheckBox ch_have_ab_gahbz;
    }
}