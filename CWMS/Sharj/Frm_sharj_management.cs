﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using FarsiLibrary.Utils;
using System.Threading;
using System.Globalization;
using System.Data.SqlClient;
using System.Linq;
namespace CWMS
{
    public partial class Frm_sharj_management : MyMetroForm
    {

        FrmMain FrmMainOfProject = null;
        public Frm_sharj_management()
        {
            InitializeComponent();


        }

        public Frm_sharj_management(FrmMain frm)
        {
            InitializeComponent();
            FrmMainOfProject = frm;
        }


        private void txt_doreh_code_Enter(object sender, EventArgs e)
        {
            Payam.Show("امکان تعریف کد دوره به صورت درستی وجود ندارد. بر روی دکمه تنظیم کلیک نمایید");
        }

        private void btn_set_parameter_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DtLastSharjDoreh = Classes.clsDoreh.Get_last_Sharj_Doreh();
                DataTable DtSharjMablagh = Classes.clsSharj.Get_last_Sharj_mablagh_tasvib_shodeh();
                DataTable DtSharjInfo = Classes.clsDoreh.Get_Sharj_info();


                if (DtSharjInfo.Rows.Count == 0)
                {
                    Payam.Show("کاربر گرامی ، ابتدا به منوی تنظیمات کلی رفته ومقادیر مربوطه را پر نمایید و سپس به این فرم مراجعه نمایید");
                    this.Close();
                }
                else if (DtSharjMablagh.Rows.Count == 0)
                {
                    Payam.Show("متاسفانه هیچ مبلغ مصوبی جهت تعیین هزینه شارژ بر اساس تاریخ سیستم شما تعریف نشده است .\n لطفا ابتدا تعرفه های شارژ را در قسمت مربوطه وارد کنید.");
                    this.Close();
                }
                else
                {
                    txt_mablagh.Text = DtSharjMablagh.Rows[0]["mablagh"].ToString();


                    //بدست اوردن تنظیمات کلی مربوط به تمامی دوره های شارژ اوردن جدول اب اینفو
                    txtDarsadMaliat.Text = DtSharjInfo.Rows[0]["darsad_maliat"].ToString();
                    txttejarizarib.Tag = txttejarizarib.Text = DtSharjInfo.Rows[0]["zaribtejari"].ToString();
                    //بدست اوردن تنظیمات کلی مربوط به دتمامی دوره های اب اوردن جدول شارز اینفو



                    if (DtLastSharjDoreh.Rows.Count == 0)
                    {
                        Create_doreh_for_first_time(DtSharjInfo);
                    }
                    else
                    {


                        DateTime StartTime = Convert.ToDateTime(DtLastSharjDoreh.Rows[0]["end_time"]).AddDays(1);

                        //----------- بدست اوردن بازه دوره بعدی 
                        Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(StartTime, Convert.ToInt32(DtSharjInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtSharjInfo.Rows[0]["mohlat_pardakht"]));
                        //----------- بدست اوردن بازه دوره بعدی 


                        //--------------- مقداردهی کنترل های روی فرم
                        txt_start.SelectedDateTime = StartTime;
                        txt_end.SelectedDateTime = Doreh_length.End_Of_Doreh;
                        txt_mohlat.SelectedDateTime = Doreh_length.Mohlat_pardakht;

                        txt_start.Tag = StartTime;
                        txt_end.Tag = Doreh_length.End_Of_Doreh;
                        //--------------- مقداردهی کنترل های روی فرم


                        txt_doreh_code.Text = (Convert.ToInt32(DtLastSharjDoreh.Rows[0]["dcode"]) + 1).ToString();
                        Classes.ClsMain.ChangeCulture("f");
                    }
                }
            }
            catch (Exception ex)
            {
                Payam.Show("متاسفانه امکان تعریف دوره جدید وجود ندارد . لطفا با شرکت پشتیبان هماهنگ نمایید");
                Classes.ClsMain.logError(ex);

            }
        }

        void Create_doreh_for_first_time(DataTable DtSharjInfo)
        {
            txt_doreh_code.Text = "1";
            DateTime StartTime = DateTime.Now;


            //----------- بدست اوردن بازه دوره بعدی 
            Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(StartTime, Convert.ToInt32(DtSharjInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtSharjInfo.Rows[0]["mohlat_pardakht"]));
            //----------- بدست اوردن بازه دوره بعدی 


            txt_start.SelectedDateTime = StartTime;
            txt_end.SelectedDateTime = Doreh_length.End_Of_Doreh;
            txt_mohlat.SelectedDateTime = Doreh_length.Mohlat_pardakht;

            txt_start.Tag = StartTime;
            txt_end.Tag = Doreh_length.End_Of_Doreh;
        }


        private void FrmManagmentSharjDoreh_Load(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

            btn_set_parameter.PerformClick();
            Classes.ClsMain.ChangeCulture("f");
            //------------- عدم نمایش تب ایجاد دوره تا هنگام اتمام دوره فعلی  --------------------------------------
            DataTable LastDorehInfo = Classes.ClsMain.GetDataTable("select top(1) end_time from sharj_doreh order by dcode desc ");
            if (LastDorehInfo.Rows.Count != 0)
                if (DateTime.Now < Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"]))
                {
                    TabNEwDoreh.Enabled = false;
                    lblAlert.Visible = true;
                    lblAlert.Text = "نا اتمام تاریخ دوره فعلی -" + PersianDateConverter.ToPersianDate(Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"])).ToString("d") + "-" + " امکان تعریف دوره جدید شارژ وجود ندارد ";
                }




            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate (object o, RunWorkerCompletedEventArgs args)
                {
                    DateTime StartTime = DateTime.Now;
                    timer1.Enabled = false;
                    if (Result != "fail")
                    {

                        progressBar1.Value = 100;
                        Classes.ClsMain.ChangeCulture("f");
                        //EtelaatamariInfoPanel();
                        btnSodoorGhabzForAllMoshtarekin.Visible = false;
                        lblDorehInfotext.Text = "قبوض کلیه مشترکین مربوط به این دوره صادر شده است ";
                        btnChap.Visible = btnShowGhobooz.Visible = lblSuccess.Visible = grpdelete.Visible = grpStat.Visible = true;

                    }


                    else
                    {
                        Payam.Show("متاسفانه در هنگام صدور قبض مشکل پیش امده است . سریعا با شرکت پشتیبان تماس حاصل فرمایید");
                        this.Close();
                    }
                });

        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            sharj_dorehTableAdapter.Fill(mainDataSest.sharj_doreh);
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p60)
            {

                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 60);

                try
                {
                    Convert.ToSingle(txt_mablagh.Text);
                }
                catch
                {
                    Payam.Show("لطفا مبلغ را به صورت صحیح وارد نمایید");
                    txt_mablagh.SelectAll();
                    txt_mablagh.Focus();
                    return;
                }

                try
                {
                    Convert.ToSingle(txttejarizarib.Text, CultureInfo.InvariantCulture);


                }
                catch
                {
                    txttejarizarib.Text = txttejarizarib.Tag.ToString();
                    Payam.Show("ضریب تجاری صحیح نمی باشد . مقدار پیش فرض جایگزین ان می شود");
                    return;
                }





                DataTable LastDorehInfo = Classes.ClsMain.GetDataTable("select top(1) end_time,ghabz_sodoor from sharj_doreh order by dcode desc ");
                //-----------------هیچ دوره ای تا به حال در سیستم ایجاد نشده است ----------------





                if (LastDorehInfo.Rows.Count == 0)
                {
                    DataRow dr = LastDorehInfo.NewRow();
                    dr["end_time"] = txt_start.SelectedDateTime;
                    dr["ghabz_sodoor"] = true;
                    LastDorehInfo.Rows.Add(dr);

                }


                if (DateTime.Now < Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"]))
                    Payam.Show("تا اتمام تاریخ دوره فعلی امکان اضافه کردن دوره جدید وجود ندارد ");
                else if (Convert.ToBoolean(LastDorehInfo.Rows[0]["ghabz_sodoor"]) == false)
                    Payam.Show("به دلیل صادر نشدن قبوض دوره فعلی ، امکان تعریف دوره جدید وجود ندارد . ");
                else
                {
                    if (txt_start.SelectedDateTime < Convert.ToDateTime(txt_start.Tag) && txt_doreh_code.Text != "1")
                    {
                        Payam.Show("تاریخ شروع هر دوره می بایست از تاریخ پایان دوره قبل حتما بیشتر باشد");
                        txt_start.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(txt_start.Tag).Date).ToString("d");
                        return;
                    }
                    if (txt_end.SelectedDateTime <= txt_start.SelectedDateTime)
                    {
                        Payam.Show("تاریخ پایان از تاریخ شروع عقب تر یا با آن برابر است .. تاریخ پایان هم اکنون مقدار صحیحی می گیرد.");
                        txt_end.SelectedDateTime = (DateTime)txt_end.Tag;
                        return;
                    }



                    if (txt_doreh_code.Text == "")
                    {
                        Payam.Show("لطفا بر روی دکمه اطلاعات دوره جدید کلیک نمایید");
                    }

                    else
                    {




                        Classes.ClsMain.ChangeCulture("e");
                        Classes.clsSharj.Ijad_Doreh_jadid_sharj_V2(Convert.ToInt32(txt_doreh_code.Text), txt_start.SelectedDateTime, txt_end.SelectedDateTime, txt_mohlat.SelectedDateTime, Convert.ToSingle(txt_mablagh.Text), txtDarsadMaliat.Text, txttejarizarib.Text);
                        Classes.ClsMain.ChangeCulture("f");

                        txt_doreh_code.Text = "";
                        txt_mablagh.Text = "";
                        txt_mohlat.Text = txt_end.Text = txt_start.Text = "";
                        Payam.Show("دوره جدید با موفقیت در سیستم ایجاد شد ");
                        btn_set_parameter.PerformClick();
                        superTabControl1.SelectedTabIndex = 1;
                    }

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }
        public string Pco_name = "", Pgharardad = "", Pmetraj = "";








        void EtelaatamariInfoPanel()
        {
            Classes.ClsMain.ChangeCulture("e");
            sharj_dorehTableAdapter.Fill(mainDataSest.sharj_doreh);
            DataView dv = new DataView(mainDataSest.sharj_doreh);
            dv.RowFilter = "dcode=" + lblSelectedDoreh.Text;
            DataTable dt = dv.ToTable();
            grpdelete.Visible = false;

            if (Convert.ToBoolean(dt.Rows[0]["ghabz_sodoor"]) == true)
            {
                grp_gozaresh.Visible = true;

                lblDorehInfotext.Text = "قبوض کلیه مشترکین مربوط به این دوره صادر شده است ";

                btnSodoorGhabzForAllMoshtarekin.Visible = false;
                btnChap.Visible = btnShowGhobooz.Visible = lblSuccess.Visible = grpdelete.Visible = grpStat.Visible = true;
                if (dgv_doreh.Rows[0].Selected == true)
                    grpdelete.Visible = true;
                else
                    grpdelete.Visible = false;

                lblInfoAboutMoshahedeh.Text = "غیر قابل مشاهده";
                if (Convert.ToBoolean(dt.Rows[0]["visiblebymoshtarek"]) == true)
                {
                    lblInfoAboutMoshahedeh.Text = "قابل مشاهده";
                }

            }
            else
            {

                grp_gozaresh.Visible = false;
                btnSodoorGhabzForAllMoshtarekin.Visible = true;
                lblDorehInfotext.Text = "،فرآیند صدور قبض شارژ برای کلیه مشترکین اجرا نشده است  .";

                btnChap.Visible = lblSuccess.Visible = btnShowGhobooz.Visible = grpdelete.Visible = grpStat.Visible = false;
                lblInfoAboutMoshahedeh.Text = "";
            }


            Classes.ClsMain.ChangeCulture("f");

        }



        private void dgv_doreh_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_doreh.SelectedRows.Count != 0)
            {
                tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                tabdorehdetails.Visible = true;
                lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
                lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                lbl_sharj_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();

                superTabControl1.SelectedTabIndex = 2;
                EtelaatamariInfoPanel();




            }
        }









        private void superTabControl1_SelectedTabChanging(object sender, SuperTabStripSelectedTabChangingEventArgs e)
        {
            if (superTabControl1.SelectedTabIndex == 0)
            {
                sharj_dorehTableAdapter.Fill(mainDataSest.sharj_doreh);
            }
        }

        private void FrmManagmentSharjDoreh_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            if (FrmMainOfProject != null)
                FrmMainOfProject.UpdateDorehPanel();

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p12)
            {

                if (Classes.ClsMain.isClosed("Frm_sharj_mablagh"))
                {
                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 12);

                    Sharj.Frm_sharj_mablagh frm = new Sharj.Frm_sharj_mablagh();
                    Classes.ClsMain.openForm("Frm_sharj_mablagh");
                    frm.Show();
                }
                btn_set_parameter.PerformClick();
            }
            else
                Payam.Show("شما مجوز دسترسی به این بخش را ندارید");
        }


        string Result = "";


        bool Check_Moshtarekin_tarikh_gharardad()
        {
            bool flag = true;
            for (int i = 0; i < Classes.ClsMain.MoshtarekDT.Rows.Count; i++)
            {
                try
                {
                    Convert.ToDateTime(Classes.ClsMain.MoshtarekDT.Rows[i]["tarikh_gharardad"]);
                }
                catch (Exception)
                {
                    Payam.Show("لطفا ابتدا به منوی مشترکین رفته و تاریخ قرارداد  " + Classes.ClsMain.MoshtarekDT.Rows[i]["gharardad"] + " را اصلاح نمایید ");
                    flag = false;
                    break;
                }
            }
            return flag;
        }


        private void btnSodoorGhabzForAllMoshtarekin_Click(object sender, EventArgs e)
        {

            if (Classes.clsMoshtarekin.UserInfo.p64)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 64);
                if (Cpayam.Show("آیا  با صدور قبوض این دوره موافق هستید؟") == DialogResult.Yes)
                {
                    Payam.Show("با  تایید شما عملیات صدور قبض اجرا خواهد شد . لطفا پس از تایید چند لحظه صبر نمایید");



                    try
                    {
                        if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                        {
                            //if (Check_Moshtarekin_tarikh_gharardad())
                            //{
                            timer1.Enabled = true;
                            backgroundWorker1.RunWorkerAsync(backgroundWorker1);
                            btnSodoorGhabzForAllMoshtarekin.Visible = false;

                            //}
                        }
                        else
                        {
                            timer1.Enabled = true;
                            backgroundWorker1.RunWorkerAsync(backgroundWorker1);
                            btnSodoorGhabzForAllMoshtarekin.Visible = false;

                        }
                        //--------------------اجرای تابع صدور قبض-----------------------




                    }
                    catch (Exception eee)
                    {
                        Payam.Show(eee.ToString());
                        Payam.Show("کاربر محترم، متاسفانه در حین انجام عملیات خطای سیستمی رخ داده است . لطفا سریعا با شرکت پشتیبان تماس حاصل فرمایید");
                        grpdelete.Visible = grpStat.Visible = false;

                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void btn_chap_tak_ghabz_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 90)
            {
                progressBar1.Value += 10;
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            if (dgv_doreh.SelectedRows.Count != 0)
            {
                tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                tabdorehdetails.Visible = true;
                lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
                lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                lbl_sharj_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();

                superTabControl1.SelectedTabIndex = 2;
                EtelaatamariInfoPanel();

            }
            Classes.ClsMain.ChangeCulture("f");
        }

        void delete_Ghobooz(bool By_pardakhti)
        {
            if (Classes.clsMoshtarekin.UserInfo.p66)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 66);

                Payam.Show("با حذف قبوض دوره اخیر ، کلیه اطلاعات آن از سیستم پاک خواهد شد");

                if (Cpayam.Show("آیا با حذف کلیه قبوض دوره جاری موافق هستید؟") == DialogResult.Yes)
                {
                    SqlConnection con = new SqlConnection();
                    SqlCommand com = new SqlCommand();
                    con.ConnectionString = Classes.ClsMain.ConnectionStr;
                    con.Open();
                    SqlTransaction transaction = con.BeginTransaction("DeleteGhab");
                    com.Connection = con;
                    com.Transaction = transaction;
                    //------------------------------------------------------------------
                    string Doreh = lblSelectedDoreh.Text;

                    try
                    {
                        if (By_pardakhti)
                        {
                            com.CommandText = "delete from ghabz_pardakht where dcode=" + Doreh + " and is_ab=0";
                            com.ExecuteNonQuery();
                        }
                        com.CommandText = "delete from sharj_ghabz where dcode=" + Doreh;
                        com.ExecuteNonQuery();

                        com.CommandText = "update sharj_doreh set ghabz_sodoor=0  where dcode=" + Doreh;
                        com.ExecuteNonQuery();

                        transaction.Commit();
                        con.Close();
                        progressBar1.Value = 0;
                        btnSodoorGhabzForAllMoshtarekin.Enabled = true;
                        Payam.Show("کلیه قبوض و پرداختی های دوره جاری با موفقیت از سیستم حذف شد");
                        this.Close();

                    }
                    catch (Exception rt)
                    {
                        Classes.ClsMain.logError(rt);
                        Payam.Show("متاسفانه در هنگام حذف اطلاعات دوره خطا پیش امده است");
                    }

                    finally
                    {
                        con.Close();
                    }

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX5_Click_1(object sender, EventArgs e)
        {

        }

        private void btn_showInfo_Click_1(object sender, EventArgs e)
        {

        }

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p9)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 9);

                new Frm_sharj_For_all(lblSelectedDoreh.Text).ShowDialog();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void BtnDeleteOne_Click(object sender, EventArgs e)
        {
            new Ab.FrmDeleteOneGhabz("sharj", lblSelectedDoreh.Text).ShowDialog();
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            if (dgv_doreh.SelectedRows.Count != 0)
            {
                tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                tabdorehdetails.Visible = true;
                lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
                lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                lbl_sharj_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();
                superTabControl1.SelectedTabIndex = 2;
                EtelaatamariInfoPanel();




            }
            else
            {
                Payam.Show("لطفا یکی از دوره ها را انتخاب کنید ");
            }
            Classes.ClsMain.ChangeCulture("f");
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p69)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 69);

                if (dgv_doreh.Rows[0].Selected == true)
                {
                    bool IsGhoboozSader = mainDataSest.sharj_doreh.Where(p => p.dcode == Convert.ToInt32(dgv_doreh.Rows[0].Cells[0].Value.ToString())).First().ghabz_sodoor;
                    //new Sharj.FrmTamdid(dgv_doreh.Rows[0].Cells[0].Value.ToString(), Convert.ToDateTime(dgv_doreh.Rows[0].Cells[1].Value), Convert.ToDateTime(dgv_doreh.Rows[0].Cells[2].Value), Convert.ToDateTime(dgv_doreh.Rows[0].Cells[3].Value), "sharj", IsGhoboozSader).ShowDialog();
                }
                else
                    Payam.Show("شما تنها می توانید اطلاعات مربوط به دوره اخر را تمدید نمایید");
                sharj_dorehTableAdapter.Fill(mainDataSest.sharj_doreh);
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p63)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 63);

                if (dgv_doreh.Rows[0].Selected)
                {
                    Payam.Show("با حذف دوره اخیر ، کلیه اطلاعات آن از سیستم پاک خواهد شد");
                    if (Cpayam.Show("آیا با حذف کلیه اطلاعات مربوط به دوره اخیر موافق هستید؟") == DialogResult.Yes)
                    {
                        //------------------------------------------------------------------

                        //------------------------------------------------------------------
                        //------------------------------------------------------------------
                        SqlConnection con = new SqlConnection();
                        SqlCommand com = new SqlCommand();
                        con.ConnectionString = Classes.ClsMain.ConnectionStr;
                        con.Open();
                        SqlTransaction transaction = con.BeginTransaction("DeleteGhab");
                        com.Connection = con;
                        com.Transaction = transaction;
                        //------------------------------------------------------------------
                        string Doreh = dgv_doreh.Rows[0].Cells[0].Value.ToString();

                        try
                        {
                            com.CommandText = "delete from ghabz_pardakht where dcode=" + Doreh + " and is_ab=0";
                            com.ExecuteNonQuery();

                            com.CommandText = "delete from sharj_ghabz where dcode=" + Doreh;
                            com.ExecuteNonQuery();
                            com.CommandText = "delete from sharj_doreh where dcode=" + Doreh;
                            com.ExecuteNonQuery();

                            transaction.Commit();
                            con.Close();
                            Payam.Show("کلیه اطلاعات دوره جاری از سیستم حذف شدند");
                        }
                        catch (Exception rt)
                        {
                            Classes.ClsMain.logError(rt);
                            Payam.Show("متاسفانه در هنگام حذف اطلاعات دوره خطا پیش امدها است");
                        }

                        finally
                        {
                            con.Close();
                        }

                        this.Close();


                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Classes.clsSharj sharjClass = new Classes.clsSharj();
            Result = sharjClass.Generate_Ghabz();
            //Result = Classes.clsSharj.Create_ghobooz_sharj_Background();
        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p74)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 74);

                Classes.ClsMain.ExecuteNoneQuery("update sharj_doreh set visiblebymoshtarek=1 where dcode=" + lblSelectedDoreh.Text + ";update sharj_ghabz set visiblebymoshtarek=1 where dcode=" + lblSelectedDoreh.Text);
                Payam.Show("مشترکین می توانند قبوض دوره " + lblSelectedDoreh.Text + "  خود را مشاهده کنند ");
                lblInfoAboutMoshahedeh.Text = "قابل مشاهده";
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p75)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 75);

                Classes.ClsMain.ExecuteNoneQuery("update sharj_doreh set visiblebymoshtarek=0 where dcode=" + lblSelectedDoreh.Text + ";update sharj_ghabz set visiblebymoshtarek=0 where dcode=" + lblSelectedDoreh.Text);
                Payam.Show("از این پس مشترکین قادر به مشاهده قبوض این دوره نخواهند بود");
                lblInfoAboutMoshahedeh.Text = "غیر قابل مشاهده";
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {

            new FrmSmsInform("اطلاع رسانی قبوض به کلیه مشترکین", lblSelectedDoreh.Text, 0, "sharj").ShowDialog();
        }

        private void grpStat_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmManagmentSharjDoreh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void dgv_doreh_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgv_doreh_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv_doreh.SelectedRows.Count != 0)
                if (dgv_doreh.SelectedRows[0].Index == 0)
                    btn_delete.Visible = true;
                else
                    btn_delete.Visible = false;

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            delete_Ghobooz(true);
        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            delete_Ghobooz(false);

        }
        void make_valid_date(advanced_report.advanced_report_dataset.Arzesh_afzoodeh_sharjDataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["tarikh"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt.Rows[i]["tarikh"])).ToString("d");
            }
        }
        private void buttonX3_Click_2(object sender, EventArgs e)
        {
            CrysReports.maliat.paytakht_sharj_arzesh_afzoodeh report1 = new CrysReports.maliat.paytakht_sharj_arzesh_afzoodeh();
            advanced_report.advanced_report_dataset ds = new advanced_report.advanced_report_dataset();
            advanced_report.advanced_report_datasetTableAdapters.settingTableAdapter sta = new advanced_report.advanced_report_datasetTableAdapters.settingTableAdapter();
            sta.Fill(ds.setting);
            advanced_report.advanced_report_datasetTableAdapters.Arzesh_afzoodeh_sharjTableAdapter ab_ta = new advanced_report.advanced_report_datasetTableAdapters.Arzesh_afzoodeh_sharjTableAdapter();
            ab_ta.FillByDcode(ds.Arzesh_afzoodeh_sharj, Convert.ToInt32(lblSelectedDoreh.Text));
            make_valid_date(ds.Arzesh_afzoodeh_sharj);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            new FrmShowReport(report1).ShowDialog();
        }

        private void btn_gozesh_karbodi_jame_sharj_Click(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                Classes.shahrak_classes.esf_bozorg.Gozaresh_karbordi_sharj(Convert.ToInt32(lblSelectedDoreh.Text));
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
                Classes.shahrak_classes.paytakht.Gozaresh_karbordi_sharj(Convert.ToInt32(lblSelectedDoreh.Text));
            else
            {
                Classes.shahrak_classes.esf_bozorg.Gozaresh_karbordi_sharj(Convert.ToInt32(lblSelectedDoreh.Text));
            }


        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
                Classes.shahrak_classes.paytakht.Gozaresh_asnad_hesabdari_sharj(Convert.ToInt32(lblSelectedDoreh.Text));
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                new forms.esf_bozorg.frm_pishnevis_factor(lblSelectedDoreh.Text, "sharj").ShowDialog();
                //Classes.shahrak_classes.esf_bozorg.Gozaresh_asnad_hesabdari_sharj(Convert.ToInt32(lblSelectedDoreh.Text));
            }
            else
            {
                Payam.Show("این قابلیت برای شهرک شما پیاده سازی نشده است .و صرفا گزارش پیش فرض نمایش داده خواهد شد");
                Classes.shahrak_classes.paytakht.Gozaresh_asnad_hesabdari_sharj(Convert.ToInt32(lblSelectedDoreh.Text));

            }
        }

        private void btnChap_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p70)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 70);

                new Frm_ghobooz_chap(lblSelectedDoreh.Text, "شارژ", "sharj").Show();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void dgv_doreh_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowDetails.PerformClick();
        }


    }
}