﻿namespace CWMS.Sharj
{
    partial class FrmTamdid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEnd = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txtStart = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.txt_mohlat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lbldoreh = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX44 = new DevComponents.DotNetBar.LabelX();
            this.labelX46 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_mablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtDarsadMaliat = new CWMS.FloatTextBox();
            this.txt_fazelab_mablagh = new CWMS.FloatTextBox();
            this.txt_masraf_omoomi = new CWMS.FloatTextBox();
            this.txt_aboonman = new CWMS.FloatTextBox();
            this.txtZaribSakhtoSaz = new CWMS.FloatTextBox();
            this.txttejarizarib = new CWMS.FloatTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.txtDarsadMaliat);
            this.groupBox1.Controls.Add(this.labelX10);
            this.groupBox1.Controls.Add(this.txt_mablagh);
            this.groupBox1.Controls.Add(this.labelX4);
            this.groupBox1.Controls.Add(this.txt_fazelab_mablagh);
            this.groupBox1.Controls.Add(this.labelX11);
            this.groupBox1.Controls.Add(this.txt_masraf_omoomi);
            this.groupBox1.Controls.Add(this.labelX13);
            this.groupBox1.Controls.Add(this.txt_aboonman);
            this.groupBox1.Controls.Add(this.labelX9);
            this.groupBox1.Controls.Add(this.txtZaribSakhtoSaz);
            this.groupBox1.Controls.Add(this.labelX44);
            this.groupBox1.Controls.Add(this.txttejarizarib);
            this.groupBox1.Controls.Add(this.labelX46);
            this.groupBox1.Controls.Add(this.txtEnd);
            this.groupBox1.Controls.Add(this.txtStart);
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Controls.Add(this.btn_create_new_doreh);
            this.groupBox1.Controls.Add(this.txt_mohlat);
            this.groupBox1.Controls.Add(this.labelX5);
            this.groupBox1.Controls.Add(this.labelX3);
            this.groupBox1.Controls.Add(this.lbldoreh);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(809, 315);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "تمدید زمان پایان دوره";
            // 
            // txtEnd
            // 
            this.txtEnd.IsReadonly = true;
            this.txtEnd.Location = new System.Drawing.Point(25, 34);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Readonly = true;
            this.txtEnd.Size = new System.Drawing.Size(149, 21);
            this.txtEnd.TabIndex = 30;
            this.txtEnd.Tag = "";
            this.txtEnd.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txtStart
            // 
            this.txtStart.IsReadonly = true;
            this.txtStart.Location = new System.Drawing.Point(277, 34);
            this.txtStart.Name = "txtStart";
            this.txtStart.Readonly = true;
            this.txtStart.Size = new System.Drawing.Size(149, 21);
            this.txtStart.TabIndex = 29;
            this.txtStart.Tag = "txtEnd";
            this.txtStart.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(432, 32);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(99, 24);
            this.labelX1.TabIndex = 28;
            this.labelX1.Text = "تاریخ شروع دوره :";
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(25, 273);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(199, 24);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 27;
            this.btn_create_new_doreh.Text = "ثبت تغییرات دوره";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // txt_mohlat
            // 
            this.txt_mohlat.Location = new System.Drawing.Point(506, 79);
            this.txt_mohlat.Name = "txt_mohlat";
            this.txt_mohlat.Size = new System.Drawing.Size(149, 21);
            this.txt_mohlat.TabIndex = 26;
            this.txt_mohlat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(671, 78);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 24);
            this.labelX5.TabIndex = 25;
            this.labelX5.Text = "مهلت پرداخت:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(180, 32);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 24);
            this.labelX3.TabIndex = 23;
            this.labelX3.Text = "تاریخ پایان :";
            // 
            // lbldoreh
            // 
            this.lbldoreh.BackColor = System.Drawing.Color.White;
            this.lbldoreh.ForeColor = System.Drawing.Color.Black;
            this.lbldoreh.Location = new System.Drawing.Point(644, 33);
            this.lbldoreh.Name = "lbldoreh";
            this.lbldoreh.Size = new System.Drawing.Size(40, 23);
            this.lbldoreh.TabIndex = 1;
            this.lbldoreh.Text = "0";
            this.lbldoreh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(708, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "دوره :";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(297, 211);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(96, 27);
            this.labelX13.TabIndex = 48;
            this.labelX13.Text = "مصرف عمومی :";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(577, 194);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(169, 27);
            this.labelX9.TabIndex = 46;
            this.labelX9.Text = "ضریب آبونمان :";
            // 
            // labelX44
            // 
            this.labelX44.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX44.ForeColor = System.Drawing.Color.Black;
            this.labelX44.Location = new System.Drawing.Point(603, 157);
            this.labelX44.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX44.Name = "labelX44";
            this.labelX44.Size = new System.Drawing.Size(143, 27);
            this.labelX44.TabIndex = 44;
            this.labelX44.Text = "ضریب در حال ساخت و ساز:";
            // 
            // labelX46
            // 
            this.labelX46.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX46.ForeColor = System.Drawing.Color.Black;
            this.labelX46.Location = new System.Drawing.Point(609, 122);
            this.labelX46.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX46.Name = "labelX46";
            this.labelX46.Size = new System.Drawing.Size(137, 27);
            this.labelX46.TabIndex = 42;
            this.labelX46.Text = "ضریب تجاری آب :";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(297, 145);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(96, 27);
            this.labelX11.TabIndex = 50;
            this.labelX11.Text = "مبلغ فاضلاب :";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(287, 177);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(106, 27);
            this.labelX10.TabIndex = 53;
            this.labelX10.Text = "درصد مالیات :";
            // 
            // txt_mablagh
            // 
            this.txt_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mablagh.Border.Class = "TextBoxBorder";
            this.txt_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_mablagh.Location = new System.Drawing.Point(180, 112);
            this.txt_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_mablagh.Name = "txt_mablagh";
            this.txt_mablagh.PreventEnterBeep = true;
            this.txt_mablagh.Size = new System.Drawing.Size(111, 26);
            this.txt_mablagh.TabIndex = 52;
            this.txt_mablagh.TextChanged += new System.EventHandler(this.txt_mablagh_TextChanged);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(318, 113);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 27);
            this.labelX4.TabIndex = 51;
            this.labelX4.Text = "آب بها :";
            this.labelX4.Click += new System.EventHandler(this.labelX4_Click);
            // 
            // txtDarsadMaliat
            // 
            this.txtDarsadMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDarsadMaliat.Border.Class = "TextBoxBorder";
            this.txtDarsadMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDarsadMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtDarsadMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtDarsadMaliat.Location = new System.Drawing.Point(180, 176);
            this.txtDarsadMaliat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtDarsadMaliat.Name = "txtDarsadMaliat";
            this.txtDarsadMaliat.PreventEnterBeep = true;
            this.txtDarsadMaliat.Size = new System.Drawing.Size(111, 26);
            this.txtDarsadMaliat.TabIndex = 54;
            this.txtDarsadMaliat.Text = ".0";
            // 
            // txt_fazelab_mablagh
            // 
            this.txt_fazelab_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_fazelab_mablagh.Border.Class = "TextBoxBorder";
            this.txt_fazelab_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_fazelab_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_fazelab_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_fazelab_mablagh.Location = new System.Drawing.Point(178, 144);
            this.txt_fazelab_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_fazelab_mablagh.Name = "txt_fazelab_mablagh";
            this.txt_fazelab_mablagh.PreventEnterBeep = true;
            this.txt_fazelab_mablagh.Size = new System.Drawing.Size(113, 26);
            this.txt_fazelab_mablagh.TabIndex = 49;
            this.txt_fazelab_mablagh.Text = ".0";
            // 
            // txt_masraf_omoomi
            // 
            this.txt_masraf_omoomi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_masraf_omoomi.Border.Class = "TextBoxBorder";
            this.txt_masraf_omoomi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_masraf_omoomi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_masraf_omoomi.ForeColor = System.Drawing.Color.Black;
            this.txt_masraf_omoomi.Location = new System.Drawing.Point(178, 210);
            this.txt_masraf_omoomi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_masraf_omoomi.Name = "txt_masraf_omoomi";
            this.txt_masraf_omoomi.PreventEnterBeep = true;
            this.txt_masraf_omoomi.Size = new System.Drawing.Size(113, 26);
            this.txt_masraf_omoomi.TabIndex = 47;
            this.txt_masraf_omoomi.Text = ".0";
            // 
            // txt_aboonman
            // 
            this.txt_aboonman.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_aboonman.Border.Class = "TextBoxBorder";
            this.txt_aboonman.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_aboonman.DisabledBackColor = System.Drawing.Color.White;
            this.txt_aboonman.ForeColor = System.Drawing.Color.Black;
            this.txt_aboonman.Location = new System.Drawing.Point(530, 193);
            this.txt_aboonman.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_aboonman.Name = "txt_aboonman";
            this.txt_aboonman.PreventEnterBeep = true;
            this.txt_aboonman.Size = new System.Drawing.Size(61, 26);
            this.txt_aboonman.TabIndex = 45;
            this.txt_aboonman.Text = ".0";
            // 
            // txtZaribSakhtoSaz
            // 
            this.txtZaribSakhtoSaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtZaribSakhtoSaz.Border.Class = "TextBoxBorder";
            this.txtZaribSakhtoSaz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtZaribSakhtoSaz.DisabledBackColor = System.Drawing.Color.White;
            this.txtZaribSakhtoSaz.ForeColor = System.Drawing.Color.Black;
            this.txtZaribSakhtoSaz.Location = new System.Drawing.Point(530, 156);
            this.txtZaribSakhtoSaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtZaribSakhtoSaz.Name = "txtZaribSakhtoSaz";
            this.txtZaribSakhtoSaz.PreventEnterBeep = true;
            this.txtZaribSakhtoSaz.Size = new System.Drawing.Size(61, 26);
            this.txtZaribSakhtoSaz.TabIndex = 43;
            this.txtZaribSakhtoSaz.Text = ".0";
            // 
            // txttejarizarib
            // 
            this.txttejarizarib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txttejarizarib.Border.Class = "TextBoxBorder";
            this.txttejarizarib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txttejarizarib.DisabledBackColor = System.Drawing.Color.White;
            this.txttejarizarib.ForeColor = System.Drawing.Color.Black;
            this.txttejarizarib.Location = new System.Drawing.Point(530, 121);
            this.txttejarizarib.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txttejarizarib.Name = "txttejarizarib";
            this.txttejarizarib.PreventEnterBeep = true;
            this.txttejarizarib.Size = new System.Drawing.Size(61, 26);
            this.txttejarizarib.TabIndex = 41;
            this.txttejarizarib.Text = ".0";
            // 
            // FrmTamdid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 340);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTamdid";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmTamdid_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmTamdid_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private FarsiLibrary.Win.Controls.FADatePicker txt_mohlat;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private FarsiLibrary.Win.Controls.FADatePicker txtStart;
        private DevComponents.DotNetBar.LabelX labelX1;
        private FarsiLibrary.Win.Controls.FADatePicker txtEnd;
        private System.Windows.Forms.Label lbldoreh;
        private System.Windows.Forms.Label label1;
        private FloatTextBox txt_masraf_omoomi;
        private DevComponents.DotNetBar.LabelX labelX13;
        private FloatTextBox txt_aboonman;
        private DevComponents.DotNetBar.LabelX labelX9;
        private FloatTextBox txtZaribSakhtoSaz;
        private DevComponents.DotNetBar.LabelX labelX44;
        private FloatTextBox txttejarizarib;
        private DevComponents.DotNetBar.LabelX labelX46;
        private FloatTextBox txt_fazelab_mablagh;
        private DevComponents.DotNetBar.LabelX labelX11;
        private FloatTextBox txtDarsadMaliat;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mablagh;
        private DevComponents.DotNetBar.LabelX labelX4;
    }
}