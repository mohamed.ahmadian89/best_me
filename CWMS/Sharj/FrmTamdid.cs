﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Sharj
{
    public partial class FrmTamdid : DevComponents.DotNetBar.Metro.MetroForm
    {
        string tableName = "";
        bool IsGhobbozSaderShodeh = false;
        public FrmTamdid(string dcode, DateTime start, DateTime end, DateTime mohlat, string table, bool
            SoddorGhabz,
            decimal z_tejari,decimal z_sakhtosaz,decimal z_aboonman,
            decimal ab_baha,decimal fazelab,decimal maliat,decimal masraf_omoomi)
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();

            txtStart.SelectedDateTime = start;
            txtEnd.SelectedDateTime = end;
            txt_mohlat.SelectedDateTime = mohlat;
            lbldoreh.Text = dcode;
            tableName = table;
            IsGhobbozSaderShodeh = SoddorGhabz;

            txtDarsadMaliat.Text = maliat.ToString();
            txttejarizarib.Text = z_tejari.ToString();
            txtZaribSakhtoSaz.Text = z_sakhtosaz.ToString();
            txt_aboonman.Text = z_aboonman.ToString();

            txt_fazelab_mablagh.Text = fazelab.ToString();
            txt_mablagh.Text = ab_baha.ToString();
            txt_masraf_omoomi.Text = masraf_omoomi.ToString();




        }

        private void FrmTamdid_Load(object sender, EventArgs e)
        {

        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            //if (tableName == "شارژ")
            //    if (Classes.clsMoshtarekin.UserInfo.p69)
            //    {

            //        Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 69);

            //        try
            //        {
            //            Convert.ToDateTime(txt_mohlat.SelectedDateTime);
            //        }
            //        catch (Exception)
            //        {
            //            Payam.Show("لطفا مهلت پراخت را درست وارد کنید");
            //        }


            //        if (txtEnd.SelectedDateTime < txtStart.SelectedDateTime)
            //            Payam.Show("تاریخ پایان دوره نمی تواند از تاریخ شروع دوره کمتر باشد");
            //        else
            //        {
            //            Classes.ClsMain.ChangeCulture("e");
            //            Classes.ClsMain.ExecuteNoneQuery("update " + tableName + "_doreh set mohlat_pardakht='" + txt_mohlat.SelectedDateTime + "'  where dcode=" + lbldoreh.Text);
            //            if (IsGhobbozSaderShodeh)
            //            {
            //                Classes.ClsMain.ExecuteNoneQuery("update " + tableName + "_ghabz set mohlat_pardakht='" + txt_mohlat.SelectedDateTime + "'  where dcode=" + lbldoreh.Text);

            //            }
            //            Classes.ClsMain.ChangeCulture("f");
            //            Payam.Show("اطلاعات با موفقیت بروزرسانی شد");
            //            this.Close();

            //        }
            //    }
            //    else
            //        Payam.Show("شما مجوز انجام این کار را ندارید");
            //else
            //{
            if (Classes.clsMoshtarekin.UserInfo.p68)
            {


                try
                {




                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 68);

                    try
                    {
                        Convert.ToDateTime(txt_mohlat.SelectedDateTime);
                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا مهلت پراخت را درست وارد کنید");
                    }


                    if (txtEnd.SelectedDateTime < txtStart.SelectedDateTime)
                        Payam.Show("تاریخ پایان دوره نمی تواند از تاریخ شروع دوره کمتر باشد");
                    else
                    {
                        Classes.ClsMain.ChangeCulture("e");
                        Classes.ClsMain.ExecuteNoneQuery("update ab_doreh set mohlat_pardakht='" + txt_mohlat.SelectedDateTime + "'"
                            + ",masraf_omoomi=" + txt_masraf_omoomi.Text + ",mablagh_fazelab=" + txt_fazelab_mablagh.Text
                            + ",zarib_aboonman=" + txt_aboonman.Text + ",mablagh_ab=" + txt_mablagh.Text + ",ZaribTejari=" + txttejarizarib.Text
                            + ",ZaribSakhtoSaz=" + txtZaribSakhtoSaz.Text + ",darsad_maliat=" + txtDarsadMaliat.Text



                            + " where dcode=" + lbldoreh.Text);

                        Classes.ClsMain.ChangeCulture("f");
                        Payam.Show("اطلاعات با موفقیت بروزرسانی شد");
                        this.Close();

                    }
                }
                catch (Exception)
                {

                    Payam.Show("لطفا اطلاعات را به درستی وارد نمایید");
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmTamdid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void labelX4_Click(object sender, EventArgs e)
        {

        }

        private void txt_mablagh_TextChanged(object sender, EventArgs e)
        {

        }
    }
}