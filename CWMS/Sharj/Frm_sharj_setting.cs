﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
using System.Data.SqlClient;

namespace CWMS
{
    public partial class Frm_sharj_setting : MyMetroForm
    {
        public Frm_sharj_setting()
        {
            Classes.ClsMain.ChangeCulture("e");
            InitializeComponent();
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
            this.Close();

        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p76)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 76);

                int auto = 0;
                if (ch_auto.Checked == true)
                    auto = 1;

                if (txt_mah.Text.Trim() == "")
                    txt_mah.Text = "2";

                if (txt_mohlat.Text.Trim() == "")
                    txt_mohlat.Text = "10";
                if (txttejarizarib.Text.Trim() == "")
                    txttejarizarib.Text = "0";
                if (txtDardadMaliat.Text.Trim() == "")
                    txtDardadMaliat.Text = "0";


                try
                {
                    if (Classes.clsSharj.IsSharjInfoExist() == false)
                        Classes.ClsMain.ExecuteScalar("insert into sharj_info (id) values (1)");
                    string com = "update sharj_info set tozihat=N'" + txt_tozihat.Text + "',ejraye_automat=" + auto.ToString() + ",tedad_mah_dar_doreh=" + txt_mah.Text + ",mohlat_pardakht=" + txt_mohlat.Text + ",zaribtejari=" + txttejarizarib.Text + ",darsad_maliat=" + txtDardadMaliat.Text;
                    Classes.ClsMain.ExecuteNoneQuery(com);
                    Payam.Show("تغییرات با موفقیت در سیستم ثبت شد");
                    this.Close();
                }
                catch
                {
                    Payam.Show("مقادیر ورودی نامعتبر می باشد . لطفا بررسی نمایید");

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void Frm_doreh_sharj_period_Load(object sender, EventArgs e)
        {
            DataTable dt = Classes.ClsMain.GetDataTable("select * from sharj_info");
            if (dt.Rows.Count != 0)
            {
                txt_tozihat.Text = dt.Rows[0]["tozihat"].ToString();
                txt_mah.Text = dt.Rows[0]["tedad_mah_dar_doreh"].ToString();
                txt_mohlat.Text = dt.Rows[0]["mohlat_pardakht"].ToString();
                txttejarizarib.Text = dt.Rows[0]["zaribtejari"].ToString();
                txtDardadMaliat.Text = dt.Rows[0]["darsad_maliat"].ToString();
                ch_auto.Checked = (bool)dt.Rows[0]["ejraye_automat"];




            }


        }

        private void btn_first_sharj_Click(object sender, EventArgs e)
        {


        }

        private void Frm_doreh_sharj_period_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txttejarizarib_TextChanged(object sender, EventArgs e)
        {

        }
    }
}