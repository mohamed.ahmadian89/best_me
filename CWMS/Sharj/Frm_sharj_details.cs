﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Globalization;
using CrystalDecisions.Windows.Forms;
using FarsiLibrary.Utils;

namespace CWMS
{
    public partial class Frm_sharj_details : MyMetroForm
    {
        bool flag; // این مقدار برای چک کردن صحت اطلاعات فرم قبل از ذخیره در دیتابیس درنظر گرفته شده

        string Gharardad = "";
        float DarsadMaliat = 0;

        float ZaribTejari = 0;

        Boolean IsNewGhabz = false;
        bool was_bestankar_in_sodoorGhabz = false;
        long bestankari_from_last_doreh = 0;

        DateTime? last_pardakht = null;
        int has_pardakhti = 0;


        void LoadGhabzInfo(string gcode)
        {
            DataTable dt_info = Classes.clsSharj.get_akharin_ghabz(gcode);
            if (dt_info.Rows.Count > 0)
            {
                try
                {
                    chtejari.Checked = false;
                    cmb_vaziat.SelectedIndex = Convert.ToInt32(dt_info.Rows[0]["vaziat"]);
                    txt_ghabz_code.Text = dt_info.Rows[0]["gcode"].ToString();
                    txtdoreh.Text = dt_info.Rows[0]["dcode"].ToString();
                    txtMablagh.Tag = txtMablagh.Text = dt_info.Rows[0]["mablagh"].ToString();
                    txt_takhfif.Text= dt_info.Rows[0]["sharj_takhfif"].ToString();

                    txtMablaghKol.Text = dt_info.Rows[0]["mablaghkol"].ToString();
                    txtMaliat.Text = dt_info.Rows[0]["maliat"].ToString();
                    txt_sayer.Text = dt_info.Rows[0]["sayer"].ToString();
                    txt_sayer_tozihat.Text = dt_info.Rows[0]["sayer_tozihat"].ToString();
                    txtMablaghHoroof.Text = Classes.clsNumber.Number_to_Str(txtMablaghKol.Text);
                    txtMetraj.Text = dt_info.Rows[0]["metraj"].ToString();
                    txtMohlat.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["mohlat_pardakht"].ToString());
                    txtTarefe.Text = dt_info.Rows[0]["tarefe"].ToString();
                    txtBedehi.Text = dt_info.Rows[0]["bedehi"].ToString();
                    txtDarsadMaliat.Text = dt_info.Rows[0]["darsad_maliat"].ToString();
                    DarsadMaliat = Convert.ToSingle(dt_info.Rows[0]["darsad_maliat"]);
                    txtTarikhSoddor.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["tarikh"].ToString());
                    txt_jarimeh.Text = dt_info.Rows[0]["jarimeh"].ToString();
                    txtJarimehTozihat.Text = dt_info.Rows[0]["jarimeh_tozihat"].ToString();
                    txt_kasr.Text = dt_info.Rows[0]["kasr_hezar"].ToString();
                    txt_shenase_ghabz.Text = dt_info.Rows[0]["shenase_ghabz"].ToString();
                    txt_shenase_pardakht.Text = dt_info.Rows[0]["shenase_pardakht"].ToString();



                    was_bestankar_in_sodoorGhabz = Convert.ToBoolean(dt_info.Rows[0]["was_bestankar_in_sodoorGhabz"]);
                    try
                    {
                        last_pardakht = Convert.ToDateTime(dt_info.Rows[0]["last_pardakht"]);
                        has_pardakhti = 1;
                    }
                    catch (Exception)
                    {
                        last_pardakht = null;
                        has_pardakhti = 0;
                    }
                    txt_tozihat.Text = dt_info.Rows[0]["tozihat"].ToString();
                    txBaghimandeh.Text = dt_info.Rows[0]["mande"].ToString();



                    // مبلغ بستانکاری که ازدوره قبل به این دوره منتقل شده است 
                    try
                    {
                        int doreh = Convert.ToInt32(txtdoreh.Text);
                        if (doreh == 1)
                        {
                            TxtBestankariFromLastDoreh.Text = Classes.ClsMain.ExecuteScalar("select bestankar from sharj_bedehi_bestankar_First_time where gharardad=" + dt_info.Rows[0]["gharardad"].ToString()).ToString();
                            txt_tozihat.Text = "مبلغ پرداختی از اطلاعات وارد شده در قسمت راه اندازی اولیه سیستم - اطلاعات اولیه شارژ  استخراج شده است ";
                        }
                        else
                            TxtBestankariFromLastDoreh.Text = Classes.ClsMain.ExecuteScalar("select bestankari from sharj_ghabz where dcode=" + (doreh - 1) + " and gharardad=" + dt_info.Rows[0]["gharardad"].ToString()).ToString();

                    }
                    catch (Exception)
                    {

                        TxtBestankariFromLastDoreh.Text = "0";
                    }



                    // مبلغ بستانکاری مربوط به دوره فعلی

                    if (Convert.ToInt64(dt_info.Rows[0]["bestankari"]) > 0)
                    {
                        lblBestankar.Visible = txtBestankar.Visible = true;
                        txtBestankar.Text = dt_info.Rows[0]["bestankari"].ToString();

                    }
                    else
                    {
                        txtBestankar.Text = "0";
                        lblBestankar.Visible = txtBestankar.Visible = false;

                    }

                    txtPardakhti.Text = (Convert.ToInt64(dt_info.Rows[0]["mablaghkol"]) -
                        (Convert.ToInt64(dt_info.Rows[0]["mande"]) + Convert.ToInt64(txt_kasr.Text))
                        ).ToString();




                    bool Istejari = Convert.ToBoolean(dt_info.Rows[0]["tejari"]);
                    chtejari.Checked = Istejari;


                    DataTable dt_doreh = Classes.ClsMain.GetDataTable("select ZaribTejari, start_time, end_time from sharj_doreh where dcode=" + dt_info.Rows[0]["dcode"].ToString());
                    pinfo3.Text = dt_doreh.Rows[0]["ZaribTejari"].ToString();
                    ZaribTejari = Convert.ToSingle(pinfo3.Text);


                    startOfDoreh = Convert.ToDateTime(dt_doreh.Rows[0]["start_time"]);
                    endOfDoreh = Convert.ToDateTime(dt_doreh.Rows[0]["end_time"]);
                    int days = (int)(dt_doreh.Rows[0].Field<DateTime>("end_time") -
                        dt_doreh.Rows[0].Field<DateTime>("start_time")).TotalDays;
                    txtDayes.Tag = days.ToString();
                    txtDayes.Text = dt_info.Rows[0]["tedadRooz"].ToString();


                }
                catch (Exception e1)
                {
                    Payam.Show("متاسفانه در هنگام نمایش اطلاعات قبض خطا رخ داده است ");
                    Classes.ClsMain.logError(e1);
                }
            }
        }


        public Frm_sharj_details(string gcode, string GharardadTemp)
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            Gharardad = GharardadTemp;
            LoadGhabzInfo(gcode);
            txtGharardad.Text = Gharardad.ToString();
            
        }



        int lastDcode = 0;

        int TedadRoozDoreh = 0;

        DateTime startOfDoreh;
        DateTime endOfDoreh;


        private void txtMaliat_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt64(txtMaliat.Text);
            }
            catch
            {
                txtMaliat.Text = "0";
            }

      

        }

        private void txt_sayer_Leave(object sender, EventArgs e)
        {

            try
            {
                Convert.ToInt64(txt_sayer.Text);
            }
            catch
            {
                txt_sayer.Text = "0";
            }
       

        }



        private void btn_save_Click(object sender, EventArgs e)
        {
            if (txt_sayer.Text == "")
                txt_sayer.Text = "0";
            if (!flag)
                return;

            if (Classes.clsMoshtarekin.UserInfo.p57)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 57, txt_ghabz_code.Text);

          

                try
                {
                    Convert.ToSingle(txtDarsadMaliat.Text, CultureInfo.InvariantCulture);
                    
                    Classes.clsSharjGhabz ghabzInfo = new Classes.clsSharjGhabz(Convert.ToInt32(Gharardad));
                    ghabzInfo.was_bestankar_in_sodoorGhabz = was_bestankar_in_sodoorGhabz;
                    ghabzInfo.darsad_maliat = Convert.ToDecimal(txtDarsadMaliat.Text, CultureInfo.InvariantCulture);

                    ghabzInfo.is_Updating_ghabz = true;
                    ghabzInfo.gharardad = Convert.ToInt32(Gharardad);
                    ghabzInfo.doreh = Convert.ToInt32(txtdoreh.Text);
                    ghabzInfo.Tarefe = Convert.ToInt32(txtTarefe.Text);
                    ghabzInfo.tejari = chtejari.Checked;
                    ghabzInfo.metraj = Convert.ToDecimal(txtMetraj.Text, CultureInfo.InvariantCulture);
                    ghabzInfo.tarikh_sodoor = Convert.ToDateTime(txtTarikhSoddor.SelectedDateTime);
                    try
                    {
                        ghabzInfo.mohlat_pardakht_ghabz = Convert.ToDateTime(txtMohlat.SelectedDateTime);

                    }
                    catch (Exception)
                    {
                        Payam.Show("لطفا مهلت پرداخت را به صورت صحیح وارد نمایید");
                        txtMohlat.SelectedDateTime = DateTime.Now;
                        txtMohlat.Focus();
                    }

                    ghabzInfo.last_pardakht = last_pardakht;
                    
                    ghabzInfo.tedad_rooz = Convert.ToInt32(txtDayes.Text);
                    ghabzInfo.hazineh_jarimeh = Convert.ToDecimal(txt_jarimeh.Text);
                    ghabzInfo.hazineh_sayer = Convert.ToDecimal(txt_sayer.Text);
                    ghabzInfo.hazineh_mablagh = Convert.ToDecimal(txtMablagh.Text);
                    ghabzInfo.tozihat = txt_tozihat.Text;
                    ghabzInfo.tozihat_sayer = txt_sayer_tozihat.Text;
                    ghabzInfo.tozihat_jarimeh = txtJarimehTozihat.Text;


                    try
                    {
                        ghabzInfo.kasr_hezar_from_last_doreh = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select kasr_hezar from sharj_ghabz where gharardad=" + ghabzInfo.gharardad
                                     + " and dcode=" + (ghabzInfo.doreh - 1).ToString()));
                    }
                    catch (Exception)
                    {
                        ghabzInfo.kasr_hezar_from_last_doreh = 0;
                    }
                    

                    ghabzInfo.bedehkar = Convert.ToDecimal(txtBedehi.Text);
                    ghabzInfo.bestankar = Convert.ToDecimal(TxtBestankariFromLastDoreh.Text);

                    // اگر کاربر مبلغ قبض ان صفر باشد و مبلغ بستانکاری مقداری مخالف صفر  داشته باشد 
                    // در این حالت کاربر بواسطه بستانکاری از دوره قبل ،
                    // مبلغ قبض آن صفر شده است و باقیمانده ان به عنوان بستانکار در فیلد بستانکار ذخیره شده است 


                    Classes.clsSharj SharjClass = new Classes.clsSharj();
                    SharjClass.Generate_Ghabz(ghabzInfo);


                    //if(Classes.clsFactor.Has_factor(ghabzInfo.gcode.ToString()))
                    //{
                    //    //================== فاکتور ============================
                    //    string table_name = "sharj_ghabz";
                    //    int ghabz_type = 1;
                    //    DataTable dt_ghabz_info = Classes.ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + ghabzInfo.gcode);

                    //    Classes.clsFactor factor_class = new Classes.clsFactor(
                    //        Classes.clsFactor.Get_Max_factor_num(), Convert.ToInt32(ghabzInfo.gharardad),
                    //        Convert.ToInt32(ghabzInfo.gharardad), Convert.ToInt32(ghabzInfo.doreh),
                    //        0, ghabzInfo.darsad_maliat, table_name, ghabz_type, "ارائه خدمات شارژ دوره" + ghabzInfo.doreh);
                    //    factor_class.dt_ghabz = dt_ghabz_info;
                    //    factor_class.tarikh = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select tarikh from factors where gcode=" + ghabzInfo.gcode));
                    //    factor_class.Start_genete_ghabz();
                    //    //================== فاکتور ============================
                    //}

                    Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
                    LoadGhabzInfo(txt_ghabz_code.Text);
                    Classes.ClsMain.ChangeCulture("f");
                }
                catch (Exception ex)
                {
                    Payam.Show("خطا در ثبت اطلاعات قبض . لطفا با شرکت پشتیبان تماس حاصل فرمایید");
                    Classes.ClsMain.logError(ex);
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void Frm_sharj_details_Load(object sender, EventArgs e)
        {
            flag = true;
        }



        private void Frm_sharj_details_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            frmMainRefrence.Update_Moshtarek_Status();

        }





        private void buttonX2_Click(object sender, EventArgs e)
        {

            if (Classes.clsMoshtarekin.UserInfo.p58)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 58);

                Classes.ClsMain.ChangeCulture("e");
                Sharj.FrmGhabzPardakht frm = new Sharj.FrmGhabzPardakht(false, txt_ghabz_code.Text, txtdoreh.Text, Gharardad, txtMablaghKol.Text, txtMablaghHoroof.Text, Convert.ToInt64(TxtBestankariFromLastDoreh
                    .Text), txtTarikhSoddor.Text.ToString());
                frm.ShowDialog();
                Classes.ClsMain.ChangeCulture("f");
                LoadGhabzInfo(txt_ghabz_code.Text);

              



            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

 
        static long GetRoundedInteger(decimal number)
        {
            return Convert.ToInt64(Math.Round(number));
        }


        private void buttonX3_Click(object sender, EventArgs e)
        {
            int newDays;
            if (Int32.TryParse(txtDayes.Text, out newDays))
            {
                if (newDays > Convert.ToInt32(txtDayes.Tag))
                {
                    flag = false;
                    Payam.Show("تعداد روزها بیشتر از تعداد روزهای دوره میباشد");
                }
                else if (newDays < 0)
                {
                    flag = false;
                    Payam.Show("عدد وارد شده برای تعداد روزها معتبر نیست. لطفا تصحیح نمایید");
                }
                else
                {
                    decimal Moshtarek_Mablagh_sharj = 0 ,sharj_takhfif=0;
                    DataTable dt_doreh_info= Classes.ClsMain.GetDataTable("select start_time,end_time from sharj_doreh where dcode=" + txtdoreh.Text);
                    DateTime DorehStartDate = Convert.ToDateTime(dt_doreh_info.Rows[0][0]);
                    DateTime DorehEndDate = Convert.ToDateTime(dt_doreh_info.Rows[0][1]);
                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.abbas_abad)
                    {
                        Classes.shahrak_classes.abbas_abad.Get_hazineh_Sharj(ref Moshtarek_Mablagh_sharj,
                            DorehStartDate, Convert.ToDecimal(txtTarefe.Text), Convert.ToDecimal(txtMetraj.Text), newDays);
                    }
                    else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
                    {
                        Classes.shahrak_classes.paytakht.Get_hazineh_Sharj(ref Moshtarek_Mablagh_sharj,
                            DorehStartDate, Convert.ToDecimal(txtTarefe.Text), Convert.ToDecimal(txtMetraj.Text), newDays);
                    }
                    else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {
                        DateTime tarikh_gharardad = DateTime.Now;
                        try
                        {
                            tarikh_gharardad = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select tarikh_gharardad from moshtarekin where gharardad=" + txtGharardad.Text));
                        }
                        catch (Exception)
                        {
                            Payam.Show("لطفا ابتدا به صفحه اطلاعات مشترکین رفته و  تاریخ قرارداد مشترک را مشخص نمایید");
                            flag = false;
                            return;
                        }
                        Classes.shahrak_classes.esf_bozorg.Get_hazineh_Sharj(ref Moshtarek_Mablagh_sharj,Convert.ToDecimal(txtMetraj.Text),ref sharj_takhfif,tarikh_gharardad,DorehStartDate,DorehEndDate, txt_ghabz_code.Text);
                    }

                 
                    if (chtejari.Checked)
                    {
                        Moshtarek_Mablagh_sharj = GetRoundedInteger(Moshtarek_Mablagh_sharj * (decimal) ZaribTejari);
                    }


                    txtMablagh.Text = Moshtarek_Mablagh_sharj.ToString();
                    flag = true;
                    txt_takhfif.Text = sharj_takhfif.ToString();
                }
            }
            else
            {
                flag = false;
                Payam.Show("عدد وارد شده برای تعداد روزها معتبر نیست. لطفا تصحیح نمایید");
            }
        }


        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p45)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 45, txt_ghabz_code.Text);

                if (txt_ghabz_code.Text.Trim() != "")
                {
                    CrystalReportViewer repVUer = new CrystalReportViewer();

                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {
                        int ab_dcode = 0;
                        try
                        {
                            ab_dcode = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select max(dcode) from ab_ghabz where gharardad=" + Gharardad));
                        }
                        catch (Exception)
                        {
                            ab_dcode = 0;
                        }


                        Classes.shahrak_classes.esf_bozorg.Print_Ghabz
                            (
                            Convert.ToInt32(Gharardad),
                             Convert.ToInt32(txtdoreh.Text),
                             ab_dcode
                             );
                    }
                    else
                    {
                        Classes.clsSharj.print_Ghabz(
                        txt_ghabz_code.Text,
                        Gharardad,


                         "print"
                         , false
                        );
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }





        private void Frm_sharj_details_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p45)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 45, txt_ghabz_code.Text);

                if (txt_ghabz_code.Text.Trim() != "")
                {
                    CrystalReportViewer repVUer = new CrystalReportViewer();


                    Classes.clsSharj.print_Ghabz(
                        txt_ghabz_code.Text,
                        Gharardad,


                         "print"
                         , true
                        );
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            Payam.Show("به زودی این قابلیت به سیستم اضافه خواهد شد ");
        }
    }
}