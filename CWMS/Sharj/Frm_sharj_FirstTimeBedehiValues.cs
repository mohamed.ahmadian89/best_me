﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Sharj
{
    public partial class Frm_sharj_FirstTimeBedehiValues : MyMetroForm
    {
        public Frm_sharj_FirstTimeBedehiValues()
        {
            InitializeComponent();
        }

        private void FrmFirstTimeKontorValues_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset.sharj_bedehi_bestankar_First_time' table. You can move, or remove it, as needed.
            this.sharj_bedehi_bestankar_First_timeTableAdapter.Fill(this.mainAbDataset.sharj_bedehi_bestankar_First_time);
            // TODO: This line of code loads data into the 'mainAbDataset.sharj_bedehi_from_Past' table. You can move, or remove it, as needed.

            // TODO: This line of code loads data into the 'mainAbDataset.kontor_first_time_values' table. You can move, or remove it, as needed.
            if (this.mainAbDataset.sharj_bedehi_bestankar_First_time.Rows.Count == 0)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from sharj_bedehi_bestankar_First_time;insert into sharj_bedehi_bestankar_First_time select gharardad, 0,0 from moshtarekin ");
                this.sharj_bedehi_bestankar_First_timeTableAdapter.Fill(this.mainAbDataset.sharj_bedehi_bestankar_First_time);

            }
           

        }

        private void btnReadKontorKhanAndUpdate_Click(object sender, EventArgs e)
        {
   

            sharjbedehibestankarFirsttimeBindingSource.EndEdit();
            bool Continue = true;
            for (int i = 0; i < dgv_info.Rows.Count; i++)
            {
                if ((Convert.ToDecimal(dgv_info.Rows[i].Cells[2].Value) > 0 && Convert.ToDecimal(dgv_info.Rows[i].Cells[1].Value) != 0) ||
                    (Convert.ToDecimal(dgv_info.Rows[i].Cells[1].Value) > 0 && Convert.ToDecimal(dgv_info.Rows[i].Cells[2].Value) != 0))
                {
                    Payam.Show("مقادیر وارد شده برای مشترک :" + dgv_info.Rows[i].Cells[0].Value.ToString() + " معتبر نمی باشد ");
                    Continue = false;
                    break;
                }
            
            }
            if(Continue)
            {
                sharj_bedehi_bestankar_First_timeTableAdapter.Update(this.mainAbDataset.sharj_bedehi_bestankar_First_time);
                Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");

            }
        }

        private void txtFindByGhararda_TextChanged(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text != "")
                sharjbedehibestankarFirsttimeBindingSource.Filter = "gharardad =" + txtFindByGhararda.Text;
            else
                sharjbedehibestankarFirsttimeBindingSource.RemoveFilter();
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            sharjbedehibestankarFirsttimeBindingSource.RemoveFilter();
        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text != "")
                sharjbedehibestankarFirsttimeBindingSource.Filter = "gharardad =" + txtFindByGhararda.Text;

        }

        private void FrmFirstTimeKontorValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtFindByGhararda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gharardad.PerformClick();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if(Cpayam.Show("ایا با ایجاد لیست دوباره مشترکین موافق هستید?")==DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from ab_bedehi_bestankar_First_time");
                string com = "insert into ab_bedehi_bestankar_First_time values ";
                DataTable dt_moshtarekin = Classes.ClsMain.GetDataTable("select gharardad from moshtarekin");
                foreach (DataRow item in dt_moshtarekin.Rows)
                {
                    com += "(" + item[0].ToString() + ",0,0,0,'" + DateTime.Now + "'),";
                }
                com = com.Substring(0, com.Length - 1);
                Classes.ClsMain.ExecuteNoneQuery(com);
            }
        }
    }
}