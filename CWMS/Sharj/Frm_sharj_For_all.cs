﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;

namespace CWMS
{
    public partial class Frm_sharj_For_all : MyMetroForm
    {
        public Frm_sharj_For_all()
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
        }
        public Frm_sharj_For_all(string dcode)
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
            cmbDoreh.SelectedValue = dcode;
            btn_find.PerformClick();




        }

        private void Frm_all_Sharj_ghabz_for_all_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.sharj_ghabz' table. You can move, or remove it, as needed.

            // TODO: This line of code loads data into the 'cWMSDataSet.sharj_ghabz' table. You can move, or remove it, as needed.
            cmbDoreh.Focus();
            Classes.ClsMain.ChangeCulture("f");

        }

        void Amar_information()
        {
            double Maliat = 0, Bedehi = 0, mablaghkol = 0, sayer = 0, jarimeh = 0;
            for (int i = 0; i < this.mainDataSest.sharj_ghabz.Rows.Count; i++)
            {

                Maliat += Convert.ToDouble(this.mainDataSest.sharj_ghabz.Rows[i]["maliat"]);



                //if (this.mainDataSest.sharj_ghabz.Rows[i]["vaziat"].ToString() == "0")
                //    sum_nashodeh += Convert.ToDouble(this.mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());
                //else if (this.mainDataSest.sharj_ghabz.Rows[i]["vaziat"].ToString() == "1")
                //    sum_shode += Convert.ToDouble(this.mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());

                //sum_all += Convert.ToDouble(this.mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());
            }

            //lbl_sum_all.Text = sum_all.ToString();
            //lbl_sum_nashode.Text = sum_nashodeh.ToString();
            //lbl_sum_shode.Text = sum_shode.ToString();


        }

        //void Amar_log(string dcode)
        //{
        //    try
        //    {
        //        DataTable dtInfo = Classes.ClsMain.GetDataTable("select * from sharj_ghabz_log where dcode=" + dcode);
        //        lblStartTime.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dtInfo.Rows[0]["start_time"])).ToString();
        //        lblEndtime.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dtInfo.Rows[0]["end_time"])).ToString();
        //        lblLength.Text = (Convert.ToDateTime(dtInfo.Rows[0]["end_time"]) - Convert.ToDateTime(dtInfo.Rows[0]["start_time"])).Seconds.ToString() + " ثانیه ";
        //        lblSucesss.Text = dtInfo.Rows[0]["success"].ToString();
        //        lblFails.Text = dtInfo.Rows[0]["fails"].ToString();

        //    }
        //    catch
        //    { }


        //}

        void Amar_failed_gahbz(string dcode)
        {
            sharj_ghabz_failsTableAdapter.FillByDoreh(cWMSDataSet.sharj_ghabz_fails, Convert.ToInt32(dcode));
        }
        void Set_pardakhti(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].Cells["pardakhti"].Value =
                      (
                      Convert.ToDecimal(dgv.Rows[i].Cells["mablaghkolDataGridViewTextBoxColumn"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["mande"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["kasr_hezar"].Value) +
                      Convert.ToDecimal(dgv.Rows[i].Cells["bestankari"].Value)


                      );
            }
        }
        private void btn_find_Click(object sender, EventArgs e)
        {
            if (cmbDoreh.Text.Trim() != "")
            {
                this.sharj_ghabzTableAdapter.FillByDoreh(this.mainDataSest.sharj_ghabz, Convert.ToInt32(cmbDoreh.Text));
                dgv_ghobooz.DataSource = this.mainDataSest.sharj_ghabz;
                Set_pardakhti(dgv_ghobooz);

                DataTable dt_amar = Classes.ClsMain.GetDataTable("exec sp_sharj_amar_info " + cmbDoreh.Text);
                lbl_pardakht_sodeh.Text = dt_amar.Rows[0]["p"].ToString();
                lbl_pardakht_nasodeh.Text = dt_amar.Rows[0]["pn"].ToString();
                lbl_dar_hal_barrrasi.Text = dt_amar.Rows[0]["pj"].ToString();
                lblTedadKol.Text = (
    Convert.ToDecimal(lbl_pardakht_sodeh.Text) +
    Convert.ToDecimal(lbl_pardakht_nasodeh.Text) +
    Convert.ToDecimal(lbl_dar_hal_barrrasi.Text)

    ).ToString();

                //  lbl_majmoo_pardakhti.Text = dt_amar.Rows[0]["sp"].ToString();
                //  lbl_Majmoo_pardakhtNashodeh.Text = dt_amar.Rows[0]["spn"].ToString();
                //  lblMajmo_darHal_barrasi.Text = dt_amar.Rows[0]["spj"].ToString();

                //  lblMajmuKol.Text = (
                //Convert.ToDecimal(lbl_majmoo_pardakhti.Text) +
                //Convert.ToDecimal(lbl_Majmoo_pardakhtNashodeh.Text) +
                //Convert.ToDecimal(lblMajmo_darHal_barrasi.Text)

                //).ToString();
                decimal sum = 0;
                for (int i = 0; i < this.mainDataSest.sharj_ghabz.Rows.Count; i++)
                {
                    sum += Convert.ToDecimal(this.mainDataSest.sharj_ghabz[i]["metraj"]);
                }
                lbl_metraj.Text = sum.ToString();

            }
            else
            {
                Payam.Show("لطفا یک دوره انتخاب نمایید");
            }


        }

        private void btn_dar_hal_barrrasi_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=2";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض  با پرداخت جزئی: " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_padakhr_nashode_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=0";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت نشده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_pardakht_shodeh_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=1";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت شده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void dgv_ghobooz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Frm_sharj_details frm = new Frm_sharj_details(dgv_ghobooz.Rows[e.RowIndex].Cells[0].Value.ToString(), dgv_ghobooz.Rows[e.RowIndex].Cells[1].Value.ToString());
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                this.sharj_ghabzTableAdapter.FillByDoreh(this.mainDataSest.sharj_ghabz, Convert.ToInt32(cmbDoreh.Text));
                dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";

            }
        }

        private void cmbDoreh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find.PerformClick();
        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            //if (mainDataSest.sharj_ghabz.Rows.Count != 0)
            //{
            //    DataView dv = new DataView(mainDataSest.sharj_ghabz);
            //    dv.RowFilter = "";
            //    dgv_ghobooz.DataSource = dv.ToTable();
            //    lblPayam.Text = "تعداد کلیه قبوض: " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
            //}
            //else
            //{
            //    Payam.Show("لطفا دوره مربوطه را انتخاب نمایید و بر روی دکمه نمایش کلیک نمایید");
            //}
            if (cmbDoreh.Text.Trim() != "")
            {
                int LastDoreh= Classes.clsSharj.Get_last_doreh_dcode();
                this.sharj_ghabzTableAdapter.FillByDoreh(this.mainDataSest.sharj_ghabz, LastDoreh);
                dgv_ghobooz.DataSource = this.mainDataSest.sharj_ghabz;
                Set_pardakhti(dgv_ghobooz);


                DataTable dt_amar = Classes.ClsMain.GetDataTable("exec sp_sharj_amar_info " + LastDoreh);
                lbl_pardakht_sodeh.Text = dt_amar.Rows[0]["p"].ToString();
                lbl_pardakht_nasodeh.Text = dt_amar.Rows[0]["pn"].ToString();
                lbl_dar_hal_barrrasi.Text = dt_amar.Rows[0]["pj"].ToString();
                lblTedadKol.Text = (
Convert.ToDecimal(lbl_pardakht_sodeh.Text) +
Convert.ToDecimal(lbl_pardakht_nasodeh.Text) +
Convert.ToDecimal(lbl_dar_hal_barrrasi.Text)

).ToString();

                //  lbl_majmoo_pardakhti.Text = dt_amar.Rows[0]["sp"].ToString();
                //  lbl_Majmoo_pardakhtNashodeh.Text = dt_amar.Rows[0]["spn"].ToString();
                //  lblMajmo_darHal_barrasi.Text = dt_amar.Rows[0]["spj"].ToString();

                //  lblMajmuKol.Text = (
                //Convert.ToDecimal(lbl_majmoo_pardakhti.Text) +
                //Convert.ToDecimal(lbl_Majmoo_pardakhtNashodeh.Text) +
                //Convert.ToDecimal(lblMajmo_darHal_barrasi.Text)

                //).ToString();
                decimal sum = 0;
                for (int i = 0; i < this.mainDataSest.sharj_ghabz.Rows.Count; i++)
                {
                    sum += Convert.ToDecimal(this.mainDataSest.sharj_ghabz[i]["metraj"]);
                }
                lbl_metraj.Text = sum.ToString();
            }
            else
            {
                Payam.Show("هیچ قبضی برای این دوره موجود نیست");
            }


        }

        private void dgv_ghobooz_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_find_ghabz_byCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_find_ghabz_byCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                //btn_find_by_gcode.PerformClick();

            }
        }




        private void Frm_all_Sharj_ghabz_for_all_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

        }

        private void btn_rpt_na_Click(object sender, EventArgs e)
        {
            Rpt_Sharj_Ghobooz_PN frm = new Rpt_Sharj_Ghobooz_PN(cmbDoreh.Text);
            frm.ShowDialog();
        }



 

        private void dgv_ghobooz_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowGhobooz.PerformClick();
        }

        private void lbl_majmoo_pardakhti_Click(object sender, EventArgs e)
        {

        }

        private void dgv_ghobooz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (dgv_ghobooz.SelectedRows.Count != 0)
                {
                    Frm_sharj_details frm = new Frm_sharj_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), dgv_ghobooz.SelectedRows[0].Cells[1].Value.ToString());
                    if (frm.ShowDialog() == DialogResult.Yes)
                    {
                        sharj_ghabzTableAdapter.FillByDoreh(mainDataSest.sharj_ghabz, Convert.ToInt32(cmbDoreh.Text));
                        dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                        lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";

                    }
                }
        }

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                Frm_sharj_details frm = new Frm_sharj_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), dgv_ghobooz.SelectedRows[0].Cells[1].Value.ToString());
                if (frm.ShowDialog() == DialogResult.Yes)
                {
                    sharj_ghabzTableAdapter.FillByDoreh(mainDataSest.sharj_ghabz, Convert.ToInt32(cmbDoreh.Text));
                    dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                    lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";

                }
            }
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_all_Sharj_ghabz_for_all_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btn_find_by_gcode_Click_2(object sender, EventArgs e)
        {
            if (txt_find_ghabz_byCode.Text.Trim() == "")
            {
                DataView dv = new DataView(mainDataSest.sharj_ghabz);
                dv.RowFilter = "";
                dgv_ghobooz.DataSource = dv.ToTable();
            }
            else
            {
                DataView dv = new DataView(mainDataSest.sharj_ghabz);
                dv.RowFilter = "gcode=" + txt_find_ghabz_byCode.Text;
                dgv_ghobooz.DataSource = dv.ToTable();
            }
        }

        private void btnFindByGharardad_Click(object sender, EventArgs e)
        {
            if (txt_find_by_gharardad.Text.Trim() == "")
            {
                DataView dv = new DataView(mainDataSest.sharj_ghabz);
                dv.RowFilter = "";
                dgv_ghobooz.DataSource = dv.ToTable();
            }
            else
            {
                DataView dv = new DataView(mainDataSest.sharj_ghabz);
                dv.RowFilter = "gharardad=" + txt_find_by_gharardad.Text;
                dgv_ghobooz.DataSource = dv.ToTable();
            }
        }

        private void txt_find_by_gharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFindByGharardad.PerformClick();
        }

        private void txt_find_ghabz_byCode_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gcode.PerformClick();
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.rptRizGozareshSharjForAll rpt = new CrysReports.rptRizGozareshSharjForAll();

            Classes.clsSharj.RizHeasbsharjForAll(mainDataSest,
                repVUer,
                rpt
                );
        }







    }
}