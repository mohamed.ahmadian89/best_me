﻿namespace CWMS
{
    partial class Frm_sharj_per_user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sharjghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.sharj_ghabzTableAdapter = new CWMS.MainDataSestTableAdapters.sharj_ghabzTableAdapter();
            this.btn_all = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_sum_nashode = new DevComponents.DotNetBar.LabelX();
            this.lbl_sum_shode = new DevComponents.DotNetBar.LabelX();
            this.lbl_sum_all = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnShowGhobooz = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_pardakht_shodeh = new DevComponents.DotNetBar.ButtonX();
            this.btn_padakhr_nashode = new DevComponents.DotNetBar.ButtonX();
            this.btn_dar_hal_barrrasi = new DevComponents.DotNetBar.ButtonX();
            this.lblPayam = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find = new DevComponents.DotNetBar.ButtonX();
            this.cmbDoreh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txt_find_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbl_modir_amel = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.lbl_co_name = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lbl_gharardad = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dgv_ghobooz = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.metraj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablagh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maliatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kasr_hezar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pardakhti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mande = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestankari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.groupPanel5.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).BeginInit();
            this.SuspendLayout();
            // 
            // sharjghabzBindingSource
            // 
            this.sharjghabzBindingSource.DataMember = "sharj_ghabz";
            this.sharjghabzBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sharj_ghabzTableAdapter
            // 
            this.sharj_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // btn_all
            // 
            this.btn_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_all.Location = new System.Drawing.Point(4, 8);
            this.btn_all.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_all.Name = "btn_all";
            this.btn_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_all.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btn_all.Size = new System.Drawing.Size(334, 27);
            this.btn_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_all.Symbol = "";
            this.btn_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_all.SymbolSize = 9F;
            this.btn_all.TabIndex = 1;
            this.btn_all.Text = "نمایش کلیه قبوض شارژ مشترک - F1";
            this.btn_all.Tooltip = "F1";
            this.btn_all.Click += new System.EventHandler(this.btn_all_Click);
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.lbl_sum_nashode);
            this.groupPanel5.Controls.Add(this.lbl_sum_shode);
            this.groupPanel5.Controls.Add(this.lbl_sum_all);
            this.groupPanel5.Controls.Add(this.labelX8);
            this.groupPanel5.Controls.Add(this.labelX6);
            this.groupPanel5.Controls.Add(this.labelX4);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Location = new System.Drawing.Point(44, 644);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.Size = new System.Drawing.Size(1170, 70);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 82;
            this.groupPanel5.Text = "اطلاعات اماری راجع به کلیه قبوض مشترک";
            // 
            // lbl_sum_nashode
            // 
            this.lbl_sum_nashode.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_sum_nashode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sum_nashode.ForeColor = System.Drawing.Color.Black;
            this.lbl_sum_nashode.Location = new System.Drawing.Point(34, 0);
            this.lbl_sum_nashode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_sum_nashode.Name = "lbl_sum_nashode";
            this.lbl_sum_nashode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_sum_nashode.Size = new System.Drawing.Size(155, 27);
            this.lbl_sum_nashode.TabIndex = 32;
            this.lbl_sum_nashode.Text = "0";
            this.lbl_sum_nashode.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_sum_shode
            // 
            this.lbl_sum_shode.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_sum_shode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sum_shode.ForeColor = System.Drawing.Color.Black;
            this.lbl_sum_shode.Location = new System.Drawing.Point(447, 0);
            this.lbl_sum_shode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_sum_shode.Name = "lbl_sum_shode";
            this.lbl_sum_shode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_sum_shode.Size = new System.Drawing.Size(155, 27);
            this.lbl_sum_shode.TabIndex = 31;
            this.lbl_sum_shode.Text = "0";
            this.lbl_sum_shode.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_sum_all
            // 
            this.lbl_sum_all.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_sum_all.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sum_all.ForeColor = System.Drawing.Color.Black;
            this.lbl_sum_all.Location = new System.Drawing.Point(847, 0);
            this.lbl_sum_all.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_sum_all.Name = "lbl_sum_all";
            this.lbl_sum_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_sum_all.Size = new System.Drawing.Size(155, 27);
            this.lbl_sum_all.TabIndex = 30;
            this.lbl_sum_all.Text = "0";
            this.lbl_sum_all.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(198, 0);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX8.Size = new System.Drawing.Size(166, 27);
            this.labelX8.TabIndex = 29;
            this.labelX8.Text = "مجموع قبوض پرداخت نشده:";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(611, 0);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX6.Size = new System.Drawing.Size(166, 27);
            this.labelX6.TabIndex = 28;
            this.labelX6.Text = "مجموع قبوض پرداخت شده:";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(986, 0);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(155, 27);
            this.labelX4.TabIndex = 27;
            this.labelX4.Text = "جمع کلیه قبوض:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(417, 206);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(35, 27);
            this.labelX10.Symbol = "";
            this.labelX10.SymbolColor = System.Drawing.Color.OrangeRed;
            this.labelX10.TabIndex = 80;
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(896, 206);
            this.labelX21.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX21.Name = "labelX21";
            this.labelX21.Size = new System.Drawing.Size(35, 27);
            this.labelX21.Symbol = "";
            this.labelX21.SymbolColor = System.Drawing.Color.OrangeRed;
            this.labelX21.TabIndex = 79;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(466, 209);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(422, 27);
            this.labelX2.TabIndex = 28;
            this.labelX2.Text = "جهت مشاهده جزئیات قبض بر روی آن دوبار کلیک نمایید";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.btnShowGhobooz);
            this.groupPanel4.Controls.Add(this.buttonX4);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(44, 591);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(1170, 43);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 32;
            // 
            // btnShowGhobooz
            // 
            this.btnShowGhobooz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowGhobooz.BackColor = System.Drawing.Color.Transparent;
            this.btnShowGhobooz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowGhobooz.Location = new System.Drawing.Point(741, 2);
            this.btnShowGhobooz.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnShowGhobooz.Name = "btnShowGhobooz";
            this.btnShowGhobooz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowGhobooz.Size = new System.Drawing.Size(401, 31);
            this.btnShowGhobooz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowGhobooz.Symbol = "";
            this.btnShowGhobooz.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowGhobooz.SymbolSize = 12F;
            this.btnShowGhobooz.TabIndex = 84;
            this.btnShowGhobooz.Text = "نمایش جزئیات قبض انتخاب شده";
            this.btnShowGhobooz.Click += new System.EventHandler(this.btnShowGhobooz_Click);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(18, 6);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX4.Size = new System.Drawing.Size(259, 27);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 53;
            this.buttonX4.Text = "پرینت قبوض به همراه پرداختی های مربوطه ";
            this.buttonX4.Tooltip = "Ctrl+P";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.btn_pardakht_shodeh);
            this.groupPanel3.Controls.Add(this.btn_padakhr_nashode);
            this.groupPanel3.Controls.Add(this.btn_dar_hal_barrrasi);
            this.groupPanel3.Controls.Add(this.lblPayam);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(44, 159);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(1170, 43);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 31;
            // 
            // btn_pardakht_shodeh
            // 
            this.btn_pardakht_shodeh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_pardakht_shodeh.BackColor = System.Drawing.Color.Transparent;
            this.btn_pardakht_shodeh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_pardakht_shodeh.Location = new System.Drawing.Point(4, 5);
            this.btn_pardakht_shodeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_pardakht_shodeh.Name = "btn_pardakht_shodeh";
            this.btn_pardakht_shodeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_pardakht_shodeh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_pardakht_shodeh.Size = new System.Drawing.Size(186, 27);
            this.btn_pardakht_shodeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_pardakht_shodeh.Symbol = "";
            this.btn_pardakht_shodeh.SymbolColor = System.Drawing.Color.Green;
            this.btn_pardakht_shodeh.SymbolSize = 9F;
            this.btn_pardakht_shodeh.TabIndex = 24;
            this.btn_pardakht_shodeh.Text = "قبوض پرداخت شده";
            this.btn_pardakht_shodeh.Click += new System.EventHandler(this.btn_pardakht_shodeh_Click);
            // 
            // btn_padakhr_nashode
            // 
            this.btn_padakhr_nashode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_padakhr_nashode.BackColor = System.Drawing.Color.Transparent;
            this.btn_padakhr_nashode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_padakhr_nashode.Location = new System.Drawing.Point(198, 5);
            this.btn_padakhr_nashode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_padakhr_nashode.Name = "btn_padakhr_nashode";
            this.btn_padakhr_nashode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_padakhr_nashode.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_padakhr_nashode.Size = new System.Drawing.Size(186, 27);
            this.btn_padakhr_nashode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_padakhr_nashode.Symbol = "";
            this.btn_padakhr_nashode.SymbolColor = System.Drawing.Color.Maroon;
            this.btn_padakhr_nashode.SymbolSize = 9F;
            this.btn_padakhr_nashode.TabIndex = 25;
            this.btn_padakhr_nashode.Text = "قبوض پرداخت نشده";
            this.btn_padakhr_nashode.Click += new System.EventHandler(this.btn_padakhr_nashode_Click);
            // 
            // btn_dar_hal_barrrasi
            // 
            this.btn_dar_hal_barrrasi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_dar_hal_barrrasi.BackColor = System.Drawing.Color.Transparent;
            this.btn_dar_hal_barrrasi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_dar_hal_barrrasi.Location = new System.Drawing.Point(392, 5);
            this.btn_dar_hal_barrrasi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_dar_hal_barrrasi.Name = "btn_dar_hal_barrrasi";
            this.btn_dar_hal_barrrasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_dar_hal_barrrasi.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_dar_hal_barrrasi.Size = new System.Drawing.Size(186, 27);
            this.btn_dar_hal_barrrasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_dar_hal_barrrasi.Symbol = "";
            this.btn_dar_hal_barrrasi.SymbolColor = System.Drawing.Color.Teal;
            this.btn_dar_hal_barrrasi.SymbolSize = 9F;
            this.btn_dar_hal_barrrasi.TabIndex = 26;
            this.btn_dar_hal_barrrasi.Text = "قبوض با پرداخت جزئی";
            this.btn_dar_hal_barrrasi.Click += new System.EventHandler(this.btn_dar_hal_barrrasi_Click);
            // 
            // lblPayam
            // 
            this.lblPayam.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblPayam.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPayam.ForeColor = System.Drawing.Color.Black;
            this.lblPayam.Location = new System.Drawing.Point(637, 5);
            this.lblPayam.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblPayam.Name = "lblPayam";
            this.lblPayam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPayam.Size = new System.Drawing.Size(494, 27);
            this.lblPayam.TabIndex = 27;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_find);
            this.groupPanel2.Controls.Add(this.cmbDoreh);
            this.groupPanel2.Controls.Add(this.txt_find_gharardad);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.btn_all);
            this.groupPanel2.Controls.Add(this.labelX9);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(44, 6);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1170, 68);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "جستجوی اطلاعات";
            // 
            // btn_find
            // 
            this.btn_find.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find.BackColor = System.Drawing.Color.Transparent;
            this.btn_find.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find.Location = new System.Drawing.Point(423, 8);
            this.btn_find.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_find.Name = "btn_find";
            this.btn_find.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2);
            this.btn_find.Size = new System.Drawing.Size(216, 27);
            this.btn_find.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find.Symbol = "";
            this.btn_find.SymbolColor = System.Drawing.Color.Green;
            this.btn_find.SymbolSize = 9F;
            this.btn_find.TabIndex = 2;
            this.btn_find.Text = "نمایش قبوض شارژ مشترک";
            this.btn_find.Tooltip = "F2";
            this.btn_find.Click += new System.EventHandler(this.btn_find_Click);
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.DisplayMember = "Text";
            this.cmbDoreh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDoreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoreh.ForeColor = System.Drawing.Color.Black;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.ItemHeight = 22;
            this.cmbDoreh.Location = new System.Drawing.Point(662, 7);
            this.cmbDoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDoreh.Size = new System.Drawing.Size(175, 28);
            this.cmbDoreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDoreh.TabIndex = 1;
            this.cmbDoreh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDoreh_KeyDown);
            // 
            // txt_find_gharardad
            // 
            this.txt_find_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_gharardad.Border.Class = "TextBoxBorder";
            this.txt_find_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_find_gharardad.Location = new System.Drawing.Point(935, 7);
            this.txt_find_gharardad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_find_gharardad.Name = "txt_find_gharardad";
            this.txt_find_gharardad.PreventEnterBeep = true;
            this.txt_find_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_find_gharardad.Size = new System.Drawing.Size(87, 28);
            this.txt_find_gharardad.TabIndex = 0;
            this.txt_find_gharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_gharardad_KeyDown);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(824, 8);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(87, 27);
            this.labelX7.TabIndex = 24;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(1018, 8);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX9.Size = new System.Drawing.Size(78, 27);
            this.labelX9.TabIndex = 22;
            this.labelX9.Text = "قرارداد:";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.lbl_modir_amel);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.lbl_co_name);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.lbl_gharardad);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(44, 81);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1170, 70);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 29;
            this.groupPanel1.Text = "اطلاعات مشترک";
            // 
            // lbl_modir_amel
            // 
            this.lbl_modir_amel.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_modir_amel.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_modir_amel.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_modir_amel.ForeColor = System.Drawing.Color.Black;
            this.lbl_modir_amel.Location = new System.Drawing.Point(86, 3);
            this.lbl_modir_amel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_modir_amel.Name = "lbl_modir_amel";
            this.lbl_modir_amel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_modir_amel.Size = new System.Drawing.Size(274, 27);
            this.lbl_modir_amel.TabIndex = 27;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(368, 3);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX5.Size = new System.Drawing.Size(87, 27);
            this.labelX5.TabIndex = 26;
            this.labelX5.Text = "مدیریت عامل:";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_co_name
            // 
            this.lbl_co_name.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_co_name.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_co_name.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_co_name.ForeColor = System.Drawing.Color.Black;
            this.lbl_co_name.Location = new System.Drawing.Point(463, 3);
            this.lbl_co_name.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_co_name.Name = "lbl_co_name";
            this.lbl_co_name.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_co_name.Size = new System.Drawing.Size(353, 27);
            this.lbl_co_name.TabIndex = 25;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(825, 3);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(87, 27);
            this.labelX3.TabIndex = 24;
            this.labelX3.Text = "نام مشترک :";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_gharardad
            // 
            this.lbl_gharardad.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_gharardad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_gharardad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbl_gharardad.ForeColor = System.Drawing.Color.Black;
            this.lbl_gharardad.Location = new System.Drawing.Point(952, 3);
            this.lbl_gharardad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_gharardad.Name = "lbl_gharardad";
            this.lbl_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_gharardad.Size = new System.Drawing.Size(72, 27);
            this.lbl_gharardad.TabIndex = 23;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(1019, 3);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(78, 27);
            this.labelX1.TabIndex = 22;
            this.labelX1.Text = "قرارداد:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dgv_ghobooz
            // 
            this.dgv_ghobooz.AllowUserToAddRows = false;
            this.dgv_ghobooz.AllowUserToDeleteRows = false;
            this.dgv_ghobooz.AllowUserToResizeColumns = false;
            this.dgv_ghobooz.AllowUserToResizeRows = false;
            this.dgv_ghobooz.AutoGenerateColumns = false;
            this.dgv_ghobooz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_ghobooz.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_ghobooz.BackgroundColor = System.Drawing.Color.White;
            this.dgv_ghobooz.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ghobooz.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ghobooz.ColumnHeadersHeight = 30;
            this.dgv_ghobooz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcodeDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.metraj,
            this.bedehi,
            this.mablagh,
            this.maliatDataGridViewTextBoxColumn,
            this.mablaghkolDataGridViewTextBoxColumn,
            this.kasr_hezar,
            this.pardakhti,
            this.mande,
            this.bestankari});
            this.dgv_ghobooz.DataSource = this.sharjghabzBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ghobooz.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_ghobooz.EnableHeadersVisualStyles = false;
            this.dgv_ghobooz.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_ghobooz.Location = new System.Drawing.Point(44, 240);
            this.dgv_ghobooz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgv_ghobooz.MultiSelect = false;
            this.dgv_ghobooz.Name = "dgv_ghobooz";
            this.dgv_ghobooz.ReadOnly = true;
            this.dgv_ghobooz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ghobooz.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_ghobooz.RowHeadersVisible = false;
            this.dgv_ghobooz.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dgv_ghobooz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ghobooz.Size = new System.Drawing.Size(1170, 344);
            this.dgv_ghobooz.TabIndex = 0;
            this.dgv_ghobooz.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ghobooz_CellDoubleClick);
            this.dgv_ghobooz.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ghobooz_CellEndEdit);
            this.dgv_ghobooz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_ghobooz_KeyDown);
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.FillWeight = 80F;
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کدقبض";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.gcodeDataGridViewTextBoxColumn.Width = 75;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.dcodeDataGridViewTextBoxColumn.Width = 60;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.FillWeight = 70F;
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhDataGridViewTextBoxColumn.Width = 62;
            // 
            // metraj
            // 
            this.metraj.DataPropertyName = "metraj";
            this.metraj.HeaderText = "متراژ";
            this.metraj.Name = "metraj";
            this.metraj.ReadOnly = true;
            this.metraj.Width = 62;
            // 
            // bedehi
            // 
            this.bedehi.DataPropertyName = "bedehi";
            this.bedehi.HeaderText = "بدهی دوره قبل";
            this.bedehi.Name = "bedehi";
            this.bedehi.ReadOnly = true;
            this.bedehi.Width = 116;
            // 
            // mablagh
            // 
            this.mablagh.DataPropertyName = "mablagh";
            dataGridViewCellStyle2.Format = "#,#";
            dataGridViewCellStyle2.NullValue = "0";
            this.mablagh.DefaultCellStyle = dataGridViewCellStyle2;
            this.mablagh.HeaderText = "مبلغ";
            this.mablagh.Name = "mablagh";
            this.mablagh.ReadOnly = true;
            this.mablagh.Width = 58;
            // 
            // maliatDataGridViewTextBoxColumn
            // 
            this.maliatDataGridViewTextBoxColumn.DataPropertyName = "maliat";
            dataGridViewCellStyle3.Format = "#,#";
            dataGridViewCellStyle3.NullValue = "0";
            this.maliatDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.maliatDataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.maliatDataGridViewTextBoxColumn.Name = "maliatDataGridViewTextBoxColumn";
            this.maliatDataGridViewTextBoxColumn.ReadOnly = true;
            this.maliatDataGridViewTextBoxColumn.Width = 67;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            dataGridViewCellStyle4.Format = "#,#";
            dataGridViewCellStyle4.NullValue = "0";
            this.mablaghkolDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            this.mablaghkolDataGridViewTextBoxColumn.Width = 77;
            // 
            // kasr_hezar
            // 
            this.kasr_hezar.DataPropertyName = "kasr_hezar";
            this.kasr_hezar.HeaderText = "کسرهزار";
            this.kasr_hezar.Name = "kasr_hezar";
            this.kasr_hezar.ReadOnly = true;
            this.kasr_hezar.Width = 82;
            // 
            // pardakhti
            // 
            this.pardakhti.HeaderText = "پرداختی";
            this.pardakhti.Name = "pardakhti";
            this.pardakhti.ReadOnly = true;
            this.pardakhti.Width = 78;
            // 
            // mande
            // 
            this.mande.DataPropertyName = "mande";
            dataGridViewCellStyle5.Format = "#,#";
            dataGridViewCellStyle5.NullValue = "0";
            this.mande.DefaultCellStyle = dataGridViewCellStyle5;
            this.mande.HeaderText = "باقیمانده";
            this.mande.Name = "mande";
            this.mande.ReadOnly = true;
            this.mande.Width = 80;
            // 
            // bestankari
            // 
            this.bestankari.DataPropertyName = "bestankari";
            this.bestankari.HeaderText = "بستانکاری";
            this.bestankari.Name = "bestankari";
            this.bestankari.ReadOnly = true;
            this.bestankari.Width = 87;
            // 
            // Frm_sharj_per_user
            // 
            this.AcceptButton = this.btn_all;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 722);
            this.Controls.Add(this.groupPanel5);
            this.Controls.Add(this.labelX10);
            this.Controls.Add(this.labelX21);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.dgv_ghobooz);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_sharj_per_user";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "قبوض شارژ مشترک";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAllSharjGhabz_FormClosed);
            this.Load += new System.EventHandler(this.FrmAllSharjGhabz_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAllSharjGhabz_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.sharjghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.groupPanel5.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_ghobooz;
        private DevComponents.DotNetBar.ButtonX btn_all;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btn_pardakht_shodeh;
        private DevComponents.DotNetBar.ButtonX btn_padakhr_nashode;
        private DevComponents.DotNetBar.ButtonX btn_dar_hal_barrrasi;
        private DevComponents.DotNetBar.LabelX lblPayam;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX lbl_modir_amel;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX lbl_co_name;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lbl_gharardad;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_find;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDoreh;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_gharardad;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource sharjghabzBindingSource;
        private MainDataSestTableAdapters.sharj_ghabzTableAdapter sharj_ghabzTableAdapter;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.LabelX lbl_sum_nashode;
        private DevComponents.DotNetBar.LabelX lbl_sum_shode;
        private DevComponents.DotNetBar.LabelX lbl_sum_all;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.ButtonX btnShowGhobooz;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn metraj;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedehi;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablagh;
        private System.Windows.Forms.DataGridViewTextBoxColumn maliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kasr_hezar;
        private System.Windows.Forms.DataGridViewTextBoxColumn pardakhti;
        private System.Windows.Forms.DataGridViewTextBoxColumn mande;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestankari;
    }
}