﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
namespace CWMS.Sharj
{
    public partial class Frm_sharj_mablagh : MyMetroForm
    {
        public Frm_sharj_mablagh()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }

        private void FrmSharjMablagh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.sharj_mablagh' table. You can move, or remove it, as needed.
            this.sharj_mablaghTableAdapter.Fill(this.mainDataSest.sharj_mablagh);

        }


        bool adding = false;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (adding == false)
            {
                adding = true;
                sharjmablaghBindingSource.AddNew();
                tarikhTasvib.Focus();
                tarikhTasvib.SelectedDateTime = tarikhEjra.SelectedDateTime = DateTime.Now;
                txtZrib.Text = "1";
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p79)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 79);

               
                try
                {
                    Convert.ToInt32(txtMablagh.Text);
                }
                catch
                {
                    Payam.Show("مبلغ شارژ صحیح نمی باشد.");
                    txtMablagh.SelectAll();
                    txtMablagh.Focus();
                    return;
                }





                if (txtZrib.Text.Trim() == "" || Convert.ToInt32(txtZrib.Text) < 1)
                {
                    txtZrib.Text = "1";

                }

                else if (tarikhTasvib.Text.Contains("هی") == true)
                {
                    Payam.Show("لطفا تاریخ تصویب را از کادر مربوطه انتخاب کنید");
                    tarikhTasvib.Focus();

                }
                else if (tarikhEjra.Text.Contains("هی") == true)
                {
                    Payam.Show("لطفا تاریخ اجرا را از کادر مربوطه انتخاب کنید");
                    tarikhEjra.Focus();

                }
                else
                {
                    if (adding)
                    {
                        int item = mainDataSest.sharj_mablagh.Where(p => p.tarikh_ejra > tarikhEjra.SelectedDateTime).Count();
                        if (item != 0)
                        {
                            Payam.Show("تاریخ اجرا می بایست از تاریخ های اجرای موجود در لیست بیشتر باشد");
                            tarikhEjra.Focus();
                        }
                        else
                        {
                            sharjmablaghBindingSource.EndEdit();
                            sharj_mablaghTableAdapter.Update(mainDataSest.sharj_mablagh);
                            Payam.Show("اطلاعات در سیستم ثبت شد");
                            adding = false;

                        }

                    }
                    else
                    {
                        sharjmablaghBindingSource.EndEdit();
                        sharj_mablaghTableAdapter.Update(mainDataSest.sharj_mablagh);
                        Payam.Show("اطلاعات در سیستم ثبت شد");
                        adding = false;

                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void FrmSharjMablagh_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p79)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 79, "حذف تعرفه");

                if (adding)
                    adding = false;
                else if (sharjmablaghBindingSource.Count > 0)
                {
                    if (Cpayam.Show("ایا با حذف اطلاعات موافقید؟") == DialogResult.Yes)
                    {
                        sharjmablaghBindingSource.RemoveCurrent();
                        sharjmablaghBindingSource.EndEdit();
                        sharj_mablaghTableAdapter.Update(mainDataSest.sharj_mablagh);
                    }

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmSharjMablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtMablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtHesab.Focus();
        }

        private void txtHesab_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtZrib.Focus();
        }

        private void txtZrib_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSave.PerformClick();
        }
    }
}