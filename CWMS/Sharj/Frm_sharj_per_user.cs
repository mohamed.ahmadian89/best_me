﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CWMS.Classes;
using CrystalDecisions.Windows.Forms;

namespace CWMS
{
    public partial class Frm_sharj_per_user : MyMetroForm
    {
        string gharardad = "";
        public Frm_sharj_per_user(string gharardadtemp = "")
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
            if (gharardadtemp != "")
            {
                txt_find_gharardad.Text = gharardadtemp;


                btn_all.PerformClick();
            }

        }


        void ChangeDateTime_GridView()
        {
            for (int i = 0; i < dgv_ghobooz.Rows.Count; i++)
            {
                try
                {
                    dgv_ghobooz.Rows[i].Cells[2].Value = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_ghobooz.Rows[i].Cells[2].Value).ToString("MM/dd/yyyy")).ToString("d");
                }
                catch
                {
                    ;
                }


            }
        }

        private void FrmAllSharjGhabz_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.sharj_ghabz' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'cWMSDataSet.sharj_ghabz' table. You can move, or remove it, as needed.
            txt_find_gharardad.Focus();
            Classes.ClsMain.ChangeCulture("f");

        }

        string DorehForChap = "all";

        private void btn_all_Click(object sender, EventArgs e)
        {
            if (txt_find_gharardad.Text.Trim() == "")
            {
                txt_find_gharardad.Focus();
                Payam.Show("لطفا شماره قرارداد را وارد نمایید");
            }
            else
            {
                sharj_ghabzTableAdapter.FillByGharardad(mainDataSest.sharj_ghabz, Convert.ToInt32(txt_find_gharardad.Text));

                dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
                //SetVaziat();
                Etelaat_amari();
                DorehForChap = "all";
                dgv_ghobooz.Focus();
                Set_pardakhti(dgv_ghobooz);

                DataTable DtMoshtarek = Classes.ClsMain.ProcessInMoshtarekinData("gharardad=" + txt_find_gharardad.Text);
                if (DtMoshtarek.Rows.Count == 0)
                {
                    Payam.Show(txt_find_gharardad.Text + " در سیستم ثبت نشده است  ");
                    txt_find_gharardad.Clear();
                    txt_find_gharardad.Focus();
                }
                else
                {
                    lbl_co_name.Text = DtMoshtarek.Rows[0]["co_name"].ToString();
                    lbl_gharardad.Text = DtMoshtarek.Rows[0]["gharardad"].ToString();
                    lbl_modir_amel.Text = DtMoshtarek.Rows[0]["modir_amel"].ToString();


                }
            }



        }




        private void txt_find_gharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmbDoreh.Focus();
        }



        void SetVaziat(DataTable dv = null)
        {
            if (dv == null)
            {
                for (int i = 0; i < mainDataSest.sharj_ghabz.Rows.Count; i++)
                {
                    if (mainDataSest.sharj_ghabz.Rows[i]["vaziat"].ToString() == "1")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "پرداخت شده";
                    else if (mainDataSest.sharj_ghabz.Rows[i]["vaziat"].ToString() == "0")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "پرداخت نشده";

                    else if (mainDataSest.sharj_ghabz.Rows[i]["vaziat"].ToString() == "2")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "در حال بررسی";

                }
            }
            else
            {
                for (int i = 0; i < dv.Rows.Count; i++)
                {
                    if (dv.Rows[i]["vaziat"].ToString() == "1")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "پرداخت شده";
                    else if (dv.Rows[i]["vaziat"].ToString() == "0")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "پرداخت نشده";

                    else if (dv.Rows[i]["vaziat"].ToString() == "2")
                        dgv_ghobooz.Rows[i].Cells["vaz"].Value = "در حال بررسی";

                }
            }
        }


        void Etelaat_amari()
        {
            decimal sumall = 0, shodeh = 0, nashodeh = 0;
            for (int i = 0; i < mainDataSest.sharj_ghabz.Rows.Count; i++)
            {
                sumall += Convert.ToDecimal(mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());
                if (Convert.ToInt32(mainDataSest.sharj_ghabz.Rows[i]["vaziat"]) == 1)
                    shodeh += Convert.ToDecimal(mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());
                else
                    nashodeh += Convert.ToDecimal(mainDataSest.sharj_ghabz.Rows[i]["mablaghkol"].ToString());
            }

            lbl_sum_all.Text = sumall.ToString();
            lbl_sum_nashode.Text = nashodeh.ToString();
            lbl_sum_shode.Text = shodeh.ToString();

        }
        void Set_pardakhti(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].Cells["pardakhti"].Value =
                      (
                      Convert.ToDecimal(dgv.Rows[i].Cells["mablaghkolDataGridViewTextBoxColumn"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["mande"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["kasr_hezar"].Value) +
                      Convert.ToDecimal(dgv.Rows[i].Cells["bestankari"].Value)


                      );
            }
        }
        private void btn_find_Click(object sender, EventArgs e)
        {


            if (txt_find_gharardad.Text.Trim() == "")
            {
                txt_find_gharardad.Focus();
                Payam.Show("لطفا شماره قرارداد را وارد نمایید");
            }
            else if (cmbDoreh.Text.Trim() == "")
            {
                btn_all.PerformClick();
                DorehForChap = "all";


            }
            else
            {
                int n = 0;
                try
                {
                    n = Convert.ToInt32(txt_find_gharardad.Text);
                }
                catch (Exception)
                {
                    Payam.Show("شماره قرارداد به درستی وارد نشده است");
                }
                sharj_ghabzTableAdapter.FillByGharardad(mainDataSest.sharj_ghabz, n);

                try
                {
                    Etelaat_amari();
                }
                catch (Exception ex)
                {
                    ClsMain.logError(ex, "Etelaat_amari at FrmAllSharjGhabz");
                    Payam.Show("خطا! لطفا با پشتیبان هماهنگ نمایید");
                }

                DataTable MoshtarekDT = Classes.ClsMain.ProcessInMoshtarekinData("gharardad=" + txt_find_gharardad.Text);
                if (MoshtarekDT.Rows.Count == 0)
                {
                    Payam.Show(txt_find_gharardad.Text + " در سیستم ثبت نشده است  ");
                    txt_find_gharardad.Clear();
                    txt_find_gharardad.Focus();
                }
                else
                {
                    lbl_co_name.Text = MoshtarekDT.Rows[0]["co_name"].ToString();
                    lbl_gharardad.Text = MoshtarekDT.Rows[0]["gharardad"].ToString();
                    lbl_modir_amel.Text = MoshtarekDT.Rows[0]["modir_amel"].ToString();

                    //..
                    DataView dv = new DataView(mainDataSest.sharj_ghabz);
                    dv.RowFilter = "dcode=" + cmbDoreh.Text;
                    dgv_ghobooz.DataSource = dv.ToTable();
                    //SetVaziat(dv.ToTable());
                    //lblPayam.Text = "تعداد کلیه قبوض در دوره" + cmbDoreh.Text + ":" + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
                    lblPayam.Text = "";
                    DorehForChap = cmbDoreh.Text;
                    dgv_ghobooz.Focus();
                    Set_pardakhti(dgv_ghobooz);

                }
            }
        }

        private void cmbDoreh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find.PerformClick();
        }

        private void btn_dar_hal_barrrasi_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=2";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض  با پرداخت جزئی: " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_padakhr_nashode_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=0";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت نشده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_pardakht_shodeh_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(mainDataSest.sharj_ghabz);
            dv.RowFilter = "vaziat=1";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت شده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void dgv_ghobooz_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7 || e.ColumnIndex == 8)
            {
                int res;
                bool IsNumber = Int32.TryParse(dgv_ghobooz.Rows[e.RowIndex].Cells[7].Value.ToString(), out res);
                if (IsNumber == false)
                    dgv_ghobooz.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0";

                IsNumber = Int32.TryParse(dgv_ghobooz.Rows[e.RowIndex].Cells[8].Value.ToString(), out res);
                if (IsNumber == false)
                    dgv_ghobooz.Rows[e.RowIndex].Cells[8].Value = "0";


                float maliat = Convert.ToSingle(dgv_ghobooz.Rows[e.RowIndex].Cells[7].Value);
                float sayer = Convert.ToSingle(dgv_ghobooz.Rows[e.RowIndex].Cells[8].Value);
                float mablah = Convert.ToSingle(dgv_ghobooz.Rows[e.RowIndex].Cells[6].Value);
                dgv_ghobooz.Rows[e.RowIndex].Cells[9].Value = (maliat + sayer + mablah).ToString();
            }
        }



        private void FrmAllSharjGhabz_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.Sharj_doreh rpt = new CrysReports.Sharj_doreh();
          
            Classes.clsSharj.ChapSharjDoreh(DorehForChap,
                    repVUer,
                    rpt, "print", txt_find_gharardad.Text
                    );

        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.Sharj_doreh rpt = new CrysReports.Sharj_doreh();
            Classes.clsSharj.ChapSharjDoreh(DorehForChap,
                    repVUer,
                    rpt, "excel", txt_find_gharardad.Text
                    );
        }

        private void dgv_ghobooz_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowGhobooz.PerformClick();
        }

        private void dgv_ghobooz_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                if (dgv_ghobooz.SelectedRows.Count != 0)
                {
                    Frm_sharj_details frm = new Frm_sharj_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), lbl_gharardad.Text);
                    if (frm.ShowDialog() == DialogResult.Yes)
                    {
                        sharj_ghabzTableAdapter.FillByGharardad(mainDataSest.sharj_ghabz, Convert.ToInt32(txt_find_gharardad.Text));
                        dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                        lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";

                    }
                }
        }

    

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                Frm_sharj_details frm = new Frm_sharj_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), lbl_gharardad.Text);
                if (frm.ShowDialog() == DialogResult.Yes)
                {
                    sharj_ghabzTableAdapter.FillByGharardad(mainDataSest.sharj_ghabz, Convert.ToInt32(txt_find_gharardad.Text));
                    dgv_ghobooz.DataSource = mainDataSest.sharj_ghabz;
                    lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";

                }
            }
        }


        private void FrmAllSharjGhabz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}