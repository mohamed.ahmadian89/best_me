﻿namespace CWMS
{
    partial class Frm_sharj_details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_takhfif = new CWMS.MoneyTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txt_shenase_pardakht = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_shenase_ghabz = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label25 = new System.Windows.Forms.Label();
            this.txt_sayer = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtBestankariFromLastDoreh = new CWMS.MoneyTextBox();
            this.txtBestankar = new CWMS.MoneyTextBox();
            this.lblBestankar = new System.Windows.Forms.Label();
            this.txtGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDarsadMaliat = new CWMS.FloatTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chtejari = new System.Windows.Forms.CheckBox();
            this.pinfo3 = new System.Windows.Forms.Label();
            this.pinfo2 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_kasr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPardakhti = new CWMS.MoneyTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txBaghimandeh = new CWMS.MoneyTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtJarimehTozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_jarimeh = new CWMS.MoneyTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnHesabSharj = new DevComponents.DotNetBar.ButtonX();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDayes = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_tozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmb_vaziat = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBedehi = new CWMS.MoneyTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_sayer_tozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_ghabz_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMablaghKol = new CWMS.MoneyTextBox();
            this.txtMaliat = new CWMS.MoneyTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMohlat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txtTarikhSoddor = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txtMablaghHoroof = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtMablagh = new CWMS.MoneyTextBox();
            this.txtMetraj = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTarefe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtdoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_save.Location = new System.Drawing.Point(8, 533);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4);
            this.btn_save.Name = "btn_save";
            this.btn_save.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_save.Size = new System.Drawing.Size(322, 28);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 24;
            this.btn_save.Text = "ثبت تغییرات مربوط به این قبض";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.buttonX1);
            this.groupPanel2.Controls.Add(this.buttonX4);
            this.groupPanel2.Controls.Add(this.buttonX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(30, 605);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1118, 63);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 28;
            this.groupPanel2.Text = "منوی عملیات";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(276, 3);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(250, 28);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 30;
            this.buttonX3.Text = "صدور مجدد این قبض";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_1);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(685, 3);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX1.Size = new System.Drawing.Size(212, 28);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 29;
            this.buttonX1.Text = "چاپ قبض ( پس زمینه )";
            this.buttonX1.Visible = false;
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(905, 3);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX4.Size = new System.Drawing.Size(170, 28);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 28;
            this.buttonX4.Text = "چاپ قبض";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(8, 3);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(250, 28);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 26;
            this.buttonX2.Text = "مشاهده پرداخت ها";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.txt_takhfif);
            this.groupPanel1.Controls.Add(this.label27);
            this.groupPanel1.Controls.Add(this.txt_shenase_pardakht);
            this.groupPanel1.Controls.Add(this.label26);
            this.groupPanel1.Controls.Add(this.txt_shenase_ghabz);
            this.groupPanel1.Controls.Add(this.label25);
            this.groupPanel1.Controls.Add(this.txt_sayer);
            this.groupPanel1.Controls.Add(this.label24);
            this.groupPanel1.Controls.Add(this.TxtBestankariFromLastDoreh);
            this.groupPanel1.Controls.Add(this.txtBestankar);
            this.groupPanel1.Controls.Add(this.lblBestankar);
            this.groupPanel1.Controls.Add(this.txtGharardad);
            this.groupPanel1.Controls.Add(this.label22);
            this.groupPanel1.Controls.Add(this.txtDarsadMaliat);
            this.groupPanel1.Controls.Add(this.label17);
            this.groupPanel1.Controls.Add(this.chtejari);
            this.groupPanel1.Controls.Add(this.pinfo3);
            this.groupPanel1.Controls.Add(this.pinfo2);
            this.groupPanel1.Controls.Add(this.label23);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.txt_kasr);
            this.groupPanel1.Controls.Add(this.label21);
            this.groupPanel1.Controls.Add(this.txtPardakhti);
            this.groupPanel1.Controls.Add(this.label20);
            this.groupPanel1.Controls.Add(this.txBaghimandeh);
            this.groupPanel1.Controls.Add(this.label15);
            this.groupPanel1.Controls.Add(this.label14);
            this.groupPanel1.Controls.Add(this.txtJarimehTozihat);
            this.groupPanel1.Controls.Add(this.txt_jarimeh);
            this.groupPanel1.Controls.Add(this.label10);
            this.groupPanel1.Controls.Add(this.btnHesabSharj);
            this.groupPanel1.Controls.Add(this.label9);
            this.groupPanel1.Controls.Add(this.txtDayes);
            this.groupPanel1.Controls.Add(this.label8);
            this.groupPanel1.Controls.Add(this.txt_tozihat);
            this.groupPanel1.Controls.Add(this.cmb_vaziat);
            this.groupPanel1.Controls.Add(this.label7);
            this.groupPanel1.Controls.Add(this.btn_save);
            this.groupPanel1.Controls.Add(this.txtBedehi);
            this.groupPanel1.Controls.Add(this.label19);
            this.groupPanel1.Controls.Add(this.label18);
            this.groupPanel1.Controls.Add(this.txt_sayer_tozihat);
            this.groupPanel1.Controls.Add(this.txt_ghabz_code);
            this.groupPanel1.Controls.Add(this.label16);
            this.groupPanel1.Controls.Add(this.txtMablaghKol);
            this.groupPanel1.Controls.Add(this.txtMaliat);
            this.groupPanel1.Controls.Add(this.label13);
            this.groupPanel1.Controls.Add(this.label12);
            this.groupPanel1.Controls.Add(this.label11);
            this.groupPanel1.Controls.Add(this.txtMohlat);
            this.groupPanel1.Controls.Add(this.txtTarikhSoddor);
            this.groupPanel1.Controls.Add(this.txtMablaghHoroof);
            this.groupPanel1.Controls.Add(this.txtMablagh);
            this.groupPanel1.Controls.Add(this.txtMetraj);
            this.groupPanel1.Controls.Add(this.txtTarefe);
            this.groupPanel1.Controls.Add(this.txtdoreh);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Controls.Add(this.label5);
            this.groupPanel1.Controls.Add(this.label4);
            this.groupPanel1.Controls.Add(this.label3);
            this.groupPanel1.Controls.Add(this.label2);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(28, 5);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1118, 596);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 22;
            this.groupPanel1.Text = "اطلاعات قبض شارژ";
            // 
            // txt_takhfif
            // 
            this.txt_takhfif.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_takhfif.Border.Class = "TextBoxBorder";
            this.txt_takhfif.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_takhfif.DisabledBackColor = System.Drawing.Color.White;
            this.txt_takhfif.ForeColor = System.Drawing.Color.Black;
            this.txt_takhfif.Location = new System.Drawing.Point(13, 157);
            this.txt_takhfif.Margin = new System.Windows.Forms.Padding(4);
            this.txt_takhfif.Name = "txt_takhfif";
            this.txt_takhfif.PreventEnterBeep = true;
            this.txt_takhfif.ReadOnly = true;
            this.txt_takhfif.Size = new System.Drawing.Size(136, 28);
            this.txt_takhfif.TabIndex = 172;
            this.txt_takhfif.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(157, 162);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 18);
            this.label27.TabIndex = 171;
            this.label27.Text = "تخفیف :";
            // 
            // txt_shenase_pardakht
            // 
            this.txt_shenase_pardakht.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_shenase_pardakht.Border.Class = "TextBoxBorder";
            this.txt_shenase_pardakht.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_shenase_pardakht.DisabledBackColor = System.Drawing.Color.White;
            this.txt_shenase_pardakht.Enabled = false;
            this.txt_shenase_pardakht.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_shenase_pardakht.ForeColor = System.Drawing.Color.Black;
            this.txt_shenase_pardakht.Location = new System.Drawing.Point(32, 492);
            this.txt_shenase_pardakht.Margin = new System.Windows.Forms.Padding(4);
            this.txt_shenase_pardakht.Name = "txt_shenase_pardakht";
            this.txt_shenase_pardakht.PreventEnterBeep = true;
            this.txt_shenase_pardakht.Size = new System.Drawing.Size(122, 28);
            this.txt_shenase_pardakht.TabIndex = 170;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(170, 495);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(82, 18);
            this.label26.TabIndex = 169;
            this.label26.Text = "شناسه پرداخت :";
            // 
            // txt_shenase_ghabz
            // 
            this.txt_shenase_ghabz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_shenase_ghabz.Border.Class = "TextBoxBorder";
            this.txt_shenase_ghabz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_shenase_ghabz.DisabledBackColor = System.Drawing.Color.White;
            this.txt_shenase_ghabz.Enabled = false;
            this.txt_shenase_ghabz.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_shenase_ghabz.ForeColor = System.Drawing.Color.Black;
            this.txt_shenase_ghabz.Location = new System.Drawing.Point(32, 461);
            this.txt_shenase_ghabz.Margin = new System.Windows.Forms.Padding(4);
            this.txt_shenase_ghabz.Name = "txt_shenase_ghabz";
            this.txt_shenase_ghabz.PreventEnterBeep = true;
            this.txt_shenase_ghabz.Size = new System.Drawing.Size(122, 28);
            this.txt_shenase_ghabz.TabIndex = 168;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(170, 464);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 18);
            this.label25.TabIndex = 167;
            this.label25.Text = "شناسه قبض :";
            // 
            // txt_sayer
            // 
            this.txt_sayer.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sayer.Border.Class = "TextBoxBorder";
            this.txt_sayer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sayer.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sayer.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sayer.ForeColor = System.Drawing.Color.Black;
            this.txt_sayer.Location = new System.Drawing.Point(760, 234);
            this.txt_sayer.Margin = new System.Windows.Forms.Padding(4);
            this.txt_sayer.Name = "txt_sayer";
            this.txt_sayer.PreventEnterBeep = true;
            this.txt_sayer.Size = new System.Drawing.Size(122, 28);
            this.txt_sayer.TabIndex = 166;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(626, 166);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 18);
            this.label24.TabIndex = 165;
            this.label24.Text = "بستانکاری از دوره قبل:";
            // 
            // TxtBestankariFromLastDoreh
            // 
            this.TxtBestankariFromLastDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtBestankariFromLastDoreh.Border.Class = "TextBoxBorder";
            this.TxtBestankariFromLastDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtBestankariFromLastDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtBestankariFromLastDoreh.ForeColor = System.Drawing.Color.Black;
            this.TxtBestankariFromLastDoreh.Location = new System.Drawing.Point(443, 162);
            this.TxtBestankariFromLastDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtBestankariFromLastDoreh.Name = "TxtBestankariFromLastDoreh";
            this.TxtBestankariFromLastDoreh.PreventEnterBeep = true;
            this.TxtBestankariFromLastDoreh.ReadOnly = true;
            this.TxtBestankariFromLastDoreh.Size = new System.Drawing.Size(170, 28);
            this.TxtBestankariFromLastDoreh.TabIndex = 164;
            this.TxtBestankariFromLastDoreh.Text = "0";
            // 
            // txtBestankar
            // 
            this.txtBestankar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBestankar.Border.Class = "TextBoxBorder";
            this.txtBestankar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBestankar.DisabledBackColor = System.Drawing.Color.White;
            this.txtBestankar.ForeColor = System.Drawing.Color.Black;
            this.txtBestankar.Location = new System.Drawing.Point(32, 418);
            this.txtBestankar.Margin = new System.Windows.Forms.Padding(4);
            this.txtBestankar.Name = "txtBestankar";
            this.txtBestankar.PreventEnterBeep = true;
            this.txtBestankar.ReadOnly = true;
            this.txtBestankar.Size = new System.Drawing.Size(170, 28);
            this.txtBestankar.TabIndex = 163;
            this.txtBestankar.Text = "0";
            this.txtBestankar.Visible = false;
            // 
            // lblBestankar
            // 
            this.lblBestankar.AutoSize = true;
            this.lblBestankar.BackColor = System.Drawing.Color.Transparent;
            this.lblBestankar.Font = new System.Drawing.Font("B Yekan", 9F);
            this.lblBestankar.ForeColor = System.Drawing.Color.Black;
            this.lblBestankar.Location = new System.Drawing.Point(210, 424);
            this.lblBestankar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBestankar.Name = "lblBestankar";
            this.lblBestankar.Size = new System.Drawing.Size(317, 18);
            this.lblBestankar.TabIndex = 162;
            this.lblBestankar.Text = "مبلغ بستانکاری که در فرآیند صدور قبض دوره بعد منظور خواهد گشت";
            this.lblBestankar.Visible = false;
            // 
            // txtGharardad
            // 
            this.txtGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGharardad.Border.Class = "TextBoxBorder";
            this.txtGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtGharardad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtGharardad.Location = new System.Drawing.Point(815, 10);
            this.txtGharardad.Margin = new System.Windows.Forms.Padding(4);
            this.txtGharardad.Name = "txtGharardad";
            this.txtGharardad.PreventEnterBeep = true;
            this.txtGharardad.ReadOnly = true;
            this.txtGharardad.Size = new System.Drawing.Size(103, 28);
            this.txtGharardad.TabIndex = 161;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(942, 15);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 18);
            this.label22.TabIndex = 160;
            this.label22.Text = "شماره قرارداد:";
            // 
            // txtDarsadMaliat
            // 
            this.txtDarsadMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDarsadMaliat.Border.Class = "TextBoxBorder";
            this.txtDarsadMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDarsadMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtDarsadMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtDarsadMaliat.Location = new System.Drawing.Point(815, 307);
            this.txtDarsadMaliat.Margin = new System.Windows.Forms.Padding(4);
            this.txtDarsadMaliat.Name = "txtDarsadMaliat";
            this.txtDarsadMaliat.PreventEnterBeep = true;
            this.txtDarsadMaliat.Size = new System.Drawing.Size(56, 28);
            this.txtDarsadMaliat.TabIndex = 159;
            this.txtDarsadMaliat.Text = ".0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(894, 313);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 18);
            this.label17.TabIndex = 158;
            this.label17.Text = "درصد مالیات:";
            // 
            // chtejari
            // 
            this.chtejari.AutoSize = true;
            this.chtejari.BackColor = System.Drawing.Color.Transparent;
            this.chtejari.ForeColor = System.Drawing.Color.Black;
            this.chtejari.Location = new System.Drawing.Point(227, 86);
            this.chtejari.Margin = new System.Windows.Forms.Padding(4);
            this.chtejari.Name = "chtejari";
            this.chtejari.Size = new System.Drawing.Size(215, 25);
            this.chtejari.TabIndex = 157;
            this.chtejari.Text = "این مشترک دارای واحد تجاری است ";
            this.chtejari.UseVisualStyleBackColor = false;
            // 
            // pinfo3
            // 
            this.pinfo3.AutoSize = true;
            this.pinfo3.BackColor = System.Drawing.Color.Transparent;
            this.pinfo3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.pinfo3.ForeColor = System.Drawing.Color.Black;
            this.pinfo3.Location = new System.Drawing.Point(28, 86);
            this.pinfo3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pinfo3.Name = "pinfo3";
            this.pinfo3.Size = new System.Drawing.Size(18, 21);
            this.pinfo3.TabIndex = 156;
            this.pinfo3.Text = "2";
            // 
            // pinfo2
            // 
            this.pinfo2.AutoSize = true;
            this.pinfo2.BackColor = System.Drawing.Color.Transparent;
            this.pinfo2.Font = new System.Drawing.Font("B Yekan", 9F);
            this.pinfo2.ForeColor = System.Drawing.Color.Black;
            this.pinfo2.Location = new System.Drawing.Point(62, 89);
            this.pinfo2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pinfo2.Name = "pinfo2";
            this.pinfo2.Size = new System.Drawing.Size(71, 18);
            this.pinfo2.TabIndex = 155;
            this.pinfo2.Text = "ضریب تجاری :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("B Yekan", 11F, System.Drawing.FontStyle.Underline);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(968, 157);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 23);
            this.label23.TabIndex = 146;
            this.label23.Text = "مبالغ مالی :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(1044, 157);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(34, 28);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Orange;
            this.labelX1.TabIndex = 145;
            // 
            // txt_kasr
            // 
            this.txt_kasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kasr.Border.Class = "TextBoxBorder";
            this.txt_kasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kasr.DisabledBackColor = System.Drawing.Color.White;
            this.txt_kasr.ForeColor = System.Drawing.Color.Black;
            this.txt_kasr.Location = new System.Drawing.Point(32, 344);
            this.txt_kasr.Margin = new System.Windows.Forms.Padding(4);
            this.txt_kasr.Name = "txt_kasr";
            this.txt_kasr.PreventEnterBeep = true;
            this.txt_kasr.ReadOnly = true;
            this.txt_kasr.Size = new System.Drawing.Size(103, 28);
            this.txt_kasr.TabIndex = 142;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(147, 348);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 18);
            this.label21.TabIndex = 141;
            this.label21.Text = "کسر هزار :";
            // 
            // txtPardakhti
            // 
            this.txtPardakhti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPardakhti.Border.Class = "TextBoxBorder";
            this.txtPardakhti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPardakhti.DisabledBackColor = System.Drawing.Color.White;
            this.txtPardakhti.ForeColor = System.Drawing.Color.Black;
            this.txtPardakhti.Location = new System.Drawing.Point(736, 387);
            this.txtPardakhti.Margin = new System.Windows.Forms.Padding(4);
            this.txtPardakhti.Name = "txtPardakhti";
            this.txtPardakhti.PreventEnterBeep = true;
            this.txtPardakhti.ReadOnly = true;
            this.txtPardakhti.Size = new System.Drawing.Size(136, 28);
            this.txtPardakhti.TabIndex = 142;
            this.txtPardakhti.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(894, 393);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 18);
            this.label20.TabIndex = 141;
            this.label20.Text = "مبلغ پرداخت شده :";
            // 
            // txBaghimandeh
            // 
            this.txBaghimandeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txBaghimandeh.Border.Class = "TextBoxBorder";
            this.txBaghimandeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txBaghimandeh.DisabledBackColor = System.Drawing.Color.White;
            this.txBaghimandeh.ForeColor = System.Drawing.Color.Black;
            this.txBaghimandeh.Location = new System.Drawing.Point(32, 387);
            this.txBaghimandeh.Margin = new System.Windows.Forms.Padding(4);
            this.txBaghimandeh.Name = "txBaghimandeh";
            this.txBaghimandeh.PreventEnterBeep = true;
            this.txBaghimandeh.ReadOnly = true;
            this.txBaghimandeh.Size = new System.Drawing.Size(170, 28);
            this.txBaghimandeh.TabIndex = 140;
            this.txBaghimandeh.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(202, 393);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 18);
            this.label15.TabIndex = 139;
            this.label15.Text = "مبلغ باقیمانده :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(626, 205);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 18);
            this.label14.TabIndex = 115;
            this.label14.Text = "علت جریمه :";
            // 
            // txtJarimehTozihat
            // 
            this.txtJarimehTozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtJarimehTozihat.Border.Class = "TextBoxBorder";
            this.txtJarimehTozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtJarimehTozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txtJarimehTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtJarimehTozihat.Location = new System.Drawing.Point(291, 200);
            this.txtJarimehTozihat.Margin = new System.Windows.Forms.Padding(4);
            this.txtJarimehTozihat.Name = "txtJarimehTozihat";
            this.txtJarimehTozihat.PreventEnterBeep = true;
            this.txtJarimehTozihat.Size = new System.Drawing.Size(323, 28);
            this.txtJarimehTozihat.TabIndex = 114;
            // 
            // txt_jarimeh
            // 
            this.txt_jarimeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_jarimeh.Border.Class = "TextBoxBorder";
            this.txt_jarimeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_jarimeh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_jarimeh.ForeColor = System.Drawing.Color.Black;
            this.txt_jarimeh.Location = new System.Drawing.Point(760, 199);
            this.txt_jarimeh.Margin = new System.Windows.Forms.Padding(4);
            this.txt_jarimeh.Name = "txt_jarimeh";
            this.txt_jarimeh.Size = new System.Drawing.Size(122, 28);
            this.txt_jarimeh.TabIndex = 113;
            this.txt_jarimeh.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(905, 204);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 18);
            this.label10.TabIndex = 112;
            this.label10.Text = "جریمه :";
            // 
            // btnHesabSharj
            // 
            this.btnHesabSharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnHesabSharj.BackColor = System.Drawing.Color.Transparent;
            this.btnHesabSharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnHesabSharj.Location = new System.Drawing.Point(13, 121);
            this.btnHesabSharj.Margin = new System.Windows.Forms.Padding(4);
            this.btnHesabSharj.Name = "btnHesabSharj";
            this.btnHesabSharj.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnHesabSharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnHesabSharj.Size = new System.Drawing.Size(223, 28);
            this.btnHesabSharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnHesabSharj.Symbol = "";
            this.btnHesabSharj.SymbolColor = System.Drawing.Color.Green;
            this.btnHesabSharj.SymbolSize = 9F;
            this.btnHesabSharj.TabIndex = 111;
            this.btnHesabSharj.Text = "محاسبه میزان شارژ بر اساس روز";
            this.btnHesabSharj.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(326, 52);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 18);
            this.label9.TabIndex = 110;
            this.label9.Text = "تعداد روزهای این دوره :";
            // 
            // txtDayes
            // 
            this.txtDayes.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDayes.Border.Class = "TextBoxBorder";
            this.txtDayes.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDayes.DisabledBackColor = System.Drawing.Color.White;
            this.txtDayes.ForeColor = System.Drawing.Color.Black;
            this.txtDayes.Location = new System.Drawing.Point(205, 47);
            this.txtDayes.Margin = new System.Windows.Forms.Padding(4);
            this.txtDayes.Name = "txtDayes";
            this.txtDayes.PreventEnterBeep = true;
            this.txtDayes.Size = new System.Drawing.Size(110, 28);
            this.txtDayes.TabIndex = 109;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(829, 466);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 18);
            this.label8.TabIndex = 108;
            this.label8.Text = "توضیحات کلی راجع به این قبض :";
            // 
            // txt_tozihat
            // 
            this.txt_tozihat.AcceptsReturn = true;
            this.txt_tozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_tozihat.Border.Class = "TextBoxBorder";
            this.txt_tozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_tozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_tozihat.ForeColor = System.Drawing.Color.Black;
            this.txt_tozihat.Location = new System.Drawing.Point(265, 463);
            this.txt_tozihat.Margin = new System.Windows.Forms.Padding(4);
            this.txt_tozihat.Multiline = true;
            this.txt_tozihat.Name = "txt_tozihat";
            this.txt_tozihat.PreventEnterBeep = true;
            this.txt_tozihat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_tozihat.Size = new System.Drawing.Size(560, 52);
            this.txt_tozihat.TabIndex = 107;
            // 
            // cmb_vaziat
            // 
            this.cmb_vaziat.DisplayMember = "Text";
            this.cmb_vaziat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_vaziat.Enabled = false;
            this.cmb_vaziat.ForeColor = System.Drawing.Color.Black;
            this.cmb_vaziat.FormattingEnabled = true;
            this.cmb_vaziat.ItemHeight = 22;
            this.cmb_vaziat.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cmb_vaziat.Location = new System.Drawing.Point(590, 533);
            this.cmb_vaziat.Margin = new System.Windows.Forms.Padding(4);
            this.cmb_vaziat.Name = "cmb_vaziat";
            this.cmb_vaziat.Size = new System.Drawing.Size(235, 28);
            this.cmb_vaziat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmb_vaziat.TabIndex = 106;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "پرداخت نشده";
            this.comboItem1.Value = "0";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "پرداخت شده";
            this.comboItem2.Value = "1";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "پرداخت جزئی";
            this.comboItem3.Value = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(829, 541);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 18);
            this.label7.TabIndex = 105;
            this.label7.Text = "وضعیت پرداخت قبض :";
            // 
            // txtBedehi
            // 
            this.txtBedehi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBedehi.Border.Class = "TextBoxBorder";
            this.txtBedehi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBedehi.DisabledBackColor = System.Drawing.Color.White;
            this.txtBedehi.ForeColor = System.Drawing.Color.Black;
            this.txtBedehi.Location = new System.Drawing.Point(760, 162);
            this.txtBedehi.Margin = new System.Windows.Forms.Padding(4);
            this.txtBedehi.Name = "txtBedehi";
            this.txtBedehi.PreventEnterBeep = true;
            this.txtBedehi.Size = new System.Drawing.Size(122, 28);
            this.txtBedehi.TabIndex = 104;
            this.txtBedehi.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(905, 167);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 18);
            this.label19.TabIndex = 103;
            this.label19.Text = "بدهی:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(623, 239);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 18);
            this.label18.TabIndex = 102;
            this.label18.Text = "عنوان هزینه:";
            // 
            // txt_sayer_tozihat
            // 
            this.txt_sayer_tozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sayer_tozihat.Border.Class = "TextBoxBorder";
            this.txt_sayer_tozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sayer_tozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sayer_tozihat.ForeColor = System.Drawing.Color.Black;
            this.txt_sayer_tozihat.Location = new System.Drawing.Point(291, 235);
            this.txt_sayer_tozihat.Margin = new System.Windows.Forms.Padding(4);
            this.txt_sayer_tozihat.Name = "txt_sayer_tozihat";
            this.txt_sayer_tozihat.PreventEnterBeep = true;
            this.txt_sayer_tozihat.Size = new System.Drawing.Size(322, 28);
            this.txt_sayer_tozihat.TabIndex = 101;
            // 
            // txt_ghabz_code
            // 
            this.txt_ghabz_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ghabz_code.Border.Class = "TextBoxBorder";
            this.txt_ghabz_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ghabz_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ghabz_code.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ghabz_code.ForeColor = System.Drawing.Color.Black;
            this.txt_ghabz_code.Location = new System.Drawing.Point(815, 46);
            this.txt_ghabz_code.Margin = new System.Windows.Forms.Padding(4);
            this.txt_ghabz_code.Name = "txt_ghabz_code";
            this.txt_ghabz_code.PreventEnterBeep = true;
            this.txt_ghabz_code.ReadOnly = true;
            this.txt_ghabz_code.Size = new System.Drawing.Size(103, 28);
            this.txt_ghabz_code.TabIndex = 100;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(942, 51);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 18);
            this.label16.TabIndex = 24;
            this.label16.Text = "کد قبض:";
            // 
            // txtMablaghKol
            // 
            this.txtMablaghKol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghKol.Border.Class = "TextBoxBorder";
            this.txtMablaghKol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghKol.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghKol.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghKol.Location = new System.Drawing.Point(736, 348);
            this.txtMablaghKol.Margin = new System.Windows.Forms.Padding(4);
            this.txtMablaghKol.Name = "txtMablaghKol";
            this.txtMablaghKol.PreventEnterBeep = true;
            this.txtMablaghKol.ReadOnly = true;
            this.txtMablaghKol.Size = new System.Drawing.Size(136, 28);
            this.txtMablaghKol.TabIndex = 22;
            this.txtMablaghKol.Text = "0";
            // 
            // txtMaliat
            // 
            this.txtMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMaliat.Border.Class = "TextBoxBorder";
            this.txtMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtMaliat.Location = new System.Drawing.Point(421, 307);
            this.txtMaliat.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaliat.Name = "txtMaliat";
            this.txtMaliat.PreventEnterBeep = true;
            this.txtMaliat.ReadOnly = true;
            this.txtMaliat.Size = new System.Drawing.Size(138, 28);
            this.txtMaliat.TabIndex = 20;
            this.txtMaliat.Text = "0";
            this.txtMaliat.Leave += new System.EventHandler(this.txtMaliat_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(895, 238);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 18);
            this.label13.TabIndex = 19;
            this.label13.Text = "هزینه اضافی:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(894, 350);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 18);
            this.label12.TabIndex = 18;
            this.label12.Text = "مبلغ کل :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(582, 313);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 18);
            this.label11.TabIndex = 16;
            this.label11.Text = "مالیات بر ارزش افزوده :";
            // 
            // txtMohlat
            // 
            this.txtMohlat.Location = new System.Drawing.Point(32, 10);
            this.txtMohlat.Name = "txtMohlat";
            this.txtMohlat.Readonly = true;
            this.txtMohlat.Size = new System.Drawing.Size(165, 25);
            this.txtMohlat.TabIndex = 15;
            this.txtMohlat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txtTarikhSoddor
            // 
            this.txtTarikhSoddor.Location = new System.Drawing.Point(363, 10);
            this.txtTarikhSoddor.Name = "txtTarikhSoddor";
            this.txtTarikhSoddor.Readonly = true;
            this.txtTarikhSoddor.Size = new System.Drawing.Size(165, 25);
            this.txtTarikhSoddor.TabIndex = 14;
            this.txtTarikhSoddor.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txtMablaghHoroof
            // 
            this.txtMablaghHoroof.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghHoroof.Border.Class = "TextBoxBorder";
            this.txtMablaghHoroof.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghHoroof.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghHoroof.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghHoroof.Location = new System.Drawing.Point(236, 348);
            this.txtMablaghHoroof.Margin = new System.Windows.Forms.Padding(4);
            this.txtMablaghHoroof.Name = "txtMablaghHoroof";
            this.txtMablaghHoroof.PreventEnterBeep = true;
            this.txtMablaghHoroof.ReadOnly = true;
            this.txtMablaghHoroof.Size = new System.Drawing.Size(491, 28);
            this.txtMablaghHoroof.TabIndex = 13;
            // 
            // txtMablagh
            // 
            this.txtMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablagh.Border.Class = "TextBoxBorder";
            this.txtMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablagh.ForeColor = System.Drawing.Color.Black;
            this.txtMablagh.Location = new System.Drawing.Point(13, 187);
            this.txtMablagh.Margin = new System.Windows.Forms.Padding(4);
            this.txtMablagh.Name = "txtMablagh";
            this.txtMablagh.PreventEnterBeep = true;
            this.txtMablagh.ReadOnly = true;
            this.txtMablagh.Size = new System.Drawing.Size(136, 28);
            this.txtMablagh.TabIndex = 12;
            this.txtMablagh.Text = "0";
            // 
            // txtMetraj
            // 
            this.txtMetraj.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMetraj.Border.Class = "TextBoxBorder";
            this.txtMetraj.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMetraj.DisabledBackColor = System.Drawing.Color.White;
            this.txtMetraj.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMetraj.ForeColor = System.Drawing.Color.Black;
            this.txtMetraj.Location = new System.Drawing.Point(815, 84);
            this.txtMetraj.Margin = new System.Windows.Forms.Padding(4);
            this.txtMetraj.Name = "txtMetraj";
            this.txtMetraj.PreventEnterBeep = true;
            this.txtMetraj.Size = new System.Drawing.Size(103, 28);
            this.txtMetraj.TabIndex = 9;
            // 
            // txtTarefe
            // 
            this.txtTarefe.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTarefe.Border.Class = "TextBoxBorder";
            this.txtTarefe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTarefe.DisabledBackColor = System.Drawing.Color.White;
            this.txtTarefe.ForeColor = System.Drawing.Color.Black;
            this.txtTarefe.Location = new System.Drawing.Point(510, 84);
            this.txtTarefe.Margin = new System.Windows.Forms.Padding(4);
            this.txtTarefe.Name = "txtTarefe";
            this.txtTarefe.PreventEnterBeep = true;
            this.txtTarefe.Size = new System.Drawing.Size(103, 28);
            this.txtTarefe.TabIndex = 8;
            // 
            // txtdoreh
            // 
            this.txtdoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtdoreh.Border.Class = "TextBoxBorder";
            this.txtdoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtdoreh.DisabledBackColor = System.Drawing.Color.White;
            this.txtdoreh.ForeColor = System.Drawing.Color.Black;
            this.txtdoreh.Location = new System.Drawing.Point(510, 46);
            this.txtdoreh.Margin = new System.Windows.Forms.Padding(4);
            this.txtdoreh.Name = "txtdoreh";
            this.txtdoreh.PreventEnterBeep = true;
            this.txtdoreh.ReadOnly = true;
            this.txtdoreh.Size = new System.Drawing.Size(103, 28);
            this.txtdoreh.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(157, 192);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "مبلغ شارژ پس از تخفیف :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(942, 89);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "متراژ :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(205, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "مهلت پرداخت :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(623, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "تعرفه  شارژ در این دوره :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(557, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "تاریخ صدور قبض :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(623, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "دوره  :";
            // 
            // Frm_sharj_details
            // 
            this.AcceptButton = this.btn_save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 671);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_sharj_details";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_sharj_details_FormClosed);
            this.Load += new System.EventHandler(this.Frm_sharj_details_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_sharj_details_KeyDown);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.Label label18;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sayer_tozihat;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ghabz_code;
        private System.Windows.Forms.Label label16;
        private MoneyTextBox txtMablaghKol;
        private MoneyTextBox txtMaliat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private FarsiLibrary.Win.Controls.FADatePicker txtMohlat;
        private FarsiLibrary.Win.Controls.FADatePicker txtTarikhSoddor;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMablaghHoroof;
        private MoneyTextBox txtMablagh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMetraj;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTarefe;
        private DevComponents.DotNetBar.Controls.TextBoxX txtdoreh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MoneyTextBox txtBedehi;
        private System.Windows.Forms.Label label19;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmb_vaziat;
        private System.Windows.Forms.Label label7;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_tozihat;
        private System.Windows.Forms.Label label9;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDayes;
        private DevComponents.DotNetBar.ButtonX btnHesabSharj;
        private MoneyTextBox txt_jarimeh;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private DevComponents.DotNetBar.Controls.TextBoxX txtJarimehTozihat;
        private MoneyTextBox txtPardakhti;
        private System.Windows.Forms.Label label20;
        private MoneyTextBox txBaghimandeh;
        private System.Windows.Forms.Label label15;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_kasr;
        private System.Windows.Forms.Label label21;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private System.Windows.Forms.Label label23;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Label pinfo3;
        private System.Windows.Forms.Label pinfo2;
        private System.Windows.Forms.CheckBox chtejari;
        private FloatTextBox txtDarsadMaliat;
        private System.Windows.Forms.Label label17;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGharardad;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblBestankar;
        private MoneyTextBox txtBestankar;
        private System.Windows.Forms.Label label24;
        private MoneyTextBox TxtBestankariFromLastDoreh;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sayer;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_shenase_pardakht;
        private System.Windows.Forms.Label label26;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_shenase_ghabz;
        private System.Windows.Forms.Label label25;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private MoneyTextBox txt_takhfif;
        private System.Windows.Forms.Label label27;
    }
}