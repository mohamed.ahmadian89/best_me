﻿namespace CWMS
{
    partial class Frm_sharj_management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_sharj_management));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.grp_gozaresh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.btn_gozesh_karbodi_jame_sharj = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.grpdelete = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.grpStat = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblInfoAboutMoshahedeh = new DevComponents.DotNetBar.LabelX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.lbl_dar_hal_barrrasi = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnShowGhobooz = new DevComponents.DotNetBar.ButtonX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.lbl_mohlatPardakht = new DevComponents.DotNetBar.LabelX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.lbl_sharj_mablagh = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.lblSelectedDoreh = new DevComponents.DotNetBar.LabelX();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnChap = new DevComponents.DotNetBar.ButtonX();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblSuccess = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btnSodoorGhabzForAllMoshtarekin = new DevComponents.DotNetBar.ButtonX();
            this.lblDorehInfotext = new DevComponents.DotNetBar.LabelX();
            this.tabdorehdetails = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel9 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX41 = new DevComponents.DotNetBar.LabelX();
            this.labelX42 = new DevComponents.DotNetBar.LabelX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.labelX39 = new DevComponents.DotNetBar.LabelX();
            this.labelX40 = new DevComponents.DotNetBar.LabelX();
            this.labelX38 = new DevComponents.DotNetBar.LabelX();
            this.labelX32 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX45 = new DevComponents.DotNetBar.LabelX();
            this.txttejarizarib = new CWMS.FloatTextBox();
            this.labelX46 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_doreh_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_mohlat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_mablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.btn_set_parameter = new DevComponents.DotNetBar.ButtonX();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.txtDarsadMaliat = new CWMS.FloatTextBox();
            this.TabNEwDoreh = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.btnShowDetails = new DevComponents.DotNetBar.ButtonX();
            this.btn_delete = new DevComponents.DotNetBar.ButtonX();
            this.dgv_doreh = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.starttimeDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.endtimeDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mohlatpardakhtDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mablaghsharjDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.darsad_maliat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZaribTejari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.sharj_dorehTableAdapter = new CWMS.MainDataSestTableAdapters.sharj_dorehTableAdapter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblAlert = new DevComponents.DotNetBar.LabelX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.grp_gozaresh.SuspendLayout();
            this.grpdelete.SuspendLayout();
            this.grpStat.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.groupPanel9.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.SuspendLayout();
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(40, 3);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(1183, 517);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 0;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabNEwDoreh,
            this.superTabItem2,
            this.tabdorehdetails});
            this.superTabControl1.Text = "مدیریت دوره ها";
            this.superTabControl1.SelectedTabChanging += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripSelectedTabChangingEventArgs>(this.superTabControl1_SelectedTabChanging);
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.grp_gozaresh);
            this.superTabControlPanel3.Controls.Add(this.grpdelete);
            this.superTabControlPanel3.Controls.Add(this.grpStat);
            this.superTabControlPanel3.Controls.Add(this.groupPanel3);
            this.superTabControlPanel3.Controls.Add(this.groupPanel4);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(1183, 488);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.tabdorehdetails;
            // 
            // grp_gozaresh
            // 
            this.grp_gozaresh.BackColor = System.Drawing.Color.White;
            this.grp_gozaresh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_gozaresh.Controls.Add(this.buttonX9);
            this.grp_gozaresh.Controls.Add(this.btn_gozesh_karbodi_jame_sharj);
            this.grp_gozaresh.Controls.Add(this.buttonX3);
            this.grp_gozaresh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_gozaresh.Location = new System.Drawing.Point(45, 194);
            this.grp_gozaresh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grp_gozaresh.Name = "grp_gozaresh";
            this.grp_gozaresh.Size = new System.Drawing.Size(641, 106);
            // 
            // 
            // 
            this.grp_gozaresh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_gozaresh.Style.BackColorGradientAngle = 90;
            this.grp_gozaresh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_gozaresh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_gozaresh.Style.BorderBottomWidth = 1;
            this.grp_gozaresh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_gozaresh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_gozaresh.Style.BorderLeftWidth = 1;
            this.grp_gozaresh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_gozaresh.Style.BorderRightWidth = 1;
            this.grp_gozaresh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_gozaresh.Style.BorderTopWidth = 1;
            this.grp_gozaresh.Style.CornerDiameter = 4;
            this.grp_gozaresh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_gozaresh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_gozaresh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_gozaresh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_gozaresh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_gozaresh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_gozaresh.TabIndex = 26;
            this.grp_gozaresh.Text = "گزارشات کاربردی";
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Transparent;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(17, 45);
            this.buttonX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX9.Size = new System.Drawing.Size(278, 24);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.Symbol = "";
            this.buttonX9.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX9.SymbolSize = 12F;
            this.buttonX9.TabIndex = 58;
            this.buttonX9.Text = "صدور پیش نویس قبوض  شارژ ( حسابداری )";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // btn_gozesh_karbodi_jame_sharj
            // 
            this.btn_gozesh_karbodi_jame_sharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_gozesh_karbodi_jame_sharj.BackColor = System.Drawing.Color.Transparent;
            this.btn_gozesh_karbodi_jame_sharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_gozesh_karbodi_jame_sharj.Location = new System.Drawing.Point(17, 15);
            this.btn_gozesh_karbodi_jame_sharj.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_gozesh_karbodi_jame_sharj.Name = "btn_gozesh_karbodi_jame_sharj";
            this.btn_gozesh_karbodi_jame_sharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btn_gozesh_karbodi_jame_sharj.Size = new System.Drawing.Size(278, 24);
            this.btn_gozesh_karbodi_jame_sharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_gozesh_karbodi_jame_sharj.Symbol = "";
            this.btn_gozesh_karbodi_jame_sharj.SymbolColor = System.Drawing.Color.Teal;
            this.btn_gozesh_karbodi_jame_sharj.SymbolSize = 12F;
            this.btn_gozesh_karbodi_jame_sharj.TabIndex = 57;
            this.btn_gozesh_karbodi_jame_sharj.Text = "گزارش جامع شارژ";
            this.btn_gozesh_karbodi_jame_sharj.Click += new System.EventHandler(this.btn_gozesh_karbodi_jame_sharj_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(339, 15);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX3.Size = new System.Drawing.Size(278, 24);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX3.SymbolSize = 12F;
            this.buttonX3.TabIndex = 56;
            this.buttonX3.Text = "گزارش ارزش افزوده دوره  جاری";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_2);
            // 
            // grpdelete
            // 
            this.grpdelete.BackColor = System.Drawing.Color.White;
            this.grpdelete.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpdelete.Controls.Add(this.buttonX1);
            this.grpdelete.Controls.Add(this.buttonX5);
            this.grpdelete.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpdelete.Location = new System.Drawing.Point(40, 316);
            this.grpdelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpdelete.Name = "grpdelete";
            this.grpdelete.Size = new System.Drawing.Size(641, 118);
            // 
            // 
            // 
            this.grpdelete.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpdelete.Style.BackColorGradientAngle = 90;
            this.grpdelete.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpdelete.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderBottomWidth = 1;
            this.grpdelete.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpdelete.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderLeftWidth = 1;
            this.grpdelete.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderRightWidth = 1;
            this.grpdelete.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderTopWidth = 1;
            this.grpdelete.Style.CornerDiameter = 4;
            this.grpdelete.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpdelete.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpdelete.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpdelete.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpdelete.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpdelete.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpdelete.TabIndex = 24;
            this.grpdelete.Text = "حذف کلیه قبوض دوره جاری";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(57, 45);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX1.Size = new System.Drawing.Size(520, 27);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 58;
            this.buttonX1.Text = "صرفا قبوض دوره حذف شود و پرداختی های مربوطه در سیستم ذخیره بماند";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click_1);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(57, 12);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX5.Size = new System.Drawing.Size(520, 27);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX5.SymbolSize = 12F;
            this.buttonX5.TabIndex = 57;
            this.buttonX5.Text = "حذف کلیه قبوض دوره جاری همراه با کلیه پرداختی های ثبت شده در سامانه پرداخت";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // grpStat
            // 
            this.grpStat.BackColor = System.Drawing.Color.White;
            this.grpStat.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpStat.Controls.Add(this.lblInfoAboutMoshahedeh);
            this.grpStat.Controls.Add(this.buttonX8);
            this.grpStat.Controls.Add(this.buttonX7);
            this.grpStat.Controls.Add(this.buttonX6);
            this.grpStat.Controls.Add(this.lbl_dar_hal_barrrasi);
            this.grpStat.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpStat.Location = new System.Drawing.Point(742, 194);
            this.grpStat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpStat.Name = "grpStat";
            this.grpStat.Size = new System.Drawing.Size(421, 187);
            // 
            // 
            // 
            this.grpStat.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpStat.Style.BackColorGradientAngle = 90;
            this.grpStat.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpStat.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderBottomWidth = 1;
            this.grpStat.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpStat.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderLeftWidth = 1;
            this.grpStat.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderRightWidth = 1;
            this.grpStat.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderTopWidth = 1;
            this.grpStat.Style.CornerDiameter = 4;
            this.grpStat.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpStat.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpStat.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpStat.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpStat.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpStat.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpStat.TabIndex = 23;
            this.grpStat.Text = "سامانه اطلاع رسانی";
            this.grpStat.Click += new System.EventHandler(this.grpStat_Click);
            // 
            // lblInfoAboutMoshahedeh
            // 
            this.lblInfoAboutMoshahedeh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblInfoAboutMoshahedeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblInfoAboutMoshahedeh.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.lblInfoAboutMoshahedeh.ForeColor = System.Drawing.Color.Black;
            this.lblInfoAboutMoshahedeh.Location = new System.Drawing.Point(25, 113);
            this.lblInfoAboutMoshahedeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblInfoAboutMoshahedeh.Name = "lblInfoAboutMoshahedeh";
            this.lblInfoAboutMoshahedeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblInfoAboutMoshahedeh.Size = new System.Drawing.Size(345, 27);
            this.lblInfoAboutMoshahedeh.TabIndex = 84;
            this.lblInfoAboutMoshahedeh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Transparent;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Location = new System.Drawing.Point(18, 47);
            this.buttonX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX8.Size = new System.Drawing.Size(352, 27);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX8.SymbolSize = 12F;
            this.buttonX8.TabIndex = 81;
            this.buttonX8.Text = "قبوض دوره فعلی برای مشترکین قابل مشاهده نباشد";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Transparent;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Location = new System.Drawing.Point(18, 14);
            this.buttonX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX7.Size = new System.Drawing.Size(352, 27);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.Symbol = "";
            this.buttonX7.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX7.SymbolSize = 12F;
            this.buttonX7.TabIndex = 80;
            this.buttonX7.Text = "قبوض دوره فعلی برای مشترکین قابل مشاهده باشد";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(18, 80);
            this.buttonX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX6.Size = new System.Drawing.Size(352, 27);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX6.SymbolSize = 12F;
            this.buttonX6.TabIndex = 79;
            this.buttonX6.Text = "سامانه اطلاع رسانی مشترکین";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // lbl_dar_hal_barrrasi
            // 
            this.lbl_dar_hal_barrrasi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_dar_hal_barrrasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_dar_hal_barrrasi.ForeColor = System.Drawing.Color.Black;
            this.lbl_dar_hal_barrrasi.Location = new System.Drawing.Point(68, 37);
            this.lbl_dar_hal_barrrasi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_dar_hal_barrrasi.Name = "lbl_dar_hal_barrrasi";
            this.lbl_dar_hal_barrrasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_dar_hal_barrrasi.Size = new System.Drawing.Size(83, 27);
            this.lbl_dar_hal_barrrasi.TabIndex = 77;
            this.lbl_dar_hal_barrrasi.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.btnShowGhobooz);
            this.groupPanel3.Controls.Add(this.labelX27);
            this.groupPanel3.Controls.Add(this.labelX28);
            this.groupPanel3.Controls.Add(this.lbl_mohlatPardakht);
            this.groupPanel3.Controls.Add(this.labelX25);
            this.groupPanel3.Controls.Add(this.labelX26);
            this.groupPanel3.Controls.Add(this.lbl_sharj_mablagh);
            this.groupPanel3.Controls.Add(this.labelX24);
            this.groupPanel3.Controls.Add(this.labelX23);
            this.groupPanel3.Controls.Add(this.lblSelectedDoreh);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(812, 26);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(352, 161);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 14;
            this.groupPanel3.Text = "اطلاعات دوره ";
            // 
            // btnShowGhobooz
            // 
            this.btnShowGhobooz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowGhobooz.BackColor = System.Drawing.Color.Transparent;
            this.btnShowGhobooz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowGhobooz.Location = new System.Drawing.Point(18, 102);
            this.btnShowGhobooz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowGhobooz.Name = "btnShowGhobooz";
            this.btnShowGhobooz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowGhobooz.Size = new System.Drawing.Size(263, 27);
            this.btnShowGhobooz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowGhobooz.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowGhobooz.SymbolSize = 12F;
            this.btnShowGhobooz.TabIndex = 63;
            this.btnShowGhobooz.Text = "نمایش قبوض صادرشده";
            this.btnShowGhobooz.Click += new System.EventHandler(this.btnShowGhobooz_Click);
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(274, 73);
            this.labelX27.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX27.Name = "labelX27";
            this.labelX27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX27.Size = new System.Drawing.Size(34, 27);
            this.labelX27.Symbol = "";
            this.labelX27.SymbolColor = System.Drawing.Color.Teal;
            this.labelX27.SymbolSize = 15F;
            this.labelX27.TabIndex = 57;
            this.labelX27.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(163, 73);
            this.labelX28.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX28.Name = "labelX28";
            this.labelX28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX28.Size = new System.Drawing.Size(115, 27);
            this.labelX28.TabIndex = 56;
            this.labelX28.Text = "مهلت پرداخت:";
            this.labelX28.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_mohlatPardakht
            // 
            this.lbl_mohlatPardakht.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_mohlatPardakht.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_mohlatPardakht.ForeColor = System.Drawing.Color.Black;
            this.lbl_mohlatPardakht.Location = new System.Drawing.Point(4, 73);
            this.lbl_mohlatPardakht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_mohlatPardakht.Name = "lbl_mohlatPardakht";
            this.lbl_mohlatPardakht.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_mohlatPardakht.Size = new System.Drawing.Size(156, 27);
            this.lbl_mohlatPardakht.TabIndex = 55;
            this.lbl_mohlatPardakht.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(274, 38);
            this.labelX25.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX25.Name = "labelX25";
            this.labelX25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX25.Size = new System.Drawing.Size(34, 27);
            this.labelX25.Symbol = "";
            this.labelX25.SymbolColor = System.Drawing.Color.Teal;
            this.labelX25.SymbolSize = 15F;
            this.labelX25.TabIndex = 54;
            this.labelX25.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(163, 38);
            this.labelX26.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX26.Name = "labelX26";
            this.labelX26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX26.Size = new System.Drawing.Size(115, 27);
            this.labelX26.TabIndex = 53;
            this.labelX26.Text = "مبلغ شارژ:";
            this.labelX26.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_sharj_mablagh
            // 
            this.lbl_sharj_mablagh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_sharj_mablagh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_sharj_mablagh.ForeColor = System.Drawing.Color.Black;
            this.lbl_sharj_mablagh.Location = new System.Drawing.Point(45, 38);
            this.lbl_sharj_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_sharj_mablagh.Name = "lbl_sharj_mablagh";
            this.lbl_sharj_mablagh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_sharj_mablagh.Size = new System.Drawing.Size(115, 27);
            this.lbl_sharj_mablagh.TabIndex = 52;
            this.lbl_sharj_mablagh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(274, 6);
            this.labelX24.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX24.Name = "labelX24";
            this.labelX24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX24.Size = new System.Drawing.Size(34, 27);
            this.labelX24.Symbol = "";
            this.labelX24.SymbolColor = System.Drawing.Color.Teal;
            this.labelX24.SymbolSize = 15F;
            this.labelX24.TabIndex = 51;
            this.labelX24.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(163, 6);
            this.labelX23.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX23.Name = "labelX23";
            this.labelX23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX23.Size = new System.Drawing.Size(115, 27);
            this.labelX23.TabIndex = 50;
            this.labelX23.Text = "دوره :";
            this.labelX23.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblSelectedDoreh
            // 
            this.lblSelectedDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSelectedDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSelectedDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblSelectedDoreh.Location = new System.Drawing.Point(45, 6);
            this.lblSelectedDoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblSelectedDoreh.Name = "lblSelectedDoreh";
            this.lblSelectedDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSelectedDoreh.Size = new System.Drawing.Size(115, 27);
            this.lblSelectedDoreh.TabIndex = 49;
            this.lblSelectedDoreh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.btnChap);
            this.groupPanel4.Controls.Add(this.progressBar1);
            this.groupPanel4.Controls.Add(this.lblSuccess);
            this.groupPanel4.Controls.Add(this.labelX7);
            this.groupPanel4.Controls.Add(this.btnSodoorGhabzForAllMoshtarekin);
            this.groupPanel4.Controls.Add(this.lblDorehInfotext);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Location = new System.Drawing.Point(40, 26);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(748, 162);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 13;
            this.groupPanel4.Text = "دوره جاری";
            // 
            // btnChap
            // 
            this.btnChap.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChap.BackColor = System.Drawing.Color.Transparent;
            this.btnChap.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnChap.Location = new System.Drawing.Point(4, 101);
            this.btnChap.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnChap.Name = "btnChap";
            this.btnChap.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.btnChap.Size = new System.Drawing.Size(259, 27);
            this.btnChap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnChap.Symbol = "";
            this.btnChap.SymbolColor = System.Drawing.Color.Teal;
            this.btnChap.SymbolSize = 12F;
            this.btnChap.TabIndex = 80;
            this.btnChap.Text = "چاپ قبوض دوره";
            this.btnChap.Visible = false;
            this.btnChap.Click += new System.EventHandler(this.btnChap_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.Black;
            this.progressBar1.Location = new System.Drawing.Point(373, 102);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(332, 26);
            this.progressBar1.TabIndex = 78;
            // 
            // lblSuccess
            // 
            this.lblSuccess.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSuccess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSuccess.ForeColor = System.Drawing.Color.Black;
            this.lblSuccess.Location = new System.Drawing.Point(57, 11);
            this.lblSuccess.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSuccess.Size = new System.Drawing.Size(57, 53);
            this.lblSuccess.Symbol = "";
            this.lblSuccess.SymbolColor = System.Drawing.Color.Teal;
            this.lblSuccess.SymbolSize = 30F;
            this.lblSuccess.TabIndex = 77;
            this.lblSuccess.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblSuccess.Visible = false;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(590, 24);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(115, 27);
            this.labelX7.TabIndex = 56;
            this.labelX7.Text = "وضعیت فعلی دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnSodoorGhabzForAllMoshtarekin
            // 
            this.btnSodoorGhabzForAllMoshtarekin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSodoorGhabzForAllMoshtarekin.BackColor = System.Drawing.Color.Transparent;
            this.btnSodoorGhabzForAllMoshtarekin.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSodoorGhabzForAllMoshtarekin.Location = new System.Drawing.Point(50, 69);
            this.btnSodoorGhabzForAllMoshtarekin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSodoorGhabzForAllMoshtarekin.Name = "btnSodoorGhabzForAllMoshtarekin";
            this.btnSodoorGhabzForAllMoshtarekin.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnSodoorGhabzForAllMoshtarekin.Size = new System.Drawing.Size(659, 27);
            this.btnSodoorGhabzForAllMoshtarekin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSodoorGhabzForAllMoshtarekin.Symbol = "";
            this.btnSodoorGhabzForAllMoshtarekin.SymbolColor = System.Drawing.Color.Teal;
            this.btnSodoorGhabzForAllMoshtarekin.SymbolSize = 12F;
            this.btnSodoorGhabzForAllMoshtarekin.TabIndex = 55;
            this.btnSodoorGhabzForAllMoshtarekin.Text = "جهت صدور قبوض این دوره بر روی این دکمه کلیک نمایید";
            this.btnSodoorGhabzForAllMoshtarekin.Click += new System.EventHandler(this.btnSodoorGhabzForAllMoshtarekin_Click);
            // 
            // lblDorehInfotext
            // 
            this.lblDorehInfotext.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblDorehInfotext.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDorehInfotext.ForeColor = System.Drawing.Color.Black;
            this.lblDorehInfotext.Location = new System.Drawing.Point(181, 24);
            this.lblDorehInfotext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblDorehInfotext.Name = "lblDorehInfotext";
            this.lblDorehInfotext.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDorehInfotext.Size = new System.Drawing.Size(401, 27);
            this.lblDorehInfotext.TabIndex = 52;
            this.lblDorehInfotext.Text = "تعداد قبوض پرداخت شده:";
            // 
            // tabdorehdetails
            // 
            this.tabdorehdetails.AttachedControl = this.superTabControlPanel3;
            this.tabdorehdetails.GlobalItem = false;
            this.tabdorehdetails.Name = "tabdorehdetails";
            this.tabdorehdetails.Text = "اطلاعات دوره ";
            this.tabdorehdetails.Visible = false;
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.groupPanel9);
            this.superTabControlPanel1.Controls.Add(this.groupPanel1);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(1183, 488);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.TabNEwDoreh;
            // 
            // groupPanel9
            // 
            this.groupPanel9.BackColor = System.Drawing.Color.White;
            this.groupPanel9.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel9.Controls.Add(this.labelX8);
            this.groupPanel9.Controls.Add(this.labelX9);
            this.groupPanel9.Controls.Add(this.labelX41);
            this.groupPanel9.Controls.Add(this.labelX42);
            this.groupPanel9.Controls.Add(this.buttonX2);
            this.groupPanel9.Controls.Add(this.labelX39);
            this.groupPanel9.Controls.Add(this.labelX40);
            this.groupPanel9.Controls.Add(this.labelX38);
            this.groupPanel9.Controls.Add(this.labelX32);
            this.groupPanel9.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel9.Location = new System.Drawing.Point(42, 262);
            this.groupPanel9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel9.Name = "groupPanel9";
            this.groupPanel9.Size = new System.Drawing.Size(1122, 197);
            // 
            // 
            // 
            this.groupPanel9.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel9.Style.BackColorGradientAngle = 90;
            this.groupPanel9.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel9.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderBottomWidth = 1;
            this.groupPanel9.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel9.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderLeftWidth = 1;
            this.groupPanel9.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderRightWidth = 1;
            this.groupPanel9.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel9.Style.BorderTopWidth = 1;
            this.groupPanel9.Style.CornerDiameter = 4;
            this.groupPanel9.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel9.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel9.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel9.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel9.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel9.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel9.TabIndex = 1;
            this.groupPanel9.Text = "سیستم راهنما";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(1045, 124);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX8.Size = new System.Drawing.Size(34, 27);
            this.labelX8.Symbol = "";
            this.labelX8.SymbolColor = System.Drawing.Color.Teal;
            this.labelX8.SymbolSize = 15F;
            this.labelX8.TabIndex = 65;
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(144, 125);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(892, 27);
            this.labelX9.TabIndex = 64;
            this.labelX9.Text = "مبلغ شارژ به صورت روزانه می باشد . در نتیجه نحوه محاسبه مبلغ شارژ بر اساس حاصلضرب" +
    " تعداد روزهای دوره در مبلغ شارژ روزانه می باشد";
            // 
            // labelX41
            // 
            this.labelX41.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX41.ForeColor = System.Drawing.Color.Black;
            this.labelX41.Location = new System.Drawing.Point(1045, 55);
            this.labelX41.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX41.Name = "labelX41";
            this.labelX41.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX41.Size = new System.Drawing.Size(34, 27);
            this.labelX41.Symbol = "";
            this.labelX41.SymbolColor = System.Drawing.Color.Teal;
            this.labelX41.SymbolSize = 15F;
            this.labelX41.TabIndex = 63;
            this.labelX41.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX42
            // 
            this.labelX42.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX42.ForeColor = System.Drawing.Color.Black;
            this.labelX42.Location = new System.Drawing.Point(53, 57);
            this.labelX42.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX42.Name = "labelX42";
            this.labelX42.Size = new System.Drawing.Size(984, 27);
            this.labelX42.TabIndex = 62;
            this.labelX42.Text = "در صورتی که اطلاعات دوره جدید ، مقادیر معتبری نداشته باشد می توانید با کلیک بر رو" +
    "ی دکمه اطلاعات دوره جدید ، مقادیر درست را برای آنها تنظیم نمایید";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(97, 89);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(152, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 10F;
            this.buttonX2.TabIndex = 61;
            this.buttonX2.Text = "مبالغ شارژ";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // labelX39
            // 
            this.labelX39.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX39.ForeColor = System.Drawing.Color.Black;
            this.labelX39.Location = new System.Drawing.Point(1045, 89);
            this.labelX39.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX39.Name = "labelX39";
            this.labelX39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX39.Size = new System.Drawing.Size(34, 27);
            this.labelX39.Symbol = "";
            this.labelX39.SymbolColor = System.Drawing.Color.Teal;
            this.labelX39.SymbolSize = 15F;
            this.labelX39.TabIndex = 60;
            this.labelX39.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX40
            // 
            this.labelX40.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX40.ForeColor = System.Drawing.Color.Black;
            this.labelX40.Location = new System.Drawing.Point(257, 90);
            this.labelX40.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX40.Name = "labelX40";
            this.labelX40.Size = new System.Drawing.Size(780, 27);
            this.labelX40.TabIndex = 59;
            this.labelX40.Text = "مبلغ شارژ بر اساس اخرین مبلغ شارژی می باشد که در سیستم تعریف شده است . جهت تغییر " +
    "این مقدار به بخش مبالغ شارژ مراجعه کنید";
            // 
            // labelX38
            // 
            this.labelX38.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX38.ForeColor = System.Drawing.Color.Black;
            this.labelX38.Location = new System.Drawing.Point(1045, 3);
            this.labelX38.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX38.Name = "labelX38";
            this.labelX38.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX38.Size = new System.Drawing.Size(34, 27);
            this.labelX38.Symbol = "";
            this.labelX38.SymbolColor = System.Drawing.Color.Teal;
            this.labelX38.SymbolSize = 15F;
            this.labelX38.TabIndex = 58;
            this.labelX38.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX32
            // 
            this.labelX32.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX32.ForeColor = System.Drawing.Color.Black;
            this.labelX32.Location = new System.Drawing.Point(53, 8);
            this.labelX32.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX32.Name = "labelX32";
            this.labelX32.Size = new System.Drawing.Size(983, 42);
            this.labelX32.TabIndex = 13;
            this.labelX32.Text = resources.GetString("labelX32.Text");
            this.labelX32.WordWrap = true;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.labelX12);
            this.groupPanel1.Controls.Add(this.labelX45);
            this.groupPanel1.Controls.Add(this.txttejarizarib);
            this.groupPanel1.Controls.Add(this.labelX46);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.txt_doreh_code);
            this.groupPanel1.Controls.Add(this.txt_mohlat);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txt_mablagh);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.txt_end);
            this.groupPanel1.Controls.Add(this.txt_start);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.btn_set_parameter);
            this.groupPanel1.Controls.Add(this.btn_create_new_doreh);
            this.groupPanel1.Controls.Add(this.txtDarsadMaliat);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(42, 21);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1122, 235);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "اطلاعات دوره جدید";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(115, 55);
            this.labelX12.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(274, 27);
            this.labelX12.TabIndex = 37;
            this.labelX12.Text = "به ازای هر متر مکعب";
            // 
            // labelX45
            // 
            this.labelX45.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX45.ForeColor = System.Drawing.Color.Black;
            this.labelX45.Location = new System.Drawing.Point(802, 127);
            this.labelX45.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX45.Name = "labelX45";
            this.labelX45.Size = new System.Drawing.Size(40, 27);
            this.labelX45.TabIndex = 36;
            this.labelX45.Text = "برابر";
            // 
            // txttejarizarib
            // 
            this.txttejarizarib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txttejarizarib.Border.Class = "TextBoxBorder";
            this.txttejarizarib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txttejarizarib.DisabledBackColor = System.Drawing.Color.White;
            this.txttejarizarib.ForeColor = System.Drawing.Color.Black;
            this.txttejarizarib.Location = new System.Drawing.Point(856, 125);
            this.txttejarizarib.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txttejarizarib.Name = "txttejarizarib";
            this.txttejarizarib.PreventEnterBeep = true;
            this.txttejarizarib.Size = new System.Drawing.Size(61, 28);
            this.txttejarizarib.TabIndex = 34;
            this.txttejarizarib.Text = ".0";
            // 
            // labelX46
            // 
            this.labelX46.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX46.ForeColor = System.Drawing.Color.Black;
            this.labelX46.Location = new System.Drawing.Point(956, 127);
            this.labelX46.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX46.Name = "labelX46";
            this.labelX46.Size = new System.Drawing.Size(137, 27);
            this.labelX46.TabIndex = 35;
            this.labelX46.Text = "ضریب تجاری  :";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(583, 127);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(106, 27);
            this.labelX10.TabIndex = 24;
            this.labelX10.Text = "درصد مالیات :";
            // 
            // txt_doreh_code
            // 
            this.txt_doreh_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_doreh_code.Border.Class = "TextBoxBorder";
            this.txt_doreh_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_doreh_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_doreh_code.ForeColor = System.Drawing.Color.Black;
            this.txt_doreh_code.Location = new System.Drawing.Point(774, 57);
            this.txt_doreh_code.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_doreh_code.Name = "txt_doreh_code";
            this.txt_doreh_code.PreventEnterBeep = true;
            this.txt_doreh_code.ReadOnly = true;
            this.txt_doreh_code.Size = new System.Drawing.Size(143, 28);
            this.txt_doreh_code.TabIndex = 23;
            this.txt_doreh_code.Enter += new System.EventHandler(this.txt_doreh_code_Enter);
            // 
            // txt_mohlat
            // 
            this.txt_mohlat.Location = new System.Drawing.Point(72, 95);
            this.txt_mohlat.Name = "txt_mohlat";
            this.txt_mohlat.Size = new System.Drawing.Size(182, 24);
            this.txt_mohlat.TabIndex = 22;
            this.txt_mohlat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(270, 93);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(92, 27);
            this.labelX5.TabIndex = 21;
            this.labelX5.Text = "مهلت پرداخت:";
            // 
            // txt_mablagh
            // 
            this.txt_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mablagh.Border.Class = "TextBoxBorder";
            this.txt_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_mablagh.Location = new System.Drawing.Point(401, 52);
            this.txt_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_mablagh.Name = "txt_mablagh";
            this.txt_mablagh.PreventEnterBeep = true;
            this.txt_mablagh.ReadOnly = true;
            this.txt_mablagh.Size = new System.Drawing.Size(111, 28);
            this.txt_mablagh.TabIndex = 20;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(520, 55);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(117, 27);
            this.labelX4.TabIndex = 19;
            this.labelX4.Text = "مبلغ شارژ سالیانه :";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(409, 95);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(182, 24);
            this.txt_end.TabIndex = 17;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(735, 95);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(182, 24);
            this.txt_start.TabIndex = 16;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(585, 93);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(92, 27);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "تاریخ پایان :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(1001, 93);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(92, 27);
            this.labelX2.TabIndex = 14;
            this.labelX2.Text = "تاریخ شروع :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(1018, 58);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 27);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "کد دوره :";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(600, 23);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(477, 27);
            this.labelX6.TabIndex = 12;
            this.labelX6.Text = "کابرگرامی ! جهت تنظیم مقادیر دوره جدید بر روی دکمه  تنظیم کلیک نمایید";
            // 
            // btn_set_parameter
            // 
            this.btn_set_parameter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_set_parameter.BackColor = System.Drawing.Color.Transparent;
            this.btn_set_parameter.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_set_parameter.Location = new System.Drawing.Point(4, 3);
            this.btn_set_parameter.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_set_parameter.Name = "btn_set_parameter";
            this.btn_set_parameter.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_set_parameter.Size = new System.Drawing.Size(221, 27);
            this.btn_set_parameter.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_set_parameter.Symbol = "";
            this.btn_set_parameter.SymbolColor = System.Drawing.Color.Green;
            this.btn_set_parameter.SymbolSize = 10F;
            this.btn_set_parameter.TabIndex = 11;
            this.btn_set_parameter.Text = "اطلاعات دوره جدید";
            this.btn_set_parameter.Click += new System.EventHandler(this.btn_set_parameter_Click);
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(15, 163);
            this.btn_create_new_doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(323, 27);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 10;
            this.btn_create_new_doreh.Text = "ایجاد دوره جدید";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // txtDarsadMaliat
            // 
            this.txtDarsadMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDarsadMaliat.Border.Class = "TextBoxBorder";
            this.txtDarsadMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDarsadMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtDarsadMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtDarsadMaliat.Location = new System.Drawing.Point(472, 125);
            this.txtDarsadMaliat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtDarsadMaliat.Name = "txtDarsadMaliat";
            this.txtDarsadMaliat.PreventEnterBeep = true;
            this.txtDarsadMaliat.Size = new System.Drawing.Size(111, 28);
            this.txtDarsadMaliat.TabIndex = 25;
            this.txtDarsadMaliat.Text = ".0";
            // 
            // TabNEwDoreh
            // 
            this.TabNEwDoreh.AttachedControl = this.superTabControlPanel1;
            this.TabNEwDoreh.GlobalItem = false;
            this.TabNEwDoreh.Name = "TabNEwDoreh";
            this.TabNEwDoreh.Text = "تعریف دوره جدید";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.groupPanel2);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(1183, 488);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX4);
            this.groupPanel2.Controls.Add(this.btnShowDetails);
            this.groupPanel2.Controls.Add(this.btn_delete);
            this.groupPanel2.Controls.Add(this.dgv_doreh);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(26, 20);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1138, 443);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "کلیه دوره های شارژ";
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(264, 358);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX4.Size = new System.Drawing.Size(252, 27);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX4.SymbolSize = 12F;
            this.buttonX4.TabIndex = 83;
            this.buttonX4.Text = "تمدید زمان آخرین دوره";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowDetails.BackColor = System.Drawing.Color.Transparent;
            this.btnShowDetails.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowDetails.Location = new System.Drawing.Point(858, 358);
            this.btnShowDetails.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowDetails.Size = new System.Drawing.Size(252, 27);
            this.btnShowDetails.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowDetails.Symbol = "";
            this.btnShowDetails.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowDetails.SymbolSize = 12F;
            this.btnShowDetails.TabIndex = 82;
            this.btnShowDetails.Text = "نمایش جزئیات دوره";
            this.btnShowDetails.Click += new System.EventHandler(this.buttonX3_Click_1);
            // 
            // btn_delete
            // 
            this.btn_delete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_delete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_delete.Location = new System.Drawing.Point(16, 358);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btn_delete.Size = new System.Drawing.Size(237, 27);
            this.btn_delete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_delete.Symbol = "";
            this.btn_delete.SymbolColor = System.Drawing.Color.DarkRed;
            this.btn_delete.SymbolSize = 12F;
            this.btn_delete.TabIndex = 81;
            this.btn_delete.Text = "حذف آخرین دوره";
            this.btn_delete.Visible = false;
            this.btn_delete.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // dgv_doreh
            // 
            this.dgv_doreh.AllowUserToAddRows = false;
            this.dgv_doreh.AllowUserToDeleteRows = false;
            this.dgv_doreh.AllowUserToResizeColumns = false;
            this.dgv_doreh.AllowUserToResizeRows = false;
            this.dgv_doreh.AutoGenerateColumns = false;
            this.dgv_doreh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_doreh.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_doreh.ColumnHeadersHeight = 30;
            this.dgv_doreh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dcodeDataGridViewTextBoxColumn,
            this.starttimeDataGridViewTextBoxColumn,
            this.endtimeDataGridViewTextBoxColumn,
            this.mohlatpardakhtDataGridViewTextBoxColumn,
            this.mablaghsharjDataGridViewTextBoxColumn,
            this.darsad_maliat,
            this.ZaribTejari});
            this.dgv_doreh.DataSource = this.sharjdorehBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_doreh.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_doreh.EnableHeadersVisualStyles = false;
            this.dgv_doreh.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_doreh.Location = new System.Drawing.Point(16, 9);
            this.dgv_doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_doreh.Name = "dgv_doreh";
            this.dgv_doreh.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_doreh.RowTemplate.Height = 40;
            this.dgv_doreh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_doreh.Size = new System.Drawing.Size(1095, 327);
            this.dgv_doreh.TabIndex = 12;
            this.dgv_doreh.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellContentClick);
            this.dgv_doreh.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellContentDoubleClick);
            this.dgv_doreh.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellClick);
            this.dgv_doreh.SelectionChanged += new System.EventHandler(this.dgv_doreh_SelectionChanged);
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // starttimeDataGridViewTextBoxColumn
            // 
            this.starttimeDataGridViewTextBoxColumn.DataPropertyName = "start_time";
            this.starttimeDataGridViewTextBoxColumn.HeaderText = "تاریخ شروع";
            this.starttimeDataGridViewTextBoxColumn.Name = "starttimeDataGridViewTextBoxColumn";
            this.starttimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.starttimeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.starttimeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // endtimeDataGridViewTextBoxColumn
            // 
            this.endtimeDataGridViewTextBoxColumn.DataPropertyName = "end_time";
            this.endtimeDataGridViewTextBoxColumn.HeaderText = "تاریخ پایان";
            this.endtimeDataGridViewTextBoxColumn.Name = "endtimeDataGridViewTextBoxColumn";
            this.endtimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.endtimeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.endtimeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // mohlatpardakhtDataGridViewTextBoxColumn
            // 
            this.mohlatpardakhtDataGridViewTextBoxColumn.DataPropertyName = "mohlat_pardakht";
            this.mohlatpardakhtDataGridViewTextBoxColumn.HeaderText = "مهلت پرداخت";
            this.mohlatpardakhtDataGridViewTextBoxColumn.Name = "mohlatpardakhtDataGridViewTextBoxColumn";
            this.mohlatpardakhtDataGridViewTextBoxColumn.ReadOnly = true;
            this.mohlatpardakhtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.mohlatpardakhtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // mablaghsharjDataGridViewTextBoxColumn
            // 
            this.mablaghsharjDataGridViewTextBoxColumn.DataPropertyName = "mablagh_sharj";
            this.mablaghsharjDataGridViewTextBoxColumn.FillWeight = 70F;
            this.mablaghsharjDataGridViewTextBoxColumn.HeaderText = "مبلغ شارژ";
            this.mablaghsharjDataGridViewTextBoxColumn.Name = "mablaghsharjDataGridViewTextBoxColumn";
            this.mablaghsharjDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // darsad_maliat
            // 
            this.darsad_maliat.DataPropertyName = "darsad_maliat";
            this.darsad_maliat.FillWeight = 60F;
            this.darsad_maliat.HeaderText = "درصد مالیات";
            this.darsad_maliat.Name = "darsad_maliat";
            this.darsad_maliat.ReadOnly = true;
            // 
            // ZaribTejari
            // 
            this.ZaribTejari.DataPropertyName = "ZaribTejari";
            this.ZaribTejari.FillWeight = 60F;
            this.ZaribTejari.HeaderText = "ضریب تجاری";
            this.ZaribTejari.Name = "ZaribTejari";
            this.ZaribTejari.ReadOnly = true;
            // 
            // sharjdorehBindingSource
            // 
            this.sharjdorehBindingSource.DataMember = "sharj_doreh";
            this.sharjdorehBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "مدیریت دوره ها";
            // 
            // sharj_dorehTableAdapter
            // 
            this.sharj_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblAlert
            // 
            this.lblAlert.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAlert.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAlert.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblAlert.ForeColor = System.Drawing.Color.Black;
            this.lblAlert.Location = new System.Drawing.Point(265, 534);
            this.lblAlert.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.SingleLineColor = System.Drawing.Color.Red;
            this.lblAlert.Size = new System.Drawing.Size(713, 27);
            this.lblAlert.TabIndex = 38;
            this.lblAlert.Text = "تا اتمام تاریخ دوره فعلی امکان صدور دوره جدید وجود ندارد";
            this.lblAlert.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblAlert.Visible = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // Frm_sharj_management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 569);
            this.Controls.Add(this.lblAlert);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_sharj_management";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmManagmentSharjDoreh_FormClosed);
            this.Load += new System.EventHandler(this.FrmManagmentSharjDoreh_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmManagmentSharjDoreh_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            this.grp_gozaresh.ResumeLayout(false);
            this.grpdelete.ResumeLayout(false);
            this.grpStat.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.groupPanel9.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.SuperTabItem TabNEwDoreh;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_doreh_code;
        private FarsiLibrary.Win.Controls.FADatePicker txt_mohlat;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mablagh;
        private DevComponents.DotNetBar.LabelX labelX4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX btn_set_parameter;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.SuperTabItem tabdorehdetails;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX lblSelectedDoreh;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.LabelX lbl_sharj_mablagh;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.LabelX lbl_mohlatPardakht;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_doreh;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource sharjdorehBindingSource;
        private MainDataSestTableAdapters.sharj_dorehTableAdapter sharj_dorehTableAdapter;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel9;
        private DevComponents.DotNetBar.LabelX labelX32;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX labelX39;
        private DevComponents.DotNetBar.LabelX labelX40;
        private DevComponents.DotNetBar.LabelX labelX38;
        private DevComponents.DotNetBar.LabelX labelX41;
        private DevComponents.DotNetBar.LabelX labelX42;
        private DevComponents.DotNetBar.LabelX lblDorehInfotext;
        private DevComponents.DotNetBar.ButtonX btnSodoorGhabzForAllMoshtarekin;
        private FloatTextBox txtDarsadMaliat;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.LabelX labelX45;
        private FloatTextBox txttejarizarib;
        private DevComponents.DotNetBar.LabelX labelX46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn starttimeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn endtimeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn mohlatpardakhtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghsharjDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn darsad_maliat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZaribTejari;
        private DevComponents.DotNetBar.Controls.GroupPanel grpdelete;
        private DevComponents.DotNetBar.Controls.GroupPanel grpStat;
        private DevComponents.DotNetBar.LabelX lbl_dar_hal_barrrasi;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btnShowGhobooz;
        private DevComponents.DotNetBar.LabelX lblSuccess;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX lblAlert;
        private DevComponents.DotNetBar.ButtonX btn_delete;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX btnShowDetails;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.LabelX labelX12;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevComponents.DotNetBar.LabelX lblInfoAboutMoshahedeh;
        private DevComponents.DotNetBar.ButtonX btnChap;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_gozaresh;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX btn_gozesh_karbodi_jame_sharj;
        private DevComponents.DotNetBar.ButtonX buttonX9;
    }
}