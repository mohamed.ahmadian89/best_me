﻿namespace CWMS
{
    partial class FrmElhaghi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.moshtarekinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.moshtarekinTableAdapter = new CWMS.CWMSDataSetTableAdapters.moshtarekinTableAdapter();
            this.dgvElhaghi = new System.Windows.Forms.DataGridView();
            this.gharardad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.co_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modir_amel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghi)).BeginInit();
            this.SuspendLayout();
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // moshtarekinBindingSource
            // 
            this.moshtarekinBindingSource.DataMember = "moshtarekin";
            this.moshtarekinBindingSource.DataSource = this.cWMSDataSet;
            // 
            // moshtarekinTableAdapter
            // 
            this.moshtarekinTableAdapter.ClearBeforeFill = true;
            // 
            // dgvElhaghi
            // 
            this.dgvElhaghi.AllowUserToAddRows = false;
            this.dgvElhaghi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvElhaghi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvElhaghi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvElhaghi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardad,
            this.co_name,
            this.modir_amel});
            this.dgvElhaghi.Location = new System.Drawing.Point(16, 55);
            this.dgvElhaghi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvElhaghi.Name = "dgvElhaghi";
            this.dgvElhaghi.ReadOnly = true;
            this.dgvElhaghi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvElhaghi.RowHeadersVisible = false;
            this.dgvElhaghi.RowTemplate.Height = 24;
            this.dgvElhaghi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvElhaghi.Size = new System.Drawing.Size(856, 465);
            this.dgvElhaghi.TabIndex = 1;
            // 
            // gharardad
            // 
            this.gharardad.HeaderText = "قرارداد";
            this.gharardad.Name = "gharardad";
            this.gharardad.ReadOnly = true;
            // 
            // co_name
            // 
            this.co_name.FillWeight = 250F;
            this.co_name.HeaderText = "نام مشترک";
            this.co_name.Name = "co_name";
            this.co_name.ReadOnly = true;
            // 
            // modir_amel
            // 
            this.modir_amel.FillWeight = 250F;
            this.modir_amel.HeaderText = "نام مدیرعامل";
            this.modir_amel.Name = "modir_amel";
            this.modir_amel.ReadOnly = true;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(28, 539);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(184, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 41;
            this.buttonX2.Text = "ذخیره";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(16, 18);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(184, 27);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 10F;
            this.buttonX3.TabIndex = 45;
            this.buttonX3.Text = "افزودن";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "co_name", true));
            this.textBox1.Location = new System.Drawing.Point(208, 14);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox1.Size = new System.Drawing.Size(520, 28);
            this.textBox1.TabIndex = 47;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DataSource = this.moshtarekinBindingSource;
            this.comboBox1.DisplayMember = "gharardad";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(737, 14);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(133, 28);
            this.comboBox1.TabIndex = 48;
            this.comboBox1.ValueMember = "gharardad";
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(624, 539);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Size = new System.Drawing.Size(248, 27);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonX4.SymbolSize = 10F;
            this.buttonX4.TabIndex = 49;
            this.buttonX4.Text = "حذف قرارداد الحاقیه انتخاب شده";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // FrmElhaghi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 580);
            this.Controls.Add(this.buttonX4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonX3);
            this.Controls.Add(this.buttonX2);
            this.Controls.Add(this.dgvElhaghi);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmElhaghi";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmElhaghi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvElhaghi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource moshtarekinBindingSource;
        private CWMSDataSetTableAdapters.moshtarekinTableAdapter moshtarekinTableAdapter;
        private System.Windows.Forms.DataGridView dgvElhaghi;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardad;
        private System.Windows.Forms.DataGridViewTextBoxColumn co_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn modir_amel;
        private DevComponents.DotNetBar.ButtonX buttonX4;
    }
}