﻿using FarsiLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.forms.esf_bozorg
{
    public partial class frm_pishnevis_factor : MyMetroForm
    {
        string get_dorh_mah_sal(string table_name, string dcode)
        {
            try
            {
                DateTime dt_start = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select start_time from " + table_name + " where dcode=" + dcode));
                string[] t = new PersianDate(dt_start).ToString("M").Split(' ');
                return t[1] + " " + t[0];


            }
            catch (Exception)
            {
                return "";
            }

        }

        string table_type_name = "";
        DataTable dt_rows, dt_bestankar_rows, dt_all;

        public frm_pishnevis_factor(string dcode, string table_type)
        {
            InitializeComponent();
            dt_rows = new DataTable();


            DataColumn t = new DataColumn();
            t.ColumnName = t.Caption = "کدحساب";
            DataColumn t1 = new DataColumn();
            t1.ColumnName = t1.Caption = "شرح";
            DataColumn t2 = new DataColumn();
            t2.ColumnName = t2.Caption = "بدهکار";

            DataColumn t3 = new DataColumn();
            t3.ColumnName = t3.Caption = "بستانکار";
            DataColumn t4 = new DataColumn();
            t4.ColumnName = t4.Caption = "اشخاص و مؤسسات";

            DataColumn t5 = new DataColumn();
            t5.ColumnName = t5.Caption = "نوع بدهی";

            dt_rows.Columns.Add(t);
            dt_rows.Columns.Add(t1);
            dt_rows.Columns.Add(t2);
            dt_rows.Columns.Add(t3);
            dt_rows.Columns.Add(t4);
            dt_rows.Columns.Add(t5);


            dt_bestankar_rows = dt_rows.Clone();




            string doreh_mah_sal = get_dorh_mah_sal(table_type + "_doreh", dcode);
            table_type_name = table_type;

            DataTable dt_ghabz = Classes.ClsMain.GetDataTable("select gharardad,mablaghkol from " + table_type + "_ghabz where dcode=" + dcode);
            DataTable dt_moshtarekin;
            string persian_type = "شارژ", code_hesab = "33/01", noe_bedehi = "0001";
            if (table_type == "ab")
            {
                persian_type = "آب";
                code_hesab = "33/15";
                noe_bedehi = "0002";
            }
            for (int i = 0; i < dt_ghabz.Rows.Count; i++)
            {
                dt_moshtarekin = Classes.ClsMain.GetDataTable("select co_name," + table_type + "_tafsili from moshtarekin where gharardad=" + dt_ghabz.Rows[i]["gharardad"].ToString());

                dt_rows.Rows.Add(
                    "12/01",
                    "بابت حق " + persian_type + " " + doreh_mah_sal,
                    dt_ghabz.Rows[i]["mablaghkol"].ToString(),
                    "",
                     dt_moshtarekin.Rows[0][1].ToString(),
                    noe_bedehi);



                dt_bestankar_rows.Rows.Add(
                    code_hesab,
                     "بابت حق " + persian_type + " " + doreh_mah_sal + " " + dt_moshtarekin.Rows[0]["co_name"].ToString(),
                     "",
                        dt_ghabz.Rows[i]["mablaghkol"].ToString(),
                       dt_moshtarekin.Rows[0][1].ToString(),
                        noe_bedehi

                    );

            }
            dgv_bedehkar.DataSource = dt_rows;
            dgv_bestankar.DataSource = dt_bestankar_rows;

            dt_all = dt_rows.Clone();
            dt_all.Merge(dt_rows);
            dt_all.Merge(dt_bestankar_rows);

        }

        private void frm_pishnevis_factor_Load(object sender, EventArgs e)
        {

        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            if (table_type_name == "sharj")
            {
                Classes.clsExcel.save_Excel_file(dt_all, "پیش نویس شارژ");
                Payam.Show("پیش نویس شارژ با موفقیت ایجاد شد ");
            }
            else
            {
                Classes.clsExcel.save_Excel_file(dt_all, "پیش نویس آب");
                Payam.Show("پیش نویس آب با موفقیت ایجاد شد ");
            }
            }
        }
}
