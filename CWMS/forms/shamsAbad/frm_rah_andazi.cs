﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ClosedXML.Excel;


namespace CWMS.forms.shamsAbad
{
    public partial class frm_rah_andazi : Form
    {
        public frm_rah_andazi()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            //قرائت های قبلی مشترکین.xlsx
            if (op.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = Classes.clsExcel.Read_Excel_file(op.FileName);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Classes.ClsMain.ChangeCulture("e");
                    Classes.ClsMain.ExecuteNoneQuery("delete ab_bedehi_bestankar_First_time;insert into ab_bedehi_bestankar_First_time values"
                        + "(" + dt.Rows[i][0].ToString() + ",0,0," + dt.Rows[i][1].ToString() + ",'" + DateTime.Now + "')");



                }
                Payam.Show("قرائت های قبلی مشترکین با موفقیت در سیستم ذخیره شد .");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            //لیست مشترکین جهت انتقال.xlxs
            decimal metraj = 0, size_ensheab = 0, masrafe_mojaz;
            if (op.ShowDialog() == DialogResult.OK)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete moshtarekin");
                DataTable dt = Classes.clsExcel.Read_Excel_file(op.FileName);
                Payam.Show("تا اتمام درج " + dt.Rows.Count.ToString() + "  مشترک لطفا صبر نمایید ");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Classes.ClsMain.ChangeCulture("e");
                    try
                    {
                        metraj = Convert.ToDecimal(dt.Rows[i][4].ToString());

                    }
                    catch (Exception)
                    {
                        metraj = 0;
                    }
                    try
                    {
                        size_ensheab = Convert.ToDecimal(dt.Rows[i][5].ToString());

                    }
                    catch (Exception)
                    {
                        size_ensheab = 0;
                    }
                    try
                    {
                        masrafe_mojaz = Convert.ToDecimal(dt.Rows[i][6].ToString());

                    }
                    catch (Exception)
                    {
                        masrafe_mojaz = 0;
                    }

                    Classes.ClsMain.ExecuteNoneQuery("insert into moshtarekin (gharardad,co_name,ghataat,address,metraj,size_ensheab,masrafe_mojaz) values "
                        + "(" + dt.Rows[i][0].ToString() + ",'" + dt.Rows[i][1].ToString() + "','" + dt.Rows[i][2].ToString() +
                        "','" + dt.Rows[i][3].ToString() + "'," +
                        metraj.ToString() + "," +
                       size_ensheab.ToString() + "," +
                        masrafe_mojaz.ToString() +

                        ")");



                }
                Payam.Show("اطلاعات مشترکین با موفقیت در سیستم ذخیره شد .");
            }
        }
    }
}
