﻿namespace CWMS
{
    partial class Rpt_Sharj_Ghobooz_PN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_all = new DevComponents.DotNetBar.ButtonX();
            this.btn_find = new DevComponents.DotNetBar.ButtonX();
            this.cmbDoreh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.dgbedehkar = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modiramelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharjdorehPNBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.lblDoreh = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txthoroof = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtsum = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new System.Windows.Forms.Label();
            this.txtcnt = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.sharj_doreh_PNTableAdapter = new CWMS.MainDataSestTableAdapters.Sharj_doreh_PNTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgbedehkar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehPNBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btn_all);
            this.groupPanel1.Controls.Add(this.btn_find);
            this.groupPanel1.Controls.Add(this.cmbDoreh);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.dgbedehkar);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(21, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(925, 433);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "نمایش لیست مشترکینی که قبوض خود را پرداخت نکرده اند";
            // 
            // btn_all
            // 
            this.btn_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_all.Location = new System.Drawing.Point(17, 18);
            this.btn_all.Name = "btn_all";
            this.btn_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_all.Size = new System.Drawing.Size(213, 23);
            this.btn_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_all.Symbol = "";
            this.btn_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_all.SymbolSize = 9F;
            this.btn_all.TabIndex = 28;
            this.btn_all.Text = "نمایش بدهکاران دوره فعلی";
            this.btn_all.Click += new System.EventHandler(this.btn_all_Click);
            // 
            // btn_find
            // 
            this.btn_find.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find.BackColor = System.Drawing.Color.Transparent;
            this.btn_find.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find.Location = new System.Drawing.Point(560, 12);
            this.btn_find.Name = "btn_find";
            this.btn_find.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find.Size = new System.Drawing.Size(152, 23);
            this.btn_find.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find.Symbol = "";
            this.btn_find.SymbolColor = System.Drawing.Color.Green;
            this.btn_find.SymbolSize = 9F;
            this.btn_find.TabIndex = 26;
            this.btn_find.Text = "نمایش مشترکین";
            this.btn_find.Click += new System.EventHandler(this.btn_find_Click);
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.DisplayMember = "Text";
            this.cmbDoreh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.ItemHeight = 18;
            this.cmbDoreh.Location = new System.Drawing.Point(728, 11);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDoreh.Size = new System.Drawing.Size(124, 24);
            this.cmbDoreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDoreh.TabIndex = 25;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(841, 12);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(61, 23);
            this.labelX7.TabIndex = 27;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dgbedehkar
            // 
            this.dgbedehkar.AllowUserToAddRows = false;
            this.dgbedehkar.AllowUserToDeleteRows = false;
            this.dgbedehkar.AllowUserToResizeColumns = false;
            this.dgbedehkar.AllowUserToResizeRows = false;
            this.dgbedehkar.AutoGenerateColumns = false;
            this.dgbedehkar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgbedehkar.BackgroundColor = System.Drawing.Color.White;
            this.dgbedehkar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcode,
            this.dcodeDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.modiramelDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn,
            this.bedehiDataGridViewTextBoxColumn,
            this.mablaghkolDataGridViewTextBoxColumn});
            this.dgbedehkar.DataSource = this.sharjdorehPNBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgbedehkar.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgbedehkar.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgbedehkar.Location = new System.Drawing.Point(3, 61);
            this.dgbedehkar.Name = "dgbedehkar";
            this.dgbedehkar.ReadOnly = true;
            this.dgbedehkar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgbedehkar.Size = new System.Drawing.Size(899, 333);
            this.dgbedehkar.TabIndex = 0;
            this.dgbedehkar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgbedehkar_CellClick);
            // 
            // gcode
            // 
            this.gcode.DataPropertyName = "gcode";
            this.gcode.HeaderText = "کدقبض";
            this.gcode.Name = "gcode";
            this.gcode.ReadOnly = true;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.FillWeight = 200F;
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام مشترک";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modiramelDataGridViewTextBoxColumn
            // 
            this.modiramelDataGridViewTextBoxColumn.DataPropertyName = "modir_amel";
            this.modiramelDataGridViewTextBoxColumn.FillWeight = 150F;
            this.modiramelDataGridViewTextBoxColumn.HeaderText = "مدیرعامل";
            this.modiramelDataGridViewTextBoxColumn.Name = "modiramelDataGridViewTextBoxColumn";
            this.modiramelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "مبلغ";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bedehiDataGridViewTextBoxColumn
            // 
            this.bedehiDataGridViewTextBoxColumn.DataPropertyName = "bedehi";
            this.bedehiDataGridViewTextBoxColumn.HeaderText = "بدهی";
            this.bedehiDataGridViewTextBoxColumn.Name = "bedehiDataGridViewTextBoxColumn";
            this.bedehiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sharjdorehPNBindingSource
            // 
            this.sharjdorehPNBindingSource.DataMember = "Sharj_doreh_PN";
            this.sharjdorehPNBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(841, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(68, 23);
            this.labelX1.TabIndex = 2;
            this.labelX1.Text = "دوره انتخابی :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblDoreh
            // 
            this.lblDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDoreh.Location = new System.Drawing.Point(742, 6);
            this.lblDoreh.Name = "lblDoreh";
            this.lblDoreh.Size = new System.Drawing.Size(110, 23);
            this.lblDoreh.TabIndex = 1;
            this.lblDoreh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txthoroof);
            this.groupPanel2.Controls.Add(this.txtsum);
            this.groupPanel2.Controls.Add(this.label1);
            this.groupPanel2.Controls.Add(this.txtcnt);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.lblDoreh);
            this.groupPanel2.Controls.Add(this.label5);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(21, 443);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(925, 61);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "اطلاعات آماری";
            // 
            // txthoroof
            // 
            this.txthoroof.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txthoroof.Border.Class = "TextBoxBorder";
            this.txthoroof.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txthoroof.DisabledBackColor = System.Drawing.Color.White;
            this.txthoroof.ForeColor = System.Drawing.Color.Black;
            this.txthoroof.Location = new System.Drawing.Point(3, 7);
            this.txthoroof.Name = "txthoroof";
            this.txthoroof.PreventEnterBeep = true;
            this.txthoroof.ReadOnly = true;
            this.txthoroof.Size = new System.Drawing.Size(391, 24);
            this.txthoroof.TabIndex = 14;
            // 
            // txtsum
            // 
            this.txtsum.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtsum.Border.Class = "TextBoxBorder";
            this.txtsum.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtsum.DisabledBackColor = System.Drawing.Color.White;
            this.txtsum.ForeColor = System.Drawing.Color.Black;
            this.txtsum.Location = new System.Drawing.Point(400, 7);
            this.txtsum.Name = "txtsum";
            this.txtsum.PreventEnterBeep = true;
            this.txtsum.ReadOnly = true;
            this.txtsum.Size = new System.Drawing.Size(127, 24);
            this.txtsum.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(533, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "مجموع مبلغ :";
            // 
            // txtcnt
            // 
            this.txtcnt.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtcnt.Border.Class = "TextBoxBorder";
            this.txtcnt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtcnt.DisabledBackColor = System.Drawing.Color.White;
            this.txtcnt.ForeColor = System.Drawing.Color.Black;
            this.txtcnt.Location = new System.Drawing.Point(603, 7);
            this.txtcnt.Name = "txtcnt";
            this.txtcnt.PreventEnterBeep = true;
            this.txtcnt.ReadOnly = true;
            this.txtcnt.Size = new System.Drawing.Size(75, 24);
            this.txtcnt.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(684, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "تعداد کل :";
            // 
            // sharj_doreh_PNTableAdapter
            // 
            this.sharj_doreh_PNTableAdapter.ClearBeforeFill = true;
            // 
            // Rpt_Sharj_Ghobooz_PN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 513);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rpt_Sharj_Ghobooz_PN";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Rpt_Sharj_Ghobooz_PN_Load);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgbedehkar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharjdorehPNBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgbedehkar;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtsum;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtcnt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource sharjdorehPNBindingSource;
        private MainDataSest mainDataSest;
        private MainDataSestTableAdapters.Sharj_doreh_PNTableAdapter sharj_doreh_PNTableAdapter;
        private DevComponents.DotNetBar.LabelX lblDoreh;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btn_find;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDoreh;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txthoroof;
        private DevComponents.DotNetBar.ButtonX btn_all;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modiramelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedehiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;

    }
}