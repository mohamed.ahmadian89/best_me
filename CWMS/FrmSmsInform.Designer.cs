﻿namespace CWMS
{
    partial class FrmSmsInform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grp_send_info = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txttozihat = new System.Windows.Forms.TextBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnSendInPortal = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.txtend = new DevComponents.Editors.IntegerInput();
            this.txtstart = new DevComponents.Editors.IntegerInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgv_doreh = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moshtareknotifylogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest = new CWMS.MainDataSest();
            this.moshtarek_notify_logTableAdapter = new CWMS.MainDataSestTableAdapters.moshtarek_notify_logTableAdapter();
            this.grp_send_info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstart)).BeginInit();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtareknotifylogBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.SuspendLayout();
            // 
            // grp_send_info
            // 
            this.grp_send_info.BackColor = System.Drawing.Color.Transparent;
            this.grp_send_info.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grp_send_info.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_send_info.Controls.Add(this.labelX3);
            this.grp_send_info.Controls.Add(this.txttozihat);
            this.grp_send_info.Controls.Add(this.labelX2);
            this.grp_send_info.Controls.Add(this.btnSendInPortal);
            this.grp_send_info.Controls.Add(this.buttonX6);
            this.grp_send_info.Controls.Add(this.txtend);
            this.grp_send_info.Controls.Add(this.txtstart);
            this.grp_send_info.Controls.Add(this.labelX1);
            this.grp_send_info.Controls.Add(this.labelX14);
            this.grp_send_info.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_send_info.Location = new System.Drawing.Point(9, 14);
            this.grp_send_info.Margin = new System.Windows.Forms.Padding(4);
            this.grp_send_info.Name = "grp_send_info";
            this.grp_send_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grp_send_info.Size = new System.Drawing.Size(1200, 261);
            // 
            // 
            // 
            this.grp_send_info.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_send_info.Style.BackColorGradientAngle = 90;
            this.grp_send_info.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_send_info.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_send_info.Style.BorderBottomWidth = 1;
            this.grp_send_info.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_send_info.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_send_info.Style.BorderLeftWidth = 1;
            this.grp_send_info.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_send_info.Style.BorderRightWidth = 1;
            this.grp_send_info.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_send_info.Style.BorderTopWidth = 1;
            this.grp_send_info.Style.CornerDiameter = 4;
            this.grp_send_info.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_send_info.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_send_info.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_send_info.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_send_info.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_send_info.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_send_info.TabIndex = 25;
            this.grp_send_info.Text = "سامانه اطلاع رسانی مشترکین";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(134, 4);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(912, 27);
            this.labelX3.TabIndex = 85;
            this.labelX3.Text = "لطفا شماره قرارداد هایی که قرار است اطلاعیه ذیل برای انها ارسال شود را از کادر ذی" +
    "ل انتخاب نمایید";
            // 
            // txttozihat
            // 
            this.txttozihat.Location = new System.Drawing.Point(96, 90);
            this.txttozihat.Margin = new System.Windows.Forms.Padding(4);
            this.txttozihat.Multiline = true;
            this.txttozihat.Name = "txttozihat";
            this.txttozihat.Size = new System.Drawing.Size(948, 86);
            this.txttozihat.TabIndex = 84;
            this.txttozihat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttozihat_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(1026, 91);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(139, 27);
            this.labelX2.TabIndex = 83;
            this.labelX2.Text = "متن اطلاعیه :";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnSendInPortal
            // 
            this.btnSendInPortal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSendInPortal.BackColor = System.Drawing.Color.Transparent;
            this.btnSendInPortal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSendInPortal.Location = new System.Drawing.Point(14, 184);
            this.btnSendInPortal.Margin = new System.Windows.Forms.Padding(4);
            this.btnSendInPortal.Name = "btnSendInPortal";
            this.btnSendInPortal.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.btnSendInPortal.Size = new System.Drawing.Size(243, 27);
            this.btnSendInPortal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSendInPortal.Symbol = "";
            this.btnSendInPortal.SymbolColor = System.Drawing.Color.Teal;
            this.btnSendInPortal.SymbolSize = 12F;
            this.btnSendInPortal.TabIndex = 81;
            this.btnSendInPortal.Text = "ثبت در پورتال اینترنتی مشترکین";
            this.btnSendInPortal.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(934, 184);
            this.buttonX6.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX6.Size = new System.Drawing.Size(243, 27);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX6.SymbolSize = 12F;
            this.buttonX6.TabIndex = 80;
            this.buttonX6.Text = "ارسال پیامک اطلاع رسانی";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // txtend
            // 
            this.txtend.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtend.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtend.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtend.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtend.ForeColor = System.Drawing.Color.Black;
            this.txtend.Location = new System.Drawing.Point(563, 54);
            this.txtend.Margin = new System.Windows.Forms.Padding(4);
            this.txtend.Name = "txtend";
            this.txtend.ShowUpDown = true;
            this.txtend.Size = new System.Drawing.Size(95, 28);
            this.txtend.TabIndex = 67;
            this.txtend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtend_KeyDown);
            // 
            // txtstart
            // 
            this.txtstart.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtstart.BackgroundStyle.Class = "DateTimeInputBackground";
            this.txtstart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtstart.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.txtstart.ForeColor = System.Drawing.Color.Black;
            this.txtstart.Location = new System.Drawing.Point(804, 54);
            this.txtstart.Margin = new System.Windows.Forms.Padding(4);
            this.txtstart.Name = "txtstart";
            this.txtstart.ShowUpDown = true;
            this.txtstart.Size = new System.Drawing.Size(95, 28);
            this.txtstart.TabIndex = 66;
            this.txtstart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtstart_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(640, 55);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(156, 27);
            this.labelX1.TabIndex = 65;
            this.labelX1.Text = "تا شماره قرارداد :";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(907, 55);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4);
            this.labelX14.Name = "labelX14";
            this.labelX14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX14.Size = new System.Drawing.Size(139, 27);
            this.labelX14.TabIndex = 64;
            this.labelX14.Text = "از شماره قرارداد:";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.dgv_doreh);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(9, 296);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1200, 378);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 26;
            this.groupPanel1.Text = "آخرین اطلاع رسانی های انجام شده";
            // 
            // dgv_doreh
            // 
            this.dgv_doreh.AllowUserToAddRows = false;
            this.dgv_doreh.AllowUserToDeleteRows = false;
            this.dgv_doreh.AllowUserToResizeColumns = false;
            this.dgv_doreh.AllowUserToResizeRows = false;
            this.dgv_doreh.AutoGenerateColumns = false;
            this.dgv_doreh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_doreh.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_doreh.ColumnHeadersHeight = 25;
            this.dgv_doreh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tarikhDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.matnDataGridViewTextBoxColumn});
            this.dgv_doreh.DataSource = this.moshtareknotifylogBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_doreh.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_doreh.EnableHeadersVisualStyles = false;
            this.dgv_doreh.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_doreh.Location = new System.Drawing.Point(27, 29);
            this.dgv_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_doreh.Name = "dgv_doreh";
            this.dgv_doreh.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_doreh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_doreh.Size = new System.Drawing.Size(1100, 258);
            this.dgv_doreh.TabIndex = 13;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.FillWeight = 60F;
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 30F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // matnDataGridViewTextBoxColumn
            // 
            this.matnDataGridViewTextBoxColumn.DataPropertyName = "matn";
            this.matnDataGridViewTextBoxColumn.FillWeight = 180F;
            this.matnDataGridViewTextBoxColumn.HeaderText = "توضیح";
            this.matnDataGridViewTextBoxColumn.Name = "matnDataGridViewTextBoxColumn";
            this.matnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // moshtareknotifylogBindingSource
            // 
            this.moshtareknotifylogBindingSource.DataMember = "moshtarek_notify_log";
            this.moshtareknotifylogBindingSource.DataSource = this.mainDataSest;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // moshtarek_notify_logTableAdapter
            // 
            this.moshtarek_notify_logTableAdapter.ClearBeforeFill = true;
            // 
            // FrmSmsInform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 688);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.grp_send_info);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSmsInform";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmSmsInform_Load);
            this.grp_send_info.ResumeLayout(false);
            this.grp_send_info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstart)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtareknotifylogBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grp_send_info;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.IntegerInput txtend;
        private DevComponents.Editors.IntegerInput txtstart;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX btnSendInPortal;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_doreh;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource moshtareknotifylogBindingSource;
        private MainDataSestTableAdapters.moshtarek_notify_logTableAdapter moshtarek_notify_logTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TextBox txttozihat;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn matnDataGridViewTextBoxColumn;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}