﻿namespace CWMS
{
    partial class frmMain_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewX1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modiramelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moshtarekinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cWMSDataSet = new CWMS.CWMSDataSet();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.moshtarekinTableAdapter = new CWMS.CWMSDataSetTableAdapters.moshtarekinTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewX1
            // 
            this.dataGridViewX1.AllowUserToDeleteRows = false;
            this.dataGridViewX1.AllowUserToResizeColumns = false;
            this.dataGridViewX1.AllowUserToResizeRows = false;
            this.dataGridViewX1.AutoGenerateColumns = false;
            this.dataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewX1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewX1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.conameDataGridViewTextBoxColumn,
            this.modiramelDataGridViewTextBoxColumn});
            this.dataGridViewX1.DataSource = this.moshtarekinBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewX1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewX1.EnableHeadersVisualStyles = false;
            this.dataGridViewX1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dataGridViewX1.Location = new System.Drawing.Point(26, 45);
            this.dataGridViewX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridViewX1.Name = "dataGridViewX1";
            this.dataGridViewX1.ReadOnly = true;
            this.dataGridViewX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewX1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewX1.Size = new System.Drawing.Size(557, 235);
            this.dataGridViewX1.TabIndex = 0;
            this.dataGridViewX1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellClick);
            this.dataGridViewX1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewX1_CellContentClick);
            this.dataGridViewX1.Click += new System.EventHandler(this.dataGridViewX1_Click);
            this.dataGridViewX1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridViewX1_KeyDown);
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 36.3592F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // conameDataGridViewTextBoxColumn
            // 
            this.conameDataGridViewTextBoxColumn.DataPropertyName = "co_name";
            this.conameDataGridViewTextBoxColumn.FillWeight = 141.8134F;
            this.conameDataGridViewTextBoxColumn.HeaderText = "نام مشترک";
            this.conameDataGridViewTextBoxColumn.Name = "conameDataGridViewTextBoxColumn";
            this.conameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modiramelDataGridViewTextBoxColumn
            // 
            this.modiramelDataGridViewTextBoxColumn.DataPropertyName = "modir_amel";
            this.modiramelDataGridViewTextBoxColumn.FillWeight = 121.8274F;
            this.modiramelDataGridViewTextBoxColumn.HeaderText = "مدیریت عامل";
            this.modiramelDataGridViewTextBoxColumn.Name = "modiramelDataGridViewTextBoxColumn";
            this.modiramelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // moshtarekinBindingSource
            // 
            this.moshtarekinBindingSource.DataMember = "moshtarekin";
            this.moshtarekinBindingSource.DataSource = this.cWMSDataSet;
            // 
            // cWMSDataSet
            // 
            this.cWMSDataSet.DataSetName = "CWMSDataSet";
            this.cWMSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(107, 5);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(365, 30);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "لطفا جهت انتخاب مشترک، بر روی ان کلیک نمایید";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // moshtarekinTableAdapter
            // 
            this.moshtarekinTableAdapter.ClearBeforeFill = true;
            // 
            // frmMain_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 295);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.dataGridViewX1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain_search";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مشترکین";
            this.Load += new System.EventHandler(this.frmMain_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cWMSDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridViewX1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private CWMSDataSet cWMSDataSet;
        private System.Windows.Forms.BindingSource moshtarekinBindingSource;
        private CWMSDataSetTableAdapters.moshtarekinTableAdapter moshtarekinTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modiramelDataGridViewTextBoxColumn;
    }
}