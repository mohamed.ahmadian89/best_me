﻿namespace CWMS
{
    partial class FrmJarimehAfterGhabzDeadLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_ab_mohlat_pardaht = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txt_ab_Doreh = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_sharj_mohlat_pardakht = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txt_sharj_doreh = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_sharj_cnt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgv_sharj = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txt_ab_cnt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgv_ab = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn_ab_jarimeh = new DevComponents.DotNetBar.ButtonX();
            this.txt_ab_mablagh_jarimhe = new System.Windows.Forms.TextBox();
            this.lbl_ab_jarimeh_type = new System.Windows.Forms.Label();
            this.rd_ab_darsad = new System.Windows.Forms.RadioButton();
            this.rd_ab_sabet = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btn_sharj_jarimeh = new DevComponents.DotNetBar.ButtonX();
            this.txt_sharj_jarimeh = new System.Windows.Forms.TextBox();
            this.lbl_sharj_jarimeh_type = new System.Windows.Forms.Label();
            this.rd_sharj_darasad = new System.Windows.Forms.RadioButton();
            this.rd_sharj_sabet = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txt_ab_mohlat_pardaht);
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Controls.Add(this.txt_ab_Doreh);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(502, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(490, 56);
            this.groupBox1.TabIndex = 158;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "امور آب ";
            // 
            // txt_ab_mohlat_pardaht
            // 
            this.txt_ab_mohlat_pardaht.Location = new System.Drawing.Point(19, 18);
            this.txt_ab_mohlat_pardaht.Name = "txt_ab_mohlat_pardaht";
            this.txt_ab_mohlat_pardaht.Readonly = true;
            this.txt_ab_mohlat_pardaht.Size = new System.Drawing.Size(171, 24);
            this.txt_ab_mohlat_pardaht.TabIndex = 161;
            this.txt_ab_mohlat_pardaht.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(445, 18);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(35, 27);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Orange;
            this.labelX1.TabIndex = 157;
            // 
            // txt_ab_Doreh
            // 
            this.txt_ab_Doreh.BackColor = System.Drawing.Color.White;
            this.txt_ab_Doreh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_Doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_Doreh.Location = new System.Drawing.Point(299, 18);
            this.txt_ab_Doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_ab_Doreh.Name = "txt_ab_Doreh";
            this.txt_ab_Doreh.Size = new System.Drawing.Size(66, 28);
            this.txt_ab_Doreh.TabIndex = 156;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(373, 25);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 17);
            this.label22.TabIndex = 141;
            this.label22.Text = "دوره جاری آب :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(197, 22);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 17);
            this.label23.TabIndex = 143;
            this.label23.Text = "پایان مهلت پرداخت :";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.txt_sharj_mohlat_pardakht);
            this.groupBox2.Controls.Add(this.labelX2);
            this.groupBox2.Controls.Add(this.txt_sharj_doreh);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(1, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(490, 56);
            this.groupBox2.TabIndex = 159;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "امور شارژ";
            // 
            // txt_sharj_mohlat_pardakht
            // 
            this.txt_sharj_mohlat_pardakht.Location = new System.Drawing.Point(19, 18);
            this.txt_sharj_mohlat_pardakht.Name = "txt_sharj_mohlat_pardakht";
            this.txt_sharj_mohlat_pardakht.Readonly = true;
            this.txt_sharj_mohlat_pardakht.Size = new System.Drawing.Size(171, 24);
            this.txt_sharj_mohlat_pardakht.TabIndex = 161;
            this.txt_sharj_mohlat_pardakht.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(455, 18);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(35, 27);
            this.labelX2.Symbol = "";
            this.labelX2.SymbolColor = System.Drawing.Color.Orange;
            this.labelX2.TabIndex = 157;
            // 
            // txt_sharj_doreh
            // 
            this.txt_sharj_doreh.BackColor = System.Drawing.Color.White;
            this.txt_sharj_doreh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_doreh.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_doreh.Location = new System.Drawing.Point(299, 18);
            this.txt_sharj_doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_sharj_doreh.Name = "txt_sharj_doreh";
            this.txt_sharj_doreh.Size = new System.Drawing.Size(66, 28);
            this.txt_sharj_doreh.TabIndex = 156;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(373, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 141;
            this.label1.Text = "دوره جاری شارژ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(197, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 143;
            this.label2.Text = "پایان مهلت پرداخت :";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.txt_sharj_cnt);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.dgv_sharj);
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(1, 74);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox3.Size = new System.Drawing.Size(490, 357);
            this.groupBox3.TabIndex = 160;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "مشترکینی که قبوض شارژ خود را در تاریخ معین پرداخت نکرده اند ";
            // 
            // txt_sharj_cnt
            // 
            this.txt_sharj_cnt.BackColor = System.Drawing.Color.White;
            this.txt_sharj_cnt.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_cnt.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_cnt.Location = new System.Drawing.Point(246, 325);
            this.txt_sharj_cnt.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_sharj_cnt.Name = "txt_sharj_cnt";
            this.txt_sharj_cnt.Size = new System.Drawing.Size(140, 28);
            this.txt_sharj_cnt.TabIndex = 165;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(394, 332);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 17);
            this.label5.TabIndex = 164;
            this.label5.Text = "تعداد مشترکین :";
            // 
            // dgv_sharj
            // 
            this.dgv_sharj.AllowDrop = true;
            this.dgv_sharj.AllowUserToAddRows = false;
            this.dgv_sharj.AllowUserToDeleteRows = false;
            this.dgv_sharj.AllowUserToResizeColumns = false;
            this.dgv_sharj.AllowUserToResizeRows = false;
            this.dgv_sharj.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sharj.BackgroundColor = System.Drawing.Color.White;
            this.dgv_sharj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_sharj.ColumnHeadersHeight = 30;
            this.dgv_sharj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Column4});
            this.dgv_sharj.Location = new System.Drawing.Point(19, 30);
            this.dgv_sharj.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_sharj.Name = "dgv_sharj";
            this.dgv_sharj.RowHeadersVisible = false;
            this.dgv_sharj.RowTemplate.Height = 24;
            this.dgv_sharj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_sharj.Size = new System.Drawing.Size(457, 287);
            this.dgv_sharj.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.HeaderText = "قرارداد";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle1.Format = "#,#";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn2.HeaderText = "مبلغ کل قبض دوره جاری";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // Column4
            // 
            dataGridViewCellStyle2.Format = "#,#";
            this.Column4.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column4.HeaderText = "جریمه";
            this.Column4.Name = "Column4";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.txt_ab_cnt);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.dgv_ab);
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(502, 74);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox4.Size = new System.Drawing.Size(490, 357);
            this.groupBox4.TabIndex = 161;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "مشترکینی که قبوض  آب خود را در تاریخ معین پرداخت نکرده اند ";
            // 
            // txt_ab_cnt
            // 
            this.txt_ab_cnt.BackColor = System.Drawing.Color.White;
            this.txt_ab_cnt.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_cnt.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_cnt.Location = new System.Drawing.Point(244, 323);
            this.txt_ab_cnt.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_ab_cnt.Name = "txt_ab_cnt";
            this.txt_ab_cnt.Size = new System.Drawing.Size(140, 28);
            this.txt_ab_cnt.TabIndex = 163;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(392, 330);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 162;
            this.label4.Text = "تعداد مشترکین :";
            // 
            // dgv_ab
            // 
            this.dgv_ab.AllowDrop = true;
            this.dgv_ab.AllowUserToAddRows = false;
            this.dgv_ab.AllowUserToDeleteRows = false;
            this.dgv_ab.AllowUserToResizeColumns = false;
            this.dgv_ab.AllowUserToResizeRows = false;
            this.dgv_ab.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ab.BackgroundColor = System.Drawing.Color.White;
            this.dgv_ab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_ab.ColumnHeadersHeight = 30;
            this.dgv_ab.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgv_ab.Location = new System.Drawing.Point(19, 31);
            this.dgv_ab.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_ab.Name = "dgv_ab";
            this.dgv_ab.RowHeadersVisible = false;
            this.dgv_ab.RowTemplate.Height = 24;
            this.dgv_ab.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ab.Size = new System.Drawing.Size(457, 286);
            this.dgv_ab.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.FillWeight = 50F;
            this.Column1.HeaderText = "قرارداد";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            dataGridViewCellStyle3.Format = "#,#";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.HeaderText = "مبلغ کل قبض دوره جاری";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            dataGridViewCellStyle4.Format = "#,#";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "جریمه";
            this.Column3.Name = "Column3";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.btn_ab_jarimeh);
            this.groupBox5.Controls.Add(this.txt_ab_mablagh_jarimhe);
            this.groupBox5.Controls.Add(this.lbl_ab_jarimeh_type);
            this.groupBox5.Controls.Add(this.rd_ab_darsad);
            this.groupBox5.Controls.Add(this.rd_ab_sabet);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(499, 437);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox5.Size = new System.Drawing.Size(490, 124);
            this.groupBox5.TabIndex = 162;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "اعمال جریمه آب";
            // 
            // btn_ab_jarimeh
            // 
            this.btn_ab_jarimeh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_ab_jarimeh.BackColor = System.Drawing.Color.Transparent;
            this.btn_ab_jarimeh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_ab_jarimeh.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_ab_jarimeh.Location = new System.Drawing.Point(13, 88);
            this.btn_ab_jarimeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_ab_jarimeh.Name = "btn_ab_jarimeh";
            this.btn_ab_jarimeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_ab_jarimeh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_ab_jarimeh.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_ab_jarimeh.Size = new System.Drawing.Size(189, 27);
            this.btn_ab_jarimeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_ab_jarimeh.Symbol = "";
            this.btn_ab_jarimeh.SymbolColor = System.Drawing.Color.Green;
            this.btn_ab_jarimeh.SymbolSize = 9F;
            this.btn_ab_jarimeh.TabIndex = 25;
            this.btn_ab_jarimeh.Text = "اعمال جریمه بر روی قبوض";
            this.btn_ab_jarimeh.Click += new System.EventHandler(this.btn_ab_jarimeh_Click);
            // 
            // txt_ab_mablagh_jarimhe
            // 
            this.txt_ab_mablagh_jarimhe.BackColor = System.Drawing.Color.White;
            this.txt_ab_mablagh_jarimhe.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ab_mablagh_jarimhe.ForeColor = System.Drawing.Color.Black;
            this.txt_ab_mablagh_jarimhe.Location = new System.Drawing.Point(33, 50);
            this.txt_ab_mablagh_jarimhe.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_ab_mablagh_jarimhe.Name = "txt_ab_mablagh_jarimhe";
            this.txt_ab_mablagh_jarimhe.Size = new System.Drawing.Size(138, 28);
            this.txt_ab_mablagh_jarimhe.TabIndex = 163;
            // 
            // lbl_ab_jarimeh_type
            // 
            this.lbl_ab_jarimeh_type.AutoSize = true;
            this.lbl_ab_jarimeh_type.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ab_jarimeh_type.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.lbl_ab_jarimeh_type.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_jarimeh_type.Location = new System.Drawing.Point(179, 57);
            this.lbl_ab_jarimeh_type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ab_jarimeh_type.Name = "lbl_ab_jarimeh_type";
            this.lbl_ab_jarimeh_type.Size = new System.Drawing.Size(61, 17);
            this.lbl_ab_jarimeh_type.TabIndex = 162;
            this.lbl_ab_jarimeh_type.Text = "مبلغ جریمه :";
            // 
            // rd_ab_darsad
            // 
            this.rd_ab_darsad.AutoSize = true;
            this.rd_ab_darsad.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_darsad.Location = new System.Drawing.Point(279, 71);
            this.rd_ab_darsad.Name = "rd_ab_darsad";
            this.rd_ab_darsad.Size = new System.Drawing.Size(189, 25);
            this.rd_ab_darsad.TabIndex = 164;
            this.rd_ab_darsad.Text = "درصدی از مبلغ کل هر مشترک ";
            this.rd_ab_darsad.UseVisualStyleBackColor = true;
            // 
            // rd_ab_sabet
            // 
            this.rd_ab_sabet.AutoSize = true;
            this.rd_ab_sabet.Checked = true;
            this.rd_ab_sabet.ForeColor = System.Drawing.Color.Black;
            this.rd_ab_sabet.Location = new System.Drawing.Point(353, 48);
            this.rd_ab_sabet.Name = "rd_ab_sabet";
            this.rd_ab_sabet.Size = new System.Drawing.Size(115, 25);
            this.rd_ab_sabet.TabIndex = 163;
            this.rd_ab_sabet.TabStop = true;
            this.rd_ab_sabet.Text = "جریمه مبلغ ثابت ";
            this.rd_ab_sabet.UseVisualStyleBackColor = true;
            this.rd_ab_sabet.CheckedChanged += new System.EventHandler(this.rd_ab_sabet_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(174, 27);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 17);
            this.label3.TabIndex = 142;
            this.label3.Text = "جهت اعمال جریمه برای مشترکینی که قبوض خود را پرداخت نکرده اند ";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.btn_sharj_jarimeh);
            this.groupBox6.Controls.Add(this.txt_sharj_jarimeh);
            this.groupBox6.Controls.Add(this.lbl_sharj_jarimeh_type);
            this.groupBox6.Controls.Add(this.rd_sharj_darasad);
            this.groupBox6.Controls.Add(this.rd_sharj_sabet);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.ForeColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(1, 437);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox6.Size = new System.Drawing.Size(490, 124);
            this.groupBox6.TabIndex = 163;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "اعمال جریمه شارژ";
            // 
            // btn_sharj_jarimeh
            // 
            this.btn_sharj_jarimeh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_sharj_jarimeh.BackColor = System.Drawing.Color.Transparent;
            this.btn_sharj_jarimeh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_sharj_jarimeh.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_sharj_jarimeh.Location = new System.Drawing.Point(13, 88);
            this.btn_sharj_jarimeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_sharj_jarimeh.Name = "btn_sharj_jarimeh";
            this.btn_sharj_jarimeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_sharj_jarimeh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_sharj_jarimeh.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_sharj_jarimeh.Size = new System.Drawing.Size(189, 27);
            this.btn_sharj_jarimeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_sharj_jarimeh.Symbol = "";
            this.btn_sharj_jarimeh.SymbolColor = System.Drawing.Color.Green;
            this.btn_sharj_jarimeh.SymbolSize = 9F;
            this.btn_sharj_jarimeh.TabIndex = 25;
            this.btn_sharj_jarimeh.Text = "اعمال جریمه بر روی قبوض";
            this.btn_sharj_jarimeh.Click += new System.EventHandler(this.btn_sharj_jarimeh_Click);
            // 
            // txt_sharj_jarimeh
            // 
            this.txt_sharj_jarimeh.BackColor = System.Drawing.Color.White;
            this.txt_sharj_jarimeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sharj_jarimeh.ForeColor = System.Drawing.Color.Black;
            this.txt_sharj_jarimeh.Location = new System.Drawing.Point(33, 50);
            this.txt_sharj_jarimeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_sharj_jarimeh.Name = "txt_sharj_jarimeh";
            this.txt_sharj_jarimeh.Size = new System.Drawing.Size(138, 28);
            this.txt_sharj_jarimeh.TabIndex = 163;
            // 
            // lbl_sharj_jarimeh_type
            // 
            this.lbl_sharj_jarimeh_type.AutoSize = true;
            this.lbl_sharj_jarimeh_type.BackColor = System.Drawing.Color.Transparent;
            this.lbl_sharj_jarimeh_type.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.lbl_sharj_jarimeh_type.ForeColor = System.Drawing.Color.Black;
            this.lbl_sharj_jarimeh_type.Location = new System.Drawing.Point(179, 57);
            this.lbl_sharj_jarimeh_type.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_sharj_jarimeh_type.Name = "lbl_sharj_jarimeh_type";
            this.lbl_sharj_jarimeh_type.Size = new System.Drawing.Size(61, 17);
            this.lbl_sharj_jarimeh_type.TabIndex = 162;
            this.lbl_sharj_jarimeh_type.Text = "مبلغ جریمه :";
            // 
            // rd_sharj_darasad
            // 
            this.rd_sharj_darasad.AutoSize = true;
            this.rd_sharj_darasad.ForeColor = System.Drawing.Color.Black;
            this.rd_sharj_darasad.Location = new System.Drawing.Point(279, 71);
            this.rd_sharj_darasad.Name = "rd_sharj_darasad";
            this.rd_sharj_darasad.Size = new System.Drawing.Size(189, 25);
            this.rd_sharj_darasad.TabIndex = 164;
            this.rd_sharj_darasad.Text = "درصدی از مبلغ کل هر مشترک ";
            this.rd_sharj_darasad.UseVisualStyleBackColor = true;
            // 
            // rd_sharj_sabet
            // 
            this.rd_sharj_sabet.AutoSize = true;
            this.rd_sharj_sabet.Checked = true;
            this.rd_sharj_sabet.ForeColor = System.Drawing.Color.Black;
            this.rd_sharj_sabet.Location = new System.Drawing.Point(353, 48);
            this.rd_sharj_sabet.Name = "rd_sharj_sabet";
            this.rd_sharj_sabet.Size = new System.Drawing.Size(115, 25);
            this.rd_sharj_sabet.TabIndex = 163;
            this.rd_sharj_sabet.TabStop = true;
            this.rd_sharj_sabet.Text = "جریمه مبلغ ثابت ";
            this.rd_sharj_sabet.UseVisualStyleBackColor = true;
            this.rd_sharj_sabet.CheckedChanged += new System.EventHandler(this.rd_sharj_sabet_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(174, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(299, 17);
            this.label7.TabIndex = 142;
            this.label7.Text = "جهت اعمال جریمه برای مشترکینی که قبوض خود را پرداخت نکرده اند ";
            // 
            // FrmJarimehAfterGhabzDeadLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 562);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmJarimehAfterGhabzDeadLine";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmJarimehAfterGhabzDeadLine_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ab)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.TextBox txt_ab_Doreh;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private FarsiLibrary.Win.Controls.FADatePicker txt_ab_mohlat_pardaht;
        private System.Windows.Forms.GroupBox groupBox2;
        private FarsiLibrary.Win.Controls.FADatePicker txt_sharj_mohlat_pardakht;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.TextBox txt_sharj_doreh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_ab;
        private System.Windows.Forms.DataGridView dgv_sharj;
        private System.Windows.Forms.TextBox txt_ab_mablagh_jarimhe;
        private System.Windows.Forms.Label lbl_ab_jarimeh_type;
        private System.Windows.Forms.RadioButton rd_ab_darsad;
        private System.Windows.Forms.RadioButton rd_ab_sabet;
        private DevComponents.DotNetBar.ButtonX btn_ab_jarimeh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TextBox txt_sharj_cnt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_ab_cnt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox6;
        private DevComponents.DotNetBar.ButtonX btn_sharj_jarimeh;
        private System.Windows.Forms.TextBox txt_sharj_jarimeh;
        private System.Windows.Forms.Label lbl_sharj_jarimeh_type;
        private System.Windows.Forms.RadioButton rd_sharj_darasad;
        private System.Windows.Forms.RadioButton rd_sharj_sabet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}