﻿namespace CWMS.Ab
{
    partial class FrmDeleteOneGhabz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnShowGhobooz = new DevComponents.DotNetBar.ButtonX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.lbl_mohlatPardakht = new DevComponents.DotNetBar.LabelX();
            this.lbl_ab_mablagh = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.lblSelectedDoreh = new DevComponents.DotNetBar.LabelX();
            this.grpInfo = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.BtnDeleteOne = new DevComponents.DotNetBar.ButtonX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lblMablagh = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.lblGcode = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3.SuspendLayout();
            this.grpInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.txtGharardad);
            this.groupPanel3.Controls.Add(this.btnShowGhobooz);
            this.groupPanel3.Controls.Add(this.labelX27);
            this.groupPanel3.Controls.Add(this.labelX28);
            this.groupPanel3.Controls.Add(this.lbl_mohlatPardakht);
            this.groupPanel3.Controls.Add(this.lbl_ab_mablagh);
            this.groupPanel3.Controls.Add(this.labelX24);
            this.groupPanel3.Controls.Add(this.labelX23);
            this.groupPanel3.Controls.Add(this.lblSelectedDoreh);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(12, 12);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(497, 88);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 15;
            this.groupPanel3.Text = "حذف تک قبض";
            // 
            // txtGharardad
            // 
            this.txtGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGharardad.Border.Class = "TextBoxBorder";
            this.txtGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtGharardad.Location = new System.Drawing.Point(138, 31);
            this.txtGharardad.Name = "txtGharardad";
            this.txtGharardad.PreventEnterBeep = true;
            this.txtGharardad.Size = new System.Drawing.Size(78, 29);
            this.txtGharardad.TabIndex = 63;
            this.txtGharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGharardad_KeyDown);
            // 
            // btnShowGhobooz
            // 
            this.btnShowGhobooz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowGhobooz.BackColor = System.Drawing.Color.Transparent;
            this.btnShowGhobooz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowGhobooz.Location = new System.Drawing.Point(3, 32);
            this.btnShowGhobooz.Name = "btnShowGhobooz";
            this.btnShowGhobooz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowGhobooz.Size = new System.Drawing.Size(116, 23);
            this.btnShowGhobooz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowGhobooz.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowGhobooz.SymbolSize = 12F;
            this.btnShowGhobooz.TabIndex = 62;
            this.btnShowGhobooz.Text = "نمایش قبض";
            this.btnShowGhobooz.Click += new System.EventHandler(this.btnShowGhobooz_Click);
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(455, 32);
            this.labelX27.Name = "labelX27";
            this.labelX27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX27.Size = new System.Drawing.Size(24, 23);
            this.labelX27.Symbol = "";
            this.labelX27.SymbolColor = System.Drawing.Color.Teal;
            this.labelX27.SymbolSize = 15F;
            this.labelX27.TabIndex = 57;
            this.labelX27.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(223, 32);
            this.labelX28.Name = "labelX28";
            this.labelX28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX28.Size = new System.Drawing.Size(219, 23);
            this.labelX28.TabIndex = 56;
            this.labelX28.Text = "لطفا شماره قرارداد مشترک مورد نظر را وارد نمایید:";
            this.labelX28.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_mohlatPardakht
            // 
            this.lbl_mohlatPardakht.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_mohlatPardakht.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_mohlatPardakht.ForeColor = System.Drawing.Color.Black;
            this.lbl_mohlatPardakht.Location = new System.Drawing.Point(3, 58);
            this.lbl_mohlatPardakht.Name = "lbl_mohlatPardakht";
            this.lbl_mohlatPardakht.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_mohlatPardakht.Size = new System.Drawing.Size(110, 23);
            this.lbl_mohlatPardakht.TabIndex = 55;
            this.lbl_mohlatPardakht.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_ab_mablagh
            // 
            this.lbl_ab_mablagh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_ab_mablagh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_mablagh.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_mablagh.Location = new System.Drawing.Point(32, 29);
            this.lbl_ab_mablagh.Name = "lbl_ab_mablagh";
            this.lbl_ab_mablagh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_ab_mablagh.Size = new System.Drawing.Size(81, 23);
            this.lbl_ab_mablagh.TabIndex = 52;
            this.lbl_ab_mablagh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(455, 3);
            this.labelX24.Name = "labelX24";
            this.labelX24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX24.Size = new System.Drawing.Size(24, 23);
            this.labelX24.Symbol = "";
            this.labelX24.SymbolColor = System.Drawing.Color.Teal;
            this.labelX24.SymbolSize = 15F;
            this.labelX24.TabIndex = 51;
            this.labelX24.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(385, 3);
            this.labelX23.Name = "labelX23";
            this.labelX23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX23.Size = new System.Drawing.Size(81, 23);
            this.labelX23.TabIndex = 50;
            this.labelX23.Text = "دوره :";
            this.labelX23.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblSelectedDoreh
            // 
            this.lblSelectedDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSelectedDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSelectedDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblSelectedDoreh.Location = new System.Drawing.Point(306, 3);
            this.lblSelectedDoreh.Name = "lblSelectedDoreh";
            this.lblSelectedDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSelectedDoreh.Size = new System.Drawing.Size(81, 23);
            this.lblSelectedDoreh.TabIndex = 49;
            this.lblSelectedDoreh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // grpInfo
            // 
            this.grpInfo.BackColor = System.Drawing.Color.White;
            this.grpInfo.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpInfo.Controls.Add(this.BtnDeleteOne);
            this.grpInfo.Controls.Add(this.labelX1);
            this.grpInfo.Controls.Add(this.labelX2);
            this.grpInfo.Controls.Add(this.lblMablagh);
            this.grpInfo.Controls.Add(this.labelX3);
            this.grpInfo.Controls.Add(this.labelX4);
            this.grpInfo.Controls.Add(this.labelX5);
            this.grpInfo.Controls.Add(this.labelX6);
            this.grpInfo.Controls.Add(this.lblGcode);
            this.grpInfo.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpInfo.Location = new System.Drawing.Point(15, 108);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpInfo.Size = new System.Drawing.Size(497, 88);
            // 
            // 
            // 
            this.grpInfo.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpInfo.Style.BackColorGradientAngle = 90;
            this.grpInfo.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpInfo.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpInfo.Style.BorderBottomWidth = 1;
            this.grpInfo.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpInfo.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpInfo.Style.BorderLeftWidth = 1;
            this.grpInfo.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpInfo.Style.BorderRightWidth = 1;
            this.grpInfo.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpInfo.Style.BorderTopWidth = 1;
            this.grpInfo.Style.CornerDiameter = 4;
            this.grpInfo.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpInfo.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpInfo.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpInfo.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpInfo.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpInfo.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpInfo.TabIndex = 16;
            this.grpInfo.Text = "اطلاعات قبض";
            this.grpInfo.Visible = false;
            // 
            // BtnDeleteOne
            // 
            this.BtnDeleteOne.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnDeleteOne.BackColor = System.Drawing.Color.Transparent;
            this.BtnDeleteOne.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnDeleteOne.Location = new System.Drawing.Point(119, 32);
            this.BtnDeleteOne.Name = "BtnDeleteOne";
            this.BtnDeleteOne.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.BtnDeleteOne.Size = new System.Drawing.Size(213, 23);
            this.BtnDeleteOne.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnDeleteOne.Symbol = "";
            this.BtnDeleteOne.SymbolColor = System.Drawing.Color.DarkRed;
            this.BtnDeleteOne.SymbolSize = 12F;
            this.BtnDeleteOne.TabIndex = 66;
            this.BtnDeleteOne.Text = "حذف قبض مشترک";
            this.BtnDeleteOne.Click += new System.EventHandler(this.BtnDeleteOne_Click);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(238, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(24, 23);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Teal;
            this.labelX1.SymbolSize = 15F;
            this.labelX1.TabIndex = 65;
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(168, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(81, 23);
            this.labelX2.TabIndex = 64;
            this.labelX2.Text = "مبلغ:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblMablagh
            // 
            this.lblMablagh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMablagh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMablagh.ForeColor = System.Drawing.Color.Black;
            this.lblMablagh.Location = new System.Drawing.Point(32, 3);
            this.lblMablagh.Name = "lblMablagh";
            this.lblMablagh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblMablagh.Size = new System.Drawing.Size(138, 23);
            this.lblMablagh.TabIndex = 63;
            this.lblMablagh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(3, 58);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(110, 23);
            this.labelX3.TabIndex = 55;
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(32, 29);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(81, 23);
            this.labelX4.TabIndex = 52;
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(455, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX5.Size = new System.Drawing.Size(24, 23);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.Teal;
            this.labelX5.SymbolSize = 15F;
            this.labelX5.TabIndex = 51;
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(385, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX6.Size = new System.Drawing.Size(81, 23);
            this.labelX6.TabIndex = 50;
            this.labelX6.Text = "کدقبض:";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblGcode
            // 
            this.lblGcode.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblGcode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblGcode.ForeColor = System.Drawing.Color.Black;
            this.lblGcode.Location = new System.Drawing.Point(306, 3);
            this.lblGcode.Name = "lblGcode";
            this.lblGcode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblGcode.Size = new System.Drawing.Size(81, 23);
            this.lblGcode.TabIndex = 49;
            this.lblGcode.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // FrmDeleteOneGhabz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 199);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.groupPanel3);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDeleteOneGhabz";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmDeleteOneGhabz_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmDeleteOneGhabz_KeyDown);
            this.groupPanel3.ResumeLayout(false);
            this.grpInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX btnShowGhobooz;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.LabelX lbl_mohlatPardakht;
        private DevComponents.DotNetBar.LabelX lbl_ab_mablagh;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX lblSelectedDoreh;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGharardad;
        private DevComponents.DotNetBar.Controls.GroupPanel grpInfo;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX lblMablagh;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX lblGcode;
        private DevComponents.DotNetBar.ButtonX BtnDeleteOne;
    }
}