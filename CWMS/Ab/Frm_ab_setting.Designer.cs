﻿namespace CWMS.Ab
{
    partial class Frm_ab_setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ab_setting));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_fazelab_mablagh = new CWMS.FloatTextBox();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.txt_aboonman = new CWMS.FloatTextBox();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.txtDardadMaliat = new CWMS.FloatTextBox();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txtZaribSakhtoSaz = new CWMS.FloatTextBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txttejarizarib = new CWMS.FloatTextBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_mohlat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_mah = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.txt_tozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ch_auto = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.hazineh_taviz_kontor = new CWMS.FloatTextBox();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.panelEx3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panelEx2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.hazineh_taviz_kontor);
            this.groupPanel1.Controls.Add(this.labelX15);
            this.groupPanel1.Controls.Add(this.txt_fazelab_mablagh);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.txt_aboonman);
            this.groupPanel1.Controls.Add(this.labelX13);
            this.groupPanel1.Controls.Add(this.txtDardadMaliat);
            this.groupPanel1.Controls.Add(this.labelX12);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.txtZaribSakhtoSaz);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.txttejarizarib);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.panelEx3);
            this.groupPanel1.Controls.Add(this.txt_mohlat);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.panelEx2);
            this.groupPanel1.Controls.Add(this.panelEx1);
            this.groupPanel1.Controls.Add(this.txt_mah);
            this.groupPanel1.Controls.Add(this.btn_save);
            this.groupPanel1.Controls.Add(this.txt_tozihat);
            this.groupPanel1.Controls.Add(this.ch_auto);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(28, 20);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(832, 465);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 1;
            this.groupPanel1.Text = "مدیریت کلی دوره های آب";
            // 
            // txt_fazelab_mablagh
            // 
            this.txt_fazelab_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_fazelab_mablagh.Border.Class = "TextBoxBorder";
            this.txt_fazelab_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_fazelab_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_fazelab_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_fazelab_mablagh.Location = new System.Drawing.Point(350, 371);
            this.txt_fazelab_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_fazelab_mablagh.Name = "txt_fazelab_mablagh";
            this.txt_fazelab_mablagh.PreventEnterBeep = true;
            this.txt_fazelab_mablagh.Size = new System.Drawing.Size(113, 28);
            this.txt_fazelab_mablagh.TabIndex = 39;
            this.txt_fazelab_mablagh.Text = ".0";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(471, 372);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(96, 27);
            this.labelX14.TabIndex = 40;
            this.labelX14.Text = "مبلغ فاضلاب :";
            // 
            // txt_aboonman
            // 
            this.txt_aboonman.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_aboonman.Border.Class = "TextBoxBorder";
            this.txt_aboonman.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_aboonman.DisabledBackColor = System.Drawing.Color.White;
            this.txt_aboonman.ForeColor = System.Drawing.Color.Black;
            this.txt_aboonman.Location = new System.Drawing.Point(65, 298);
            this.txt_aboonman.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_aboonman.Name = "txt_aboonman";
            this.txt_aboonman.PreventEnterBeep = true;
            this.txt_aboonman.Size = new System.Drawing.Size(61, 28);
            this.txt_aboonman.TabIndex = 36;
            this.txt_aboonman.Text = ".0";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(38, 297);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(169, 27);
            this.labelX13.TabIndex = 37;
            this.labelX13.Text = "ضریب آبونمان :";
            // 
            // txtDardadMaliat
            // 
            this.txtDardadMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDardadMaliat.Border.Class = "TextBoxBorder";
            this.txtDardadMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDardadMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtDardadMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtDardadMaliat.Location = new System.Drawing.Point(605, 371);
            this.txtDardadMaliat.Name = "txtDardadMaliat";
            this.txtDardadMaliat.PreventEnterBeep = true;
            this.txtDardadMaliat.Size = new System.Drawing.Size(43, 28);
            this.txtDardadMaliat.TabIndex = 21;
            this.txtDardadMaliat.Text = ".0";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(669, 374);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(126, 23);
            this.labelX12.TabIndex = 22;
            this.labelX12.Text = "درصد مالیات :";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(232, 298);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(40, 27);
            this.labelX10.TabIndex = 20;
            this.labelX10.Text = "برابر";
            // 
            // txtZaribSakhtoSaz
            // 
            this.txtZaribSakhtoSaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtZaribSakhtoSaz.Border.Class = "TextBoxBorder";
            this.txtZaribSakhtoSaz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtZaribSakhtoSaz.DisabledBackColor = System.Drawing.Color.White;
            this.txtZaribSakhtoSaz.ForeColor = System.Drawing.Color.Black;
            this.txtZaribSakhtoSaz.Location = new System.Drawing.Point(281, 296);
            this.txtZaribSakhtoSaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtZaribSakhtoSaz.Name = "txtZaribSakhtoSaz";
            this.txtZaribSakhtoSaz.PreventEnterBeep = true;
            this.txtZaribSakhtoSaz.Size = new System.Drawing.Size(61, 28);
            this.txtZaribSakhtoSaz.TabIndex = 18;
            this.txtZaribSakhtoSaz.Text = ".0";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(350, 298);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(169, 27);
            this.labelX11.TabIndex = 19;
            this.labelX11.Text = "ضریب در حال ساخت و ساز:";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(539, 301);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(40, 27);
            this.labelX9.TabIndex = 17;
            this.labelX9.Text = "برابر";
            // 
            // txttejarizarib
            // 
            this.txttejarizarib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txttejarizarib.Border.Class = "TextBoxBorder";
            this.txttejarizarib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txttejarizarib.DisabledBackColor = System.Drawing.Color.White;
            this.txttejarizarib.ForeColor = System.Drawing.Color.Black;
            this.txttejarizarib.Location = new System.Drawing.Point(588, 298);
            this.txttejarizarib.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txttejarizarib.Name = "txttejarizarib";
            this.txttejarizarib.PreventEnterBeep = true;
            this.txttejarizarib.Size = new System.Drawing.Size(61, 28);
            this.txttejarizarib.TabIndex = 15;
            this.txttejarizarib.Text = ".0";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(658, 301);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(137, 27);
            this.labelX1.TabIndex = 16;
            this.labelX1.Text = "ضریب تجاری آب :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(435, 209);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(180, 27);
            this.labelX8.TabIndex = 14;
            this.labelX8.Text = "روز پس از پایان دوره";
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.pictureBox3);
            this.panelEx3.Controls.Add(this.labelX6);
            this.panelEx3.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx3.Location = new System.Drawing.Point(34, 246);
            this.panelEx3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(757, 46);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 13;
            // 
            // pictureBox3
            // 
            this.pictureBox3.ForeColor = System.Drawing.Color.Black;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(702, 3);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(51, 36);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("B Yekan", 8F);
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(1, 1);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(691, 43);
            this.labelX6.TabIndex = 3;
            this.labelX6.Text = "این گزینه مشخص می کند که چند روز پس از صدور دوره آب ، مهلت پرداخت ان به پایان رسی" +
    "ده و اخطار ان به مشترکین سیستم ارسال شود ";
            this.labelX6.WordWrap = true;
            // 
            // txt_mohlat
            // 
            this.txt_mohlat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mohlat.Border.Class = "TextBoxBorder";
            this.txt_mohlat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mohlat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mohlat.ForeColor = System.Drawing.Color.Black;
            this.txt_mohlat.Location = new System.Drawing.Point(627, 207);
            this.txt_mohlat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_mohlat.Name = "txt_mohlat";
            this.txt_mohlat.PreventEnterBeep = true;
            this.txt_mohlat.Size = new System.Drawing.Size(61, 28);
            this.txt_mohlat.TabIndex = 3;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(611, 209);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(180, 27);
            this.labelX7.TabIndex = 11;
            this.labelX7.Text = "مهلت پرداخت :";
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.pictureBox2);
            this.panelEx2.Controls.Add(this.labelX5);
            this.panelEx2.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx2.Location = new System.Drawing.Point(34, 151);
            this.panelEx2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(757, 45);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 9;
            // 
            // pictureBox2
            // 
            this.pictureBox2.ForeColor = System.Drawing.Color.Black;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(702, 3);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 36);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("B Yekan", 8F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(2, 0);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(691, 43);
            this.labelX5.TabIndex = 3;
            this.labelX5.Text = "با فعال کردن این گزینه ، دوره آب به صورت خودکار در سیستم ایجاد می شوند . ";
            this.labelX5.WordWrap = true;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.pictureBox1);
            this.panelEx1.Controls.Add(this.labelX4);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Location = new System.Drawing.Point(37, 57);
            this.panelEx1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(757, 46);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ForeColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(702, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 8F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(1, -1);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(691, 43);
            this.labelX4.TabIndex = 3;
            this.labelX4.Text = "در این قسمت مشخص می کنید که به ازا  هر چند ماه ، دوره جدید آبی تشکیل شود و قبوض م" +
    "ربوطه صادر شود";
            this.labelX4.WordWrap = true;
            // 
            // txt_mah
            // 
            this.txt_mah.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mah.Border.Class = "TextBoxBorder";
            this.txt_mah.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mah.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mah.ForeColor = System.Drawing.Color.Black;
            this.txt_mah.Location = new System.Drawing.Point(554, 23);
            this.txt_mah.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_mah.Name = "txt_mah";
            this.txt_mah.PreventEnterBeep = true;
            this.txt_mah.Size = new System.Drawing.Size(61, 28);
            this.txt_mah.TabIndex = 1;
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_save.Location = new System.Drawing.Point(4, 405);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Size = new System.Drawing.Size(132, 27);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "ثبت اطلاعات";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txt_tozihat
            // 
            this.txt_tozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_tozihat.Border.Class = "TextBoxBorder";
            this.txt_tozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_tozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_tozihat.ForeColor = System.Drawing.Color.Black;
            this.txt_tozihat.Location = new System.Drawing.Point(38, 337);
            this.txt_tozihat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_tozihat.Name = "txt_tozihat";
            this.txt_tozihat.PreventEnterBeep = true;
            this.txt_tozihat.Size = new System.Drawing.Size(610, 28);
            this.txt_tozihat.TabIndex = 4;
            // 
            // ch_auto
            // 
            this.ch_auto.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ch_auto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ch_auto.ForeColor = System.Drawing.Color.Black;
            this.ch_auto.Location = new System.Drawing.Point(585, 124);
            this.ch_auto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ch_auto.Name = "ch_auto";
            this.ch_auto.Size = new System.Drawing.Size(208, 27);
            this.ch_auto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ch_auto.TabIndex = 2;
            this.ch_auto.Text = "مدیریت خودکار دوره ها ";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(637, 338);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(158, 27);
            this.labelX3.TabIndex = 2;
            this.labelX3.Text = "توضیحات :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(615, 24);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(180, 27);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "تعداد ماه ها میان هر دوره :";
            // 
            // hazineh_taviz_kontor
            // 
            this.hazineh_taviz_kontor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.hazineh_taviz_kontor.Border.Class = "TextBoxBorder";
            this.hazineh_taviz_kontor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.hazineh_taviz_kontor.DisabledBackColor = System.Drawing.Color.White;
            this.hazineh_taviz_kontor.ForeColor = System.Drawing.Color.Black;
            this.hazineh_taviz_kontor.Location = new System.Drawing.Point(106, 371);
            this.hazineh_taviz_kontor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hazineh_taviz_kontor.Name = "hazineh_taviz_kontor";
            this.hazineh_taviz_kontor.PreventEnterBeep = true;
            this.hazineh_taviz_kontor.Size = new System.Drawing.Size(113, 28);
            this.hazineh_taviz_kontor.TabIndex = 41;
            this.hazineh_taviz_kontor.Text = ".0";
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(213, 372);
            this.labelX15.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(116, 27);
            this.labelX15.TabIndex = 42;
            this.labelX15.Text = "هزینه تعویض کنتور:";
            // 
            // Frm_ab_setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 505);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_setting";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmManementAbInfo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmManementAbInfo_KeyDown);
            this.groupPanel1.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panelEx2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelEx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX9;
        private FloatTextBox txttejarizarib;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mohlat;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mah;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_tozihat;
        private DevComponents.DotNetBar.Controls.CheckBoxX ch_auto;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX10;
        private FloatTextBox txtZaribSakhtoSaz;
        private DevComponents.DotNetBar.LabelX labelX11;
        private FloatTextBox txtDardadMaliat;
        private DevComponents.DotNetBar.LabelX labelX12;
        private FloatTextBox txt_aboonman;
        private DevComponents.DotNetBar.LabelX labelX13;
        private FloatTextBox txt_fazelab_mablagh;
        private DevComponents.DotNetBar.LabelX labelX14;
        private FloatTextBox hazineh_taviz_kontor;
        private DevComponents.DotNetBar.LabelX labelX15;
    }
}