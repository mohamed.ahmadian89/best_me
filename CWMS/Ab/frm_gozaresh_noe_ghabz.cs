﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Ab
{
    public partial class frm_gozaresh_noe_ghabz : MyMetroForm
    {
        public frm_gozaresh_noe_ghabz()
        {
            InitializeComponent();
        }

        private void frm_gozaresh_noe_ghabz_Load(object sender, EventArgs e)
        {
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from ab_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt_ghabz = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + cmbDoreh.Text);
                txtGharardadCnt.Text = dt_ghabz.Rows.Count.ToString();
                if (dt_ghabz.Rows.Count != 0)
                {
                    txtDorKamel.Text = dt_ghabz.AsEnumerable().Where(p => p.Field<bool>("dor_kamel")).Count().ToString();
                    txtAdamGheraat.Text = dt_ghabz.AsEnumerable().Where(p => p.Field<bool>("adam_gheraat")).Count().ToString();
                    txtGhat.Text = dt_ghabz.AsEnumerable().Where(p => p.Field<bool>("ghat")).Count().ToString();
                    txtKharab.Text = dt_ghabz.AsEnumerable().Where(p => p.Field<bool>("kharab")).Count().ToString();
                    txtTaviz.Text = dt_ghabz.AsEnumerable().Where(p => p.Field<bool>("taviz")).Count().ToString();

                }
            }
            catch (Exception)
            {
                txtDorKamel.Text =
                txtAdamGheraat.Text =
                txtGhat.Text =
                txtKharab.Text =
                txtTaviz.Text = "";
            }

        }
    }
}
