﻿namespace CWMS.Ab
{
    partial class Frm_CreateKontorKhan_input
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.cmb_group = new System.Windows.Forms.ComboBox();
            this.kontorkhangharardadgroupingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset = new CWMS.Ab.MainAbDataset();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_create_new_ghabz = new DevComponents.DotNetBar.ButtonX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.kontor_khan_gharardad_groupingTableAdapter = new CWMS.Ab.MainAbDatasetTableAdapters.kontor_khan_gharardad_groupingTableAdapter();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grpUpdateGhobooozAb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kontorkhangharardadgroupingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(836, 258);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grpUpdateGhobooozAb);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(828, 225);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ارسال اطلاعات به نرم افزار اندروید";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.White;
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX3);
            this.grpUpdateGhobooozAb.Controls.Add(this.cmb_group);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX1);
            this.grpUpdateGhobooozAb.Controls.Add(this.btn_create_new_ghabz);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Font = new System.Drawing.Font("B Yekan", 10F);
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(48, 19);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(732, 190);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 22;
            this.grpUpdateGhobooozAb.Text = "تولید لیست ورودی نرم افزار اندروید کنتورخوان";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("B Yekan", 8F);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(20, 126);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(683, 31);
            this.labelX3.TabIndex = 33;
            this.labelX3.Text = "کاربر گرامی ، دقت نمایید تنها قراردادهایی به گوشی مامورین کنتورخوان ارسال خواهد ش" +
    "د که وضعیت صدور قبوض آب انها تایید شده باشد ";
            this.labelX3.WordWrap = true;
            // 
            // cmb_group
            // 
            this.cmb_group.DataSource = this.kontorkhangharardadgroupingBindingSource;
            this.cmb_group.DisplayMember = "group_name";
            this.cmb_group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_group.FormattingEnabled = true;
            this.cmb_group.Location = new System.Drawing.Point(353, 23);
            this.cmb_group.Name = "cmb_group";
            this.cmb_group.Size = new System.Drawing.Size(174, 28);
            this.cmb_group.TabIndex = 32;
            this.cmb_group.ValueMember = "id";
            // 
            // kontorkhangharardadgroupingBindingSource
            // 
            this.kontorkhangharardadgroupingBindingSource.DataMember = "kontor_khan_gharardad_grouping";
            this.kontorkhangharardadgroupingBindingSource.DataSource = this.mainAbDataset;
            // 
            // mainAbDataset
            // 
            this.mainAbDataset.DataSetName = "MainAbDataset";
            this.mainAbDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(523, 22);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(80, 31);
            this.labelX1.TabIndex = 31;
            this.labelX1.Text = "گروه مربوطه :";
            this.labelX1.WordWrap = true;
            // 
            // btn_create_new_ghabz
            // 
            this.btn_create_new_ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_ghabz.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_ghabz.Location = new System.Drawing.Point(20, 62);
            this.btn_create_new_ghabz.Margin = new System.Windows.Forms.Padding(4);
            this.btn_create_new_ghabz.Name = "btn_create_new_ghabz";
            this.btn_create_new_ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_ghabz.Size = new System.Drawing.Size(311, 38);
            this.btn_create_new_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_ghabz.Symbol = "";
            this.btn_create_new_ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_ghabz.SymbolSize = 10F;
            this.btn_create_new_ghabz.TabIndex = 4;
            this.btn_create_new_ghabz.Text = "ایجاد فایل حاوی قراردادهای گروه انتخاب شده";
            this.btn_create_new_ghabz.Click += new System.EventHandler(this.btn_create_new_ghabz_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(828, 225);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "گروه بندی قراردادها";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.Azure;
            this.groupPanel1.CanvasColor = System.Drawing.Color.Azure;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(48, 28);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(732, 167);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 23;
            this.groupPanel1.Text = "دریافت لیست ورودی از نرم افزار کنتورخوان";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(161, 69);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(402, 38);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 10F;
            this.buttonX2.TabIndex = 36;
            this.buttonX2.Text = "گروه بندی قراردادها";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(82, 17);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(607, 31);
            this.labelX7.TabIndex = 25;
            this.labelX7.Text = "جهت دسته بندی قراردادها در قالب گروه های مختلف ، بر روی دکمه زیر کلیک نمایید";
            this.labelX7.WordWrap = true;
            // 
            // kontor_khan_gharardad_groupingTableAdapter
            // 
            this.kontor_khan_gharardad_groupingTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_CreateKontorKhan_input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 282);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Frm_CreateKontorKhan_input";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سامانه مدیریت نرم افزار کنتورخوان";
            this.Load += new System.EventHandler(this.Frm_CreateKontorKhan_input_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kontorkhangharardadgroupingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private DevComponents.DotNetBar.ButtonX btn_create_new_ghabz;
        private System.Windows.Forms.TabPage tabPage2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.ComboBox cmb_group;
        private MainAbDataset mainAbDataset;
        private System.Windows.Forms.BindingSource kontorkhangharardadgroupingBindingSource;
        private MainAbDatasetTableAdapters.kontor_khan_gharardad_groupingTableAdapter kontor_khan_gharardad_groupingTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}