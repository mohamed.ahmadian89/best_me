﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class Frm_ab_ensheab_hazineh : MyMetroForm
    {
        public Frm_ab_ensheab_hazineh()
        {
            InitializeComponent();
        }

        private void EnsheabHazineh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.EnsheabHazineh' table. You can move, or remove it, as needed.
            this.ensheabHazinehTableAdapter.Fill(this.mainDataSest.EnsheabHazineh);
        }
        bool adding = false;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (adding == false)
            {
                adding = true;
                ensheabHazinehBindingSource.AddNew();
                txtSize.Focus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p81)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 81);

                if (txthazineh.Text.Trim() == "")
                {
                    txthazineh.Focus();
                    Payam.Show("لطفا هزینه را درست وارد کنید");
                }
                else if (txtSize.Text.Trim() == "")
                {
                    txtSize.Focus();
                    Payam.Show("لطفا مقدار اندازه را درست وارد کنید");
                }
                else
                {
                    try
                    {
                        Convert.ToSingle(txtSize.Text);
                    }
                    catch
                    {
                        txtSize.Focus();
                        Payam.Show("لطفا مقدار اندازه را درست وارد کنید");
                        return;
                    }
                    try
                    {
                        Convert.ToSingle(txthazineh.Text);
                    }
                    catch
                    {
                        txthazineh.Focus();
                        Payam.Show("لطفا هزینه را درست وارد کنید");
                        return;
                    }

                    adding = false;
                    ensheabHazinehBindingSource.EndEdit();
                    ensheabHazinehTableAdapter.Update(mainDataSest.EnsheabHazineh);
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p81)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 81,"حذف هزینه");

                if (adding == true)
                    adding = false;
                else if (ensheabHazinehBindingSource.Count > 0)
                {
                    ensheabHazinehBindingSource.RemoveCurrent();
                    ensheabHazinehBindingSource.EndEdit();
                    ensheabHazinehTableAdapter.Update(mainDataSest.EnsheabHazineh);
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EnsheabHazineh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }
    }
}