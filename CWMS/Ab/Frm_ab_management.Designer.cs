﻿namespace CWMS.Ab
{
    partial class Frm_ab_management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblAlert = new DevComponents.DotNetBar.LabelX();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel3 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.grp_arzesh_afzoodeh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX12 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.btn_gozesh_karbodi_jame_sharj = new DevComponents.DotNetBar.ButtonX();
            this.buttonX6 = new DevComponents.DotNetBar.ButtonX();
            this.grpStat = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblInfoAboutMoshahedeh = new DevComponents.DotNetBar.LabelX();
            this.buttonX8 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX7 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX9 = new DevComponents.DotNetBar.ButtonX();
            this.lbl_dar_hal_barrrasi = new DevComponents.DotNetBar.LabelX();
            this.grpdelete = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX5 = new DevComponents.DotNetBar.ButtonX();
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblSecondStep = new DevComponents.DotNetBar.LabelX();
            this.lblDorehInfotext2 = new DevComponents.DotNetBar.LabelX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.btnReadKontorKhanAndUpdate = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnChap = new DevComponents.DotNetBar.ButtonX();
            this.btnShowGhobooz = new DevComponents.DotNetBar.ButtonX();
            this.labelX27 = new DevComponents.DotNetBar.LabelX();
            this.labelX28 = new DevComponents.DotNetBar.LabelX();
            this.lbl_mohlatPardakht = new DevComponents.DotNetBar.LabelX();
            this.labelX25 = new DevComponents.DotNetBar.LabelX();
            this.labelX26 = new DevComponents.DotNetBar.LabelX();
            this.lbl_ab_mablagh = new DevComponents.DotNetBar.LabelX();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.lblSelectedDoreh = new DevComponents.DotNetBar.LabelX();
            this.grpKontorkhan = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.lblFirstStep = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.btnSodoorGhabzForAllMoshtarekin = new DevComponents.DotNetBar.ButtonX();
            this.lblDorehInfotext = new DevComponents.DotNetBar.LabelX();
            this.tabdorehdetails = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.hazineh_taviz_kontor = new CWMS.FloatTextBox();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txt_masraf_omoomi = new CWMS.FloatTextBox();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.txt_fazelab_mablagh = new CWMS.FloatTextBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.txt_aboonman = new CWMS.FloatTextBox();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.txtZaribSakhtoSaz = new CWMS.FloatTextBox();
            this.labelX44 = new DevComponents.DotNetBar.LabelX();
            this.labelX45 = new DevComponents.DotNetBar.LabelX();
            this.txttejarizarib = new CWMS.FloatTextBox();
            this.labelX46 = new DevComponents.DotNetBar.LabelX();
            this.txtDarsadMaliat = new CWMS.FloatTextBox();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.txt_doreh_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_mohlat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_mablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_end = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txt_start = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btn_set_parameter = new DevComponents.DotNetBar.ButtonX();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.TabNewDoreh = new DevComponents.DotNetBar.SuperTabItem();
            this.superTabControlPanel2 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnChapGheraat = new DevComponents.DotNetBar.ButtonX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.btnShowDetails = new DevComponents.DotNetBar.ButtonX();
            this.buttonX11 = new DevComponents.DotNetBar.ButtonX();
            this.dgv_doreh = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.starttimeDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.endtimeDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mohlatpardakhtDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mablaghabDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.darsadmaliatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZaribTejari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZaribSakhtoSaz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zarib_aboonman = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablagh_fazelab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf_omoomi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abdorehBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.superTabItem2 = new DevComponents.DotNetBar.SuperTabItem();
            this.ab_dorehTableAdapter = new CWMS.AbDatasetTableAdapters.ab_dorehTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel3.SuspendLayout();
            this.grp_arzesh_afzoodeh.SuspendLayout();
            this.grpStat.SuspendLayout();
            this.grpdelete.SuspendLayout();
            this.grpUpdateGhobooozAb.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.grpKontorkhan.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.superTabControlPanel2.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblAlert
            // 
            this.lblAlert.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblAlert.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAlert.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lblAlert.ForeColor = System.Drawing.Color.Black;
            this.lblAlert.Location = new System.Drawing.Point(317, 615);
            this.lblAlert.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.SingleLineColor = System.Drawing.Color.Red;
            this.lblAlert.Size = new System.Drawing.Size(713, 27);
            this.lblAlert.TabIndex = 39;
            this.lblAlert.Text = "تا اتمام تاریخ دوره فعلی امکان صدور دوره جدید وجود ندارد";
            this.lblAlert.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblAlert.Visible = false;
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel3);
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.Controls.Add(this.superTabControlPanel2);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(49, 25);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(1183, 577);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 1;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabNewDoreh,
            this.superTabItem2,
            this.tabdorehdetails});
            this.superTabControl1.Text = "مدیریت دوره ها";
            this.superTabControl1.SelectedTabChanged += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs>(this.superTabControl1_SelectedTabChanged);
            // 
            // superTabControlPanel3
            // 
            this.superTabControlPanel3.Controls.Add(this.grp_arzesh_afzoodeh);
            this.superTabControlPanel3.Controls.Add(this.grpStat);
            this.superTabControlPanel3.Controls.Add(this.grpdelete);
            this.superTabControlPanel3.Controls.Add(this.grpUpdateGhobooozAb);
            this.superTabControlPanel3.Controls.Add(this.groupPanel3);
            this.superTabControlPanel3.Controls.Add(this.grpKontorkhan);
            this.superTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel3.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel3.Name = "superTabControlPanel3";
            this.superTabControlPanel3.Size = new System.Drawing.Size(1183, 548);
            this.superTabControlPanel3.TabIndex = 0;
            this.superTabControlPanel3.TabItem = this.tabdorehdetails;
            // 
            // grp_arzesh_afzoodeh
            // 
            this.grp_arzesh_afzoodeh.BackColor = System.Drawing.Color.White;
            this.grp_arzesh_afzoodeh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grp_arzesh_afzoodeh.Controls.Add(this.buttonX12);
            this.grp_arzesh_afzoodeh.Controls.Add(this.buttonX10);
            this.grp_arzesh_afzoodeh.Controls.Add(this.btn_gozesh_karbodi_jame_sharj);
            this.grp_arzesh_afzoodeh.Controls.Add(this.buttonX6);
            this.grp_arzesh_afzoodeh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grp_arzesh_afzoodeh.Location = new System.Drawing.Point(40, 262);
            this.grp_arzesh_afzoodeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grp_arzesh_afzoodeh.Name = "grp_arzesh_afzoodeh";
            this.grp_arzesh_afzoodeh.Size = new System.Drawing.Size(641, 141);
            // 
            // 
            // 
            this.grp_arzesh_afzoodeh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grp_arzesh_afzoodeh.Style.BackColorGradientAngle = 90;
            this.grp_arzesh_afzoodeh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grp_arzesh_afzoodeh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_arzesh_afzoodeh.Style.BorderBottomWidth = 1;
            this.grp_arzesh_afzoodeh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grp_arzesh_afzoodeh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_arzesh_afzoodeh.Style.BorderLeftWidth = 1;
            this.grp_arzesh_afzoodeh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_arzesh_afzoodeh.Style.BorderRightWidth = 1;
            this.grp_arzesh_afzoodeh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grp_arzesh_afzoodeh.Style.BorderTopWidth = 1;
            this.grp_arzesh_afzoodeh.Style.CornerDiameter = 4;
            this.grp_arzesh_afzoodeh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grp_arzesh_afzoodeh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grp_arzesh_afzoodeh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grp_arzesh_afzoodeh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grp_arzesh_afzoodeh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grp_arzesh_afzoodeh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grp_arzesh_afzoodeh.TabIndex = 25;
            this.grp_arzesh_afzoodeh.Text = "گزارشات کاربردی";
            // 
            // buttonX12
            // 
            this.buttonX12.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX12.BackColor = System.Drawing.Color.Transparent;
            this.buttonX12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX12.Location = new System.Drawing.Point(339, 51);
            this.buttonX12.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX12.Name = "buttonX12";
            this.buttonX12.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX12.Size = new System.Drawing.Size(278, 24);
            this.buttonX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX12.Symbol = "";
            this.buttonX12.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX12.SymbolSize = 12F;
            this.buttonX12.TabIndex = 60;
            this.buttonX12.Text = "گزارش قبوض به تفکیک نوع";
            this.buttonX12.Click += new System.EventHandler(this.buttonX12_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.BackColor = System.Drawing.Color.Transparent;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(17, 51);
            this.buttonX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX10.Size = new System.Drawing.Size(278, 24);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX10.SymbolSize = 12F;
            this.buttonX10.TabIndex = 59;
            this.buttonX10.Text = "صدور پیش نویس قبوض  آب ( حسابداری )";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click_1);
            // 
            // btn_gozesh_karbodi_jame_sharj
            // 
            this.btn_gozesh_karbodi_jame_sharj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_gozesh_karbodi_jame_sharj.BackColor = System.Drawing.Color.Transparent;
            this.btn_gozesh_karbodi_jame_sharj.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_gozesh_karbodi_jame_sharj.Location = new System.Drawing.Point(17, 22);
            this.btn_gozesh_karbodi_jame_sharj.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_gozesh_karbodi_jame_sharj.Name = "btn_gozesh_karbodi_jame_sharj";
            this.btn_gozesh_karbodi_jame_sharj.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btn_gozesh_karbodi_jame_sharj.Size = new System.Drawing.Size(278, 24);
            this.btn_gozesh_karbodi_jame_sharj.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_gozesh_karbodi_jame_sharj.Symbol = "";
            this.btn_gozesh_karbodi_jame_sharj.SymbolColor = System.Drawing.Color.Teal;
            this.btn_gozesh_karbodi_jame_sharj.SymbolSize = 12F;
            this.btn_gozesh_karbodi_jame_sharj.TabIndex = 58;
            this.btn_gozesh_karbodi_jame_sharj.Text = "گزارش جامع آب";
            this.btn_gozesh_karbodi_jame_sharj.Click += new System.EventHandler(this.btn_gozesh_karbodi_jame_sharj_Click);
            // 
            // buttonX6
            // 
            this.buttonX6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX6.BackColor = System.Drawing.Color.Transparent;
            this.buttonX6.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX6.Location = new System.Drawing.Point(339, 22);
            this.buttonX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX6.Name = "buttonX6";
            this.buttonX6.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX6.Size = new System.Drawing.Size(278, 24);
            this.buttonX6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX6.Symbol = "";
            this.buttonX6.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX6.SymbolSize = 12F;
            this.buttonX6.TabIndex = 56;
            this.buttonX6.Text = "گزارش ارزش افزوده دوره  جاری";
            this.buttonX6.Click += new System.EventHandler(this.buttonX6_Click);
            // 
            // grpStat
            // 
            this.grpStat.BackColor = System.Drawing.Color.White;
            this.grpStat.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpStat.Controls.Add(this.lblInfoAboutMoshahedeh);
            this.grpStat.Controls.Add(this.buttonX8);
            this.grpStat.Controls.Add(this.buttonX7);
            this.grpStat.Controls.Add(this.buttonX9);
            this.grpStat.Controls.Add(this.lbl_dar_hal_barrrasi);
            this.grpStat.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpStat.Location = new System.Drawing.Point(689, 300);
            this.grpStat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpStat.Name = "grpStat";
            this.grpStat.Size = new System.Drawing.Size(475, 234);
            // 
            // 
            // 
            this.grpStat.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpStat.Style.BackColorGradientAngle = 90;
            this.grpStat.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpStat.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderBottomWidth = 1;
            this.grpStat.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpStat.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderLeftWidth = 1;
            this.grpStat.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderRightWidth = 1;
            this.grpStat.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderTopWidth = 1;
            this.grpStat.Style.CornerDiameter = 4;
            this.grpStat.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpStat.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpStat.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpStat.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpStat.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpStat.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpStat.TabIndex = 24;
            this.grpStat.Text = "سامانه اطلاع رسانی";
            // 
            // lblInfoAboutMoshahedeh
            // 
            this.lblInfoAboutMoshahedeh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblInfoAboutMoshahedeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblInfoAboutMoshahedeh.Font = new System.Drawing.Font("B Yekan", 9F, System.Drawing.FontStyle.Underline);
            this.lblInfoAboutMoshahedeh.ForeColor = System.Drawing.Color.Black;
            this.lblInfoAboutMoshahedeh.Location = new System.Drawing.Point(7, 100);
            this.lblInfoAboutMoshahedeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblInfoAboutMoshahedeh.Name = "lblInfoAboutMoshahedeh";
            this.lblInfoAboutMoshahedeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblInfoAboutMoshahedeh.Size = new System.Drawing.Size(458, 27);
            this.lblInfoAboutMoshahedeh.TabIndex = 85;
            this.lblInfoAboutMoshahedeh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX8
            // 
            this.buttonX8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX8.BackColor = System.Drawing.Color.Transparent;
            this.buttonX8.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX8.Location = new System.Drawing.Point(73, 72);
            this.buttonX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX8.Name = "buttonX8";
            this.buttonX8.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX8.Size = new System.Drawing.Size(364, 27);
            this.buttonX8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX8.Symbol = "";
            this.buttonX8.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX8.SymbolSize = 12F;
            this.buttonX8.TabIndex = 81;
            this.buttonX8.Text = "قبوض دوره فعلی برای مشترکین قابل مشاهده نباشد";
            this.buttonX8.Click += new System.EventHandler(this.buttonX8_Click);
            // 
            // buttonX7
            // 
            this.buttonX7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX7.BackColor = System.Drawing.Color.Transparent;
            this.buttonX7.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX7.Location = new System.Drawing.Point(73, 39);
            this.buttonX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX7.Name = "buttonX7";
            this.buttonX7.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX7.Size = new System.Drawing.Size(364, 27);
            this.buttonX7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX7.Symbol = "";
            this.buttonX7.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX7.SymbolSize = 12F;
            this.buttonX7.TabIndex = 80;
            this.buttonX7.Text = "قبوض دوره فعلی برای مشترکین قابل مشاهده باشد";
            this.buttonX7.Click += new System.EventHandler(this.buttonX7_Click);
            // 
            // buttonX9
            // 
            this.buttonX9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX9.BackColor = System.Drawing.Color.Transparent;
            this.buttonX9.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX9.Location = new System.Drawing.Point(115, 5);
            this.buttonX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX9.Name = "buttonX9";
            this.buttonX9.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.buttonX9.Size = new System.Drawing.Size(271, 27);
            this.buttonX9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX9.Symbol = "";
            this.buttonX9.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX9.SymbolSize = 12F;
            this.buttonX9.TabIndex = 79;
            this.buttonX9.Text = "سامانه اطلاع رسانی مشترکین";
            this.buttonX9.Click += new System.EventHandler(this.buttonX9_Click);
            // 
            // lbl_dar_hal_barrrasi
            // 
            this.lbl_dar_hal_barrrasi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_dar_hal_barrrasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_dar_hal_barrrasi.ForeColor = System.Drawing.Color.Black;
            this.lbl_dar_hal_barrrasi.Location = new System.Drawing.Point(68, 37);
            this.lbl_dar_hal_barrrasi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_dar_hal_barrrasi.Name = "lbl_dar_hal_barrrasi";
            this.lbl_dar_hal_barrrasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_dar_hal_barrrasi.Size = new System.Drawing.Size(83, 27);
            this.lbl_dar_hal_barrrasi.TabIndex = 77;
            this.lbl_dar_hal_barrrasi.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // grpdelete
            // 
            this.grpdelete.BackColor = System.Drawing.Color.White;
            this.grpdelete.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpdelete.Controls.Add(this.buttonX2);
            this.grpdelete.Controls.Add(this.buttonX5);
            this.grpdelete.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpdelete.Location = new System.Drawing.Point(40, 409);
            this.grpdelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpdelete.Name = "grpdelete";
            this.grpdelete.Size = new System.Drawing.Size(641, 125);
            // 
            // 
            // 
            this.grpdelete.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpdelete.Style.BackColorGradientAngle = 90;
            this.grpdelete.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpdelete.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderBottomWidth = 1;
            this.grpdelete.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpdelete.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderLeftWidth = 1;
            this.grpdelete.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderRightWidth = 1;
            this.grpdelete.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpdelete.Style.BorderTopWidth = 1;
            this.grpdelete.Style.CornerDiameter = 4;
            this.grpdelete.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpdelete.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpdelete.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpdelete.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpdelete.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpdelete.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpdelete.TabIndex = 20;
            this.grpdelete.Text = "حذف قبوض";
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(57, 57);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX2.Size = new System.Drawing.Size(520, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX2.SymbolSize = 12F;
            this.buttonX2.TabIndex = 56;
            this.buttonX2.Text = "صرفا قبوض دوره حذف شود و پرداختی های مربوطه در سیستم ذخیره بماند";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click_1);
            // 
            // buttonX5
            // 
            this.buttonX5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX5.BackColor = System.Drawing.Color.Transparent;
            this.buttonX5.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX5.Location = new System.Drawing.Point(57, 24);
            this.buttonX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX5.Name = "buttonX5";
            this.buttonX5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX5.Size = new System.Drawing.Size(520, 27);
            this.buttonX5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX5.Symbol = "";
            this.buttonX5.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX5.SymbolSize = 12F;
            this.buttonX5.TabIndex = 55;
            this.buttonX5.Text = "حذف کلیه قبوض دوره جاری همراه با کلیه پرداختی های ثبت شده در سامانه پرداخت";
            this.buttonX5.Click += new System.EventHandler(this.buttonX5_Click);
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.White;
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.progressBar1);
            this.grpUpdateGhobooozAb.Controls.Add(this.lblSecondStep);
            this.grpUpdateGhobooozAb.Controls.Add(this.lblDorehInfotext2);
            this.grpUpdateGhobooozAb.Controls.Add(this.buttonX4);
            this.grpUpdateGhobooozAb.Controls.Add(this.btnReadKontorKhanAndUpdate);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(40, 142);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(748, 114);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 18;
            this.grpUpdateGhobooozAb.Text = "مرحله دوم - صدور قبوض آب";
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.ForeColor = System.Drawing.Color.Black;
            this.progressBar1.Location = new System.Drawing.Point(172, 57);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(361, 26);
            this.progressBar1.TabIndex = 76;
            // 
            // lblSecondStep
            // 
            this.lblSecondStep.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSecondStep.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecondStep.ForeColor = System.Drawing.Color.Black;
            this.lblSecondStep.Location = new System.Drawing.Point(4, 1);
            this.lblSecondStep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblSecondStep.Name = "lblSecondStep";
            this.lblSecondStep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSecondStep.Size = new System.Drawing.Size(57, 53);
            this.lblSecondStep.Symbol = "";
            this.lblSecondStep.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblSecondStep.SymbolSize = 30F;
            this.lblSecondStep.TabIndex = 75;
            this.lblSecondStep.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblSecondStep.Visible = false;
            // 
            // lblDorehInfotext2
            // 
            this.lblDorehInfotext2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblDorehInfotext2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDorehInfotext2.ForeColor = System.Drawing.Color.Black;
            this.lblDorehInfotext2.Location = new System.Drawing.Point(70, 36);
            this.lblDorehInfotext2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblDorehInfotext2.Name = "lblDorehInfotext2";
            this.lblDorehInfotext2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDorehInfotext2.Size = new System.Drawing.Size(593, 27);
            this.lblDorehInfotext2.TabIndex = 59;
            this.lblDorehInfotext2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Font = new System.Drawing.Font("B Yekan", 15F, System.Drawing.FontStyle.Bold);
            this.buttonX4.Location = new System.Drawing.Point(666, 10);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX4.Size = new System.Drawing.Size(59, 41);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX4.SymbolSize = 12F;
            this.buttonX4.TabIndex = 58;
            this.buttonX4.Text = "2";
            // 
            // btnReadKontorKhanAndUpdate
            // 
            this.btnReadKontorKhanAndUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReadKontorKhanAndUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnReadKontorKhanAndUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnReadKontorKhanAndUpdate.Location = new System.Drawing.Point(158, 6);
            this.btnReadKontorKhanAndUpdate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnReadKontorKhanAndUpdate.Name = "btnReadKontorKhanAndUpdate";
            this.btnReadKontorKhanAndUpdate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnReadKontorKhanAndUpdate.Size = new System.Drawing.Size(394, 24);
            this.btnReadKontorKhanAndUpdate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnReadKontorKhanAndUpdate.Symbol = "";
            this.btnReadKontorKhanAndUpdate.SymbolColor = System.Drawing.Color.Teal;
            this.btnReadKontorKhanAndUpdate.SymbolSize = 12F;
            this.btnReadKontorKhanAndUpdate.TabIndex = 55;
            this.btnReadKontorKhanAndUpdate.Text = "صدور قبوض آب مشترکین";
            this.btnReadKontorKhanAndUpdate.Click += new System.EventHandler(this.btnReadKontorKhanAndUpdate_Click);
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.btnChap);
            this.groupPanel3.Controls.Add(this.btnShowGhobooz);
            this.groupPanel3.Controls.Add(this.labelX27);
            this.groupPanel3.Controls.Add(this.labelX28);
            this.groupPanel3.Controls.Add(this.lbl_mohlatPardakht);
            this.groupPanel3.Controls.Add(this.labelX25);
            this.groupPanel3.Controls.Add(this.labelX26);
            this.groupPanel3.Controls.Add(this.lbl_ab_mablagh);
            this.groupPanel3.Controls.Add(this.labelX24);
            this.groupPanel3.Controls.Add(this.labelX23);
            this.groupPanel3.Controls.Add(this.lblSelectedDoreh);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(812, 16);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(352, 276);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 14;
            this.groupPanel3.Text = "اطلاعات دوره ";
            // 
            // btnChap
            // 
            this.btnChap.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChap.BackColor = System.Drawing.Color.Transparent;
            this.btnChap.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnChap.Location = new System.Drawing.Point(45, 207);
            this.btnChap.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnChap.Name = "btnChap";
            this.btnChap.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.btnChap.Size = new System.Drawing.Size(259, 27);
            this.btnChap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnChap.Symbol = "";
            this.btnChap.SymbolColor = System.Drawing.Color.Teal;
            this.btnChap.SymbolSize = 12F;
            this.btnChap.TabIndex = 81;
            this.btnChap.Text = "چاپ قبوض دوره";
            this.btnChap.Visible = false;
            this.btnChap.Click += new System.EventHandler(this.btnChap_Click);
            // 
            // btnShowGhobooz
            // 
            this.btnShowGhobooz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowGhobooz.BackColor = System.Drawing.Color.Transparent;
            this.btnShowGhobooz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowGhobooz.Location = new System.Drawing.Point(45, 168);
            this.btnShowGhobooz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowGhobooz.Name = "btnShowGhobooz";
            this.btnShowGhobooz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowGhobooz.Size = new System.Drawing.Size(263, 27);
            this.btnShowGhobooz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowGhobooz.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowGhobooz.SymbolSize = 12F;
            this.btnShowGhobooz.TabIndex = 62;
            this.btnShowGhobooz.Text = "نمایش قبوض صادرشده";
            this.btnShowGhobooz.Click += new System.EventHandler(this.buttonX3_Click_1);
            // 
            // labelX27
            // 
            this.labelX27.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX27.ForeColor = System.Drawing.Color.Black;
            this.labelX27.Location = new System.Drawing.Point(274, 95);
            this.labelX27.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX27.Name = "labelX27";
            this.labelX27.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX27.Size = new System.Drawing.Size(34, 27);
            this.labelX27.Symbol = "";
            this.labelX27.SymbolColor = System.Drawing.Color.Teal;
            this.labelX27.SymbolSize = 15F;
            this.labelX27.TabIndex = 57;
            this.labelX27.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX28
            // 
            this.labelX28.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX28.ForeColor = System.Drawing.Color.Black;
            this.labelX28.Location = new System.Drawing.Point(163, 95);
            this.labelX28.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX28.Name = "labelX28";
            this.labelX28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX28.Size = new System.Drawing.Size(115, 27);
            this.labelX28.TabIndex = 56;
            this.labelX28.Text = "مهلت پرداخت:";
            this.labelX28.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_mohlatPardakht
            // 
            this.lbl_mohlatPardakht.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_mohlatPardakht.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_mohlatPardakht.ForeColor = System.Drawing.Color.Black;
            this.lbl_mohlatPardakht.Location = new System.Drawing.Point(4, 95);
            this.lbl_mohlatPardakht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_mohlatPardakht.Name = "lbl_mohlatPardakht";
            this.lbl_mohlatPardakht.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_mohlatPardakht.Size = new System.Drawing.Size(156, 27);
            this.lbl_mohlatPardakht.TabIndex = 55;
            this.lbl_mohlatPardakht.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX25
            // 
            this.labelX25.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX25.ForeColor = System.Drawing.Color.Black;
            this.labelX25.Location = new System.Drawing.Point(274, 61);
            this.labelX25.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX25.Name = "labelX25";
            this.labelX25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX25.Size = new System.Drawing.Size(34, 27);
            this.labelX25.Symbol = "";
            this.labelX25.SymbolColor = System.Drawing.Color.Teal;
            this.labelX25.SymbolSize = 15F;
            this.labelX25.TabIndex = 54;
            this.labelX25.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX26
            // 
            this.labelX26.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX26.ForeColor = System.Drawing.Color.Black;
            this.labelX26.Location = new System.Drawing.Point(163, 61);
            this.labelX26.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX26.Name = "labelX26";
            this.labelX26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX26.Size = new System.Drawing.Size(115, 27);
            this.labelX26.TabIndex = 53;
            this.labelX26.Text = "مبلغ آب :";
            this.labelX26.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_ab_mablagh
            // 
            this.lbl_ab_mablagh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_ab_mablagh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_ab_mablagh.ForeColor = System.Drawing.Color.Black;
            this.lbl_ab_mablagh.Location = new System.Drawing.Point(45, 61);
            this.lbl_ab_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbl_ab_mablagh.Name = "lbl_ab_mablagh";
            this.lbl_ab_mablagh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_ab_mablagh.Size = new System.Drawing.Size(115, 27);
            this.lbl_ab_mablagh.TabIndex = 52;
            this.lbl_ab_mablagh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX24
            // 
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(274, 29);
            this.labelX24.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX24.Name = "labelX24";
            this.labelX24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX24.Size = new System.Drawing.Size(34, 27);
            this.labelX24.Symbol = "";
            this.labelX24.SymbolColor = System.Drawing.Color.Teal;
            this.labelX24.SymbolSize = 15F;
            this.labelX24.TabIndex = 51;
            this.labelX24.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX23
            // 
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(163, 29);
            this.labelX23.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX23.Name = "labelX23";
            this.labelX23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX23.Size = new System.Drawing.Size(115, 27);
            this.labelX23.TabIndex = 50;
            this.labelX23.Text = "دوره :";
            this.labelX23.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblSelectedDoreh
            // 
            this.lblSelectedDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSelectedDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSelectedDoreh.ForeColor = System.Drawing.Color.Black;
            this.lblSelectedDoreh.Location = new System.Drawing.Point(45, 29);
            this.lblSelectedDoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblSelectedDoreh.Name = "lblSelectedDoreh";
            this.lblSelectedDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSelectedDoreh.Size = new System.Drawing.Size(115, 27);
            this.lblSelectedDoreh.TabIndex = 49;
            this.lblSelectedDoreh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // grpKontorkhan
            // 
            this.grpKontorkhan.BackColor = System.Drawing.Color.White;
            this.grpKontorkhan.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpKontorkhan.Controls.Add(this.labelX7);
            this.grpKontorkhan.Controls.Add(this.lblFirstStep);
            this.grpKontorkhan.Controls.Add(this.buttonX1);
            this.grpKontorkhan.Controls.Add(this.btnSodoorGhabzForAllMoshtarekin);
            this.grpKontorkhan.Controls.Add(this.lblDorehInfotext);
            this.grpKontorkhan.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpKontorkhan.Location = new System.Drawing.Point(40, 15);
            this.grpKontorkhan.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpKontorkhan.Name = "grpKontorkhan";
            this.grpKontorkhan.Size = new System.Drawing.Size(748, 121);
            // 
            // 
            // 
            this.grpKontorkhan.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpKontorkhan.Style.BackColorGradientAngle = 90;
            this.grpKontorkhan.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpKontorkhan.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpKontorkhan.Style.BorderBottomWidth = 1;
            this.grpKontorkhan.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpKontorkhan.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpKontorkhan.Style.BorderLeftWidth = 1;
            this.grpKontorkhan.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpKontorkhan.Style.BorderRightWidth = 1;
            this.grpKontorkhan.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpKontorkhan.Style.BorderTopWidth = 1;
            this.grpKontorkhan.Style.CornerDiameter = 4;
            this.grpKontorkhan.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpKontorkhan.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpKontorkhan.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpKontorkhan.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpKontorkhan.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpKontorkhan.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpKontorkhan.TabIndex = 13;
            this.grpKontorkhan.Text = "مدیریت قرائت های مربوط به کنتورهای آب";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(86, 14);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(531, 27);
            this.labelX7.TabIndex = 82;
            this.labelX7.Text = "کاربر گرامی ، ابتدا قرائت های مربوط به کنتورهای آب را در سیستم مشاهده و یا به آن " +
    "اضافه نمایید و سپس اقدام به صدور قبوض آب نمایید";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblFirstStep
            // 
            this.lblFirstStep.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblFirstStep.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFirstStep.ForeColor = System.Drawing.Color.Black;
            this.lblFirstStep.Location = new System.Drawing.Point(4, 5);
            this.lblFirstStep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblFirstStep.Name = "lblFirstStep";
            this.lblFirstStep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblFirstStep.Size = new System.Drawing.Size(57, 53);
            this.lblFirstStep.Symbol = "";
            this.lblFirstStep.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblFirstStep.SymbolSize = 30F;
            this.lblFirstStep.TabIndex = 76;
            this.lblFirstStep.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblFirstStep.Visible = false;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Font = new System.Drawing.Font("B Yekan", 15F, System.Drawing.FontStyle.Bold);
            this.buttonX1.Location = new System.Drawing.Point(666, 14);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.buttonX1.Size = new System.Drawing.Size(59, 41);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 58;
            this.buttonX1.Text = "1";
            // 
            // btnSodoorGhabzForAllMoshtarekin
            // 
            this.btnSodoorGhabzForAllMoshtarekin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSodoorGhabzForAllMoshtarekin.BackColor = System.Drawing.Color.Transparent;
            this.btnSodoorGhabzForAllMoshtarekin.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSodoorGhabzForAllMoshtarekin.Location = new System.Drawing.Point(112, 47);
            this.btnSodoorGhabzForAllMoshtarekin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSodoorGhabzForAllMoshtarekin.Name = "btnSodoorGhabzForAllMoshtarekin";
            this.btnSodoorGhabzForAllMoshtarekin.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnSodoorGhabzForAllMoshtarekin.Size = new System.Drawing.Size(505, 27);
            this.btnSodoorGhabzForAllMoshtarekin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSodoorGhabzForAllMoshtarekin.Symbol = "";
            this.btnSodoorGhabzForAllMoshtarekin.SymbolColor = System.Drawing.Color.Teal;
            this.btnSodoorGhabzForAllMoshtarekin.SymbolSize = 12F;
            this.btnSodoorGhabzForAllMoshtarekin.TabIndex = 55;
            this.btnSodoorGhabzForAllMoshtarekin.Text = "مشاهده یا ثبت دستی قرائت های مربوط به کنتورهاب آب مشترکین\r\n";
            this.btnSodoorGhabzForAllMoshtarekin.Click += new System.EventHandler(this.btnSodoorGhabzForAllMoshtarekin_Click);
            // 
            // lblDorehInfotext
            // 
            this.lblDorehInfotext.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblDorehInfotext.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDorehInfotext.ForeColor = System.Drawing.Color.Black;
            this.lblDorehInfotext.Location = new System.Drawing.Point(57, 58);
            this.lblDorehInfotext.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblDorehInfotext.Name = "lblDorehInfotext";
            this.lblDorehInfotext.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDorehInfotext.Size = new System.Drawing.Size(593, 10);
            this.lblDorehInfotext.TabIndex = 57;
            this.lblDorehInfotext.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // tabdorehdetails
            // 
            this.tabdorehdetails.AttachedControl = this.superTabControlPanel3;
            this.tabdorehdetails.GlobalItem = false;
            this.tabdorehdetails.Name = "tabdorehdetails";
            this.tabdorehdetails.Text = "اطلاعات دوره ";
            this.tabdorehdetails.Visible = false;
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.groupPanel1);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 29);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(1183, 548);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.TabNewDoreh;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.hazineh_taviz_kontor);
            this.groupPanel1.Controls.Add(this.labelX15);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.txt_masraf_omoomi);
            this.groupPanel1.Controls.Add(this.labelX13);
            this.groupPanel1.Controls.Add(this.txt_fazelab_mablagh);
            this.groupPanel1.Controls.Add(this.labelX11);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.txt_aboonman);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.txtZaribSakhtoSaz);
            this.groupPanel1.Controls.Add(this.labelX44);
            this.groupPanel1.Controls.Add(this.labelX45);
            this.groupPanel1.Controls.Add(this.txttejarizarib);
            this.groupPanel1.Controls.Add(this.labelX46);
            this.groupPanel1.Controls.Add(this.txtDarsadMaliat);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.txt_doreh_code);
            this.groupPanel1.Controls.Add(this.txt_mohlat);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txt_mablagh);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.txt_end);
            this.groupPanel1.Controls.Add(this.txt_start);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.btn_set_parameter);
            this.groupPanel1.Controls.Add(this.btn_create_new_doreh);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(42, 20);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1122, 498);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "اطلاعات دوره جدید";
            // 
            // hazineh_taviz_kontor
            // 
            this.hazineh_taviz_kontor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.hazineh_taviz_kontor.Border.Class = "TextBoxBorder";
            this.hazineh_taviz_kontor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.hazineh_taviz_kontor.DisabledBackColor = System.Drawing.Color.White;
            this.hazineh_taviz_kontor.ForeColor = System.Drawing.Color.Black;
            this.hazineh_taviz_kontor.Location = new System.Drawing.Point(170, 225);
            this.hazineh_taviz_kontor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.hazineh_taviz_kontor.Name = "hazineh_taviz_kontor";
            this.hazineh_taviz_kontor.PreventEnterBeep = true;
            this.hazineh_taviz_kontor.Size = new System.Drawing.Size(113, 28);
            this.hazineh_taviz_kontor.TabIndex = 43;
            this.hazineh_taviz_kontor.Text = ".0";
            // 
            // labelX15
            // 
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(277, 226);
            this.labelX15.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(116, 27);
            this.labelX15.TabIndex = 44;
            this.labelX15.Text = "هزینه تعویض کنتور:";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(179, 288);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(576, 27);
            this.labelX14.TabIndex = 42;
            this.labelX14.Text = "هزینه مصارف عمومی شهرک که می بایست سهم هریک از مشترکین بر اساس متراژ آنها محاسبه " +
    "و تقسیم شود";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(223, 186);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(576, 27);
            this.labelX6.TabIndex = 41;
            this.labelX6.Text = "در صورتی که مشترک در حال ساخت و ساز باشد ، مبلغ آبها آنها در ضریب ساخت و ساز ضرب " +
    "خواهد شد";
            // 
            // txt_masraf_omoomi
            // 
            this.txt_masraf_omoomi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_masraf_omoomi.Border.Class = "TextBoxBorder";
            this.txt_masraf_omoomi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_masraf_omoomi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_masraf_omoomi.ForeColor = System.Drawing.Color.Black;
            this.txt_masraf_omoomi.Location = new System.Drawing.Point(789, 288);
            this.txt_masraf_omoomi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_masraf_omoomi.Name = "txt_masraf_omoomi";
            this.txt_masraf_omoomi.PreventEnterBeep = true;
            this.txt_masraf_omoomi.Size = new System.Drawing.Size(113, 28);
            this.txt_masraf_omoomi.TabIndex = 39;
            this.txt_masraf_omoomi.Text = ".0";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.Location = new System.Drawing.Point(952, 289);
            this.labelX13.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(96, 27);
            this.labelX13.TabIndex = 40;
            this.labelX13.Text = "مصرف عمومی :";
            // 
            // txt_fazelab_mablagh
            // 
            this.txt_fazelab_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_fazelab_mablagh.Border.Class = "TextBoxBorder";
            this.txt_fazelab_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_fazelab_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_fazelab_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_fazelab_mablagh.Location = new System.Drawing.Point(409, 226);
            this.txt_fazelab_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_fazelab_mablagh.Name = "txt_fazelab_mablagh";
            this.txt_fazelab_mablagh.PreventEnterBeep = true;
            this.txt_fazelab_mablagh.Size = new System.Drawing.Size(113, 28);
            this.txt_fazelab_mablagh.TabIndex = 37;
            this.txt_fazelab_mablagh.Text = ".0";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(530, 227);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(96, 27);
            this.labelX11.TabIndex = 38;
            this.labelX11.Text = "مبلغ فاضلاب :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(757, 227);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(40, 27);
            this.labelX8.TabIndex = 36;
            this.labelX8.Text = "برابر";
            // 
            // txt_aboonman
            // 
            this.txt_aboonman.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_aboonman.Border.Class = "TextBoxBorder";
            this.txt_aboonman.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_aboonman.DisabledBackColor = System.Drawing.Color.White;
            this.txt_aboonman.ForeColor = System.Drawing.Color.Black;
            this.txt_aboonman.Location = new System.Drawing.Point(832, 226);
            this.txt_aboonman.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_aboonman.Name = "txt_aboonman";
            this.txt_aboonman.PreventEnterBeep = true;
            this.txt_aboonman.Size = new System.Drawing.Size(61, 28);
            this.txt_aboonman.TabIndex = 34;
            this.txt_aboonman.Text = ".0";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(879, 227);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(169, 27);
            this.labelX9.TabIndex = 35;
            this.labelX9.Text = "ضریب آبونمان :";
            // 
            // txtZaribSakhtoSaz
            // 
            this.txtZaribSakhtoSaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtZaribSakhtoSaz.Border.Class = "TextBoxBorder";
            this.txtZaribSakhtoSaz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtZaribSakhtoSaz.DisabledBackColor = System.Drawing.Color.White;
            this.txtZaribSakhtoSaz.ForeColor = System.Drawing.Color.Black;
            this.txtZaribSakhtoSaz.Location = new System.Drawing.Point(832, 189);
            this.txtZaribSakhtoSaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtZaribSakhtoSaz.Name = "txtZaribSakhtoSaz";
            this.txtZaribSakhtoSaz.PreventEnterBeep = true;
            this.txtZaribSakhtoSaz.Size = new System.Drawing.Size(61, 28);
            this.txtZaribSakhtoSaz.TabIndex = 31;
            this.txtZaribSakhtoSaz.Text = ".0";
            // 
            // labelX44
            // 
            this.labelX44.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX44.ForeColor = System.Drawing.Color.Black;
            this.labelX44.Location = new System.Drawing.Point(905, 190);
            this.labelX44.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX44.Name = "labelX44";
            this.labelX44.Size = new System.Drawing.Size(143, 27);
            this.labelX44.TabIndex = 32;
            this.labelX44.Text = "ضریب در حال ساخت و ساز:";
            // 
            // labelX45
            // 
            this.labelX45.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX45.ForeColor = System.Drawing.Color.Black;
            this.labelX45.Location = new System.Drawing.Point(223, 152);
            this.labelX45.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX45.Name = "labelX45";
            this.labelX45.Size = new System.Drawing.Size(576, 27);
            this.labelX45.TabIndex = 30;
            this.labelX45.Text = "در صورتی که مشترک دارای واحد تجاری باشد ، مبلغ آبها آنها در ضریب تجاری ضرب خواهد " +
    "شد";
            // 
            // txttejarizarib
            // 
            this.txttejarizarib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txttejarizarib.Border.Class = "TextBoxBorder";
            this.txttejarizarib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txttejarizarib.DisabledBackColor = System.Drawing.Color.White;
            this.txttejarizarib.ForeColor = System.Drawing.Color.Black;
            this.txttejarizarib.Location = new System.Drawing.Point(832, 154);
            this.txttejarizarib.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txttejarizarib.Name = "txttejarizarib";
            this.txttejarizarib.PreventEnterBeep = true;
            this.txttejarizarib.Size = new System.Drawing.Size(61, 28);
            this.txttejarizarib.TabIndex = 28;
            this.txttejarizarib.Text = ".0";
            // 
            // labelX46
            // 
            this.labelX46.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX46.ForeColor = System.Drawing.Color.Black;
            this.labelX46.Location = new System.Drawing.Point(911, 155);
            this.labelX46.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX46.Name = "labelX46";
            this.labelX46.Size = new System.Drawing.Size(137, 27);
            this.labelX46.TabIndex = 29;
            this.labelX46.Text = "ضریب تجاری آب :";
            // 
            // txtDarsadMaliat
            // 
            this.txtDarsadMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDarsadMaliat.Border.Class = "TextBoxBorder";
            this.txtDarsadMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDarsadMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtDarsadMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtDarsadMaliat.Location = new System.Drawing.Point(277, 57);
            this.txtDarsadMaliat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtDarsadMaliat.Name = "txtDarsadMaliat";
            this.txtDarsadMaliat.PreventEnterBeep = true;
            this.txtDarsadMaliat.ReadOnly = true;
            this.txtDarsadMaliat.Size = new System.Drawing.Size(111, 28);
            this.txtDarsadMaliat.TabIndex = 27;
            this.txtDarsadMaliat.Text = ".0";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(384, 58);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(106, 27);
            this.labelX10.TabIndex = 26;
            this.labelX10.Text = "درصد مالیات :";
            // 
            // txt_doreh_code
            // 
            this.txt_doreh_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_doreh_code.Border.Class = "TextBoxBorder";
            this.txt_doreh_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_doreh_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_doreh_code.ForeColor = System.Drawing.Color.Black;
            this.txt_doreh_code.Location = new System.Drawing.Point(757, 58);
            this.txt_doreh_code.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_doreh_code.Name = "txt_doreh_code";
            this.txt_doreh_code.PreventEnterBeep = true;
            this.txt_doreh_code.ReadOnly = true;
            this.txt_doreh_code.Size = new System.Drawing.Size(182, 28);
            this.txt_doreh_code.TabIndex = 23;
            // 
            // txt_mohlat
            // 
            this.txt_mohlat.Location = new System.Drawing.Point(115, 94);
            this.txt_mohlat.Name = "txt_mohlat";
            this.txt_mohlat.Size = new System.Drawing.Size(182, 24);
            this.txt_mohlat.TabIndex = 22;
            this.txt_mohlat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(307, 93);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(92, 27);
            this.labelX5.TabIndex = 21;
            this.labelX5.Text = "مهلت پرداخت:";
            // 
            // txt_mablagh
            // 
            this.txt_mablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_mablagh.Border.Class = "TextBoxBorder";
            this.txt_mablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_mablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_mablagh.ForeColor = System.Drawing.Color.Black;
            this.txt_mablagh.Location = new System.Drawing.Point(524, 57);
            this.txt_mablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_mablagh.Name = "txt_mablagh";
            this.txt_mablagh.PreventEnterBeep = true;
            this.txt_mablagh.ReadOnly = true;
            this.txt_mablagh.Size = new System.Drawing.Size(111, 28);
            this.txt_mablagh.TabIndex = 20;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(662, 58);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 27);
            this.labelX4.TabIndex = 19;
            this.labelX4.Text = "آب بها :";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(453, 94);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(182, 24);
            this.txt_end.TabIndex = 17;
            this.txt_end.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(757, 94);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(182, 24);
            this.txt_start.TabIndex = 16;
            this.txt_start.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(645, 93);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(92, 27);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "تاریخ پایان :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(962, 93);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(92, 27);
            this.labelX2.TabIndex = 14;
            this.labelX2.Text = "تاریخ شروع :";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(979, 59);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 27);
            this.labelX1.TabIndex = 13;
            this.labelX1.Text = "کد دوره :";
            // 
            // btn_set_parameter
            // 
            this.btn_set_parameter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_set_parameter.BackColor = System.Drawing.Color.Transparent;
            this.btn_set_parameter.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_set_parameter.Location = new System.Drawing.Point(4, 3);
            this.btn_set_parameter.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_set_parameter.Name = "btn_set_parameter";
            this.btn_set_parameter.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_set_parameter.Size = new System.Drawing.Size(43, 27);
            this.btn_set_parameter.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_set_parameter.Symbol = "";
            this.btn_set_parameter.SymbolColor = System.Drawing.Color.Green;
            this.btn_set_parameter.SymbolSize = 10F;
            this.btn_set_parameter.TabIndex = 11;
            this.btn_set_parameter.Click += new System.EventHandler(this.btn_set_parameter_Click);
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(110, 346);
            this.btn_create_new_doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(238, 60);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 10;
            this.btn_create_new_doreh.Text = "ایجاد دوره جدید";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // TabNewDoreh
            // 
            this.TabNewDoreh.AttachedControl = this.superTabControlPanel1;
            this.TabNewDoreh.GlobalItem = false;
            this.TabNewDoreh.Name = "TabNewDoreh";
            this.TabNewDoreh.Text = "تعریف دوره جدید";
            // 
            // superTabControlPanel2
            // 
            this.superTabControlPanel2.Controls.Add(this.groupPanel2);
            this.superTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel2.Location = new System.Drawing.Point(0, 0);
            this.superTabControlPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel2.Name = "superTabControlPanel2";
            this.superTabControlPanel2.Size = new System.Drawing.Size(1183, 577);
            this.superTabControlPanel2.TabIndex = 0;
            this.superTabControlPanel2.TabItem = this.superTabItem2;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btnChapGheraat);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.btnShowDetails);
            this.groupPanel2.Controls.Add(this.buttonX11);
            this.groupPanel2.Controls.Add(this.dgv_doreh);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(20, 20);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1140, 500);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "کلیه دوره های آب";
            // 
            // btnChapGheraat
            // 
            this.btnChapGheraat.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChapGheraat.BackColor = System.Drawing.Color.Transparent;
            this.btnChapGheraat.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnChapGheraat.Location = new System.Drawing.Point(851, 441);
            this.btnChapGheraat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnChapGheraat.Name = "btnChapGheraat";
            this.btnChapGheraat.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnChapGheraat.Size = new System.Drawing.Size(252, 27);
            this.btnChapGheraat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnChapGheraat.Symbol = "";
            this.btnChapGheraat.SymbolColor = System.Drawing.Color.Teal;
            this.btnChapGheraat.SymbolSize = 12F;
            this.btnChapGheraat.TabIndex = 87;
            this.btnChapGheraat.Text = "چاپ فرم قرائت ها";
            this.btnChapGheraat.Click += new System.EventHandler(this.btnChapGheraat_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(591, 408);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX3.Size = new System.Drawing.Size(252, 27);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX3.SymbolSize = 12F;
            this.buttonX3.TabIndex = 86;
            this.buttonX3.Text = "ویرایش اطلاعات دوره";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_2);
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowDetails.BackColor = System.Drawing.Color.Transparent;
            this.btnShowDetails.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowDetails.Location = new System.Drawing.Point(851, 408);
            this.btnShowDetails.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowDetails.Size = new System.Drawing.Size(252, 27);
            this.btnShowDetails.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowDetails.Symbol = "";
            this.btnShowDetails.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowDetails.SymbolSize = 12F;
            this.btnShowDetails.TabIndex = 85;
            this.btnShowDetails.Text = "نمایش جزئیات دوره";
            this.btnShowDetails.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // buttonX11
            // 
            this.buttonX11.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX11.BackColor = System.Drawing.Color.Transparent;
            this.buttonX11.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX11.Location = new System.Drawing.Point(4, 422);
            this.buttonX11.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX11.Name = "buttonX11";
            this.buttonX11.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX11.Size = new System.Drawing.Size(237, 27);
            this.buttonX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX11.Symbol = "";
            this.buttonX11.SymbolColor = System.Drawing.Color.DarkRed;
            this.buttonX11.SymbolSize = 12F;
            this.buttonX11.TabIndex = 84;
            this.buttonX11.Text = "حذف دوره انتخاب شده";
            this.buttonX11.Click += new System.EventHandler(this.buttonX11_Click);
            // 
            // dgv_doreh
            // 
            this.dgv_doreh.AllowUserToAddRows = false;
            this.dgv_doreh.AllowUserToDeleteRows = false;
            this.dgv_doreh.AllowUserToResizeColumns = false;
            this.dgv_doreh.AllowUserToResizeRows = false;
            this.dgv_doreh.AutoGenerateColumns = false;
            this.dgv_doreh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_doreh.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_doreh.ColumnHeadersHeight = 30;
            this.dgv_doreh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dcodeDataGridViewTextBoxColumn,
            this.starttimeDataGridViewTextBoxColumn,
            this.endtimeDataGridViewTextBoxColumn,
            this.mohlatpardakhtDataGridViewTextBoxColumn,
            this.mablaghabDataGridViewTextBoxColumn,
            this.darsadmaliatDataGridViewTextBoxColumn,
            this.ZaribTejari,
            this.ZaribSakhtoSaz,
            this.zarib_aboonman,
            this.mablagh_fazelab,
            this.masraf_omoomi});
            this.dgv_doreh.DataSource = this.abdorehBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_doreh.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_doreh.EnableHeadersVisualStyles = false;
            this.dgv_doreh.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_doreh.Location = new System.Drawing.Point(16, 14);
            this.dgv_doreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_doreh.Name = "dgv_doreh";
            this.dgv_doreh.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_doreh.RowHeadersVisible = false;
            this.dgv_doreh.RowTemplate.Height = 50;
            this.dgv_doreh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_doreh.Size = new System.Drawing.Size(1087, 388);
            this.dgv_doreh.TabIndex = 67;
            this.dgv_doreh.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellContentClick);
            this.dgv_doreh.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellDoubleClick);
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 60F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "کد";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // starttimeDataGridViewTextBoxColumn
            // 
            this.starttimeDataGridViewTextBoxColumn.DataPropertyName = "start_time";
            this.starttimeDataGridViewTextBoxColumn.HeaderText = "تاریخ شروع";
            this.starttimeDataGridViewTextBoxColumn.Name = "starttimeDataGridViewTextBoxColumn";
            this.starttimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.starttimeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.starttimeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // endtimeDataGridViewTextBoxColumn
            // 
            this.endtimeDataGridViewTextBoxColumn.DataPropertyName = "end_time";
            this.endtimeDataGridViewTextBoxColumn.HeaderText = "تاریخ پایان";
            this.endtimeDataGridViewTextBoxColumn.Name = "endtimeDataGridViewTextBoxColumn";
            this.endtimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.endtimeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.endtimeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // mohlatpardakhtDataGridViewTextBoxColumn
            // 
            this.mohlatpardakhtDataGridViewTextBoxColumn.DataPropertyName = "mohlat_pardakht";
            this.mohlatpardakhtDataGridViewTextBoxColumn.HeaderText = "مهلت پرداخت";
            this.mohlatpardakhtDataGridViewTextBoxColumn.Name = "mohlatpardakhtDataGridViewTextBoxColumn";
            this.mohlatpardakhtDataGridViewTextBoxColumn.ReadOnly = true;
            this.mohlatpardakhtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.mohlatpardakhtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // mablaghabDataGridViewTextBoxColumn
            // 
            this.mablaghabDataGridViewTextBoxColumn.DataPropertyName = "mablagh_ab";
            this.mablaghabDataGridViewTextBoxColumn.FillWeight = 60F;
            this.mablaghabDataGridViewTextBoxColumn.HeaderText = "آب بها";
            this.mablaghabDataGridViewTextBoxColumn.Name = "mablaghabDataGridViewTextBoxColumn";
            this.mablaghabDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // darsadmaliatDataGridViewTextBoxColumn
            // 
            this.darsadmaliatDataGridViewTextBoxColumn.DataPropertyName = "darsad_maliat";
            this.darsadmaliatDataGridViewTextBoxColumn.FillWeight = 60F;
            this.darsadmaliatDataGridViewTextBoxColumn.HeaderText = "درصدمالیات";
            this.darsadmaliatDataGridViewTextBoxColumn.Name = "darsadmaliatDataGridViewTextBoxColumn";
            this.darsadmaliatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ZaribTejari
            // 
            this.ZaribTejari.DataPropertyName = "ZaribTejari";
            this.ZaribTejari.FillWeight = 60F;
            this.ZaribTejari.HeaderText = "ضریب تجاری";
            this.ZaribTejari.Name = "ZaribTejari";
            this.ZaribTejari.ReadOnly = true;
            // 
            // ZaribSakhtoSaz
            // 
            this.ZaribSakhtoSaz.DataPropertyName = "ZaribSakhtoSaz";
            this.ZaribSakhtoSaz.FillWeight = 90F;
            this.ZaribSakhtoSaz.HeaderText = "ضریب ساخت و ساز";
            this.ZaribSakhtoSaz.Name = "ZaribSakhtoSaz";
            this.ZaribSakhtoSaz.ReadOnly = true;
            // 
            // zarib_aboonman
            // 
            this.zarib_aboonman.DataPropertyName = "zarib_aboonman";
            this.zarib_aboonman.HeaderText = "ضریب آبونمان";
            this.zarib_aboonman.Name = "zarib_aboonman";
            this.zarib_aboonman.ReadOnly = true;
            // 
            // mablagh_fazelab
            // 
            this.mablagh_fazelab.DataPropertyName = "mablagh_fazelab";
            this.mablagh_fazelab.HeaderText = "مبلغ فاضلاب";
            this.mablagh_fazelab.Name = "mablagh_fazelab";
            this.mablagh_fazelab.ReadOnly = true;
            // 
            // masraf_omoomi
            // 
            this.masraf_omoomi.DataPropertyName = "masraf_omoomi";
            this.masraf_omoomi.HeaderText = "مصرف عمومی";
            this.masraf_omoomi.Name = "masraf_omoomi";
            this.masraf_omoomi.ReadOnly = true;
            // 
            // abdorehBindingSource
            // 
            this.abdorehBindingSource.DataMember = "ab_doreh";
            this.abdorehBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabItem2
            // 
            this.superTabItem2.AttachedControl = this.superTabControlPanel2;
            this.superTabItem2.GlobalItem = false;
            this.superTabItem2.Name = "superTabItem2";
            this.superTabItem2.Text = "مدیریت دوره ها";
            // 
            // ab_dorehTableAdapter
            // 
            this.ab_dorehTableAdapter.ClearBeforeFill = true;
            // 
            // Frm_ab_management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 647);
            this.Controls.Add(this.lblAlert);
            this.Controls.Add(this.superTabControl1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_management";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAbDoreh_FormClosed);
            this.Load += new System.EventHandler(this.FrmAbDoreh_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAbDoreh_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel3.ResumeLayout(false);
            this.grp_arzesh_afzoodeh.ResumeLayout(false);
            this.grpStat.ResumeLayout(false);
            this.grpdelete.ResumeLayout(false);
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.grpKontorkhan.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.superTabControlPanel2.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abdorehBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_doreh_code;
        private FarsiLibrary.Win.Controls.FADatePicker txt_mohlat;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_mablagh;
        private DevComponents.DotNetBar.LabelX labelX4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_end;
        private FarsiLibrary.Win.Controls.FADatePicker txt_start;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btn_set_parameter;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private DevComponents.DotNetBar.SuperTabItem TabNewDoreh;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.LabelX labelX27;
        private DevComponents.DotNetBar.LabelX labelX28;
        private DevComponents.DotNetBar.LabelX lbl_mohlatPardakht;
        private DevComponents.DotNetBar.LabelX labelX25;
        private DevComponents.DotNetBar.LabelX labelX26;
        private DevComponents.DotNetBar.LabelX lbl_ab_mablagh;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX labelX23;
        private DevComponents.DotNetBar.LabelX lblSelectedDoreh;
        private DevComponents.DotNetBar.Controls.GroupPanel grpKontorkhan;
        private DevComponents.DotNetBar.ButtonX btnSodoorGhabzForAllMoshtarekin;
        private DevComponents.DotNetBar.SuperTabItem tabdorehdetails;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.SuperTabItem superTabItem2;
        private FloatTextBox txtDarsadMaliat;
        private DevComponents.DotNetBar.LabelX labelX10;


        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_doreh;
        private FloatTextBox txtZaribSakhtoSaz;
        private DevComponents.DotNetBar.LabelX labelX44;
        private DevComponents.DotNetBar.LabelX labelX45;
        private FloatTextBox txttejarizarib;
        private DevComponents.DotNetBar.LabelX labelX46;
        private AbDataset abDataset;
        private AbDatasetTableAdapters.ab_dorehTableAdapter ab_dorehTableAdapter;
        private System.Windows.Forms.BindingSource abdorehBindingSource;
        private DevComponents.DotNetBar.LabelX lblDorehInfotext;
        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private DevComponents.DotNetBar.ButtonX btnReadKontorKhanAndUpdate;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX lblDorehInfotext2;
        private DevComponents.DotNetBar.LabelX lblSecondStep;
        private DevComponents.DotNetBar.LabelX lblFirstStep;
        private DevComponents.DotNetBar.ButtonX btnShowGhobooz;
        private DevComponents.DotNetBar.Controls.GroupPanel grpdelete;
        private DevComponents.DotNetBar.ButtonX buttonX5;
        private DevComponents.DotNetBar.LabelX lblAlert;
        private DevComponents.DotNetBar.Controls.GroupPanel grpStat;
        private DevComponents.DotNetBar.ButtonX buttonX8;
        private DevComponents.DotNetBar.ButtonX buttonX7;
        private DevComponents.DotNetBar.ButtonX buttonX9;
        private DevComponents.DotNetBar.LabelX lbl_dar_hal_barrrasi;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.ButtonX btnShowDetails;
        private DevComponents.DotNetBar.ButtonX buttonX11;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.ButtonX btnChap;
        private DevComponents.DotNetBar.LabelX lblInfoAboutMoshahedeh;
        private DevComponents.DotNetBar.ButtonX btnChapGheraat;
        private DevComponents.DotNetBar.LabelX labelX7;
        private FloatTextBox txt_fazelab_mablagh;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX8;
        private FloatTextBox txt_aboonman;
        private DevComponents.DotNetBar.LabelX labelX9;
        private FloatTextBox txt_masraf_omoomi;
        private DevComponents.DotNetBar.LabelX labelX13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn starttimeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn endtimeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn mohlatpardakhtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghabDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn darsadmaliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZaribTejari;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZaribSakhtoSaz;
        private System.Windows.Forms.DataGridViewTextBoxColumn zarib_aboonman;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablagh_fazelab;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf_omoomi;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.Controls.GroupPanel grp_arzesh_afzoodeh;
        private DevComponents.DotNetBar.ButtonX buttonX6;
        private DevComponents.DotNetBar.ButtonX btn_gozesh_karbodi_jame_sharj;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.ButtonX buttonX12;
        private FloatTextBox hazineh_taviz_kontor;
        private DevComponents.DotNetBar.LabelX labelX15;
    }
}