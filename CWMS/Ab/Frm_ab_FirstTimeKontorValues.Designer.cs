﻿namespace CWMS.Ab
{
    partial class Frm_ab_FirstTimeKontorValues
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnReadKontorKhanAndUpdate = new DevComponents.DotNetBar.ButtonX();
            this.dgv_info = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.btn_find_by_gharardad = new DevComponents.DotNetBar.ButtonX();
            this.txtFindByGhararda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.abbedehibestankarFirsttimeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset = new CWMS.Ab.MainAbDataset();
            this.ab_bedehi_bestankar_First_timeTableAdapter = new CWMS.Ab.MainAbDatasetTableAdapters.ab_bedehi_bestankar_First_timeTableAdapter();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestankarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontorstartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikh_gheraat = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_info)).BeginInit();
            this.grpUpdateGhobooozAb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abbedehibestankarFirsttimeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.btnReadKontorKhanAndUpdate);
            this.groupPanel1.Controls.Add(this.dgv_info);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(16, 118);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(772, 584);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 22;
            this.groupPanel1.Text = "لطفا مقادیر اولیه مربوط به کنتورهای آب مشترکین را در کادر زیر وارد نمایید";
            // 
            // btnReadKontorKhanAndUpdate
            // 
            this.btnReadKontorKhanAndUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReadKontorKhanAndUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnReadKontorKhanAndUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnReadKontorKhanAndUpdate.Location = new System.Drawing.Point(16, 506);
            this.btnReadKontorKhanAndUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnReadKontorKhanAndUpdate.Name = "btnReadKontorKhanAndUpdate";
            this.btnReadKontorKhanAndUpdate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 0, 0, 8);
            this.btnReadKontorKhanAndUpdate.Size = new System.Drawing.Size(380, 28);
            this.btnReadKontorKhanAndUpdate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnReadKontorKhanAndUpdate.Symbol = "";
            this.btnReadKontorKhanAndUpdate.SymbolColor = System.Drawing.Color.Teal;
            this.btnReadKontorKhanAndUpdate.SymbolSize = 12F;
            this.btnReadKontorKhanAndUpdate.TabIndex = 69;
            this.btnReadKontorKhanAndUpdate.Text = "ذخیره کلیه تغییرات و بستن فرم";
            this.btnReadKontorKhanAndUpdate.Click += new System.EventHandler(this.btnReadKontorKhanAndUpdate_Click);
            // 
            // dgv_info
            // 
            this.dgv_info.AllowUserToAddRows = false;
            this.dgv_info.AllowUserToDeleteRows = false;
            this.dgv_info.AllowUserToResizeColumns = false;
            this.dgv_info.AllowUserToResizeRows = false;
            this.dgv_info.AutoGenerateColumns = false;
            this.dgv_info.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_info.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_info.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_info.ColumnHeadersHeight = 30;
            this.dgv_info.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.bedehiDataGridViewTextBoxColumn,
            this.bestankarDataGridViewTextBoxColumn,
            this.kontorstartDataGridViewTextBoxColumn,
            this.tarikh_gheraat});
            this.dgv_info.DataSource = this.abbedehibestankarFirsttimeBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_info.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_info.EnableHeadersVisualStyles = false;
            this.dgv_info.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_info.Location = new System.Drawing.Point(21, 20);
            this.dgv_info.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_info.Name = "dgv_info";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_info.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_info.RowHeadersVisible = false;
            this.dgv_info.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_info.Size = new System.Drawing.Size(723, 475);
            this.dgv_info.TabIndex = 68;
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.White;
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.btn_create_new_doreh);
            this.grpUpdateGhobooozAb.Controls.Add(this.btn_find_by_gharardad);
            this.grpUpdateGhobooozAb.Controls.Add(this.txtFindByGhararda);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX1);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(16, 12);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(772, 95);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 21;
            this.grpUpdateGhobooozAb.Text = "جستجوی اطلاعات مشترکین";
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(34, 19);
            this.btn_create_new_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(311, 28);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 29;
            this.btn_create_new_doreh.Text = "نمایش اطلاعات همه مشترکین";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // btn_find_by_gharardad
            // 
            this.btn_find_by_gharardad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_by_gharardad.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_by_gharardad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_by_gharardad.Location = new System.Drawing.Point(404, 19);
            this.btn_find_by_gharardad.Margin = new System.Windows.Forms.Padding(4);
            this.btn_find_by_gharardad.Name = "btn_find_by_gharardad";
            this.btn_find_by_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find_by_gharardad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_by_gharardad.Size = new System.Drawing.Size(43, 28);
            this.btn_find_by_gharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_by_gharardad.Symbol = "";
            this.btn_find_by_gharardad.SymbolColor = System.Drawing.Color.Teal;
            this.btn_find_by_gharardad.SymbolSize = 9F;
            this.btn_find_by_gharardad.TabIndex = 28;
            this.btn_find_by_gharardad.Click += new System.EventHandler(this.btn_find_by_gharardad_Click);
            // 
            // txtFindByGhararda
            // 
            this.txtFindByGhararda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFindByGhararda.Border.Class = "TextBoxBorder";
            this.txtFindByGhararda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFindByGhararda.DisabledBackColor = System.Drawing.Color.White;
            this.txtFindByGhararda.ForeColor = System.Drawing.Color.Black;
            this.txtFindByGhararda.Location = new System.Drawing.Point(465, 20);
            this.txtFindByGhararda.Margin = new System.Windows.Forms.Padding(4);
            this.txtFindByGhararda.Name = "txtFindByGhararda";
            this.txtFindByGhararda.PreventEnterBeep = true;
            this.txtFindByGhararda.Size = new System.Drawing.Size(91, 26);
            this.txtFindByGhararda.TabIndex = 25;
            this.txtFindByGhararda.TextChanged += new System.EventHandler(this.txtFindByGhararda_TextChanged);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(509, 19);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(183, 28);
            this.labelX1.TabIndex = 24;
            this.labelX1.Text = "قرارداد مشترک :";
            // 
            // abbedehibestankarFirsttimeBindingSource
            // 
            this.abbedehibestankarFirsttimeBindingSource.DataMember = "ab_bedehi_bestankar_First_time";
            this.abbedehibestankarFirsttimeBindingSource.DataSource = this.mainAbDataset;
            // 
            // mainAbDataset
            // 
            this.mainAbDataset.DataSetName = "MainAbDataset";
            this.mainAbDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ab_bedehi_bestankar_First_timeTableAdapter
            // 
            this.ab_bedehi_bestankar_First_timeTableAdapter.ClearBeforeFill = true;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            // 
            // bedehiDataGridViewTextBoxColumn
            // 
            this.bedehiDataGridViewTextBoxColumn.DataPropertyName = "bedehi";
            this.bedehiDataGridViewTextBoxColumn.HeaderText = "بدهی";
            this.bedehiDataGridViewTextBoxColumn.Name = "bedehiDataGridViewTextBoxColumn";
            // 
            // bestankarDataGridViewTextBoxColumn
            // 
            this.bestankarDataGridViewTextBoxColumn.DataPropertyName = "bestankar";
            this.bestankarDataGridViewTextBoxColumn.HeaderText = "بستانکار";
            this.bestankarDataGridViewTextBoxColumn.Name = "bestankarDataGridViewTextBoxColumn";
            // 
            // kontorstartDataGridViewTextBoxColumn
            // 
            this.kontorstartDataGridViewTextBoxColumn.DataPropertyName = "kontor_start";
            this.kontorstartDataGridViewTextBoxColumn.HeaderText = "شماره کنتور";
            this.kontorstartDataGridViewTextBoxColumn.Name = "kontorstartDataGridViewTextBoxColumn";
            // 
            // tarikh_gheraat
            // 
            this.tarikh_gheraat.DataPropertyName = "tarikh_gheraat";
            this.tarikh_gheraat.HeaderText = "تاریخ قرائت";
            this.tarikh_gheraat.Name = "tarikh_gheraat";
            this.tarikh_gheraat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikh_gheraat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Frm_ab_FirstTimeKontorValues
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 717);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.grpUpdateGhobooozAb);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_FirstTimeKontorValues";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmFirstTimeKontorValues_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmFirstTimeKontorValues_KeyDown);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_info)).EndInit();
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.abbedehibestankarFirsttimeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btnReadKontorKhanAndUpdate;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_info;
        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private DevComponents.DotNetBar.ButtonX btn_find_by_gharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFindByGhararda;
        private DevComponents.DotNetBar.LabelX labelX1;
        private MainAbDataset mainAbDataset;
        private System.Windows.Forms.BindingSource abbedehibestankarFirsttimeBindingSource;
        private MainAbDatasetTableAdapters.ab_bedehi_bestankar_First_timeTableAdapter ab_bedehi_bestankar_First_timeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedehiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestankarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontorstartDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikh_gheraat;
    }
}