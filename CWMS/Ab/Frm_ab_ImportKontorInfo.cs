﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
using System.Data.SqlClient;
using System.Diagnostics;
using Newtonsoft.Json;
namespace CWMS.Ab
{
    public partial class Frm_ab_ImportKontorInfo : MyMetroForm
    {
        int dcode = 0;
        public Frm_ab_ImportKontorInfo(int code)
        {
            InitializeComponent();
            dcode = code;
            //grpdasti.Text = "ثبت اطلاعات کنتورهای مربوط به دوره " + dcode;

        }

        public Frm_ab_ImportKontorInfo(string code)
        {
            InitializeComponent();
            dcode = Convert.ToInt32(code);
            //grpdasti.Text = "ثبت اطلاعات کنتورهای مربوط به دوره " + dcode;

        }

        public Frm_ab_ImportKontorInfo()
        {
            InitializeComponent();


        }




        private void FrmImportKontorInfo_Load(object sender, EventArgs e)
        {
            //if (dcode <= 1)
            //{
            //    DataTable dt_moshtarek = Classes.ClsMain.GetDataTable("select gharardad,kontor_start from kontor_first_time_values");
            //    for (int i = 0; i < dt_moshtarek.Rows.Count; i++)
            //    {
            //        dgv_moshtarek.Rows.Add(dt_moshtarek.Rows[i]["gharardad"].ToString(), dt_moshtarek.Rows[i]["kontor_start"].ToString(), 0, false, false, false, false, false, false);
            //    }
            //}
            //else
            //{
            //    DataTable dt_moshtarek = Classes.ClsMain.GetDataTable("SELECT moshtarekin.gharardad, moshtarekin.co_name, ab_ghabz.kontor_end FROM  ab_ghabz INNER JOIN moshtarekin ON ab_ghabz.gharardad = moshtarekin.gharardad where dcode=" + (dcode-1).ToString());
            //    for (int i = 0; i < dt_moshtarek.Rows.Count; i++)
            //    {
            //        dgv_moshtarek.Rows.Add(dt_moshtarek.Rows[i]["gharardad"].ToString(), dt_moshtarek.Rows[i]["co_name"].ToString(), dt_moshtarek.Rows[i]["kontor_end"].ToString(), 0, false, false, false, false, false, false);
            //    }

            //}







        }

        private void buttonX2_Click(object sender, EventArgs e)
        {

            DataTable dt_kontorKahan = Classes.ClsMain.GetDataTable("select * from kontor_khan");




            txtGharardadCnt.Text = dt_kontorKahan.Rows.Count.ToString();
            if (dt_kontorKahan.Rows.Count != 0)
            {
                txtDorKamel.Text = dt_kontorKahan.AsEnumerable().Where(p => p.Field<bool>("dor_kamel")).Count().ToString();
                txtAdamGheraat.Text = dt_kontorKahan.AsEnumerable().Where(p => p.Field<bool>("adam_gheraat")).Count().ToString();
                txtGhat.Text = dt_kontorKahan.AsEnumerable().Where(p => p.Field<bool>("ghat")).Count().ToString();
                txtKharab.Text = dt_kontorKahan.AsEnumerable().Where(p => p.Field<bool>("kharab")).Count().ToString();
                txtTaviz.Text = dt_kontorKahan.AsEnumerable().Where(p => p.Field<bool>("taviz")).Count().ToString();

            }

        }

        private void buttonX6_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select count(*) from kontor_khan")) == 0)
                Payam.Show("متاسفانه اطلاعات در سیستم ذخیره نشده است .علت را بررسی نمایید");
            else
            {
                Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شده است ");
                this.Close();
            }
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            new Ab.Frm_ab_CheckGheraat().Show();

        }
        string GetValidBoolean(string str)
        {
            if (str.ToLower() == "false")
                return "0";
            return "1";
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void FrmImportKontorInfo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            new Ab.Frm_ab_CheckGheraat().ShowDialog();
        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            new Ab.Frm_ab_CheckGheraat().ShowDialog();
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            buttonX3.Enabled = false;
            Insert_data_from_android_to_db();
            buttonX3.Enabled = true;

        }
        int changeBoolType(bool k)
        {
            return (k == true ? 1 : 0);
        }

        DateTime make_valid_tarikh_gheraat(string arg)
        {
            if (arg.Trim() == "-")
                return DateTime.Now;
            string part1 = arg.Split(' ')[0];
            string[] tarikh = part1.Split('-');
            return new DateTime(
                Convert.ToInt32(tarikh[2]),
                Convert.ToInt32(tarikh[1]),
                Convert.ToInt32(tarikh[0])
                );
        }

        void Insert_data_from_android_to_db()
        {
            Classes.ClsMain.ChangeCulture("e");
            OpenFileDialog opendiag = new OpenFileDialog();
            bool DO_IT = true;
            if (opendiag.ShowDialog() == DialogResult.OK)
            {
                var list_of_info = JsonConvert.DeserializeObject<dynamic>(System.IO.File.ReadAllText(opendiag.FileName));

                int gharardad_exist = Convert.ToInt32(
                        Classes.ClsMain.ExecuteScalar(
                            "select count(*) from kontor_khan where gharardad=" + list_of_info[0]["gharardad"].ToString()
                            )
                        );
                if (gharardad_exist == 1)
                {
                    if (Cpayam.Show("اطلاعات مربوط به مشترکین موجود در این فایل در دیتابیس وجود دارد. آیا مایل به حذف اطلاعات  و جایگزینی اطلاعات موجود در این فایل هستید؟") == DialogResult.Yes)
                    {
                        string List_of_gharardad_for_delete = "";
                        for (int i = 0; i < list_of_info.Count; i++)
                        {
                            List_of_gharardad_for_delete += list_of_info[i]["gharardad"].ToString() + ",";
                        }
                        List_of_gharardad_for_delete = List_of_gharardad_for_delete.Substring(0, List_of_gharardad_for_delete.Length - 1);
                        if (List_of_gharardad_for_delete.Length != 0)
                            Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan where gharardad in (" + List_of_gharardad_for_delete + ")");

                    }
                    else
                        DO_IT = false;

                }
                if(DO_IT)
                {
                    string tarikh_gheraat = "-";
                    bool dar_hal_sakht = false;
                    Classes.ClsMain.ChangeCulture("e");
                    for (int i = 0; i < list_of_info.Count; i++)
                    {
                        // یادت باشه که حتما مقدار در حال ساخت را اضافه کنیم به برنامه چون الان وجود نداره وبه جایش عدم قرائت گذاشته می شه
                        try
                        {
                            tarikh_gheraat =list_of_info[i]["tarikh_gheraat"].ToString();
                        }
                        catch
                        {
                            tarikh_gheraat = "-";
                        }
                        try
                        {
                            dar_hal_sakht = Convert.ToBoolean(list_of_info[i]["dar_hal_sakht"]);
                        }
                        catch (Exception)
                        {

                            dar_hal_sakht = false;
                        }


                        Classes.ClsMain.ExecuteNoneQuery(
                            "insert into kontor_khan values (" +
                            list_of_info[i]["gharardad"].ToString() + ",'" + make_valid_tarikh_gheraat(tarikh_gheraat) + "'," + list_of_info[i]["kontor_feli"].ToString() + "," +
                            changeBoolType(Convert.ToBoolean(list_of_info[i]["adam_gheraat"])) + "," +
                            changeBoolType(dar_hal_sakht) + "," +
                            changeBoolType(Convert.ToBoolean(list_of_info[i]["kharab"])) + "," +
                            changeBoolType(Convert.ToBoolean(list_of_info[i]["ghat"])) + "," +
                            changeBoolType(Convert.ToBoolean(list_of_info[i]["dor_kamel"])) + "," +
                            changeBoolType(Convert.ToBoolean(list_of_info[i]["taviz"]))

                            + ")"
                            );
                    }
                    Payam.Show("کلیه اطلاعات در سامانه با موفقیت ثبت شدند");
                }
             



            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if(Cpayam.Show("آیا با حذف اطلاعات موافق هستید")==DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan");
                Payam.Show("کلیه اطلاعات مربوط به کنتورها با موفقیت از سیستم حذف شدند");
            }
        }
    }
}