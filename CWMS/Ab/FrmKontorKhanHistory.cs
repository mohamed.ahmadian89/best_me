﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class FrmKontorKhanHistory : MyMetroForm
    {
        public FrmKontorKhanHistory()
        {
            InitializeComponent();
        }

        private void FrmKontorKhanHistory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.kontor_khan_history' table. You can move, or remove it, as needed.
            cmbdoreh.DataSource = Classes.ClsMain.GetDataTable("select distinct(dcode) from kontor_khan_history order by dcode desc");
            cmbdoreh.DisplayMember = cmbdoreh.ValueMember = "dcode";
        }

        private void txtFindByGhararda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

                buttonX1.PerformClick();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text.Trim() != "")
            {
                kontorkhanhistoryBindingSource.Filter = "gharardad=" + txtFindByGhararda.Text;
            }
            else
                kontorkhanhistoryBindingSource.RemoveFilter();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {

            this.kontor_khan_historyTableAdapter.Fill(this.mainDataSest.kontor_khan_history);
            kontorkhanhistoryBindingSource.RemoveFilter();
        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            this.kontor_khan_historyTableAdapter.FillByDcode(this.mainDataSest.kontor_khan_history, Convert.ToInt32(cmbdoreh.SelectedValue.ToString()));

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {

            string dcode = Classes.clsDoreh.Get_last_Ab_dcode().ToString();
            if (cmbdoreh.Text== dcode)
            {
                if (Cpayam.Show("ایا مطئن هستید که قرائت های دوره " + dcode + " به عنوان قر ائت های دوره فعلی شما ثبت شود") == DialogResult.Yes)
                {
                    Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan;insert into kontor_khan select  [gharardad],[tarikh],[shomareh_kontor],[adam_gheraat],[dar_hal_sakht],[kharab],[ghat],[dor_kamel],[taviz] from kontor_khan_history where dcode=" + dcode);
                    Payam.Show("قرائت ها با موفقیت در سیستم ذخیره شدند");
                }
            }
            else
                Payam.Show(" متاسفانه برای دوره " + dcode + " اطلاعاتی در جدول پشتیبان کنتورخوان وجود ندارد");
        }
    }
}