﻿namespace CWMS.Ab
{
    partial class Frm_ab_CheckGheraat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ab_CheckGheraat));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_create_new_doreh = new DevComponents.DotNetBar.ButtonX();
            this.btn_find_by_gharardad = new DevComponents.DotNetBar.ButtonX();
            this.txtFindByGhararda = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.grpDoreh = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LblDoreh = new DevComponents.DotNetBar.LabelX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.btnReadKontorKhanAndUpdate = new DevComponents.DotNetBar.ButtonX();
            this.dgv_doreh = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.kontorkhanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset = new CWMS.Ab.MainAbDataset();
            this.kontor_khanTableAdapter = new CWMS.Ab.MainAbDatasetTableAdapters.kontor_khanTableAdapter();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.grpShouldAdd = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.dgvShouldAdd = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shomarehkontorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adamgheraatDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.darhalsakhtDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.kharabDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ghatDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dorkamelDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tavizDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.grp_last = new System.Windows.Forms.GroupBox();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txt_tarikh_gheraat_Felli = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtlast = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lbllast = new DevComponents.DotNetBar.LabelX();
            this.txt_last_date_gheraat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.grpDoreh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kontorkhanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).BeginInit();
            this.grpShouldAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShouldAdd)).BeginInit();
            this.grp_last.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_create_new_doreh
            // 
            this.btn_create_new_doreh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_doreh.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_doreh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_doreh.Location = new System.Drawing.Point(43, 4);
            this.btn_create_new_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_create_new_doreh.Name = "btn_create_new_doreh";
            this.btn_create_new_doreh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_doreh.Size = new System.Drawing.Size(272, 25);
            this.btn_create_new_doreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_doreh.Symbol = "";
            this.btn_create_new_doreh.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_doreh.SymbolSize = 10F;
            this.btn_create_new_doreh.TabIndex = 29;
            this.btn_create_new_doreh.Text = "نمایش اطلاعات همه مشترکین";
            this.btn_create_new_doreh.Click += new System.EventHandler(this.btn_create_new_doreh_Click);
            // 
            // btn_find_by_gharardad
            // 
            this.btn_find_by_gharardad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_by_gharardad.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_by_gharardad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_by_gharardad.Location = new System.Drawing.Point(323, 4);
            this.btn_find_by_gharardad.Margin = new System.Windows.Forms.Padding(4);
            this.btn_find_by_gharardad.Name = "btn_find_by_gharardad";
            this.btn_find_by_gharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find_by_gharardad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_by_gharardad.Size = new System.Drawing.Size(38, 25);
            this.btn_find_by_gharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_by_gharardad.Symbol = "";
            this.btn_find_by_gharardad.SymbolColor = System.Drawing.Color.Teal;
            this.btn_find_by_gharardad.SymbolSize = 9F;
            this.btn_find_by_gharardad.TabIndex = 28;
            this.btn_find_by_gharardad.Click += new System.EventHandler(this.btn_find_by_gharardad_Click);
            // 
            // txtFindByGhararda
            // 
            this.txtFindByGhararda.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFindByGhararda.Border.Class = "TextBoxBorder";
            this.txtFindByGhararda.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFindByGhararda.DisabledBackColor = System.Drawing.Color.White;
            this.txtFindByGhararda.ForeColor = System.Drawing.Color.Black;
            this.txtFindByGhararda.Location = new System.Drawing.Point(369, 4);
            this.txtFindByGhararda.Margin = new System.Windows.Forms.Padding(4);
            this.txtFindByGhararda.Name = "txtFindByGhararda";
            this.txtFindByGhararda.PreventEnterBeep = true;
            this.txtFindByGhararda.Size = new System.Drawing.Size(80, 26);
            this.txtFindByGhararda.TabIndex = 25;
            this.txtFindByGhararda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFindByGhararda_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(395, 4);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(130, 25);
            this.labelX1.TabIndex = 24;
            this.labelX1.Text = "قرارداد مشترک :";
            this.labelX1.Click += new System.EventHandler(this.labelX1_Click);
            // 
            // grpDoreh
            // 
            this.grpDoreh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpDoreh.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpDoreh.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpDoreh.Controls.Add(this.grp_last);
            this.grpDoreh.Controls.Add(this.btn_create_new_doreh);
            this.grpDoreh.Controls.Add(this.btn_find_by_gharardad);
            this.grpDoreh.Controls.Add(this.LblDoreh);
            this.grpDoreh.Controls.Add(this.txtFindByGhararda);
            this.grpDoreh.Controls.Add(this.btnSave);
            this.grpDoreh.Controls.Add(this.labelX1);
            this.grpDoreh.Controls.Add(this.buttonX1);
            this.grpDoreh.Controls.Add(this.btnReadKontorKhanAndUpdate);
            this.grpDoreh.Controls.Add(this.dgv_doreh);
            this.grpDoreh.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpDoreh.Location = new System.Drawing.Point(13, 244);
            this.grpDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.grpDoreh.Name = "grpDoreh";
            this.grpDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpDoreh.Size = new System.Drawing.Size(969, 454);
            // 
            // 
            // 
            this.grpDoreh.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpDoreh.Style.BackColorGradientAngle = 90;
            this.grpDoreh.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpDoreh.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDoreh.Style.BorderBottomWidth = 1;
            this.grpDoreh.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpDoreh.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDoreh.Style.BorderLeftWidth = 1;
            this.grpDoreh.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDoreh.Style.BorderRightWidth = 1;
            this.grpDoreh.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpDoreh.Style.BorderTopWidth = 1;
            this.grpDoreh.Style.CornerDiameter = 4;
            this.grpDoreh.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpDoreh.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpDoreh.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpDoreh.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpDoreh.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpDoreh.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpDoreh.TabIndex = 20;
            this.grpDoreh.Text = "قرائت های کلیه مشترکین";
            // 
            // LblDoreh
            // 
            this.LblDoreh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.LblDoreh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblDoreh.Font = new System.Drawing.Font("B Yekan", 12F);
            this.LblDoreh.ForeColor = System.Drawing.Color.Black;
            this.LblDoreh.Location = new System.Drawing.Point(594, 4);
            this.LblDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.LblDoreh.Name = "LblDoreh";
            this.LblDoreh.Size = new System.Drawing.Size(337, 25);
            this.LblDoreh.TabIndex = 73;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(19, 385);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 0, 0, 8);
            this.btnSave.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btnSave.Size = new System.Drawing.Size(197, 35);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.SymbolColor = System.Drawing.Color.Teal;
            this.btnSave.SymbolSize = 12F;
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "ذخیره سازی اطلاعات CTRL+S";
            this.btnSave.Tooltip = "Ctrl + S";
            this.btnSave.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(739, 398);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 0, 0, 8);
            this.buttonX1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX1.Size = new System.Drawing.Size(192, 25);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX1.SymbolSize = 12F;
            this.buttonX1.TabIndex = 70;
            this.buttonX1.Text = "چاپ قرائت ها";
            this.buttonX1.Tooltip = "Ctrl + P";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btnReadKontorKhanAndUpdate
            // 
            this.btnReadKontorKhanAndUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnReadKontorKhanAndUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnReadKontorKhanAndUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnReadKontorKhanAndUpdate.Location = new System.Drawing.Point(540, 398);
            this.btnReadKontorKhanAndUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnReadKontorKhanAndUpdate.Name = "btnReadKontorKhanAndUpdate";
            this.btnReadKontorKhanAndUpdate.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8, 0, 0, 8);
            this.btnReadKontorKhanAndUpdate.Size = new System.Drawing.Size(191, 25);
            this.btnReadKontorKhanAndUpdate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnReadKontorKhanAndUpdate.Symbol = "";
            this.btnReadKontorKhanAndUpdate.SymbolColor = System.Drawing.Color.Teal;
            this.btnReadKontorKhanAndUpdate.SymbolSize = 12F;
            this.btnReadKontorKhanAndUpdate.TabIndex = 69;
            this.btnReadKontorKhanAndUpdate.Text = "بررسی صحت اطلاعات وارد شده";
            this.btnReadKontorKhanAndUpdate.Tooltip = "Ctrl + S";
            this.btnReadKontorKhanAndUpdate.Visible = false;
            this.btnReadKontorKhanAndUpdate.Click += new System.EventHandler(this.btnReadKontorKhanAndUpdate_Click);
            // 
            // dgv_doreh
            // 
            this.dgv_doreh.AllowUserToAddRows = false;
            this.dgv_doreh.AllowUserToDeleteRows = false;
            this.dgv_doreh.AllowUserToResizeColumns = false;
            this.dgv_doreh.AllowUserToResizeRows = false;
            this.dgv_doreh.AutoGenerateColumns = false;
            this.dgv_doreh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_doreh.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_doreh.ColumnHeadersHeight = 30;
            this.dgv_doreh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gharardadDataGridViewTextBoxColumn,
            this.shomarehkontorDataGridViewTextBoxColumn,
            this.adamgheraatDataGridViewCheckBoxColumn,
            this.darhalsakhtDataGridViewCheckBoxColumn,
            this.kharabDataGridViewCheckBoxColumn,
            this.ghatDataGridViewCheckBoxColumn,
            this.dorkamelDataGridViewCheckBoxColumn,
            this.tavizDataGridViewCheckBoxColumn});
            this.dgv_doreh.DataSource = this.kontorkhanBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_doreh.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_doreh.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_doreh.EnableHeadersVisualStyles = false;
            this.dgv_doreh.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgv_doreh.Location = new System.Drawing.Point(19, 37);
            this.dgv_doreh.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_doreh.Name = "dgv_doreh";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_doreh.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_doreh.RowHeadersVisible = false;
            this.dgv_doreh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_doreh.Size = new System.Drawing.Size(922, 262);
            this.dgv_doreh.TabIndex = 68;
            this.dgv_doreh.TabStop = false;
            this.dgv_doreh.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_CellContentClick);
            this.dgv_doreh.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_doreh_DataError);
            this.dgv_doreh.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_doreh_RowEnter);
            this.dgv_doreh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgv_doreh_Scroll);
            this.dgv_doreh.SelectionChanged += new System.EventHandler(this.dgv_doreh_SelectionChanged);
            // 
            // kontorkhanBindingSource
            // 
            this.kontorkhanBindingSource.DataMember = "kontor_khan";
            this.kontorkhanBindingSource.DataSource = this.mainAbDataset;
            // 
            // mainAbDataset
            // 
            this.mainAbDataset.DataSetName = "MainAbDataset";
            this.mainAbDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // kontor_khanTableAdapter
            // 
            this.kontor_khanTableAdapter.ClearBeforeFill = true;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(594, 4);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(347, 166);
            this.labelX3.TabIndex = 30;
            this.labelX3.Text = resources.GetString("labelX3.Text");
            this.labelX3.WordWrap = true;
            // 
            // grpShouldAdd
            // 
            this.grpShouldAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpShouldAdd.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.grpShouldAdd.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpShouldAdd.Controls.Add(this.buttonX3);
            this.grpShouldAdd.Controls.Add(this.dgvShouldAdd);
            this.grpShouldAdd.Controls.Add(this.labelX3);
            this.grpShouldAdd.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpShouldAdd.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grpShouldAdd.Location = new System.Drawing.Point(13, 13);
            this.grpShouldAdd.Margin = new System.Windows.Forms.Padding(4);
            this.grpShouldAdd.Name = "grpShouldAdd";
            this.grpShouldAdd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpShouldAdd.Size = new System.Drawing.Size(966, 230);
            // 
            // 
            // 
            this.grpShouldAdd.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpShouldAdd.Style.BackColorGradientAngle = 90;
            this.grpShouldAdd.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpShouldAdd.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpShouldAdd.Style.BorderBottomWidth = 1;
            this.grpShouldAdd.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpShouldAdd.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpShouldAdd.Style.BorderLeftWidth = 1;
            this.grpShouldAdd.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpShouldAdd.Style.BorderRightWidth = 1;
            this.grpShouldAdd.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpShouldAdd.Style.BorderTopWidth = 1;
            this.grpShouldAdd.Style.CornerDiameter = 4;
            this.grpShouldAdd.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpShouldAdd.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpShouldAdd.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpShouldAdd.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpShouldAdd.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpShouldAdd.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpShouldAdd.TabIndex = 31;
            this.grpShouldAdd.Text = "لیست مشترکین اضافه شده به صورت خودکار باتوجه به تنظیمات سیستم";
            this.grpShouldAdd.Click += new System.EventHandler(this.groupPanel3_Click);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(43, 174);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(272, 25);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 10F;
            this.buttonX3.TabIndex = 32;
            this.buttonX3.Text = "افزودن مقدار کنتورخوان";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click_2);
            // 
            // dgvShouldAdd
            // 
            this.dgvShouldAdd.AllowUserToAddRows = false;
            this.dgvShouldAdd.AllowUserToDeleteRows = false;
            this.dgvShouldAdd.AllowUserToResizeColumns = false;
            this.dgvShouldAdd.AllowUserToResizeRows = false;
            this.dgvShouldAdd.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShouldAdd.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShouldAdd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvShouldAdd.ColumnHeadersHeight = 30;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShouldAdd.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvShouldAdd.EnableHeadersVisualStyles = false;
            this.dgvShouldAdd.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.dgvShouldAdd.Location = new System.Drawing.Point(43, 4);
            this.dgvShouldAdd.Margin = new System.Windows.Forms.Padding(4);
            this.dgvShouldAdd.Name = "dgvShouldAdd";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShouldAdd.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvShouldAdd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShouldAdd.Size = new System.Drawing.Size(543, 162);
            this.dgvShouldAdd.TabIndex = 69;
            this.dgvShouldAdd.TabStop = false;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shomarehkontorDataGridViewTextBoxColumn
            // 
            this.shomarehkontorDataGridViewTextBoxColumn.DataPropertyName = "shomareh_kontor";
            this.shomarehkontorDataGridViewTextBoxColumn.HeaderText = "قرائت جدید";
            this.shomarehkontorDataGridViewTextBoxColumn.Name = "shomarehkontorDataGridViewTextBoxColumn";
            // 
            // adamgheraatDataGridViewCheckBoxColumn
            // 
            this.adamgheraatDataGridViewCheckBoxColumn.DataPropertyName = "adam_gheraat";
            this.adamgheraatDataGridViewCheckBoxColumn.HeaderText = "عدم قرائت";
            this.adamgheraatDataGridViewCheckBoxColumn.Name = "adamgheraatDataGridViewCheckBoxColumn";
            // 
            // darhalsakhtDataGridViewCheckBoxColumn
            // 
            this.darhalsakhtDataGridViewCheckBoxColumn.DataPropertyName = "dar_hal_sakht";
            this.darhalsakhtDataGridViewCheckBoxColumn.HeaderText = "در حال ساخت";
            this.darhalsakhtDataGridViewCheckBoxColumn.Name = "darhalsakhtDataGridViewCheckBoxColumn";
            // 
            // kharabDataGridViewCheckBoxColumn
            // 
            this.kharabDataGridViewCheckBoxColumn.DataPropertyName = "kharab";
            this.kharabDataGridViewCheckBoxColumn.HeaderText = "خراب";
            this.kharabDataGridViewCheckBoxColumn.Name = "kharabDataGridViewCheckBoxColumn";
            // 
            // ghatDataGridViewCheckBoxColumn
            // 
            this.ghatDataGridViewCheckBoxColumn.DataPropertyName = "ghat";
            this.ghatDataGridViewCheckBoxColumn.HeaderText = "قطع";
            this.ghatDataGridViewCheckBoxColumn.Name = "ghatDataGridViewCheckBoxColumn";
            // 
            // dorkamelDataGridViewCheckBoxColumn
            // 
            this.dorkamelDataGridViewCheckBoxColumn.DataPropertyName = "dor_kamel";
            this.dorkamelDataGridViewCheckBoxColumn.HeaderText = "دورکامل";
            this.dorkamelDataGridViewCheckBoxColumn.Name = "dorkamelDataGridViewCheckBoxColumn";
            // 
            // tavizDataGridViewCheckBoxColumn
            // 
            this.tavizDataGridViewCheckBoxColumn.DataPropertyName = "taviz";
            this.tavizDataGridViewCheckBoxColumn.HeaderText = "تعویض";
            this.tavizDataGridViewCheckBoxColumn.Name = "tavizDataGridViewCheckBoxColumn";
            // 
            // grp_last
            // 
            this.grp_last.Controls.Add(this.txt_last_date_gheraat);
            this.grp_last.Controls.Add(this.labelX4);
            this.grp_last.Controls.Add(this.txt_tarikh_gheraat_Felli);
            this.grp_last.Controls.Add(this.labelX2);
            this.grp_last.Controls.Add(this.txtlast);
            this.grp_last.Controls.Add(this.lbllast);
            this.grp_last.Location = new System.Drawing.Point(276, 307);
            this.grp_last.Name = "grp_last";
            this.grp_last.Size = new System.Drawing.Size(665, 84);
            this.grp_last.TabIndex = 74;
            this.grp_last.TabStop = false;
            this.grp_last.Text = "اطلاعات مشترک مربوطه ";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(459, 19);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(167, 25);
            this.labelX4.TabIndex = 172;
            this.labelX4.Text = "تاریخ قرائت دوره قبلی:";
            // 
            // txt_tarikh_gheraat_Felli
            // 
            this.txt_tarikh_gheraat_Felli.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.kontorkhanBindingSource, "tarikh", true));
            this.txt_tarikh_gheraat_Felli.Location = new System.Drawing.Point(318, 51);
            this.txt_tarikh_gheraat_Felli.Name = "txt_tarikh_gheraat_Felli";
            this.txt_tarikh_gheraat_Felli.Size = new System.Drawing.Size(167, 24);
            this.txt_tarikh_gheraat_Felli.TabIndex = 169;
            this.txt_tarikh_gheraat_Felli.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(459, 50);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(167, 25);
            this.labelX2.TabIndex = 170;
            this.labelX2.Text = "تاریخ قرائت دوره فعلی :";
            // 
            // txtlast
            // 
            this.txtlast.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtlast.Border.Class = "TextBoxBorder";
            this.txtlast.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtlast.DisabledBackColor = System.Drawing.Color.White;
            this.txtlast.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txtlast.ForeColor = System.Drawing.Color.Black;
            this.txtlast.Location = new System.Drawing.Point(81, 18);
            this.txtlast.Margin = new System.Windows.Forms.Padding(4);
            this.txtlast.Name = "txtlast";
            this.txtlast.PreventEnterBeep = true;
            this.txtlast.Size = new System.Drawing.Size(112, 26);
            this.txtlast.TabIndex = 168;
            // 
            // lbllast
            // 
            this.lbllast.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbllast.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbllast.Font = new System.Drawing.Font("B Yekan", 10F);
            this.lbllast.ForeColor = System.Drawing.Color.Black;
            this.lbllast.Location = new System.Drawing.Point(39, 19);
            this.lbllast.Margin = new System.Windows.Forms.Padding(4);
            this.lbllast.Name = "lbllast";
            this.lbllast.Size = new System.Drawing.Size(249, 25);
            this.lbllast.TabIndex = 167;
            this.lbllast.Text = "قرائت دوره قبل:";
            // 
            // txt_last_date_gheraat
            // 
            this.txt_last_date_gheraat.Location = new System.Drawing.Point(318, 25);
            this.txt_last_date_gheraat.Name = "txt_last_date_gheraat";
            this.txt_last_date_gheraat.Readonly = true;
            this.txt_last_date_gheraat.Size = new System.Drawing.Size(167, 24);
            this.txt_last_date_gheraat.TabIndex = 173;
            this.txt_last_date_gheraat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // Frm_ab_CheckGheraat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 711);
            this.Controls.Add(this.grpShouldAdd);
            this.Controls.Add(this.grpDoreh);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_CheckGheraat";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmCheckGheraat_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCheckGheraat_KeyDown);
            this.grpDoreh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_doreh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kontorkhanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset)).EndInit();
            this.grpShouldAdd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShouldAdd)).EndInit();
            this.grp_last.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.Controls.GroupPanel grpDoreh;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_doreh;
        private MainAbDataset mainAbDataset;
        private System.Windows.Forms.BindingSource kontorkhanBindingSource;
        private MainAbDatasetTableAdapters.kontor_khanTableAdapter kontor_khanTableAdapter;
        private DevComponents.DotNetBar.ButtonX btnReadKontorKhanAndUpdate;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFindByGhararda;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btn_find_by_gharardad;
        private DevComponents.DotNetBar.ButtonX btn_create_new_doreh;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.LabelX LblDoreh;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.GroupPanel grpShouldAdd;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvShouldAdd;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shomarehkontorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn adamgheraatDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn darhalsakhtDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn kharabDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ghatDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dorkamelDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tavizDataGridViewCheckBoxColumn;
        private System.Windows.Forms.GroupBox grp_last;
        private DevComponents.DotNetBar.LabelX labelX4;
        private FarsiLibrary.Win.Controls.FADatePicker txt_tarikh_gheraat_Felli;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtlast;
        private DevComponents.DotNetBar.LabelX lbllast;
        private FarsiLibrary.Win.Controls.FADatePicker txt_last_date_gheraat;
    }
}