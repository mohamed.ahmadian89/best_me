﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class Frm_ab_FirstTimeKontorValues : MyMetroForm
    {
        public Frm_ab_FirstTimeKontorValues()
        {
            InitializeComponent();
        }

        private void FrmFirstTimeKontorValues_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset.ab_bedehi_bestankar_First_time' table. You can move, or remove it, as needed.
            this.ab_bedehi_bestankar_First_timeTableAdapter.Fill(this.mainAbDataset.ab_bedehi_bestankar_First_time);
            // TODO: This line of code loads data into the 'mainAbDataset.kontor_first_time_values' table. You can move, or remove it, as needed.
            if (mainAbDataset.ab_bedehi_bestankar_First_time.Rows.Count == 0)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from ab_bedehi_bestankar_First_time;insert into ab_bedehi_bestankar_First_time(gharardad,bedehi,bestankar,kontor_start)  select gharardad, 0,0,0 from moshtarekin ");
            }
            this.ab_bedehi_bestankar_First_timeTableAdapter.Fill(this.mainAbDataset.ab_bedehi_bestankar_First_time);
            Classes.ClsMain.ChangeCulture("f");
        }

        private void btnReadKontorKhanAndUpdate_Click(object sender, EventArgs e)
        {
            abbedehibestankarFirsttimeBindingSource.EndEdit();
            bool Continue = true;
            for (int i = 0; i < dgv_info.Rows.Count; i++)
            {
                if ((Convert.ToDecimal(dgv_info.Rows[i].Cells[2].Value) > 0 && Convert.ToDecimal(dgv_info.Rows[i].Cells[1].Value) != 0) ||
                    (Convert.ToDecimal(dgv_info.Rows[i].Cells[1].Value) > 0 && Convert.ToDecimal(dgv_info.Rows[i].Cells[2].Value) != 0) ||
                        Convert.ToInt32(dgv_info.Rows[i].Cells[3].Value) < 0)
                {
                    Payam.Show("مقادیر وارد شده برای مشترک :" + dgv_info.Rows[i].Cells[0].Value.ToString() + " معتبر نمی باشد ");
                    Continue = false;
                    break;
                }
            }
            if (Continue)
            {
                ab_bedehi_bestankar_First_timeTableAdapter.Update(mainAbDataset.ab_bedehi_bestankar_First_time);
                Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
            }
        }

        private void txtFindByGhararda_TextChanged(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text != "")
                abbedehibestankarFirsttimeBindingSource.Filter = "gharardad =" + txtFindByGhararda.Text;
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            abbedehibestankarFirsttimeBindingSource.RemoveFilter();
        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            if (txtFindByGhararda.Text != "")
                abbedehibestankarFirsttimeBindingSource.Filter = "gharardad =" + txtFindByGhararda.Text;

        }

        private void FrmFirstTimeKontorValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}