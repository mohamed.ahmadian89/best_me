﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;

namespace CWMS.Ab
{
    public partial class Frm_ab_per_user : MyMetroForm
    {
        string DorehForChap = "all";
        public Frm_ab_per_user(string gharardadtemp)
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from ab_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
            if (gharardadtemp != "")
            {
                txt_find_gharardad.Text = gharardadtemp;
                btn_all.PerformClick();
            }
        }

        private void FrmAllAbGhabz_Load(object sender, EventArgs e)
        {

        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            if (txt_find_gharardad.Text.Trim() == "")
            {
                txt_find_gharardad.Focus();
                Payam.Show("لطفا شماره قرارداد را وارد نمایید");
            }
            else
            {
                ab_ghabzTableAdapter.FillByGharardad(abDataset.ab_ghabz, Convert.ToInt32(txt_find_gharardad.Text));

                dgv_ghobooz.DataSource = abDataset.ab_ghabz;
                lblPayam.Text = " تعداد کلیه قبوض  : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
                Etelaat_amari();
                DorehForChap = "all";

                dgv_ghobooz.Focus();

                DataTable DtMoshtarek = Classes.ClsMain.ProcessInMoshtarekinData("gharardad=" + txt_find_gharardad.Text);
                if (DtMoshtarek.Rows.Count == 0)
                {
                    Payam.Show(txt_find_gharardad.Text + " در سیستم ثبت نشده است  ");
                    txt_find_gharardad.Clear();
                    txt_find_gharardad.Focus();
                }
                else
                {
                    lbl_co_name.Text = DtMoshtarek.Rows[0]["co_name"].ToString();
                    lbl_gharardad.Text = DtMoshtarek.Rows[0]["gharardad"].ToString();
                    lbl_modir_amel.Text = DtMoshtarek.Rows[0]["modir_amel"].ToString();
                    Set_pardakhti(dgv_ghobooz);
                }
            }
        }

        private void btn_dar_hal_barrrasi_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=2";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض  درحال بررسی: " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_padakhr_nashode_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=0";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت نشده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_pardakht_shodeh_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=1";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت شده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        void Etelaat_amari()
        {
            decimal sumall = 0, shodeh = 0, nashodeh = 0;
            for (int i = 0; i < abDataset.ab_ghabz.Rows.Count; i++)
            {
                sumall += Convert.ToDecimal(abDataset.ab_ghabz.Rows[i]["mablaghkol"].ToString());
                if (Convert.ToInt32(abDataset.ab_ghabz.Rows[i]["vaziat"]) == 1)
                    shodeh += Convert.ToDecimal(abDataset.ab_ghabz.Rows[i]["mablaghkol"].ToString());
                else
                    nashodeh += Convert.ToDecimal(abDataset.ab_ghabz.Rows[i]["mablaghkol"].ToString());
            }

            lbl_sum_all.Text = sumall.ToString();
            lbl_sum_nashode.Text = nashodeh.ToString();
            lbl_sum_shode.Text = shodeh.ToString();

        }





        private void btn_find_Click(object sender, EventArgs e)
        {
            if (txt_find_gharardad.Text.Trim() == "")
            {
                txt_find_gharardad.Focus();
                Payam.Show("لطفا شماره قرارداد را وارد نمایید");
            }
            else if (cmbDoreh.Text.Trim() == "")
            {
                btn_all.PerformClick();
            }
            else
            {
                DorehForChap = cmbDoreh.Text;
                ab_ghabzTableAdapter.FillByGharardad(abDataset.ab_ghabz, Convert.ToInt32(txt_find_gharardad.Text));

                dgv_ghobooz.Focus();

                Etelaat_amari();
                DataTable MoshtarekDT = Classes.ClsMain.ProcessInMoshtarekinData("gharardad=" + txt_find_gharardad.Text);
                if (MoshtarekDT.Rows.Count == 0)
                {
                    Payam.Show(txt_find_gharardad.Text + " در سیستم ثبت نشده است  ");
                    txt_find_gharardad.Clear();
                    txt_find_gharardad.Focus();
                }
                else
                {
                    lbl_co_name.Text = MoshtarekDT.Rows[0]["co_name"].ToString();
                    lbl_gharardad.Text = MoshtarekDT.Rows[0]["gharardad"].ToString();
                    lbl_modir_amel.Text = MoshtarekDT.Rows[0]["modir_amel"].ToString();


                    //..
                    DataView dv = new DataView(abDataset.ab_ghabz);
                    dv.RowFilter = "dcode=" + cmbDoreh.Text;
                    dgv_ghobooz.DataSource = dv.ToTable();
                    Set_pardakhti(dgv_ghobooz);
                    lblPayam.Text = "تعداد کلیه قبوض در دوره" + cmbDoreh.Text + ":" + "(" + dgv_ghobooz.Rows.Count.ToString() + "";


                }
            }
        }

        void Set_pardakhti(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].Cells["pardakhti"].Value =
                    (
                    Convert.ToDecimal(dgv.Rows[i].Cells["mablaghkolDataGridViewTextBoxColumn"].Value)  -
                    Convert.ToDecimal(dgv.Rows[i].Cells["mande"].Value) -
                    Convert.ToDecimal(dgv.Rows[i].Cells["kasr_hezar"].Value) +
                    Convert.ToDecimal(dgv.Rows[i].Cells["bestankari"].Value)


                    );
            }
        }


        private void dgv_ghobooz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                Ab.Frm_ab_details frm = new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells["gcodeDataGridViewTextBoxColumn"].Value.ToString(), lbl_gharardad.Text);
                frm.ShowDialog();
                btn_all.PerformClick();
            }
        }

        private void مشاهدهاطلاعاتکاملقبضToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                Ab.Frm_ab_details frm = new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), lbl_gharardad.Text);
                frm.ShowDialog();
                btn_all.PerformClick();
            }
        }

        private void مشاهدهپرداختیهایقبضToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string gcode = dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString();
            string doreh = dgv_ghobooz.SelectedRows[0].Cells[1].Value.ToString();
            string gharardad = dgv_ghobooz.SelectedRows[0].Cells[2].Value.ToString();
            string mablaghkol = dgv_ghobooz.SelectedRows[0].Cells[9].Value.ToString();
            string mhoroof = Classes.clsNumber.Number_to_Str(mablaghkol);
            Classes.ClsMain.ChangeCulture("e");
            string tarikh = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(dgv_ghobooz.SelectedRows[0].Cells[3].Value.ToString()).ToString("d");
            Classes.ClsMain.ChangeCulture("f");


            long bestankari_from_last_doreh = Convert.ToInt64(Classes.ClsMain.ExecuteScalar(
              "select isnull(bestankari_from_last_doreh,0) from ab_ghabz where gcode=" + gcode
              ));

            new Sharj.FrmGhabzPardakht(true, gcode, doreh, gharardad, mablaghkol, mhoroof,bestankari_from_last_doreh, tarikh).ShowDialog();
        }

        private void txt_find_gharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmbDoreh.Focus();
        }

        private void cmbDoreh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find.PerformClick();
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (txt_find_gharardad.Text.Trim() != "")
            {
                CrystalReportViewer repVUer = new CrystalReportViewer();
                CrysReports.Ab_doreh rpt = new CrysReports.Ab_doreh();
                Classes.clsAb.ChapAbDoreh(DorehForChap,
                        repVUer,
                        rpt, "print", txt_find_gharardad.Text
                        );
            }
        }

        private void dgv_ghobooz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (dgv_ghobooz.SelectedRows.Count != 0)
                {
                    Ab.Frm_ab_details frm = new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), lbl_gharardad.Text);
                    frm.ShowDialog();
                    btn_all.PerformClick();
                }
        }

        private void dgv_ghobooz_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ab_Per_User_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
            {
                Ab.Frm_ab_details frm = new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells["gcodeDataGridViewTextBoxColumn"].Value.ToString(), lbl_gharardad.Text);
                frm.ShowDialog();
                btn_all.PerformClick();
            }
        }

        private void dgv_ghobooz_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowGhobooz.PerformClick();
        }
    }
}