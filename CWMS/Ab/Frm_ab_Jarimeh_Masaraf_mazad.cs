﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class Frm_ab_Jarimeh_Masaraf_mazad : MyMetroForm
    {
        public Frm_ab_Jarimeh_Masaraf_mazad()
        {
            InitializeComponent();
        }

        private void Frm_Jarimeh_Masaraf_mazad_Load(object sender, EventArgs e)
        {

            DataTable dt_ab_doreh = Classes.ClsMain.GetDataTable("select top(1) mablagh_ab from ab_doreh order by dcode desc");
            if (dt_ab_doreh.Rows.Count == 0)
            {
                Payam.Show("لطفا ابتدا دوره آب ایجاد کرده وسپس به این سمت مراجعه نمایید");
                    this.Close();
            }
            txt_last_mablagh.Text = dt_ab_doreh.Rows[0][0].ToString();

            // TODO: This line of code loads data into the 'mainAbDataset.Jarime_masraf_mazad' table. You can move, or remove it, as needed.
            this.jarime_masraf_mazadTableAdapter.Fill(this.mainAbDataset.Jarime_masraf_mazad);
            // TODO: This line of code loads data into the 'abDataset.Jarime_masraf_mazad' table. You can move, or remove it, as needed.
            //this.jarime_masraf_mazadTableAdapter.Fill(this.abDataset.Jarime_masraf_mazad);
            txtSize.Focus();
        }

        bool Adding = false;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!Adding)
            {
                Adding = true;
                jarimemasrafmazadBindingSource.AddNew();
                txtSize.Focus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p80)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 80);

                Adding = false;
                jarimemasrafmazadBindingSource.EndEdit();
                jarime_masraf_mazadTableAdapter.Update(mainAbDataset.Jarime_masraf_mazad);
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p80)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 80, "حذف");

                if (jarimemasrafmazadBindingSource.Count > 0)
                {
                    jarimemasrafmazadBindingSource.RemoveCurrent();
                    jarimemasrafmazadBindingSource.EndEdit();
                    jarime_masraf_mazadTableAdapter.Update(mainAbDataset.Jarime_masraf_mazad);
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void txtSize_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txthazineh.Focus();
        }

        private void txthazineh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSave.PerformClick();
        }




        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            if (Cpayam.Show("با ثبت هزینه ثابت، کلیه اطلاعات بازنویسی خواهد شد.آیا موافق هستید?") == DialogResult.Yes)
            {
                if (txt_zarib.Text != "0")
                {
                    Classes.ClsMain.ExecuteNoneQuery("delete from Jarime_masraf_mazad");
                    jarime_masraf_mazadTableAdapter.Insert(1, Convert.ToDouble(txt_zarib.Text));
                    this.jarime_masraf_mazadTableAdapter.Fill(this.mainAbDataset.Jarime_masraf_mazad);
                    Payam.Show("هزینه ثابت پس از تبدیل به ضریب متناسب در سیستم ثبت شد");
                }
            }
        }

        private void txt_mablagh_sabet_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                txt_zarib.Text = String.Format("{0:0.##}", Convert.ToDecimal(txt_mablagh_sabet.Text) / Convert.ToDecimal(txt_last_mablagh.Text));
            }
            catch (Exception)
            {
                txt_zarib.Text = "0";
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            new Ab.Frm_masraf_mazad_mesal().ShowDialog();
        }
    }
}