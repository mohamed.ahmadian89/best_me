﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;
using System.Globalization;
using System.Collections;

namespace CWMS.Ab
{
    public partial class Frm_ab_details : MyMetroForm
    {
        string Gharardad = "";
        bool IsNewGhabz = false;
        int KasrHezarFromLastDoreh = 0;
        int MoshtarekTopMasraf = -1;
        int Ab_Doreh_Tarefe = 0;
        bool was_bestankar_in_sodoorGhabz = false;

        DateTime? last_pardakht = null;
        void LoadGhabzValue(string gcode)
        {
            DataTable dt_info = Classes.clsAb.Get_Ab_ghabz_info(gcode);
            if (dt_info.Rows.Count > 0)
            {
                try
                {

                    txtGhabli.Text = dt_info.Rows[0]["kontor_start"].ToString();
                    txtFelli.Text = dt_info.Rows[0]["kontor_end"].ToString();
                    txtMasrafKol.Text = dt_info.Rows[0]["masraf"].ToString();
                    txtMojaz.Text = dt_info.Rows[0]["masraf_mojaz"].ToString();
                    txtGheirMojaz.Text = dt_info.Rows[0]["masraf_gheir_mojaz"].ToString();

                    KasrHezarFromLastDoreh = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select isnull(kasr_hezar,0) from ab_ghabz where gharardad=" +
     dt_info.Rows[0]["gharardad"].ToString() + " and dcode=" + (Convert.ToInt32(dt_info.Rows[0]["dcode"].ToString()) - 1).ToString()
    ));

                    cmb_vaziat.SelectedIndex = Convert.ToInt32(dt_info.Rows[0]["vaziat"]);
                    txt_ghabz_code.Text = dt_info.Rows[0]["gcode"].ToString();
                    txtdoreh.Text = dt_info.Rows[0]["dcode"].ToString();
                    txtMablagh.Text = dt_info.Rows[0]["mablagh"].ToString();
                    txtMablaghKol.Text = dt_info.Rows[0]["mablaghkol"].ToString();
                    txtMaliat.Text = dt_info.Rows[0]["maliat"].ToString();
                    txt_sayer.Text = dt_info.Rows[0]["sayer"].ToString();
                    txt_sayer_tozihat.Text = dt_info.Rows[0]["sayer_tozihat"].ToString();
                    txtMablaghHoroof.Text = Classes.clsNumber.Number_to_Str(txtMablaghKol.Text);
                    txt_ensheab.Text = dt_info.Rows[0]["Ensheab"].ToString();
                    txtMohlat.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["mohlat_pardakht"]);
                    txtTarefe.Text = dt_info.Rows[0]["tarefe"].ToString();
                    txtBedehi.Text = dt_info.Rows[0]["bedehi"].ToString();
                    txtDarsad.Text = dt_info.Rows[0]["darsad_maliat"].ToString();
                    txtTarikhSoddor.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["tarikh"]);
                    txt_gheraat.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["tarikh_gheraat"]);
                    try
                    {
                        txt_tarikh_gheraat_habli.SelectedDateTime = Convert.ToDateTime(dt_info.Rows[0]["tarikh_gheraat_ghabli"]);

                    }
                    catch (Exception)
                    {

                    }
                    txt_jarimeh.Text = dt_info.Rows[0]["jarimeh"].ToString();
                    txtJarimehTozihat.Text = dt_info.Rows[0]["jarimeh_tozihat"].ToString();
                    txtAboonman.Text = dt_info.Rows[0]["aboonmah"].ToString();
                    txtZaribFazelab.Text = dt_info.Rows[0]["darsad_fazelab"].ToString();
                    txtfazelab.Text = dt_info.Rows[0]["fazelab"].ToString();
                    txt_masraf_omoomi.Text = dt_info.Rows[0]["masraf_omoomi"].ToString();

                    txt_tozihat.Text = dt_info.Rows[0]["tozihat"].ToString();
                    txBaghimandeh.Text = dt_info.Rows[0]["mande"].ToString();
                    txt_kasr.Text = dt_info.Rows[0]["kasr_hezar"].ToString();
                    txt_taviz_kontor_hazineh.Text = dt_info.Rows[0]["hazineh_taviz_kontor"].ToString();

                    txt_shenase_ghabz.Text = dt_info.Rows[0]["shenase_ghabz"].ToString();
                    txt_shenase_pardakht.Text = dt_info.Rows[0]["shenase_pardakht"].ToString();

                    txtPardakhti.Text = (Convert.ToInt64(dt_info.Rows[0]["mablaghkol"]) -
                        (Convert.ToInt64(dt_info.Rows[0]["mande"]) + Convert.ToInt64(txt_kasr.Text))

                        ).ToString();

                    try
                    {
                        last_pardakht = Convert.ToDateTime(dt_info.Rows[0]["last_pardakht"]);

                    }
                    catch (Exception)
                    {

                        last_pardakht = null;
                    }
                    chKarab.Checked = Convert.ToBoolean(dt_info.Rows[0]["kharab"]);
                    chTaviz.Checked = Convert.ToBoolean(dt_info.Rows[0]["taviz"]);
                    chDoreKamel.Checked = Convert.ToBoolean(dt_info.Rows[0]["dor_kamel"]);
                    chAdameGheraat.Checked = Convert.ToBoolean(dt_info.Rows[0]["adam_gheraat"]);
                    chGhat.Checked = Convert.ToBoolean(dt_info.Rows[0]["ghat"]);

                    was_bestankar_in_sodoorGhabz = Convert.ToBoolean(dt_info.Rows[0]["was_bestankar_in_sodoorGhabz"]);
                    chtejari.Checked = Convert.ToBoolean(dt_info.Rows[0]["tejari"]);
                    chSakhtoSaz.Checked = Convert.ToBoolean(dt_info.Rows[0]["darhalsakht"]);




                    // مبلغ بستانکاری که ازدوره قبل به این دوره منتقل شده است 
                    try
                    {
                        int doreh = Convert.ToInt32(txtdoreh.Text);
                        if (doreh == 1)
                        {
                            TxtBestankariFromLastDoreh.Text = Classes.ClsMain.ExecuteScalar("select bestankar from ab_bedehi_bestankar_First_time where gharardad=" + dt_info.Rows[0]["gharardad"].ToString()).ToString();
                            txt_tozihat.Text = "مبلغ پرداختی از اطلاعات وارد شده در قسمت راه اندازی اولیه سیستم - اطلاعات اولیه آب  استخراج شده است ";

                        }
                        else
                            TxtBestankariFromLastDoreh.Text = Classes.ClsMain.ExecuteScalar("select bestankari from ab_ghabz where dcode=" + (doreh - 1).ToString() + " and gharardad=" + dt_info.Rows[0]["gharardad"].ToString()).ToString();

                    }
                    catch (Exception)
                    {

                        TxtBestankariFromLastDoreh.Text = "0";
                    }



                    // مبلغ بستانکاری مربوط به دوره فعلی


                    //bestankari_from_last_doreh = Convert.ToInt64(dt_info.Rows[0]["bestankari_from_last_doreh"]);

                    if (Convert.ToInt64(dt_info.Rows[0]["bestankari"]) > 0)
                    {
                        lblBestankar.Visible = txtBestankar.Visible = true;
                        txtBestankar.Text = dt_info.Rows[0]["bestankari"].ToString();
                    }
                    else
                    {
                        txtBestankar.Text = "0";
                        lblBestankar.Visible = txtBestankar.Visible = false;

                    }







                    if (Convert.ToBoolean(dt_info.Rows[0]["locked"]))
                    {
                        btn_save.Visible = false;
                        lbllocked.Visible = true;
                    }
                    else
                    {
                        btn_save.Visible = true;
                        lbllocked.Visible = false;

                    }
                }
                catch (Exception e1)
                {
                    Payam.Show("متاسفانه در هنگام نمایش اطلاعات قبض خطا رخ داده است ");
                    Classes.ClsMain.logError(e1);
                }
            }
        }
        public Frm_ab_details(string gcode, string GharardadTemp)
        {
            Classes.ClsMain.ChangeCulture("f");

            InitializeComponent();
            Gharardad = GharardadTemp;
            MoshtarekTopMasraf = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select masrafe_mojaz from moshtarekin where gharardad=" + Gharardad));
            LoadGhabzValue(gcode);

        }

        static long GetRoundedInteger(decimal number)
        {
            return Convert.ToInt64(Math.Round(number));
        }
        //public Frm_ab_details(string GharardadTemp, bool IsNewGhabzAb)
        //{
        //    Classes.ClsMain.ChangeCulture("f");
        //    IsNewGhabz = IsNewGhabzAb;
        //    InitializeComponent();
        //    Gharardad = GharardadTemp;
        //    txt_gheraat.SelectedDateTime = DateTime.Now;

        //    MoshtarekTopMasraf = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select masrafe_mojaz from moshtarekin where gharardad=" + Gharardad));

        //    // اطلاعات مشترک
        //    DataTable dt_moshtarek = Classes.ClsMain.GetDataTable("select * from moshtarekin where gharardad=" + Gharardad);
        //    txtZaribFazelab.Text = dt_moshtarek.Rows[0]["zarib_fazelab"].ToString();
        //    chtejari.Checked = Convert.ToBoolean(dt_moshtarek.Rows[0]["tejari"]);




        //    // اطلاعات دوره اب
        //    DataTable Ab_doreh = Classes.clsDoreh.Get_last_Ab_Doreh();
        //    txtMohlat.SelectedDateTime = Convert.ToDateTime(Ab_doreh.Rows[0]["mohlat_pardakht"]);
        //    txtTarikhSoddor.SelectedDateTime = DateTime.Now;
        //    txtdoreh.Text = Ab_doreh.Rows[0]["dcode"].ToString();
        //    txtTarefe.Text = Ab_doreh.Rows[0]["mablagh_ab"].ToString();
        //    txtDarsad.Text = Ab_doreh.Rows[0]["darsad_maliat"].ToString();


        //    int gcode = Classes.ClsMain.CodeGhabzGenerate(Convert.ToInt32(txtdoreh.Text), true, Convert.ToInt32(Gharardad));
        //    txt_ghabz_code.Text = gcode.ToString();

        //    DataTable dt_lastDoreh = null;
        //    try
        //    {
        //        dt_lastDoreh = Classes.ClsMain.GetDataTable("select kontor_end from ab_ghabz where gharardad=" + Gharardad + " and dcode=" + (Convert.ToInt32(txtdoreh.Text) - 1).ToString());
        //        if (dt_lastDoreh.Rows.Count == 0)
        //        {
        //            txtGhabli.Text = "0";
        //            TxtBestankariFromLastDoreh.Text = "0";
        //        }
        //        else
        //        {
        //            txtGhabli.Text = dt_lastDoreh.Rows[0]["kontor_end"].ToString();
        //            TxtBestankariFromLastDoreh.Text = (dt_lastDoreh.Rows[0]["bestankari"]).ToString();
        //        }

        //        try
        //        {
        //            DataTable dt_kontorKhan = Classes.ClsMain.GetDataTable("select  * from kontor_khan where gharardad=" + Gharardad);
        //            txtFelli.Text = dt_kontorKhan.Rows[0]["shomareh_kontor"].ToString();
        //            chAdameGheraat.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["adam_gheraat"]);
        //            chGhat.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["ghat"]);
        //            chTaviz.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["taviz"]);
        //            chDoreKamel.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["dor_kamel"]);
        //            chKarab.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["kharab"]);
        //            chSakhtoSaz.Checked = Convert.ToBoolean(dt_kontorKhan.Rows[0]["dar_hal_sakht"]);

        //        }
        //        catch (Exception)
        //        {
        //            txtFelli.Text = txtGhabli.Text;
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        txtFelli.Text = txtGhabli.Text = "0";
        //        TxtBestankariFromLastDoreh.Text = "0";
        //    }
        //    CalcMablaghAndMasraf();



        //    //------------------------------------------------------------------            
        //    // محاسبه ابونماه با توجه به سایز انشعاب
        //    txtAboonman.Text = GetRoundedInteger(Classes.clsAb.Moshtarek_ensheab_hazineh(Convert.ToSingle(dt_moshtarek.Rows[0]["size_ensheab"]))).ToString();
        //    //------------------------------------------------------------------      


        //    txt_ghabz_code.Text = Classes.ClsMain.CodeGhabzGenerate(Classes.clsDoreh.Get_last_Ab_dcode(), true, Convert.ToInt32(Gharardad)).ToString();




        //    txtfazelab.Text = Classes.clsAb.HazinehFazelab(Convert.ToDecimal(txtMablaghKol.Text), Convert.ToSingle(txtZaribFazelab.Text, CultureInfo.InvariantCulture)).ToString();
        //    CalculateMablaghKol();

        //}

        void CalculateMablaghKol()
        {



            if (txtMaliat.Text.Trim() == "")
                txtMaliat.Text = "0";
            if (txt_sayer.Text.Trim() == "")
                txt_sayer.Text = "0";
            if (txtBedehi.Text.Trim() == "")
                txtBedehi.Text = "0";
            if (txt_jarimeh.Text.Trim() == "")
                txt_jarimeh.Text = "0";

            if (txtfazelab.Text == "")
                txtfazelab.Text = "0";

            // به نظر می رسه هیچ نیازی نیست که در این فرم محاسباتی انجام بشه
            // به دلیل اینکه در حالتی که کاربر مبلغ قبض ان صفر باشد
            // و همچنین بستانکار هم باشد  اطلاعات دچار ناهماهنگی در مبلغ بستانکاری شده
            // و سبب می شود که  عملیات صدور قبض به درستی انجام نشود 



            //txtMablaghKol.Text = (Convert.ToInt64(txt_sayer.Text) + Convert.ToInt64(txtMablagh.Text) + Convert.ToInt64(txt_jarimeh.Text)).ToString();
            //txtMaliat.Text = GetRoundedInteger(Convert.ToDecimal((Convert.ToInt64(txtMablaghKol.Text) * Convert.ToSingle(txtDarsad.Text, CultureInfo.InvariantCulture)) / 100)).ToString();
            //txtMablaghKol.Text = (Convert.ToInt64(txtMablaghKol.Text) + Convert.ToInt64(txtMaliat.Text) + Convert.ToInt64(txtBedehi.Text) + KasrHezarFromLastDoreh + Convert.ToInt64(txtfazelab.Text)).ToString();

            //long AllPardakhti = Convert.ToInt64(Classes.ClsMain.ExecuteScalar("select isnull(sum(mablagh),0) from ghabz_pardakht where vaziat=1 and gcode=" + txt_ghabz_code.Text));
            //int kasr_hezar = 0;
            //if (Convert.ToInt64(txtMablaghKol.Text) > Convert.ToInt64(TxtBestankariFromLastDoreh.Text))
            //{
            //    txtMablaghKol.Text = (Convert.ToInt64(txtMablaghKol.Text) - Convert.ToInt64(TxtBestankariFromLastDoreh.Text)).ToString();
            //    kasr_hezar = Convert.ToInt32(Convert.ToInt64(txtMablaghKol.Text) % 1000);
            //    txtMablaghKol.Text = (Convert.ToInt64(txtMablaghKol.Text) - kasr_hezar).ToString();
            //    txt_kasr.Text = kasr_hezar.ToString();
            //}
            //else
            //{
            //    AllPardakhti +=
            //     (Convert.ToInt64(TxtBestankariFromLastDoreh.Text) - Convert.ToInt64(txtMablaghKol.Text));
            //    txtMablaghKol.Text = txt_kasr.Text =  "0";
            //}

            //txtMablaghHoroof.Text = Classes.clsNumber.Number_to_Str(txtMablaghKol.Text);

            ////---------------------------------------------------------------------------



            //long Result = Convert.ToInt64(txtMablaghKol.Text);
            //txtPardakhti.Text = AllPardakhti.ToString();
            //if (AllPardakhti > Result)
            //{
            //    txBaghimandeh.Text = "0";
            //    lblBestankar.Visible = txtBestankar.Visible = true;
            //    txtBestankar.Text = (AllPardakhti - Result).ToString();

            //}
            //else
            //{
            //    txBaghimandeh.Text = (Result - AllPardakhti).ToString();
            //    txtBestankar.Text = "0";
            //    lblBestankar.Visible = txtBestankar.Visible = false;
            //}


        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (txt_sayer.Text == "")
                txt_sayer.Text = "0";
            if (Classes.clsMoshtarekin.UserInfo.p62)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 62, txt_ghabz_code.Text);
                CalculateMablaghKol();

            


                Classes.clsGhabz ghabzInfo = new Classes.clsGhabz(Convert.ToInt32(Gharardad));
                ghabzInfo.was_bestankar_in_sodoorGhabz = was_bestankar_in_sodoorGhabz;
                ghabzInfo.last_pardakht = last_pardakht;

                //if (was_bestankar_in_sodoorGhabz)
                //{
                //    Payam.Show("کاربر گرامی ، دقت نمایید هرگونه تغییر در مبالغ قبض بر روی مبلغ بستانکاری تاثیر می گذ ارد . ");

                //}
                ghabzInfo.is_Updating_ghabz = true;
                ghabzInfo.gharardad = Convert.ToInt32(Gharardad);
                ghabzInfo.doreh = Convert.ToInt32(txtdoreh.Text);
                ghabzInfo.tejari = Convert.ToBoolean(chtejari.Checked);
                ghabzInfo.darsad_maliat = Convert.ToDecimal(txtDarsad.Text, CultureInfo.InvariantCulture);
                ghabzInfo.zarib_fazelab = Convert.ToDecimal(txtZaribFazelab.Text, CultureInfo.InvariantCulture);
                ghabzInfo.tozihat = txt_tozihat.Text;
                ghabzInfo.tozihat_sayer = txt_sayer_tozihat.Text;
                ghabzInfo.hazineh_jarimeh = Convert.ToDecimal(txt_jarimeh.Text, CultureInfo.InvariantCulture);
                ghabzInfo.hazineh_sayer = Convert.ToDecimal(txt_sayer.Text, CultureInfo.InvariantCulture);
                ghabzInfo.tozihat_jarimeh = txtJarimehTozihat.Text;
                ghabzInfo.hazineh_taviz_kontor = Convert.ToDecimal(txt_taviz_kontor_hazineh.Text);
                ghabzInfo.tarefe_ab = Convert.ToInt32(txtTarefe.Text);

                ghabzInfo.tozihat_jarimeh = txtJarimehTozihat.Text;
                ghabzInfo.tarikh_sodoor = Convert.ToDateTime(txtTarikhSoddor.SelectedDateTime);
                try
                {
                    ghabzInfo.tarikh_gheraat_ghabli = Convert.ToDateTime(txt_tarikh_gheraat_habli.SelectedDateTime);
                }
                catch (Exception)
                {
                    Payam.Show("لطفا تاریخ قرائت قبلی را به درستی وارد نمایید");
                }

                try
                {
                    ghabzInfo.mohlat_pardakht_ghabz = Convert.ToDateTime(txtMohlat.SelectedDateTime);

                }
                catch (Exception)
                {

                    Payam.Show("لطفا مهلت پرداخت را به صورت صحیح وارد نمایید");
                    txtMohlat.SelectedDateTime = DateTime.Now;
                    txtMohlat.Focus();
                }



                // اطلاعاتی  که در این فرایند پر خواهد شد
                // ghabzInfo.Get_last_abGhabz_of_Moshtarek();
               
                ghabzInfo.kontor_ghabli = Convert.ToInt32(txtGhabli.Text);
                ghabzInfo.bedehkar = Convert.ToDecimal(txtBedehi.Text);
                ghabzInfo.bestankar = Convert.ToDecimal(TxtBestankariFromLastDoreh.Text);
                ghabzInfo.hazineh_ensheab = Convert.ToDecimal(txtAboonman.Text);


                try
                {
                    ghabzInfo.kasr_hezar_from_last_doreh = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select kasr_hezar from ab_ghabz where gharardad=" + ghabzInfo.gharardad
                                 + " and dcode=" + (ghabzInfo.doreh - 1).ToString()));
                }
                catch (Exception)
                {
                    ghabzInfo.kasr_hezar_from_last_doreh = 0;

                }




                //ghabzInfo.have_value_for_moshtarek__in_kontorKhan
                ghabzInfo.kontor_khan_state_of_moshtarek = new MainAbDataset.kontor_khanDataTable().Newkontor_khanRow();
                ghabzInfo.kontor_khan_state_of_moshtarek.shomareh_kontor = Convert.ToInt32(txtFelli.Text);
                ghabzInfo.kontor_khan_state_of_moshtarek.ghat = chGhat.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.taviz = chTaviz.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.dor_kamel = chDoreKamel.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.kharab = chKarab.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.adam_gheraat = chAdameGheraat.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.dar_hal_sakht = chSakhtoSaz.Checked;
                ghabzInfo.kontor_khan_state_of_moshtarek.tarikh = Convert.ToDateTime(txt_gheraat.SelectedDateTime);

                Classes.clsAb AbClass = new Classes.clsAb();
                string res = AbClass.Generate_Ghabz(ghabzInfo);
                if (res == "success")
                    Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد ");


                LoadGhabzValue(txt_ghabz_code.Text);

                if (Classes.clsFactor.Has_factor(ghabzInfo.gcode.ToString()))
                {
                    //================== فاکتور ============================
                    string table_name = "ab_ghabz";
                    int ghabz_type = 0;
                    DataTable dt_ghabz_info = Classes.ClsMain.GetDataTable("select * from " + table_name + " where gcode=" + ghabzInfo.gcode);

                    Classes.clsFactor factor_class = new Classes.clsFactor(
                        Classes.clsFactor.Get_Max_factor_num(), Convert.ToInt32(ghabzInfo.gharardad),
                        Convert.ToInt32(ghabzInfo.gharardad), Convert.ToInt32(ghabzInfo.doreh),
                        0, ghabzInfo.darsad_maliat, table_name, ghabz_type, "ارائه خدمات آب دوره" + ghabzInfo.doreh);
                    factor_class.dt_ghabz = dt_ghabz_info;
                    factor_class.tarikh = Convert.ToDateTime(Classes.ClsMain.ExecuteScalar("select tarikh from factors where gcode=" + ghabzInfo.gcode));
                    factor_class.Start_genete_ghabz();
                    //================== فاکتور ============================
                }


            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }



        private void buttonX2_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p59)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 59, txt_ghabz_code.Text);

                Classes.ClsMain.ChangeCulture("e");
                //                if (Convert.ToInt64(txtBestankar.Text) > 0 && txtMablaghKol.Text == "0")
                //                {
                //                    string matn = "کاربر گرامی ، قبض این دوره به دلیل وجود بستانکاری از دوره قبل با مبلغ صفر صادر شده است " +
                //" با این حال کاربر مبلغ " + txtBestankar.Text + " بستانکار می باشد  که قرار است به دوره بعد منتقل شود " +
                //" . به اطلاع می رساند که در این دوره امکان ثبت هیچ گونه پرداختی جدید برای کاربر وجود ندارد ";

                //                    new FrmPeygham(matn).ShowDialog();
                //                    return;
                //                }
                new CWMS.Sharj.FrmGhabzPardakht(true, txt_ghabz_code.Text, txtdoreh.Text, Gharardad, txtMablaghKol.Text, txtMablaghHoroof.Text, Convert.ToInt64(TxtBestankariFromLastDoreh.Text), txtTarikhSoddor.Text.ToString()).ShowDialog();
                Classes.ClsMain.ChangeCulture("f");
                LoadGhabzValue(txt_ghabz_code.Text);
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }



        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p47)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 47, txt_ghabz_code.Text);

                if (txt_ghabz_code.Text.Trim() != "")
                {

                    if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                    {

                        int sharj_dcode = 0;
                        try
                        {
                            sharj_dcode = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select max(dcode) from sharj_ghabz where gharardad=" + Gharardad));
                        }
                        catch (Exception)
                        {
                            sharj_dcode = 0;
                        }

                        Classes.shahrak_classes.esf_bozorg.Print_Ghabz
                            (
                            Convert.ToInt32(Gharardad),
                          sharj_dcode,
                             Convert.ToInt32(txtdoreh.Text)

                            );
                    }
                    else
                    {

                        Classes.clsAb.print_Ghabz(txt_ghabz_code.Text, Gharardad, false);
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void FrmAbDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void Frm_ab_details_Load(object sender, EventArgs e)
        {

        }


        void CalcMablaghAndMasraf()
        {
            try
            {
                int start = Convert.ToInt32(txtGhabli.Text);
                int finish = Convert.ToInt32(txtFelli.Text);
                int mizan_masraf = finish - start;
                txtMasrafKol.Text = mizan_masraf.ToString();
                if (mizan_masraf > MoshtarekTopMasraf)
                {
                    txtGheirMojaz.Text = (mizan_masraf - MoshtarekTopMasraf).ToString();
                    txtMojaz.Text = MoshtarekTopMasraf.ToString();
                }
                else
                {
                    txtMojaz.Text = mizan_masraf.ToString();
                    txtGheirMojaz.Text = "0";
                }


                DataTable dt_Jarime_masraf_mazad = Classes.ClsMain.GetDataTable("select * from Jarime_masraf_mazad");
                long mablagh = 0, Mablagh_gheir_Mojaz = 0;
                //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------
                mablagh = Convert.ToInt32(txtMojaz.Text) * Convert.ToInt32(txtTarefe.Text);
                Mablagh_gheir_Mojaz = Classes.clsAb.Get_Masrafe_gheir_mojaz_hazineh(Convert.ToInt32(txtGheirMojaz.Text), dt_Jarime_masraf_mazad, Convert.ToInt32(txtTarefe.Text), Convert.ToInt32(txtMojaz.Text));
                txtMablagh.Text = (mablagh + Mablagh_gheir_Mojaz).ToString();
                //---------------------- هزینه میزان مصرف مجاز و غیر مجاز-------------------

                CalculateMablaghKol();

            }
            catch (Exception)
            {
                Payam.Show("لطفا قرائت قبلی و فعلی کنتور  آب را به درستی وارد نمایید");
            }
        }

        private void txtGhabli_Leave(object sender, EventArgs e)
        {
            CalcMablaghAndMasraf();
        }

        private void txtFelli_Leave(object sender, EventArgs e)
        {
            CalcMablaghAndMasraf();
        }

        private void txtMablagh_TextChanged(object sender, EventArgs e)
        {
            CalculateMablaghKol();
        }

        private void txt_jarimeh_TextChanged(object sender, EventArgs e)
        {
            CalculateMablaghKol();
        }

        private void txt_sayer_TextChanged(object sender, EventArgs e)
        {
            CalculateMablaghKol();
        }

        private void txtFelli_TextChanged(object sender, EventArgs e)
        {

        }

        private void Frm_ab_details_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmMainRefrence.Update_Moshtarek_Status();

        }

        private void buttonX1_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p47)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 47, txt_ghabz_code.Text);

                if (txt_ghabz_code.Text.Trim() != "")
                {
                    Classes.clsAb.print_Ghabz(txt_ghabz_code.Text, Gharardad, false);
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Payam.Show("به زودی این قابلیت به سیستم اضافه خواهد شد ");

        }

        private void chTaviz_CheckedChanged(object sender, EventArgs e)
        {
            if (chTaviz.Checked)
                btn_hazineh_taviz.Visible = true;
            else
            {
                btn_hazineh_taviz.Visible = false;
                txt_taviz_kontor_hazineh.Text = "0";
            }

        }

        private void btn_hazineh_taviz_Click(object sender, EventArgs e)
        {
            txt_taviz_kontor_hazineh.Text = Classes.ClsMain.ExecuteScalar("select top(1) hazineh_taviz_kontor from ab_info").ToString();
        }
    }
}