﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Ab
{
    public partial class frm_group_gharardad_for_kontorkhan : MyMetroForm
    {
        public frm_group_gharardad_for_kontorkhan()
        {
            InitializeComponent();
        }

        private void frm_group_gharardad_for_kontorkhan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset.kontor_khan_gharardad_grouping' table. You can move, or remove it, as needed.
            this.kontor_khan_gharardad_groupingTableAdapter.Fill(this.mainAbDataset.kontor_khan_gharardad_grouping);

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            if(dgv_group.SelectedRows.Count!=0)
            {
                new Ab.frm_group_gharardad_details(
                    Convert.ToInt32(dgv_group.SelectedRows[0].Cells[0].Value)
                    ,
                    dgv_group.SelectedRows[0].Cells[1].Value.ToString()
                    
                    ).ShowDialog();
                kontor_khan_gharardad_groupingTableAdapter.Fill(mainAbDataset.kontor_khan_gharardad_grouping);
            }
        }
        bool adding = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(adding==false)
            {
                adding = true;kontorkhangharardadgroupingBindingSource.AddNew();
                txt_group_name.Focus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            adding = false;
            kontorkhangharardadgroupingBindingSource.EndEdit();
            kontor_khan_gharardad_groupingTableAdapter.Update(mainAbDataset.kontor_khan_gharardad_grouping);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(adding==true)
            {
                adding = false;
                kontorkhangharardadgroupingBindingSource.CancelEdit();
            }
            else
            {
                if(kontorkhangharardadgroupingBindingSource.Count>0)
                {
                    if (Cpayam.Show("آیا با حذف گروه موافق هستید؟") == DialogResult.Yes)
                    {
                        string group_id = dgv_group.SelectedRows[0].Cells[0].Value.ToString();
                        kontorkhangharardadgroupingBindingSource.RemoveCurrent();
                        kontor_khan_gharardad_groupingTableAdapter.Update(mainAbDataset.kontor_khan_gharardad_grouping);
                        Classes.ClsMain.ExecuteNoneQuery
                            (
                            "delete from kontor_khan_grouping_details where group_id=" + group_id
                            );
                    }
                }
            }
        }

        private void dgv_group_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            buttonX1.PerformClick();
        }
    }
}
