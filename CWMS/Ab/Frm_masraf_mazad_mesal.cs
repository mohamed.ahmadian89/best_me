﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Ab
{
    public partial class Frm_masraf_mazad_mesal : Form
    {
        public Frm_masraf_mazad_mesal()
        {
            InitializeComponent();
            this.jarime_masraf_mazadTableAdapter.Fill(this.abDataset.Jarime_masraf_mazad);
            txt_tarefe.Text = Classes.ClsMain.ExecuteScalar(
                "select top(1) mablagh_ab from ab_doreh order by dcode desc").ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DgvResult.Rows.Clear();
            decimal masraf_mojaz = 0, masraf_gheir_mojaz = 0, masraf_kol = 0;
            masraf_mojaz = Convert.ToDecimal(txtmasraf_mojaz.Text);
            masraf_gheir_mojaz = Convert.ToDecimal(txtmasraf_gheir_mojaz.Text);
            masraf_kol = Convert.ToDecimal(txtmasraf_kol.Text);

           
            decimal FirstDarsad = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[0]["ta_darsad"]);
            decimal FirstZarib = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[0]["zarib_ab_baha"]);
            decimal sum = 0;

            decimal Tarefe = Convert.ToDecimal(txt_tarefe.Text);
            decimal lastMasrafDarsad = 0;



            if (masraf_gheir_mojaz < (FirstDarsad * masraf_mojaz / 100))
            {
                Payam.Show("هزینه مصرف غیر مجاز:" + (FirstZarib * Tarefe * masraf_gheir_mojaz));
                DgvResult.Rows.Add(FirstDarsad, (FirstDarsad * masraf_mojaz / 100), FirstZarib, masraf_gheir_mojaz, (FirstZarib * Tarefe * masraf_gheir_mojaz));
                return;
            }


            decimal CurrentDarsad = 0;
            decimal CurrentZarib = 0;
            for (int i = 0; i < abDataset.Jarime_masraf_mazad.Rows.Count; i++)
            {


                CurrentDarsad = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[i]["ta_darsad"]);
                CurrentZarib = Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[i]["zarib_ab_baha"]);
                if ((CurrentDarsad / 100 * masraf_mojaz) + masraf_mojaz <= masraf_kol)
                {

                    sum += ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe;
                    DgvResult.Rows.Add(CurrentDarsad + "/100",
                        (CurrentDarsad / 100 * masraf_mojaz),
                        CurrentZarib,
                        ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad), 
                        ((CurrentDarsad / 100 * masraf_mojaz) - lastMasrafDarsad) * CurrentZarib * Tarefe);

                    lastMasrafDarsad = (CurrentDarsad / 100 * masraf_mojaz);
                }
                else
                {
                    if (masraf_gheir_mojaz != lastMasrafDarsad)
                    {
                        decimal zarib = CurrentZarib;

                        sum += (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib;
                        DgvResult.Rows.Add(CurrentDarsad, (masraf_gheir_mojaz - lastMasrafDarsad), zarib, (masraf_gheir_mojaz - lastMasrafDarsad), (masraf_gheir_mojaz - lastMasrafDarsad) * Tarefe * zarib);
                        break;
                    }
                }

            }

            decimal Last = Convert.ToDecimal(Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["ta_darsad"]) / 100 * masraf_mojaz);

            if (masraf_gheir_mojaz > Last)
            {
                sum += (masraf_gheir_mojaz - Last) * Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe;
                DgvResult.Rows.Add(Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["ta_darsad"]), (masraf_gheir_mojaz ),
                Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]), (masraf_gheir_mojaz - Last), (masraf_gheir_mojaz - Last) * Convert.ToDecimal(abDataset.Jarime_masraf_mazad.Rows[abDataset.Jarime_masraf_mazad.Rows.Count - 1]
                ["zarib_ab_baha"]) * Tarefe

                );
            }

            txt_total_gheir_mojaz.Text = String.Format("{0:0.##}", Convert.ToDecimal(Math.Round(sum)));
           
            txt_total_mojaz.Text = String.Format("{0:0.##}", Convert.ToDecimal(txtmasraf_mojaz.Text) * Tarefe);
            txt_total.Text = (Convert.ToDecimal(txt_total_gheir_mojaz.Text) + Convert.ToDecimal(txt_total_mojaz.Text)).ToString();


            //Payam.Show(PelekaniTarefe(masraf_mojaz, masraf_gheir_mojaz, masraf_kol, (DataTable)abDataset.Jarime_masraf_mazad, Tarefe).ToString());
        }

        private void txtmasraf_mojaz_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtmasraf_kol.Text = (Convert.ToInt32(txtmasraf_gheir_mojaz.Text) + Convert.ToInt32(txtmasraf_mojaz.Text)).ToString();
            }
            catch (Exception)
            {

                txtmasraf_kol.Text = "0";
            }
        }

        private void txtmasraf_gheir_mojaz_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtmasraf_kol.Text = (Convert.ToInt32(txtmasraf_gheir_mojaz.Text) + Convert.ToInt32(txtmasraf_mojaz.Text)).ToString();
            }
            catch (Exception)
            {

                txtmasraf_kol.Text = "0";
            }
        }

        private void Frm_masraf_mazad_mesal_Load(object sender, EventArgs e)
        {

        }
    }
}
