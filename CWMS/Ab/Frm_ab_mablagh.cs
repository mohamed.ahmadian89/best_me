﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;

namespace CWMS.Ab
{
    public partial class Frm_ab_mablagh : MyMetroForm
    {
        public Frm_ab_mablagh()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
        }
        bool adding = false;
        private void FrmAbMabalegh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset1.ab_mablagh' table. You can move, or remove it, as needed.
            this.ab_mablaghTableAdapter1.Fill(this.mainAbDataset1.ab_mablagh);
            // TODO: This line of code loads data into the 'mainDataSest.ab_mablagh' table. You can move, or remove it, as needed.
            adding = false;

        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (adding == false)
            {
                adding = true;
                abmablaghBindingSource.AddNew();
                tarikhTasvib.Focus();
                tarikhEjra.SelectedDateTime = tarikhTasvib.SelectedDateTime = DateTime.Now;

            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p78)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 78);


                try
                {
                    Convert.ToInt32(txtMablagh.Text);
                }
                catch
                {
                    Payam.Show("مبلغ آب بها صحیح نمی باشد.");
                    txtMablagh.SelectAll();
                    txtMablagh.Focus();
                    return;
                }





                if (txtZrib.Text.Trim() == "" || Convert.ToInt32(txtZrib.Text) < 1)
                {
                    txtZrib.Text = "1";

                }

                else if (tarikhTasvib.Text.Contains("هی") == true)
                {
                    Payam.Show("لطفا تاریخ تصویب را از کادر مربوطه انتخاب کنید");
                    tarikhTasvib.Focus();

                }
                else if (tarikhEjra.Text.Contains("هی") == true)
                {
                    Payam.Show("لطفا تاریخ اجرا را از کادر مربوطه انتخاب کنید");
                    tarikhEjra.Focus();

                }
                else
                {
                    if (adding)
                    {
                        int item = mainDataSest.ab_mablagh.Where(p => p.tarikh_ejra > tarikhEjra.SelectedDateTime).Count();
                        if (item != 0)
                        {
                            Payam.Show("تاریخ اجرا می بایست از تاریخ های اجرای موجود در لیست بیشتر باشد");
                            tarikhEjra.Focus();
                        }
                        else
                        {
                            abmablaghBindingSource.EndEdit();
                            this.ab_mablaghTableAdapter1.Update(this.mainAbDataset1.ab_mablagh);
                            Payam.Show("اطلاعات در سیستم ثبت شد");
                            adding = false;

                        }

                    }
                    else
                    {
                        abmablaghBindingSource.EndEdit();
                        this.ab_mablaghTableAdapter1.Update(this.mainAbDataset1.ab_mablagh);
                        Payam.Show("اطلاعات در سیستم ثبت شد");
                        adding = false;

                    }
                }


            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p78)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 78,"حذف");

                if (adding == true)
                    adding = false;
                else
                {
                    if (abmablaghBindingSource.Count > 0)
                    {
                        abmablaghBindingSource.RemoveCurrent();
                        abmablaghBindingSource.EndEdit();
                        this.ab_mablaghTableAdapter1.Update(this.mainAbDataset1.ab_mablagh);
                    }
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void FrmAbMabalegh_FormClosed(object sender, FormClosedEventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");

        }

        private void FrmAbMabalegh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtMablagh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtHesab.Focus();
        }

        private void txtHesab_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtZrib.Focus();
        }

        private void txtZrib_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSave.PerformClick();
        }
    }
}