﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class Frm_ab_setting : MyMetroForm
    {
        public Frm_ab_setting()
        {
            Classes.ClsMain.ChangeCulture("e");
            InitializeComponent();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p77)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 77);

                int auto = 0;
                if (ch_auto.Checked == true)
                    auto = 1;

                if (txt_mah.Text.Trim() == "")
                    txt_mah.Text = "2";

                if (txt_mohlat.Text.Trim() == "")
                    txt_mohlat.Text = "10";
                if (txttejarizarib.Text.Trim() == "")
                    txttejarizarib.Text = "0";
                if (txtZaribSakhtoSaz.Text.Trim() == "")
                    txtZaribSakhtoSaz.Text = "0";

                if (txtDardadMaliat.Text.Trim() == "")
                    txtDardadMaliat.Text = "0";


                try
                {
                    if (Classes.clsAb.IsAbInfoExist() == false)
                        Classes.ClsMain.ExecuteScalar("insert into ab_info (id) values (1)");
                    string com = "update ab_info set zarib_aboonman=" + txt_aboonman.Text + ",mablagh_fazelab=" + txt_fazelab_mablagh.Text + ",tozihat=N'" + txt_tozihat.Text + "',ejraye_automat=" + auto.ToString() + ",tedad_mah_dar_doreh=" + txt_mah.Text + ",mohlat_pardakht=" + txt_mohlat.Text + ",zaribtejari=" + txttejarizarib.Text + ",ZaribDarhalSakht=" + txtZaribSakhtoSaz.Text + ",darsad_maliat=" + txtDardadMaliat.Text + ",hazineh_taviz_kontor=" + hazineh_taviz_kontor.Text;
                    Classes.ClsMain.ExecuteNoneQuery(com);
                    Payam.Show("تغییرات با موفقیت در سیستم ثبت شد");
                    this.Close();

                }
                catch
                {
                    Payam.Show("مقادیر ورودی نامعتبر می باشد . لطفا بررسی نمایید");

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");

        }

        private void FrmManementAbInfo_Load(object sender, EventArgs e)
        {
            DataTable dt = Classes.ClsMain.GetDataTable("select * from ab_info");
            if (dt.Rows.Count != 0)
            {

                txtZaribSakhtoSaz.Text = dt.Rows[0]["ZaribDarhalSakht"].ToString();
                txt_tozihat.Text = dt.Rows[0]["tozihat"].ToString();
                txt_mah.Text = dt.Rows[0]["tedad_mah_dar_doreh"].ToString();
                txt_mohlat.Text = dt.Rows[0]["mohlat_pardakht"].ToString();
                txttejarizarib.Text = dt.Rows[0]["zaribtejari"].ToString();
                ch_auto.Checked = (bool)dt.Rows[0]["ejraye_automat"];
                txtDardadMaliat.Text = dt.Rows[0]["darsad_maliat"].ToString();
                txt_fazelab_mablagh.Text = dt.Rows[0]["mablagh_fazelab"].ToString();
                txt_aboonman.Text = dt.Rows[0]["zarib_aboonman"].ToString();
                hazineh_taviz_kontor.Text = dt.Rows[0]["hazineh_taviz_kontor"].ToString();


            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmManementAbInfo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}