﻿namespace CWMS.Ab
{
    partial class frm_gozaresh_noe_ghabz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtTaviz = new System.Windows.Forms.TextBox();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.txtKharab = new System.Windows.Forms.TextBox();
            this.txtGhat = new System.Windows.Forms.TextBox();
            this.txtDorKamel = new System.Windows.Forms.TextBox();
            this.txtAdamGheraat = new System.Windows.Forms.TextBox();
            this.txtGharardadCnt = new System.Windows.Forms.TextBox();
            this.labelX39 = new DevComponents.DotNetBar.LabelX();
            this.labelX40 = new DevComponents.DotNetBar.LabelX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.labelX19 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.cmbDoreh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.cmbDoreh);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.txtTaviz);
            this.groupPanel1.Controls.Add(this.buttonX2);
            this.groupPanel1.Controls.Add(this.txtKharab);
            this.groupPanel1.Controls.Add(this.txtGhat);
            this.groupPanel1.Controls.Add(this.txtDorKamel);
            this.groupPanel1.Controls.Add(this.txtAdamGheraat);
            this.groupPanel1.Controls.Add(this.txtGharardadCnt);
            this.groupPanel1.Controls.Add(this.labelX39);
            this.groupPanel1.Controls.Add(this.labelX40);
            this.groupPanel1.Controls.Add(this.labelX21);
            this.groupPanel1.Controls.Add(this.labelX35);
            this.groupPanel1.Controls.Add(this.labelX14);
            this.groupPanel1.Controls.Add(this.labelX19);
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.labelX10);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel1.Location = new System.Drawing.Point(22, 3);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(820, 254);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 3;
            this.groupPanel1.Text = "وضعیت قبوض";
            // 
            // txtTaviz
            // 
            this.txtTaviz.BackColor = System.Drawing.Color.White;
            this.txtTaviz.ForeColor = System.Drawing.Color.Black;
            this.txtTaviz.Location = new System.Drawing.Point(89, 160);
            this.txtTaviz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtTaviz.Name = "txtTaviz";
            this.txtTaviz.ReadOnly = true;
            this.txtTaviz.Size = new System.Drawing.Size(82, 28);
            this.txtTaviz.TabIndex = 106;
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(391, 26);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.buttonX2.Size = new System.Drawing.Size(219, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX2.SymbolSize = 12F;
            this.buttonX2.TabIndex = 56;
            this.buttonX2.Text = "نمایش وضعیت قبوض دوره";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // txtKharab
            // 
            this.txtKharab.BackColor = System.Drawing.Color.White;
            this.txtKharab.ForeColor = System.Drawing.Color.Black;
            this.txtKharab.Location = new System.Drawing.Point(89, 83);
            this.txtKharab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtKharab.Name = "txtKharab";
            this.txtKharab.ReadOnly = true;
            this.txtKharab.Size = new System.Drawing.Size(82, 28);
            this.txtKharab.TabIndex = 105;
            // 
            // txtGhat
            // 
            this.txtGhat.BackColor = System.Drawing.Color.White;
            this.txtGhat.ForeColor = System.Drawing.Color.Black;
            this.txtGhat.Location = new System.Drawing.Point(89, 120);
            this.txtGhat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtGhat.Name = "txtGhat";
            this.txtGhat.ReadOnly = true;
            this.txtGhat.Size = new System.Drawing.Size(82, 28);
            this.txtGhat.TabIndex = 104;
            // 
            // txtDorKamel
            // 
            this.txtDorKamel.BackColor = System.Drawing.Color.White;
            this.txtDorKamel.ForeColor = System.Drawing.Color.Black;
            this.txtDorKamel.Location = new System.Drawing.Point(437, 160);
            this.txtDorKamel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtDorKamel.Name = "txtDorKamel";
            this.txtDorKamel.ReadOnly = true;
            this.txtDorKamel.Size = new System.Drawing.Size(82, 28);
            this.txtDorKamel.TabIndex = 103;
            // 
            // txtAdamGheraat
            // 
            this.txtAdamGheraat.BackColor = System.Drawing.Color.White;
            this.txtAdamGheraat.ForeColor = System.Drawing.Color.Black;
            this.txtAdamGheraat.Location = new System.Drawing.Point(437, 120);
            this.txtAdamGheraat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtAdamGheraat.Name = "txtAdamGheraat";
            this.txtAdamGheraat.ReadOnly = true;
            this.txtAdamGheraat.Size = new System.Drawing.Size(82, 28);
            this.txtAdamGheraat.TabIndex = 101;
            // 
            // txtGharardadCnt
            // 
            this.txtGharardadCnt.BackColor = System.Drawing.Color.White;
            this.txtGharardadCnt.ForeColor = System.Drawing.Color.Black;
            this.txtGharardadCnt.Location = new System.Drawing.Point(437, 83);
            this.txtGharardadCnt.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtGharardadCnt.Name = "txtGharardadCnt";
            this.txtGharardadCnt.ReadOnly = true;
            this.txtGharardadCnt.Size = new System.Drawing.Size(82, 28);
            this.txtGharardadCnt.TabIndex = 100;
            // 
            // labelX39
            // 
            this.labelX39.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX39.ForeColor = System.Drawing.Color.Black;
            this.labelX39.Location = new System.Drawing.Point(662, 161);
            this.labelX39.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX39.Name = "labelX39";
            this.labelX39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX39.Size = new System.Drawing.Size(40, 27);
            this.labelX39.Symbol = "";
            this.labelX39.SymbolColor = System.Drawing.Color.Green;
            this.labelX39.SymbolSize = 15F;
            this.labelX39.TabIndex = 99;
            this.labelX39.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX40
            // 
            this.labelX40.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX40.ForeColor = System.Drawing.Color.Black;
            this.labelX40.Location = new System.Drawing.Point(361, 161);
            this.labelX40.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX40.Name = "labelX40";
            this.labelX40.Size = new System.Drawing.Size(281, 27);
            this.labelX40.TabIndex = 98;
            this.labelX40.Text = "قبوض ( دور کامل ) :";
            this.labelX40.WordWrap = true;
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(662, 120);
            this.labelX21.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX21.Name = "labelX21";
            this.labelX21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX21.Size = new System.Drawing.Size(40, 28);
            this.labelX21.Symbol = "";
            this.labelX21.SymbolColor = System.Drawing.Color.Green;
            this.labelX21.SymbolSize = 15F;
            this.labelX21.TabIndex = 97;
            this.labelX21.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX35
            // 
            this.labelX35.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.ForeColor = System.Drawing.Color.Black;
            this.labelX35.Location = new System.Drawing.Point(409, 120);
            this.labelX35.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX35.Name = "labelX35";
            this.labelX35.Size = new System.Drawing.Size(233, 28);
            this.labelX35.TabIndex = 96;
            this.labelX35.Text = "قبوض ( عدم قرائت ) :";
            this.labelX35.WordWrap = true;
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(386, 161);
            this.labelX14.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX14.Name = "labelX14";
            this.labelX14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX14.Size = new System.Drawing.Size(34, 27);
            this.labelX14.Symbol = "";
            this.labelX14.SymbolColor = System.Drawing.Color.Green;
            this.labelX14.SymbolSize = 15F;
            this.labelX14.TabIndex = 95;
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX19
            // 
            this.labelX19.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX19.ForeColor = System.Drawing.Color.Black;
            this.labelX19.Location = new System.Drawing.Point(113, 161);
            this.labelX19.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX19.Name = "labelX19";
            this.labelX19.Size = new System.Drawing.Size(182, 27);
            this.labelX19.TabIndex = 94;
            this.labelX19.Text = "قبوض ( تعویض شده ) :";
            this.labelX19.WordWrap = true;
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(312, 120);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX9.Size = new System.Drawing.Size(34, 28);
            this.labelX9.Symbol = "";
            this.labelX9.SymbolColor = System.Drawing.Color.Green;
            this.labelX9.SymbolSize = 15F;
            this.labelX9.TabIndex = 93;
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(157, 120);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(138, 28);
            this.labelX10.TabIndex = 92;
            this.labelX10.Text = "قبوض (قطع ) :";
            this.labelX10.WordWrap = true;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(312, 83);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX5.Size = new System.Drawing.Size(34, 28);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.Green;
            this.labelX5.SymbolSize = 15F;
            this.labelX5.TabIndex = 91;
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(157, 83);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(138, 28);
            this.labelX6.TabIndex = 90;
            this.labelX6.Text = "قبوض ( خراب ) :";
            this.labelX6.WordWrap = true;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(663, 83);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(39, 28);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Green;
            this.labelX1.SymbolSize = 15F;
            this.labelX1.TabIndex = 87;
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(362, 83);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(283, 28);
            this.labelX2.TabIndex = 13;
            this.labelX2.Text = "تعداد قبوض :";
            this.labelX2.WordWrap = true;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(312, 161);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(34, 27);
            this.labelX3.Symbol = "";
            this.labelX3.SymbolColor = System.Drawing.Color.Green;
            this.labelX3.SymbolSize = 15F;
            this.labelX3.TabIndex = 107;
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.DisplayMember = "Text";
            this.cmbDoreh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDoreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoreh.ForeColor = System.Drawing.Color.Black;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.ItemHeight = 22;
            this.cmbDoreh.Location = new System.Drawing.Point(634, 29);
            this.cmbDoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDoreh.Size = new System.Drawing.Size(84, 28);
            this.cmbDoreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDoreh.TabIndex = 108;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(713, 29);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(76, 24);
            this.labelX7.TabIndex = 109;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // frm_gozaresh_noe_ghabz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 277);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_gozaresh_noe_ghabz";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "گزارش نوع قبوض";
            this.Load += new System.EventHandler(this.frm_gozaresh_noe_ghabz_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.TextBox txtTaviz;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.TextBox txtKharab;
        private System.Windows.Forms.TextBox txtGhat;
        private System.Windows.Forms.TextBox txtDorKamel;
        private System.Windows.Forms.TextBox txtAdamGheraat;
        private System.Windows.Forms.TextBox txtGharardadCnt;
        private DevComponents.DotNetBar.LabelX labelX39;
        private DevComponents.DotNetBar.LabelX labelX40;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX labelX35;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.LabelX labelX19;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDoreh;
        private DevComponents.DotNetBar.LabelX labelX7;
    }
}