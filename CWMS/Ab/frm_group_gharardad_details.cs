﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace CWMS.Ab
{
    public partial class frm_group_gharardad_details : MyMetroForm
    {
        MainAbDatasetTableAdapters.kontor_khan_grouping_detailsTableAdapter ta;
        int Group_id = 0;
        public frm_group_gharardad_details(int group_id,string group_name)
        {
            InitializeComponent();
            ta = new MainAbDatasetTableAdapters.kontor_khan_grouping_detailsTableAdapter();
            Group_id = group_id;
            txt_group_name.Text = group_name;
            
            kontor_khan_grouping_details_by_addTableAdapter.Fill(mainAbDataset.kontor_khan_grouping_details_by_add,Group_id);
            txt_gharardad_in_group_count.Text = mainAbDataset.kontor_khan_grouping_details_by_add.Rows.Count.ToString();
        }
        DataTable dt_moshtarekin;
        void Get_moshtarekin()
        {
             dt_moshtarekin = Classes.ClsMain.GetDataTable(
               "select gharardad as قرارداد,co_name as [نام واحد],address as آدرس"
               + "  from moshtarekin where  have_ab_ghabz=1 and isnull(tarikh_gharardad_ab,1)<>1 and  gharardad not in  " +
               " ( select gharardad from kontor_khan_grouping_details) "
               );
            dgv_moshtarekin.DataSource = dt_moshtarekin;
           
        }

        private void frm_group_gharardad_details_Load(object sender, EventArgs e)
        {
            Get_moshtarekin();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(dgv_gharardad_in_group.SelectedRows.Count!=0)
            {
                int gharardad = Convert.ToInt32(dgv_gharardad_in_group.SelectedRows[0].Cells[0].Value);
                Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan_grouping_details where gharardad=" + gharardad.ToString());
                kontor_khan_grouping_details_by_addTableAdapter.Fill(mainAbDataset.kontor_khan_grouping_details_by_add, Group_id);

                Get_moshtarekin();
                txt_gharardad_in_group_count.Text = mainAbDataset.kontor_khan_grouping_details_by_add.Rows.Count.ToString();



            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(dgv_moshtarekin.SelectedRows.Count!=0)
            {
                ta.Insert(
                    Convert.ToInt32(dgv_moshtarekin.SelectedRows[0].Cells[0].Value),
                    Group_id
                    );
                kontor_khan_grouping_details_by_addTableAdapter.Fill(mainAbDataset.kontor_khan_grouping_details_by_add, Group_id);

                dgv_moshtarekin.Rows.Remove(dgv_moshtarekin.SelectedRows[0]);
                txt_gharardad_in_group_count.Text = dgv_gharardad_in_group.Rows.Count.ToString();

            }
        }

        private void frm_group_gharardad_details_FormClosing(object sender, FormClosingEventArgs e)
        {
            Classes.ClsMain.ExecuteNoneQuery("update kontor_khan_gharardad_grouping set gharardad_in_group=" + txt_gharardad_in_group_count.Text + " where id=" + Group_id);
        }
    }
}
