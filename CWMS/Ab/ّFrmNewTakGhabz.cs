﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Ab
{
    public partial class _ّFrmNewTakGhabz : DevComponents.DotNetBar.Metro.MetroForm
    {
        string gharadadOfuser, dorehOfAb;
        // در حالتی که مقادیر از کنتورخوان خوانده شده اما کاربر قصد دارید به صورت دستی مشترک دیگری را به لیست کنتورخوان اضافه نماید
        // عملیات ثبت قبض در مرحله بعد انجام خواهد شد .
        bool JustForKonotrkhanNotNewGhabzCreation = false;
        public _ّFrmNewTakGhabz(string doreh, string gharardad, bool JustForKonotrkhan = false)
        {
            InitializeComponent();
            Classes.ClsMain.ChangeCulture("f");
            gharadadOfuser = gharardad;
            JustForKonotrkhanNotNewGhabzCreation = JustForKonotrkhan;

            dorehOfAb = doreh;
            try
            {
                if (doreh == "1")
                    txtGhabli.Text = "0";
                else
                {
                    try
                    {
                        txtGhabli.Text = Classes.ClsMain.ExecuteScalar("select kontor_end from ab_ghabz where gharardad=" + gharadadOfuser + " and dcode=" +
                  Classes.clsDoreh.Get_last_Ab_dcode_of_moshtarek(Convert.ToInt32(gharardad))).ToString();
                    }
                    catch (Exception)
                    {

                        txtGhabli.Text = "0";
                    }
                }

                chAdameGheraat.Checked = chGhat.Checked = chTaviz.Checked = chKarab.Checked = chSakhtoSaz.Checked = chTaviz.Checked = false;
                txtfelli.Focus();
                txtGhabli.Tag = txtGhabli.Text;
                txtdate.SelectedDateTime = DateTime.Now;

            }
            catch (Exception tt)
            {
                Payam.Show("امکان ثبت قبض جدید برای مشترک وجود ندارد . لطفا با پشتیبان هماهنگ کنید");
                Classes.ClsMain.logError(tt);

            }
        }

        private void _ّFrmNewTakGhabz_Load(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("f");
            txtfelli.Focus();
            txtdate.SelectedDateTime = DateTime.Now;
        }

        private void textBoxX1_TextChanged(object sender, EventArgs e)
        {

        }


        int getbool(Boolean s)
        {
            if (s == true)
                return 1;
            return 0;
        }
        private void btn_create_new_ghabz_Click(object sender, EventArgs e)
        {
            try
            {

                Convert.ToDateTime(txtdate.SelectedDateTime);

                int felli = Convert.ToInt32(txtfelli.Text);
                if (Convert.ToInt32(txtGhabli.Tag) > felli)
                    Payam.Show("مقدار کنتور فعلی نمی تواند از مقدار کنتور قبلی کمتر باشد");
                else if(felli> Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select kontor_ragham from moshtarekin where gharardad=" + gharadadOfuser)))
                {

                    Payam.Show("رقم کنتور وارد شده از حداکثر رقم دستگاه کنتور آب بیشتر است .");
                }
                else
                {
                    Classes.ClsMain.ChangeCulture("e");

                    Classes.ClsMain.ExecuteNoneQuery("insert into kontor_khan values (" + gharadadOfuser + ",'" + txtdate.SelectedDateTime + "',"
                        + txtfelli.Text + "," + getbool(chAdameGheraat.Checked) + "," + getbool(chSakhtoSaz.Checked) + "," + getbool(chKarab.Checked) + ","
                        + getbool(chGhat.Checked) + "," + getbool(chDoreKamel.Checked) + "," + getbool(chTaviz.Checked) + ")");



                    if (JustForKonotrkhanNotNewGhabzCreation == false)
                    {
                        Classes.clsGhabz ghabzinfo = new Classes.clsGhabz(Convert.ToInt32(gharadadOfuser));
                        new Classes.clsAb().Generate_Ghabz(ghabzinfo);
                        ManageKontorKhanInfo(dorehOfAb, gharadadOfuser);
                        
                    }



                    Classes.ClsMain.ChangeCulture("f");
                    Classes.ClsMain.NewTakGabzStat = true;
                    this.Close();

                }
            }
            catch (Exception ert)
            {
                Payam.Show("شماره کنتور فعلی نامعتبر می باشد .");
                Classes.ClsMain.NewTakGabzStat = false;
                Classes.ClsMain.logError(ert,"در هنگام ایجاد قبض جدید");

            }



        }
        void ManageKontorKhanInfo(string dcode,string gharardad)
        {
            Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan_history where dcode=" + dcode + " and gharardad=" + gharardad + ";insert into kontor_khan_history select " + dcode + " ,* from kontor_khan;delete from kontor_khan;");

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.NewTakGabzStat = false;
            this.Close();
        }

        private void txtfelli_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_create_new_ghabz.PerformClick();
        }

       
    }
}
