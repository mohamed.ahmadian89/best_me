﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
namespace CWMS.Ab
{
    public partial class Frm_CreateKontorKhan_input : DevComponents.DotNetBar.Metro.MetroForm
    {
        public Frm_CreateKontorKhan_input()
        {
            InitializeComponent();
        }

        private void Frm_CreateKontorKhan_input_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset.kontor_khan_gharardad_grouping' table. You can move, or remove it, as needed.
            kontor_khan_gharardad_groupingTableAdapter.Fill(mainAbDataset.kontor_khan_gharardad_grouping);




        }



        int changeBoolType(bool k)
        {
            return (k == true ? 1 : 0);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            OpenFileDialog opendiag = new OpenFileDialog();
            if (opendiag.ShowDialog() == DialogResult.OK)
            {

                Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan");

                var list_of_info = JsonConvert.DeserializeObject<dynamic>(System.IO.File.ReadAllText(opendiag.FileName));

                for (int i = 0; i < list_of_info.Count; i++)
                {

                    // یادت باشه که حتما مقدار در حال ساخت را اضافه کنیم به برنامه چون الان وجود نداره وبه جایش عدم قرائت گذاشته می شه
                    Classes.ClsMain.ExecuteNoneQuery(
                        "insert into kontor_khan values (" +
                        list_of_info[i]["gharardad"].ToString() + ",'" + Convert.ToDateTime(list_of_info[i]["tarikh_gheraat"]) + "'," + list_of_info[i]["kontor_feli"].ToString() + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["adam_gheraat"])) + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["dar_hal_sakht"])) + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["kharab"])) + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["ghat"])) + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["dor_kamel"])) + "," +
                        changeBoolType(Convert.ToBoolean(list_of_info[i]["taviz"])) + "," +
                        "'" + "11'"


                        + ")"
                        );
                }



                Payam.Show("کلیه اطلاعات در سامانه با موفقیت ثبت شدند");
            }
        }

        private void btn_create_new_ghabz_Click_1(object sender, EventArgs e)
        {
            try
            {
                DataTable dt_gharardad_of_group = Classes.ClsMain.GetDataTable("select gharardad from kontor_khan_grouping_details where group_id=" + cmb_group.SelectedValue.ToString());
                if (dt_gharardad_of_group.Rows.Count == 0)
                {
                    Payam.Show("برای این گروه هیچ قراردادی تعیین نشده است . لطفا به مدیریت گروه ها مراجعه کنید");
                    return;
                }
                FolderBrowserDialog opendiag = new FolderBrowserDialog();
                if (opendiag.ShowDialog() == DialogResult.OK)
                {
                    Payam.Show("لطفا تا اتمام تولید لیست خروجی چند لحظه صبر نمایید");
                    string gharardad = "";
                    for (int i = 0; i < dt_gharardad_of_group.Rows.Count; i++)
                    {
                        gharardad += dt_gharardad_of_group.Rows[i][0].ToString() + ",";
                    }
                    gharardad = gharardad.Substring(0, gharardad.Length - 1);
                    object[] ot = new object[2];
                    ot[0] = gharardad;
                    if (System.IO.Directory.Exists(opendiag.SelectedPath + "\\"+ cmb_group.Text))
                        System.IO.Directory.Delete(opendiag.SelectedPath + "\\" + cmb_group.Text,true);
                    System.IO.Directory.CreateDirectory(opendiag.SelectedPath + "\\" + cmb_group.Text);
                    ot[1] = opendiag.SelectedPath + "\\" + cmb_group.Text;
                    System.Threading.Thread th = new System.Threading.Thread(

                        new System.Threading.ParameterizedThreadStart(Make_list));
                    th.Start(ot);
                    
                }



            }
            catch (Exception ert)
            {
                Payam.Show("متاسفانه در هنگام ایجاد فایل خروجی خطا به وجود امده است . لطفا با پشتیبان هماهنگ فرمایید");
                Classes.ClsMain.logError(ert);
            }
        }

        static void Make_list(object temp)
        {

            object[] ot = (object[])temp;
            string path = ot[1].ToString();
            string gharardad = ot[0].ToString();


            int last_doreh = Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select top(1) dcode from ab_doreh order by dcode desc"));
            DataTable dt_moshtarekin = Classes.ClsMain.GetDataTable("select gharardad,co_name,address from moshtarekin where have_ab_ghabz=1 and gharardad in ( " + gharardad + " )");
            DataTable dt_last_ghabz = Classes.ClsMain.GetDataTable("select gharardad,kontor_end from ab_ghabz where dcode=" + (last_doreh - 1).ToString() + " and gharardad in ( " + gharardad + " )");

            List<Dictionary<string, object>> list_of_info = new List<Dictionary<string, object>>();
            for (int i = 0; i < dt_moshtarekin.Rows.Count; i++)
            {
                Dictionary<string, object> info = new Dictionary<string, object>();
                info["gharardad"] = dt_moshtarekin.Rows[i]["gharardad"].ToString();
                info["name"] = dt_moshtarekin.Rows[i]["co_name"].ToString();
                info["address"] = dt_moshtarekin.Rows[i]["address"].ToString();

                try
                {
                    var last_moshtarek_ghabz = dt_last_ghabz.AsEnumerable().Where(p => p.Field<int>("gharardad") == Convert.ToInt32(info["gharardad"])).First();
                    info["kontor_ghabl"] = Convert.ToInt32(last_moshtarek_ghabz["kontor_end"]);
                }
                catch (Exception)
                {
                    info["kontor_ghabl"] = 0;
                }
                info["kontor_feli"] = 0;
                info["checked"] =
                info["dar_hal_sakht"] =
                info["ghat"] =
                info["dor_kamel"] =
                info["kharab"] =
                info["taviz"] =
                info["adam_gheraat"] = false;

                list_of_info.Add(info);
            }

            string input_json = JsonConvert.SerializeObject(list_of_info);


            System.IO.File.WriteAllText(path + "\\input.json", input_json, Encoding.UTF8);
            System.Diagnostics.Process.Start(path.ToString());

        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            new Ab.frm_group_gharardad_for_kontorkhan().ShowDialog();
            kontor_khan_gharardad_groupingTableAdapter.Fill(mainAbDataset.kontor_khan_gharardad_grouping);

        }
    }
}
