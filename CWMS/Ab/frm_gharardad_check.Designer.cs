﻿namespace CWMS.Ab
{
    partial class frm_gharardad_check
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cmb_gharardad = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnChap = new DevComponents.DotNetBar.ButtonX();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(68, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(252, 23);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "لطفا اطلاعات قرارداهای زیر را بررسی نمایید";
            // 
            // cmb_gharardad
            // 
            this.cmb_gharardad.DisplayMember = "Text";
            this.cmb_gharardad.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_gharardad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cmb_gharardad.FormattingEnabled = true;
            this.cmb_gharardad.ItemHeight = 20;
            this.cmb_gharardad.Location = new System.Drawing.Point(12, 41);
            this.cmb_gharardad.Name = "cmb_gharardad";
            this.cmb_gharardad.Size = new System.Drawing.Size(308, 321);
            this.cmb_gharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmb_gharardad.TabIndex = 1;
            // 
            // btnChap
            // 
            this.btnChap.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChap.BackColor = System.Drawing.Color.Transparent;
            this.btnChap.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnChap.Location = new System.Drawing.Point(29, 378);
            this.btnChap.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnChap.Name = "btnChap";
            this.btnChap.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 7, 8, 0);
            this.btnChap.Size = new System.Drawing.Size(259, 27);
            this.btnChap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnChap.Symbol = "";
            this.btnChap.SymbolColor = System.Drawing.Color.Teal;
            this.btnChap.SymbolSize = 12F;
            this.btnChap.TabIndex = 82;
            this.btnChap.Text = "تنظیم تاریخ قرائت اولین دوره";
            this.btnChap.Click += new System.EventHandler(this.btnChap_Click);
            // 
            // frm_gharardad_check
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 431);
            this.Controls.Add(this.btnChap);
            this.Controls.Add(this.cmb_gharardad);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_gharardad_check";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "قراردادهای مربوطه";
            this.Load += new System.EventHandler(this.frm_gharardad_check_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmb_gharardad;
        private DevComponents.DotNetBar.ButtonX btnChap;
    }
}