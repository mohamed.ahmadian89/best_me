﻿namespace CWMS.Ab
{
    partial class Frm_ab_mablagh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvMabalegh = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhtasvibDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.tarikhejraDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hesabDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaribdirkardDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abmablaghBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset1 = new CWMS.Ab.MainAbDataset();
            this.mainDataSest = new CWMS.MainDataSest();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.btnDelete = new DevComponents.DotNetBar.ButtonX();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.btnAdd = new DevComponents.DotNetBar.ButtonX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.txtZrib = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtHesab = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtMablagh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.tarikhEjra = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.tarikhTasvib = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.ab_mablaghTableAdapter = new CWMS.MainDataSestTableAdapters.ab_mablaghTableAdapter();
            this.ab_mablaghTableAdapter1 = new CWMS.Ab.MainAbDatasetTableAdapters.ab_mablaghTableAdapter();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMabalegh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abmablaghBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.dgvMabalegh);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(40, 252);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1079, 366);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 3;
            this.groupPanel2.Text = "کلیه مبالغ تعریف شده";
            // 
            // dgvMabalegh
            // 
            this.dgvMabalegh.AllowUserToAddRows = false;
            this.dgvMabalegh.AllowUserToDeleteRows = false;
            this.dgvMabalegh.AllowUserToResizeColumns = false;
            this.dgvMabalegh.AllowUserToResizeRows = false;
            this.dgvMabalegh.AutoGenerateColumns = false;
            this.dgvMabalegh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMabalegh.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMabalegh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvMabalegh.ColumnHeadersHeight = 30;
            this.dgvMabalegh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.tarikhtasvibDataGridViewTextBoxColumn,
            this.tarikhejraDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn,
            this.hesabDataGridViewTextBoxColumn,
            this.zaribdirkardDataGridViewTextBoxColumn});
            this.dgvMabalegh.DataSource = this.abmablaghBindingSource;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMabalegh.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvMabalegh.EnableHeadersVisualStyles = false;
            this.dgvMabalegh.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvMabalegh.Location = new System.Drawing.Point(38, 15);
            this.dgvMabalegh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvMabalegh.Name = "dgvMabalegh";
            this.dgvMabalegh.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMabalegh.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvMabalegh.RowHeadersVisible = false;
            this.dgvMabalegh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMabalegh.Size = new System.Drawing.Size(1011, 306);
            this.dgvMabalegh.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.FillWeight = 50F;
            this.idDataGridViewTextBoxColumn.HeaderText = "کد";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhtasvibDataGridViewTextBoxColumn
            // 
            this.tarikhtasvibDataGridViewTextBoxColumn.DataPropertyName = "tarikh_tasvib";
            this.tarikhtasvibDataGridViewTextBoxColumn.HeaderText = "تاریخ تصویب";
            this.tarikhtasvibDataGridViewTextBoxColumn.Name = "tarikhtasvibDataGridViewTextBoxColumn";
            this.tarikhtasvibDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhtasvibDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhtasvibDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tarikhejraDataGridViewTextBoxColumn
            // 
            this.tarikhejraDataGridViewTextBoxColumn.DataPropertyName = "tarikh_ejra";
            this.tarikhejraDataGridViewTextBoxColumn.HeaderText = "تاریخ اجرا";
            this.tarikhejraDataGridViewTextBoxColumn.Name = "tarikhejraDataGridViewTextBoxColumn";
            this.tarikhejraDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhejraDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhejraDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            this.mablaghDataGridViewTextBoxColumn.FillWeight = 50F;
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "اب بها";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hesabDataGridViewTextBoxColumn
            // 
            this.hesabDataGridViewTextBoxColumn.DataPropertyName = "hesab";
            this.hesabDataGridViewTextBoxColumn.HeaderText = "شماره حساب";
            this.hesabDataGridViewTextBoxColumn.Name = "hesabDataGridViewTextBoxColumn";
            this.hesabDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // zaribdirkardDataGridViewTextBoxColumn
            // 
            this.zaribdirkardDataGridViewTextBoxColumn.DataPropertyName = "zarib_dirkard";
            this.zaribdirkardDataGridViewTextBoxColumn.FillWeight = 30F;
            this.zaribdirkardDataGridViewTextBoxColumn.HeaderText = "دیرکرد";
            this.zaribdirkardDataGridViewTextBoxColumn.Name = "zaribdirkardDataGridViewTextBoxColumn";
            this.zaribdirkardDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaribdirkardDataGridViewTextBoxColumn.Visible = false;
            // 
            // abmablaghBindingSource
            // 
            this.abmablaghBindingSource.DataMember = "ab_mablagh";
            this.abmablaghBindingSource.DataSource = this.mainAbDataset1;
            // 
            // mainAbDataset1
            // 
            this.mainAbDataset1.DataSetName = "MainAbDataset";
            this.mainAbDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mainDataSest
            // 
            this.mainDataSest.DataSetName = "MainDataSest";
            this.mainDataSest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.labelX9);
            this.groupPanel1.Controls.Add(this.labelX8);
            this.groupPanel1.Controls.Add(this.btnDelete);
            this.groupPanel1.Controls.Add(this.btnSave);
            this.groupPanel1.Controls.Add(this.btnAdd);
            this.groupPanel1.Controls.Add(this.labelX6);
            this.groupPanel1.Controls.Add(this.txtZrib);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txtHesab);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.txtMablagh);
            this.groupPanel1.Controls.Add(this.labelX3);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.tarikhEjra);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.Controls.Add(this.tarikhTasvib);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(40, 15);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1079, 231);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 2;
            this.groupPanel1.Text = "اطلاعات مربوطه";
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(992, 172);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(35, 23);
            this.labelX9.Symbol = "";
            this.labelX9.SymbolColor = System.Drawing.Color.Teal;
            this.labelX9.SymbolSize = 12F;
            this.labelX9.TabIndex = 17;
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(992, 147);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(35, 23);
            this.labelX8.Symbol = "";
            this.labelX8.SymbolColor = System.Drawing.Color.Teal;
            this.labelX8.SymbolSize = 12F;
            this.labelX8.TabIndex = 16;
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDelete.Location = new System.Drawing.Point(78, 111);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(106, 27);
            this.btnDelete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDelete.Symbol = "";
            this.btnDelete.SymbolColor = System.Drawing.Color.Maroon;
            this.btnDelete.SymbolSize = 12F;
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "حذف";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSave.Location = new System.Drawing.Point(194, 111);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(106, 27);
            this.btnSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSave.Symbol = "";
            this.btnSave.SymbolColor = System.Drawing.Color.Green;
            this.btnSave.SymbolSize = 12F;
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "ثبت";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAdd.Location = new System.Drawing.Point(309, 111);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 27);
            this.btnAdd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAdd.Symbol = "";
            this.btnAdd.SymbolColor = System.Drawing.Color.Green;
            this.btnAdd.SymbolSize = 12F;
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "اضافه";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(12, 138);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(970, 42);
            this.labelX6.TabIndex = 14;
            this.labelX6.Text = "کاربرگرامی! جهت ثبت دوره جدید ، ابتدا بر روی دکمه اضافه کلیک کرده ، مقادیر را وار" +
    "د نمایید و سپس بر روی دکمه ثبت کلیک نمایید";
            this.labelX6.WordWrap = true;
            // 
            // txtZrib
            // 
            this.txtZrib.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtZrib.Border.Class = "TextBoxBorder";
            this.txtZrib.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtZrib.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.abmablaghBindingSource, "zarib_dirkard", true));
            this.txtZrib.DisabledBackColor = System.Drawing.Color.White;
            this.txtZrib.ForeColor = System.Drawing.Color.Black;
            this.txtZrib.Location = new System.Drawing.Point(78, 52);
            this.txtZrib.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtZrib.Name = "txtZrib";
            this.txtZrib.PreventEnterBeep = true;
            this.txtZrib.Size = new System.Drawing.Size(86, 28);
            this.txtZrib.TabIndex = 9;
            this.txtZrib.Visible = false;
            this.txtZrib.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZrib_KeyDown);
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(172, 52);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(93, 27);
            this.labelX5.TabIndex = 8;
            this.labelX5.Text = "ضریب دیرکرد:";
            this.labelX5.Visible = false;
            // 
            // txtHesab
            // 
            this.txtHesab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtHesab.Border.Class = "TextBoxBorder";
            this.txtHesab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtHesab.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.abmablaghBindingSource, "hesab", true));
            this.txtHesab.DisabledBackColor = System.Drawing.Color.White;
            this.txtHesab.ForeColor = System.Drawing.Color.Black;
            this.txtHesab.Location = new System.Drawing.Point(309, 52);
            this.txtHesab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtHesab.Name = "txtHesab";
            this.txtHesab.PreventEnterBeep = true;
            this.txtHesab.Size = new System.Drawing.Size(242, 28);
            this.txtHesab.TabIndex = 7;
            this.txtHesab.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHesab_KeyDown);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(560, 53);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(84, 27);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "شماره حساب :";
            // 
            // txtMablagh
            // 
            this.txtMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablagh.Border.Class = "TextBoxBorder";
            this.txtMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablagh.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.abmablaghBindingSource, "mablagh", true));
            this.txtMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablagh.ForeColor = System.Drawing.Color.Black;
            this.txtMablagh.Location = new System.Drawing.Point(719, 54);
            this.txtMablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMablagh.Name = "txtMablagh";
            this.txtMablagh.PreventEnterBeep = true;
            this.txtMablagh.Size = new System.Drawing.Size(171, 28);
            this.txtMablagh.TabIndex = 5;
            this.txtMablagh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMablagh_KeyDown);
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(968, 55);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 27);
            this.labelX3.TabIndex = 4;
            this.labelX3.Text = "مبلغ آب بها :";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(560, 20);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(84, 27);
            this.labelX2.TabIndex = 3;
            this.labelX2.Text = "تاریخ اجرا :";
            // 
            // tarikhEjra
            // 
            this.tarikhEjra.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.abmablaghBindingSource, "tarikh_ejra", true));
            this.tarikhEjra.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.tarikhEjra.Location = new System.Drawing.Point(380, 20);
            this.tarikhEjra.Name = "tarikhEjra";
            this.tarikhEjra.Size = new System.Drawing.Size(171, 24);
            this.tarikhEjra.TabIndex = 2;
            this.tarikhEjra.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(957, 20);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(86, 27);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "تاریخ تصویب :";
            // 
            // tarikhTasvib
            // 
            this.tarikhTasvib.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.abmablaghBindingSource, "tarikh_tasvib", true));
            this.tarikhTasvib.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.tarikhTasvib.Location = new System.Drawing.Point(719, 20);
            this.tarikhTasvib.Name = "tarikhTasvib";
            this.tarikhTasvib.Size = new System.Drawing.Size(171, 24);
            this.tarikhTasvib.TabIndex = 0;
            this.tarikhTasvib.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(12, 163);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(970, 42);
            this.labelX7.TabIndex = 15;
            this.labelX7.Text = "جهت ویرایش اطلاعات کافیست ان را از کادر زیر انتخاب کرده و پس از تغییر مقادیر بر ر" +
    "وی دکمه ثبت کلیک نمایید";
            this.labelX7.WordWrap = true;
            // 
            // ab_mablaghTableAdapter
            // 
            this.ab_mablaghTableAdapter.ClearBeforeFill = true;
            // 
            // ab_mablaghTableAdapter1
            // 
            this.ab_mablaghTableAdapter1.ClearBeforeFill = true;
            // 
            // Frm_ab_mablagh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 623);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_mablagh";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAbMabalegh_FormClosed);
            this.Load += new System.EventHandler(this.FrmAbMabalegh_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAbMabalegh_KeyDown);
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMabalegh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abmablaghBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMabalegh;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.ButtonX btnDelete;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private DevComponents.DotNetBar.ButtonX btnAdd;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtHesab;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMablagh;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private FarsiLibrary.Win.Controls.FADatePicker tarikhEjra;
        private DevComponents.DotNetBar.LabelX labelX1;
        private FarsiLibrary.Win.Controls.FADatePicker tarikhTasvib;
        private DevComponents.DotNetBar.LabelX labelX7;
        private MainDataSest mainDataSest;
        private System.Windows.Forms.BindingSource abmablaghBindingSource;
        private MainDataSestTableAdapters.ab_mablaghTableAdapter ab_mablaghTableAdapter;
        private MainAbDataset mainAbDataset1;
        private MainAbDatasetTableAdapters.ab_mablaghTableAdapter ab_mablaghTableAdapter1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtZrib;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhtasvibDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhejraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hesabDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaribdirkardDataGridViewTextBoxColumn;
    }
}