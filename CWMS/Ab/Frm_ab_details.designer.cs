﻿namespace CWMS.Ab
{
    partial class Frm_ab_details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX4 = new DevComponents.DotNetBar.ButtonX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.lbllocked = new System.Windows.Forms.Label();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.txt_taviz_kontor_hazineh = new CWMS.MoneyTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt_masraf_omoomi = new CWMS.MoneyTextBox();
            this.txt_shenase_pardakht = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtAboonman = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label17 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txt_shenase_ghabz = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label32 = new System.Windows.Forms.Label();
            this.txt_sayer = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtBestankar = new CWMS.MoneyTextBox();
            this.lblBestankar = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtBestankariFromLastDoreh = new CWMS.MoneyTextBox();
            this.txtfazelab = new CWMS.MoneyTextBox();
            this.txtZaribFazelab = new CWMS.FloatTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chSakhtoSaz = new System.Windows.Forms.CheckBox();
            this.chtejari = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_hazineh_taviz = new DevComponents.DotNetBar.ButtonX();
            this.chGhat = new System.Windows.Forms.CheckBox();
            this.chAdameGheraat = new System.Windows.Forms.CheckBox();
            this.chTaviz = new System.Windows.Forms.CheckBox();
            this.chDoreKamel = new System.Windows.Forms.CheckBox();
            this.chKarab = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_tarikh_gheraat_habli = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.txt_gheraat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txt_ghabz_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMohlat = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txtTarikhSoddor = new FarsiLibrary.Win.Controls.FADatePicker();
            this.txt_ensheab = new CWMS.FloatTextBox();
            this.txtTarefe = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtdoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtMojaz = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtMasrafKol = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label27 = new System.Windows.Forms.Label();
            this.txtGheirMojaz = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtFelli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGhabli = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txt_kasr = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPardakhti = new CWMS.MoneyTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txBaghimandeh = new CWMS.MoneyTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDarsad = new CWMS.FloatTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtJarimehTozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txt_jarimeh = new CWMS.MoneyTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_tozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtBedehi = new CWMS.MoneyTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_sayer_tozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtMablaghKol = new CWMS.MoneyTextBox();
            this.txtMaliat = new CWMS.MoneyTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMablaghHoroof = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmb_vaziat = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.txtMablagh = new CWMS.MoneyTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.groupPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.buttonX1);
            this.groupPanel2.Controls.Add(this.buttonX4);
            this.groupPanel2.Controls.Add(this.buttonX2);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(23, 663);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1307, 68);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 31;
            this.groupPanel2.Text = "منوی عملیات";
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(277, 2);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(250, 28);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Green;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 35;
            this.buttonX3.Text = "صدور مجدد این قبض";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(801, 5);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX1.Size = new System.Drawing.Size(237, 27);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Green;
            this.buttonX1.SymbolSize = 9F;
            this.buttonX1.TabIndex = 34;
            this.buttonX1.Text = "چاپ قبض ( با پس زمینه )";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click_1);
            // 
            // buttonX4
            // 
            this.buttonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX4.BackColor = System.Drawing.Color.Transparent;
            this.buttonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX4.Location = new System.Drawing.Point(1046, 5);
            this.buttonX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX4.Name = "buttonX4";
            this.buttonX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonX4.Size = new System.Drawing.Size(171, 27);
            this.buttonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX4.Symbol = "";
            this.buttonX4.SymbolColor = System.Drawing.Color.Green;
            this.buttonX4.SymbolSize = 9F;
            this.buttonX4.TabIndex = 33;
            this.buttonX4.Text = "چاپ قبض";
            this.buttonX4.Click += new System.EventHandler(this.buttonX4_Click);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(9, 3);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Size = new System.Drawing.Size(259, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Green;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 26;
            this.buttonX2.Text = "مشاهده پرداخت ها";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // lbllocked
            // 
            this.lbllocked.AutoSize = true;
            this.lbllocked.BackColor = System.Drawing.Color.Transparent;
            this.lbllocked.ForeColor = System.Drawing.Color.Black;
            this.lbllocked.Location = new System.Drawing.Point(299, 17);
            this.lbllocked.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbllocked.Name = "lbllocked";
            this.lbllocked.Size = new System.Drawing.Size(0, 21);
            this.lbllocked.TabIndex = 30;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.label35);
            this.groupPanel1.Controls.Add(this.txt_taviz_kontor_hazineh);
            this.groupPanel1.Controls.Add(this.label33);
            this.groupPanel1.Controls.Add(this.txt_masraf_omoomi);
            this.groupPanel1.Controls.Add(this.txt_shenase_pardakht);
            this.groupPanel1.Controls.Add(this.txtAboonman);
            this.groupPanel1.Controls.Add(this.label17);
            this.groupPanel1.Controls.Add(this.label31);
            this.groupPanel1.Controls.Add(this.txt_shenase_ghabz);
            this.groupPanel1.Controls.Add(this.label32);
            this.groupPanel1.Controls.Add(this.txt_sayer);
            this.groupPanel1.Controls.Add(this.txtBestankar);
            this.groupPanel1.Controls.Add(this.lblBestankar);
            this.groupPanel1.Controls.Add(this.label25);
            this.groupPanel1.Controls.Add(this.TxtBestankariFromLastDoreh);
            this.groupPanel1.Controls.Add(this.txtfazelab);
            this.groupPanel1.Controls.Add(this.txtZaribFazelab);
            this.groupPanel1.Controls.Add(this.label24);
            this.groupPanel1.Controls.Add(this.groupBox5);
            this.groupPanel1.Controls.Add(this.groupBox4);
            this.groupPanel1.Controls.Add(this.groupBox3);
            this.groupPanel1.Controls.Add(this.groupBox2);
            this.groupPanel1.Controls.Add(this.groupBox1);
            this.groupPanel1.Controls.Add(this.txt_kasr);
            this.groupPanel1.Controls.Add(this.label21);
            this.groupPanel1.Controls.Add(this.txtPardakhti);
            this.groupPanel1.Controls.Add(this.label20);
            this.groupPanel1.Controls.Add(this.txBaghimandeh);
            this.groupPanel1.Controls.Add(this.label15);
            this.groupPanel1.Controls.Add(this.txtDarsad);
            this.groupPanel1.Controls.Add(this.label9);
            this.groupPanel1.Controls.Add(this.label14);
            this.groupPanel1.Controls.Add(this.txtJarimehTozihat);
            this.groupPanel1.Controls.Add(this.txt_jarimeh);
            this.groupPanel1.Controls.Add(this.label10);
            this.groupPanel1.Controls.Add(this.label8);
            this.groupPanel1.Controls.Add(this.txt_tozihat);
            this.groupPanel1.Controls.Add(this.txtBedehi);
            this.groupPanel1.Controls.Add(this.label19);
            this.groupPanel1.Controls.Add(this.label18);
            this.groupPanel1.Controls.Add(this.txt_sayer_tozihat);
            this.groupPanel1.Controls.Add(this.txtMablaghKol);
            this.groupPanel1.Controls.Add(this.txtMaliat);
            this.groupPanel1.Controls.Add(this.label13);
            this.groupPanel1.Controls.Add(this.label12);
            this.groupPanel1.Controls.Add(this.label11);
            this.groupPanel1.Controls.Add(this.txtMablaghHoroof);
            this.groupPanel1.Controls.Add(this.cmb_vaziat);
            this.groupPanel1.Controls.Add(this.label7);
            this.groupPanel1.Controls.Add(this.btn_save);
            this.groupPanel1.Controls.Add(this.txtMablagh);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Controls.Add(this.label30);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.groupPanel1.Location = new System.Drawing.Point(23, 8);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1307, 647);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 29;
            this.groupPanel1.Text = "اطلاعات قبض آب";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(871, 388);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(97, 17);
            this.label35.TabIndex = 179;
            this.label35.Text = "هزینه تعویض کنتور :";
            // 
            // txt_taviz_kontor_hazineh
            // 
            this.txt_taviz_kontor_hazineh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_taviz_kontor_hazineh.Border.Class = "TextBoxBorder";
            this.txt_taviz_kontor_hazineh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_taviz_kontor_hazineh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_taviz_kontor_hazineh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_taviz_kontor_hazineh.ForeColor = System.Drawing.Color.Black;
            this.txt_taviz_kontor_hazineh.Location = new System.Drawing.Point(762, 383);
            this.txt_taviz_kontor_hazineh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_taviz_kontor_hazineh.Name = "txt_taviz_kontor_hazineh";
            this.txt_taviz_kontor_hazineh.Size = new System.Drawing.Size(101, 28);
            this.txt_taviz_kontor_hazineh.TabIndex = 178;
            this.txt_taviz_kontor_hazineh.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(675, 389);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(71, 17);
            this.label33.TabIndex = 177;
            this.label33.Text = "مصرف عمومی :";
            // 
            // txt_masraf_omoomi
            // 
            this.txt_masraf_omoomi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_masraf_omoomi.Border.Class = "TextBoxBorder";
            this.txt_masraf_omoomi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_masraf_omoomi.DisabledBackColor = System.Drawing.Color.White;
            this.txt_masraf_omoomi.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_masraf_omoomi.ForeColor = System.Drawing.Color.Black;
            this.txt_masraf_omoomi.Location = new System.Drawing.Point(566, 384);
            this.txt_masraf_omoomi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_masraf_omoomi.Name = "txt_masraf_omoomi";
            this.txt_masraf_omoomi.PreventEnterBeep = true;
            this.txt_masraf_omoomi.ReadOnly = true;
            this.txt_masraf_omoomi.Size = new System.Drawing.Size(101, 28);
            this.txt_masraf_omoomi.TabIndex = 176;
            this.txt_masraf_omoomi.Text = "0";
            // 
            // txt_shenase_pardakht
            // 
            this.txt_shenase_pardakht.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_shenase_pardakht.Border.Class = "TextBoxBorder";
            this.txt_shenase_pardakht.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_shenase_pardakht.DisabledBackColor = System.Drawing.Color.White;
            this.txt_shenase_pardakht.Enabled = false;
            this.txt_shenase_pardakht.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_shenase_pardakht.ForeColor = System.Drawing.Color.Black;
            this.txt_shenase_pardakht.Location = new System.Drawing.Point(369, 596);
            this.txt_shenase_pardakht.Margin = new System.Windows.Forms.Padding(4);
            this.txt_shenase_pardakht.Name = "txt_shenase_pardakht";
            this.txt_shenase_pardakht.PreventEnterBeep = true;
            this.txt_shenase_pardakht.Size = new System.Drawing.Size(122, 26);
            this.txt_shenase_pardakht.TabIndex = 175;
            // 
            // txtAboonman
            // 
            this.txtAboonman.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAboonman.Border.Class = "TextBoxBorder";
            this.txtAboonman.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAboonman.DisabledBackColor = System.Drawing.Color.White;
            this.txtAboonman.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtAboonman.ForeColor = System.Drawing.Color.Black;
            this.txtAboonman.Location = new System.Drawing.Point(1030, 233);
            this.txtAboonman.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtAboonman.Name = "txtAboonman";
            this.txtAboonman.PreventEnterBeep = true;
            this.txtAboonman.Size = new System.Drawing.Size(106, 28);
            this.txtAboonman.TabIndex = 114;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(1168, 239);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 17);
            this.label17.TabIndex = 113;
            this.label17.Text = "آبونمان :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(507, 599);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 18);
            this.label31.TabIndex = 174;
            this.label31.Text = "شناسه پرداخت :";
            // 
            // txt_shenase_ghabz
            // 
            this.txt_shenase_ghabz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_shenase_ghabz.Border.Class = "TextBoxBorder";
            this.txt_shenase_ghabz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_shenase_ghabz.DisabledBackColor = System.Drawing.Color.White;
            this.txt_shenase_ghabz.Enabled = false;
            this.txt_shenase_ghabz.Font = new System.Drawing.Font("B Yekan", 9F);
            this.txt_shenase_ghabz.ForeColor = System.Drawing.Color.Black;
            this.txt_shenase_ghabz.Location = new System.Drawing.Point(369, 565);
            this.txt_shenase_ghabz.Margin = new System.Windows.Forms.Padding(4);
            this.txt_shenase_ghabz.Name = "txt_shenase_ghabz";
            this.txt_shenase_ghabz.PreventEnterBeep = true;
            this.txt_shenase_ghabz.Size = new System.Drawing.Size(122, 26);
            this.txt_shenase_ghabz.TabIndex = 173;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("B Yekan", 9F);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(507, 568);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 18);
            this.label32.TabIndex = 172;
            this.label32.Text = "شناسه قبض :";
            // 
            // txt_sayer
            // 
            this.txt_sayer.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sayer.Border.Class = "TextBoxBorder";
            this.txt_sayer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sayer.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sayer.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sayer.ForeColor = System.Drawing.Color.Black;
            this.txt_sayer.Location = new System.Drawing.Point(1030, 334);
            this.txt_sayer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_sayer.Name = "txt_sayer";
            this.txt_sayer.PreventEnterBeep = true;
            this.txt_sayer.Size = new System.Drawing.Size(106, 28);
            this.txt_sayer.TabIndex = 163;
            // 
            // txtBestankar
            // 
            this.txtBestankar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBestankar.Border.Class = "TextBoxBorder";
            this.txtBestankar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBestankar.DisabledBackColor = System.Drawing.Color.White;
            this.txtBestankar.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtBestankar.ForeColor = System.Drawing.Color.Black;
            this.txtBestankar.Location = new System.Drawing.Point(133, 523);
            this.txtBestankar.Margin = new System.Windows.Forms.Padding(4);
            this.txtBestankar.Name = "txtBestankar";
            this.txtBestankar.PreventEnterBeep = true;
            this.txtBestankar.ReadOnly = true;
            this.txtBestankar.Size = new System.Drawing.Size(165, 28);
            this.txtBestankar.TabIndex = 171;
            this.txtBestankar.Text = "0";
            this.txtBestankar.Visible = false;
            // 
            // lblBestankar
            // 
            this.lblBestankar.AutoSize = true;
            this.lblBestankar.BackColor = System.Drawing.Color.Transparent;
            this.lblBestankar.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.lblBestankar.ForeColor = System.Drawing.Color.Black;
            this.lblBestankar.Location = new System.Drawing.Point(328, 529);
            this.lblBestankar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBestankar.Name = "lblBestankar";
            this.lblBestankar.Size = new System.Drawing.Size(298, 17);
            this.lblBestankar.TabIndex = 170;
            this.lblBestankar.Text = "مبلغ بستانکاری که در فرآیند صدور قبض دوره بعد منظور خواهد گشت";
            this.lblBestankar.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(871, 423);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 17);
            this.label25.TabIndex = 169;
            this.label25.Text = "هزینه فاضلاب :";
            // 
            // TxtBestankariFromLastDoreh
            // 
            this.TxtBestankariFromLastDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.TxtBestankariFromLastDoreh.Border.Class = "TextBoxBorder";
            this.TxtBestankariFromLastDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TxtBestankariFromLastDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.TxtBestankariFromLastDoreh.ForeColor = System.Drawing.Color.Black;
            this.TxtBestankariFromLastDoreh.Location = new System.Drawing.Point(409, 269);
            this.TxtBestankariFromLastDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtBestankariFromLastDoreh.Name = "TxtBestankariFromLastDoreh";
            this.TxtBestankariFromLastDoreh.PreventEnterBeep = true;
            this.TxtBestankariFromLastDoreh.ReadOnly = true;
            this.TxtBestankariFromLastDoreh.Size = new System.Drawing.Size(161, 24);
            this.TxtBestankariFromLastDoreh.TabIndex = 166;
            this.TxtBestankariFromLastDoreh.Text = "0";
            // 
            // txtfazelab
            // 
            this.txtfazelab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfazelab.Border.Class = "TextBoxBorder";
            this.txtfazelab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfazelab.DisabledBackColor = System.Drawing.Color.White;
            this.txtfazelab.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtfazelab.ForeColor = System.Drawing.Color.Black;
            this.txtfazelab.Location = new System.Drawing.Point(762, 417);
            this.txtfazelab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtfazelab.Name = "txtfazelab";
            this.txtfazelab.PreventEnterBeep = true;
            this.txtfazelab.ReadOnly = true;
            this.txtfazelab.Size = new System.Drawing.Size(101, 28);
            this.txtfazelab.TabIndex = 168;
            this.txtfazelab.Text = "0";
            // 
            // txtZaribFazelab
            // 
            this.txtZaribFazelab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtZaribFazelab.Border.Class = "TextBoxBorder";
            this.txtZaribFazelab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtZaribFazelab.DisabledBackColor = System.Drawing.Color.White;
            this.txtZaribFazelab.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtZaribFazelab.ForeColor = System.Drawing.Color.Black;
            this.txtZaribFazelab.Location = new System.Drawing.Point(950, 417);
            this.txtZaribFazelab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtZaribFazelab.Name = "txtZaribFazelab";
            this.txtZaribFazelab.PreventEnterBeep = true;
            this.txtZaribFazelab.Size = new System.Drawing.Size(62, 28);
            this.txtZaribFazelab.TabIndex = 167;
            this.txtZaribFazelab.Text = ".0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(1025, 423);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(103, 17);
            this.label24.TabIndex = 166;
            this.label24.Text = "ضریب/درصد فاضلاب :";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.chSakhtoSaz);
            this.groupBox5.Controls.Add(this.chtejari);
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(33, 326);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Size = new System.Drawing.Size(341, 84);
            this.groupBox5.TabIndex = 165;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "وضعیت مشترک";
            // 
            // chSakhtoSaz
            // 
            this.chSakhtoSaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chSakhtoSaz.AutoSize = true;
            this.chSakhtoSaz.BackColor = System.Drawing.Color.Transparent;
            this.chSakhtoSaz.ForeColor = System.Drawing.Color.Black;
            this.chSakhtoSaz.Location = new System.Drawing.Point(125, 50);
            this.chSakhtoSaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chSakhtoSaz.Name = "chSakhtoSaz";
            this.chSakhtoSaz.Size = new System.Drawing.Size(201, 21);
            this.chSakhtoSaz.TabIndex = 159;
            this.chSakhtoSaz.Text = "این مشترک در حال ساخت و ساز می باشد";
            this.chSakhtoSaz.UseVisualStyleBackColor = false;
            // 
            // chtejari
            // 
            this.chtejari.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chtejari.AutoSize = true;
            this.chtejari.BackColor = System.Drawing.Color.Transparent;
            this.chtejari.ForeColor = System.Drawing.Color.Black;
            this.chtejari.Location = new System.Drawing.Point(148, 23);
            this.chtejari.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chtejari.Name = "chtejari";
            this.chtejari.Size = new System.Drawing.Size(178, 21);
            this.chtejari.TabIndex = 158;
            this.chtejari.Text = "این مشترک دارای واحد تجاری است ";
            this.chtejari.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.btn_hazineh_taviz);
            this.groupBox4.Controls.Add(this.chGhat);
            this.groupBox4.Controls.Add(this.chAdameGheraat);
            this.groupBox4.Controls.Add(this.chTaviz);
            this.groupBox4.Controls.Add(this.chDoreKamel);
            this.groupBox4.Controls.Add(this.chKarab);
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(33, 104);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Size = new System.Drawing.Size(341, 216);
            this.groupBox4.TabIndex = 158;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "وضعیت کنتور آب";
            // 
            // btn_hazineh_taviz
            // 
            this.btn_hazineh_taviz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_hazineh_taviz.BackColor = System.Drawing.Color.Transparent;
            this.btn_hazineh_taviz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_hazineh_taviz.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_hazineh_taviz.Location = new System.Drawing.Point(34, 176);
            this.btn_hazineh_taviz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_hazineh_taviz.Name = "btn_hazineh_taviz";
            this.btn_hazineh_taviz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_hazineh_taviz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_hazineh_taviz.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_hazineh_taviz.Size = new System.Drawing.Size(160, 27);
            this.btn_hazineh_taviz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_hazineh_taviz.Symbol = "";
            this.btn_hazineh_taviz.SymbolColor = System.Drawing.Color.Green;
            this.btn_hazineh_taviz.SymbolSize = 9F;
            this.btn_hazineh_taviz.TabIndex = 165;
            this.btn_hazineh_taviz.Text = "اعمال هزینه تعویض کنتور";
            this.btn_hazineh_taviz.Visible = false;
            this.btn_hazineh_taviz.Click += new System.EventHandler(this.btn_hazineh_taviz_Click);
            // 
            // chGhat
            // 
            this.chGhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chGhat.AutoSize = true;
            this.chGhat.ForeColor = System.Drawing.Color.Black;
            this.chGhat.Location = new System.Drawing.Point(189, 62);
            this.chGhat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chGhat.Name = "chGhat";
            this.chGhat.Size = new System.Drawing.Size(95, 21);
            this.chGhat.TabIndex = 164;
            this.chGhat.Text = "کنتور قطع است ";
            this.chGhat.UseVisualStyleBackColor = true;
            // 
            // chAdameGheraat
            // 
            this.chAdameGheraat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chAdameGheraat.AutoSize = true;
            this.chAdameGheraat.ForeColor = System.Drawing.Color.Black;
            this.chAdameGheraat.Location = new System.Drawing.Point(156, 92);
            this.chAdameGheraat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chAdameGheraat.Name = "chAdameGheraat";
            this.chAdameGheraat.Size = new System.Drawing.Size(128, 21);
            this.chAdameGheraat.TabIndex = 163;
            this.chAdameGheraat.Text = "کنتور قرائت نشده است ";
            this.chAdameGheraat.UseVisualStyleBackColor = true;
            // 
            // chTaviz
            // 
            this.chTaviz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chTaviz.AutoSize = true;
            this.chTaviz.ForeColor = System.Drawing.Color.Black;
            this.chTaviz.Location = new System.Drawing.Point(155, 149);
            this.chTaviz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chTaviz.Name = "chTaviz";
            this.chTaviz.Size = new System.Drawing.Size(129, 21);
            this.chTaviz.TabIndex = 162;
            this.chTaviz.Text = "کنتور تعویض شده است ";
            this.chTaviz.UseVisualStyleBackColor = true;
            this.chTaviz.CheckedChanged += new System.EventHandler(this.chTaviz_CheckedChanged);
            // 
            // chDoreKamel
            // 
            this.chDoreKamel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chDoreKamel.AutoSize = true;
            this.chDoreKamel.ForeColor = System.Drawing.Color.Black;
            this.chDoreKamel.Location = new System.Drawing.Point(148, 122);
            this.chDoreKamel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chDoreKamel.Name = "chDoreKamel";
            this.chDoreKamel.Size = new System.Drawing.Size(136, 21);
            this.chDoreKamel.TabIndex = 161;
            this.chDoreKamel.Text = "کنتور دور کامل شده است";
            this.chDoreKamel.UseVisualStyleBackColor = true;
            // 
            // chKarab
            // 
            this.chKarab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chKarab.AutoSize = true;
            this.chKarab.ForeColor = System.Drawing.Color.Black;
            this.chKarab.Location = new System.Drawing.Point(182, 29);
            this.chKarab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chKarab.Name = "chKarab";
            this.chKarab.Size = new System.Drawing.Size(102, 21);
            this.chKarab.TabIndex = 160;
            this.chKarab.Text = "کنتور خراب است ";
            this.chKarab.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.txt_tarikh_gheraat_habli);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.txt_gheraat);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.labelX3);
            this.groupBox3.Controls.Add(this.txt_ghabz_code);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtMohlat);
            this.groupBox3.Controls.Add(this.txtTarikhSoddor);
            this.groupBox3.Controls.Add(this.txt_ensheab);
            this.groupBox3.Controls.Add(this.txtTarefe);
            this.groupBox3.Controls.Add(this.txtdoreh);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(33, 3);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(1227, 90);
            this.groupBox3.TabIndex = 159;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "اطلاعات اولیه قبض آب";
            // 
            // txt_tarikh_gheraat_habli
            // 
            this.txt_tarikh_gheraat_habli.Location = new System.Drawing.Point(299, 53);
            this.txt_tarikh_gheraat_habli.Name = "txt_tarikh_gheraat_habli";
            this.txt_tarikh_gheraat_habli.Readonly = true;
            this.txt_tarikh_gheraat_habli.Size = new System.Drawing.Size(171, 24);
            this.txt_tarikh_gheraat_habli.TabIndex = 162;
            this.txt_tarikh_gheraat_habli.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(481, 57);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(86, 17);
            this.label34.TabIndex = 161;
            this.label34.Text = "تاریخ قرائت قبلی :";
            // 
            // txt_gheraat
            // 
            this.txt_gheraat.Location = new System.Drawing.Point(299, 23);
            this.txt_gheraat.Name = "txt_gheraat";
            this.txt_gheraat.Readonly = true;
            this.txt_gheraat.Size = new System.Drawing.Size(171, 24);
            this.txt_gheraat.TabIndex = 160;
            this.txt_gheraat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(481, 27);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 17);
            this.label26.TabIndex = 159;
            this.label26.Text = "تاریخ قرائت :";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(1173, 33);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(35, 27);
            this.labelX3.Symbol = "";
            this.labelX3.SymbolColor = System.Drawing.Color.Orange;
            this.labelX3.TabIndex = 158;
            // 
            // txt_ghabz_code
            // 
            this.txt_ghabz_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ghabz_code.Border.Class = "TextBoxBorder";
            this.txt_ghabz_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ghabz_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ghabz_code.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ghabz_code.ForeColor = System.Drawing.Color.Black;
            this.txt_ghabz_code.Location = new System.Drawing.Point(922, 20);
            this.txt_ghabz_code.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_ghabz_code.Name = "txt_ghabz_code";
            this.txt_ghabz_code.PreventEnterBeep = true;
            this.txt_ghabz_code.ReadOnly = true;
            this.txt_ghabz_code.Size = new System.Drawing.Size(106, 28);
            this.txt_ghabz_code.TabIndex = 112;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(1047, 26);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 17);
            this.label16.TabIndex = 111;
            this.label16.Text = "کد قبض:";
            // 
            // txtMohlat
            // 
            this.txtMohlat.IsReadonly = true;
            this.txtMohlat.Location = new System.Drawing.Point(23, 55);
            this.txtMohlat.Name = "txtMohlat";
            this.txtMohlat.Size = new System.Drawing.Size(171, 24);
            this.txtMohlat.TabIndex = 110;
            this.txtMohlat.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txtTarikhSoddor
            // 
            this.txtTarikhSoddor.Location = new System.Drawing.Point(23, 23);
            this.txtTarikhSoddor.Name = "txtTarikhSoddor";
            this.txtTarikhSoddor.Readonly = true;
            this.txtTarikhSoddor.Size = new System.Drawing.Size(171, 24);
            this.txtTarikhSoddor.TabIndex = 109;
            this.txtTarikhSoddor.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // txt_ensheab
            // 
            this.txt_ensheab.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ensheab.Border.Class = "TextBoxBorder";
            this.txt_ensheab.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ensheab.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ensheab.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_ensheab.ForeColor = System.Drawing.Color.Black;
            this.txt_ensheab.Location = new System.Drawing.Point(632, 20);
            this.txt_ensheab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_ensheab.Name = "txt_ensheab";
            this.txt_ensheab.PreventEnterBeep = true;
            this.txt_ensheab.ReadOnly = true;
            this.txt_ensheab.Size = new System.Drawing.Size(106, 28);
            this.txt_ensheab.TabIndex = 108;
            this.txt_ensheab.Text = ".0";
            // 
            // txtTarefe
            // 
            this.txtTarefe.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTarefe.Border.Class = "TextBoxBorder";
            this.txtTarefe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTarefe.DisabledBackColor = System.Drawing.Color.White;
            this.txtTarefe.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtTarefe.ForeColor = System.Drawing.Color.Black;
            this.txtTarefe.Location = new System.Drawing.Point(632, 56);
            this.txtTarefe.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtTarefe.Name = "txtTarefe";
            this.txtTarefe.PreventEnterBeep = true;
            this.txtTarefe.Size = new System.Drawing.Size(106, 28);
            this.txtTarefe.TabIndex = 107;
            // 
            // txtdoreh
            // 
            this.txtdoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtdoreh.Border.Class = "TextBoxBorder";
            this.txtdoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtdoreh.DisabledBackColor = System.Drawing.Color.White;
            this.txtdoreh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtdoreh.ForeColor = System.Drawing.Color.Black;
            this.txtdoreh.Location = new System.Drawing.Point(922, 56);
            this.txtdoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtdoreh.Name = "txtdoreh";
            this.txtdoreh.PreventEnterBeep = true;
            this.txtdoreh.ReadOnly = true;
            this.txtdoreh.Size = new System.Drawing.Size(106, 28);
            this.txtdoreh.TabIndex = 106;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(768, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 105;
            this.label5.Text = "انشعاب :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(230, 58);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 17);
            this.label4.TabIndex = 104;
            this.label4.Text = "مهلت پرداخت :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(768, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 103;
            this.label3.Text = "آب بها :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(205, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 17);
            this.label2.TabIndex = 102;
            this.label2.Text = "تاریخ صدور قبض :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1036, 62);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 101;
            this.label1.Text = "دوره  :";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.labelX2);
            this.groupBox2.Controls.Add(this.txtMojaz);
            this.groupBox2.Controls.Add(this.txtMasrafKol);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.txtGheirMojaz);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(409, 158);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(851, 52);
            this.groupBox2.TabIndex = 158;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "میزان مصرف";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(808, 19);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(35, 27);
            this.labelX2.Symbol = "";
            this.labelX2.SymbolColor = System.Drawing.Color.Orange;
            this.labelX2.TabIndex = 158;
            // 
            // txtMojaz
            // 
            this.txtMojaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMojaz.Border.Class = "TextBoxBorder";
            this.txtMojaz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMojaz.DisabledBackColor = System.Drawing.Color.White;
            this.txtMojaz.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMojaz.ForeColor = System.Drawing.Color.Black;
            this.txtMojaz.Location = new System.Drawing.Point(550, 18);
            this.txtMojaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMojaz.Name = "txtMojaz";
            this.txtMojaz.PreventEnterBeep = true;
            this.txtMojaz.ReadOnly = true;
            this.txtMojaz.Size = new System.Drawing.Size(106, 28);
            this.txtMojaz.TabIndex = 162;
            // 
            // txtMasrafKol
            // 
            this.txtMasrafKol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMasrafKol.Border.Class = "TextBoxBorder";
            this.txtMasrafKol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMasrafKol.DisabledBackColor = System.Drawing.Color.White;
            this.txtMasrafKol.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMasrafKol.ForeColor = System.Drawing.Color.Black;
            this.txtMasrafKol.Location = new System.Drawing.Point(18, 18);
            this.txtMasrafKol.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMasrafKol.Name = "txtMasrafKol";
            this.txtMasrafKol.PreventEnterBeep = true;
            this.txtMasrafKol.ReadOnly = true;
            this.txtMasrafKol.Size = new System.Drawing.Size(106, 28);
            this.txtMasrafKol.TabIndex = 161;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(138, 24);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 17);
            this.label27.TabIndex = 160;
            this.label27.Text = "میزان کل مصرف :";
            // 
            // txtGheirMojaz
            // 
            this.txtGheirMojaz.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGheirMojaz.Border.Class = "TextBoxBorder";
            this.txtGheirMojaz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGheirMojaz.DisabledBackColor = System.Drawing.Color.White;
            this.txtGheirMojaz.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtGheirMojaz.ForeColor = System.Drawing.Color.Black;
            this.txtGheirMojaz.Location = new System.Drawing.Point(270, 18);
            this.txtGheirMojaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtGheirMojaz.Name = "txtGheirMojaz";
            this.txtGheirMojaz.PreventEnterBeep = true;
            this.txtGheirMojaz.ReadOnly = true;
            this.txtGheirMojaz.Size = new System.Drawing.Size(106, 28);
            this.txtGheirMojaz.TabIndex = 159;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(381, 24);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(101, 17);
            this.label28.TabIndex = 158;
            this.label28.Text = "میزان مصرف غیرمجاز:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(661, 24);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(87, 17);
            this.label29.TabIndex = 157;
            this.label29.Text = "میزان مصرف مجاز:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.labelX1);
            this.groupBox1.Controls.Add(this.txtFelli);
            this.groupBox1.Controls.Add(this.txtGhabli);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(513, 104);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(747, 50);
            this.groupBox1.TabIndex = 157;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "قرائت های کنتور آب";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(703, 15);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(35, 27);
            this.labelX1.Symbol = "";
            this.labelX1.SymbolColor = System.Drawing.Color.Orange;
            this.labelX1.TabIndex = 157;
            // 
            // txtFelli
            // 
            this.txtFelli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFelli.Border.Class = "TextBoxBorder";
            this.txtFelli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFelli.DisabledBackColor = System.Drawing.Color.White;
            this.txtFelli.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtFelli.ForeColor = System.Drawing.Color.Black;
            this.txtFelli.Location = new System.Drawing.Point(152, 17);
            this.txtFelli.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtFelli.Name = "txtFelli";
            this.txtFelli.PreventEnterBeep = true;
            this.txtFelli.Size = new System.Drawing.Size(106, 28);
            this.txtFelli.TabIndex = 144;
            this.txtFelli.TextChanged += new System.EventHandler(this.txtFelli_TextChanged);
            this.txtFelli.Leave += new System.EventHandler(this.txtFelli_Leave);
            // 
            // txtGhabli
            // 
            this.txtGhabli.BackColor = System.Drawing.Color.White;
            this.txtGhabli.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtGhabli.ForeColor = System.Drawing.Color.Black;
            this.txtGhabli.Location = new System.Drawing.Point(445, 16);
            this.txtGhabli.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtGhabli.Name = "txtGhabli";
            this.txtGhabli.Size = new System.Drawing.Size(105, 28);
            this.txtGhabli.TabIndex = 156;
            this.txtGhabli.Leave += new System.EventHandler(this.txtGhabli_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(560, 21);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 17);
            this.label22.TabIndex = 141;
            this.label22.Text = "قرائت قبلی :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(271, 22);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 17);
            this.label23.TabIndex = 143;
            this.label23.Text = "قرائت فعلی :";
            // 
            // txt_kasr
            // 
            this.txt_kasr.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_kasr.Border.Class = "TextBoxBorder";
            this.txt_kasr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_kasr.DisabledBackColor = System.Drawing.Color.White;
            this.txt_kasr.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_kasr.ForeColor = System.Drawing.Color.Black;
            this.txt_kasr.Location = new System.Drawing.Point(133, 453);
            this.txt_kasr.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_kasr.Name = "txt_kasr";
            this.txt_kasr.PreventEnterBeep = true;
            this.txt_kasr.ReadOnly = true;
            this.txt_kasr.Size = new System.Drawing.Size(106, 28);
            this.txt_kasr.TabIndex = 140;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(248, 459);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 17);
            this.label21.TabIndex = 139;
            this.label21.Text = "کسر هزار :";
            // 
            // txtPardakhti
            // 
            this.txtPardakhti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPardakhti.Border.Class = "TextBoxBorder";
            this.txtPardakhti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPardakhti.DisabledBackColor = System.Drawing.Color.White;
            this.txtPardakhti.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtPardakhti.ForeColor = System.Drawing.Color.Black;
            this.txtPardakhti.Location = new System.Drawing.Point(871, 488);
            this.txtPardakhti.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtPardakhti.Name = "txtPardakhti";
            this.txtPardakhti.PreventEnterBeep = true;
            this.txtPardakhti.ReadOnly = true;
            this.txtPardakhti.Size = new System.Drawing.Size(141, 28);
            this.txtPardakhti.TabIndex = 138;
            this.txtPardakhti.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(1026, 494);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 17);
            this.label20.TabIndex = 137;
            this.label20.Text = "مبلغ پرداخت شده :";
            // 
            // txBaghimandeh
            // 
            this.txBaghimandeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txBaghimandeh.Border.Class = "TextBoxBorder";
            this.txBaghimandeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txBaghimandeh.DisabledBackColor = System.Drawing.Color.White;
            this.txBaghimandeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txBaghimandeh.ForeColor = System.Drawing.Color.Black;
            this.txBaghimandeh.Location = new System.Drawing.Point(133, 488);
            this.txBaghimandeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBaghimandeh.Name = "txBaghimandeh";
            this.txBaghimandeh.PreventEnterBeep = true;
            this.txBaghimandeh.ReadOnly = true;
            this.txBaghimandeh.Size = new System.Drawing.Size(165, 28);
            this.txBaghimandeh.TabIndex = 136;
            this.txBaghimandeh.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(306, 494);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 17);
            this.label15.TabIndex = 135;
            this.label15.Text = "مبلغ باقیمانده :";
            // 
            // txtDarsad
            // 
            this.txtDarsad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDarsad.Border.Class = "TextBoxBorder";
            this.txtDarsad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDarsad.DisabledBackColor = System.Drawing.Color.White;
            this.txtDarsad.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtDarsad.ForeColor = System.Drawing.Color.Black;
            this.txtDarsad.Location = new System.Drawing.Point(605, 421);
            this.txtDarsad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtDarsad.Name = "txtDarsad";
            this.txtDarsad.PreventEnterBeep = true;
            this.txtDarsad.Size = new System.Drawing.Size(62, 28);
            this.txtDarsad.TabIndex = 134;
            this.txtDarsad.Text = ".0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(680, 427);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 17);
            this.label9.TabIndex = 133;
            this.label9.Text = "درصد مالیات:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(873, 307);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 17);
            this.label14.TabIndex = 132;
            this.label14.Text = "علت جریمه :";
            // 
            // txtJarimehTozihat
            // 
            this.txtJarimehTozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtJarimehTozihat.Border.Class = "TextBoxBorder";
            this.txtJarimehTozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtJarimehTozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txtJarimehTozihat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtJarimehTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtJarimehTozihat.Location = new System.Drawing.Point(468, 302);
            this.txtJarimehTozihat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtJarimehTozihat.Name = "txtJarimehTozihat";
            this.txtJarimehTozihat.PreventEnterBeep = true;
            this.txtJarimehTozihat.Size = new System.Drawing.Size(391, 28);
            this.txtJarimehTozihat.TabIndex = 131;
            // 
            // txt_jarimeh
            // 
            this.txt_jarimeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_jarimeh.Border.Class = "TextBoxBorder";
            this.txt_jarimeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_jarimeh.DisabledBackColor = System.Drawing.Color.White;
            this.txt_jarimeh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_jarimeh.ForeColor = System.Drawing.Color.Black;
            this.txt_jarimeh.Location = new System.Drawing.Point(1030, 302);
            this.txt_jarimeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_jarimeh.Name = "txt_jarimeh";
            this.txt_jarimeh.Size = new System.Drawing.Size(106, 28);
            this.txt_jarimeh.TabIndex = 130;
            this.txt_jarimeh.Text = "0";
            this.txt_jarimeh.TextChanged += new System.EventHandler(this.txt_jarimeh_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(1172, 307);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 17);
            this.label10.TabIndex = 129;
            this.label10.Text = "جریمه :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(1026, 569);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(163, 17);
            this.label8.TabIndex = 128;
            this.label8.Text = "توضیحات کلی راجع به این قبض آب :";
            // 
            // txt_tozihat
            // 
            this.txt_tozihat.AcceptsReturn = true;
            this.txt_tozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_tozihat.Border.Class = "TextBoxBorder";
            this.txt_tozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_tozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_tozihat.ForeColor = System.Drawing.Color.Black;
            this.txt_tozihat.Location = new System.Drawing.Point(605, 567);
            this.txt_tozihat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_tozihat.Multiline = true;
            this.txt_tozihat.Name = "txt_tozihat";
            this.txt_tozihat.PreventEnterBeep = true;
            this.txt_tozihat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_tozihat.Size = new System.Drawing.Size(407, 50);
            this.txt_tozihat.TabIndex = 127;
            // 
            // txtBedehi
            // 
            this.txtBedehi.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBedehi.Border.Class = "TextBoxBorder";
            this.txtBedehi.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBedehi.DisabledBackColor = System.Drawing.Color.White;
            this.txtBedehi.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtBedehi.ForeColor = System.Drawing.Color.Black;
            this.txtBedehi.Location = new System.Drawing.Point(697, 267);
            this.txtBedehi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtBedehi.Name = "txtBedehi";
            this.txtBedehi.PreventEnterBeep = true;
            this.txtBedehi.Size = new System.Drawing.Size(161, 28);
            this.txtBedehi.TabIndex = 126;
            this.txtBedehi.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(873, 273);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 17);
            this.label19.TabIndex = 125;
            this.label19.Text = "بدهی:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(873, 340);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 17);
            this.label18.TabIndex = 124;
            this.label18.Text = "عنوان هزینه :";
            // 
            // txt_sayer_tozihat
            // 
            this.txt_sayer_tozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_sayer_tozihat.Border.Class = "TextBoxBorder";
            this.txt_sayer_tozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_sayer_tozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txt_sayer_tozihat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txt_sayer_tozihat.ForeColor = System.Drawing.Color.Black;
            this.txt_sayer_tozihat.Location = new System.Drawing.Point(468, 334);
            this.txt_sayer_tozihat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_sayer_tozihat.Name = "txt_sayer_tozihat";
            this.txt_sayer_tozihat.PreventEnterBeep = true;
            this.txt_sayer_tozihat.Size = new System.Drawing.Size(391, 28);
            this.txt_sayer_tozihat.TabIndex = 123;
            // 
            // txtMablaghKol
            // 
            this.txtMablaghKol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghKol.Border.Class = "TextBoxBorder";
            this.txtMablaghKol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghKol.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghKol.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMablaghKol.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghKol.Location = new System.Drawing.Point(871, 453);
            this.txtMablaghKol.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMablaghKol.Name = "txtMablaghKol";
            this.txtMablaghKol.PreventEnterBeep = true;
            this.txtMablaghKol.ReadOnly = true;
            this.txtMablaghKol.Size = new System.Drawing.Size(141, 28);
            this.txtMablaghKol.TabIndex = 122;
            this.txtMablaghKol.Text = "0";
            // 
            // txtMaliat
            // 
            this.txtMaliat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMaliat.Border.Class = "TextBoxBorder";
            this.txtMaliat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMaliat.DisabledBackColor = System.Drawing.Color.White;
            this.txtMaliat.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMaliat.ForeColor = System.Drawing.Color.Black;
            this.txtMaliat.Location = new System.Drawing.Point(355, 421);
            this.txtMaliat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMaliat.Name = "txtMaliat";
            this.txtMaliat.PreventEnterBeep = true;
            this.txtMaliat.ReadOnly = true;
            this.txtMaliat.Size = new System.Drawing.Size(136, 28);
            this.txtMaliat.TabIndex = 120;
            this.txtMaliat.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(1144, 340);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 17);
            this.label13.TabIndex = 119;
            this.label13.Text = "هزینه اضافی :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(1026, 459);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 17);
            this.label12.TabIndex = 118;
            this.label12.Text = "مبلغ کل :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(499, 427);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 17);
            this.label11.TabIndex = 117;
            this.label11.Text = "مالیات بر ارزش افزوده :";
            // 
            // txtMablaghHoroof
            // 
            this.txtMablaghHoroof.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghHoroof.Border.Class = "TextBoxBorder";
            this.txtMablaghHoroof.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghHoroof.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghHoroof.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMablaghHoroof.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghHoroof.Location = new System.Drawing.Point(355, 453);
            this.txtMablaghHoroof.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMablaghHoroof.Name = "txtMablaghHoroof";
            this.txtMablaghHoroof.PreventEnterBeep = true;
            this.txtMablaghHoroof.ReadOnly = true;
            this.txtMablaghHoroof.Size = new System.Drawing.Size(508, 28);
            this.txtMablaghHoroof.TabIndex = 116;
            // 
            // cmb_vaziat
            // 
            this.cmb_vaziat.DisplayMember = "Text";
            this.cmb_vaziat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_vaziat.Enabled = false;
            this.cmb_vaziat.ForeColor = System.Drawing.Color.Black;
            this.cmb_vaziat.FormattingEnabled = true;
            this.cmb_vaziat.ItemHeight = 18;
            this.cmb_vaziat.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cmb_vaziat.Location = new System.Drawing.Point(768, 537);
            this.cmb_vaziat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmb_vaziat.Name = "cmb_vaziat";
            this.cmb_vaziat.Size = new System.Drawing.Size(244, 24);
            this.cmb_vaziat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmb_vaziat.TabIndex = 106;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "پرداخت نشده";
            this.comboItem1.Value = "0";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "پرداخت شده";
            this.comboItem2.Value = "1";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "پرداخت جزئی";
            this.comboItem3.Value = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(1026, 542);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 105;
            this.label7.Text = "وضعیت پرداخت قبض :";
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_save.Location = new System.Drawing.Point(10, 569);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_save.Name = "btn_save";
            this.btn_save.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_save.Size = new System.Drawing.Size(241, 27);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 24;
            this.btn_save.Text = "ثبت تغییرات مربوط به این قبض";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txtMablagh
            // 
            this.txtMablagh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablagh.Border.Class = "TextBoxBorder";
            this.txtMablagh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablagh.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablagh.Font = new System.Drawing.Font("B Yekan", 10F);
            this.txtMablagh.ForeColor = System.Drawing.Color.Black;
            this.txtMablagh.Location = new System.Drawing.Point(1030, 267);
            this.txtMablagh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtMablagh.Name = "txtMablagh";
            this.txtMablagh.PreventEnterBeep = true;
            this.txtMablagh.ReadOnly = true;
            this.txtMablagh.Size = new System.Drawing.Size(106, 28);
            this.txtMablagh.TabIndex = 12;
            this.txtMablagh.Text = "0";
            this.txtMablagh.TextChanged += new System.EventHandler(this.txtMablagh_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1180, 273);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "مبلغ :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(578, 273);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(108, 17);
            this.label30.TabIndex = 167;
            this.label30.Text = "بستانکاری از دوره قبل :";
            // 
            // Frm_ab_details
            // 
            this.AcceptButton = this.btn_save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 733);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.lbllocked);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Frm_ab_details";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_ab_details_FormClosed);
            this.Load += new System.EventHandler(this.Frm_ab_details_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAbDetails_KeyDown);
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private System.Windows.Forms.Label lbllocked;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmb_vaziat;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private MoneyTextBox txtMablagh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private DevComponents.DotNetBar.Controls.TextBoxX txtJarimehTozihat;
        private MoneyTextBox txt_jarimeh;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_tozihat;
        private MoneyTextBox txtBedehi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sayer_tozihat;
        private MoneyTextBox txtMablaghKol;
        private MoneyTextBox txtMaliat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMablaghHoroof;
        private FloatTextBox txtDarsad;
        private System.Windows.Forms.Label label9;
        private MoneyTextBox txBaghimandeh;
        private System.Windows.Forms.Label label15;
        private MoneyTextBox txtPardakhti;
        private System.Windows.Forms.Label label20;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_kasr;
        private System.Windows.Forms.Label label21;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFelli;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtGhabli;
        private DevComponents.DotNetBar.ButtonX buttonX4;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ghabz_code;
        private System.Windows.Forms.Label label16;
        private FarsiLibrary.Win.Controls.FADatePicker txtMohlat;
        private FarsiLibrary.Win.Controls.FADatePicker txtTarikhSoddor;
        private FloatTextBox txt_ensheab;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTarefe;
        private DevComponents.DotNetBar.Controls.TextBoxX txtdoreh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMojaz;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMasrafKol;
        private System.Windows.Forms.Label label27;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGheirMojaz;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chGhat;
        private System.Windows.Forms.CheckBox chAdameGheraat;
        private System.Windows.Forms.CheckBox chTaviz;
        private System.Windows.Forms.CheckBox chDoreKamel;
        private System.Windows.Forms.CheckBox chKarab;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAboonman;
        private System.Windows.Forms.Label label17;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chSakhtoSaz;
        private System.Windows.Forms.CheckBox chtejari;
        private CWMS.FloatTextBox txtZaribFazelab;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private MoneyTextBox txtfazelab;
        private FarsiLibrary.Win.Controls.FADatePicker txt_gheraat;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label30;
        private MoneyTextBox TxtBestankariFromLastDoreh;
        private MoneyTextBox txtBestankar;
        private System.Windows.Forms.Label lblBestankar;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_sayer;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_shenase_pardakht;
        private System.Windows.Forms.Label label31;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_shenase_ghabz;
        private System.Windows.Forms.Label label32;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private System.Windows.Forms.Label label33;
        private MoneyTextBox txt_masraf_omoomi;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private FarsiLibrary.Win.Controls.FADatePicker txt_tarikh_gheraat_habli;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private MoneyTextBox txt_taviz_kontor_hazineh;
        private DevComponents.DotNetBar.ButtonX btn_hazineh_taviz;
    }
}