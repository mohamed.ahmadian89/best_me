﻿namespace CWMS.Ab
{
    partial class Frm_ab_for_all
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مشاهدهپرداختیهایقبضToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.AbDataset();
            this.superTabItem3 = new DevComponents.DotNetBar.SuperTabItem();
            this.ab_ghabzTableAdapter = new CWMS.AbDatasetTableAdapters.ab_ghabzTableAdapter();
            this.superTabControl1 = new DevComponents.DotNetBar.SuperTabControl();
            this.superTabControlPanel1 = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.dgv_ghobooz = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.gcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gharardadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.kontor_start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kontor_end = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf_gheir_mojaz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masraf_mojaz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ensheab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jarimeh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maliatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sayerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bedehi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghkolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kasr_hezar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pardakhti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mande = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestankari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.superTabItem1 = new DevComponents.DotNetBar.SuperTabItem();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnFindByGharardad = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_by_gharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btn_find_by_gcode = new DevComponents.DotNetBar.ButtonX();
            this.txt_find_ghabz_byCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_pardakht_shodeh = new DevComponents.DotNetBar.ButtonX();
            this.btn_padakhr_nashode = new DevComponents.DotNetBar.ButtonX();
            this.btn_dar_hal_barrrasi = new DevComponents.DotNetBar.ButtonX();
            this.lblPayam = new DevComponents.DotNetBar.LabelX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btn_find = new DevComponents.DotNetBar.ButtonX();
            this.cmbDoreh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btn_all = new DevComponents.DotNetBar.ButtonX();
            this.grpStat = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX30 = new DevComponents.DotNetBar.LabelX();
            this.lblTedadKol = new DevComponents.DotNetBar.LabelX();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.labelX20 = new DevComponents.DotNetBar.LabelX();
            this.labelX21 = new DevComponents.DotNetBar.LabelX();
            this.lbl_dar_hal_barrrasi = new DevComponents.DotNetBar.LabelX();
            this.lbl_pardakht_nasodeh = new DevComponents.DotNetBar.LabelX();
            this.labelX31 = new DevComponents.DotNetBar.LabelX();
            this.labelX35 = new DevComponents.DotNetBar.LabelX();
            this.lbl_pardakht_sodeh = new DevComponents.DotNetBar.LabelX();
            this.btnShowGhobooz = new DevComponents.DotNetBar.ButtonX();
            this.buttonX10 = new DevComponents.DotNetBar.ButtonX();
            this.cmb_noe = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).BeginInit();
            this.superTabControl1.SuspendLayout();
            this.superTabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).BeginInit();
            this.groupPanel1.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.grpStat.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem,
            this.مشاهدهپرداختیهایقبضToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 48);
            // 
            // مشاهدهاطلاعاتکاملقبضToolStripMenuItem
            // 
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem.Name = "مشاهدهاطلاعاتکاملقبضToolStripMenuItem";
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem.Text = "مشاهده اطلاعات کامل قبض";
            this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem.Click += new System.EventHandler(this.مشاهدهاطلاعاتکاملقبضToolStripMenuItem_Click);
            // 
            // مشاهدهپرداختیهایقبضToolStripMenuItem
            // 
            this.مشاهدهپرداختیهایقبضToolStripMenuItem.Name = "مشاهدهپرداختیهایقبضToolStripMenuItem";
            this.مشاهدهپرداختیهایقبضToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.مشاهدهپرداختیهایقبضToolStripMenuItem.Text = "مشاهده پرداختی های قبض";
            this.مشاهدهپرداختیهایقبضToolStripMenuItem.Click += new System.EventHandler(this.مشاهدهپرداختیهایقبضToolStripMenuItem_Click);
            // 
            // abghabzBindingSource
            // 
            this.abghabzBindingSource.DataMember = "ab_ghabz";
            this.abghabzBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "AbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // superTabItem3
            // 
            this.superTabItem3.GlobalItem = false;
            this.superTabItem3.Name = "superTabItem3";
            this.superTabItem3.Text = "قبوض صادر نشده";
            // 
            // ab_ghabzTableAdapter
            // 
            this.ab_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // superTabControl1
            // 
            this.superTabControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControl1.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControl1.ControlBox.MenuBox.Name = "";
            this.superTabControl1.ControlBox.Name = "";
            this.superTabControl1.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControl1.ControlBox.CloseBox,
            this.superTabControl1.ControlBox.MenuBox});
            this.superTabControl1.Controls.Add(this.superTabControlPanel1);
            this.superTabControl1.ForeColor = System.Drawing.Color.Black;
            this.superTabControl1.Location = new System.Drawing.Point(9, 173);
            this.superTabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControl1.Name = "superTabControl1";
            this.superTabControl1.ReorderTabsEnabled = true;
            this.superTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.superTabControl1.SelectedTabFont = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Bold);
            this.superTabControl1.SelectedTabIndex = 0;
            this.superTabControl1.Size = new System.Drawing.Size(1024, 372);
            this.superTabControl1.TabFont = new System.Drawing.Font("B Yekan", 8.25F);
            this.superTabControl1.TabIndex = 45;
            this.superTabControl1.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItem1});
            this.superTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue;
            this.superTabControl1.Text = "superTabControl1";
            // 
            // superTabControlPanel1
            // 
            this.superTabControlPanel1.Controls.Add(this.dgv_ghobooz);
            this.superTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.superTabControlPanel1.Location = new System.Drawing.Point(0, 27);
            this.superTabControlPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.superTabControlPanel1.Name = "superTabControlPanel1";
            this.superTabControlPanel1.Size = new System.Drawing.Size(1024, 345);
            this.superTabControlPanel1.TabIndex = 1;
            this.superTabControlPanel1.TabItem = this.superTabItem1;
            // 
            // dgv_ghobooz
            // 
            this.dgv_ghobooz.AllowUserToAddRows = false;
            this.dgv_ghobooz.AllowUserToDeleteRows = false;
            this.dgv_ghobooz.AllowUserToResizeColumns = false;
            this.dgv_ghobooz.AllowUserToResizeRows = false;
            this.dgv_ghobooz.AutoGenerateColumns = false;
            this.dgv_ghobooz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_ghobooz.BackgroundColor = System.Drawing.Color.White;
            this.dgv_ghobooz.ColumnHeadersHeight = 30;
            this.dgv_ghobooz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gcodeDataGridViewTextBoxColumn,
            this.gharardadDataGridViewTextBoxColumn,
            this.dcodeDataGridViewTextBoxColumn,
            this.tarikhDataGridViewTextBoxColumn,
            this.kontor_start,
            this.kontor_end,
            this.masraf_gheir_mojaz,
            this.masraf_mojaz,
            this.Ensheab,
            this.jarimeh,
            this.maliatDataGridViewTextBoxColumn,
            this.sayerDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn,
            this.bedehi,
            this.mablaghkolDataGridViewTextBoxColumn,
            this.kasr_hezar,
            this.pardakhti,
            this.mande,
            this.bestankari});
            this.dgv_ghobooz.ContextMenuStrip = this.contextMenuStrip1;
            this.dgv_ghobooz.DataSource = this.abghabzBindingSource;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("B Yekan", 9F);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ghobooz.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgv_ghobooz.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_ghobooz.Location = new System.Drawing.Point(1, 1);
            this.dgv_ghobooz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_ghobooz.MultiSelect = false;
            this.dgv_ghobooz.Name = "dgv_ghobooz";
            this.dgv_ghobooz.ReadOnly = true;
            this.dgv_ghobooz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgv_ghobooz.RowHeadersVisible = false;
            this.dgv_ghobooz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ghobooz.Size = new System.Drawing.Size(1021, 324);
            this.dgv_ghobooz.TabIndex = 39;
            this.dgv_ghobooz.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ghobooz_CellContentDoubleClick);
            this.dgv_ghobooz.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ghobooz_CellClick);
            this.dgv_ghobooz.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_ghobooz_CellMouseClick);
            // 
            // gcodeDataGridViewTextBoxColumn
            // 
            this.gcodeDataGridViewTextBoxColumn.DataPropertyName = "gcode";
            this.gcodeDataGridViewTextBoxColumn.FillWeight = 80F;
            this.gcodeDataGridViewTextBoxColumn.HeaderText = "کد";
            this.gcodeDataGridViewTextBoxColumn.Name = "gcodeDataGridViewTextBoxColumn";
            this.gcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.gcodeDataGridViewTextBoxColumn.Width = 45;
            // 
            // gharardadDataGridViewTextBoxColumn
            // 
            this.gharardadDataGridViewTextBoxColumn.DataPropertyName = "gharardad";
            this.gharardadDataGridViewTextBoxColumn.FillWeight = 50F;
            this.gharardadDataGridViewTextBoxColumn.HeaderText = "قرارداد";
            this.gharardadDataGridViewTextBoxColumn.Name = "gharardadDataGridViewTextBoxColumn";
            this.gharardadDataGridViewTextBoxColumn.ReadOnly = true;
            this.gharardadDataGridViewTextBoxColumn.Width = 66;
            // 
            // dcodeDataGridViewTextBoxColumn
            // 
            this.dcodeDataGridViewTextBoxColumn.DataPropertyName = "dcode";
            this.dcodeDataGridViewTextBoxColumn.FillWeight = 50F;
            this.dcodeDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.dcodeDataGridViewTextBoxColumn.Name = "dcodeDataGridViewTextBoxColumn";
            this.dcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.dcodeDataGridViewTextBoxColumn.Width = 54;
            // 
            // tarikhDataGridViewTextBoxColumn
            // 
            this.tarikhDataGridViewTextBoxColumn.DataPropertyName = "tarikh";
            this.tarikhDataGridViewTextBoxColumn.HeaderText = "تاریخ صدور";
            this.tarikhDataGridViewTextBoxColumn.Name = "tarikhDataGridViewTextBoxColumn";
            this.tarikhDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tarikhDataGridViewTextBoxColumn.Width = 84;
            // 
            // kontor_start
            // 
            this.kontor_start.DataPropertyName = "kontor_start";
            this.kontor_start.HeaderText = "کنتور قبلی";
            this.kontor_start.Name = "kontor_start";
            this.kontor_start.ReadOnly = true;
            this.kontor_start.Width = 80;
            // 
            // kontor_end
            // 
            this.kontor_end.DataPropertyName = "kontor_end";
            this.kontor_end.HeaderText = "کنتور فعلی";
            this.kontor_end.Name = "kontor_end";
            this.kontor_end.ReadOnly = true;
            this.kontor_end.Width = 81;
            // 
            // masraf_gheir_mojaz
            // 
            this.masraf_gheir_mojaz.DataPropertyName = "masraf_gheir_mojaz";
            this.masraf_gheir_mojaz.HeaderText = "مصرف غیرمجاز";
            this.masraf_gheir_mojaz.Name = "masraf_gheir_mojaz";
            this.masraf_gheir_mojaz.ReadOnly = true;
            this.masraf_gheir_mojaz.Width = 97;
            // 
            // masraf_mojaz
            // 
            this.masraf_mojaz.DataPropertyName = "masraf_mojaz";
            this.masraf_mojaz.HeaderText = "مصرف مجاز";
            this.masraf_mojaz.Name = "masraf_mojaz";
            this.masraf_mojaz.ReadOnly = true;
            this.masraf_mojaz.Width = 83;
            // 
            // Ensheab
            // 
            this.Ensheab.DataPropertyName = "aboonmah";
            this.Ensheab.FillWeight = 50F;
            this.Ensheab.HeaderText = "ابونمان";
            this.Ensheab.Name = "Ensheab";
            this.Ensheab.ReadOnly = true;
            this.Ensheab.Width = 63;
            // 
            // jarimeh
            // 
            this.jarimeh.DataPropertyName = "jarimeh";
            dataGridViewCellStyle7.Format = "0,0";
            this.jarimeh.DefaultCellStyle = dataGridViewCellStyle7;
            this.jarimeh.HeaderText = "جریمه";
            this.jarimeh.Name = "jarimeh";
            this.jarimeh.ReadOnly = true;
            this.jarimeh.Width = 59;
            // 
            // maliatDataGridViewTextBoxColumn
            // 
            this.maliatDataGridViewTextBoxColumn.DataPropertyName = "maliat";
            dataGridViewCellStyle15.Format = "0,0";
            this.maliatDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this.maliatDataGridViewTextBoxColumn.HeaderText = "مالیات";
            this.maliatDataGridViewTextBoxColumn.Name = "maliatDataGridViewTextBoxColumn";
            this.maliatDataGridViewTextBoxColumn.ReadOnly = true;
            this.maliatDataGridViewTextBoxColumn.Width = 60;
            // 
            // sayerDataGridViewTextBoxColumn
            // 
            this.sayerDataGridViewTextBoxColumn.DataPropertyName = "sayer";
            dataGridViewCellStyle16.Format = "0,0";
            this.sayerDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this.sayerDataGridViewTextBoxColumn.HeaderText = "سایر";
            this.sayerDataGridViewTextBoxColumn.Name = "sayerDataGridViewTextBoxColumn";
            this.sayerDataGridViewTextBoxColumn.ReadOnly = true;
            this.sayerDataGridViewTextBoxColumn.Width = 54;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            dataGridViewCellStyle17.Format = "0,0";
            this.mablaghDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "مبلغ";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            this.mablaghDataGridViewTextBoxColumn.Width = 53;
            // 
            // bedehi
            // 
            this.bedehi.DataPropertyName = "bedehi";
            this.bedehi.HeaderText = "بدهی";
            this.bedehi.Name = "bedehi";
            this.bedehi.ReadOnly = true;
            this.bedehi.Width = 58;
            // 
            // mablaghkolDataGridViewTextBoxColumn
            // 
            this.mablaghkolDataGridViewTextBoxColumn.DataPropertyName = "mablaghkol";
            dataGridViewCellStyle18.Format = "0,0";
            this.mablaghkolDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this.mablaghkolDataGridViewTextBoxColumn.HeaderText = "مبلغ کل";
            this.mablaghkolDataGridViewTextBoxColumn.Name = "mablaghkolDataGridViewTextBoxColumn";
            this.mablaghkolDataGridViewTextBoxColumn.ReadOnly = true;
            this.mablaghkolDataGridViewTextBoxColumn.Width = 69;
            // 
            // kasr_hezar
            // 
            this.kasr_hezar.DataPropertyName = "kasr_hezar";
            this.kasr_hezar.HeaderText = "کسرهزار";
            this.kasr_hezar.Name = "kasr_hezar";
            this.kasr_hezar.ReadOnly = true;
            this.kasr_hezar.Width = 72;
            // 
            // pardakhti
            // 
            this.pardakhti.HeaderText = "پرداختی";
            this.pardakhti.Name = "pardakhti";
            this.pardakhti.ReadOnly = true;
            this.pardakhti.Width = 69;
            // 
            // mande
            // 
            this.mande.DataPropertyName = "mande";
            dataGridViewCellStyle19.Format = "0,0";
            this.mande.DefaultCellStyle = dataGridViewCellStyle19;
            this.mande.HeaderText = "باقیمانده";
            this.mande.Name = "mande";
            this.mande.ReadOnly = true;
            this.mande.Width = 71;
            // 
            // bestankari
            // 
            this.bestankari.DataPropertyName = "bestankari";
            this.bestankari.HeaderText = "بستانکاری";
            this.bestankari.Name = "bestankari";
            this.bestankari.ReadOnly = true;
            this.bestankari.Width = 77;
            // 
            // superTabItem1
            // 
            this.superTabItem1.AttachedControl = this.superTabControlPanel1;
            this.superTabItem1.GlobalItem = false;
            this.superTabItem1.Name = "superTabItem1";
            this.superTabItem1.Text = "کلیه قبوض";
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.cmb_noe);
            this.groupPanel1.Controls.Add(this.labelX4);
            this.groupPanel1.Controls.Add(this.btnFindByGharardad);
            this.groupPanel1.Controls.Add(this.txt_find_by_gharardad);
            this.groupPanel1.Controls.Add(this.labelX2);
            this.groupPanel1.Controls.Add(this.btn_find_by_gcode);
            this.groupPanel1.Controls.Add(this.txt_find_ghabz_byCode);
            this.groupPanel1.Controls.Add(this.labelX1);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(8, 127);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1024, 39);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 44;
            // 
            // btnFindByGharardad
            // 
            this.btnFindByGharardad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnFindByGharardad.BackColor = System.Drawing.Color.Transparent;
            this.btnFindByGharardad.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnFindByGharardad.Location = new System.Drawing.Point(384, 6);
            this.btnFindByGharardad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFindByGharardad.Name = "btnFindByGharardad";
            this.btnFindByGharardad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnFindByGharardad.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btnFindByGharardad.Size = new System.Drawing.Size(38, 24);
            this.btnFindByGharardad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnFindByGharardad.Symbol = "";
            this.btnFindByGharardad.SymbolColor = System.Drawing.Color.Teal;
            this.btnFindByGharardad.SymbolSize = 9F;
            this.btnFindByGharardad.TabIndex = 30;
            this.btnFindByGharardad.Click += new System.EventHandler(this.btnFindByGharardad_Click);
            // 
            // txt_find_by_gharardad
            // 
            this.txt_find_by_gharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_by_gharardad.Border.Class = "TextBoxBorder";
            this.txt_find_by_gharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_by_gharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_by_gharardad.ForeColor = System.Drawing.Color.Black;
            this.txt_find_by_gharardad.Location = new System.Drawing.Point(430, 5);
            this.txt_find_by_gharardad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_find_by_gharardad.Name = "txt_find_by_gharardad";
            this.txt_find_by_gharardad.PreventEnterBeep = true;
            this.txt_find_by_gharardad.Size = new System.Drawing.Size(136, 26);
            this.txt_find_by_gharardad.TabIndex = 29;
            this.txt_find_by_gharardad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_by_gharardad_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(574, 6);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(88, 24);
            this.labelX2.TabIndex = 28;
            this.labelX2.Text = "شماره قرار داد:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btn_find_by_gcode
            // 
            this.btn_find_by_gcode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find_by_gcode.BackColor = System.Drawing.Color.Transparent;
            this.btn_find_by_gcode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find_by_gcode.Location = new System.Drawing.Point(739, 6);
            this.btn_find_by_gcode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_find_by_gcode.Name = "btn_find_by_gcode";
            this.btn_find_by_gcode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find_by_gcode.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find_by_gcode.Size = new System.Drawing.Size(38, 24);
            this.btn_find_by_gcode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find_by_gcode.Symbol = "";
            this.btn_find_by_gcode.SymbolColor = System.Drawing.Color.Teal;
            this.btn_find_by_gcode.SymbolSize = 9F;
            this.btn_find_by_gcode.TabIndex = 27;
            this.btn_find_by_gcode.Click += new System.EventHandler(this.btn_find_by_gcode_Click);
            // 
            // txt_find_ghabz_byCode
            // 
            this.txt_find_ghabz_byCode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_find_ghabz_byCode.Border.Class = "TextBoxBorder";
            this.txt_find_ghabz_byCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_find_ghabz_byCode.DisabledBackColor = System.Drawing.Color.White;
            this.txt_find_ghabz_byCode.ForeColor = System.Drawing.Color.Black;
            this.txt_find_ghabz_byCode.Location = new System.Drawing.Point(785, 5);
            this.txt_find_ghabz_byCode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt_find_ghabz_byCode.Name = "txt_find_ghabz_byCode";
            this.txt_find_ghabz_byCode.PreventEnterBeep = true;
            this.txt_find_ghabz_byCode.Size = new System.Drawing.Size(136, 26);
            this.txt_find_ghabz_byCode.TabIndex = 26;
            this.txt_find_ghabz_byCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_find_ghabz_byCode_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(909, 6);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(76, 24);
            this.labelX1.TabIndex = 25;
            this.labelX1.Text = "کد قبض:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.btn_pardakht_shodeh);
            this.groupPanel3.Controls.Add(this.btn_padakhr_nashode);
            this.groupPanel3.Controls.Add(this.btn_dar_hal_barrrasi);
            this.groupPanel3.Controls.Add(this.lblPayam);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Location = new System.Drawing.Point(9, 86);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(1024, 39);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 42;
            // 
            // btn_pardakht_shodeh
            // 
            this.btn_pardakht_shodeh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_pardakht_shodeh.BackColor = System.Drawing.Color.Transparent;
            this.btn_pardakht_shodeh.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_pardakht_shodeh.Location = new System.Drawing.Point(4, 4);
            this.btn_pardakht_shodeh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_pardakht_shodeh.Name = "btn_pardakht_shodeh";
            this.btn_pardakht_shodeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_pardakht_shodeh.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_pardakht_shodeh.Size = new System.Drawing.Size(163, 24);
            this.btn_pardakht_shodeh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_pardakht_shodeh.Symbol = "";
            this.btn_pardakht_shodeh.SymbolColor = System.Drawing.Color.Green;
            this.btn_pardakht_shodeh.SymbolSize = 9F;
            this.btn_pardakht_shodeh.TabIndex = 24;
            this.btn_pardakht_shodeh.Text = "قبوض پرداخت شده";
            this.btn_pardakht_shodeh.Click += new System.EventHandler(this.btn_pardakht_shodeh_Click);
            // 
            // btn_padakhr_nashode
            // 
            this.btn_padakhr_nashode.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_padakhr_nashode.BackColor = System.Drawing.Color.Transparent;
            this.btn_padakhr_nashode.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_padakhr_nashode.Location = new System.Drawing.Point(173, 4);
            this.btn_padakhr_nashode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_padakhr_nashode.Name = "btn_padakhr_nashode";
            this.btn_padakhr_nashode.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_padakhr_nashode.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_padakhr_nashode.Size = new System.Drawing.Size(163, 24);
            this.btn_padakhr_nashode.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_padakhr_nashode.Symbol = "";
            this.btn_padakhr_nashode.SymbolColor = System.Drawing.Color.Maroon;
            this.btn_padakhr_nashode.SymbolSize = 9F;
            this.btn_padakhr_nashode.TabIndex = 25;
            this.btn_padakhr_nashode.Text = "قبوض پرداخت نشده";
            this.btn_padakhr_nashode.Click += new System.EventHandler(this.btn_padakhr_nashode_Click);
            // 
            // btn_dar_hal_barrrasi
            // 
            this.btn_dar_hal_barrrasi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_dar_hal_barrrasi.BackColor = System.Drawing.Color.Transparent;
            this.btn_dar_hal_barrrasi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_dar_hal_barrrasi.Location = new System.Drawing.Point(343, 4);
            this.btn_dar_hal_barrrasi.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_dar_hal_barrrasi.Name = "btn_dar_hal_barrrasi";
            this.btn_dar_hal_barrrasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_dar_hal_barrrasi.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_dar_hal_barrrasi.Size = new System.Drawing.Size(163, 24);
            this.btn_dar_hal_barrrasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_dar_hal_barrrasi.Symbol = "";
            this.btn_dar_hal_barrrasi.SymbolColor = System.Drawing.Color.Teal;
            this.btn_dar_hal_barrrasi.SymbolSize = 9F;
            this.btn_dar_hal_barrrasi.TabIndex = 26;
            this.btn_dar_hal_barrrasi.Text = "قبوض با پرداخت جزئی";
            this.btn_dar_hal_barrrasi.Click += new System.EventHandler(this.btn_dar_hal_barrrasi_Click);
            // 
            // lblPayam
            // 
            this.lblPayam.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblPayam.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPayam.ForeColor = System.Drawing.Color.Black;
            this.lblPayam.Location = new System.Drawing.Point(557, 4);
            this.lblPayam.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lblPayam.Name = "lblPayam";
            this.lblPayam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPayam.Size = new System.Drawing.Size(432, 24);
            this.lblPayam.TabIndex = 27;
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.btn_find);
            this.groupPanel2.Controls.Add(this.cmbDoreh);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.btn_all);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(9, 13);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(1024, 65);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 41;
            this.groupPanel2.Text = "جستجوی اطلاعات";
            // 
            // btn_find
            // 
            this.btn_find.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_find.BackColor = System.Drawing.Color.Transparent;
            this.btn_find.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_find.Location = new System.Drawing.Point(557, 5);
            this.btn_find.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_find.Name = "btn_find";
            this.btn_find.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_find.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_find.Size = new System.Drawing.Size(189, 24);
            this.btn_find.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_find.Symbol = "";
            this.btn_find.SymbolColor = System.Drawing.Color.Green;
            this.btn_find.SymbolSize = 9F;
            this.btn_find.TabIndex = 2;
            this.btn_find.Text = "نمایش قبوض آب دوره انتخاب شده";
            this.btn_find.Click += new System.EventHandler(this.btn_find_Click);
            // 
            // cmbDoreh
            // 
            this.cmbDoreh.DisplayMember = "Text";
            this.cmbDoreh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDoreh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDoreh.ForeColor = System.Drawing.Color.Black;
            this.cmbDoreh.FormattingEnabled = true;
            this.cmbDoreh.ItemHeight = 20;
            this.cmbDoreh.Location = new System.Drawing.Point(766, 4);
            this.cmbDoreh.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbDoreh.Name = "cmbDoreh";
            this.cmbDoreh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbDoreh.Size = new System.Drawing.Size(154, 26);
            this.cmbDoreh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDoreh.TabIndex = 1;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(908, 5);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(76, 24);
            this.labelX7.TabIndex = 24;
            this.labelX7.Text = "دوره:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btn_all
            // 
            this.btn_all.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_all.BackColor = System.Drawing.Color.Transparent;
            this.btn_all.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_all.Location = new System.Drawing.Point(4, 7);
            this.btn_all.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_all.Name = "btn_all";
            this.btn_all.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_all.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_all.Size = new System.Drawing.Size(292, 24);
            this.btn_all.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_all.Symbol = "";
            this.btn_all.SymbolColor = System.Drawing.Color.Green;
            this.btn_all.SymbolSize = 9F;
            this.btn_all.TabIndex = 1;
            this.btn_all.Text = "نمایش کلیه قبوض آب دوره فعلی";
            this.btn_all.Click += new System.EventHandler(this.btn_all_Click);
            // 
            // grpStat
            // 
            this.grpStat.BackColor = System.Drawing.Color.White;
            this.grpStat.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpStat.Controls.Add(this.labelX3);
            this.grpStat.Controls.Add(this.labelX30);
            this.grpStat.Controls.Add(this.lblTedadKol);
            this.grpStat.Controls.Add(this.labelX22);
            this.grpStat.Controls.Add(this.labelX20);
            this.grpStat.Controls.Add(this.labelX21);
            this.grpStat.Controls.Add(this.lbl_dar_hal_barrrasi);
            this.grpStat.Controls.Add(this.lbl_pardakht_nasodeh);
            this.grpStat.Controls.Add(this.labelX31);
            this.grpStat.Controls.Add(this.labelX35);
            this.grpStat.Controls.Add(this.lbl_pardakht_sodeh);
            this.grpStat.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpStat.Location = new System.Drawing.Point(10, 584);
            this.grpStat.Margin = new System.Windows.Forms.Padding(4);
            this.grpStat.Name = "grpStat";
            this.grpStat.Size = new System.Drawing.Size(1022, 60);
            // 
            // 
            // 
            this.grpStat.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpStat.Style.BackColorGradientAngle = 90;
            this.grpStat.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpStat.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderBottomWidth = 1;
            this.grpStat.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpStat.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderLeftWidth = 1;
            this.grpStat.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderRightWidth = 1;
            this.grpStat.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpStat.Style.BorderTopWidth = 1;
            this.grpStat.Style.CornerDiameter = 4;
            this.grpStat.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpStat.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpStat.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpStat.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpStat.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpStat.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpStat.TabIndex = 47;
            this.grpStat.Text = "اطلاعات آماری دوره ";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(944, -2);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(57, 24);
            this.labelX3.TabIndex = 95;
            this.labelX3.Text = "تعداد کل:";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX30
            // 
            this.labelX30.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX30.ForeColor = System.Drawing.Color.Black;
            this.labelX30.Location = new System.Drawing.Point(485, -2);
            this.labelX30.Margin = new System.Windows.Forms.Padding(4);
            this.labelX30.Name = "labelX30";
            this.labelX30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX30.Size = new System.Drawing.Size(30, 24);
            this.labelX30.Symbol = "";
            this.labelX30.SymbolColor = System.Drawing.Color.LightCoral;
            this.labelX30.SymbolSize = 15F;
            this.labelX30.TabIndex = 76;
            this.labelX30.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblTedadKol
            // 
            this.lblTedadKol.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTedadKol.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTedadKol.ForeColor = System.Drawing.Color.Black;
            this.lblTedadKol.Location = new System.Drawing.Point(864, -2);
            this.lblTedadKol.Margin = new System.Windows.Forms.Padding(4);
            this.lblTedadKol.Name = "lblTedadKol";
            this.lblTedadKol.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTedadKol.Size = new System.Drawing.Size(72, 24);
            this.lblTedadKol.TabIndex = 94;
            this.lblTedadKol.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX22
            // 
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(338, -2);
            this.labelX22.Margin = new System.Windows.Forms.Padding(4);
            this.labelX22.Name = "labelX22";
            this.labelX22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX22.Size = new System.Drawing.Size(154, 24);
            this.labelX22.TabIndex = 80;
            this.labelX22.Text = "تعداد قبوض پرداخت نشده:";
            this.labelX22.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX20
            // 
            this.labelX20.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX20.ForeColor = System.Drawing.Color.Black;
            this.labelX20.Location = new System.Drawing.Point(227, -2);
            this.labelX20.Margin = new System.Windows.Forms.Padding(4);
            this.labelX20.Name = "labelX20";
            this.labelX20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX20.Size = new System.Drawing.Size(30, 24);
            this.labelX20.Symbol = "";
            this.labelX20.SymbolColor = System.Drawing.Color.Maroon;
            this.labelX20.SymbolSize = 15F;
            this.labelX20.TabIndex = 79;
            this.labelX20.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX21
            // 
            this.labelX21.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX21.ForeColor = System.Drawing.Color.Black;
            this.labelX21.Location = new System.Drawing.Point(91, -2);
            this.labelX21.Margin = new System.Windows.Forms.Padding(4);
            this.labelX21.Name = "labelX21";
            this.labelX21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX21.Size = new System.Drawing.Size(154, 24);
            this.labelX21.TabIndex = 78;
            this.labelX21.Text = "قبوض با پرداخت جزئی :";
            this.labelX21.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_dar_hal_barrrasi
            // 
            this.lbl_dar_hal_barrrasi.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_dar_hal_barrrasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_dar_hal_barrrasi.ForeColor = System.Drawing.Color.Black;
            this.lbl_dar_hal_barrrasi.Location = new System.Drawing.Point(11, -2);
            this.lbl_dar_hal_barrrasi.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_dar_hal_barrrasi.Name = "lbl_dar_hal_barrrasi";
            this.lbl_dar_hal_barrrasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_dar_hal_barrrasi.Size = new System.Drawing.Size(72, 24);
            this.lbl_dar_hal_barrrasi.TabIndex = 77;
            this.lbl_dar_hal_barrrasi.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_pardakht_nasodeh
            // 
            this.lbl_pardakht_nasodeh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_pardakht_nasodeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_pardakht_nasodeh.ForeColor = System.Drawing.Color.Black;
            this.lbl_pardakht_nasodeh.Location = new System.Drawing.Point(256, -2);
            this.lbl_pardakht_nasodeh.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_pardakht_nasodeh.Name = "lbl_pardakht_nasodeh";
            this.lbl_pardakht_nasodeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_pardakht_nasodeh.Size = new System.Drawing.Size(94, 24);
            this.lbl_pardakht_nasodeh.TabIndex = 75;
            this.lbl_pardakht_nasodeh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX31
            // 
            this.labelX31.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX31.ForeColor = System.Drawing.Color.Black;
            this.labelX31.Location = new System.Drawing.Point(737, -2);
            this.labelX31.Margin = new System.Windows.Forms.Padding(4);
            this.labelX31.Name = "labelX31";
            this.labelX31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX31.Size = new System.Drawing.Size(30, 24);
            this.labelX31.Symbol = "";
            this.labelX31.SymbolColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelX31.SymbolSize = 15F;
            this.labelX31.TabIndex = 74;
            this.labelX31.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX35
            // 
            this.labelX35.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX35.ForeColor = System.Drawing.Color.Black;
            this.labelX35.Location = new System.Drawing.Point(591, -2);
            this.labelX35.Margin = new System.Windows.Forms.Padding(4);
            this.labelX35.Name = "labelX35";
            this.labelX35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX35.Size = new System.Drawing.Size(154, 24);
            this.labelX35.TabIndex = 73;
            this.labelX35.Text = "تعداد قبوض پرداخت شده:";
            this.labelX35.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lbl_pardakht_sodeh
            // 
            this.lbl_pardakht_sodeh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lbl_pardakht_sodeh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbl_pardakht_sodeh.ForeColor = System.Drawing.Color.Black;
            this.lbl_pardakht_sodeh.Location = new System.Drawing.Point(523, -2);
            this.lbl_pardakht_sodeh.Margin = new System.Windows.Forms.Padding(4);
            this.lbl_pardakht_sodeh.Name = "lbl_pardakht_sodeh";
            this.lbl_pardakht_sodeh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_pardakht_sodeh.Size = new System.Drawing.Size(72, 24);
            this.lbl_pardakht_sodeh.TabIndex = 72;
            this.lbl_pardakht_sodeh.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnShowGhobooz
            // 
            this.btnShowGhobooz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShowGhobooz.BackColor = System.Drawing.Color.Transparent;
            this.btnShowGhobooz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShowGhobooz.Location = new System.Drawing.Point(799, 551);
            this.btnShowGhobooz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnShowGhobooz.Name = "btnShowGhobooz";
            this.btnShowGhobooz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(8);
            this.btnShowGhobooz.Size = new System.Drawing.Size(234, 28);
            this.btnShowGhobooz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShowGhobooz.Symbol = "";
            this.btnShowGhobooz.SymbolColor = System.Drawing.Color.Teal;
            this.btnShowGhobooz.SymbolSize = 12F;
            this.btnShowGhobooz.TabIndex = 85;
            this.btnShowGhobooz.Text = "نمایش جزئیات قبض انتخاب شده";
            this.btnShowGhobooz.Click += new System.EventHandler(this.btnShowGhobooz_Click);
            // 
            // buttonX10
            // 
            this.buttonX10.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonX10.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX10.Location = new System.Drawing.Point(8, 556);
            this.buttonX10.Name = "buttonX10";
            this.buttonX10.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX10.Size = new System.Drawing.Size(178, 23);
            this.buttonX10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX10.Symbol = "";
            this.buttonX10.SymbolColor = System.Drawing.Color.Green;
            this.buttonX10.SymbolSize = 9F;
            this.buttonX10.TabIndex = 86;
            this.buttonX10.Text = "چاپ گزارش";
            this.buttonX10.Click += new System.EventHandler(this.buttonX10_Click);
            // 
            // cmb_noe
            // 
            this.cmb_noe.DisplayMember = "Text";
            this.cmb_noe.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_noe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_noe.ForeColor = System.Drawing.Color.Black;
            this.cmb_noe.FormattingEnabled = true;
            this.cmb_noe.ItemHeight = 20;
            this.cmb_noe.Items.AddRange(new object[] {
            this.comboItem6,
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5});
            this.cmb_noe.Location = new System.Drawing.Point(21, 4);
            this.cmb_noe.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmb_noe.Name = "cmb_noe";
            this.cmb_noe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmb_noe.Size = new System.Drawing.Size(154, 26);
            this.cmb_noe.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmb_noe.TabIndex = 25;
            this.cmb_noe.SelectedIndexChanged += new System.EventHandler(this.cmb_noe_SelectedIndexChanged);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(166, 5);
            this.labelX4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(76, 24);
            this.labelX4.TabIndex = 26;
            this.labelX4.Text = "نوع قبض  :";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "خراب";
            this.comboItem1.Value = "خراب";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "تعویض";
            this.comboItem2.Value = "تعویض";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "دورکامل";
            this.comboItem3.Value = "دورکامل";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "عدم قرائت";
            this.comboItem4.Value = "عدم قرائت";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "قطغ";
            this.comboItem5.Value = "قطع";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "همه موارد";
            this.comboItem6.Value = "همه موارد";
            // 
            // Frm_ab_for_all
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 660);
            this.Controls.Add(this.buttonX10);
            this.Controls.Add(this.btnShowGhobooz);
            this.Controls.Add(this.grpStat);
            this.Controls.Add(this.superTabControl1);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_ab_for_all";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "کلیه قبوض آب مشترکین";
            this.Load += new System.EventHandler(this.Frm_all_Ab_ghabz_For_all_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frm_ab_For_all_KeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.abghabzBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControl1)).EndInit();
            this.superTabControl1.ResumeLayout(false);
            this.superTabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ghobooz)).EndInit();
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.grpStat.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.SuperTabControl superTabControl1;
        private DevComponents.DotNetBar.SuperTabControlPanel superTabControlPanel1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_ghobooz;
        private DevComponents.DotNetBar.SuperTabItem superTabItem1;
        private DevComponents.DotNetBar.SuperTabItem superTabItem3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_find_by_gcode;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_ghabz_byCode;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private DevComponents.DotNetBar.ButtonX btn_pardakht_shodeh;
        private DevComponents.DotNetBar.ButtonX btn_padakhr_nashode;
        private DevComponents.DotNetBar.ButtonX btn_dar_hal_barrrasi;
        private DevComponents.DotNetBar.LabelX lblPayam;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX btn_find;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDoreh;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.ButtonX btn_all;
        private AbDataset abDataset;
        private System.Windows.Forms.BindingSource abghabzBindingSource;
        private AbDatasetTableAdapters.ab_ghabzTableAdapter ab_ghabzTableAdapter;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem مشاهدهاطلاعاتکاملقبضToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مشاهدهپرداختیهایقبضToolStripMenuItem;
        private DevComponents.DotNetBar.ButtonX btnFindByGharardad;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_find_by_gharardad;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.GroupPanel grpStat;
        private DevComponents.DotNetBar.LabelX labelX22;
        private DevComponents.DotNetBar.LabelX labelX20;
        private DevComponents.DotNetBar.LabelX labelX21;
        private DevComponents.DotNetBar.LabelX lbl_dar_hal_barrrasi;
        private DevComponents.DotNetBar.LabelX labelX30;
        private DevComponents.DotNetBar.LabelX lbl_pardakht_nasodeh;
        private DevComponents.DotNetBar.LabelX labelX31;
        private DevComponents.DotNetBar.LabelX labelX35;
        private DevComponents.DotNetBar.LabelX lbl_pardakht_sodeh;
        private DevComponents.DotNetBar.ButtonX btnShowGhobooz;
        private DevComponents.DotNetBar.ButtonX buttonX10;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblTedadKol;
        private System.Windows.Forms.DataGridViewTextBoxColumn gcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gharardadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dcodeDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontor_start;
        private System.Windows.Forms.DataGridViewTextBoxColumn kontor_end;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf_gheir_mojaz;
        private System.Windows.Forms.DataGridViewTextBoxColumn masraf_mojaz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ensheab;
        private System.Windows.Forms.DataGridViewTextBoxColumn jarimeh;
        private System.Windows.Forms.DataGridViewTextBoxColumn maliatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sayerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bedehi;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghkolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kasr_hezar;
        private System.Windows.Forms.DataGridViewTextBoxColumn pardakhti;
        private System.Windows.Forms.DataGridViewTextBoxColumn mande;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestankari;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmb_noe;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
    }
}