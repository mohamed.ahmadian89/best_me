﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.Ab
{
    public partial class frm_gharardad_check : MyMetroForm
    {
        public frm_gharardad_check(DataTable dt)
        {
            InitializeComponent();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cmb_gharardad.Items.Add(dt.Rows[i][0]);
            }
        }

        private void frm_gharardad_check_Load(object sender, EventArgs e)
        {

        }

        private void btnChap_Click(object sender, EventArgs e)
        {
            new Ab.Frm_ab_FirstTimeKontorValues().ShowDialog();
        }
    }
}
