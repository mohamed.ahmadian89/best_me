﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using FarsiLibrary.Utils;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using CrystalDecisions.Windows.Forms;

namespace CWMS.Ab
{
    public partial class Frm_ab_management : MyMetroForm
    {
        FrmMain FrmMainOfProject = null;
        int last_ab_dcode = 0;
        public Frm_ab_management()
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            btn_set_parameter.PerformClick();
        }

        public Frm_ab_management(FrmMain frm)
        {
            Classes.ClsMain.ChangeCulture("f");
            InitializeComponent();
            btn_set_parameter.PerformClick();
            FrmMainOfProject = frm;

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Ab.Frm_ab_mablagh frm = new Frm_ab_mablagh();
            frm.ShowDialog();
        }

        private void btn_set_parameter_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DtLastAbDoreh = Classes.clsDoreh.Get_last_Ab_Doreh();
                DataTable DtAbMablagh = Classes.clsAb.Get_last_Ab_mablagh_tasvib_shodeh();
                DataTable DtAbInfo = Classes.clsDoreh.Get_Ab_info();


                if (DtAbInfo.Rows.Count == 0)
                {
                    Payam.Show("کاربر گرامی ، ابتدا به منوی تنظیمات کلی رفته ومقادیر مربوطه را پر نمایید و سپس به این فرم مراجعه نمایید");
                    this.Close();

                }

                if (DtAbMablagh.Rows.Count == 0)
                {
                    Payam.Show("کاربر گرامی ،  در تاریخ فعلی هیچ مصوبه ای برای قیمت آب تعریف نشده است  .. لطفا قیمت مصوب اب را مشخص نمایید");

                    this.Close();
                }

                txt_mablagh.Text = DtAbMablagh.Rows[0]["mablagh"].ToString();
                //بدست اوردن تنظیمات کلی مربوط به دتمامی دوره های اب اوردن جدول اب اینفو
                txtDarsadMaliat.Text = DtAbInfo.Rows[0]["darsad_maliat"].ToString();
                txtZaribSakhtoSaz.Tag = txtZaribSakhtoSaz.Text = DtAbInfo.Rows[0]["ZaribDarhalSakht"].ToString();
                txttejarizarib.Tag = txttejarizarib.Text = DtAbInfo.Rows[0]["zaribtejari"].ToString();
                //بدست اوردن تنظیمات کلی مربوط به دتمامی دوره های اب اوردن جدول اب اینفو
                txt_masraf_omoomi.Text = DtAbInfo.Rows[0]["masraf_omoomi"].ToString();
                txt_fazelab_mablagh.Text = DtAbInfo.Rows[0]["mablagh_fazelab"].ToString();
                txt_aboonman.Text = DtAbInfo.Rows[0]["zarib_aboonman"].ToString();
                hazineh_taviz_kontor.Text= DtAbInfo.Rows[0]["hazineh_taviz_kontor"].ToString();



                if (DtLastAbDoreh.Rows.Count == 0)
                {
                    Create_doreh_for_first_time(DtAbInfo);
                }
                else
                {


                    DateTime StartTime = Convert.ToDateTime(DtLastAbDoreh.Rows[0]["end_time"]);

                    //----------- بدست اوردن بازه دوره بعدی 
                    Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(StartTime, Convert.ToInt32(DtAbInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtAbInfo.Rows[0]["mohlat_pardakht"]));
                    //----------- بدست اوردن بازه دوره بعدی 


                    //--------------- مقداردهی کنترل های روی فرم
                    txt_start.SelectedDateTime = StartTime;
                    txt_end.SelectedDateTime = Doreh_length.End_Of_Doreh;
                    txt_mohlat.SelectedDateTime = Doreh_length.Mohlat_pardakht;
                    //--------------- مقداردهی کنترل های روی فرم


                    txt_doreh_code.Text = (Convert.ToInt32(DtLastAbDoreh.Rows[0]["dcode"]) + 1).ToString();
                    Classes.ClsMain.ChangeCulture("f");
                }

            }
            catch (Exception ex)
            {


                Payam.Show("متاسفانه امکان تعریف دوره جدید وجود ندارد . لطفا با شرکت پشتیبان هماهنگ نمایید");
                Classes.ClsMain.LogIt(ex);

            }
        }


        void Create_doreh_for_first_time(DataTable DtAbInfo)
        {
            txt_doreh_code.Text = "1";
            DateTime StartTime = DateTime.Now;


            //----------- بدست اوردن بازه دوره بعدی 
            Classes.clsDoreh.Doreh_lenght_result Doreh_length = Classes.clsDoreh.Doreh_Length(StartTime, Convert.ToInt32(DtAbInfo.Rows[0]["tedad_mah_dar_doreh"]), Convert.ToInt32(DtAbInfo.Rows[0]["mohlat_pardakht"]));
            //----------- بدست اوردن بازه دوره بعدی 


            txt_start.SelectedDateTime = StartTime;
            txt_end.SelectedDateTime = Doreh_length.End_Of_Doreh;
            txt_mohlat.SelectedDateTime = Doreh_length.Mohlat_pardakht;

        }



        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            try
            {

                if (Classes.clsMoshtarekin.UserInfo.p61)
                {
                    int doreh_should_check_in_kharab_stat = 0;

                    

                    Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 61);
                    try
                    {
                        Convert.ToDecimal(txt_mablagh.Text);
                    }
                    catch
                    {
                        Payam.Show("لطفا مبلغ را به صورت صحیح وارد نمایید");
                        txt_mablagh.SelectAll();
                        txt_mablagh.Focus();
                        return;
                    }

                    try
                    {
                        Convert.ToDecimal(txttejarizarib.Text);


                    }
                    catch
                    {
                        txttejarizarib.Text = txttejarizarib.Tag.ToString();
                        Payam.Show("ضریب تجاری صحیح نمی باشد . مقدار پیش فرض جایگزین ان می شود");
                        return;
                    }
                    try
                    {
                        Convert.ToDecimal(txtZaribSakhtoSaz.Text);
                    }
                    catch
                    {
                        txtZaribSakhtoSaz.Text = txtZaribSakhtoSaz.Tag.ToString();
                        Payam.Show("ضریب ساخت و ساز صحیح نمی باشد . مقدار پیش فرض جایگزین ان می شود");
                        return;
                    }


                    DataTable LastDorehInfo = Classes.ClsMain.GetDataTable("select top(1) end_time,ghabz_sodoor from ab_doreh order by dcode desc ");
                    //-----------------هیچ دوره ای تا به حال در سیستم ایجاد نشده است ----------------


                    if (LastDorehInfo.Rows.Count == 0)
                    {
                        DataRow dr = LastDorehInfo.NewRow();
                        dr["end_time"] = txt_start.SelectedDateTime;
                        dr["ghabz_sodoor"] = true;
                        LastDorehInfo.Rows.Add(dr);

                    }

                    if (DateTime.Now < Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"]))
                        Payam.Show("تا اتمام تاریخ دوره فعلی امکان اضافه کردن دوره جدید وجود ندارد ");
                    else if (Convert.ToBoolean(LastDorehInfo.Rows[0]["ghabz_sodoor"]) == false)
                        Payam.Show("به دلیل صادر نشدن قبوض دوره فعلی ، امکان تعریف دوره جدید وجود ندارد . ");
                    else
                    {

                        if (txt_start.SelectedDateTime < Convert.ToDateTime(txt_start.Tag) && txt_doreh_code.Text != "1")
                        {
                            Payam.Show("تاریخ شروع هر دوره می بایست از تاریخ پایان دوره قبل حتما بیشتر باشد");
                            txt_start.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(txt_start.Tag).Date).ToString("d");
                            return;

                        }
                        if (txt_start.SelectedDateTime > txt_end.SelectedDateTime)
                        {
                            Payam.Show("تاریخ پایان از تاریخ شروع عقب تر یا با آن برابر است .. تاریخ پایان هم اکنون مقدار صحیحی می گیرد.");
                            txt_end.Text = txt_end.Tag.ToString();
                            return;
                        }



                        if (txt_doreh_code.Text == "")
                        {
                            Payam.Show("لطفا بر روی دکمه اطلاعات دوره جدید کلیک نمایید");
                        }

                        else
                        {
                            DateTime Start_time, End_time, mohlat;
                            Start_time = Convert.ToDateTime(txt_start.SelectedDateTime);
                            End_time = Convert.ToDateTime(txt_end.SelectedDateTime);
                            mohlat = Convert.ToDateTime(txt_mohlat.SelectedDateTime);

                            Classes.clsAb.Ijad_Doreh_jadid_ab_V2(
                                Convert.ToInt32(txt_doreh_code.Text), Start_time, End_time, mohlat, Convert.ToDecimal(txt_mablagh.Text),
                                txtDarsadMaliat.Text, txtZaribSakhtoSaz.Text, txttejarizarib.Text
                                , Convert.ToDecimal(txt_fazelab_mablagh.Text), Convert.ToDecimal(txt_aboonman.Text), Convert.ToDecimal(txt_masraf_omoomi.Text), Convert.ToDecimal(hazineh_taviz_kontor.Text), doreh_should_check_in_kharab_stat);
                            

                            txt_doreh_code.Text = "";
                            txt_mablagh.Text = "";
                            txt_mohlat.Text = txt_end.Text = txt_start.Text = "";
                            Payam.Show("دوره جدید با موفقیت در سیستم ایجاد شد ");
                            btn_set_parameter.PerformClick();
                            this.ab_dorehTableAdapter.Fill(this.abDataset.ab_doreh);
                            btn_create_new_doreh.Enabled = false;

                            superTabControl1.SelectedTabIndex = 1;
                        }
                    }
                }
                else
                    Payam.Show("شما مجوز انجام این کار را ندارید");
            }
            catch (Exception ertert)
            {
                Classes.ClsMain.logError(ertert, ertert.LineNumber().ToString());
            }
            Classes.ClsMain.ChangeCulture("f");
        }

        private void FrmAbDoreh_FormClosed(object sender, FormClosedEventArgs e)
        {

            Program.MainFrm.UpdateDorehPanel();

        }


        private void FrmAbDoreh_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'abDataset.ab_doreh' table. You can move, or remove it, as needed.
            this.ab_dorehTableAdapter.Fill(this.abDataset.ab_doreh);
            // TODO: This line of code loads data into the 'abDataset.ab_doreh' table. You can move, or remove it, as needed.

            //------------- عدم نمایش تب ایجاد دوره تا هنگام اتمام دوره فعلی  --------------------------------------
            DataTable LastDorehInfo = Classes.ClsMain.GetDataTable("select top(1) end_time,dcode from ab_doreh order by dcode desc ");
            if (LastDorehInfo.Rows.Count != 0)
            {
                last_ab_dcode = Convert.ToInt32(LastDorehInfo.Rows[0]["dcode"]);
                if (DateTime.Now < Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"]))
                {
                    TabNewDoreh.Enabled = false;
                    lblAlert.Visible = true;
                    lblAlert.Text = "نا اتمام تاریخ دوره فعلی -" + PersianDateConverter.ToPersianDate(Convert.ToDateTime(LastDorehInfo.Rows[0]["end_time"])).ToString("d") + "-" + " امکان تعریف دوره جدید آب وجود ندارد ";
                }
                else last_ab_dcode = -1;
            }

            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                    delegate (object o, RunWorkerCompletedEventArgs args)
                    {
                        timer1.Enabled = false;
                        if (StateOfCreateGhabe == "success")
                        {
                            Payam.Show("عملیات صدور قبوض با موفقیت انجام شد ");
                            ManageKontorKhanInfo(lblSelectedDoreh.Text);

                            lblSecondStep.Visible = true;
                            lblDorehInfotext2.Text = "قبوض آب برای کلیه مشترکین صادر شده است   ";
                            btnReadKontorKhanAndUpdate.Enabled = false;
                            grpStat.Visible = true;
                            grpdelete.Visible = true;
                            btnShowGhobooz.Visible = true;
                            progressBar1.Value = 100;
                            btnChap.Visible = true;
                            grpKontorkhan.Enabled = false;
                            grp_arzesh_afzoodeh.Visible = true;




                        }
                        else
                        {
                            Payam.Show("متاسفانه در هنگام صدور قبض مشکل پیش امده است . سریعا با شرکت پشتیبان تماس حاصل فرمایید");
                        }
                    });


            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(
                    delegate (object o, ProgressChangedEventArgs args)
                    {
                        // اگر خواستی پروگرس باز بذاری، این خط پایین رو از کامنت در بیار
                        // prgrsBar.Value = args.ProgressPercentage;
                        progressBar1.Value = args.ProgressPercentage;
                    });


        }






        private void dgv_doreh_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Classes.ClsMain.ChangeCulture("e");
            //if (dgv_doreh.SelectedRows.Count != 0)
            //{
            //    tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
            //    tabdorehdetails.Visible = true;
            //    lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
            //    lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
            //    lbl_ab_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();
            //    txt_find_gharardad_forSodoor.Text = lbl_info_for_sodoor.Text = "";
            //    btn_chap_tak_ghabz.Visible = btn_sodoor_tak_ghabz.Visible = false;
            //    txt_find_gharardad_forSodoor.Focus();
            //    superTabControl1.SelectedTabIndex = 2;
            //    EtelaatamariInfoPanel();

            //}
            //Classes.ClsMain.ChangeCulture("f");
        }
        void EtelaatamariInfoPanel()
        {
            Classes.ClsMain.ChangeCulture("e");
            ab_dorehTableAdapter.Fill(abDataset.ab_doreh);
            DataView dv = new DataView(abDataset.ab_doreh);
            dv.RowFilter = "dcode=" + lblSelectedDoreh.Text;
            DataTable dt = dv.ToTable();

            grpdelete.Visible = false;
            grp_arzesh_afzoodeh.Visible = false;
            if (Convert.ToBoolean(dt.Rows[0]["ghabz_sodoor"]) == true)
            {

                lblDorehInfotext.Text = "قرائت های فعلی کنتورهای آب در سیستم ذخیره شده اند .  ";
                lblDorehInfotext2.Text = "قبوض آب برای کلیه مشترکین صادر شده است   ";

                lblSecondStep.Visible =
                lblFirstStep.Visible =

                grpStat.Visible =
                btnShowGhobooz.Visible =
                btnChap.Visible = true;

                // امکان حذف قبوض فقط برای دوره اخیر امکان پذیر می بایست باشد

                if (dgv_doreh.Rows[0].Selected)
                    grpdelete.Visible = true;

                grp_arzesh_afzoodeh.Visible = true;

                btnReadKontorKhanAndUpdate.Enabled = btnSodoorGhabzForAllMoshtarekin.Enabled = false;
                lblInfoAboutMoshahedeh.Text = "غیر قابل مشاهده";
                if (Convert.ToBoolean(dt.Rows[0]["visiblebymoshtarek"]) == true)
                {
                    lblInfoAboutMoshahedeh.Text = "قابل مشاهده";
                }

            }

            else
            {
                lblDorehInfotext.Text = "قرائت های فعلی کنتورهای آب هنوز در سیستم ثبت نشده است   .";
                lblFirstStep.Visible = false;
                lblSecondStep.Visible = false;
                lblDorehInfotext2.Text = "هنوز قبوض نهایی صادر نشده است ";
                btnReadKontorKhanAndUpdate.Enabled = btnSodoorGhabzForAllMoshtarekin.Enabled = true;
                grpdelete.Visible = grpStat.Visible = false;
                btnShowGhobooz.Visible = false;
                btnChap.Visible = false;
                grpKontorkhan.Enabled = true;
                grp_arzesh_afzoodeh.Visible = false;
                lblInfoAboutMoshahedeh.Text = "";

            }


            DataTable dt_kontor = Classes.ClsMain.GetDataTable("select * from kontor_khan");
            if (dt_kontor.Rows.Count == 0)
            {
                lblDorehInfotext.Text = "قرائت های  کنتورهای آب هنوز در سیستم ثبت نشده است   .";
                lblFirstStep.Visible = false;
            }
            else
            {
                lblDorehInfotext.Text = "قرائت های کنتورهای اب در سیستم ذخیره شده اند";
                lblFirstStep.Visible = true;

            }





            Classes.ClsMain.ChangeCulture("f");

        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            //ab_dorehTableAdapter.Fill(mainDataSest.ab_doreh);
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            if (dgv_doreh.SelectedRows.Count != 0)
            {
                tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                tabdorehdetails.Visible = true;
                lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
                lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                lbl_ab_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();

                superTabControl1.SelectedTabIndex = 2;
                EtelaatamariInfoPanel();

            }
            Classes.ClsMain.ChangeCulture("f");
        }

        private void btnSodoorGhabzForAllMoshtarekin_Click(object sender, EventArgs e)
        {
            new Ab.Frm_ab_ImportKontorInfo(lblSelectedDoreh.Text).ShowDialog();
            if (Convert.ToInt32(Classes.ClsMain.ExecuteScalar("select count(*) from kontor_khan")) != 0)
            {
                lblDorehInfotext.Text = "قرائت های فعلی کنتورهای آب در سیستم ذخیره شده اند .  ";
                lblFirstStep.Visible = true;
            }
        }


        string GetValidBoolean(string str)
        {
            if (str.ToLower() == "false")
                return "0";
            return "1";
        }

        void ManageKontorKhanInfo(string dcode)
        {
            Classes.ClsMain.ExecuteNoneQuery("delete from kontor_khan_history where dcode=" + dcode + ";insert into kontor_khan_history select " + dcode + " ,* from kontor_khan;delete from kontor_khan;");

        }

        private void btnReadKontorKhanAndUpdate_Click(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
            {
                if (lblSelectedDoreh.Text == "1")
                {
                    DataTable Moshtarek_should_check =
                        Classes.ClsMain.GetDataTable("exec check_tarikh_gheraat_for_first_ab_doreh");
                    if (Moshtarek_should_check.Rows.Count != 0)
                    {
                        Payam.Show("لطفا تاریخ قرائت قبلی آب مشترکین را مشخص نمایید سپس  عملیات صدور قبض را انجام دهید");
                        new Ab.frm_gharardad_check(Moshtarek_should_check).ShowDialog();
                        return;
                    }
                }
            }




            if (Classes.clsMoshtarekin.UserInfo.p65)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 65);

                try
                {
                    if (Cpayam.Show("آیا  با صدور قبوض این دوره موافق هستید؟") == DialogResult.Yes)
                    {
                        btnReadKontorKhanAndUpdate.Enabled = true;

                        Classes.ClsMain.ChangeCulture("e");
                        Classes.clsAb.Dcode = lblSelectedDoreh.Text;
                        if (Classes.ClsMain.ExecuteScalar("select count(*) from kontor_khan").ToString() == "0")
                        {
                            Payam.Show("لطفا ابتدا فایل کنتور خوان را انتخاب کرده تا اطلاعات مربوط به کنتورها در سیستم ثبت گردد");

                        }
                        else if (Classes.ClsMain.ExecuteScalar("select count(*) from Jarime_masraf_mazad").ToString() == "0")
                        {
                            Payam.Show("لطفا ابتدا جرایم مربوط به مصارف مازاد را تعیین کرده و سپس عملیات صدور قبض را انجام دهید");

                        }
                        else if (Classes.ClsMain.ExecuteScalar("select * from EnsheabHazineh").ToString() == "0")
                        {
                            Payam.Show("لطفا ابتدا هزینه انشعاب از منوی امور اب  تعیین کرده و سپس عملیات صدور قبض را انجام دهید");

                        }

                        else
                        {
                            btnReadKontorKhanAndUpdate.Enabled = false;
                            Payam.Show("با تایید شما ، عملیات صدور قبوض آب شروع خواهند شد . لطفا پس از تایید ، چند ثانیه تا اتمام فرایند  صبر نمایید");

                            timer1.Enabled = true;
                            lblDorehInfotext2.Text = "در حال صدور قبوض آب ....";
                            backgroundWorker1.RunWorkerAsync(progressBar1);

                        }


                    }
                }
                catch (Exception exxx)
                {
                    Classes.ClsMain.logError(exxx);
                    lblSecondStep.Visible = false;
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        void UpdateMoshtarekinStatus(Ab.MainAbDataset.GhabzAbTempDataTable dt)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand com = new SqlCommand();
            con.ConnectionString = Classes.ClsMain.ConnectionStr;
            con.Open();
            SqlTransaction transaction = con.BeginTransaction("SodoorGhabzNahayi");
            com.Connection = con;
            com.Transaction = transaction;
            foreach (Ab.MainAbDataset.GhabzAbTempRow item in dt)
            {
                com.CommandText = "update moshtarekin set darhalsakht=" + (item.dar_hal_sakht == true ? 1 : 0).ToString() + " where gharardad=" + item.gharardad;
            }
            transaction.Commit();
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p14)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 14);

                new Ab.Frm_ab_for_all(lblSelectedDoreh.Text).ShowDialog();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }


        void Delete_ghobooz(bool Delete_pardakhti)
        {
            if (Classes.clsMoshtarekin.UserInfo.p67)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 67);

                if (Cpayam.Show("آیا با حذف کلیه قبوض دوره جاری موافق هستید؟") == DialogResult.Yes)
                {


                    //------------------------------------------------------------------
                    SqlConnection con = new SqlConnection();
                    SqlCommand com = new SqlCommand();
                    con.ConnectionString = Classes.ClsMain.ConnectionStr;
                    con.Open();
                    SqlTransaction transaction = con.BeginTransaction("DeleteGhab");
                    com.Connection = con;
                    com.Transaction = transaction;
                    //------------------------------------------------------------------
                    try
                    {
                        if (Delete_pardakhti)
                        {
                            com.CommandText = "delete from ghabz_pardakht where dcode=" + lblSelectedDoreh.Text + " and is_ab=1";
                            com.ExecuteNonQuery();
                        }
                        com.CommandText = "delete from ab_ghabz where dcode=" + lblSelectedDoreh.Text;
                        com.ExecuteNonQuery();


                        com.CommandText = "update ab_doreh set ghabz_sodoor=0  where dcode=" + lblSelectedDoreh.Text;
                        com.ExecuteNonQuery();

                        transaction.Commit();
                        con.Close();
                        lblFirstStep.Visible = lblSecondStep.Visible = false;
                        lblDorehInfotext2.Text = "هنوز قبوض نهایی صادر نشده است ";
                        lblDorehInfotext.Text = "قرائت های فعلی کنتورهای آب هنوز در سیستم ثبت نشده است   .";
                        grpStat.Visible = false;
                        grpdelete.Visible = false;
                        progressBar1.Value = 0;
                        btnReadKontorKhanAndUpdate.Enabled = true;
                        Payam.Show("کلیه قبوض به همراه پرداختی ها با موفقیت از سیستم حذف شدند");
                        this.Close();
                        ab_dorehTableAdapter.Fill(abDataset.ab_doreh);

                    }
                    catch (Exception rt)
                    {
                        Classes.ClsMain.logError(rt);
                        Payam.Show("متاسفانه در هنگام حذف اطلاعات دوره خطا پیش امدها است");
                    }

                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX5_Click(object sender, EventArgs e)
        {
            Delete_ghobooz(true);
        }

        private void BtnDeleteOne_Click(object sender, EventArgs e)
        {
            new Ab.FrmDeleteOneGhabz("ab", lblSelectedDoreh.Text).ShowDialog();

        }

        private void buttonX7_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p72)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 72);

                Classes.ClsMain.ExecuteNoneQuery("update ab_doreh set visiblebymoshtarek=1 where dcode=" + lblSelectedDoreh.Text + ";update ab_ghabz set visiblebymoshtarek=1 where dcode=" + lblSelectedDoreh.Text);
                Payam.Show("مشترکین می توانند قبوض دوره " + lblSelectedDoreh.Text + "  خود را مشاهده کنند ");
                lblInfoAboutMoshahedeh.Text = "قابل مشاهده";
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX8_Click(object sender, EventArgs e)
        {

            if (Classes.clsMoshtarekin.UserInfo.p73)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 73);

                Classes.ClsMain.ExecuteNoneQuery("update ab_doreh set visiblebymoshtarek=0 where dcode=" + lblSelectedDoreh.Text + ";update ab_ghabz set visiblebymoshtarek=0 where dcode=" + lblSelectedDoreh.Text);
                Payam.Show("از این پس مشترکین قادر به مشاهده قبوض این دوره نخواهند بود");
                lblInfoAboutMoshahedeh.Text = "غیر قابل مشاهده";
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX9_Click(object sender, EventArgs e)
        {
            DataTable dt = Classes.ClsMain.GetDataTable("select MIN(gharardad),MAX(gharardad) from ab_ghabz where dcode=" + lblSelectedDoreh.Text);
            new FrmSmsInform("اطلاع رسانی قبوض به کلیه مشترکین", lblSelectedDoreh.Text, 1, "ab").ShowDialog();
        }

        private void buttonX11_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p62)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 62);

                if (dgv_doreh.Rows[0].Selected)
                {
                    Payam.Show("با حذف دوره اخیر ، کلیه اطلاعات آن از سیستم پاک خواهد شد");
                    if (Cpayam.Show("آیا با حذف کلیه اطلاعات مربوط به دوره اخیر موافق هستید؟") == DialogResult.Yes)
                    {
                        //------------------------------------------------------------------

                        string Doreh = dgv_doreh.Rows[0].Cells[0].Value.ToString();

                        //------------------------------------------------------------------
                        SqlConnection con = new SqlConnection();
                        SqlCommand com = new SqlCommand();
                        con.ConnectionString = Classes.ClsMain.ConnectionStr;
                        con.Open();
                        SqlTransaction transaction = con.BeginTransaction("DeleteGhab");
                        com.Connection = con;
                        com.Transaction = transaction;
                        //------------------------------------------------------------------
                        try
                        {
                            com.CommandText = "delete from ghabz_pardakht where dcode=" + Doreh + " and is_ab=1";
                            com.ExecuteNonQuery();

                            com.CommandText = "delete from ab_ghabz where dcode=" + Doreh;
                            com.ExecuteNonQuery();
                            com.CommandText = "delete from ab_doreh where dcode=" + Doreh;
                            com.ExecuteNonQuery();

                            if(Classes.ClsMain.ShahrakSetting.shahrak_ID==Classes.Shahrak_IDS.shams_abad)
                            {
                                com.CommandText = "delete from shams_abad_ab_Additional_info where dcode=" + Doreh;
                                com.ExecuteNonQuery();
                            }


                            transaction.Commit();
                            con.Close();
                        }
                        catch (Exception rt)
                        {
                            Classes.ClsMain.logError(rt);
                            Payam.Show("متاسفانه در هنگام حذف اطلاعات دوره خطا پیش امدها است");
                        }

                        finally
                        {
                            con.Close();
                        }





                        Payam.Show("کلیه اطلاعات مربوط به دوره اخیر از سیستم حذف شد");

                        this.Close();

                    }
                    else
                        Payam.Show("در سیستم فقط امکان حذف آخرین دوره وجود دارد");
                }
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX3_Click_2(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p68)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 68);

                if (dgv_doreh.SelectedRows.Count != 0)
                {
                    bool IsGhoboozSader = abDataset.ab_doreh.Where(p => p.dcode == Convert.ToInt32(dgv_doreh.Rows[0].Cells[0].Value.ToString())).First().ghabz_sodoor;
                    new Sharj.FrmTamdid(dgv_doreh.Rows[0].Cells[0].Value.ToString(),
                        Convert.ToDateTime(dgv_doreh.Rows[0].Cells[1].Value),
                        Convert.ToDateTime(dgv_doreh.Rows[0].Cells[2].Value),
                        Convert.ToDateTime(dgv_doreh.Rows[0].Cells[3].Value), "ab", IsGhoboozSader
                       , Convert.ToDecimal(dgv_doreh.Rows[0].Cells[6].Value), Convert.ToDecimal(dgv_doreh.Rows[0].Cells[7].Value),
                        Convert.ToDecimal(dgv_doreh.Rows[0].Cells[8].Value), Convert.ToDecimal(dgv_doreh.Rows[0].Cells[4].Value),
                        Convert.ToDecimal(dgv_doreh.Rows[0].Cells[9].Value), Convert.ToDecimal(dgv_doreh.Rows[0].Cells[5].Value),
                        Convert.ToDecimal(dgv_doreh.Rows[0].Cells[10].Value)




                        ).ShowDialog();
                }
                ab_dorehTableAdapter.Fill(abDataset.ab_doreh);
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void buttonX10_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ChangeCulture("e");
            if (dgv_doreh.SelectedRows.Count != 0)
            {
                tabdorehdetails.Text = "اطلاعات دوره " + dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                tabdorehdetails.Visible = true;
                lbl_mohlatPardakht.Text = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(Convert.ToDateTime(dgv_doreh.SelectedRows[0].Cells[3].Value)).ToString("d");
                lblSelectedDoreh.Text = dgv_doreh.SelectedRows[0].Cells[0].Value.ToString();
                lbl_ab_mablagh.Text = dgv_doreh.SelectedRows[0].Cells[4].Value.ToString();

                superTabControl1.SelectedTabIndex = 2;
                EtelaatamariInfoPanel();



            }
            Classes.ClsMain.ChangeCulture("f");
        }

        private void buttonX6_Click_1(object sender, EventArgs e)
        {
            new Ab.Frm_ab_CheckGheraat().ShowDialog();
        }

        string StateOfCreateGhabe = "";

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Classes.clsAb AbClass = new Classes.clsAb();
            backgroundWorker1.WorkerReportsProgress = true;
            StateOfCreateGhabe = AbClass.Generate_Ghabz();


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value <= 99)
            {
                progressBar1.Value += 1;
            }
        }

        private void FrmAbDoreh_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnChap_Click(object sender, EventArgs e)
        {
            if (Classes.clsMoshtarekin.UserInfo.p71)
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 71);

                new Frm_ghobooz_chap(lblSelectedDoreh.Text, "آب", "ab").Show();
            }
            else
                Payam.Show("شما مجوز انجام این کار را ندارید");
        }

        private void dgv_doreh_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowDetails.PerformClick();
        }

        private void btnMoshahaedehGheraat_Click(object sender, EventArgs e)
        {
            new Ab.Frm_ab_CheckGheraat().Show();
        }

        private void dgv_doreh_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnChapGheraat_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.ChapGheraat rpt = new CrysReports.ChapGheraat();

            Classes.clsAb.ChapGheraat_For_KontorKhan_ha(repVUer, rpt, Convert.ToInt32(dgv_doreh.SelectedRows[0].Cells[0].Value) - 1);
        }

        private void superTabControl1_SelectedTabChanged(object sender, SuperTabStripSelectedTabChangedEventArgs e)
        {

            if (superTabControl1.SelectedTabIndex == 1)
                tabdorehdetails.Visible = false;
        }

        private void buttonX2_Click_1(object sender, EventArgs e)
        {
            Delete_ghobooz(false);

        }


        void make_valid_date(advanced_report.advanced_report_dataset.Arzesh_afzoodeh_abDataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["tarikh"] = new FarsiLibrary.Utils.PersianDate(Convert.ToDateTime(dt.Rows[i]["tarikh"])).ToString("d");
            }
        }
        private void buttonX6_Click(object sender, EventArgs e)
        {
            CrysReports.maliat.paytakht_ab_arzesh_afzoodeh report1 = new CrysReports.maliat.paytakht_ab_arzesh_afzoodeh();
            advanced_report.advanced_report_dataset ds = new advanced_report.advanced_report_dataset();
            advanced_report.advanced_report_datasetTableAdapters.settingTableAdapter sta = new advanced_report.advanced_report_datasetTableAdapters.settingTableAdapter();
            sta.Fill(ds.setting);
            advanced_report.advanced_report_datasetTableAdapters.Arzesh_afzoodeh_abTableAdapter ab_ta = new advanced_report.advanced_report_datasetTableAdapters.Arzesh_afzoodeh_abTableAdapter();
            ab_ta.FillByDcode(ds.Arzesh_afzoodeh_ab, Convert.ToInt32(lblSelectedDoreh.Text));
            make_valid_date(ds.Arzesh_afzoodeh_ab);
            report1.SetDataSource(ds);
            report1.SetParameterValue("tarikh", new FarsiLibrary.Utils.PersianDate(DateTime.Now).ToString("d"));
            new FrmShowReport(report1).ShowDialog();

        }

        private void btn_gozesh_karbodi_jame_sharj_Click(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                Classes.shahrak_classes.esf_bozorg.Gozaresh_karbordi_ab(Convert.ToInt32(lblSelectedDoreh.Text));
            else if(Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
            {
                Classes.shahrak_classes.paytakht.Gozaresh_karbordi_ab(Convert.ToInt32(lblSelectedDoreh.Text));
            }
            else
            {
                Classes.shahrak_classes.esf_bozorg.Gozaresh_karbordi_ab(Convert.ToInt32(lblSelectedDoreh.Text));
            }

        }

        private void buttonX10_Click_1(object sender, EventArgs e)
        {
            if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.paytakht)
                Classes.shahrak_classes.paytakht.Gozaresh_asnad_hesabdari_ab(Convert.ToInt32(lblSelectedDoreh.Text));
            else if (Classes.ClsMain.ShahrakSetting.shahrak_ID == Classes.Shahrak_IDS.esf_bozorg)
                new forms.esf_bozorg.frm_pishnevis_factor(lblSelectedDoreh.Text, "ab").ShowDialog();
            //Classes.shahrak_classes.esf_bozorg.Gozaresh_asnad_hesabdari_ab(Convert.ToInt32(lblSelectedDoreh.Text));
            else
            {
                Payam.Show("این قابلیت برای شهرک شما پیاده سازی نشده است .و صرفا گزارش پیش فرض نمایش داده خواهد شد");
                Classes.shahrak_classes.paytakht.Gozaresh_asnad_hesabdari_ab(Convert.ToInt32(lblSelectedDoreh.Text));

            }
        }

        private void buttonX12_Click(object sender, EventArgs e)
        {
            new Ab.frm_gozaresh_noe_ghabz().ShowDialog();
        }
    }
}