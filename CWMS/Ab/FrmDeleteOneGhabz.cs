﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS.Ab
{
    public partial class FrmDeleteOneGhabz : DevComponents.DotNetBar.Metro.MetroForm
    {
        string type = "";
        public FrmDeleteOneGhabz(string t,string dcode)
        {
            InitializeComponent();
            type=t;
            lblSelectedDoreh.Text = dcode;
        }

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if(txtGharardad.Text.Trim()!="")
            {
                DataTable dt_ghabz=Classes.ClsMain.GetDataTable("select * from " + type+"_ghabz where gharardad=" + txtGharardad.Text + " and dcode=" + lblSelectedDoreh.Text );
                if(dt_ghabz.Rows.Count==0)
                {
                    Payam.Show("برای این مشترک در این دوره قبضی صادر نشده است ");
                    grpInfo.Visible=false;
                }
                else
                {
                    grpInfo.Visible=true;
                    lblGcode.Text=dt_ghabz.Rows[0]["gcode"].ToString();
                    lblMablagh.Text=dt_ghabz.Rows[0]["mablaghkol"].ToString();
                }
            }
        }

        private void BtnDeleteOne_Click(object sender, EventArgs e)
        {
            if(Cpayam.Show("آیا با حذف این قبض موافقید؟")==DialogResult.Yes)
            {
                Classes.ClsMain.ExecuteNoneQuery("delete from ghabz_pardakht where gcode=" + lblGcode.Text);
                Classes.ClsMain.ExecuteNoneQuery("delete from " + type + "_ghabz  where gcode=" + lblGcode.Text);
                Payam.Show("قبض مربوطه از سیستم حذف شد");
                this.Close();

            }
        }

        private void FrmDeleteOneGhabz_Load(object sender, EventArgs e)
        {
            txtGharardad.Focus();
        }

        private void txtGharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnShowGhobooz.PerformClick();
        }

        private void FrmDeleteOneGhabz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}