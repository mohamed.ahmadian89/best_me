﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;

namespace CWMS.Ab
{
    public partial class Frm_ab_for_all : MyMetroForm
    {

        public Frm_ab_for_all()
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from ab_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";

     
        }

        public Frm_ab_for_all(string dcode) : this()
        {
            cmbDoreh.SelectedValue = dcode;
            btn_find.PerformClick();
        }
        private void Frm_all_Ab_ghabz_For_all_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'abDataset.ab_ghabz' table. You can move, or remove it, as needed.

        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            if (cmbDoreh.Text.Trim() != "")
            {
                this.ab_ghabzTableAdapter.FillByDCode(this.abDataset.ab_ghabz, Convert.ToInt32(cmbDoreh.Text));
                dgv_ghobooz.DataSource = this.abDataset.ab_ghabz;
               
                
                DataTable dt_amar = Classes.ClsMain.GetDataTable("exec sp_ab_amar_info " + cmbDoreh.Text);
                lbl_pardakht_sodeh.Text = dt_amar.Rows[0]["p"].ToString();
                lbl_pardakht_nasodeh.Text = dt_amar.Rows[0]["pn"].ToString();
                lbl_dar_hal_barrrasi.Text = dt_amar.Rows[0]["pj"].ToString();
                lblTedadKol.Text = (
                    Convert.ToDecimal
                    (lbl_pardakht_sodeh.Text) +
                    Convert.ToDecimal(lbl_pardakht_nasodeh.Text) +
                    Convert.ToDecimal(lbl_dar_hal_barrrasi.Text) 

                    ).ToString();
                

                //lbl_majmoo_pardakhti.Text = dt_amar.Rows[0]["sp"].ToString();
                //lbl_Majmoo_pardakhtNashodeh.Text = dt_amar.Rows[0]["spn"].ToString();
                //lblMajmo_darHal_barrasi.Text = dt_amar.Rows[0]["spj"].ToString();
                txt_find_by_gharardad.Text = txt_find_ghabz_byCode.Text = "";

                //lblMajmuKol.Text = (
                //    Convert.ToDecimal(lbl_majmoo_pardakhti.Text) +
                //    Convert.ToDecimal(lbl_Majmoo_pardakhtNashodeh.Text) +
                //    Convert.ToDecimal(lblMajmo_darHal_barrasi.Text)

                //    ).ToString();

            }
            else
            {
                Payam.Show("لطفا یک دوره انتخاب نمایید");
            }

        }

        void Set_pardakhti(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].Cells["pardakhti"].Value =
                      (
                      Convert.ToDecimal(dgv.Rows[i].Cells["mablaghkolDataGridViewTextBoxColumn"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["mande"].Value) -
                      Convert.ToDecimal(dgv.Rows[i].Cells["kasr_hezar"].Value) +
                      Convert.ToDecimal(dgv.Rows[i].Cells["bestankari"].Value)


                      );
            }
        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            if (cmbDoreh.Text.Trim() != "")
            {
                int lastDoreh=Classes.clsAb.Get_last_doreh_dcode();
                this.ab_ghabzTableAdapter.FillByDCode(this.abDataset.ab_ghabz, lastDoreh);
                dgv_ghobooz.DataSource = this.abDataset.ab_ghabz;
                //Amar_log(cmbDoreh.Text);
                //Amar_failed_gahbz(cmbDoreh.Text);
                txt_find_by_gharardad.Text = txt_find_ghabz_byCode.Text = "";
                DataTable dt_amar = Classes.ClsMain.GetDataTable("exec sp_ab_amar_info " + lastDoreh);
                lbl_pardakht_sodeh.Text = dt_amar.Rows[0]["p"].ToString();
                lbl_pardakht_nasodeh.Text = dt_amar.Rows[0]["pn"].ToString();
                lbl_dar_hal_barrrasi.Text = dt_amar.Rows[0]["pj"].ToString();
                lblTedadKol.Text = (
            Convert.ToDecimal(lbl_pardakht_sodeh.Text) +
            Convert.ToDecimal(lbl_pardakht_nasodeh.Text) +
            Convert.ToDecimal(lbl_dar_hal_barrrasi.Text)

            ).ToString();

             //   lbl_majmoo_pardakhti.Text = dt_amar.Rows[0]["sp"].ToString();
             //   lbl_Majmoo_pardakhtNashodeh.Text = dt_amar.Rows[0]["spn"].ToString();
             //   lblMajmo_darHal_barrasi.Text = dt_amar.Rows[0]["spj"].ToString();
             //   txt_find_by_gharardad.Text = txt_find_ghabz_byCode.Text = "";
             //   lblMajmuKol.Text = (
             //Convert.ToDecimal(lbl_majmoo_pardakhti.Text) +
             //Convert.ToDecimal(lbl_Majmoo_pardakhtNashodeh.Text) +
             //Convert.ToDecimal(lblMajmo_darHal_barrasi.Text)

             //).ToString();
            }
            else
            {
                Payam.Show("هیچ قبضی برای این دوره موجود نیست");
            }

        }

        private void btn_dar_hal_barrrasi_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(this.abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=2";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض  با پرداخت جزئی: " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_padakhr_nashode_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(this.abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=0";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت نشده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_pardakht_shodeh_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(this.abDataset.ab_ghabz);
            dv.RowFilter = "vaziat=1";
            dgv_ghobooz.DataSource = dv.ToTable();
            lblPayam.Text = " تعداد قبوض پرداخت شده : " + "(" + dgv_ghobooz.Rows.Count.ToString() + ")";
        }

        private void btn_find_by_gcode_Click(object sender, EventArgs e)
        {
            if (txt_find_ghabz_byCode.Text.Trim() == "")
            {
                DataView dv = new DataView(this.abDataset.ab_ghabz);
                dv.RowFilter = "";
                dgv_ghobooz.DataSource = dv.ToTable();
            }
            else
            {
                DataView dv = new DataView(this.abDataset.ab_ghabz);
                dv.RowFilter = "gcode=" + txt_find_ghabz_byCode.Text;
                dgv_ghobooz.DataSource = dv.ToTable();
            }
        }

        private void dgv_ghobooz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgv_ghobooz.SelectedRows.Count!=0)
            {
                new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells["gcodeDataGridViewTextBoxColumn"].Value.ToString(), dgv_ghobooz.SelectedRows[0].Cells["gharardadDataGridViewTextBoxColumn"].Value.ToString()).ShowDialog();

            }



        }


        private void مشاهدهاطلاعاتکاملقبضToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
                new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString(), dgv_ghobooz.SelectedRows[0].Cells[2].Value.ToString()).ShowDialog();

        }

        private void مشاهدهپرداختیهایقبضToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string gcode = dgv_ghobooz.SelectedRows[0].Cells[0].Value.ToString();
            string doreh = dgv_ghobooz.SelectedRows[0].Cells[1].Value.ToString();
            string gharardad = dgv_ghobooz.SelectedRows[0].Cells[2].Value.ToString();
            string mablaghkol = dgv_ghobooz.SelectedRows[0].Cells[9].Value.ToString();
            string mhoroof = Classes.clsNumber.Number_to_Str(mablaghkol);
            Classes.ClsMain.ChangeCulture("e");
            string tarikh = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(dgv_ghobooz.SelectedRows[0].Cells[3].Value.ToString()).ToString("d");
            Classes.ClsMain.ChangeCulture("f");


            long bestankari_from_last_doreh = Convert.ToInt64(Classes.ClsMain.ExecuteScalar(
  "select isnull(bestankari_from_last_doreh,0) from ab_ghabz where gcode=" + gcode
  ));

            new Sharj.FrmGhabzPardakht(true, gcode, doreh, gharardad, mablaghkol, mhoroof,bestankari_from_last_doreh, tarikh).ShowDialog();

        }

        private void dgv_ghobooz_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
                dgv_ghobooz.Rows[e.RowIndex].Selected = true;
        }

        private void txt_find_ghabz_byCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gcode.PerformClick();
        }

        private void txt_find_by_gharardad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnFindByGharardad.PerformClick();
        }

        private void btnFindByGharardad_Click(object sender, EventArgs e)
        {
            if (txt_find_by_gharardad.Text.Trim() == "")
            {
                DataView dv = new DataView(this.abDataset.ab_ghabz);
                dv.RowFilter = "";
                dgv_ghobooz.DataSource = dv.ToTable();
            }
            else
            {
                DataView dv = new DataView(this.abDataset.ab_ghabz);
                dv.RowFilter = "gharardad=" + txt_find_by_gharardad.Text;
                dgv_ghobooz.DataSource = dv.ToTable();
            }
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
        
        }

        private void Frm_ab_For_all_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void dgv_ghobooz_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnShowGhobooz.PerformClick();
        }

        private void btnShowGhobooz_Click(object sender, EventArgs e)
        {
            if (dgv_ghobooz.SelectedRows.Count != 0)
                new Frm_ab_details(dgv_ghobooz.SelectedRows[0].Cells["gcodeDataGridViewTextBoxColumn"].Value.ToString(), dgv_ghobooz.SelectedRows[0].Cells["gharardadDataGridViewTextBoxColumn"].Value.ToString()).ShowDialog();

        }

        private void buttonX10_Click(object sender, EventArgs e)
        {

            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.rptRizGozareshAbForAll rpt = new CrysReports.rptRizGozareshAbForAll();
            AbDatasetTableAdapters.settingTableAdapter ta =
                        new AbDatasetTableAdapters.settingTableAdapter();
            ta.Fill(abDataset.setting);

            Classes.clsAb.RizHeasbAbForAll(
                abDataset,
                repVUer,
                rpt

                );
        }

        private void cmb_noe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_noe.Text == "خراب")
                abghabzBindingSource.Filter = "kharab=1";
            else if (cmb_noe.Text == "تعویض")
                abghabzBindingSource.Filter = "taviz=1";
            else if (cmb_noe.Text == "عدم قرائت")
                abghabzBindingSource.Filter = "adam_gheraat=1";
            else if (cmb_noe.Text == "دورکامل")
                abghabzBindingSource.Filter = "dor_kamel=1";
            else if (cmb_noe.Text == "قطع")
                abghabzBindingSource.Filter = "ghat=1";
            else
                abghabzBindingSource.RemoveFilter();

        }
    }
}