﻿namespace CWMS.Ab
{
    partial class Frm_masraf_mazad_mesal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtmasraf_gheir_mojaz = new System.Windows.Forms.TextBox();
            this.txtmasraf_kol = new System.Windows.Forms.TextBox();
            this.txtmasraf_mojaz = new System.Windows.Forms.TextBox();
            this.DgvResult = new System.Windows.Forms.DataGridView();
            this.jarime_masraf_mazadTableAdapter = new CWMS.Ab.MainAbDatasetTableAdapters.Jarime_masraf_mazadTableAdapter();
            this.jarimemasrafmazadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abDataset = new CWMS.Ab.MainAbDataset();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_total_gheir_mojaz = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_total_mojaz = new System.Windows.Forms.TextBox();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ض = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_tarefe = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jarimemasrafmazadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(290, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "مصرف غیر مجاز:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "مصرف کل:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(290, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "مصرف مجاز:";
            // 
            // txtmasraf_gheir_mojaz
            // 
            this.txtmasraf_gheir_mojaz.Location = new System.Drawing.Point(373, 53);
            this.txtmasraf_gheir_mojaz.Name = "txtmasraf_gheir_mojaz";
            this.txtmasraf_gheir_mojaz.Size = new System.Drawing.Size(100, 26);
            this.txtmasraf_gheir_mojaz.TabIndex = 10;
            this.txtmasraf_gheir_mojaz.TextChanged += new System.EventHandler(this.txtmasraf_gheir_mojaz_TextChanged);
            // 
            // txtmasraf_kol
            // 
            this.txtmasraf_kol.Location = new System.Drawing.Point(373, 83);
            this.txtmasraf_kol.Name = "txtmasraf_kol";
            this.txtmasraf_kol.Size = new System.Drawing.Size(100, 26);
            this.txtmasraf_kol.TabIndex = 11;
            // 
            // txtmasraf_mojaz
            // 
            this.txtmasraf_mojaz.Location = new System.Drawing.Point(373, 23);
            this.txtmasraf_mojaz.Name = "txtmasraf_mojaz";
            this.txtmasraf_mojaz.Size = new System.Drawing.Size(100, 26);
            this.txtmasraf_mojaz.TabIndex = 9;
            this.txtmasraf_mojaz.TextChanged += new System.EventHandler(this.txtmasraf_mojaz_TextChanged);
            // 
            // DgvResult
            // 
            this.DgvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvResult.BackgroundColor = System.Drawing.Color.White;
            this.DgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.ض,
            this.Column2,
            this.Column3});
            this.DgvResult.Location = new System.Drawing.Point(545, 12);
            this.DgvResult.Name = "DgvResult";
            this.DgvResult.RowHeadersVisible = false;
            this.DgvResult.Size = new System.Drawing.Size(398, 281);
            this.DgvResult.TabIndex = 15;
            // 
            // jarime_masraf_mazadTableAdapter
            // 
            this.jarime_masraf_mazadTableAdapter.ClearBeforeFill = true;
            // 
            // jarimemasrafmazadBindingSource
            // 
            this.jarimemasrafmazadBindingSource.DataMember = "Jarime_masraf_mazad";
            this.jarimemasrafmazadBindingSource.DataSource = this.abDataset;
            // 
            // abDataset
            // 
            this.abDataset.DataSetName = "MainAbDataset";
            this.abDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dataGridView1.DataSource = this.jarimemasrafmazadBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(13, 12);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(263, 273);
            this.dataGridView1.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ta_darsad";
            this.dataGridViewTextBoxColumn1.HeaderText = "تا این درصد مازاد مصرف";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "zarib_ab_baha";
            this.dataGridViewTextBoxColumn2.HeaderText = "این ضریب اعمال گردد";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("B Yekan", 14F);
            this.button1.Location = new System.Drawing.Point(395, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 36);
            this.button1.TabIndex = 17;
            this.button1.Text = "محاسبه";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(295, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 18);
            this.label4.TabIndex = 19;
            this.label4.Text = "هزینه مصرف غیر مجاز :";
            // 
            // txt_total_gheir_mojaz
            // 
            this.txt_total_gheir_mojaz.Location = new System.Drawing.Point(408, 203);
            this.txt_total_gheir_mojaz.Name = "txt_total_gheir_mojaz";
            this.txt_total_gheir_mojaz.Size = new System.Drawing.Size(128, 26);
            this.txt_total_gheir_mojaz.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 18);
            this.label5.TabIndex = 20;
            this.label5.Text = "هزینه  مصرف مجاز :";
            // 
            // txt_total_mojaz
            // 
            this.txt_total_mojaz.Location = new System.Drawing.Point(408, 233);
            this.txt_total_mojaz.Name = "txt_total_mojaz";
            this.txt_total_mojaz.Size = new System.Drawing.Size(128, 26);
            this.txt_total_mojaz.TabIndex = 21;
            // 
            // txt_total
            // 
            this.txt_total.Location = new System.Drawing.Point(408, 264);
            this.txt_total.Name = "txt_total";
            this.txt_total.Size = new System.Drawing.Size(128, 26);
            this.txt_total.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(295, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 18);
            this.label6.TabIndex = 22;
            this.label6.Text = "هزینه کل :";
            // 
            // Column1
            // 
            this.Column1.FillWeight = 65F;
            this.Column1.HeaderText = "درصد";
            this.Column1.Name = "Column1";
            // 
            // Column4
            // 
            dataGridViewCellStyle7.Format = "0,0";
            this.Column4.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column4.HeaderText = "درصد/مصرف";
            this.Column4.Name = "Column4";
            // 
            // ض
            // 
            this.ض.FillWeight = 50F;
            this.ض.HeaderText = "ضریب";
            this.ض.Name = "ض";
            // 
            // Column2
            // 
            dataGridViewCellStyle8.Format = "0,0";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column2.HeaderText = "مصرف";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            dataGridViewCellStyle9.Format = "0,0";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column3.HeaderText = "هزینه";
            this.Column3.Name = "Column3";
            // 
            // txt_tarefe
            // 
            this.txt_tarefe.Location = new System.Drawing.Point(373, 113);
            this.txt_tarefe.Name = "txt_tarefe";
            this.txt_tarefe.Size = new System.Drawing.Size(100, 26);
            this.txt_tarefe.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(288, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 18);
            this.label7.TabIndex = 24;
            this.label7.Text = "تعرفه مجاز اب:";
            // 
            // Frm_masraf_mazad_mesal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 305);
            this.Controls.Add(this.txt_tarefe);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_total);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_total_mojaz);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_total_gheir_mojaz);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.DgvResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtmasraf_gheir_mojaz);
            this.Controls.Add(this.txtmasraf_kol);
            this.Controls.Add(this.txtmasraf_mojaz);
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Frm_masraf_mazad_mesal";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "بررسی نحوه محاسبه مصرف مازاد بر اساس یک مثال";
            this.Load += new System.EventHandler(this.Frm_masraf_mazad_mesal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jarimemasrafmazadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtmasraf_gheir_mojaz;
        private System.Windows.Forms.TextBox txtmasraf_kol;
        private System.Windows.Forms.TextBox txtmasraf_mojaz;
        private System.Windows.Forms.DataGridView DgvResult;
        private MainAbDatasetTableAdapters.Jarime_masraf_mazadTableAdapter jarime_masraf_mazadTableAdapter;
        private System.Windows.Forms.BindingSource jarimemasrafmazadBindingSource;
        private MainAbDataset abDataset;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_total_gheir_mojaz;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_total_mojaz;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ض;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TextBox txt_tarefe;
        private System.Windows.Forms.Label label7;
    }
}