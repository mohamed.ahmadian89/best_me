﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
namespace CWMS.Ab
{
    public partial class FrmChooseMoshtarekForAbGhabz : MyMetroForm
    {
        public FrmChooseMoshtarekForAbGhabz()
        {
            InitializeComponent();
        }

        void LoadData()
        {
            this.moshtarekinTableAdapter.Fill(this.mainDataSest.moshtarekin);
            abBinding.Filter = "have_ab_ghabz=0";
            SharjBinding.Filter = "have_sharj_ghabz=0";

            tabAll.Text = "کلیه مشترکین" + ":" + moshtarekinBindingSource.Count;
            tabAb.Text = "مشترکینی که قبوض آب برای آنها صادر نمی شود" + ":" + abBinding.Count;
            tabSharj.Text = "مشترکینی که قبوض شارژ برای آنها صادر نمی شود" + ":" + SharjBinding.Count;
        }

        private void FrmChooseMoshtarekForAbGhabz_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainDataSest.moshtarekin' table. You can move, or remove it, as needed.

            LoadData();

        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            moshtarekinBindingSource.EndEdit();
            moshtarekinTableAdapter.Update(this.mainDataSest.moshtarekin);
            Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
            tabAll.Text = "کلیه مشترکین" + ":" + moshtarekinBindingSource.Count;
            tabAb.Text = "مشترکینی که قبوض آب برای آنها صادر نمی شود" + ":" + abBinding.Count;
            tabSharj.Text = "مشترکینی که قبوض شارژ برای آنها صادر نمی شود" + ":" + SharjBinding.Count;
        }



        private void txtFindByGhararda_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtFindByGhararda.Text != "")
                if (e.KeyCode == Keys.Enter)
                {
                    btn_find_by_gharardad.PerformClick();
                }
        }

        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            moshtarekinBindingSource.RemoveFilter();
        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            try
            {
                moshtarekinBindingSource.Filter = "gharardad=" + txtFindByGhararda.Text;

            }
            catch (Exception)
            {

                moshtarekinBindingSource.RemoveFilter();

            }
        }

        private void grpUpdateGhobooozAb_Click(object sender, EventArgs e)
        {

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ExecuteNoneQuery("update moshtarekin set have_ab_ghabz=1");
            LoadData();
            Payam.Show("قبوض آب برای کلیه مشترکین صادر خواهد شد ");

        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            Classes.ClsMain.ExecuteNoneQuery("update moshtarekin set have_sharj_ghabz=1");
            LoadData();
            Payam.Show("قبوض شارژ برای کلیه مشترکین صادر خواهد شد ");
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            string com = "delete from kontor_khan_input;";
            com += "delete from kontor_khan_input;" +
                    "insert into kontor_khan_input" +
                    "select moshtarekin.gharardad,'13940101', co_name from moshtarekin" +
                    "inner join ab_ghabz" +
                    "on moshtarekin.gharardad = ab_ghabz.gharardad" +
"where dcode = (select MAX(dcode) from ab_doreh) ";
            Classes.ClsMain.ExecuteNoneQuery(com);
            Payam.Show("هم اکنون می توانید دستگاه کنتورخوان را به سیستم متصل نمایید و عملیات مربوطه را انجام دهید.");
        }
    }
}