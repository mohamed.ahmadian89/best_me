﻿namespace CWMS.Ab
{
    partial class _ّFrmNewTakGhabz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpUpdateGhobooozAb = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.txtdate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.chSakhtoSaz = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chDoreKamel = new System.Windows.Forms.CheckBox();
            this.chTaviz = new System.Windows.Forms.CheckBox();
            this.chAdameGheraat = new System.Windows.Forms.CheckBox();
            this.chGhat = new System.Windows.Forms.CheckBox();
            this.chKarab = new System.Windows.Forms.CheckBox();
            this.txtfelli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btn_create_new_ghabz = new DevComponents.DotNetBar.ButtonX();
            this.txtGhabli = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.grpUpdateGhobooozAb.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpUpdateGhobooozAb
            // 
            this.grpUpdateGhobooozAb.BackColor = System.Drawing.Color.Azure;
            this.grpUpdateGhobooozAb.CanvasColor = System.Drawing.Color.Azure;
            this.grpUpdateGhobooozAb.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grpUpdateGhobooozAb.Controls.Add(this.buttonX1);
            this.grpUpdateGhobooozAb.Controls.Add(this.txtdate);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX3);
            this.grpUpdateGhobooozAb.Controls.Add(this.chSakhtoSaz);
            this.grpUpdateGhobooozAb.Controls.Add(this.groupBox1);
            this.grpUpdateGhobooozAb.Controls.Add(this.txtfelli);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX2);
            this.grpUpdateGhobooozAb.Controls.Add(this.btn_create_new_ghabz);
            this.grpUpdateGhobooozAb.Controls.Add(this.txtGhabli);
            this.grpUpdateGhobooozAb.Controls.Add(this.labelX1);
            this.grpUpdateGhobooozAb.DisabledBackColor = System.Drawing.Color.Empty;
            this.grpUpdateGhobooozAb.Font = new System.Drawing.Font("B Yekan", 9F);
            this.grpUpdateGhobooozAb.Location = new System.Drawing.Point(13, 13);
            this.grpUpdateGhobooozAb.Margin = new System.Windows.Forms.Padding(4);
            this.grpUpdateGhobooozAb.Name = "grpUpdateGhobooozAb";
            this.grpUpdateGhobooozAb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.grpUpdateGhobooozAb.Size = new System.Drawing.Size(843, 187);
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grpUpdateGhobooozAb.Style.BackColorGradientAngle = 90;
            this.grpUpdateGhobooozAb.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grpUpdateGhobooozAb.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderBottomWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grpUpdateGhobooozAb.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderLeftWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderRightWidth = 1;
            this.grpUpdateGhobooozAb.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grpUpdateGhobooozAb.Style.BorderTopWidth = 1;
            this.grpUpdateGhobooozAb.Style.CornerDiameter = 4;
            this.grpUpdateGhobooozAb.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grpUpdateGhobooozAb.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grpUpdateGhobooozAb.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grpUpdateGhobooozAb.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grpUpdateGhobooozAb.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grpUpdateGhobooozAb.TabIndex = 20;
            this.grpUpdateGhobooozAb.Text = "اطلاعات مربوط به قبض جدید آب";
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.BackColor = System.Drawing.Color.Transparent;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.DialogResult = System.Windows.Forms.DialogResult.No;
            this.buttonX1.Location = new System.Drawing.Point(215, 129);
            this.buttonX1.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX1.Size = new System.Drawing.Size(91, 25);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.Symbol = "";
            this.buttonX1.SymbolColor = System.Drawing.Color.Crimson;
            this.buttonX1.SymbolSize = 10F;
            this.buttonX1.TabIndex = 170;
            this.buttonX1.Text = "انصراف";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // txtdate
            // 
            this.txtdate.Location = new System.Drawing.Point(177, 17);
            this.txtdate.Name = "txtdate";
            this.txtdate.Size = new System.Drawing.Size(120, 20);
            this.txtdate.TabIndex = 3;
            this.txtdate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(285, 17);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(76, 25);
            this.labelX3.TabIndex = 161;
            this.labelX3.Text = "تاریخ قرائت :";
            // 
            // chSakhtoSaz
            // 
            this.chSakhtoSaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chSakhtoSaz.AutoSize = true;
            this.chSakhtoSaz.BackColor = System.Drawing.Color.Transparent;
            this.chSakhtoSaz.ForeColor = System.Drawing.Color.Black;
            this.chSakhtoSaz.Location = new System.Drawing.Point(576, 127);
            this.chSakhtoSaz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chSakhtoSaz.Name = "chSakhtoSaz";
            this.chSakhtoSaz.Size = new System.Drawing.Size(211, 22);
            this.chSakhtoSaz.TabIndex = 160;
            this.chSakhtoSaz.Text = "این مشترک در حال ساخت و ساز می باشد";
            this.chSakhtoSaz.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.chDoreKamel);
            this.groupBox1.Controls.Add(this.chTaviz);
            this.groupBox1.Controls.Add(this.chAdameGheraat);
            this.groupBox1.Controls.Add(this.chGhat);
            this.groupBox1.Controls.Add(this.chKarab);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(22, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(791, 71);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "وضعیت کنتور :";
            // 
            // chDoreKamel
            // 
            this.chDoreKamel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chDoreKamel.AutoSize = true;
            this.chDoreKamel.ForeColor = System.Drawing.Color.Black;
            this.chDoreKamel.Location = new System.Drawing.Point(7, 31);
            this.chDoreKamel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chDoreKamel.Name = "chDoreKamel";
            this.chDoreKamel.Size = new System.Drawing.Size(142, 22);
            this.chDoreKamel.TabIndex = 166;
            this.chDoreKamel.Text = "کنتور دور کامل شده است";
            this.chDoreKamel.UseVisualStyleBackColor = true;
            // 
            // chTaviz
            // 
            this.chTaviz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chTaviz.AutoSize = true;
            this.chTaviz.ForeColor = System.Drawing.Color.Black;
            this.chTaviz.Location = new System.Drawing.Point(193, 31);
            this.chTaviz.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chTaviz.Name = "chTaviz";
            this.chTaviz.Size = new System.Drawing.Size(136, 22);
            this.chTaviz.TabIndex = 167;
            this.chTaviz.Text = "کنتور تعویض شده است ";
            this.chTaviz.UseVisualStyleBackColor = true;
            // 
            // chAdameGheraat
            // 
            this.chAdameGheraat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chAdameGheraat.AutoSize = true;
            this.chAdameGheraat.ForeColor = System.Drawing.Color.Black;
            this.chAdameGheraat.Location = new System.Drawing.Point(355, 31);
            this.chAdameGheraat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chAdameGheraat.Name = "chAdameGheraat";
            this.chAdameGheraat.Size = new System.Drawing.Size(135, 22);
            this.chAdameGheraat.TabIndex = 168;
            this.chAdameGheraat.Text = "کنتور قرائت نشده است ";
            this.chAdameGheraat.UseVisualStyleBackColor = true;
            // 
            // chGhat
            // 
            this.chGhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chGhat.AutoSize = true;
            this.chGhat.ForeColor = System.Drawing.Color.Black;
            this.chGhat.Location = new System.Drawing.Point(530, 31);
            this.chGhat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chGhat.Name = "chGhat";
            this.chGhat.Size = new System.Drawing.Size(99, 22);
            this.chGhat.TabIndex = 169;
            this.chGhat.Text = "کنتور قطع است ";
            this.chGhat.UseVisualStyleBackColor = true;
            // 
            // chKarab
            // 
            this.chKarab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chKarab.AutoSize = true;
            this.chKarab.ForeColor = System.Drawing.Color.Black;
            this.chKarab.Location = new System.Drawing.Point(652, 31);
            this.chKarab.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chKarab.Name = "chKarab";
            this.chKarab.Size = new System.Drawing.Size(104, 22);
            this.chKarab.TabIndex = 165;
            this.chKarab.Text = "کنتور خراب است ";
            this.chKarab.UseVisualStyleBackColor = true;
            // 
            // txtfelli
            // 
            this.txtfelli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfelli.Border.Class = "TextBoxBorder";
            this.txtfelli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfelli.DisabledBackColor = System.Drawing.Color.White;
            this.txtfelli.ForeColor = System.Drawing.Color.Black;
            this.txtfelli.Location = new System.Drawing.Point(399, 16);
            this.txtfelli.Margin = new System.Windows.Forms.Padding(4);
            this.txtfelli.Name = "txtfelli";
            this.txtfelli.PreventEnterBeep = true;
            this.txtfelli.Size = new System.Drawing.Size(80, 26);
            this.txtfelli.TabIndex = 2;
            this.txtfelli.TextChanged += new System.EventHandler(this.textBoxX1_TextChanged);
            this.txtfelli.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfelli_KeyDown);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(472, 17);
            this.labelX2.Margin = new System.Windows.Forms.Padding(4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(96, 25);
            this.labelX2.TabIndex = 30;
            this.labelX2.Text = "شماره کنتور فعلی :";
            // 
            // btn_create_new_ghabz
            // 
            this.btn_create_new_ghabz.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_create_new_ghabz.BackColor = System.Drawing.Color.Transparent;
            this.btn_create_new_ghabz.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_create_new_ghabz.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_create_new_ghabz.Location = new System.Drawing.Point(4, 129);
            this.btn_create_new_ghabz.Margin = new System.Windows.Forms.Padding(4);
            this.btn_create_new_ghabz.Name = "btn_create_new_ghabz";
            this.btn_create_new_ghabz.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_create_new_ghabz.Size = new System.Drawing.Size(200, 25);
            this.btn_create_new_ghabz.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_create_new_ghabz.Symbol = "";
            this.btn_create_new_ghabz.SymbolColor = System.Drawing.Color.Green;
            this.btn_create_new_ghabz.SymbolSize = 10F;
            this.btn_create_new_ghabz.TabIndex = 4;
            this.btn_create_new_ghabz.Text = "ثبت قبض جدید در سیستم";
            this.btn_create_new_ghabz.Click += new System.EventHandler(this.btn_create_new_ghabz_Click);
            // 
            // txtGhabli
            // 
            this.txtGhabli.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGhabli.Border.Class = "TextBoxBorder";
            this.txtGhabli.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGhabli.DisabledBackColor = System.Drawing.Color.White;
            this.txtGhabli.ForeColor = System.Drawing.Color.Black;
            this.txtGhabli.Location = new System.Drawing.Point(612, 16);
            this.txtGhabli.Margin = new System.Windows.Forms.Padding(4);
            this.txtGhabli.Name = "txtGhabli";
            this.txtGhabli.PreventEnterBeep = true;
            this.txtGhabli.ReadOnly = true;
            this.txtGhabli.Size = new System.Drawing.Size(80, 26);
            this.txtGhabli.TabIndex = 1;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(700, 17);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(87, 25);
            this.labelX1.TabIndex = 24;
            this.labelX1.Text = "شماره کنتور قبلی :";
            // 
            // _ّFrmNewTakGhabz
            // 
            this.AcceptButton = this.btn_create_new_ghabz;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 213);
            this.ControlBox = false;
            this.Controls.Add(this.grpUpdateGhobooozAb);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "_ّFrmNewTakGhabz";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ثبت قبض جدید آب";
            this.Load += new System.EventHandler(this._ّFrmNewTakGhabz_Load);
            this.grpUpdateGhobooozAb.ResumeLayout(false);
            this.grpUpdateGhobooozAb.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel grpUpdateGhobooozAb;
        private DevComponents.DotNetBar.ButtonX btn_create_new_ghabz;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGhabli;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfelli;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chDoreKamel;
        private System.Windows.Forms.CheckBox chTaviz;
        private System.Windows.Forms.CheckBox chAdameGheraat;
        private System.Windows.Forms.CheckBox chGhat;
        private System.Windows.Forms.CheckBox chKarab;
        private System.Windows.Forms.CheckBox chSakhtoSaz;
        private FarsiLibrary.Win.Controls.FADatePicker txtdate;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}