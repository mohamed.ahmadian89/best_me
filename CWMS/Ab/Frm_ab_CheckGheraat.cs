﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions.Windows.Forms;
using System.Collections;
using System.Linq;

namespace CWMS.Ab
{
    public partial class Frm_ab_CheckGheraat : DevComponents.DotNetBar.Metro.MetroForm
    {
        public Frm_ab_CheckGheraat()
        {
            Classes.ClsMain.ChangeCulture("f");

            InitializeComponent();

        }
        string MoshtarekGharardad = "";


        Dictionary<int, string> Kontor_endValues, kontor_GheraatGhabli_tarikh;

        //string CurrentDcode = "";
        //Dictionary<string, string> LastKontorValues;
        //Dictionary<string, string> CurrentKontorValues;

        //Dictionary<string, string> MoshtarekTopKontorRagham;
        bool Isloading = false;
        private void FrmCheckGheraat_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mainAbDataset.kontor_khan' table. You can move, or remove it, as needed.
            int lstAbdcode = Classes.clsDoreh.Get_last_Ab_dcode();
            LblDoreh.Text = "ثبت قرائت های کنتور آب مشترکین برای دوره :" + lstAbdcode;

            Isloading = true;
            this.kontor_khanTableAdapter.Fill(this.mainAbDataset.kontor_khan);
            txtFindByGhararda.Focus();
            FarsiLibrary.Utils.PersianDate pd = FarsiLibrary.Utils.PersianDateConverter.ToPersianDate(DateTime.Now);
            string persiandate = pd.ToString("d").Replace("/", "");

            if (mainAbDataset.kontor_khan.Rows.Count == 0)
            {
                if (Cpayam.Show("هیچ اطلاعاتی در جدول کنتورخوان سیستم وجود ندارد . آیا مایل هستید سیستم  بر اساس قبوض دوره قبل ، اقدام به ایجاد آن نماید؟") == DialogResult.Yes)
                {
                    Classes.ClsMain.ChangeCulture("e");
                    Classes.ClsMain.ExecuteNoneQuery("insert into kontor_khan (gharardad,tarikh,shomareh_kontor)  select moshtarekin.gharardad,getdate(),ab_ghabz.kontor_end from moshtarekin inner join  ab_ghabz on moshtarekin.gharardad=ab_ghabz.gharardad where have_ab_ghabz=1 and ab_ghabz.dcode=" + (lstAbdcode - 1).ToString());
                    this.kontor_khanTableAdapter.Fill(this.mainAbDataset.kontor_khan);

                    // این حالت زمانی اتفاق میفته که بسیار نادره اما دوره قبلی وجود نداره که بخواد داده های اون رو بیاره
                    if (this.mainAbDataset.kontor_khan.Rows.Count == 0)
                    {
                        Classes.ClsMain.ExecuteNoneQuery("insert into kontor_khan (gharardad,tarikh,shomareh_kontor)  select moshtarekin.gharardad,getdate(),0 from moshtarekin where have_ab_ghabz=1");
                        this.kontor_khanTableAdapter.Fill(this.mainAbDataset.kontor_khan);
                    }
                    Classes.ClsMain.ChangeCulture("f");

                }
                else
                {
                    Payam.Show("پس لطفا به نرم افزار کنتورخوان مراجعه نموده و اقدام به ارسال اطلاعات از دستگاه به سیستم نمایید");
                    this.Close();
                }
            }
            else
            {
                dgvShouldAdd.DataSource = Classes.ClsMain.GetDataTable("select gharardad as [قرارداد],co_name as [نام مشترک] from moshtarekin where have_ab_ghabz=1 and gharardad not in (select gharardad from kontor_khan)");
                //if(dgvShouldAdd.Rows.Count==0)
                //{
                //    grpDoreh.Top = grpShouldAdd.Top;
                //    grpDoreh.Height += grpShouldAdd.Height;
                //    grpShouldAdd.Visible = false;
                //    dgv_doreh.Height += grpShouldAdd.Height - 100;
                //}
            }


            DataTable dt = Classes.ClsMain.GetDataTable("select  ab_ghabz.tarikh_gheraat as gheraat_ghabli_tarikh, ab_ghabz.kontor_end as kontor_end,moshtarekin.gharardad as gharardad from moshtarekin inner join ab_ghabz on moshtarekin.gharardad = ab_ghabz.gharardad where ab_ghabz.dcode = " + (lstAbdcode - 1).ToString());

            Kontor_endValues = new Dictionary<int, string>();
            kontor_GheraatGhabli_tarikh = new Dictionary<int, string>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Kontor_endValues.Add(Convert.ToInt32(dt.Rows[i]["gharardad"]), dt.Rows[i]["kontor_end"].ToString());
                kontor_GheraatGhabli_tarikh.Add(Convert.ToInt32(dt.Rows[i]["gharardad"]), dt.Rows[i]["gheraat_ghabli_tarikh"].ToString());
            }





            //Kontor_endValues = Classes.ClsMain.GetDataTable("select gharardad,kontor_ragham from moshtarekin");



            //if (CurrentDcode == "1")
            //{
            //    DataTable dt_first = Classes.ClsMain.GetDataTable("select * from kontor_first_time_values");
            //    for (int i = 0; i < dt_first.Rows.Count; i++)
            //    {
            //        LastKontorValues[dt_first.Rows[i]["gharardad"].ToString()] = dt_first.Rows[i]["kontor_start"].ToString();
            //    }
            //}
            //else
            //{
            //    DataTable dt_first = Classes.ClsMain.GetDataTable("select * from ab_ghabz where dcode=" + (Convert.ToInt32(CurrentDcode) - 1).ToString());
            //    for (int i = 0; i < dt_first.Rows.Count; i++)
            //    {
            //        LastKontorValues[dt_first.Rows[i]["gharardad"].ToString()] = dt_first.Rows[i]["kontor_end"].ToString();
            //    }
            //}

            Isloading = false;

        }

        private void btnReadKontorKhanAndUpdate_Click(object sender, EventArgs e)
        {

            //kontorkhanBindingSource.EndEdit();

            //for (int i = 0; i < mainAbDataset.kontor_khan.Rows.Count; i++)
            //{
            //    if (Convert.ToInt32(mainAbDataset.kontor_khan.Rows[i]["shomareh_kontor"]) < Convert.ToInt32(LastKontorValues[mainAbDataset.kontor_khan.Rows[i]["gharardad"].ToString()]))
            //    {
            //        Payam.Show("شماره کنتور مشترک" + mainAbDataset.kontor_khan.Rows[i]["gharardad"].ToString() + " می بایست برابر یا مساوی شماره کنتور دوره قبل :" + LastKontorValues[mainAbDataset.kontor_khan.Rows[i]["gharardad"].ToString()].ToString() + " باشد. لطفا اصلاح نمایید");
            //        return;
            //    }
            //}




            Payam.Show("کلیه مقادیر وارد شده صحیح بوده و هیچ اشکالی مشاهده نمی شود");
            this.Close();
        }

        private void labelX1_Click(object sender, EventArgs e)
        {

        }

        private void btn_find_by_gharardad_Click(object sender, EventArgs e)
        {
            txtlast.Text = "";

            try
            {
                if (txtFindByGhararda.Text.Trim() == "")
                {
                    kontorkhanBindingSource.RemoveFilter();
                }
                else
                {
                    int gharardad = Convert.ToInt32(txtFindByGhararda.Text);
                    kontorkhanBindingSource.Filter = "gharardad=" + gharardad;
                    try
                    {
                        grp_last.Text = "اطلاعات مربوط به قرارداد  " + gharardad.ToString() + ":";
                        txtlast.Text = Kontor_endValues[gharardad].ToString();
                        txt_last_date_gheraat.SelectedDateTime =Convert.ToDateTime(kontor_GheraatGhabli_tarikh[gharardad]);

                    }
                    catch (Exception)
                    {
                        txtlast.Text = "";
                        txt_last_date_gheraat.SelectedDateTime = null;

                    }

                }

            }
            catch
            {
                Payam.Show("شماره قرارداد نا معتبر می باشد");
                txtFindByGhararda.Clear();
                txtFindByGhararda.Focus();
            }

        }


        private void btn_create_new_doreh_Click(object sender, EventArgs e)
        {
            kontorkhanBindingSource.RemoveFilter();
            txtlast.Text = "";
            lbllast.Text = "";

        }

        private void txtFindByGhararda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gharardad.PerformClick();
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            CrystalReportViewer repVUer = new CrystalReportViewer();
            CrysReports.CheckGheraat rpt = new CrysReports.CheckGheraat();

            Classes.clsAb.ChapPishSodoor(repVUer, rpt);
        }

        private void FrmCheckGheraat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }



        private void dgv_doreh_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex == -1)
            //    txtlast.Text = "";
            //else
            //    txtlast.Text = Kontor_endValues[Convert.ToInt32(dgv_doreh.Rows[e.RowIndex].Cells[0].Value)].ToString();


        }


        Dictionary<string, string> MoshtarekinTopKontorRagham()
        {
            DataTable dt = Classes.ClsMain.GetDataTable("select gharardad,kontor_ragham from moshtarekin");
            Dictionary<string, string> info = new Dictionary<string, string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                info[dt.Rows[i][0].ToString()] = dt.Rows[i][1].ToString();
            }
            return info;
        }


        private void buttonX3_Click(object sender, EventArgs e)
        {
            bool AllthingRight = true;
            Dictionary<string, string> info = MoshtarekinTopKontorRagham();
            int max = 0;
            for (int i = 0; i < dgv_doreh.Rows.Count; i++)
            {
                try
                {
                    max = Convert.ToInt32(info[dgv_doreh.Rows[i].Cells[0].Value.ToString()]);
                }
                catch (Exception)
                {
                    max = 0;

                }
                if (Convert.ToInt32(dgv_doreh.Rows[i].Cells[1].Value.ToString()) > max )
                {
                    Payam.Show("حداکثر مقدار شماره کنتور برای مشترک:" + dgv_doreh.Rows[i].Cells[0].Value.ToString() + " برابر است با: " + info[dgv_doreh.Rows[i].Cells[0].Value.ToString()]);
                    AllthingRight = false;
                    break;
                }
            }

            if (AllthingRight)
            {
                kontorkhanBindingSource.EndEdit();
                kontor_khanTableAdapter.Update(mainAbDataset.kontor_khan);

                Payam.Show("تغییرات با موفقیت در سیستم ذخیره شدند");

            }
            if (MoshtarekGharardad != "")
                this.Close();
        }

        private void dgv_doreh_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Payam.Show("لطفا مقدار صحیح برای شماره کنتور وارد نمایید");
        }

        private void buttonX3_Click_1(object sender, EventArgs e)
        {
            if (Classes.ClsMain.isClosed("FrmChooseMoshtarekForAbGhabz"))
            {
                Classes.clsMoshtarekin.Report_Access(Classes.clsMoshtarekin.UserInfo.username, 101);
                Classes.ClsMain.openForm("FrmChooseMoshtarekForAbGhabz");
                new Ab.FrmChooseMoshtarekForAbGhabz().Show();

            }
        }

        //private void txtAddnewGharardad_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //        btnAdddNew.PerformClick();

        //}

        //private void btnAdddNew_Click(object sender, EventArgs e)
        //{
        //    if (txtAddnewGharardad.Text.Trim() == "")
        //        txtAddnewGharardad.Focus();
        //    else
        //    {
        //        try
        //        {
        //            int gharardad = Convert.ToInt32(txtAddnewGharardad.Text);
        //            if (this.mainAbDataset.kontor_khan.Where(p => p.gharardad == gharardad).Count() == 0)
        //            {

        //                int lastAb = Classes.clsDoreh.Get_last_Ab_dcode();
        //                new Ab._ّFrmNewTakGhabz(lastAb.ToString(), txtAddnewGharardad.Text).ShowDialog();
        //                this.kontor_khanTableAdapter.Fill(this.mainAbDataset.kontor_khan);

        //            }
        //            else
        //            {
        //                Payam.Show("اطلاعات این مشترک در لیست زیر وجود دارد !");
        //                txtFindByGhararda.Text = txtAddnewGharardad.Text;
        //                btn_find_by_gharardad.PerformClick();

        //            }

        //        }
        //        catch (Exception)
        //        {
        //            txtAddnewGharardad.Text = "";
        //            Payam.Show("لطفا شماره قرارداد را به صورت صحیح وارد نمایید");

        //        }
        //    }
        //}

        private void groupPanel3_Click(object sender, EventArgs e)
        {

        }

        private void buttonX3_Click_2(object sender, EventArgs e)
        {
            txtlast.Text = "";
            if (dgvShouldAdd.SelectedRows.Count == 0)
                Payam.Show("لطفا یک مشترک را از لیست انتخاب نمایید");
            else
            {
                try
                {
                    int gharardad = Convert.ToInt32(dgvShouldAdd.SelectedRows[0].Cells[0].Value.ToString());
                    if (this.mainAbDataset.kontor_khan.Where(p => p.gharardad == gharardad).Count() == 0)
                    {

                        int lastAb = Classes.clsDoreh.Get_last_Ab_dcode();
                        Classes.ClsMain.NewTakGabzStat = true;
                        new Ab._ّFrmNewTakGhabz(lastAb.ToString(), gharardad.ToString(), true).ShowDialog();
                        if (Classes.ClsMain.NewTakGabzStat)
                        {
                            this.kontor_khanTableAdapter.Fill(this.mainAbDataset.kontor_khan);
                            dgvShouldAdd.Rows.RemoveAt(dgvShouldAdd.SelectedRows[0].Index);
                        }
                    }
                    else
                    {
                        Payam.Show("اطلاعات این مشترک در لیست زیر وجود دارد !");

                    }

                }
                catch (Exception)
                {
                    Payam.Show("لطفا شماره قرارداد را به صورت صحیح وارد نمایید");

                }
            }
        }

        private void dgv_doreh_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void dgv_doreh_SelectionChanged(object sender, EventArgs e)
        {
            //if (dgv_doreh.SelectedRows.Count != 0 && Isloading==false)
            //    txtlast.Text = Kontor_endValues[Convert.ToInt32(dgv_doreh.SelectedRows[0].Cells[0].Value)].ToString();


        }

        private void dgv_doreh_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (Isloading == false)
            {
                try
                {
                    txtlast.Text = Kontor_endValues[Convert.ToInt32(dgv_doreh.Rows[e.RowIndex].Cells[0].Value)].ToString();
                    txt_last_date_gheraat.SelectedDateTime = Convert.ToDateTime(kontor_GheraatGhabli_tarikh[Convert.ToInt32(dgv_doreh.Rows[e.RowIndex].Cells[0].Value)]);

                }
                catch (Exception)
                {

                    txt_last_date_gheraat.SelectedDateTime = null;
                    txtlast.Text = "";
                }

            }
        }
    }
}