using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using CrystalDecisions;

namespace CWMS
{
    public partial class FrmShowReport : MyMetroForm
    {
        //public FrmShowReport(CrysReports.Sharj_doreh cr)
        //{
        //    InitializeComponent();
        //    crystalReportViewer1.ReportSource = cr;
        //}
        
        public FrmShowReport(CrystalDecisions.CrystalReports.Engine.ReportClass cr)
        {
            InitializeComponent();
            crystalReportViewer1.ReportSource = cr;
        }
        
        
        private void FrmShowReport_Load(object sender, EventArgs e)
        {
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ExportReport();

        }
    }
}