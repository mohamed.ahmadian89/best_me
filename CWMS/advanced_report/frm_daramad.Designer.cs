﻿namespace CWMS.advanced_report
{
    partial class frm_daramad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txt_to = new System.Windows.Forms.TextBox();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txt_from = new System.Windows.Forms.TextBox();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.btn_search = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dgv_sharj_result = new System.Windows.Forms.DataGridView();
            this.دورهDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمبالغDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعمالیاتDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعکلDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.daramadghabzbypardakhtBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.advanced_report_dataset = new CWMS.advanced_report.advanced_report_dataset();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.daramadghabznopardakhtBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.daramadallghabzBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupPanel5 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.دورهDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.مجموعپرداختیهاDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghabzpardakhtallBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.daramad_ghabz_by_pardakhtTableAdapter = new CWMS.advanced_report.advanced_report_datasetTableAdapters.daramad_ghabz_by_pardakhtTableAdapter();
            this.daramad_ghabz_no_pardakhtTableAdapter = new CWMS.advanced_report.advanced_report_datasetTableAdapters.daramad_ghabz_no_pardakhtTableAdapter();
            this.daramad_all_ghabzTableAdapter = new CWMS.advanced_report.advanced_report_datasetTableAdapters.daramad_all_ghabzTableAdapter();
            this.ghabz_pardakht_allTableAdapter = new CWMS.advanced_report.advanced_report_datasetTableAdapters.ghabz_pardakht_allTableAdapter();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.دوره = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rd_ab = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadghabzbypardakhtBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advanced_report_dataset)).BeginInit();
            this.groupPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadghabznopardakhtBindingSource)).BeginInit();
            this.groupPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadallghabzBindingSource)).BeginInit();
            this.groupPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghabzpardakhtallBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.radioButton1);
            this.groupPanel1.Controls.Add(this.rd_ab);
            this.groupPanel1.Controls.Add(this.txt_to);
            this.groupPanel1.Controls.Add(this.labelX5);
            this.groupPanel1.Controls.Add(this.txt_from);
            this.groupPanel1.Controls.Add(this.labelX7);
            this.groupPanel1.Controls.Add(this.btn_search);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(10, 3);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1097, 69);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 1;
            this.groupPanel1.Text = "انتخاب دوره های مربوطه";
            // 
            // txt_to
            // 
            this.txt_to.BackColor = System.Drawing.Color.White;
            this.txt_to.ForeColor = System.Drawing.Color.Black;
            this.txt_to.Location = new System.Drawing.Point(631, 8);
            this.txt_to.Name = "txt_to";
            this.txt_to.Size = new System.Drawing.Size(78, 26);
            this.txt_to.TabIndex = 92;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(704, 11);
            this.labelX5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(47, 21);
            this.labelX5.TabIndex = 91;
            this.labelX5.Text = "تا دوره :";
            // 
            // txt_from
            // 
            this.txt_from.BackColor = System.Drawing.Color.White;
            this.txt_from.ForeColor = System.Drawing.Color.Black;
            this.txt_from.Location = new System.Drawing.Point(757, 8);
            this.txt_from.Name = "txt_from";
            this.txt_from.Size = new System.Drawing.Size(78, 26);
            this.txt_from.TabIndex = 90;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(820, 11);
            this.labelX7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(225, 21);
            this.labelX7.TabIndex = 89;
            this.labelX7.Text = "مشاهده اطلاعات مربوط به قبوض از دوره :";
            // 
            // btn_search
            // 
            this.btn_search.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_search.Location = new System.Drawing.Point(226, 8);
            this.btn_search.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btn_search.Name = "btn_search";
            this.btn_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn_search.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(15, 2, 2, 15);
            this.btn_search.Size = new System.Drawing.Size(179, 29);
            this.btn_search.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_search.Symbol = "";
            this.btn_search.SymbolColor = System.Drawing.Color.Green;
            this.btn_search.SymbolSize = 12F;
            this.btn_search.TabIndex = 2;
            this.btn_search.Text = "مشاهده درامد مالیاتی";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.dgv_sharj_result);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel2.Location = new System.Drawing.Point(576, 77);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(505, 282);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 2;
            this.groupPanel2.Text = "اطلاعات قبوض دارای پرداخت ";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(437, 44);
            this.labelX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(57, 26);
            this.labelX1.TabIndex = 90;
            this.labelX1.Text = "مجموع";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dgv_sharj_result
            // 
            this.dgv_sharj_result.AutoGenerateColumns = false;
            this.dgv_sharj_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sharj_result.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_sharj_result.BackgroundColor = System.Drawing.Color.White;
            this.dgv_sharj_result.ColumnHeadersHeight = 30;
            this.dgv_sharj_result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.دورهDataGridViewTextBoxColumn,
            this.مجموعمبالغDataGridViewTextBoxColumn,
            this.مجموعمالیاتDataGridViewTextBoxColumn,
            this.مجموعکلDataGridViewTextBoxColumn});
            this.dgv_sharj_result.DataSource = this.daramadghabzbypardakhtBindingSource;
            this.dgv_sharj_result.Location = new System.Drawing.Point(5, 13);
            this.dgv_sharj_result.Name = "dgv_sharj_result";
            this.dgv_sharj_result.RowHeadersVisible = false;
            this.dgv_sharj_result.Size = new System.Drawing.Size(491, 235);
            this.dgv_sharj_result.TabIndex = 100;
            // 
            // دورهDataGridViewTextBoxColumn
            // 
            this.دورهDataGridViewTextBoxColumn.DataPropertyName = "دوره";
            this.دورهDataGridViewTextBoxColumn.FillWeight = 40F;
            this.دورهDataGridViewTextBoxColumn.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn.Name = "دورهDataGridViewTextBoxColumn";
            // 
            // مجموعمبالغDataGridViewTextBoxColumn
            // 
            this.مجموعمبالغDataGridViewTextBoxColumn.DataPropertyName = "مجموع مبالغ";
            dataGridViewCellStyle1.Format = "0,0";
            this.مجموعمبالغDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.مجموعمبالغDataGridViewTextBoxColumn.HeaderText = "مجموع مبالغ";
            this.مجموعمبالغDataGridViewTextBoxColumn.Name = "مجموعمبالغDataGridViewTextBoxColumn";
            this.مجموعمبالغDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مجموعمالیاتDataGridViewTextBoxColumn
            // 
            this.مجموعمالیاتDataGridViewTextBoxColumn.DataPropertyName = "مجموع مالیات";
            dataGridViewCellStyle2.Format = "0,0";
            this.مجموعمالیاتDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.مجموعمالیاتDataGridViewTextBoxColumn.HeaderText = "مجموع مالیات";
            this.مجموعمالیاتDataGridViewTextBoxColumn.Name = "مجموعمالیاتDataGridViewTextBoxColumn";
            this.مجموعمالیاتDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // مجموعکلDataGridViewTextBoxColumn
            // 
            this.مجموعکلDataGridViewTextBoxColumn.DataPropertyName = "مجموع کل";
            dataGridViewCellStyle3.Format = "0,0";
            this.مجموعکلDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.مجموعکلDataGridViewTextBoxColumn.HeaderText = "مجموع کل";
            this.مجموعکلDataGridViewTextBoxColumn.Name = "مجموعکلDataGridViewTextBoxColumn";
            this.مجموعکلDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // daramadghabzbypardakhtBindingSource
            // 
            this.daramadghabzbypardakhtBindingSource.DataMember = "daramad_ghabz_by_pardakht";
            this.daramadghabzbypardakhtBindingSource.DataSource = this.advanced_report_dataset;
            // 
            // advanced_report_dataset
            // 
            this.advanced_report_dataset.DataSetName = "advanced_report_dataset";
            this.advanced_report_dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.White;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.labelX3);
            this.groupPanel3.Controls.Add(this.dataGridView1);
            this.groupPanel3.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel3.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel3.Location = new System.Drawing.Point(50, 77);
            this.groupPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel3.Size = new System.Drawing.Size(505, 282);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 3;
            this.groupPanel3.Text = "اطلاعات قبوض بدون پرداختی ";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(435, 44);
            this.labelX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(57, 26);
            this.labelX3.TabIndex = 102;
            this.labelX3.Text = "مجموع";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView1.DataSource = this.daramadghabznopardakhtBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(491, 235);
            this.dataGridView1.TabIndex = 101;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "دوره";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "مجموع مبالغ";
            dataGridViewCellStyle4.Format = "0,0";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "مجموع مبالغ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "مجموع مالیات";
            dataGridViewCellStyle5.Format = "0,0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn3.HeaderText = "مجموع مالیات";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "مجموع کل";
            dataGridViewCellStyle6.Format = "0,0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn4.HeaderText = "مجموع کل";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // daramadghabznopardakhtBindingSource
            // 
            this.daramadghabznopardakhtBindingSource.DataMember = "daramad_ghabz_no_pardakht";
            this.daramadghabznopardakhtBindingSource.DataSource = this.advanced_report_dataset;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.White;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.labelX2);
            this.groupPanel4.Controls.Add(this.dataGridView2);
            this.groupPanel4.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel4.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel4.Location = new System.Drawing.Point(576, 366);
            this.groupPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel4.Size = new System.Drawing.Size(505, 277);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel4.Style.BackColorGradientAngle = 90;
            this.groupPanel4.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel4.TabIndex = 4;
            this.groupPanel4.Text = "اطلاعات کلیه قبوض";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(435, 39);
            this.labelX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(57, 26);
            this.labelX2.TabIndex = 101;
            this.labelX2.Text = "مجموع";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeight = 30;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dataGridView2.DataSource = this.daramadallghabzBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(3, 8);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(491, 235);
            this.dataGridView2.TabIndex = 100;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "دوره";
            this.dataGridViewTextBoxColumn5.FillWeight = 40F;
            this.dataGridViewTextBoxColumn5.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "مجموع مبالغ";
            dataGridViewCellStyle7.Format = "0,0";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn6.HeaderText = "مجموع مبالغ";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "مجموع مالیات";
            dataGridViewCellStyle8.Format = "0,0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn7.HeaderText = "مجموع مالیات";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "مجموع کل";
            dataGridViewCellStyle9.Format = "0,0";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn8.HeaderText = "مجموع کل";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // daramadallghabzBindingSource
            // 
            this.daramadallghabzBindingSource.DataMember = "daramad_all_ghabz";
            this.daramadallghabzBindingSource.DataSource = this.advanced_report_dataset;
            // 
            // groupPanel5
            // 
            this.groupPanel5.BackColor = System.Drawing.Color.White;
            this.groupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel5.Controls.Add(this.labelX4);
            this.groupPanel5.Controls.Add(this.dataGridView3);
            this.groupPanel5.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel5.Font = new System.Drawing.Font("B Yekan", 10F);
            this.groupPanel5.Location = new System.Drawing.Point(88, 367);
            this.groupPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupPanel5.Name = "groupPanel5";
            this.groupPanel5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel5.Size = new System.Drawing.Size(428, 277);
            // 
            // 
            // 
            this.groupPanel5.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel5.Style.BackColorGradientAngle = 90;
            this.groupPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderBottomWidth = 1;
            this.groupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderLeftWidth = 1;
            this.groupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderRightWidth = 1;
            this.groupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel5.Style.BorderTopWidth = 1;
            this.groupPanel5.Style.CornerDiameter = 4;
            this.groupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel5.TabIndex = 5;
            this.groupPanel5.Text = "اطلاعات پرداختی های مربوط به قبوض به تفکیک دوره";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(317, 38);
            this.labelX4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(96, 26);
            this.labelX4.TabIndex = 101;
            this.labelX4.Text = "مجموع";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.ColumnHeadersHeight = 30;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.دورهDataGridViewTextBoxColumn1,
            this.مجموعپرداختیهاDataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.ghabzpardakhtallBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(3, 8);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.Size = new System.Drawing.Size(410, 235);
            this.dataGridView3.TabIndex = 100;
            // 
            // دورهDataGridViewTextBoxColumn1
            // 
            this.دورهDataGridViewTextBoxColumn1.DataPropertyName = "دوره";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.دورهDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.دورهDataGridViewTextBoxColumn1.FillWeight = 30F;
            this.دورهDataGridViewTextBoxColumn1.HeaderText = "دوره";
            this.دورهDataGridViewTextBoxColumn1.Name = "دورهDataGridViewTextBoxColumn1";
            this.دورهDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // مجموعپرداختیهاDataGridViewTextBoxColumn
            // 
            this.مجموعپرداختیهاDataGridViewTextBoxColumn.DataPropertyName = "مجموع پرداختی ها";
            dataGridViewCellStyle11.Format = "0,0";
            this.مجموعپرداختیهاDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this.مجموعپرداختیهاDataGridViewTextBoxColumn.HeaderText = "مجموع پرداختی ها";
            this.مجموعپرداختیهاDataGridViewTextBoxColumn.Name = "مجموعپرداختیهاDataGridViewTextBoxColumn";
            this.مجموعپرداختیهاDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ghabzpardakhtallBindingSource
            // 
            this.ghabzpardakhtallBindingSource.DataMember = "ghabz_pardakht_all";
            this.ghabzpardakhtallBindingSource.DataSource = this.advanced_report_dataset;
            // 
            // daramad_ghabz_by_pardakhtTableAdapter
            // 
            this.daramad_ghabz_by_pardakhtTableAdapter.ClearBeforeFill = true;
            // 
            // daramad_ghabz_no_pardakhtTableAdapter
            // 
            this.daramad_ghabz_no_pardakhtTableAdapter.ClearBeforeFill = true;
            // 
            // daramad_all_ghabzTableAdapter
            // 
            this.daramad_all_ghabzTableAdapter.ClearBeforeFill = true;
            // 
            // ghabz_pardakht_allTableAdapter
            // 
            this.ghabz_pardakht_allTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "دوره";
            this.dataGridViewTextBoxColumn9.FillWeight = 40F;
            this.dataGridViewTextBoxColumn9.HeaderText = "دوره";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 488;
            // 
            // دوره
            // 
            this.دوره.DataPropertyName = "دوره";
            this.دوره.FillWeight = 50F;
            this.دوره.HeaderText = "دوره";
            this.دوره.Name = "دوره";
            // 
            // rd_ab
            // 
            this.rd_ab.AutoSize = true;
            this.rd_ab.BackColor = System.Drawing.Color.Transparent;
            this.rd_ab.Checked = true;
            this.rd_ab.Location = new System.Drawing.Point(523, 11);
            this.rd_ab.Name = "rd_ab";
            this.rd_ab.Size = new System.Drawing.Size(69, 22);
            this.rd_ab.TabIndex = 93;
            this.rd_ab.Text = "قبوض آب";
            this.rd_ab.UseVisualStyleBackColor = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.Location = new System.Drawing.Point(434, 10);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 22);
            this.radioButton1.TabIndex = 94;
            this.radioButton1.Text = "قبوض شارژ ";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // frm_daramad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 643);
            this.Controls.Add(this.groupPanel5);
            this.Controls.Add(this.groupPanel4);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_daramad";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "درآمدهای کلی ";
            this.Load += new System.EventHandler(this.frm_daramad_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sharj_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadghabzbypardakhtBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advanced_report_dataset)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadghabznopardakhtBindingSource)).EndInit();
            this.groupPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daramadallghabzBindingSource)).EndInit();
            this.groupPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghabzpardakhtallBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX btn_search;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.DataGridView dgv_sharj_result;
        private advanced_report_dataset advanced_report_dataset;
        private System.Windows.Forms.BindingSource daramadghabzbypardakhtBindingSource;
        private advanced_report_datasetTableAdapters.daramad_ghabz_by_pardakhtTableAdapter daramad_ghabz_by_pardakhtTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource daramadghabznopardakhtBindingSource;
        private advanced_report_datasetTableAdapters.daramad_ghabz_no_pardakhtTableAdapter daramad_ghabz_no_pardakhtTableAdapter;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource daramadallghabzBindingSource;
        private advanced_report_datasetTableAdapters.daramad_all_ghabzTableAdapter daramad_all_ghabzTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمبالغDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعمالیاتDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعکلDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.BindingSource ghabzpardakhtallBindingSource;
        private advanced_report_datasetTableAdapters.ghabz_pardakht_allTableAdapter ghabz_pardakht_allTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn دوره;
        private System.Windows.Forms.DataGridViewTextBoxColumn دورهDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn مجموعپرداختیهاDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox txt_to;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.TextBox txt_from;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rd_ab;
    }
}