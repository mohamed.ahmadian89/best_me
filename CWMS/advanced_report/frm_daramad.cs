﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CWMS.advanced_report
{
    public partial class frm_daramad : MyMetroForm
    {
        public frm_daramad()
        {
            InitializeComponent();
        }

        private void frm_daramad_Load(object sender, EventArgs e)
        {
            txt_from.Focus();

        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            int start = 0, finish = 0;
            try
            {
                start = Convert.ToInt32(txt_from.Text);
                finish = Convert.ToInt32(txt_to.Text);
            }
            catch (Exception)
            {
                Payam.Show("لطفا شماره دوره ها را به درستی وارد نمایید");
                txt_from.Focus();
                txt_from.SelectAll();
                return;
            }
            bool is_sharj = true;
            if (rd_ab.Checked)
                is_sharj = false;

            btn_search.Enabled = false;

            // TODO: This line of code loads data into the 'advanced_report_dataset.daramad_all_ghabz' table. You can move, or remove it, as needed.
            this.daramad_all_ghabzTableAdapter.Fill(this.advanced_report_dataset.daramad_all_ghabz, is_sharj, start,finish);
            // TODO: This line of code loads data into the 'advanced_report_dataset.daramad_ghabz_no_pardakht' table. You can move, or remove it, as needed.
            this.daramad_ghabz_no_pardakhtTableAdapter.Fill(this.advanced_report_dataset.daramad_ghabz_no_pardakht, is_sharj, start, finish);
            // TODO: This line of code loads data into the 'advanced_report_dataset.daramad_ghabz_by_pardakht' table. You can move, or remove it, as needed.
            this.daramad_ghabz_by_pardakhtTableAdapter.Fill(this.advanced_report_dataset.daramad_ghabz_by_pardakht, is_sharj, start, finish);
            // TODO: This line of code loads data into the 'advanced_report_dataset.ghabz_pardakht_all' table. You can move, or remove it, as needed.
            this.ghabz_pardakht_allTableAdapter.Fill(this.advanced_report_dataset.ghabz_pardakht_all,!is_sharj, start, finish);
            btn_search.Enabled = true;
        }
    }
}
