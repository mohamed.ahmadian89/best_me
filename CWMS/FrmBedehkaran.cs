using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data;
namespace CWMS
{
    public partial class FrmBedehkaran : DevComponents.DotNetBar.Metro.MetroForm
    {

        string state = "1doreh";
        public FrmBedehkaran()
        {
            InitializeComponent();
            cmbDoreh.DataSource = Classes.ClsMain.GetDataTable("select * from sharj_doreh order by dcode desc");
            cmbDoreh.DisplayMember = "dcode";
            cmbDoreh.ValueMember = "dcode";
        }

        private void FrmBedehkaran_Load(object sender, EventArgs e)
        {

        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            bedehkaranWithBedehTableAdapter.Fill(cWMSDataSet.bedehkaranWithBedeh,Convert.ToInt32(cmbDoreh.Text));
            dataGridViewX1.DataSource = cWMSDataSet.bedehkaranWithBedeh;
            state = "1doreh";
            
        }

        private void txt_find_ghabz_byCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_find_by_gcode.PerformClick();
        }

        private void btn_find_by_gcode_Click(object sender, EventArgs e)
        {
            if (txt_find_ghabz_byCode.Text.Trim() != "")
            {
                if (state == "1doreh")
                {
                    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedeh);
                    dv.RowFilter = "gharardad=" + txt_find_ghabz_byCode.Text;
                    dataGridViewX1.DataSource = dv.ToTable();
                }
                else
                {
                    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedehAll);
                    dv.RowFilter = "gharardad=" + txt_find_ghabz_byCode.Text;
                    dataGridViewX1.DataSource = dv.ToTable();
                }
            }
            txt_find_ghabz_byCode.SelectAll();
            txt_find_ghabz_byCode.Focus();
        }

        private void btn_all_Click(object sender, EventArgs e)
        {
            CWMSDataSetTableAdapters.bedehkaranWithBedehAllTableAdapter ta = new CWMSDataSetTableAdapters.bedehkaranWithBedehAllTableAdapter();
            ta.Fill(cWMSDataSet.bedehkaranWithBedehAll);
            dataGridViewX1.DataSource = cWMSDataSet.bedehkaranWithBedehAll;
            state = "alldoreh";
        }

        private void dataGridViewX1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                FrmBedehkarDetails frm = new FrmBedehkarDetails(dataGridViewX1.Rows[e.RowIndex].Cells[0].Value.ToString(), dataGridViewX1.Rows[e.RowIndex].Cells[1].Value.ToString());
                frm.ShowDialog();
            }
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            
            dataGridViewX1.Sort(dataGridViewX1.Columns[3],ListSortDirection.Descending);
            //if (state == "1doreh")
            //{
            //    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedeh);
            //    dv.RowFilter = "order by mablaghkol desc";
            //    dataGridViewX1.DataSource = dv.ToTable();
            //}
            //else
            //{
            //    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedehAll);
            //    dv.RowFilter = "order by mablaghkol desc";
            //    dataGridViewX1.DataSource = dv.ToTable();
            //}

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            dataGridViewX1.Sort(dataGridViewX1.Columns[3], ListSortDirection.Ascending);

            //if (state == "1doreh")
            //{
            //    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedeh);
            //    dv.RowFilter = "order by mablaghkol asc";
            //    dataGridViewX1.DataSource = dv.ToTable();
            //}
            //else
            //{
            //    DataView dv = new System.Data.DataView(cWMSDataSet.bedehkaranWithBedehAll);
            //    dv.RowFilter = "order by mablaghkol asc";
            //    dataGridViewX1.DataSource = dv.ToTable();
            //}
        }

        private void btn_bedehimax_Click(object sender, EventArgs e)
        {
            dataGridViewX1.Sort(dataGridViewX1.Columns[3], ListSortDirection.Descending);

        }

        private void bedehi_min_Click(object sender, EventArgs e)
        {
            dataGridViewX1.Sort(dataGridViewX1.Columns[3], ListSortDirection.Ascending);

        }

        private void btn_max_tedad_Click(object sender, EventArgs e)
        {
            dataGridViewX1.Sort(dataGridViewX1.Columns[4], ListSortDirection.Descending);

        }

        private void btn_min_tedad_Click(object sender, EventArgs e)
        {
            dataGridViewX1.Sort(dataGridViewX1.Columns[4], ListSortDirection.Ascending);

        }
    }
}