﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using DevComponents.DotNetBar.Controls;
using System.Globalization;

namespace CWMS
{
    class FloatTextBox : TextBoxX
    {
        protected override void OnValidating(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !IsValidFloat(this.Text);
            base.OnValidating(e);
        }

        private bool IsValidFloat(string currentText)
        {
            decimal temp;

                this.Text = currentText.Replace("/", ".");

            if (Decimal.TryParse(currentText.Replace("/", "."), NumberStyles.Float, CultureInfo.InvariantCulture, out temp))
                return true;
            return false;
        }

        public override string Text
        {
            get
            {
                if (!base.Text.Replace("/", ".").Contains('.'))
                {
                    return base.Text + ".0";
                }
                return base.Text.Replace("/", ".");
            }
            set
            {
                base.Text = value;
            }
        }
    }
}
