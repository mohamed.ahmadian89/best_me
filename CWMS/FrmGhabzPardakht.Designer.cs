﻿namespace CWMS.Sharj
{
    partial class FrmGhabzPardakht
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBestankarFromLAstDoreh = new CWMS.MoneyTextBox();
            this.txtMablaghKol = new CWMS.MoneyTextBox();
            this.txtGharardadMain = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGhabzCodeMain = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtDorehMain = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTarikhSoddor = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGharardad = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.ghabzpardakhtBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainAbDataset1 = new CWMS.Ab.MainAbDataset();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_ghabz_code = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMablaghHoroof = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtdoreh = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.txtBestankar = new CWMS.MoneyTextBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.lblMablaghHorrof = new DevComponents.DotNetBar.LabelX();
            this.txtPardakht = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX3 = new DevComponents.DotNetBar.ButtonX();
            this.txtBaghimadeh = new CWMS.MoneyTextBox();
            this.txtPardakhti = new CWMS.MoneyTextBox();
            this.ch_vaziat = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.txtSarresid = new FarsiLibrary.Win.Controls.FADatePicker();
            this.LblSarresid = new DevComponents.DotNetBar.LabelX();
            this.txtbank = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtfish = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.buttonX2 = new DevComponents.DotNetBar.ButtonX();
            this.btn_add = new DevComponents.DotNetBar.ButtonX();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.cmbahveh = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.txtTozihat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.lblNehveh = new DevComponents.DotNetBar.LabelX();
            this.TxtTarikhVosool = new FarsiLibrary.Win.Controls.FADatePicker();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.dgvPardakht = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhsabtDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.vaziatDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bankDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablaghDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tarikhvosoolDataGridViewTextBoxColumn = new FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn();
            this.nahvepardakhtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTarikhSAbt = new FarsiLibrary.Win.Controls.FADatePicker();
            this.Is_ab_checkBox = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.ghabz_pardakhtTableAdapter = new CWMS.MainDataSestTableAdapters.ghabz_pardakhtTableAdapter();
            this.ghabz_pardakhtTableAdapter1 = new CWMS.Ab.MainAbDatasetTableAdapters.ghabz_pardakhtTableAdapter();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ghabzpardakhtBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset1)).BeginInit();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPardakht)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.White;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.label4);
            this.groupPanel1.Controls.Add(this.txtBestankarFromLAstDoreh);
            this.groupPanel1.Controls.Add(this.txtMablaghKol);
            this.groupPanel1.Controls.Add(this.txtGharardadMain);
            this.groupPanel1.Controls.Add(this.txtGhabzCodeMain);
            this.groupPanel1.Controls.Add(this.txtDorehMain);
            this.groupPanel1.Controls.Add(this.txtTarikhSoddor);
            this.groupPanel1.Controls.Add(this.txtGharardad);
            this.groupPanel1.Controls.Add(this.label3);
            this.groupPanel1.Controls.Add(this.txt_ghabz_code);
            this.groupPanel1.Controls.Add(this.label12);
            this.groupPanel1.Controls.Add(this.label16);
            this.groupPanel1.Controls.Add(this.txtMablaghHoroof);
            this.groupPanel1.Controls.Add(this.txtdoreh);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.Controls.Add(this.label2);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(43, 15);
            this.groupPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel1.Size = new System.Drawing.Size(1199, 126);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 10;
            this.groupPanel1.Text = "اطلاعات قبض";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(179, 59);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 17);
            this.label4.TabIndex = 118;
            this.label4.Text = "بستانکاری از قبض دوره قبل :";
            // 
            // txtBestankarFromLAstDoreh
            // 
            this.txtBestankarFromLAstDoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBestankarFromLAstDoreh.Border.Class = "TextBoxBorder";
            this.txtBestankarFromLAstDoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBestankarFromLAstDoreh.DisabledBackColor = System.Drawing.Color.White;
            this.txtBestankarFromLAstDoreh.ForeColor = System.Drawing.Color.Black;
            this.txtBestankarFromLAstDoreh.Location = new System.Drawing.Point(14, 53);
            this.txtBestankarFromLAstDoreh.Margin = new System.Windows.Forms.Padding(4);
            this.txtBestankarFromLAstDoreh.Name = "txtBestankarFromLAstDoreh";
            this.txtBestankarFromLAstDoreh.PreventEnterBeep = true;
            this.txtBestankarFromLAstDoreh.ReadOnly = true;
            this.txtBestankarFromLAstDoreh.Size = new System.Drawing.Size(157, 28);
            this.txtBestankarFromLAstDoreh.TabIndex = 117;
            // 
            // txtMablaghKol
            // 
            this.txtMablaghKol.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghKol.Border.Class = "TextBoxBorder";
            this.txtMablaghKol.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghKol.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghKol.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghKol.Location = new System.Drawing.Point(717, 53);
            this.txtMablaghKol.Margin = new System.Windows.Forms.Padding(4);
            this.txtMablaghKol.Name = "txtMablaghKol";
            this.txtMablaghKol.PreventEnterBeep = true;
            this.txtMablaghKol.ReadOnly = true;
            this.txtMablaghKol.Size = new System.Drawing.Size(157, 28);
            this.txtMablaghKol.TabIndex = 116;
            // 
            // txtGharardadMain
            // 
            this.txtGharardadMain.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGharardadMain.Border.Class = "TextBoxBorder";
            this.txtGharardadMain.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGharardadMain.DisabledBackColor = System.Drawing.Color.White;
            this.txtGharardadMain.ForeColor = System.Drawing.Color.Black;
            this.txtGharardadMain.Location = new System.Drawing.Point(975, 15);
            this.txtGharardadMain.Margin = new System.Windows.Forms.Padding(4);
            this.txtGharardadMain.Name = "txtGharardadMain";
            this.txtGharardadMain.ReadOnly = true;
            this.txtGharardadMain.Size = new System.Drawing.Size(103, 28);
            this.txtGharardadMain.TabIndex = 115;
            // 
            // txtGhabzCodeMain
            // 
            this.txtGhabzCodeMain.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGhabzCodeMain.Border.Class = "TextBoxBorder";
            this.txtGhabzCodeMain.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGhabzCodeMain.DisabledBackColor = System.Drawing.Color.White;
            this.txtGhabzCodeMain.ForeColor = System.Drawing.Color.Black;
            this.txtGhabzCodeMain.Location = new System.Drawing.Point(418, 15);
            this.txtGhabzCodeMain.Margin = new System.Windows.Forms.Padding(4);
            this.txtGhabzCodeMain.Name = "txtGhabzCodeMain";
            this.txtGhabzCodeMain.ReadOnly = true;
            this.txtGhabzCodeMain.Size = new System.Drawing.Size(103, 28);
            this.txtGhabzCodeMain.TabIndex = 114;
            // 
            // txtDorehMain
            // 
            this.txtDorehMain.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDorehMain.Border.Class = "TextBoxBorder";
            this.txtDorehMain.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDorehMain.DisabledBackColor = System.Drawing.Color.White;
            this.txtDorehMain.ForeColor = System.Drawing.Color.Black;
            this.txtDorehMain.Location = new System.Drawing.Point(220, 15);
            this.txtDorehMain.Margin = new System.Windows.Forms.Padding(4);
            this.txtDorehMain.Name = "txtDorehMain";
            this.txtDorehMain.ReadOnly = true;
            this.txtDorehMain.Size = new System.Drawing.Size(103, 28);
            this.txtDorehMain.TabIndex = 113;
            // 
            // txtTarikhSoddor
            // 
            this.txtTarikhSoddor.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTarikhSoddor.Border.Class = "TextBoxBorder";
            this.txtTarikhSoddor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTarikhSoddor.DisabledBackColor = System.Drawing.Color.White;
            this.txtTarikhSoddor.ForeColor = System.Drawing.Color.Black;
            this.txtTarikhSoddor.Location = new System.Drawing.Point(654, 15);
            this.txtTarikhSoddor.Margin = new System.Windows.Forms.Padding(4);
            this.txtTarikhSoddor.Name = "txtTarikhSoddor";
            this.txtTarikhSoddor.PreventEnterBeep = true;
            this.txtTarikhSoddor.ReadOnly = true;
            this.txtTarikhSoddor.Size = new System.Drawing.Size(157, 28);
            this.txtTarikhSoddor.TabIndex = 112;
            // 
            // txtGharardad
            // 
            this.txtGharardad.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtGharardad.Border.Class = "TextBoxBorder";
            this.txtGharardad.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGharardad.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "gharardad", true));
            this.txtGharardad.DisabledBackColor = System.Drawing.Color.White;
            this.txtGharardad.ForeColor = System.Drawing.Color.Black;
            this.txtGharardad.Location = new System.Drawing.Point(975, 15);
            this.txtGharardad.Margin = new System.Windows.Forms.Padding(4);
            this.txtGharardad.Name = "txtGharardad";
            this.txtGharardad.ReadOnly = true;
            this.txtGharardad.Size = new System.Drawing.Size(103, 28);
            this.txtGharardad.TabIndex = 111;
            // 
            // ghabzpardakhtBindingSource
            // 
            this.ghabzpardakhtBindingSource.DataMember = "ghabz_pardakht";
            this.ghabzpardakhtBindingSource.DataSource = this.mainAbDataset1;
            // 
            // mainAbDataset1
            // 
            this.mainAbDataset1.DataSetName = "MainAbDataset";
            this.mainAbDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(1096, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 110;
            this.label3.Text = "قرارداد :";
            // 
            // txt_ghabz_code
            // 
            this.txt_ghabz_code.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txt_ghabz_code.Border.Class = "TextBoxBorder";
            this.txt_ghabz_code.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txt_ghabz_code.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "gcode", true));
            this.txt_ghabz_code.DisabledBackColor = System.Drawing.Color.White;
            this.txt_ghabz_code.ForeColor = System.Drawing.Color.Black;
            this.txt_ghabz_code.Location = new System.Drawing.Point(418, 15);
            this.txt_ghabz_code.Margin = new System.Windows.Forms.Padding(4);
            this.txt_ghabz_code.Name = "txt_ghabz_code";
            this.txt_ghabz_code.ReadOnly = true;
            this.txt_ghabz_code.Size = new System.Drawing.Size(103, 28);
            this.txt_ghabz_code.TabIndex = 109;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(877, 59);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 17);
            this.label12.TabIndex = 106;
            this.label12.Text = "مبلغ کل :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(539, 20);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 17);
            this.label16.TabIndex = 108;
            this.label16.Text = "کد قبض:";
            // 
            // txtMablaghHoroof
            // 
            this.txtMablaghHoroof.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtMablaghHoroof.Border.Class = "TextBoxBorder";
            this.txtMablaghHoroof.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtMablaghHoroof.DisabledBackColor = System.Drawing.Color.White;
            this.txtMablaghHoroof.ForeColor = System.Drawing.Color.Black;
            this.txtMablaghHoroof.Location = new System.Drawing.Point(345, 53);
            this.txtMablaghHoroof.Margin = new System.Windows.Forms.Padding(4);
            this.txtMablaghHoroof.Name = "txtMablaghHoroof";
            this.txtMablaghHoroof.PreventEnterBeep = true;
            this.txtMablaghHoroof.ReadOnly = true;
            this.txtMablaghHoroof.Size = new System.Drawing.Size(364, 28);
            this.txtMablaghHoroof.TabIndex = 104;
            // 
            // txtdoreh
            // 
            this.txtdoreh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtdoreh.Border.Class = "TextBoxBorder";
            this.txtdoreh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtdoreh.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "dcode", true));
            this.txtdoreh.DisabledBackColor = System.Drawing.Color.White;
            this.txtdoreh.ForeColor = System.Drawing.Color.Black;
            this.txtdoreh.Location = new System.Drawing.Point(219, 15);
            this.txtdoreh.Margin = new System.Windows.Forms.Padding(4);
            this.txtdoreh.Name = "txtdoreh";
            this.txtdoreh.ReadOnly = true;
            this.txtdoreh.Size = new System.Drawing.Size(103, 28);
            this.txtdoreh.TabIndex = 103;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("B Yekan", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(345, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 101;
            this.label1.Text = "دوره  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(820, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 21);
            this.label2.TabIndex = 102;
            this.label2.Text = "تاریخ صدور قبض :";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.White;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.txtBestankar);
            this.groupPanel2.Controls.Add(this.labelX1);
            this.groupPanel2.Controls.Add(this.lblMablaghHorrof);
            this.groupPanel2.Controls.Add(this.txtPardakht);
            this.groupPanel2.Controls.Add(this.buttonX3);
            this.groupPanel2.Controls.Add(this.txtBaghimadeh);
            this.groupPanel2.Controls.Add(this.txtPardakhti);
            this.groupPanel2.Controls.Add(this.ch_vaziat);
            this.groupPanel2.Controls.Add(this.txtSarresid);
            this.groupPanel2.Controls.Add(this.LblSarresid);
            this.groupPanel2.Controls.Add(this.txtbank);
            this.groupPanel2.Controls.Add(this.txtfish);
            this.groupPanel2.Controls.Add(this.buttonX2);
            this.groupPanel2.Controls.Add(this.btn_add);
            this.groupPanel2.Controls.Add(this.btn_save);
            this.groupPanel2.Controls.Add(this.labelX11);
            this.groupPanel2.Controls.Add(this.labelX10);
            this.groupPanel2.Controls.Add(this.cmbahveh);
            this.groupPanel2.Controls.Add(this.labelX7);
            this.groupPanel2.Controls.Add(this.txtTozihat);
            this.groupPanel2.Controls.Add(this.labelX6);
            this.groupPanel2.Controls.Add(this.labelX5);
            this.groupPanel2.Controls.Add(this.lblNehveh);
            this.groupPanel2.Controls.Add(this.TxtTarikhVosool);
            this.groupPanel2.Controls.Add(this.labelX9);
            this.groupPanel2.Controls.Add(this.labelX8);
            this.groupPanel2.Controls.Add(this.dgvPardakht);
            this.groupPanel2.Controls.Add(this.txtTarikhSAbt);
            this.groupPanel2.Controls.Add(this.Is_ab_checkBox);
            this.groupPanel2.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel2.Location = new System.Drawing.Point(10, 147);
            this.groupPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupPanel2.Size = new System.Drawing.Size(1262, 534);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 0;
            this.groupPanel2.Text = "اطلاعات هزینه مربوطه ";
            // 
            // txtBestankar
            // 
            this.txtBestankar.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBestankar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBestankar.DisabledBackColor = System.Drawing.Color.White;
            this.txtBestankar.ForeColor = System.Drawing.Color.Black;
            this.txtBestankar.Location = new System.Drawing.Point(326, 463);
            this.txtBestankar.Margin = new System.Windows.Forms.Padding(4);
            this.txtBestankar.Name = "txtBestankar";
            this.txtBestankar.Size = new System.Drawing.Size(178, 22);
            this.txtBestankar.TabIndex = 83;
            this.txtBestankar.Text = "0";
            this.txtBestankar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(415, 462);
            this.labelX1.Margin = new System.Windows.Forms.Padding(4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(139, 28);
            this.labelX1.TabIndex = 82;
            this.labelX1.Text = "بستانکار :";
            // 
            // lblMablaghHorrof
            // 
            this.lblMablaghHorrof.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblMablaghHorrof.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMablaghHorrof.ForeColor = System.Drawing.Color.Black;
            this.lblMablaghHorrof.Location = new System.Drawing.Point(349, 83);
            this.lblMablaghHorrof.Margin = new System.Windows.Forms.Padding(4);
            this.lblMablaghHorrof.Name = "lblMablaghHorrof";
            this.lblMablaghHorrof.Size = new System.Drawing.Size(856, 26);
            this.lblMablaghHorrof.TabIndex = 81;
            // 
            // txtPardakht
            // 
            this.txtPardakht.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPardakht.Border.Class = "TextBoxBorder";
            this.txtPardakht.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPardakht.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "mablagh", true));
            this.txtPardakht.DisabledBackColor = System.Drawing.Color.White;
            this.txtPardakht.ForeColor = System.Drawing.Color.Black;
            this.txtPardakht.Location = new System.Drawing.Point(925, 51);
            this.txtPardakht.Margin = new System.Windows.Forms.Padding(4);
            this.txtPardakht.MaxLength = 0;
            this.txtPardakht.Name = "txtPardakht";
            this.txtPardakht.PreventEnterBeep = true;
            this.txtPardakht.Size = new System.Drawing.Size(186, 28);
            this.txtPardakht.TabIndex = 2;
            this.txtPardakht.Text = "0";
            this.txtPardakht.TextChanged += new System.EventHandler(this.txtPardakht_TextChanged);
            this.txtPardakht.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPardakht_KeyDown_1);
            // 
            // buttonX3
            // 
            this.buttonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX3.BackColor = System.Drawing.Color.Transparent;
            this.buttonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX3.Location = new System.Drawing.Point(47, 464);
            this.buttonX3.Margin = new System.Windows.Forms.Padding(4);
            this.buttonX3.Name = "buttonX3";
            this.buttonX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.buttonX3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX3.Size = new System.Drawing.Size(226, 28);
            this.buttonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX3.Symbol = "";
            this.buttonX3.SymbolColor = System.Drawing.Color.Teal;
            this.buttonX3.SymbolSize = 9F;
            this.buttonX3.TabIndex = 79;
            this.buttonX3.Text = "پرینت قبض به همراه پرداختی ها";
            this.buttonX3.Click += new System.EventHandler(this.buttonX3_Click);
            // 
            // txtBaghimadeh
            // 
            this.txtBaghimadeh.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtBaghimadeh.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtBaghimadeh.DisabledBackColor = System.Drawing.Color.White;
            this.txtBaghimadeh.ForeColor = System.Drawing.Color.Black;
            this.txtBaghimadeh.Location = new System.Drawing.Point(586, 463);
            this.txtBaghimadeh.Margin = new System.Windows.Forms.Padding(4);
            this.txtBaghimadeh.Name = "txtBaghimadeh";
            this.txtBaghimadeh.Size = new System.Drawing.Size(178, 22);
            this.txtBaghimadeh.TabIndex = 77;
            this.txtBaghimadeh.Text = "0";
            this.txtBaghimadeh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPardakhti
            // 
            this.txtPardakhti.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPardakhti.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPardakhti.DisabledBackColor = System.Drawing.Color.White;
            this.txtPardakhti.ForeColor = System.Drawing.Color.Black;
            this.txtPardakhti.Location = new System.Drawing.Point(852, 463);
            this.txtPardakhti.Margin = new System.Windows.Forms.Padding(4);
            this.txtPardakhti.Name = "txtPardakhti";
            this.txtPardakhti.Size = new System.Drawing.Size(224, 22);
            this.txtPardakhti.TabIndex = 76;
            this.txtPardakhti.Text = "0";
            this.txtPardakhti.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ch_vaziat
            // 
            this.ch_vaziat.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.ch_vaziat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ch_vaziat.Checked = true;
            this.ch_vaziat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ch_vaziat.CheckValue = "Y";
            this.ch_vaziat.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.ghabzpardakhtBindingSource, "vaziat", true));
            this.ch_vaziat.ForeColor = System.Drawing.Color.Black;
            this.ch_vaziat.Location = new System.Drawing.Point(872, 173);
            this.ch_vaziat.Margin = new System.Windows.Forms.Padding(4);
            this.ch_vaziat.Name = "ch_vaziat";
            this.ch_vaziat.Size = new System.Drawing.Size(239, 28);
            this.ch_vaziat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ch_vaziat.TabIndex = 74;
            this.ch_vaziat.Text = "این پرداخت مورد تایید می باشد";
            // 
            // txtSarresid
            // 
            this.txtSarresid.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.ghabzpardakhtBindingSource, "tarikh_sarresid", true));
            this.txtSarresid.Location = new System.Drawing.Point(168, 16);
            this.txtSarresid.Name = "txtSarresid";
            this.txtSarresid.Size = new System.Drawing.Size(186, 25);
            this.txtSarresid.TabIndex = 71;
            this.txtSarresid.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.txtSarresid.Visible = false;
            // 
            // LblSarresid
            // 
            this.LblSarresid.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.LblSarresid.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LblSarresid.ForeColor = System.Drawing.Color.Black;
            this.LblSarresid.Location = new System.Drawing.Point(323, 19);
            this.LblSarresid.Margin = new System.Windows.Forms.Padding(4);
            this.LblSarresid.Name = "LblSarresid";
            this.LblSarresid.Size = new System.Drawing.Size(148, 26);
            this.LblSarresid.TabIndex = 72;
            this.LblSarresid.Text = "تاریخ سررسید چک :";
            this.LblSarresid.Visible = false;
            // 
            // txtbank
            // 
            this.txtbank.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtbank.Border.Class = "TextBoxBorder";
            this.txtbank.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtbank.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "bank", true));
            this.txtbank.DisabledBackColor = System.Drawing.Color.White;
            this.txtbank.ForeColor = System.Drawing.Color.Black;
            this.txtbank.Location = new System.Drawing.Point(66, 53);
            this.txtbank.Margin = new System.Windows.Forms.Padding(4);
            this.txtbank.MaxLength = 0;
            this.txtbank.Name = "txtbank";
            this.txtbank.PreventEnterBeep = true;
            this.txtbank.Size = new System.Drawing.Size(289, 28);
            this.txtbank.TabIndex = 4;
            this.txtbank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbank_KeyDown);
            // 
            // txtfish
            // 
            this.txtfish.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtfish.Border.Class = "TextBoxBorder";
            this.txtfish.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtfish.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "fish", true));
            this.txtfish.DisabledBackColor = System.Drawing.Color.White;
            this.txtfish.ForeColor = System.Drawing.Color.Black;
            this.txtfish.Location = new System.Drawing.Point(494, 52);
            this.txtfish.Margin = new System.Windows.Forms.Padding(4);
            this.txtfish.MaxLength = 0;
            this.txtfish.Name = "txtfish";
            this.txtfish.PreventEnterBeep = true;
            this.txtfish.Size = new System.Drawing.Size(186, 28);
            this.txtfish.TabIndex = 3;
            this.txtfish.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtfish_KeyDown);
            // 
            // buttonX2
            // 
            this.buttonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX2.BackColor = System.Drawing.Color.Transparent;
            this.buttonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX2.Location = new System.Drawing.Point(66, 175);
            this.buttonX2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonX2.Name = "buttonX2";
            this.buttonX2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.buttonX2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.buttonX2.Size = new System.Drawing.Size(113, 27);
            this.buttonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX2.Symbol = "";
            this.buttonX2.SymbolColor = System.Drawing.Color.Brown;
            this.buttonX2.SymbolSize = 9F;
            this.buttonX2.TabIndex = 68;
            this.buttonX2.Text = "حذف";
            this.buttonX2.Click += new System.EventHandler(this.buttonX2_Click);
            // 
            // btn_add
            // 
            this.btn_add.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_add.Location = new System.Drawing.Point(302, 175);
            this.btn_add.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_add.Name = "btn_add";
            this.btn_add.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_add.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btn_add.Size = new System.Drawing.Size(114, 27);
            this.btn_add.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_add.Symbol = "";
            this.btn_add.SymbolColor = System.Drawing.Color.Green;
            this.btn_add.SymbolSize = 9F;
            this.btn_add.TabIndex = 67;
            this.btn_add.Text = "اضافه - F1";
            this.btn_add.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.Location = new System.Drawing.Point(183, 175);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlS);
            this.btn_save.Size = new System.Drawing.Size(111, 27);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "ثبت اطلاعات";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(670, 462);
            this.labelX11.Margin = new System.Windows.Forms.Padding(4);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(139, 28);
            this.labelX11.TabIndex = 65;
            this.labelX11.Text = "بدهکار :";
            // 
            // labelX10
            // 
            this.labelX10.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.ForeColor = System.Drawing.Color.Black;
            this.labelX10.Location = new System.Drawing.Point(1066, 466);
            this.labelX10.Margin = new System.Windows.Forms.Padding(4);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(139, 28);
            this.labelX10.TabIndex = 63;
            this.labelX10.Text = "مجموع مبالغ پرداختی :";
            // 
            // cmbahveh
            // 
            this.cmbahveh.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.ghabzpardakhtBindingSource, "nahve_pardakht", true));
            this.cmbahveh.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "nahve_pardakht", true));
            this.cmbahveh.DisplayMember = "Text";
            this.cmbahveh.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbahveh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbahveh.ForeColor = System.Drawing.Color.Black;
            this.cmbahveh.FormattingEnabled = true;
            this.cmbahveh.ItemHeight = 22;
            this.cmbahveh.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6});
            this.cmbahveh.Location = new System.Drawing.Point(494, 16);
            this.cmbahveh.Margin = new System.Windows.Forms.Padding(4);
            this.cmbahveh.Name = "cmbahveh";
            this.cmbahveh.Size = new System.Drawing.Size(184, 28);
            this.cmbahveh.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbahveh.TabIndex = 0;
            this.cmbahveh.SelectedIndexChanged += new System.EventHandler(this.cmbahveh_SelectedIndexChanged);
            this.cmbahveh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbahveh_KeyDown);
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "بانک";
            this.comboItem7.Value = "بانک";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "دستی";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "پوز";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "فیش";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "اینترنت";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "چک";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "تهاتر";
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.ForeColor = System.Drawing.Color.Black;
            this.labelX7.Location = new System.Drawing.Point(688, 20);
            this.labelX7.Margin = new System.Windows.Forms.Padding(4);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(92, 26);
            this.labelX7.TabIndex = 60;
            this.labelX7.Text = "نحوه پرداخت:";
            // 
            // txtTozihat
            // 
            this.txtTozihat.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTozihat.Border.Class = "TextBoxBorder";
            this.txtTozihat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTozihat.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ghabzpardakhtBindingSource, "tozihat", true));
            this.txtTozihat.DisabledBackColor = System.Drawing.Color.White;
            this.txtTozihat.ForeColor = System.Drawing.Color.Black;
            this.txtTozihat.Location = new System.Drawing.Point(66, 114);
            this.txtTozihat.Margin = new System.Windows.Forms.Padding(4);
            this.txtTozihat.MaxLength = 0;
            this.txtTozihat.Multiline = true;
            this.txtTozihat.Name = "txtTozihat";
            this.txtTozihat.PreventEnterBeep = true;
            this.txtTozihat.Size = new System.Drawing.Size(1045, 51);
            this.txtTozihat.TabIndex = 5;
            this.txtTozihat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTozihat_KeyDown);
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(1119, 118);
            this.labelX6.Margin = new System.Windows.Forms.Padding(4);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(92, 26);
            this.labelX6.TabIndex = 59;
            this.labelX6.Text = "توضیحات :";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(326, 54);
            this.labelX5.Margin = new System.Windows.Forms.Padding(4);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(67, 26);
            this.labelX5.TabIndex = 57;
            this.labelX5.Text = "بانک :";
            // 
            // lblNehveh
            // 
            this.lblNehveh.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblNehveh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNehveh.ForeColor = System.Drawing.Color.Black;
            this.lblNehveh.Location = new System.Drawing.Point(688, 54);
            this.lblNehveh.Margin = new System.Windows.Forms.Padding(4);
            this.lblNehveh.Name = "lblNehveh";
            this.lblNehveh.Size = new System.Drawing.Size(140, 26);
            this.lblNehveh.TabIndex = 55;
            this.lblNehveh.Text = "شماره فیش / پیگیری";
            // 
            // TxtTarikhVosool
            // 
            this.TxtTarikhVosool.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.ghabzpardakhtBindingSource, "tarikh_vosool", true));
            this.TxtTarikhVosool.Location = new System.Drawing.Point(925, 20);
            this.TxtTarikhVosool.Name = "TxtTarikhVosool";
            this.TxtTarikhVosool.Size = new System.Drawing.Size(186, 25);
            this.TxtTarikhVosool.TabIndex = 50;
            this.TxtTarikhVosool.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.TxtTarikhVosool.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtTarikhVosool_KeyDown);
            // 
            // labelX9
            // 
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(1119, 20);
            this.labelX9.Margin = new System.Windows.Forms.Padding(4);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(92, 26);
            this.labelX9.TabIndex = 52;
            this.labelX9.Text = "تاریخ پرداخت :";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(1119, 54);
            this.labelX8.Margin = new System.Windows.Forms.Padding(4);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(92, 26);
            this.labelX8.TabIndex = 51;
            this.labelX8.Text = "مبلغ پرداختی :";
            // 
            // dgvPardakht
            // 
            this.dgvPardakht.AllowUserToAddRows = false;
            this.dgvPardakht.AllowUserToDeleteRows = false;
            this.dgvPardakht.AllowUserToResizeColumns = false;
            this.dgvPardakht.AllowUserToResizeRows = false;
            this.dgvPardakht.AutoGenerateColumns = false;
            this.dgvPardakht.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPardakht.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPardakht.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPardakht.ColumnHeadersHeight = 30;
            this.dgvPardakht.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.tarikhsabtDataGridViewTextBoxColumn,
            this.vaziatDataGridViewCheckBoxColumn,
            this.bankDataGridViewTextBoxColumn,
            this.fishDataGridViewTextBoxColumn,
            this.mablaghDataGridViewTextBoxColumn,
            this.tarikhvosoolDataGridViewTextBoxColumn,
            this.nahvepardakhtDataGridViewTextBoxColumn});
            this.dgvPardakht.DataSource = this.ghabzpardakhtBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPardakht.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPardakht.EnableHeadersVisualStyles = false;
            this.dgvPardakht.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvPardakht.Location = new System.Drawing.Point(47, 210);
            this.dgvPardakht.Margin = new System.Windows.Forms.Padding(4);
            this.dgvPardakht.Name = "dgvPardakht";
            this.dgvPardakht.ReadOnly = true;
            this.dgvPardakht.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("B Yekan", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPardakht.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPardakht.RowHeadersVisible = false;
            this.dgvPardakht.RowTemplate.Height = 40;
            this.dgvPardakht.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPardakht.Size = new System.Drawing.Size(1163, 241);
            this.dgvPardakht.TabIndex = 43;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.FillWeight = 40F;
            this.id.HeaderText = "کد";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // tarikhsabtDataGridViewTextBoxColumn
            // 
            this.tarikhsabtDataGridViewTextBoxColumn.DataPropertyName = "tarikh_sabt";
            this.tarikhsabtDataGridViewTextBoxColumn.HeaderText = "تاریخ ثبت";
            this.tarikhsabtDataGridViewTextBoxColumn.Name = "tarikhsabtDataGridViewTextBoxColumn";
            this.tarikhsabtDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhsabtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhsabtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // vaziatDataGridViewCheckBoxColumn
            // 
            this.vaziatDataGridViewCheckBoxColumn.DataPropertyName = "vaziat";
            this.vaziatDataGridViewCheckBoxColumn.HeaderText = "وضعیت";
            this.vaziatDataGridViewCheckBoxColumn.Name = "vaziatDataGridViewCheckBoxColumn";
            this.vaziatDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // bankDataGridViewTextBoxColumn
            // 
            this.bankDataGridViewTextBoxColumn.DataPropertyName = "bank";
            this.bankDataGridViewTextBoxColumn.HeaderText = "بانک";
            this.bankDataGridViewTextBoxColumn.Name = "bankDataGridViewTextBoxColumn";
            this.bankDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fishDataGridViewTextBoxColumn
            // 
            this.fishDataGridViewTextBoxColumn.DataPropertyName = "fish";
            this.fishDataGridViewTextBoxColumn.HeaderText = "فیش";
            this.fishDataGridViewTextBoxColumn.Name = "fishDataGridViewTextBoxColumn";
            this.fishDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mablaghDataGridViewTextBoxColumn
            // 
            this.mablaghDataGridViewTextBoxColumn.DataPropertyName = "mablagh";
            dataGridViewCellStyle2.Format = "0,0";
            this.mablaghDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.mablaghDataGridViewTextBoxColumn.HeaderText = "مبلغ";
            this.mablaghDataGridViewTextBoxColumn.Name = "mablaghDataGridViewTextBoxColumn";
            this.mablaghDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tarikhvosoolDataGridViewTextBoxColumn
            // 
            this.tarikhvosoolDataGridViewTextBoxColumn.DataPropertyName = "tarikh_vosool";
            this.tarikhvosoolDataGridViewTextBoxColumn.HeaderText = "تاریخ وصول";
            this.tarikhvosoolDataGridViewTextBoxColumn.Name = "tarikhvosoolDataGridViewTextBoxColumn";
            this.tarikhvosoolDataGridViewTextBoxColumn.ReadOnly = true;
            this.tarikhvosoolDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tarikhvosoolDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // nahvepardakhtDataGridViewTextBoxColumn
            // 
            this.nahvepardakhtDataGridViewTextBoxColumn.DataPropertyName = "nahve_pardakht";
            this.nahvepardakhtDataGridViewTextBoxColumn.HeaderText = "نحوه پرداخت";
            this.nahvepardakhtDataGridViewTextBoxColumn.Name = "nahvepardakhtDataGridViewTextBoxColumn";
            this.nahvepardakhtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // txtTarikhSAbt
            // 
            this.txtTarikhSAbt.DataBindings.Add(new System.Windows.Forms.Binding("SelectedDateTime", this.ghabzpardakhtBindingSource, "tarikh_sabt", true));
            this.txtTarikhSAbt.Location = new System.Drawing.Point(925, 116);
            this.txtTarikhSAbt.Name = "txtTarikhSAbt";
            this.txtTarikhSAbt.Size = new System.Drawing.Size(186, 25);
            this.txtTarikhSAbt.TabIndex = 73;
            this.txtTarikhSAbt.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // Is_ab_checkBox
            // 
            this.Is_ab_checkBox.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.Is_ab_checkBox.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Is_ab_checkBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.ghabzpardakhtBindingSource, "is_ab", true));
            this.Is_ab_checkBox.ForeColor = System.Drawing.Color.Black;
            this.Is_ab_checkBox.Location = new System.Drawing.Point(349, 173);
            this.Is_ab_checkBox.Margin = new System.Windows.Forms.Padding(4);
            this.Is_ab_checkBox.Name = "Is_ab_checkBox";
            this.Is_ab_checkBox.Size = new System.Drawing.Size(52, 28);
            this.Is_ab_checkBox.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.Is_ab_checkBox.TabIndex = 75;
            // 
            // ghabz_pardakhtTableAdapter
            // 
            this.ghabz_pardakhtTableAdapter.ClearBeforeFill = true;
            // 
            // ghabz_pardakhtTableAdapter1
            // 
            this.ghabz_pardakhtTableAdapter1.ClearBeforeFill = true;
            // 
            // FrmGhabzPardakht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 690);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmGhabzPardakht";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmGhabzPardakht_FormClosed);
            this.Load += new System.EventHandler(this.FrmGhabzPardakht_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmGhabzPardakht_KeyDown);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ghabzpardakhtBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainAbDataset1)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPardakht)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.TextBoxX txt_ghabz_code;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMablaghHoroof;
        private DevComponents.DotNetBar.Controls.TextBoxX txtdoreh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtbank;
        private DevComponents.DotNetBar.Controls.TextBoxX txtfish;
        private DevComponents.DotNetBar.ButtonX buttonX2;
        private DevComponents.DotNetBar.ButtonX btn_add;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbahveh;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTozihat;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX lblNehveh;
        private FarsiLibrary.Win.Controls.FADatePicker TxtTarikhVosool;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvPardakht;
        private System.Windows.Forms.BindingSource ghabzpardakhtBindingSource;
        private MainDataSestTableAdapters.ghabz_pardakhtTableAdapter ghabz_pardakhtTableAdapter;
        private FarsiLibrary.Win.Controls.FADatePicker txtSarresid;
        private DevComponents.DotNetBar.LabelX LblSarresid;
        private DevComponents.DotNetBar.Controls.CheckBoxX ch_vaziat;
        private FarsiLibrary.Win.Controls.FADatePicker txtTarikhSAbt;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGharardad;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTarikhSoddor;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGharardadMain;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGhabzCodeMain;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDorehMain;
        private Ab.MainAbDataset mainAbDataset1;
        private Ab.MainAbDatasetTableAdapters.ghabz_pardakhtTableAdapter ghabz_pardakhtTableAdapter1;
        private DevComponents.DotNetBar.Controls.CheckBoxX Is_ab_checkBox;
        private MoneyTextBox txtBaghimadeh;
        private MoneyTextBox txtPardakhti;
        private DevComponents.DotNetBar.ButtonX buttonX3;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPardakht;
        private DevComponents.Editors.ComboItem comboItem6;
        private MoneyTextBox txtMablaghKol;
        private DevComponents.DotNetBar.LabelX lblMablaghHorrof;
        private DevComponents.Editors.ComboItem comboItem7;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhsabtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vaziatDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bankDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fishDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablaghDataGridViewTextBoxColumn;
        private FarsiLibrary.Win.Controls.DataGridViewFADateTimePickerColumn tarikhvosoolDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nahvepardakhtDataGridViewTextBoxColumn;
        private MoneyTextBox txtBestankar;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.Label label4;
        private MoneyTextBox txtBestankarFromLAstDoreh;
    }
}