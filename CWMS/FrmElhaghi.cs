﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace CWMS
{
    public partial class FrmElhaghi : MyMetroForm
    {
        int _gharardad;
        int row;

        public FrmElhaghi(int gharardad)
        {
            InitializeComponent();
            _gharardad = gharardad;
        }

        private void FrmElhaghi_Load(object sender, EventArgs e)
        {
            try
            {
                this.moshtarekinTableAdapter.Fill(this.cWMSDataSet.moshtarekin);
                if (cWMSDataSet.moshtarekin.FindBygharardad(_gharardad).IselhaghiNull() == true)
                    return;
                string[] list = cWMSDataSet.moshtarekin.FindBygharardad(_gharardad).elhaghi.Split(',');
                try
                {
                    foreach (string elhaghi in list)
                    {
                        var result = cWMSDataSet.moshtarekin.First(p => p.gharardad == Convert.ToInt32(elhaghi));
                        dgvElhaghi.Rows.Add(elhaghi, result["co_name"], result["modir_amel"]);
                    }
                }
                catch (Exception)
                {

                }
            }
            catch (Exception ert)
            {
                Classes.ClsMain.logError(ert);
                
               
            }
           
        }

        private void buttonX3_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1)
            {
                Payam.Show("شماره قرارداد وارد شده موجود نیست");
            }
            else
            {
                foreach (DataGridViewRow row in dgvElhaghi.Rows)
                {
                    if (row.Cells["gharardad"].Value.ToString() == comboBox1.SelectedValue.ToString())
                    {
                        return;
                    }
                }
                var result = cWMSDataSet.moshtarekin.First(p => p.gharardad == (int)comboBox1.SelectedValue);
                dgvElhaghi.Rows.Add(comboBox1.SelectedValue, result["co_name"], result["modir_amel"]);
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            int temp;
            if (!Int32.TryParse(comboBox1.Text, out temp))
            {
                return;
            }
            int n = moshtarekinBindingSource.Find("gharardad", Convert.ToInt32(comboBox1.Text));
            if (n == -1)
            {
                textBox1.Visible = false;
            }
            else
            {
                textBox1.Visible = true;
                moshtarekinBindingSource.Position = n;
            }
            comboBox1.SelectionStart = comboBox1.Text.Length;
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonX2_Click(object sender, EventArgs e)
        {
            StringBuilder elhaghi = new StringBuilder();

            foreach (DataGridViewRow row in dgvElhaghi.Rows)
            {
                elhaghi.Append(row.Cells["gharardad"].Value + ",");
            }
            if (elhaghi.Length == 0)
            {
                Classes.ClsMain.ExecuteNoneQuery("update moshtarekin set elhaghi = null where gharardad = " + _gharardad);
                return;
            }
            string ss = elhaghi.ToString(0, elhaghi.Length - 1);
            Classes.ClsMain.ExecuteNoneQuery("update moshtarekin set elhaghi = '"
                + elhaghi.ToString(0, elhaghi.Length - 1) + "' where gharardad = " + _gharardad);
            Payam.Show("اطلاعات با موفقیت در سیستم ذخیره شد");
        }

        private void buttonX4_Click(object sender, EventArgs e)
        {
            if (Cpayam.Show("ایا با حذف این قرارداد موافقید؟") == DialogResult.Yes)
            {
                dgvElhaghi.Rows.RemoveAt(dgvElhaghi.CurrentRow.Index);
                Payam.Show("لطفا در هنگام بستن این فرم ، جهت ذخیره تغییرات بر روی دکمه ذخیره کلیک نمایید");
            }

        }
    }
}