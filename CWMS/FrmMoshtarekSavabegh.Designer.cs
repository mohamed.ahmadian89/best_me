﻿namespace CWMS
{
    partial class FrmMoshtarekSavabegh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtsavabegh = new System.Windows.Forms.TextBox();
            this.moshtarekinBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDataSest1 = new CWMS.MainDataSest();
            this.btn_save = new DevComponents.DotNetBar.ButtonX();
            this.moshtarekinTableAdapter = new CWMS.MainDataSestTableAdapters.moshtarekinTableAdapter();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtsavabegh
            // 
            this.txtsavabegh.BackColor = System.Drawing.Color.White;
            this.txtsavabegh.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moshtarekinBindingSource, "savabegh", true));
            this.txtsavabegh.ForeColor = System.Drawing.Color.Black;
            this.txtsavabegh.Location = new System.Drawing.Point(13, 49);
            this.txtsavabegh.Margin = new System.Windows.Forms.Padding(4);
            this.txtsavabegh.Multiline = true;
            this.txtsavabegh.Name = "txtsavabegh";
            this.txtsavabegh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtsavabegh.Size = new System.Drawing.Size(898, 268);
            this.txtsavabegh.TabIndex = 0;
            // 
            // moshtarekinBindingSource
            // 
            this.moshtarekinBindingSource.DataMember = "moshtarekin";
            this.moshtarekinBindingSource.DataSource = this.mainDataSest1;
            // 
            // mainDataSest1
            // 
            this.mainDataSest1.DataSetName = "MainDataSest";
            this.mainDataSest1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btn_save
            // 
            this.btn_save.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_save.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btn_save.Location = new System.Drawing.Point(13, 325);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4);
            this.btn_save.Name = "btn_save";
            this.btn_save.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10, 2, 2, 10);
            this.btn_save.Size = new System.Drawing.Size(138, 42);
            this.btn_save.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btn_save.Symbol = "";
            this.btn_save.SymbolColor = System.Drawing.Color.Green;
            this.btn_save.SymbolSize = 9F;
            this.btn_save.TabIndex = 26;
            this.btn_save.Text = "ذخیره سوابق";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // moshtarekinTableAdapter
            // 
            this.moshtarekinTableAdapter.ClearBeforeFill = true;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(41, 14);
            this.labelX3.Margin = new System.Windows.Forms.Padding(4);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(967, 28);
            this.labelX3.TabIndex = 27;
            this.labelX3.Tag = "g";
            this.labelX3.Text = "در این قسمت می توانید سوابق و تاریخچه مربوط به یک قرارداد را یاد داشت کرده در این" +
    "ده بتوانید با توجه به این اطلاعات ، تصمیمات مدیریتی صحیح را اتخاذ کنید";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // FrmMoshtarekSavabegh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 374);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.txtsavabegh);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Yekan", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMoshtarekSavabegh";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmMoshtarekSavabegh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.moshtarekinBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataSest1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtsavabegh;
        private DevComponents.DotNetBar.ButtonX btn_save;
        private MainDataSest mainDataSest1;
        private System.Windows.Forms.BindingSource moshtarekinBindingSource;
        private MainDataSestTableAdapters.moshtarekinTableAdapter moshtarekinTableAdapter;
        private DevComponents.DotNetBar.LabelX labelX3;
    }
}